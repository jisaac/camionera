/**CARGA ROLES**/
USE [logistica]
GO

INSERT INTO [dbo].[roles]
           ([rol]
           ,[estado])
     VALUES
           ('GERENCIA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[roles]
           ([rol]
           ,[estado])
     VALUES
           ('LOGISTICA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[roles]
           ([rol]
           ,[estado])
     VALUES
           ('MANTENIMIENTO'
           ,'HABILITADO')
GO

/**FIN CARGA ROLES**/



/**CARGA USUARIOS**/

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('G1'
           ,'G1'
           ,1
           ,1
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('G2'
           ,'G2'
           ,2
           ,1
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('G3'
           ,'G3'
           ,3
           ,1
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('L1'
           ,'L1'
           ,4
           ,2
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('L2'
           ,'L2'
           ,5
           ,2
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('L3'
           ,'L3'
           ,6
           ,2
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('M1'
           ,'M1'
           ,7
           ,3
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('M2'
           ,'M2'
           ,8
           ,3
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[usuarios]
           ([usuario]
           ,[password]
           ,[num_legajo]
           ,[id_rol]
           ,[estado])
     VALUES
           ('M9'
           ,'M9'
           ,9
           ,3
           ,'HABILITADO')
GO

/**FIN CARGA USUARIOS **/



/** CARGA CHOFERES **/

USE [logistica]
GO

INSERT INTO [dbo].[chofer]
           ([num_legajo]
           ,[nombre]
           ,[apellido]
           ,[num_cedula]
           ,[direccion]
           ,[tel_personal]
           ,[tel_trabajo]
           ,[estado])
     VALUES
           (1
           ,'JUAN'
           ,'MIGUELITO'
           ,'1234'
           ,'CALLE 1'
           ,'23456'
           ,'23456'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[chofer]
           ([num_legajo]
           ,[nombre]
           ,[apellido]
           ,[num_cedula]
           ,[direccion]
           ,[tel_personal]
           ,[tel_trabajo]
           ,[estado])
     VALUES
           (2
           ,'TOMAS'
           ,'PUERTA'
           ,'4321'
           ,'CALLE 2'
           ,'23456'
           ,'23456'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[chofer]
           ([num_legajo]
           ,[nombre]
           ,[apellido]
           ,[num_cedula]
           ,[direccion]
           ,[tel_personal]
           ,[tel_trabajo]
           ,[estado])
     VALUES
           (3
           ,'CHANGO'
           ,'PEREZ'
           ,'2341'
           ,'CALLE 3'
           ,'23456'
           ,'23456'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[chofer]
           ([num_legajo]
           ,[nombre]
           ,[apellido]
           ,[num_cedula]
           ,[direccion]
           ,[tel_personal]
           ,[tel_trabajo]
           ,[estado])
     VALUES
           (4
           ,'MARIO'
           ,'FUNES'
           ,'3412'
           ,'CALLE 4'
           ,'23456'
           ,'23456'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[chofer]
           ([num_legajo]
           ,[nombre]
           ,[apellido]
           ,[num_cedula]
           ,[direccion]
           ,[tel_personal]
           ,[tel_trabajo]
           ,[estado])
     VALUES
           (5
           ,'ALAN'
           ,'LEONTES'
           ,'4522'
           ,'CALLE 5'
           ,'23456'
           ,'23456'
           ,'HABILITADO')
GO
/** FIN CARGA CHOFERES **/




/**CARGA MARCA ACOPLADOS**/

USE [logistica]
GO

INSERT INTO [dbo].[marca_acoplado]
           ([marca]
           ,[estado])
     VALUES
           ('AGRALE'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_acoplado]
           ([marca]
           ,[estado])
     VALUES
           ('CONESE'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_acoplado]
           ([marca]
           ,[estado])
     VALUES
           ('AGROAR'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_acoplado]
           ([marca]
           ,[estado])
     VALUES
           ('SAN CARLOS'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_acoplado]
           ([marca]
           ,[estado])
     VALUES
           ('AGROTEC'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_acoplado]
           ([marca]
           ,[estado])
     VALUES
           ('ENERMOL'
           ,'HABILITADO')
GO

/**FIN MARCA CARGA ACOPLADOS**/




/**CARGA MODELO ACOPLADOS**/

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SP1324'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SR3491'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SD4010'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SL2304'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SJ3491'
           ,'HABILITADO'
           ,1)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T123'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T453'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T456'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T546'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D34'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D44'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D54'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D64'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P12'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P15'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P13'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P16'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX14'
           ,'HABILITADO'
           ,5)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX54'
           ,'HABILITADO'
           ,5)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX145'
           ,'HABILITADO'
           ,5)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_acoplado]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX542'
           ,'HABILITADO'
           ,5)
GO

/**FIN MODELO CARGA ACOPLADOS**/



/**CARGA TIPO DE CARGA ACOPLADOS**/

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_acoplado]
           ([tipo]
           ,[estado])
     VALUES
           ('MAIZ'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_acoplado]
           ([tipo]
           ,[estado])
     VALUES
           ('GANADO'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_acoplado]
           ([tipo]
           ,[estado])
     VALUES
           ('HUEVO'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_acoplado]
           ([tipo]
           ,[estado])
     VALUES
           ('CHATARRA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_acoplado]
           ([tipo]
           ,[estado])
     VALUES
           ('SOJA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_acoplado]
           ([tipo]
           ,[estado])
     VALUES
           ('COMBUSTIBLE'
           ,'HABILITADO')
GO

/**FIN TIPO DE CARGA ACOPLADOS**/

/**CARGA ACOPLADOS**/

USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('ASD123')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('C111')
GO

USE [logistica]
GO

INSERT INTO [dbo].[acoplado]
           ([dominio]
           ,[nro_chasis]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tipo_carga]
           ,[tipo_acoplado]
           ,[tara]
           ,[capacidad_carga]
           ,[volumen]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[cant_ejes]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('ASD123'
           ,'C111'
           ,1
           ,1
           ,2018
           ,1
           ,'REMOLQUE'
           ,5000
           ,5000
           ,500
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,3.5
           ,3.5
           ,3
           ,14
           ,6
           ,'2018-02-01'
           ,'HABILITADO')
GO


USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('TET340')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('K847')
GO

USE [logistica]
GO

INSERT INTO [dbo].[acoplado]
           ([dominio]
           ,[nro_chasis]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tipo_carga]
           ,[tipo_acoplado]
           ,[tara]
           ,[capacidad_carga]
           ,[volumen]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[cant_ejes]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('TET340'
           ,'K847'
           ,1
           ,2
           ,2018
           ,2
           ,'SEMIREMOLQUE'
           ,5000
           ,5000
           ,500
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,3.5
           ,3.5
           ,3
           ,14
           ,6
           ,'2018-02-01'
           ,'HABILITADO')
GO


USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('NEP401')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('P039')
GO

USE [logistica]
GO

INSERT INTO [dbo].[acoplado]
           ([dominio]
           ,[nro_chasis]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tipo_carga]
           ,[tipo_acoplado]
           ,[tara]
           ,[capacidad_carga]
           ,[volumen]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[cant_ejes]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('NEP401'
           ,'P039'
           ,2
           ,1
           ,2017
           ,1
           ,'SEMIREMOLQUE'
           ,5000
           ,5000
           ,500
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,3.5
           ,3.5
           ,3
           ,14
           ,6
           ,'2018-02-01'
           ,'HABILITADO')
GO

/**FIN ACOPLADOS**/

/**CARGA MARCA CAMION**/

USE [logistica]
GO

INSERT INTO [dbo].[marca_camion]
           ([marca]
           ,[estado])
     VALUES
           ('FORD'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_camion]
           ([marca]
           ,[estado])
     VALUES
           ('SCANIA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_camion]
           ([marca]
           ,[estado])
     VALUES
           ('IVECO'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_camion]
           ([marca]
           ,[estado])
     VALUES
           ('RENAULT'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_camion]
           ([marca]
           ,[estado])
     VALUES
           ('MERCEDES BENZ'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[marca_camion]
           ([marca]
           ,[estado])
     VALUES
           ('VOLSKWAGEN'
           ,'HABILITADO')
GO

/**FIN MARCA CAMION**/


/**CARGA MODELO CAMION **/

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SP1324'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SR3491'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SD4010'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SL2304'
           ,'HABILITADO'
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('SJ3491'
           ,'HABILITADO'
           ,1)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T123'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T453'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T456'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('T546'
           ,'HABILITADO'
           ,2)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D34'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D44'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D54'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('D64'
           ,'HABILITADO'
           ,3)
GO


USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P12'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P15'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P13'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('P16'
           ,'HABILITADO'
           ,4)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX14'
           ,'HABILITADO'
           ,5)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX54'
           ,'HABILITADO'
           ,5)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX145'
           ,'HABILITADO'
           ,5)
GO

USE [logistica]
GO

INSERT INTO [dbo].[modelo_camion]
           ([modelo]
           ,[estado]
           ,[id_marca])
     VALUES
           ('FX542'
           ,'HABILITADO'
           ,5)
GO

/**FIN MODELO CAMION **/

/**CARGA TIPO DE CARGA CAMION **/

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_rigido]
           ([tipo]
           ,[estado])
     VALUES
           ('MAIZ'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_rigido]
           ([tipo]
           ,[estado])
     VALUES
           ('GANADO'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_rigido]
           ([tipo]
           ,[estado])
     VALUES
           ('HUEVO'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_rigido]
           ([tipo]
           ,[estado])
     VALUES
           ('CHATARRA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_rigido]
           ([tipo]
           ,[estado])
     VALUES
           ('SOJA'
           ,'HABILITADO')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tipo_carga_rigido]
           ([tipo]
           ,[estado])
     VALUES
           ('COMBUSTIBLE'
           ,'HABILITADO')
GO

/**FIN TIPO DE CARGA CAMION **/

/** CARGA CAMION **/

USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('PRR032')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('T451')
GO

USE [logistica]
GO

INSERT INTO [dbo].[camion]
           ([dominio]
           ,[nro_chasis]
           ,[nro_motor]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tara]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('PRR032'
           ,'T451'
           ,'51SD2'
           ,2
           ,1
           ,2018
           ,3000
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,'2018-02-01'
           ,'HABILITADO')
GO



USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('GKE032')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('Y451')
GO

USE [logistica]
GO

INSERT INTO [dbo].[camion]
           ([dominio]
           ,[nro_chasis]
           ,[nro_motor]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tara]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('GKE032'
           ,'Y451'
           ,'50AS2'
           ,2
           ,1
           ,2018
           ,3000
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,'2018-02-01'
           ,'HABILITADO')
GO



USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('LAK204')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('K934')
GO

USE [logistica]
GO

INSERT INTO [dbo].[camion]
           ([dominio]
           ,[nro_chasis]
           ,[nro_motor]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tara]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('LAK204'
           ,'K934'
           ,'51S22'
           ,4
           ,1
           ,2018
           ,3000
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,'2018-02-01'
           ,'HABILITADO')
GO



USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('MJK393')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('A230')
GO

USE [logistica]
GO

INSERT INTO [dbo].[camion]
           ([dominio]
           ,[nro_chasis]
           ,[nro_motor]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tara]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('MJK393'
           ,'A230'
           ,'51SD3'
           ,3
           ,1
           ,2018
           ,3000
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,'2018-02-01'
           ,'HABILITADO')

GO



USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('XCV341')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('T122')
GO

USE [logistica]
GO

INSERT INTO [dbo].[camion]
           ([dominio]
           ,[nro_chasis]
           ,[nro_motor]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tara]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('XCV341'
           ,'T122'
           ,'51SD1'
           ,2
           ,1
           ,2018
           ,3000
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,'2018-02-01'
           ,'HABILITADO')
GO



USE [logistica]
GO

INSERT INTO [dbo].[dominio]
           ([dominio])
     VALUES
           ('QWE341')
GO

USE [logistica]
GO

INSERT INTO [dbo].[numero_chasis]
           ([num_chasis])
     VALUES
           ('R421')
GO

USE [logistica]
GO

INSERT INTO [dbo].[camion]
           ([dominio]
           ,[nro_chasis]
           ,[nro_motor]
           ,[marca]
           ,[modelo]
           ,[año]
           ,[tara]
           ,[km_unidad]
           ,[km_service]
           ,[km_cambio_neumaticos]
           ,[km_service_suma]
           ,[km_cambio_neumaticos_suma]
           ,[fecha_alta]
           ,[estado])
     VALUES
           ('QWE341'
           ,'R421'
           ,'51S52'
           ,3
           ,1
           ,2018
           ,3000
           ,0
           ,50000
           ,80000
           ,0
           ,0
           ,'2018-02-01'
           ,'HABILITADO')
GO



/** FIN CARGA CAMION **/


/** CARGA RIGIDO **/

USE [logistica]
GO

INSERT INTO [dbo].[rigido]
           ([dominio]
           ,[tipo_carga]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[volumen]
           ,[capacidad_carga]
           ,[enganche])
     VALUES
           ('XCV341'
           ,2
           ,2
           ,3
           ,3.4
           ,10
           ,2000
           ,5000
           ,1)
GO

USE [logistica]
GO

INSERT INTO [dbo].[rigido]
           ([dominio]
           ,[tipo_carga]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[volumen]
           ,[capacidad_carga]
           ,[enganche])
     VALUES
           ('QWE341'
           ,2
           ,2
           ,3
           ,3.4
           ,10
           ,2000
           ,5000
           ,1)
GO


USE [logistica]
GO

INSERT INTO [dbo].[rigido]
           ([dominio]
           ,[tipo_carga]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[volumen]
           ,[capacidad_carga]
           ,[enganche])
     VALUES
           ('MJK393'
           ,3
           ,2
           ,3
           ,3.4
           ,10
           ,2000
           ,5000
           ,0)
GO

USE [logistica]
GO

INSERT INTO [dbo].[rigido]
           ([dominio]
           ,[tipo_carga]
           ,[altura]
           ,[ancho_interior]
           ,[ancho_exterior]
           ,[longitud]
           ,[volumen]
           ,[capacidad_carga]
           ,[enganche])
     VALUES
           ('GKE032'
           ,3
           ,2
           ,3
           ,3.4
           ,10
           ,2000
           ,5000
           ,0)
GO


/** FIN CARGA RIGIDO **/

/** CARGA TRACTOCAMION **/

USE [logistica]
GO

INSERT INTO [dbo].[tractocamion]
           ([dominio])
     VALUES
           ('LAK204')
GO

USE [logistica]
GO

INSERT INTO [dbo].[tractocamion]
           ([dominio])
     VALUES
           ('PRR032')
GO


/** FIN CARGA TRACTOCAMION **/
