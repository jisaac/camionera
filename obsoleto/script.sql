USE [master]
GO
/****** Object:  Database [logistica]    Script Date: 18/4/2018 22:28:24 ******/
CREATE DATABASE [logistica]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'logistica', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\logistica.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'logistica_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\logistica_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [logistica] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [logistica].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [logistica] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [logistica] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [logistica] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [logistica] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [logistica] SET ARITHABORT OFF 
GO
ALTER DATABASE [logistica] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [logistica] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [logistica] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [logistica] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [logistica] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [logistica] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [logistica] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [logistica] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [logistica] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [logistica] SET  DISABLE_BROKER 
GO
ALTER DATABASE [logistica] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [logistica] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [logistica] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [logistica] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [logistica] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [logistica] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [logistica] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [logistica] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [logistica] SET  MULTI_USER 
GO
ALTER DATABASE [logistica] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [logistica] SET DB_CHAINING OFF 
GO
ALTER DATABASE [logistica] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [logistica] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [logistica] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [logistica] SET QUERY_STORE = OFF
GO
USE [logistica]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [logistica]
GO
/****** Object:  Table [dbo].[acoplado]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acoplado](
	[dominio] [nvarchar](10) NOT NULL,
	[nro_chasis] [nvarchar](20) NOT NULL,
	[marca] [int] NOT NULL,
	[modelo] [int] NOT NULL,
	[año] [int] NULL,
	[tipo_carga] [int] NOT NULL,
	[tipo_acoplado] [nvarchar](20) NOT NULL,
	[tara] [float] NULL,
	[capacidad_carga] [float] NOT NULL,
	[volumen] [float] NOT NULL,
	[km_unidad] [float] NOT NULL,
	[km_service] [float] NOT NULL,
	[km_cambio_neumaticos] [float] NOT NULL,
	[km_service_suma] [float] NOT NULL,
	[km_cambio_neumaticos_suma] [float] NOT NULL,
	[altura] [float] NOT NULL,
	[ancho_interior] [float] NOT NULL,
	[ancho_exterior] [float] NOT NULL,
	[longitud] [float] NOT NULL,
	[cant_ejes] [int] NOT NULL,
	[fecha_alta] [date] NULL,
	[estado] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[nro_chasis] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auditoria]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auditoria](
	[fecha_hr] [nvarchar](50) NOT NULL,
	[usuario_auditoria] [nvarchar](50) NOT NULL,
	[accion] [nvarchar](200) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[camion]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[camion](
	[dominio] [nvarchar](10) NOT NULL,
	[nro_chasis] [nvarchar](20) NOT NULL,
	[nro_motor] [nvarchar](20) NOT NULL,
	[marca] [int] NOT NULL,
	[modelo] [int] NOT NULL,
	[año] [int] NULL,
	[tara] [float] NULL,
	[km_unidad] [float] NOT NULL,
	[km_service] [float] NOT NULL,
	[km_cambio_neumaticos] [float] NOT NULL,
	[km_service_suma] [float] NOT NULL,
	[km_cambio_neumaticos_suma] [float] NOT NULL,
	[fecha_alta] [datetime] NULL,
	[estado] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[nro_chasis] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[nro_motor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chofer]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chofer](
	[num_legajo] [int] NOT NULL,
	[nombre] [nvarchar](20) NOT NULL,
	[apellido] [nvarchar](20) NOT NULL,
	[num_cedula] [nvarchar](20) NOT NULL,
	[direccion] [nvarchar](20) NOT NULL,
	[tel_personal] [nvarchar](30) NOT NULL,
	[tel_trabajo] [nvarchar](30) NULL,
	[estado] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[num_cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dominio]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dominio](
	[dominio] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_dominio] PRIMARY KEY CLUSTERED 
(
	[dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[marca_acoplado]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[marca_acoplado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[marca] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
 CONSTRAINT [PK_marca_acoplado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_marca_acoplado] UNIQUE NONCLUSTERED 
(
	[marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[marca_camion]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[marca_camion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[marca] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
 CONSTRAINT [PK_marca_camion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_marca_camion] UNIQUE NONCLUSTERED 
(
	[marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mecanico]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mecanico](
	[num_legajo] [int] NOT NULL,
	[nombre] [nvarchar](20) NOT NULL,
	[apellido] [nvarchar](20) NOT NULL,
	[direccion] [nvarchar](30) NOT NULL,
	[telefono1] [nvarchar](20) NOT NULL,
	[telefono2] [nvarchar](20) NULL,
	[estado] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modelo_acoplado]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modelo_acoplado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modelo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
	[id_marca] [int] NOT NULL,
 CONSTRAINT [PK_modelo_acoplado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_modelo_acoplado] UNIQUE NONCLUSTERED 
(
	[modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modelo_camion]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modelo_camion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modelo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
	[id_marca] [int] NOT NULL,
 CONSTRAINT [PK_modelo_camion] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_modelo_camion] UNIQUE NONCLUSTERED 
(
	[modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[numero_chasis]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numero_chasis](
	[num_chasis] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_numero_chasis] PRIMARY KEY CLUSTERED 
(
	[num_chasis] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pedido]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pedido](
	[id_pedido] [int] IDENTITY(1,1) NOT NULL,
	[id_chofer] [int] NOT NULL,
	[fecha_pedido] [datetime] NOT NULL,
	[fecha_salida] [datetime] NOT NULL,
	[fecha_regreso] [datetime] NOT NULL,
	[nombre_cliente] [varchar](50) NOT NULL,
	[cuil_cliente] [varchar](15) NOT NULL,
	[peso_neto] [float] NOT NULL,
	[volumen] [float] NOT NULL,
	[km_viaje] [float] NOT NULL,
	[enganche] [nvarchar](35) NULL,
	[tipo_carga_camion] [nvarchar](20) NULL,
	[altura_camion] [float] NULL,
	[ancho_interior_camion] [float] NULL,
	[ancho_exterior_camion] [float] NULL,
	[longitud_camion] [float] NULL,
	[cantidad_ejes_camion] [int] NULL,
	[tipo_carga_acoplado] [nvarchar](20) NULL,
	[altura_acoplado] [float] NULL,
	[ancho_interior_acoplado] [float] NULL,
	[ancho_exterior_acoplado] [float] NULL,
	[longitud_acoplado] [float] NULL,
	[cantidad_ejes_acoplado] [int] NULL,
	[observaciones] [text] NULL,
	[prioridad] [nchar](20) NOT NULL,
	[estado] [nchar](20) NOT NULL,
 CONSTRAINT [PK_pedido] PRIMARY KEY CLUSTERED 
(
	[id_pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rigido]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rigido](
	[dominio] [nvarchar](10) NOT NULL,
	[tipo_carga] [int] NOT NULL,
	[altura] [float] NOT NULL,
	[ancho_interior] [float] NOT NULL,
	[ancho_exterior] [float] NOT NULL,
	[longitud] [float] NOT NULL,
	[volumen] [float] NOT NULL,
	[capacidad_carga] [float] NOT NULL,
	[enganche] [bit] NOT NULL,
 CONSTRAINT [ak_camion_rigido] UNIQUE NONCLUSTERED 
(
	[dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rol] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_roles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_rol] UNIQUE NONCLUSTERED 
(
	[rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[taller]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taller](
	[id_taller] [int] IDENTITY(1,1) NOT NULL,
	[num_legajo] [int] NOT NULL,
	[entrada_taller] [datetime] NOT NULL,
	[salida_aprox] [datetime] NOT NULL,
	[salida] [datetime] NULL,
	[observaciones] [text] NULL,
	[id_unidad] [nvarchar](10) NULL,
	[estado] [nvarchar](20) NOT NULL,
 CONSTRAINT [PK_taller] PRIMARY KEY CLUSTERED 
(
	[id_taller] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipo_carga_acoplado]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipo_carga_acoplado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
 CONSTRAINT [PK_tipo_carga_acoplado] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_tipo_carga_acoplado] UNIQUE NONCLUSTERED 
(
	[tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipo_carga_rigido]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipo_carga_rigido](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
 CONSTRAINT [PK_tipo_carga_rigido] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_tipo_carga_rigido] UNIQUE NONCLUSTERED 
(
	[tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tractocamion]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tractocamion](
	[dominio] [nvarchar](10) NOT NULL,
 CONSTRAINT [ak_camion_tractocamion] UNIQUE NONCLUSTERED 
(
	[dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[usuario] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[num_legajo] [int] NOT NULL,
	[id_rol] [int] NOT NULL,
	[estado] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_login] PRIMARY KEY CLUSTERED 
(
	[usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[unidades_viaje]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[unidades_viaje](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_camion] [nvarchar](10) NOT NULL,
	[id_acoplado] [nvarchar](10) NULL,
-- CONSTRAINT [ak_acoplado] UNIQUE NONCLUSTERED 
--(
--	[id_acoplado] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
-- CONSTRAINT [ak_camion] UNIQUE NONCLUSTERED 
--(
--	[id_camion] ASC
--)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [ak_viaje] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
/****** Object:  Table [dbo].[viaje]    Script Date: 18/4/2018 22:28:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[viaje](
	[id_viaje] [int] IDENTITY(1,1) NOT NULL,
	[id_pedido] [int] NOT NULL,
	[id_unidades_viaje] [int] NOT NULL,
	[fecha_salida] [datetime] NULL,
	[fecha_regreso] [datetime] NULL,
	[estado] [nvarchar](20) NULL,
	[observaciones] [text] NULL,
 CONSTRAINT [PK_viaje] PRIMARY KEY CLUSTERED 
(
	[id_viaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO


ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [fk_marca_acoplado] FOREIGN KEY([marca])
REFERENCES [dbo].[marca_acoplado] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [fk_marca_acoplado]
GO
ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [fk_modelo_acoplado] FOREIGN KEY([modelo])
REFERENCES [dbo].[modelo_acoplado] ([id])
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [fk_modelo_acoplado]
GO
ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [fk_tipo_carga_acoplado] FOREIGN KEY([tipo_carga])
REFERENCES [dbo].[tipo_carga_acoplado] ([id])
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [fk_tipo_carga_acoplado]
GO
ALTER TABLE [dbo].[auditoria]  WITH CHECK ADD  CONSTRAINT [fk_usuario] FOREIGN KEY([usuario_auditoria])
REFERENCES [dbo].[usuarios] ([usuario])
GO
ALTER TABLE [dbo].[auditoria] CHECK CONSTRAINT [fk_usuario]
GO
ALTER TABLE [dbo].[camion]  WITH CHECK ADD  CONSTRAINT [fk_marca_camion] FOREIGN KEY([marca])
REFERENCES [dbo].[marca_camion] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[camion] CHECK CONSTRAINT [fk_marca_camion]
GO
ALTER TABLE [dbo].[modelo_camion]  WITH CHECK ADD  CONSTRAINT [fk_modelo_marca_camion] FOREIGN KEY([id_marca])
REFERENCES [dbo].[marca_camion] ([id])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[modelo_camion] CHECK CONSTRAINT [fk_modelo_marca_camion]
GO
ALTER TABLE [dbo].[pedido]  WITH CHECK ADD  CONSTRAINT [FK_Id_Chofer] FOREIGN KEY([id_chofer])
REFERENCES [dbo].[chofer] ([num_legajo])
GO
ALTER TABLE [dbo].[pedido] CHECK CONSTRAINT [FK_Id_Chofer]
GO
ALTER TABLE [dbo].[rigido]  WITH CHECK ADD  CONSTRAINT [fk_camion_rigido] FOREIGN KEY([dominio])
REFERENCES [dbo].[camion] ([dominio])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[rigido] CHECK CONSTRAINT [fk_camion_rigido]
GO
ALTER TABLE [dbo].[rigido]  WITH CHECK ADD  CONSTRAINT [fk_tipo_carga_rigido] FOREIGN KEY([tipo_carga])
REFERENCES [dbo].[tipo_carga_rigido] ([id])
GO
ALTER TABLE [dbo].[rigido] CHECK CONSTRAINT [fk_tipo_carga_rigido]
GO
ALTER TABLE [dbo].[taller]  WITH CHECK ADD  CONSTRAINT [FK_id_unidad_taller] FOREIGN KEY([id_unidad])
REFERENCES [dbo].[dominio] ([dominio])
GO
ALTER TABLE [dbo].[taller] CHECK CONSTRAINT [FK_id_unidad_taller]
GO
ALTER TABLE [dbo].[tractocamion]  WITH CHECK ADD  CONSTRAINT [fk_camion_tractocamion] FOREIGN KEY([dominio])
REFERENCES [dbo].[camion] ([dominio])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[tractocamion] CHECK CONSTRAINT [fk_camion_tractocamion]
GO
--ALTER TABLE [dbo].[unidades_viaje]  WITH CHECK ADD  CONSTRAINT [fk_acoplado] FOREIGN KEY([id_acoplado])
--REFERENCES [dbo].[acoplado] ([dominio])
--GO
--ALTER TABLE [dbo].[unidades_viaje] CHECK CONSTRAINT [fk_acoplado]
--GO
--ALTER TABLE [dbo].[unidades_viaje]  WITH CHECK ADD  CONSTRAINT [fk_camion] FOREIGN KEY([id_camion])
--REFERENCES [dbo].[camion] ([dominio])
--ON UPDATE CASCADE
--GO
--ALTER TABLE [dbo].[unidades_viaje] CHECK CONSTRAINT [fk_camion]
GO
ALTER TABLE [dbo].[usuarios]  WITH CHECK ADD  CONSTRAINT [FK_roles] FOREIGN KEY([id_rol])
REFERENCES [dbo].[roles] ([id])
GO
ALTER TABLE [dbo].[usuarios] CHECK CONSTRAINT [FK_roles]
GO
ALTER TABLE [dbo].[viaje]  WITH CHECK ADD  CONSTRAINT [FK_unidades_viaje] FOREIGN KEY([id_unidades_viaje])
REFERENCES [dbo].[unidades_viaje] ([id])
GO
ALTER TABLE [dbo].[viaje] CHECK CONSTRAINT [FK_unidades_viaje]
GO
ALTER TABLE [dbo].[viaje]  WITH CHECK ADD  CONSTRAINT [FK_viaje_pedido] FOREIGN KEY([id_pedido])
REFERENCES [dbo].[pedido] ([id_pedido])
GO
ALTER TABLE [dbo].[viaje] CHECK CONSTRAINT [FK_viaje_pedido]
GO
USE [master]
GO
ALTER DATABASE [logistica] SET  READ_WRITE 
GO
