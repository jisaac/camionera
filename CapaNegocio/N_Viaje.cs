﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using System.Data;
using CapaEntidades;
//listar por fecha de Regreso viaje es lo mismo que concluido, es decir, si buscas un viaje por su fecha de Regreso (viaje),estarias buscando la fecha de cierre del viaje
public enum Estado
{
    Todos,Pendiente,EnCurso,Concluido,Suspendido,Cancelado
}
namespace CapaNegocio
{
    public class N_Viaje
    {
        public N_Viaje(string usuario)
        {
            this.usuario = usuario;
            d_viaje = new D_Viaje(usuario);
            n_Camion = new N_Camion(usuario);
            n_acoplado = new N_Acoplado(usuario);
            n_taller = new N_Taller(usuario);
            n_pedido = new N_Pedido(usuario);
            n_dominio =new  N_Dominio(usuario);
        }
        string usuario;
        D_Viaje d_viaje;
        N_Dominio n_dominio;
        DataTable dtviaje = new DataTable();
        E_UnidadesViaje e_unidadesViaje;
        E_Rigido e_Rigido;
        E_Tractocamion e_tractocamion;
        E_Acoplado e_acoplado;
        N_Acoplado n_acoplado;
        N_Camion n_Camion;
        E_Taller e_TallerCamion;
        E_Taller e_TallerAcoplado;
        N_Taller n_taller;
        N_Pedido n_pedido;
        E_Pedido e_pedido;
        List<string> respuestas;


        public List<string> insertar(E_Viaje e_viaje, E_UnidadesViaje e_unidadesViaje)
        {
            List<string> lista = new List<string>();
            d_viaje.insertar(e_viaje, e_unidadesViaje);
            lista.Add("Exito");
            lista.Add("Viaje insertado con exito");
            return lista;
        }
        public List<string> modificarViaje(E_Viaje viaje)
        {
            List<string> lista = new List<string>();
            int resp = d_viaje.modificarViaje(viaje);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Viaje modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> modificarUnidadesViaje(E_Viaje viaje, E_UnidadesViaje e_unidadesViaje)
        {
            List<string> lista = new List<string>();
            int resp = d_viaje.modificarUnidades(viaje, e_unidadesViaje);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Viaje deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;

        }
        public E_Viaje retornaViaje(string id_viaje)
        {
            return d_viaje.retornaViaje(id_viaje);

        }
        public E_UnidadesViaje retornaUnidadesViaje(E_Viaje e_viaje)
        {
            return d_viaje.retornaUnidadesViaje(e_viaje);
        }

        public DataTable listarEstado(Estado e)
        {
            switch (e)
            {
                case Estado.Todos:
                    dtviaje= d_viaje.listar_por_Estado("%_%");
                    break;
                case Estado.Concluido:
                    dtviaje= d_viaje.listar_por_Estado("CONCLUIDO");
                    break;
                case Estado.EnCurso:
                    dtviaje= d_viaje.listar_por_Estado("EN CURSO");
                    break;
                case Estado.Pendiente:
                    dtviaje= d_viaje.listar_por_Estado("PENDIENTE");
                    break;
                case Estado.Suspendido:
                    dtviaje= d_viaje.listar_por_Estado("SUSPENDIDO");
                    break;
                case Estado.Cancelado:
                    dtviaje= d_viaje.listar_por_Estado("CANCELADO");
                    break;
            }
            return dtviaje;
          
        }
        public DataTable listarPorUnidadesViaje(string dominio)
        {

            dtviaje = d_viaje.listar_Unidades_viaje(dominio);
   
            return dtviaje;
        }
        public DataTable listar_Fecha_salida_pedido(string desde, string hasta,Estado e)
        {
         switch(e)
            {
                case Estado.Todos:
                    dtviaje= d_viaje.listar_Fecha_salida_pedido(desde,hasta,"%_%");
                    break;
                case Estado.Concluido:
                    dtviaje = d_viaje.listar_Fecha_salida_pedido(desde, hasta, "CONCLUIDO");
                    break;
                case Estado.EnCurso:
                    dtviaje = d_viaje.listar_Fecha_salida_pedido(desde, hasta, "EN CURSO");
                    break;
                case Estado.Pendiente:
                    dtviaje = d_viaje.listar_Fecha_salida_pedido(desde, hasta, "PENDIENTE");
                    break;
                case Estado.Suspendido:
                    dtviaje = d_viaje.listar_Fecha_salida_pedido(desde, hasta, "SUSPENDIDO");
                    break;
                case Estado.Cancelado:
                    dtviaje = d_viaje.listar_Fecha_salida_pedido(desde, hasta, "CANCELADO");
                    break;
            }
            return dtviaje;
        }
        public DataTable listar_Fecha_Regreso_pedido(string desde, string hasta, Estado e)
        {
            switch (e)
            {
                case Estado.Todos:
                    dtviaje = d_viaje.listar_Fecha_Regreso_pedido(desde, hasta, "%_%");
                    break;
                case Estado.Concluido:
                    dtviaje = d_viaje.listar_Fecha_Regreso_pedido(desde, hasta, "CONCLUIDO");
                    break;
                case Estado.EnCurso:
                    dtviaje = d_viaje.listar_Fecha_Regreso_pedido(desde, hasta, "EN CURSO");
                    break;
                case Estado.Pendiente:
                    dtviaje = d_viaje.listar_Fecha_Regreso_pedido(desde, hasta, "PENDIENTE");
                    break;
                case Estado.Suspendido:
                    dtviaje = d_viaje.listar_Fecha_Regreso_pedido(desde, hasta, "SUSPENDIDO");
                    break;
                case Estado.Cancelado:
                    dtviaje = d_viaje.listar_Fecha_Regreso_pedido(desde, hasta, "CANCELADO");
                    break;
            }
            return dtviaje;
        }
        public DataTable listar_Fecha_Regreso_viaje(string desde, string hasta, Estado e)
        {
            
            switch (e)
            {
                case Estado.Todos:
                    dtviaje = d_viaje.listar_Fecha_Regreso_viaje(desde, hasta, "%_%");
                    break;
                case Estado.Concluido:
                    dtviaje = d_viaje.listar_Fecha_Regreso_viaje(desde, hasta, "CONCLUIDO");
                    break;
                case Estado.EnCurso:
                    dtviaje = d_viaje.listar_Fecha_Regreso_viaje(desde, hasta, "EN CURSO");
                    break;
                case Estado.Pendiente:
                    dtviaje = d_viaje.listar_Fecha_Regreso_viaje(desde, hasta, "PENDIENTE");
                    break;
                case Estado.Suspendido:
                    dtviaje = d_viaje.listar_Fecha_Regreso_viaje(desde, hasta, "SUSPENDIDO");
                    break;
                case Estado.Cancelado:
                    dtviaje = d_viaje.listar_Fecha_Regreso_viaje(desde, hasta, "CANCELADO");
                    break;
            }
            return dtviaje;
        }
        public DataTable listar_Fecha_salida_viaje(string desde, string hasta, Estado e)
        {
            switch (e)
            {
                case Estado.Todos:
                    dtviaje = d_viaje.listar_Fecha_salida_viaje(desde, hasta, "%_%");
                    break;
                case Estado.Concluido:
                    dtviaje = d_viaje.listar_Fecha_salida_viaje(desde, hasta, "CONCLUIDO");
                    break;
                case Estado.EnCurso:
                    dtviaje = d_viaje.listar_Fecha_salida_viaje(desde, hasta, "EN CURSO");
                    break;
                case Estado.Pendiente:
                    dtviaje = d_viaje.listar_Fecha_salida_viaje(desde, hasta, "PENDIENTE");
                    break;
                case Estado.Suspendido:
                    dtviaje = d_viaje.listar_Fecha_salida_viaje(desde, hasta, "SUSPENDIDO");
                    break;
                case Estado.Cancelado:
                    dtviaje = d_viaje.listar_Fecha_salida_viaje(desde, hasta, "CANCELADO");
                    break;
            }
            return dtviaje;
        }
        public DataTable listar_porEntradaDeTexto(string texto,Estado e)
        {
            switch (e)
            {
                case Estado.Todos:
                    dtviaje = d_viaje.listar_Texto(texto, "%_%");
                    break;
                case Estado.Concluido:
                    dtviaje = d_viaje.listar_Texto(texto, "CONCLUIDO");
                    break;
                case Estado.EnCurso:
                    dtviaje = d_viaje.listar_Texto(texto, "EN CURSO");
                    break;
                case Estado.Pendiente:
                    dtviaje = d_viaje.listar_Texto(texto, "PENDIENTE");
                    break;
                case Estado.Suspendido:
                    dtviaje = d_viaje.listar_Texto(texto, "SUSPENDIDO");
                    break;
                case Estado.Cancelado:
                    dtviaje = d_viaje.listar_Texto(texto, "CANCELADO");
                    break;
            }
            return dtviaje;
        }
  
        public List<string> cambiarEstado(ref E_Viaje e_viaje, Estado e,float kmActual)//recordar que kmactual sea mayor a km unidad !!!!!!  ref paso de parametro por referencia /////////////////////////////////////
        {
            this.e_TallerAcoplado = null;
            this.e_TallerCamion = null;
            int resp;
            List<string> lista = new List<string>();

            float kmDelviaje = 0; //km con los que concluyo el viaje
            void Calculo(object u)
            {// hace el calculo de km service y uso neumaticos para acoplados y camiones
                if(u is E_Camion)
                {
                    //seteamos LOS KILOMETROS DEL VIAJE QUE VA A SERVIR PARA MODIFICAR EL KM ACTUAL DE ACOPLADO
                    kmDelviaje = (kmActual - ((E_Camion)u).Km_unidad);
                    //a km_service suma, que en realidad es una resta, se le restan los km que se hicieron ne el viaje (km viaje - km unidad)
                    ((E_Camion)u).Km_service_suma = ((E_Camion)u).Km_service_suma    -     kmDelviaje;
                    //a km_neumaticos suma, que en realidad es una resta, se le restan los km que se hicieron ne el viaje (km viaje - km unidad)
                    ((E_Camion)u).Km_cambio_neumaticos_suma = ((E_Camion)u).Km_cambio_neumaticos_suma - kmDelviaje;
                    //se setea el valor de km unidad
                    ((E_Camion)u).Km_unidad = ((E_Camion)u).Km_unidad + kmDelviaje;
                    
                    //DESDE AQUI EMPEZAMOS A HACER EL TRABAJO PARA QUE,SI CORRESPONDE,A LA UNIDAD LA MANDAMOS A TALLER
                    if (((E_Camion)u).Km_service_suma <= 0 || ((E_Camion)u).Km_cambio_neumaticos_suma <= 0)
                    {
                        this.e_TallerCamion = new E_Taller();
                        if (((E_Camion)u).Km_service_suma <= 0)
                        {
                            this.e_TallerCamion.Tipo = "SERVICE+";
                        
                        }
                        if(((E_Camion)u).Km_cambio_neumaticos_suma <= 0)
                        {
                            this.e_TallerCamion.Tipo = this.e_TallerCamion.Tipo +"NEUMATICOS+";
                        }
                        this.e_TallerCamion.id_dominio = ((E_Camion)u).Id_Dominio;

                        n_taller.insertar(this.e_TallerCamion);
                    }

                }
                else
                {
                    ((E_Acoplado)u).Km_service_suma = ((E_Acoplado)u).Km_service_suma - kmDelviaje;
                    ((E_Acoplado)u).Km_cambio_neumaticos_suma = ((E_Acoplado)u).Km_cambio_neumaticos_suma - kmDelviaje;
                    ((E_Acoplado)u).Km_unidad = ((E_Acoplado)u).Km_unidad + kmDelviaje;
                    if (((E_Acoplado)u).Km_service_suma <= 0 || ((E_Acoplado)u).Km_cambio_neumaticos_suma<=0)
                    {
                        this.e_TallerAcoplado = new E_Taller();
                        if (((E_Acoplado)u).Km_service_suma <= 0)
                        {
                            this.e_TallerAcoplado.Tipo = "SERVICE+";
                        }
                        if (((E_Acoplado)u).Km_cambio_neumaticos_suma <= 0)
                        {
                            this.e_TallerAcoplado.Tipo =this.e_TallerAcoplado.Tipo + "NEUMATICOS+";
                        }
                        this.e_TallerAcoplado.id_dominio = ((E_Acoplado)u).Id_Dominio;
                        n_taller.insertar(this.e_TallerAcoplado) ;

                    }
                }
            }
            string respuesta=string.Empty;
            switch (e)
            {
              
                case Estado.Concluido:// /////////////////////////// CONCLUIR VIAJE
                    if (e_viaje.Estado.Equals("EN CURSO"))
                    {
                        //Recuperamos las unidades asociadas al viaje
                        e_unidadesViaje = d_viaje.retornaUnidadesViaje(e_viaje);
                        Object tipoCamion = n_Camion.retornaCamion(e_unidadesViaje.Id_camion);
                        if (((E_Camion)tipoCamion).Km_unidad > kmActual)
                        {
                            lista.Add("Error");
                            lista.Add("El kilometraje indicado de la unidad al finalizar el viaje, es menor que el kilometraje actual de la misma.");
                        }
                        else
                        {
                            Calculo(tipoCamion);
                            //modifico la unidad
                            n_Camion.modificar(tipoCamion, n_dominio.Retorna_Dominio(((E_Camion)tipoCamion).Id_Dominio), "Seteo de Kilometraje");
                            if (e_TallerCamion != null)
                            {
                                respuesta = "CAMION";
                            }
                            //modifico acoplado, si este existe
                            if (!e_unidadesViaje.Id_acoplado.Equals(-1) )
                            {
                                e_acoplado = n_acoplado.retornaAcoplado(e_unidadesViaje.Id_acoplado);
                                Calculo(e_acoplado);//uso calculo para setear los km de acoplado
                                n_acoplado.modificar(e_acoplado, n_dominio.Retorna_Dominio(e_acoplado.Id_Dominio), "Seteo de kilometraje");
                                if (e_TallerAcoplado != null)
                                {
                                    respuesta = respuesta + "+ACOPLADO";
                                    
                                }

                            }
                            e_viaje.Estado = "CONCLUIDO";
                            e_viaje.Km_cierre = e_viaje.Km_cierre + kmDelviaje;
                            int aux = d_viaje.modificarEstado(e_viaje);

                            if (aux>0)
                            {
                                respuesta =respuesta+ "+Viaje concluido";
                                lista.Add("Informacion");
                                lista.Add(respuesta);
                            }
                            
                        }
                    }
                    break;
                case Estado.EnCurso:// ////////////////////////////////////// EN CURSO...SI LA RESPUESTA EN EL IF ES IGUAL A NULL, ENTONCES SE PUEDE PROSEGUIR, CASO CONTRARIO, POR ALGO NO SE PUEDE USAR LA UNIDAD
                                                                              // POR LO QUE DEBO MOSTRAR EL MENSAJE DEVUELTO POR VERIFICA UNIDAD TALLER
                    if (e_viaje.Estado.Equals("PENDIENTE"))
                    {
                        e_unidadesViaje = d_viaje.retornaUnidadesViaje(e_viaje);
                        respuesta = n_taller.Verifica_si_unidad_puede_ingresar_a_talle(n_dominio.Retorna_Dominio(e_unidadesViaje.Id_camion));
                        if (respuesta.Equals("Este remolque debe ser cargado a mantenimiento por el encargado de logistica") || respuesta.Equals("Este camion debe ser cargado a mantenimiento por el encargado de logistica"))
                        {
                            e_viaje.Estado = "EN CURSO";
                            resp = d_viaje.modificarEstado(e_viaje);
                            if (resp > 0)
                            {
                                lista.Add("Exito");
                                lista.Add("Viaje modificado con exito");
                            }
                            else
                            {
                                lista.Add("Error");
                                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
                            }
                            return lista;
                        }
                        
                        
                    }
                    
                    break;
                case Estado.Pendiente://  //////////////////////////////////////// PENDIENTE
                    if (e_viaje.Estado.Equals("EN CURSO"))
                    {// Regla : si el viaje estaba en curso y por motivo alguno se pasa pendiente, se debe setear km de unidad en viaje
                        e_unidadesViaje = d_viaje.retornaUnidadesViaje(e_viaje);
                        Object tipoCamion = n_Camion.retornaCamion(e_unidadesViaje.Id_camion);
                        if (((E_Camion)tipoCamion).Km_unidad > kmActual)
                        {
                            respuesta = "El kilometraje indicado de la unidad al finalizar el viaje, es menor que el kilometraje actual de la misma.";
                        }
                        else
                        {
                            Calculo(tipoCamion);
                            //modifico la unidad
                             n_Camion.modificar(tipoCamion, n_dominio.Retorna_Dominio(((E_Camion)tipoCamion).Id_Dominio), "Seteo kilometaje");
                            //modifico acoplado, si este existe
                            if (e_unidadesViaje.Id_acoplado>0)
                            {
                                e_acoplado = n_acoplado.retornaAcoplado(e_unidadesViaje.Id_acoplado);
                                Calculo(e_acoplado);//uso calculo para setear los km de acoplado
                                n_acoplado.modificar(e_acoplado, n_dominio.Retorna_Dominio(e_acoplado.Id_Dominio), "Seteo de kilometrajes");

                            }
                            e_viaje.Estado = "PENDIENTE";
                            d_viaje.modificarEstado(e_viaje);
                            lista.Clear();
                            lista.Add("Exito");
                            lista.Add( "Viaje modificado con exito");

                        }
                    }else if(e_viaje.Estado.Equals("SUSPENDIDO"))
                    {
                        e_viaje.Estado = "PENDIENTE";
                        d_viaje.modificarEstado(e_viaje);
                        lista.Clear();
                        lista.Add("Exito");
                        lista.Add("Viaje modificado con exito");
                    }
                    break;
                case Estado.Suspendido:
                    if (e_viaje.Estado.Equals("PENDIENTE"))
                    {
                        e_viaje.Estado = "SUSPENDIDO";
                        resp = d_viaje.modificarEstado(e_viaje);
                        if (resp > 0)
                        {
                            lista.Add("Exito");
                            lista.Add("Viaje modificado con exito");
                        }
                        else
                        {
                            lista.Add("Error");
                            lista.Add("No se pudo realizar la acción, 0 filas afectadas");
                        }
                        return lista;
                    }
                    break;
                case Estado.Cancelado:
                    if (e_viaje.Estado.Equals("PENDIENTE") || e_viaje.Estado.Equals("SUSPENDIDO"))
                    {
                        e_viaje.Estado = "CANCELADO";
                        resp = d_viaje.modificarEstado(e_viaje);
                        if (resp > 0)
                        {
                            lista.Add("Exito");
                            lista.Add("Viaje modificado con exito");
                        }
                        else
                        {
                            lista.Add("Error");
                            lista.Add("No se pudo realizar la acción, 0 filas afectadas");
                        }
                        return lista;
                    }
                    break;
            }

            return lista;
         
        }
    }

}
