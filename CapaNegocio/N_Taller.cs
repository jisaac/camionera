﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;
//
//en n taller, segun el "tipo", recordar setear el estado de km neumaticos o service...usamos d unidades
//Estados en taller POR_REVISAR,EN_PROCESO,ENTREGADO
//
//
public enum EstadoTaller
{
    EnProceso, PorRevisar, Concluido, Todos
}
namespace CapaNegocio
{
    public class N_Taller
    {
        D_Camion d_camion;
        D_Acoplado d_acoplado;
        D_Taller d_taller;
        E_Taller e_taller;
        DataTable dtTaller;
        string usuario;
        N_Dominio n_dominio;
        List<string> respList = new List<string>();


        public N_Taller(string usuario)
        {
            this.usuario = usuario;
            d_taller = new D_Taller(usuario);
            e_taller = new E_Taller();
            n_dominio = new N_Dominio(usuario);

        }
        public List<string> insertarSinSaberId_Dominio(E_Taller e_taller, string dominio)
        {
            int id_dominio = n_dominio.Retorna_Id_Dominio(dominio);
            if (id_dominio != -1)
            {
                e_taller.id_dominio = id_dominio;
                e_taller.Entrada_taller = Convert.ToDateTime(DateTime.Now.ToShortDateString());
                d_taller.insertar(e_taller);
                respList.Add("Exito");
                respList.Add("Unidad ingresó a taller");
                return respList;

            }
            else
            {
                respList.Add("Error");
                respList.Add("Ups, no pudimos ingresar la unidad a taller");
                return respList;
            }



        }
        public List<string> insertar(E_Taller e_taller)
        {
            e_taller.Entrada_taller = Convert.ToDateTime(DateTime.Now.ToShortDateString());
            d_taller.insertar(e_taller);
            respList.Add("Exito");
            respList.Add("Unidad ingresó a taller");
            return respList;
        }


        //Con la siguiente funcion si el estado es en PROCESO y no se quiere modificar el estado solo se modifican algunos atributos de de taller
        // y en el caso de CONCLUIDO, si este se quiere modificar, solo se modifican las observaciones.(recordar que modificar a  estado CONCLUIDO, trabaja con acoplado y/o camion 
        //modificando datos de estos haciendo uso de d_camion y/d_acoplado)
        public List<string> modificar(E_Taller e_taller, EstadoTaller estado, bool service, bool neumaticos, bool otros)//toda unidad pasa Por revisar
        {
            List<string> lista = new List<string>();
            int resp;
            switch (estado)
            {

                case EstadoTaller.EnProceso:
                    if (e_taller.Estado.Equals("POR_REVISAR"))
                    {

                        if (service)
                        {

                            e_taller.Tipo = "SERVICE";
                        }
                        if (neumaticos)
                        {

                            e_taller.Tipo = e_taller.Tipo + "+NEUMATICOS";
                        }
                        if (otros)
                        {
                            e_taller.Tipo = e_taller.Tipo + "+OTROS";
                        }
                        e_taller.Estado = "EN_PROCESO";

                        resp = d_taller.modificar(e_taller);
                        if (resp > 0)
                        {
                            lista.Clear();
                            lista.Add("Exito");
                            lista.Add("Unidad asignada ");
                        }
                        else
                        {
                            lista.Add("Error");
                            lista.Add("El estado debe estar en 'POR REVISAR'");
                        }
                    }
                    else if (e_taller.Estado.Equals("EN_PROCESO"))//no se modifica el estado
                    {
                        if (service)
                        {

                            e_taller.Tipo = "SERVICE";
                        }
                        if (neumaticos)
                        {

                            e_taller.Tipo = e_taller.Tipo + "+NEUMATICOS";
                        }
                        if (otros)
                        {
                            e_taller.Tipo = e_taller.Tipo + "+OTROS";
                        }

                        resp = d_taller.modificar(e_taller);
                        if (resp > 0)
                        {
                            lista.Clear();
                            lista.Add("Exito");
                            lista.Add("Unidad asignada ");
                        }
                        else
                        {
                            lista.Add("Error");
                            lista.Add("El estado debe estar en 'POR REVISAR'");
                        }
                    }
                    else
                    {
                        lista.Add("Error");
                        lista.Add("El estado debe estar en 'POR REVISAR'");
                    }
                    break;
                case EstadoTaller.Concluido:
                    if (e_taller.Estado.Equals("EN_PROCESO"))
                    {

                        E_Camion e_camion = (E_Camion)new D_Camion(usuario).RetornaCamion(e_taller.id_dominio);
                        if (e_camion != null)
                        {

                            if (service)
                            {
                                e_camion.Km_service_suma = e_camion.Km_service;
                                e_taller.Tipo = "SERVICE";
                            }
                            if (neumaticos)
                            {
                                e_camion.Km_cambio_neumaticos_suma = e_camion.Km_cambio_neumaticos;
                                e_taller.Tipo = e_taller.Tipo + "+NEUMATICOS";
                            }
                            if (otros)
                            {
                                e_taller.Tipo = e_taller.Tipo + "+OTROS";
                            }
                            new D_Camion(usuario).modificar(e_camion, n_dominio.Retorna_Dominio(e_camion.Id_Dominio), "Mantenimiento: " + e_taller.Tipo);
                            e_taller.Salida = DateTime.Now;
                            e_taller.Estado = "CONCLUIDO";
                            resp = d_taller.modificar(e_taller);
                            if (resp > 0)
                            {
                                lista.Clear();
                                lista.Add("Exito");
                                lista.Add("Dado de alta! \n Camion liberado con exito! ");
                            }
                        }
                        else
                        {
                            E_Acoplado e_acoplado = new D_Acoplado(usuario).retornaAcoplado(e_taller.id_dominio);

                            if (service)
                            {
                                e_acoplado.Km_service_suma = e_acoplado.Km_service;
                                e_taller.Tipo = "SERVICE";
                            }
                            if (neumaticos)
                            {
                                e_acoplado.Km_cambio_neumaticos_suma = e_acoplado.Km_cambio_neumaticos;
                                e_taller.Tipo = e_taller.Tipo + "+NEUMATICOS";
                            }
                            if (otros)
                            {
                                e_taller.Tipo = e_taller.Tipo + "+OTROS";
                            }
                            new D_Acoplado(usuario).modificar(e_acoplado, n_dominio.Retorna_Dominio(e_acoplado.Id_Dominio), "Mantenimiento: " + e_taller.Tipo);
                            e_taller.Salida = DateTime.Now;
                            e_taller.Estado = "CONCLUIDO";
                            resp = d_taller.modificar(e_taller);
                            if (resp > 0)
                            {
                                lista.Clear();
                                lista.Add("Exito");
                                lista.Add(" Dado de alta!  \n Remolque liberado con exito!  ");
                            }
                        }

                    }
                    else if (e_taller.Estado.Equals("CONCLUIDO"))// solo se modifican las observaciones
                    {

                        resp = d_taller.modificar(e_taller);
                        if (resp > 0)
                        {
                            lista.Clear();
                            lista.Add("Exito");
                            lista.Add("Ticket Editado ! ");
                        }
                    }
                    else
                    {
                        lista.Add("Error");
                        lista.Add("El estado debe estar en 'EN PROCESO'");
                    }
                    break;

            }
            return lista;
        }
        public E_Taller retornaTaller(string id)
        {
            E_Taller e_taller = d_taller.retornaTaller(id);
            if (e_taller != null)
                return e_taller;
            return null;
        }
        public DataTable Listar_por_Estado_Camion(EstadoTaller estado)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Estado_Camion("%_%");
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Estado_Camion("POR_REVISAR");
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Estado_Camion("EN_PROCESO");
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Estado_Camion("CONCLUIDO");
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Texto_Camion(EstadoTaller estado, string texto)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Texto_Camion("%_%", texto);
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Texto_Camion("POR_REVISAR", texto);
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Texto_Camion("EN_PROCESO", texto);
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Texto_Camion("CONCLUIDO", texto);
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Fecha_Entrada_Camion(string desde, string hasta, EstadoTaller estado)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Camion(desde, hasta, "%_%");
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Camion(desde, hasta, "POR_REVISAR");
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Camion(desde, hasta, "EN_PROCESO");
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Camion(desde, hasta, "CONCLUIDO");
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Fecha_Salida_Camion(string desde, string hasta, EstadoTaller estado)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Camion(desde, hasta, "%_%");
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Camion(desde, hasta, "POR_REVISAR");
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Camion(desde, hasta, "EN_PROCESO");
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Camion(desde, hasta, "CONCLUIDO");
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Estado_Acoplado(EstadoTaller estado)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Estado_Acoplado("%_%");
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Estado_Acoplado("POR_REVISAR");
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Estado_Acoplado("EN_PROCESO");
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Estado_Acoplado("CONCLUIDO");
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Texto_Acoplado(EstadoTaller estado, string texto)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Texto_Acoplado("%_%", texto);
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Texto_Acoplado("POR_REVISAR", texto);
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Texto_Acoplado("EN_PROCESO", texto);
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Texto_Acoplado("CONCLUIDO", texto);
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Fecha_Entrada_Acoplado(string desde, string hasta, EstadoTaller estado)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Acoplado(desde, hasta, "%_%");
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Acoplado(desde, hasta, "POR_REVISAR");
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Acoplado(desde, hasta, "EN_PROCESO");
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Fecha_Entrada_Acoplado(desde, hasta, "CONCLUIDO");
                    break;
            }

            return dtTaller;
        }
        public DataTable Listar_por_Fecha_Salida_Acoplado(string desde, string hasta, EstadoTaller estado)
        {
            switch (estado)
            {
                case EstadoTaller.Todos:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Acoplado(desde, hasta, "%_%");
                    break;
                case EstadoTaller.PorRevisar:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Acoplado(desde, hasta, "POR_REVISAR");
                    break;
                case EstadoTaller.EnProceso:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Acoplado(desde, hasta, "EN_PROCESO");
                    break;
                case EstadoTaller.Concluido:
                    dtTaller = d_taller.listar_por_Fecha_Salida_Acoplado(desde, hasta, "CONCLUIDO");
                    break;
            }

            return dtTaller;
        }
        public string Verifica_si_unidad_puede_ingresar_a_talle(string dominio)
        {
            return d_taller.verifica_si_unidad_puede_ingresar_a_taller(dominio);
        }
        public E_Taller Calculo(ref object u, float kmActual, ref float kmDelViaje)//solo calcula y setea las propiedades de unidad para ser usado en el fron de cambio de unidades
        {// hace el calculo de km service y uso neumaticos para acoplados y camiones
            this.e_taller = null;
            if (u is E_Camion)
            {
                kmDelViaje = (kmActual - ((E_Camion)u).Km_unidad);
                //a km_service suma, que en realidad es una resta, se le restan los km que se hicieron ne el viaje (km viaje - km unidad)
                ((E_Camion)u).Km_service_suma = ((E_Camion)u).Km_service_suma - kmDelViaje;
                //a km_neumaticos suma, que en realidad es una resta, se le restan los km que se hicieron ne el viaje (km viaje - km unidad)
                ((E_Camion)u).Km_cambio_neumaticos_suma = ((E_Camion)u).Km_cambio_neumaticos_suma - kmDelViaje;
                //se setea el valor de km unidad
                ((E_Camion)u).Km_unidad = ((E_Camion)u).Km_unidad + kmDelViaje;
                //DESDE AQUI EMPEZAMOS A HACER EL TRABAJO PARA QUE,SI CORRESPONDE,A LA UNIDAD LA MANDAMOS A TALLER
                if (((E_Camion)u).Km_service_suma <= 0 || ((E_Camion)u).Km_cambio_neumaticos_suma <= 0)
                {
                    this.e_taller = new E_Taller();
                    if (((E_Camion)u).Km_service_suma <= 0)
                    {
                        this.e_taller.Tipo = "SERVICE+";

                    }
                    if (((E_Camion)u).Km_cambio_neumaticos_suma <= 0)
                    {
                        this.e_taller.Tipo = this.e_taller.Tipo + "NEUMATICOS+";
                    }
                    this.e_taller.id_dominio = ((E_Camion)u).Id_Dominio;

                }

            }
            else
            {
                ((E_Acoplado)u).Km_service_suma = ((E_Acoplado)u).Km_service_suma - kmDelViaje;
                ((E_Acoplado)u).Km_cambio_neumaticos_suma = ((E_Acoplado)u).Km_cambio_neumaticos_suma - kmDelViaje;
                ((E_Acoplado)u).Km_unidad = ((E_Acoplado)u).Km_unidad + kmDelViaje;
                if (((E_Acoplado)u).Km_service_suma <= 0 || ((E_Acoplado)u).Km_cambio_neumaticos_suma <= 0)
                {
                    this.e_taller = new E_Taller();
                    if (((E_Acoplado)u).Km_service_suma <= 0)
                    {
                        this.e_taller.Tipo = "SERVICE";
                    }
                    if (((E_Acoplado)u).Km_cambio_neumaticos_suma <= 0)
                    {
                        this.e_taller.Tipo = this.e_taller.Tipo + "NEUMATICOS";
                    }
                    this.e_taller.id_dominio = ((E_Acoplado)u).Id_Dominio;


                }
            }
            return e_taller;
        }
    }
}
