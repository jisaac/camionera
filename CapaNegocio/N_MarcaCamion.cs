﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_MarcaCamion
    {
        D_MarcaCamion d_marcaCamion;
        E_MarcaCamion e_marcaCamion;
        string usuario;

        public N_MarcaCamion(string usuario)
        {
            this.usuario = usuario;
            d_marcaCamion = new D_MarcaCamion(usuario);
            e_marcaCamion = new E_MarcaCamion();
        }
        public List<string> insertar(E_MarcaCamion e_marcaCamion)
        {
            List<string> lista = new List<string>();
            d_marcaCamion.insertar(e_marcaCamion);
            lista.Add("Exito");
            lista.Add("Marca Acoplado insertada con exito");
            return lista;
        }
        public List<string> modificar(E_MarcaCamion e_marcaCamion)
        {
            List<string> lista = new List<string>();
            int resp = d_marcaCamion.modificar(e_marcaCamion);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Marca Camion modificada con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_MarcaCamion e_marcaCamion)
        {
            List<string> lista = new List<string>();
            int resp = d_marcaCamion.baja(e_marcaCamion);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Marca Camion deshabilitada con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_MarcaCamion retornaMarcaCamion(int id)
        {
            E_MarcaCamion e_marcaCamion = d_marcaCamion.retornaMarcaCamion(id);
            if (e_marcaCamion != null)
                return e_marcaCamion;
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtMarcaCamion = d_marcaCamion.listarTodos();
            if (dtMarcaCamion != null)
                return dtMarcaCamion;
            return null;
        }
        public List<String> listarComboBox()
        {
            List<String> listaCombo = d_marcaCamion.listarComboBox();
            if (listaCombo.Count != 0)
                return listaCombo;
            return null;
        }
        public bool existe(string name)
        {
            return d_marcaCamion.existe(name);
        }
        public string retornaId(string name)
        {
            return d_marcaCamion.retornaId(name);
        }

    }
}
