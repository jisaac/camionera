﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

/// 1-camion insertar deberia recibir solamente un objeto  y no preguntar q  tipo es, ya que, lo hace la capa de datos
namespace CapaNegocio
{
    public class N_Camion
    {
        D_Camion d_camion;
        E_Rigido e_rigido;
        E_Tractocamion e_tractocamion;
        string usuario;

        public N_Camion(string usuario)
        {
            this.usuario = usuario;
            d_camion = new D_Camion(usuario);
            e_rigido = new E_Rigido();
            e_tractocamion = new E_Tractocamion();
        }
        public List<string> insertar(Object camion, string dominio)
        {
            List<string> lista = new List<string>();
            if (camion is E_Rigido)
            {
                e_rigido = (E_Rigido)camion;
                d_camion.insertar(e_rigido, dominio);
                lista.Add("Exito");
                lista.Add("Camion - Rigido insertado con exito");
                return lista;
            }
            else
            {
                e_tractocamion = (E_Tractocamion)camion;
                d_camion.insertar(e_tractocamion, dominio);
                lista.Add("Exito");
                lista.Add("Camion - Rigido insertado con exito");
                return lista;
            }

        }
        public List<string> modificar(Object camion,string dominio, string motivo)
        {
            List<string> lista = new List<string>();
            if (camion is  E_Rigido)
            {
                e_rigido = (E_Rigido)camion;
                int resp = d_camion.modificar(e_rigido,dominio,motivo);
                if (resp > 0)
                {
                    lista.Add("Exito");
                    lista.Add("Camion - Rigido modificado con exito");
                }
                else
                {
                    lista.Add("Error");
                    lista.Add("No se pudo realizar la acción, 0 filas afectadas");
                }
                return lista;
            }
            else
            {
                e_tractocamion = (E_Tractocamion)camion;
                int resp = d_camion.modificar(e_tractocamion, dominio, motivo);
                if (resp > 0)
                {
                    lista.Add("Exito");
                    lista.Add("Camion - Tractocamion modificado con exito");
                }
                else
                {
                    lista.Add("Error");
                    lista.Add("No se pudo realizar la acción, 0 filas afectadas");
                }
                return lista;
            }

        }
        public List<string> baja(int id_dominio)
        {
            List<string> lista = new List<string>();
            int resp = d_camion.baja(id_dominio);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Camion - Tractocamion deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public Object retornaCamion(int id_dominio)
        {
            Object tipoCamion = d_camion.RetornaCamion(id_dominio);
            if (tipoCamion is E_Rigido){
                e_rigido = (E_Rigido)tipoCamion;
                if (e_rigido != null)
                    return e_rigido;
                return null;
            }
            else
            {
                e_tractocamion = (E_Tractocamion)tipoCamion;
                if (e_tractocamion != null)
                    return e_tractocamion;
                return null;
            }

        }
        public DataTable listarTodos()
        {
            DataTable dtCamion = d_camion.listarTodos();
            if (dtCamion != null)
                return dtCamion;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtCamion = d_camion.listarHabilitados();
            if (dtCamion != null)
                return dtCamion;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtCamion = d_camion.listarInhabilitados();
            if (dtCamion != null)
                return dtCamion;
            return null;
        }
        public DataTable listarPorFechaAltaHabilitados(string desde, string hasta)
        {
            DataTable dtCamion = d_camion.listaPorFechaAlta(desde, hasta);
            if (dtCamion != null)
                return dtCamion;
            return null;
        }
        public DataTable listarTractos()
        {
            DataTable dtCamion = d_camion.listarTractos();
            if (dtCamion != null)
            {
                return dtCamion;
            }
            return null;
        }
        public DataTable listarRigidos()
        {
            DataTable dtCamion = d_camion.listarRigidos();
            if (dtCamion != null)
            {
                return dtCamion;
            }
            return null;
        }
        public DataTable busquedaEntradaDeTexto(string palabra)
        {
            DataTable dtCamion = d_camion.busquedaPorEntradaDeTexto(palabra);
            if (dtCamion != null)
            {
                return dtCamion;
            }
            return null;

        }
        public DataTable BusquedaPorRangoDeFechaDeAlta(string desde,string hasta)
        {
            DataTable dtCamion = d_camion.busquedaPorRangoDeFechaDeAlta(desde,hasta);
            if (dtCamion != null)
            {
                return dtCamion;
            }
            return null;

        }

    }
}
