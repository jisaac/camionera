﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_Rol
    {
        D_Rol d_rol;
        E_Rol e_rol;
        string usuario;
        public N_Rol(string usuario)
        {
            this.usuario = usuario;
            d_rol = new D_Rol(usuario);
            e_rol = new E_Rol();
        }
        public List<string> insertar(E_Rol e_rol)
        {
            List<string> lista = new List<string>();
             d_rol.insertar(e_rol);
            lista.Add("Exito");
            lista.Add("Rol insertado con exito");
            return lista;
        }
        public List<string> modificar(E_Rol e_rol,String aux)
        {
            List<string> lista = new List<string>();
            int resp = d_rol.modificar(e_rol,aux);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Rol modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_Rol e_rol)
        {
            List<string> lista = new List<string>();
            int resp = d_rol.baja(e_rol);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Rol deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_Rol retornaRol(string rol)
        {
            E_Rol e_rol = d_rol.retornaRol(rol);
            if (e_rol != null)
                return e_rol;
            return null;
        }
        public DataTable listar_roles()
        {
            DataTable dtRol = d_rol.listar_roles();
            if (dtRol != null)
                return dtRol;
            return null;
        }
        public DataView buscar_rol(string rol)
        {
            DataView dvRol = d_rol.busca_rol(rol);
            if (dvRol != null)
                return dvRol;
            return null;
        }
        public int retornaId(string rol)
        {
            int idRol = d_rol.retornaID_rol(rol);
            if (idRol != -1)
                return idRol;
            return -1;
        }

        public int cantidad_usuarios_rol(string rol)
        {
            int idRol = d_rol.cantidad_usuarios_rol(rol);
            if (idRol != -1)
                return idRol;
            return -1;
        }
    }
}
