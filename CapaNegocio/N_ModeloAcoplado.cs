﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_ModeloAcoplado
    {
        D_ModeloAcoplado d_modelo_acoplado;
        E_ModeloAcoplado e_modelo_acoplado;
        string usuario;

        public N_ModeloAcoplado(string usuario)
        {
            this.usuario = usuario;
            d_modelo_acoplado = new D_ModeloAcoplado(usuario);
            e_modelo_acoplado = new E_ModeloAcoplado();
        }
        public List<string> insertar(E_ModeloAcoplado e_modelo_acoplado)
        {
            List<string> lista = new List<string>();
            d_modelo_acoplado.insertar(e_modelo_acoplado);
            lista.Add("Exito");
            lista.Add("Modelo Acoplado insertada con exito");
            return lista;
        }
        public List<string> modificar(E_ModeloAcoplado e_modelo_acoplado)
        {
            List<string> lista = new List<string>();
            int resp = d_modelo_acoplado.modificar(e_modelo_acoplado);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Modelo Acoplado modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_ModeloAcoplado e_modelo_acoplado)
        {
            List<string> lista = new List<string>();
            int resp = d_modelo_acoplado.baja(e_modelo_acoplado);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Modelo Acoplado deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_ModeloAcoplado retornaModeloAcoplado(int id)
        {
            E_ModeloAcoplado e_modelo_acoplado = d_modelo_acoplado.retornaModeloAcoplado(id);
            if (e_modelo_acoplado != null)
                return e_modelo_acoplado;
            return null;
        }
        public DataTable listarTodos(string idMarca)
        {
            DataTable dtModeloAcoplado = d_modelo_acoplado.listarTodos(idMarca);
            if (dtModeloAcoplado != null)
                return dtModeloAcoplado;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtModeloAcoplado = d_modelo_acoplado.listarHabilitados();
            if (dtModeloAcoplado != null)
                return dtModeloAcoplado;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtModeloAcoplado = d_modelo_acoplado.listarDeshabilitados();
            if (dtModeloAcoplado != null)
                return dtModeloAcoplado;
            return null;
        }
        public List<String> listarComboBox(string marca)
        {
            List<String> listaCombo = d_modelo_acoplado.listarComboBox(marca);
            if (listaCombo.Count != 0)
                return listaCombo;
            return null;
        }
        public bool existe(string name)
        {
            return d_modelo_acoplado.existe(name);
        }
        public string retornaId(string name)
        {
            return d_modelo_acoplado.retornaId(name);
        }
    }
}
