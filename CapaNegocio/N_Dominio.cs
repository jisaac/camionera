﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
  public  class N_Dominio
    {
        public N_Dominio(string usuario)
        {
            d_dominio = new D_Dominio(usuario);
        }
        D_Dominio d_dominio;
        public int Retorna_Id_Dominio( string dominio)
        {
            return d_dominio.retornaId(dominio);
        }

        public string Retorna_Dominio(int id)
        {
            return d_dominio.retornaDominio(id);
        }

    }
}
