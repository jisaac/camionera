﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using System.Data;
using CapaEntidades;

namespace CapaNegocio
{
    public class N_Usuario
    {
        D_Usuario d_usuario;
        E_Usuario e_usuario;
        string usuario;

        public N_Usuario(string usuario)
        {
            this.usuario = usuario;
            d_usuario = new D_Usuario(usuario);
            e_usuario = new E_Usuario();
        }
        public N_Usuario()
        {
            d_usuario = new D_Usuario(usuario);
            e_usuario = new E_Usuario();
        }
        public List<string> ingresar(string usuario, string clave)
        {
            return d_usuario.ingresar(usuario, clave);
        }
        public List<string> insertar(E_Usuario e_usuario)
        {
            List<string> lista = new List<string>();
            d_usuario.insertar(e_usuario);
            lista.Add("Exito");
            lista.Add("Usuario insertado con exito");
            return lista;
        }
        public List<string> modificar(E_Usuario e_usuario)
        {
            List<string> lista = new List<string>();
            int resp = d_usuario.modificar(e_usuario);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Usuario modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_Usuario e_usuario)
        {
            List<string> lista = new List<string>();
            int resp = d_usuario.baja(e_usuario);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Usuario deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_Usuario retornaUsuario(string rol)
        {
            E_Usuario e_usuario = d_usuario.retornaUsuario(rol);
            if (e_usuario != null)
                return e_usuario;
            return null;
        }
        public DataTable listar_usuarios()
        {
            return d_usuario.listar_usuarios();
        }
        public DataView buscar_usuario(string usuario)
        {
            DataView dvRol = d_usuario.buscar_usuario(usuario);
            if (dvRol != null)
                return dvRol;
            return null;
        }
        public E_Usuario retornaNumLegajo()
        {
            E_Usuario e_usuario = d_usuario.retornaNum_legajo();
            return e_usuario;
        }
        public E_Usuario retornaNumLegajo(int num_legajo)
        {
            E_Usuario e_usuario = d_usuario.retornaNum_legajo(num_legajo);
            return e_usuario;
        }
        public bool enUso(int num_legajo)
        {
            Boolean e_usuario = d_usuario.enUso(num_legajo);
            return e_usuario;
        }

    }
}
