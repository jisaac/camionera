﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_MarcaAcoplado
    {
        D_MarcaAcoplado d_marca_acoplado;
        E_MarcaAcoplado e_marca_acoplado;
        string usuario;

        public N_MarcaAcoplado(string usuario)
        {
            this.usuario = usuario;
            d_marca_acoplado = new D_MarcaAcoplado(usuario);
            e_marca_acoplado = new E_MarcaAcoplado();
        }
        public List<string> insertar(E_MarcaAcoplado e_marca_acoplado)
        {
            List<string> lista = new List<string>();
           d_marca_acoplado.insertar(e_marca_acoplado);
            lista.Add("Exito");
            lista.Add("Marca Acoplado insertada con exito");
            return lista;
        }
        public List<string> modificar(E_MarcaAcoplado e_marca_acoplado)
        {
            List<string> lista = new List<string>();
            int resp = d_marca_acoplado.modificar(e_marca_acoplado);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Marca Acoplado modificada con exito");
            }
            else
            {
                lista.Add("Advertencia");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_MarcaAcoplado e_marca_acoplado)
        {
            List<string> lista = new List<string>();
            int resp = d_marca_acoplado.baja(e_marca_acoplado);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Marca Acoplado deshabilitado con exito");
            }
            else
            {
                lista.Add("Advertencia");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_MarcaAcoplado retornaMarcaAcoplado(int id)
        {
            E_MarcaAcoplado e_marca_acoplado = d_marca_acoplado.retornaMarcaAcoplado(id);
            if (e_marca_acoplado != null)
                return e_marca_acoplado;
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtMarcaAcoplado = d_marca_acoplado.listarTodos();
            if (dtMarcaAcoplado != null)
                return dtMarcaAcoplado;
            return null;
        }
        public List<String> listarComboBox()
        {
            List<String> listaCombo = d_marca_acoplado.listarComboBox();
            if (listaCombo.Count != 0)
                return listaCombo;
            return null;
        }
        public bool existe(string name)
        {
            return d_marca_acoplado.existe(name);
        }
        public string retornaId(string name)
        {
            return d_marca_acoplado.retornaId(name);
        }
    }
}
