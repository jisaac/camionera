﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_Pedido
    {
        D_Pedido d_pedido;
        D_Camion d_camion;
        D_Acoplado d_acoplado;
        E_Pedido e_pedido;
        string usuario;

        public N_Pedido(string usuario)
        {
            this.usuario = usuario;
            d_pedido = new D_Pedido(usuario);
            d_acoplado = new D_Acoplado(usuario);
            d_camion = new D_Camion(usuario);
            e_pedido = new E_Pedido();
        }
        public List<string> insertar(E_Pedido e_pedido)
        {
            List<string> lista = new List<string>();
            d_pedido.insertar(e_pedido);
            lista.Add("Exito");
            lista.Add("Pedido insertado con exito");
            return lista;
        }
        public List<string> modificar(E_Pedido e_pedido)
        {
            List<string> lista = new List<string>();
            int resp = d_pedido.modificar(e_pedido);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Pedido modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_Pedido e_pedido)
        {
            List<string> lista = new List<string>();
            int resp = d_pedido.baja(e_pedido);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Pedido cancelado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_Pedido retornaPedido(int id)
        {
            E_Pedido e_pedido = d_pedido.retornaPedido(id);
            if (e_pedido != null)
                return e_pedido;
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtPedido = d_pedido.listarTodos();
            if (dtPedido != null)
                return dtPedido;
            return null;
        }
        public DataTable listarPendientes()
        {
            DataTable dtPedido = d_pedido.listarPendientes();
            if (dtPedido != null)
                return dtPedido;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtPedido = d_pedido.listarHabilitados();
            if (dtPedido != null)
                return dtPedido;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtPedido = d_pedido.listarDeshabilitados();
            if (dtPedido != null)
                return dtPedido;
            return null;
        }
        public DataTable buscar_pedido(int num_pedido)
        {
            DataView dvPedido = d_pedido.buscar_pedido(num_pedido);
            if (dvPedido != null)
                return dvPedido.ToTable();
            return null;
        }
        public DataTable buscar_pedido_desde_hasta(string desde,string hasta)
        {
            DataTable dtPedido = d_pedido.buscar_pedido_desde_hasta(desde,hasta);
            if (dtPedido != null)
                return dtPedido;
            return null;
        }
        public DataTable buscar_pedido_pendiente(int num_pedido)
        {
            DataView dvPedido = d_pedido.buscar_pedido_pendiente(num_pedido);
            if (dvPedido != null)
                return dvPedido.ToTable();
            return null;
        }
        public DataTable buscar_pedido_desde_hasta_pendiente(string desde, string hasta)
        {
            DataTable dtPedido = d_pedido.buscar_pedido_desde_hasta_pendientes(desde, hasta);
            if (dtPedido != null)
                return dtPedido;
            return null;
        }
        public List<DataTable> retornaListaUnidades(E_Pedido e_pedido, Int32 desde, Int32 hasta,E_UnidadesViaje UnidadesViaje)
        {
            List<DataTable> listaUnidades = new List<DataTable>();
            DateTime fecha_antes = e_pedido.Fecha_salida.AddDays(-desde);
            DateTime fecha_despues = e_pedido.Fecha_regreso.AddDays(hasta);
            if (e_pedido.Enganche == "ENGANCHE CON REMOLQUE")
            {
                listaUnidades.Add(d_camion.listar_rigidos_enganche_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Tipo_carga, e_pedido.Altura_camion, e_pedido.Ancho_interior_camion, e_pedido.Ancho_exterior_camion, e_pedido.Longitud_camion, e_pedido.Enganche, e_pedido.Fecha_salida, e_pedido.Fecha_regreso));
                listaUnidades.Add(d_acoplado.listar_remolque_semiremolque_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Altura_acoplado, e_pedido.Ancho_interior_acoplado, e_pedido.Ancho_exterior_acoplado, e_pedido.Longitud_acoplado, e_pedido.Tipo_carga, "REMOLQUE", fecha_antes, fecha_despues));
            }
            if (UnidadesViaje != null && listaUnidades[0] != null)
            {

                for(int i = listaUnidades[0].Rows.Count-1; i >= 0; i--)
                {
                    DataRow dr = listaUnidades[0].Rows[i];
                    if (Convert.ToInt32(dr["ID Dominio"]) == UnidadesViaje.Id_camion)
                        dr.Delete();
                }

            }
            if (UnidadesViaje != null && listaUnidades[1] != null)
            {

                for (int i = listaUnidades[1].Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = listaUnidades[1].Rows[i];
                    if (Convert.ToInt32(dr["ID Dominio"]) == UnidadesViaje.Id_acoplado)
                        dr.Delete();
                }

            }
            else if (e_pedido.Enganche == "SIN ENGANCHE RIGIDO")
            {
                listaUnidades.Add(d_camion.listar_rigidos_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Tipo_carga, e_pedido.Altura_camion, e_pedido.Ancho_interior_camion, e_pedido.Ancho_exterior_camion, e_pedido.Longitud_camion, fecha_antes, fecha_despues));
                if (UnidadesViaje != null && listaUnidades[0] != null)
                {

                    for (int i = listaUnidades[0].Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = listaUnidades[0].Rows[i];
                        if (Convert.ToInt32(dr["ID Dominio"]) == UnidadesViaje.Id_camion)
                            dr.Delete();
                    }

                }

            }
            else //CON ENGANCHE SEMIREMOLQUE, TRACTOCAMION
            {
                listaUnidades.Add(d_camion.listar_tractocamiones_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Tipo_carga, e_pedido.Altura_camion, e_pedido.Ancho_interior_camion, e_pedido.Ancho_exterior_camion, e_pedido.Longitud_camion, fecha_antes, fecha_despues));
                listaUnidades.Add(d_acoplado.listar_remolque_semiremolque_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Altura_acoplado, e_pedido.Ancho_interior_acoplado, e_pedido.Ancho_exterior_acoplado, e_pedido.Longitud_acoplado, e_pedido.Tipo_carga, "SEMIREMOLQUE", fecha_antes, fecha_despues));
                if (UnidadesViaje != null && listaUnidades[0] != null)
                {

                    for (int i = listaUnidades[0].Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = listaUnidades[0].Rows[i];
                        if (Convert.ToInt32(dr["ID Dominio"]) == UnidadesViaje.Id_camion)
                            dr.Delete();
                    }

                }
                if (UnidadesViaje != null && listaUnidades[1] != null)
                {

                    for (int i = listaUnidades[1].Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = listaUnidades[1].Rows[i];
                        if (Convert.ToInt32(dr["ID Dominio"]) == UnidadesViaje.Id_acoplado)
                            dr.Delete();
                    }

                }
            }

                  return listaUnidades;
        }

        public List<DataTable> retornaListaUnidadesSugerencia(E_Pedido e_pedido, Int32 desde, Int32 hasta, E_UnidadesViaje e_unidadesViaje)
        {
            List<DataTable> listaUnidades = new List<DataTable>();
            DateTime fecha_antes = e_pedido.Fecha_salida.AddDays(-desde);
            DateTime fecha_despues = e_pedido.Fecha_regreso.AddDays(hasta);
            if (e_pedido.Enganche == "ENGANCHE CON REMOLQUE")
            {
                listaUnidades.Add(d_camion.listar_rigidos_enganche_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Tipo_carga, e_pedido.Altura_camion, e_pedido.Ancho_interior_camion, e_pedido.Ancho_exterior_camion, e_pedido.Longitud_camion, e_pedido.Enganche, fecha_antes, fecha_despues));
                listaUnidades.Add(d_acoplado.listar_remolque_semiremolque_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Altura_acoplado, e_pedido.Ancho_interior_acoplado, e_pedido.Ancho_exterior_acoplado, e_pedido.Longitud_acoplado, e_pedido.Tipo_carga, "REMOLQUE", fecha_antes, fecha_despues));
            }
            else if (e_pedido.Enganche == "SIN ENGANCHE RIGIDO")
            {
                listaUnidades.Add(d_camion.listar_rigidos_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Tipo_carga, e_pedido.Altura_camion, e_pedido.Ancho_interior_camion, e_pedido.Ancho_exterior_camion, e_pedido.Longitud_camion, fecha_antes, fecha_despues));
            }
            else //CON ENGANCHE SEMIREMOLQUE, TRACTOCAMION
            {
                listaUnidades.Add(d_camion.listar_tractocamiones_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Tipo_carga, e_pedido.Altura_camion, e_pedido.Ancho_interior_camion, e_pedido.Ancho_exterior_camion, e_pedido.Longitud_camion, fecha_antes, fecha_despues));
                listaUnidades.Add(d_acoplado.listar_remolque_semiremolque_pedido(e_pedido.Km_viaje, e_pedido.Volumen, e_pedido.Altura_acoplado, e_pedido.Ancho_interior_acoplado, e_pedido.Ancho_exterior_acoplado, e_pedido.Longitud_acoplado, e_pedido.Tipo_carga, "SEMIREMOLQUE", fecha_antes, fecha_despues));
            }
            if (e_unidadesViaje != null && listaUnidades[0] != null)
            {

                for (int i = listaUnidades[0].Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = listaUnidades[0].Rows[i];
                    if (dr["Dominio"].ToString() == e_unidadesViaje.Id_camion.ToString())
                        dr.Delete();
                }

            }
            if (e_unidadesViaje != null && listaUnidades.Count>1 )
            { 
                for (int i = listaUnidades[1].Rows.Count - 1; i >= 0; i--)
                {
                    DataRow dr = listaUnidades[1].Rows[i];
                    if (dr["Dominio"].ToString() == e_unidadesViaje.Id_acoplado.ToString())
                        dr.Delete();
                }

            }
            return listaUnidades;
        }

        public List<string> retornaPrioridades()
        {
            return d_pedido.retornaPrioridades();
        }
        public List<string> retornaEstados()
        {
            return d_pedido.retornaEstados();
        }
        public int retornaIdUltimoPedido()
        {
            return d_pedido.retornaIdUltimoPedido();
        }
        public string retornaEstado(int id_pedido)
        {
            return d_pedido.retornaEstado(id_pedido);
        }
    }
}
