﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_Acoplado
    {
        D_Acoplado d_acoplado;
        E_Acoplado e_acoplado;
        string usuario;

        public N_Acoplado(string usuario)
        {
            this.usuario = usuario;
            d_acoplado = new D_Acoplado(usuario);
            e_acoplado = new E_Acoplado();
        }
        public List<string> insertar(E_Acoplado e_acoplado, string dominio)
        {
            List<string> lista = new List<string>();
            d_acoplado.insertar(e_acoplado, dominio);
            lista.Add("Exito");
            lista.Add("Acoplado insertado con exito");
            return lista;
        }
        public List<string> modificar(E_Acoplado e_acoplado, string dominio,string motivo)
        {
            List<string> lista = new List<string>();
            int resp = d_acoplado.modificar(e_acoplado,dominio,motivo);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Acoplado modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(int acoplado)
        {
            List<string> lista = new List<string>();
            int resp = d_acoplado.baja(acoplado);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Acoplado deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_Acoplado retornaAcoplado(int id_dominio)
        {
            E_Acoplado e_acoplado = d_acoplado.retornaAcoplado(id_dominio);
            if (e_acoplado != null)
                return e_acoplado;
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtAcoplado = d_acoplado.listarTodos();
            if (dtAcoplado != null)
                return dtAcoplado;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtAcoplado = d_acoplado.listarHabilitados();
            if (dtAcoplado != null)
                return dtAcoplado;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtAcoplado = d_acoplado.listarDeshabilitados();
            if (dtAcoplado != null)
                return dtAcoplado;
            return null;
        }
        public DataTable listarPorFechaAltaHabilitados(string desde, string hasta)
        {
            DataTable dtAcoplado = d_acoplado.listarPorFechaAltaHabilitados(desde, hasta);
            if (dtAcoplado != null)
                return dtAcoplado;
            return null;
        }
        public DataTable busquedaPorEntradaDeTexto(string palabra)
        {
            DataTable dtAcoplado = d_acoplado.busquedaPorEntradaDeTexto(palabra);
            if (dtAcoplado != null)
                return dtAcoplado;
            return null;
        }
        public DataTable BusquedaPorRangoDeFechaDeAlta(string desde,string hasta)
        {
            DataTable dtAcoplado = d_acoplado.busquedaPorRangoDeFechaDeAlta(desde,hasta);
            if (dtAcoplado != null)
                return dtAcoplado;
            return null;
        }
    }
    
}
