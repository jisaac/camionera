﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_Mecanico
    {
        D_Mecanico d_mecanico;
        E_Mecanico e_mecanico;
        string usuario;

        public N_Mecanico(string usuario)
        {
            this.usuario = usuario;
            d_mecanico = new D_Mecanico(usuario);
            e_mecanico = new E_Mecanico();
        }
        public List<string> insertar(E_Mecanico e_mecanico)
        {
            List<string> lista = new List<string>();
           d_mecanico.insertar(e_mecanico);
            lista.Add("Exito");
            lista.Add("Mecanico insertada con exito");
            return lista;
        }
        public List<string> modificar(E_Mecanico e_mecanico)
        {
            List<string> lista = new List<string>();
            int resp = d_mecanico.modificar(e_mecanico);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Mecanico modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_Mecanico e_mecanico)
        {
            List<string> lista = new List<string>();
            int resp = d_mecanico.baja(e_mecanico);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Mecanico deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_Mecanico retornaMarcaCamion(string num_legajo)
        {
            E_Mecanico e_mecanico = d_mecanico.retornaMecanico(num_legajo);
            if (e_mecanico != null)
                return e_mecanico;
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtMecanico = d_mecanico.listarTodos();
            if (dtMecanico != null)
                return dtMecanico;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtMecanico = d_mecanico.listarHabilitados();
            if (dtMecanico != null)
                return dtMecanico;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtMecanico = d_mecanico.listarDeshabilitados();
            if (dtMecanico != null)
                return dtMecanico;
            return null;
        }
    }
}
