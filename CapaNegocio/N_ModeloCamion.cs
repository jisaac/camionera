﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_ModeloCamion
    {
        D_ModeloCamion d_modeloCamion;
        E_ModeloCamion e_modeloCamion;
        string usuario;

        public N_ModeloCamion(string usuario)
        {
            this.usuario = usuario;
            d_modeloCamion = new D_ModeloCamion(usuario);
            e_modeloCamion = new E_ModeloCamion();
        }
        public List<string> insertar(E_ModeloCamion e_modeloCamion)
        {
            List<string> lista = new List<string>();
            d_modeloCamion.insertar(e_modeloCamion);
            lista.Add("Exito");
            lista.Add("Modelo Camion insertada con exito");
            return lista;
        }
        public List<string> modificar(E_ModeloCamion e_modeloCamion)
        {
            List<string> lista = new List<string>();
            int resp = d_modeloCamion.modificar(e_modeloCamion);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Modelo Camion modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_ModeloCamion e_modeloCamion)
        {
            List<string> lista = new List<string>();
            int resp = d_modeloCamion.baja(e_modeloCamion);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Modelo Camion deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_ModeloCamion retornaModeloCamion(int id)
        {
            E_ModeloCamion e_modeloCamion = d_modeloCamion.retornaModeloCamion(id);
            if (e_modeloCamion != null)
                return e_modeloCamion;
            return null;
        }
        public DataTable listarTodos(string idMarca)
        {
            DataTable dtModeloCamion = d_modeloCamion.listarTodos(idMarca);
            if (dtModeloCamion != null)
                return dtModeloCamion;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtModeloCamion = d_modeloCamion.listarHabilitados();
            if (dtModeloCamion != null)
                return dtModeloCamion;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtModeloCamion = d_modeloCamion.listarDeshabilitados();
            if (dtModeloCamion != null)
                return dtModeloCamion;
            return null;
        }
        public List<String> listarComboBox(string marca)
        {
            List<String> listaCombo = d_modeloCamion.listarComboBox(marca);
            if (listaCombo.Count != 0)
                return listaCombo;
            return null;
        }
        public bool existe(string name)
        {
            return d_modeloCamion.existe(name);
        }
        public string retornaId(string name)
        {
            return d_modeloCamion.retornaId(name);
        }
    }
}
