﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_TipoCarga
    {
        D_TipoCarga d_tipo_carga;
        E_TipoCarga e_tipo_carga;
        string usuario;

        public N_TipoCarga(string usuario)
        {
            this.usuario = usuario;
            d_tipo_carga = new D_TipoCarga(usuario);
            e_tipo_carga = new E_TipoCarga();
        }
        public List<string> insertar(E_TipoCarga e_tipo_carga)
        {
            List<string> lista = new List<string>();
            d_tipo_carga.insertar(e_tipo_carga);
            lista.Add("Exito");
            lista.Add("Tipo de carga insertado con exito");
            return lista;
        }
        public List<string> modificar(E_TipoCarga e_tipo_carga)
        {
            List<string> lista = new List<string>();
            int resp = d_tipo_carga.modificar(e_tipo_carga);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Tipo de carga modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_TipoCarga e_tipo_carga)
        {
            List<string> lista = new List<string>();
            int resp = d_tipo_carga.baja(e_tipo_carga);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Tipo de carga deshabilitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_TipoCarga retornaTipoCarga(int id)
        {
            E_TipoCarga e_tipo_carga = d_tipo_carga.retornaTipoCarga(id);
            if (e_tipo_carga != null)
                return e_tipo_carga;
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtTipoAcoplado = d_tipo_carga.listarTodos();
            if (dtTipoAcoplado != null)
                return dtTipoAcoplado;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtTipoAcoplado = d_tipo_carga.listarHabilitados();
            if (dtTipoAcoplado != null)
                return dtTipoAcoplado;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtTipoAcoplado = d_tipo_carga.listarDeshabilitados();
            if (dtTipoAcoplado != null)
                return dtTipoAcoplado;
            return null;
        }
        public List<String> listarComboBox()
        {
            List<String> listaCombo = d_tipo_carga.listarComboBox();
            if (listaCombo.Count != 0)
                return listaCombo;
            return null;
        }
        public bool existe(string name)
        {
            return d_tipo_carga.existe(name);
        }
        public int retornaId(string name)
        {
            return d_tipo_carga.retornaId(name);
        }
    }
}
