﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaDatos;
using CapaEntidades;
using System.Data;

namespace CapaNegocio
{
    public class N_Chofer
    {
        D_Chofer d_chofer;
        E_Chofer e_chofer;
        string usuario;

        public N_Chofer(string usuario)
        {
            this.usuario = usuario;
            d_chofer = new D_Chofer(usuario);
            e_chofer = new E_Chofer();
        }
        public List<string> insertar(E_Chofer e_chofer)
        {
            List<string> lista = new List<string>();
            d_chofer.insertar(e_chofer);
            lista.Add("Exito");
            lista.Add("Chofer insertado con exito");
            return lista;
        }
        public List<string> modificar(E_Chofer e_chofer)
        {
            List<string> lista = new List<string>();
            int resp = d_chofer.modificar(e_chofer);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Chofer modificado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public List<string> baja(E_Chofer e_chofer)
        {
            List<string> lista = new List<string>();
            int resp = d_chofer.baja(e_chofer);
            if (resp > 0)
            {
                lista.Add("Exito");
                lista.Add("Chofer deshablitado con exito");
            }
            else
            {
                lista.Add("Error");
                lista.Add("No se pudo realizar la acción, 0 filas afectadas");
            }
            return lista;
        }
        public E_Chofer retornaChofer(string chofer)
        {
            E_Chofer e_camion = d_chofer.retornaChofer(chofer);
            if (e_camion != null)
                return e_camion;
            return null;
        }
        public int retornaNumLegajoChofer(string chofer)
        {
            int e_camion = d_chofer.retornaNumLegajoChofer(chofer);
            if (e_camion != -1)
                return e_camion;
            return -1;
        }
        public DataView buscar_chofer(string num_legajo)
        {
            if (num_legajo != "BUSCAR CHOFER POR Nº CEDULA..")
            {
                DataView dvChofer = d_chofer.buscar_chofer(num_legajo);
                if (dvChofer != null)
                    return dvChofer;
            }
            return null;
        }
        public DataTable listarTodos()
        {
            DataTable dtChofer = d_chofer.listarTodos();
            if (dtChofer != null)
                return dtChofer;
            return null;
        }
        public DataTable listarHabilitados()
        {
            DataTable dtChofer = d_chofer.listarHabilitados();
            if (dtChofer != null)
                return dtChofer;
            return null;
        }
        public DataTable listarDeshabilitados()
        {
            DataTable dtChofer = d_chofer.listarDeshabilitados();
            if (dtChofer != null)
                return dtChofer;
            return null;
        }

        public bool chofer_en_viaje(int num_legajo)
        {
            bool resp = d_chofer.chofer_en_viaje(num_legajo);
            if (resp)
                return true;
            return false;
        }

        public bool chofer_libre(int num_legajo,DateTime fecha_salida, DateTime fecha_regreso)
        {
            bool resp = d_chofer.chofer_libre(num_legajo, fecha_salida, fecha_regreso);
            if (resp)
                return true;
            return false;
        }
    }
}
