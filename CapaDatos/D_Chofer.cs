﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;


namespace CapaDatos
{
    public class D_Chofer
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Chofer e_chofer;
        string usuario;
        public D_Chofer(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_chofer = new E_Chofer();
        }
        public void insertar(E_Chofer e_chofer)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta nuevo chofer en la tabla chofer**************/
                    cmd.CommandText = "insert into chofer values ('" + e_chofer.Num_legajo+ "','" + e_chofer.Nombre + "','" + e_chofer.Apellido + "','"+ e_chofer.Num_cedula+"' ,'" + e_chofer.Direccion + "','" + e_chofer.Tel_personal + "','" + e_chofer.Tel_trabajo+ "','HABILITADO')";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto el Chofer num_legajo=" + e_chofer.Num_legajo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar chofer", e);
                        default: throw new System.Exception("Ups, algo paso al insertar chofer", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar chofer", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_Chofer e_chofer)
        {
            e_chofer = this.e_chofer;
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja al mecanico,se marca el estado como DESHABILITADO**********/
                    cmd.CommandText = "update chofer set estado= 'DESHABILITADO' where  num_legajo='" + e_chofer.Num_legajo + "'; SELECT Scope_Identity(); ";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja al Chofer num_legajo=" + e_chofer.Num_legajo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar chofer", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar chofer", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar chofer", e);
                }
            }
            cnn.Close();
        }
        public int modificar(E_Chofer e_chofer)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                int num_legajo = this.retornaNumLegajoChofer(e_chofer.Num_legajo.ToString());
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "update  chofer set nombre='" + e_chofer.Nombre + "',apellido='" + e_chofer.Apellido + "',direccion='" + e_chofer.Direccion + "' , tel_personal='" + e_chofer.Tel_personal + "',tel_trabajo='" + e_chofer.Tel_trabajo + "',estado='" + e_chofer.Estado + "' where num_legajo='" + e_chofer.Num_legajo + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó el Chofer num_legajo=" + e_chofer.Num_legajo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar chofer", e);
                        default: throw new System.Exception("Ups, algo paso al modificar chofer", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar chofer", e);
                }
            }
        }
        public E_Chofer retornaChofer(string num_legajo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select num_legajo, nombre,apellido,num_cedula,direccion,tel_personal,tel_trabajo,estado from chofer where num_legajo='" + num_legajo + "'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_chofer.Num_legajo = Convert.ToInt32(dr[0].ToString());
                        e_chofer.Nombre = dr[1].ToString();
                        e_chofer.Apellido = dr[2].ToString();
                        e_chofer.Num_cedula = dr[3].ToString();
                        e_chofer.Direccion = dr[4].ToString();
                        e_chofer.Tel_personal = dr[5].ToString();
                        e_chofer.Tel_trabajo = dr[6].ToString();
                        e_chofer.Estado = dr[7].ToString();

                    }
                    cnn.Close();
                    return e_chofer;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar chofer", e);
                }
            }
        } //Funcion que retorna un objeto mecanico
        public int retornaNumLegajoChofer(string num_legajo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    string[] nombre = num_legajo.Split(' ');
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select num_legajo from chofer where nombre='" + nombre[0] + "' and apellido ='" + nombre[1] + "'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_chofer.Num_legajo = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                    return e_chofer.Num_legajo;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar legajo de chofer", e);
                }
            }
        } //Funcion que retorna un objeto mecanico
        public DataView buscar_chofer(string texto)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "nombre like '" + texto + "%' or apellido like '" + texto + "%' or num_cedula like '" + texto + "%'";
                    return dv;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar chofer", e);
                }
            }

        }  //Busca Choferes
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select num_legajo as 'N° Legajo', nombre as 'Nombre',apellido as 'Apellido',num_cedula as 'N° Cedula'," +
                        "direccion as 'Direccion',tel_personal as 'Tel. Personal',tel_trabajo 'Tel. Empresa',estado 'Estado', num_cedula,num_legajo from chofer ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los choferes", e);
                }
            }   
        }
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select num_legajo, nombre,apellido,num_cedula,direccion,tel_personal,tel_trabajo,estado from chofer  where estado='HABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los choferes habilitados", e);
                }
            }
               
        }
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select num_legajo, nombre,apellido,num_cedula,direccion,tel_personal,tel_trabajo,estado from chofer  where estado='DESHABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los choferes deshabilitados", e);
                }
            }
        }

        public bool chofer_en_viaje(int num_legajo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int resp = -1;
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select count(c.num_legajo) " +
                                      $"from chofer c join pedido p on c.num_legajo=p.id_chofer join viaje v ON v.id_pedido=p.id_pedido" +
                                      $" where p.estado like 'PENDIENTE' or v.estado like 'EN CURSO' or v.estado like 'PENDIENTE' and c.num_legajo='{num_legajo}' " +
                                      $"group by c.num_legajo";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        resp = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                    if (resp > 0 || resp == -1)
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los choferes en viaje", e);
                }
            }
        }
        public bool chofer_libre(int num_legajo,DateTime fecha_salida, DateTime fecha_regreso)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int resp = 0;
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select *" +
                                      $"from chofer c join pedido p on c.num_legajo=p.id_chofer left join viaje v ON v.id_pedido=p.id_pedido" +
                                      $" where c.num_legajo='{num_legajo}' and p.fecha_salida between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}' and p.fecha_regreso between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        resp = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                    if (resp > 0)
                        return false;
                    else
                        return true;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los choferes libres", e);
                }
            }
        }

    }
}
