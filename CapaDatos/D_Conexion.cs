﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using CapaEntidades;

namespace Logistica.controladores
{
    public enum TipoConexion
    {
        Abrir, Cerrar
    }
    public class D_Conexion
    {
        public clsConexion usuarioDB = new clsConexion();
        public ModeloConexion()
        {
            usuarioDB.Cnn = new SqlConnection();
        }
        public string Modo(TipoConexion Tipo)
        {
            string res = string.Empty;
            string StrCnn = @"Data Source =" + usuarioDB.Servidor + ";Initial Catalog=" + usuarioDB.Basedatos + "; User ID = " + usuarioDB.Usuario.ToLower() + "; Password= " + usuarioDB.Clave.ToLower();
            try
            {
                if ((usuarioDB.Cnn.State == System.Data.ConnectionState.Closed) && (Tipo ==
                TipoConexion.Abrir))
                {
                    usuarioDB.Cnn.ConnectionString = StrCnn;
                    usuarioDB.Cnn.Open();
                }
                else
                    usuarioDB.Cnn.Close();
            }
            catch (SqlException ex)
            {
                res = ex.Message;
            }
            return res;
        } //Funcion que genera una nueva conexion
        public string Validar()
        {
            string res = string.Empty;
            string StrCnn = @"Data Source =" + usuarioDB.Servidor + ";Initial Catalog=" + usuarioDB.Basedatos + "; User ID = " + usuarioDB.Usuario.ToLower() + "; Password= " + usuarioDB.Clave.ToLower();
            try
            {
                usuarioDB.Cnn.ConnectionString = StrCnn;
                usuarioDB.Cnn.Open();
                usuarioDB.Cnn.Close();
            }
            catch (SqlException ex)
            {
                res = ex.Message;
            }
            return res;
        } //Funcion que valida cadena de conexion
        public string QueryLogin(String usuario, String clave)  //Comprueba que exista en tabla login
        {
            return "Select r.rol from usuarios u join roles r on u.id_rol=r.id where usuario='" + usuario + "' and password='" + clave + "' "; 
        }
    }
}
