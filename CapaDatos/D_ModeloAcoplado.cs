﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
    public class D_ModeloAcoplado
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_ModeloAcoplado e_modelo_acoplado;
        string usuario;
        public D_ModeloAcoplado(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_modelo_acoplado = new E_ModeloAcoplado();
        }
        public void insertar(E_ModeloAcoplado e_modelo_acoplado)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta el nuevo modelo de caamion en la tabla modelo_camion**************/
                    cmd.CommandText = "insert into modelo_acoplado values ('" + e_modelo_acoplado.Modelo + "','HABILITADO','" + e_modelo_acoplado.IdMarca + "');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto el Modelo acoplado id=" + e_modelo_acoplado.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Modelo Acoplado", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Modelo Acoplado", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Modelo Acoplado", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_ModeloAcoplado e_modelo_acoplado)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja ,el modelo de camion,se marca el estado como DESHABILITADO**********/
                    cmd.CommandText = "delete from modelo_acoplado where  id='" + e_modelo_acoplado.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery(); 
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja al Modelo acoplado id=" + e_modelo_acoplado.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Modelo Acoplado", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Modelo Acoplado", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Modelo Acoplado", e);
                }
            }
        }
        public int modificar(E_ModeloAcoplado e_modelo_acoplado)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza el modelo de camion**********/
                    cmd.CommandText = "update modelo_acoplado set estado= '" + e_modelo_acoplado.Estado + "' ,modelo='" + e_modelo_acoplado.Modelo + "' where  id='" + e_modelo_acoplado.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó Modelo de remolque-semiremolque id=" + e_modelo_acoplado.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Modelo Acoplado", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Modelo Acoplado", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Modelo Acoplado", e);
                }
            }
        }
        public E_ModeloAcoplado retornaModeloAcoplado(int id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id,modelo,estado from modelo_acoplado where id = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_modelo_acoplado.Id = Convert.ToInt32(dr[0].ToString());
                        e_modelo_acoplado.Modelo = dr[1].ToString();
                        e_modelo_acoplado.Estado = dr[2].ToString();

                    }
                    cnn.Close();
                    return e_modelo_acoplado;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar modelo de acoplado", e);
                }
            }
   
        } //Funcion que retorna un objeto acoplado completo
        public DataTable listarTodos(string idMarca)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,modelo as 'Modelo',estado from modelo_acoplado where id_marca='" + idMarca + "'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los modelos de acoplados", e);
                }
            } 
        }  //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,modelo,estado from modelo_acoplado where estado ='HABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los modelos de acoplados habilitados", e);
                }
            }  
        }
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,modelo,estado from modelo_acoplado where estado ='DESHABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los modelos de acoplados deshabilitados", e);
                }
            }      
        }
        public List<String> listarComboBox(string marca)//Para listar en los combobox ,el modelo segun la marca (relacion marcaModelo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int idMarca = 0;//almacenara el id de la marca que luego va a ser usado para identificar el modelo por este
                    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    List<String> lista = new List<string>();
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("Select id From marca_acoplado where  marca='" + marca + "' ", cnn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        idMarca = Convert.ToInt32(dr[0].ToString());
                    }
                    cmd = new SqlCommand("Select modelo From modelo_acoplado where  id_marca='" + idMarca + "' ", cnn);
                    dr.Close();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        lista.Add(dr[0].ToString());
                    }
                    dr.Close();
                    cnn.Close();
                    return lista;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al devolver los combobox", e);
                }
            }
        }
        public bool existe(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                bool respuesta = false;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select * from modelo_acoplado where tipo ='" + name + "' and estado='HABILITADO' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        respuesta = true;
                    }
                    else
                    {
                        respuesta = false;
                    }

                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar si existe modelo de acoplado", e);
                }
            }  
        }
        public string retornaId(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string respuesta = null;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id from modelo_acoplado where modelo = '" + name + "' and estado='HABILITADO' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        respuesta = dr[0].ToString();

                    }
                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID de modelo de acoplado", e);
                }
            }
        }

    }
}
