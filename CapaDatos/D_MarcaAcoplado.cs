﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;


namespace CapaDatos
{
    public class D_MarcaAcoplado
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_MarcaAcoplado e_marca_acoplado;
        string usuario;
        public D_MarcaAcoplado(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_marca_acoplado = new E_MarcaAcoplado();
        }
        public void insertar(E_MarcaAcoplado e_marca_acoplado)
        {
            SqlTransaction sqlTran;

            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta la nueva marca_acoplado en la tabla marca_acoplado**************/
                    cmd.CommandText = "insert into marca_acoplado values ('" + e_marca_acoplado.Marca + "','HABILITADO');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto la Marca acoplado id=" + e_marca_acoplado.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible realizar la accion, ya existe la marca ingresada en el sistema", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Marca Acoplado", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Marca Acoplado", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_MarcaAcoplado e_marca_acoplado)
        {
            int num_rows;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "delete from marca_acoplado where  id='"+ e_marca_acoplado.Id+ "'; SELECT Scope_Identity(); ";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja a la Marca acoplado id=" + e_marca_acoplado.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar  Marca Acoplado", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar  Marca Acoplado", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar  Marca Acoplado", e);
                }
            }
        }
        public int modificar(E_MarcaAcoplado e_marca_acoplado)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza la marca de acoplado**********/
                    cmd.CommandText = "update marca_acoplado set estado= '"+ e_marca_acoplado.Estado+ "' ,marca='"+ e_marca_acoplado.Marca+"' where  id='" + e_marca_acoplado.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó a la Marca acoplado id=" + e_marca_acoplado.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Marca Acoplado", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Marca Acoplado", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Marca Acoplado", e);
                }
            }
        }
        public E_MarcaAcoplado retornaMarcaAcoplado(int id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id,marca,estado from marca_acoplado where id = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_marca_acoplado.Id = Convert.ToInt32(dr[0].ToString());
                        e_marca_acoplado.Marca = dr[1].ToString();
                        e_marca_acoplado.Estado = dr[2].ToString();

                    }
                    cnn.Close();
                    return e_marca_acoplado;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar marca de acoplado", e);
                }
            }
         
        } //Funcion que retorna un objeto acoplado completo
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,marca as 'Marca',estado from marca_acoplado ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos las Marcas de Acoplados", e);
                }
            }     
        } //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public List<String> listarComboBox()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    List<String> lista = new List<string>();
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("Select marca From marca_acoplado", cnn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        lista.Add(dr[0].ToString());
                    }
                    cnn.Close();
                    return lista;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al devolver listado de combobox", e);
                }
            }
        }
        public bool existe(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                bool respuesta = false;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select * from marca_acoplado where tipo ='" + name + "'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        respuesta = true;
                    }
                    else
                    {
                        respuesta = false;
                    }

                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar si existe marca", e);
                }
            }
                
        }
        public string retornaId(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string respuesta = null;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id from marca_acoplado where marca = '" + name + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        respuesta = dr[0].ToString();

                    }
                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID Marca acoplado", e);
                }
            }
        }
    }
}
