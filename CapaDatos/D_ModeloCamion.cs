﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
    public class D_ModeloCamion
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_ModeloCamion e_modeloCamion;
        string usuario;
        public D_ModeloCamion(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_modeloCamion = new E_ModeloCamion();
        }
        public void insertar(E_ModeloCamion e_modeloCamion)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta el nuevo modelo de caamion en la tabla modelo_camion**************/
                    cmd.CommandText = "insert into modelo_camion values ('" + e_modeloCamion.Modelo + "','HABILITADO','" + e_modeloCamion.IdMarca + "');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto el Modelo camion id=" + e_modeloCamion.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Modelo Camion", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Modelo Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Modelo Camion", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_ModeloCamion e_modeloCamion)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja ,el modelo de camion,se marca el estado como DESHABILITADO**********/
                    cmd.CommandText = "delete from modelo_camion where  id='" + e_modeloCamion.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery(); 
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja al Modelo Camion id=" + e_modeloCamion.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Modelo Camion", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Modelo Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Modelo Camion", e);
                }
            }
        }
        public int modificar(E_ModeloCamion e_modeloCamion)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza el modelo de camion**********/
                    cmd.CommandText = "update modelo_camion set estado= '" + e_modeloCamion.Estado + "' ,modelo='" + e_modeloCamion.Modelo + "' where  id='" + e_modeloCamion.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó Modelo de Camion id=" + e_modeloCamion.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Modelo Camon ", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Modelo Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Modelo Camion", e);
                }
            }
        }
        public E_ModeloCamion retornaModeloCamion(int id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id,modelo,estado from modelo_camion where id = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_modeloCamion.Id = Convert.ToInt32(dr[0].ToString());
                        e_modeloCamion.Modelo = dr[1].ToString();
                        e_modeloCamion.Estado = dr[2].ToString();

                    }
                    cnn.Close();
                    return e_modeloCamion;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar modelo de camion", e);
                }
            }
        } //Funcion que retorna un objeto modeloCamion completo
        public DataTable listarTodos(string idMarca)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,modelo as 'Modelo',estado from modelo_camion where id_marca='" + idMarca + "'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los modelos de camiones", e);
                }
            }
        } //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,modelo,estado from modelo_camion where estado ='HABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los modelos de camiones habilitados", e);
                }
            }
        }
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,modelo,estado from modelo_camion where estado ='DESHABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los modelos de camiones deshabilitados", e);
                }
            }     
        }
        public List<String> listarComboBox(string marca)//Para listar en los combobox ,el modelo segun la marca (relacion marcaModelo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int idMarca = 0;//almacenara el id de la marca que luego va a ser usado para identificar el modelo por este
                    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    List<String> lista = new List<string>();
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("Select id From marca_camion where  marca='" + marca + "' ", cnn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        idMarca = Convert.ToInt32(dr[0].ToString());
                    }
                    cmd = new SqlCommand("Select modelo From modelo_camion where  id_marca='" + idMarca + "' ", cnn);
                    dr.Close();
                    dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        lista.Add(dr[0].ToString());
                    }
                    dr.Close();
                    cnn.Close();
                    return lista;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al devolver los combobox", e);
                }
            }
        }
        public bool existe(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                bool respuesta = false;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select * from modelo_camion where tipo ='" + name + "' and estado='HABILITADO' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        respuesta = true;
                    }
                    else
                    {
                        respuesta = false;
                    }

                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar si existe modelo de camion", e);
                }
            }
        }
        public string retornaId(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string respuesta = null;
                try
                {
                    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id from modelo_camion where modelo = '" + name + "'  ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        respuesta = dr[0].ToString();

                    }
                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID de modelo de camion", e);
                }
            }
        }
    }
}
