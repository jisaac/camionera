﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class D_Usuario
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Usuario e_usuario;
        string usuario;
        public D_Usuario(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_usuario = new E_Usuario();
            dt = this.listar_usuarios();
        }
        public List<string> ingresar(string usuario, string clave)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    List<string> dato = new List<string>();
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = this.QueryLogin(usuario.ToUpper(), clave.ToUpper());
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        dato.Add(dr[0].ToString());
                        dato.Add(dr[1].ToString());
                        dato.Add(dr[2].ToString());
                    }
                    cnn.Close();
                    return dato;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al ingresar al sistema", e);
                }
            }

        }
        string QueryLogin(String usuario, String clave)  //Comprueba que exista en tabla login
        {
            return "Select u.usuario, r.rol, u.estado from usuarios u join roles r on u.id_rol=r.id where usuario='" + usuario + "' and password='" + clave + "' ";
        }
        public void insertar(E_Usuario e_usuario)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "insert into usuarios (usuario,password,num_legajo,id_rol,estado) values ('" + e_usuario.usuario + "','" + e_usuario.clave + "','" + e_usuario.num_legajo + "','" + e_usuario.id_rol + "','HABILITADO');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto usuario id  " + e_usuario.usuario + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Usuario", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Usuario", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Usuario", e);
                }
                cnn.Close();
            }
               

        } //Funcion insertar un nuevo usuario
        public int modificar(E_Usuario e_usuario)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "update usuarios set password='" + e_usuario.clave + "',num_legajo='" + e_usuario.num_legajo + "',id_rol='" + e_usuario.id_rol + "',estado='" + e_usuario.estado + "' where usuario='" + e_usuario.usuario + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se modifico usuario " + e_usuario.usuario + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Usuario ", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Usuario", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Usuario", e);
                }
            }
            // todas las funciones  de abm y otras se ejecutan con la propiedades y el login q pasamos por constructor 
        } //Funcion para modificar algun campo del usuario
        public int baja(E_Usuario e_usuario)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.Connection = cnn;
                    cmd.CommandText = "update usuarios set estado='DESHABILITADO' where usuario='" + e_usuario.usuario + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se dio de baja usuario" + e_usuario.usuario + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Modelo Camion", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Modelo Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Modelo Camion", e);
                }
            }
        } //Funcion para dar de baja un usuario
        public DataView buscar_usuario(string usuario)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "usuario like '" + usuario + "%'";
                    return dv;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar usuario", e);
                }
            }

        }  //Busca usuarios 
        public E_Usuario retornaUsuario(string usuario)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    E_Usuario obUsuario = new E_Usuario();
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select usuario,password,num_legajo,id_rol,estado from usuarios where usuario = '" + usuario + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        obUsuario.usuario = dr[0].ToString();
                        obUsuario.clave = dr[1].ToString();
                        obUsuario.num_legajo = Convert.ToInt32(dr[2]);
                        obUsuario.id_rol = Convert.ToInt32(dr[3]);
                        obUsuario.estado = dr[4].ToString();
                    }
                    cnn.Close();
                    return obUsuario;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar usuario", e);
                }
            }
        }
        public E_Usuario retornaNum_legajo()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    E_Usuario obUsuario = new E_Usuario();
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select top 1 percent num_legajo from usuarios order by num_legajo desc";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        obUsuario.num_legajo = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                    return obUsuario;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar usuario por legajo", e);
                }
            }
        } //Retorna numero legajo para mostrar al cargar nuevo usuario
        public E_Usuario retornaNum_legajo(int num_legajo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    E_Usuario obUsuario = new E_Usuario();
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select * from usuarios where num_legajo = '{num_legajo}'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        obUsuario.num_legajo = Convert.ToInt32(dr[2]);
                        obUsuario.estado = dr[4].ToString();
                    }
                    cnn.Close();
                    return obUsuario;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar usuario por legajo por ID", e);
                }
            }
        } //Retorna numero legajo para mostrar al cargar nuevo usuario

        public Boolean enUso(int num_legajo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    E_Usuario obUsuario = new E_Usuario();
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select * from usuarios where num_legajo = '{num_legajo}'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        obUsuario.usuario = dr[0].ToString();
                    }
                    cnn.Close();
                    if (obUsuario.usuario == usuario)
                        return true;
                    return false;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar usuario en uso", e);
                }
            }
        } //Retorna numero legajo para mostrar al cargar nuevo usuario

        public DataTable listar_usuarios()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter("Select u.usuario 'Usuario',u.password 'Clave',u.num_legajo 'N° Legajo',r.rol 'Rol',u.estado 'Estado' from usuarios u join roles r on u.id_rol=r.id", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar usuarios", e);
                }
            }
        }  //Carga todos los datos de la BD a un DataTable
    }
}
