﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;


namespace CapaDatos
{
    public class D_Mecanico
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Mecanico e_mecanico;
        string usuario;
        public D_Mecanico(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_mecanico = new E_Mecanico();
        }
        public void insertar(E_Mecanico e_mecanico)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta nuevo mecanico en la tabla mecanico**************/
                    cmd.CommandText = "insert into mecanico values ('" + e_mecanico.Num_legajo + "','" + e_mecanico.Nombre + "','" + e_mecanico.Apellido + "','" + e_mecanico.Direccion + "','" + e_mecanico.Telefono1 + "','" + e_mecanico.Telefono2 + "','HABILITADO');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto el Mecanico num_legajo=" + e_mecanico.Num_legajo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Mecanico", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Mecanico", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Mecanio", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_Mecanico e_mecanico)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja al mecanico,se marca el estado como DESHABILITADO**********/
                    cmd.CommandText = "update mecanico set estado= 'DESHABILITADO' where  num_legajo='" + e_mecanico.Num_legajo + "'; SELECT Scope_Identity(); ";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja al Mecanico num_legajo=" + e_mecanico.Num_legajo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Mecancio", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Mecanico", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Mecanico", e);
                }
            }
        }
        public int modificar(E_Mecanico e_mecanico)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza el tipo de acoplado**********/
                    cmd.CommandText = "update  mecanico set num_legajo='" + e_mecanico.Num_legajo + "',nombre='" + e_mecanico.Nombre + "',apellido='" + e_mecanico.Apellido + "',direccion='" + e_mecanico.Direccion + "' , telefono1='" + e_mecanico.Telefono1 + "',telefono2='" + e_mecanico.Telefono2 + "',estado='" + e_mecanico.Estado + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó el Mecanico num_legajo=" + e_mecanico.Num_legajo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Mecanico", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Mecanico", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Mecanico", e);
                }
            }
        }
        public E_Mecanico retornaMecanico(string num_legajo)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select num_legajo, nombre,apellido,direccion,telefono1,telefono2,estado from mecanico where num_legajo='" + num_legajo + "'";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_mecanico.Num_legajo = Convert.ToInt32(dr[0].ToString());
                        e_mecanico.Nombre = dr[1].ToString();
                        e_mecanico.Apellido = dr[2].ToString();
                        e_mecanico.Direccion = dr[3].ToString();
                        e_mecanico.Telefono1 = dr[4].ToString();
                        e_mecanico.Telefono2 = dr[5].ToString();
                        e_mecanico.Estado = dr[5].ToString();

                    }
                    cnn.Close();
                    return e_mecanico;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar Mecanico", e);
                }
            }
               
        } //Funcion que retorna un objeto mecanico
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select num_legajo, nombre,apellido,direccion,telefono1,telefono2,estado from mecanico ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar lista de todos los mecanicos", e);
                }
            }
                
        }
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select num_legajo, nombre, apellido, direccion, telefono1, telefono2, estado from mecanico where estado='HABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar lista de todos los mecanicos habilitados", e);
                }
            }   
        }
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select num_legajo, nombre, apellido, direccion, telefono1, telefono2, estado from mecanico where estado='DESHABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar lista de todos los mecanicos deshabilitados", e);
                }
            }
                
        }

    }
}
