﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using System.Data;
using System.Data.SqlClient;
namespace Logistica.controladores
{
    public class D_MarcaAcoplado
    {
        public D_MarcaAcopladoo(string usuario)
        {
             this.usuario = usuario;
            ModeloAcoplado = new clsModeloAcoplado();

        }
        string usuario;
        public clsModeloAcoplado ModeloAcoplado { get; set; }
        DataTable dt;
        SqlDataAdapter da;

        public string Insertar()
        {
            string respuesta;
            SqlTransaction sqlTran;
            using (conexion.Cnn)
            {
                if (Existe(ModeloAcoplado.Modelo))
                {
                    respuesta = "El Modelo ya existe!";
                }
                else
                {
                    conexion.Cnn.Open();
                    sqlTran = conexion.Cnn.BeginTransaction();
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = conexion.Cnn;
                        cmd.Transaction = sqlTran;
                        /*********se inserta el nuevo modelo de caamion en la tabla modelo_camion**************/
                        cmd.CommandText = "insert into modelo_acoplado values ('" + ModeloAcoplado.Modelo + "','ACTIVO')";
                        string id_insertado = cmd.ExecuteScalar().ToString();
                        if (id_insertado != null)
                        {
                            ModeloAcoplado.Id = Convert.ToInt32(id_insertado);
                            cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + conexion.Usuario + "','" + "se inserto el Modelo acoplado id=" + ModeloAcoplado.Id + "')";
                            cmd.ExecuteNonQuery();
                            respuesta = null;
                            sqlTran.Commit();
                        }
                        else
                        {
                            respuesta = "Error al guardar Modelo de Acoplado";
                        }

                        conexion.Cnn.Close();
                    }
                    catch
                    {
                        respuesta = "Error al agregar Modelo de Acoplado";
                        sqlTran.Rollback();

                    }
                    conexion.Cnn.Close();
                }
         
            }
            
            return respuesta;


        }
        public string Baja()
        {
            string respuesta;
            SqlTransaction sqlTran;
            using (conexion.Cnn)
            {
                conexion.Cnn.Open();
                sqlTran = conexion.Cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conexion.Cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja ,el modelo de camion,se marca el estado como inactivo**********/
                    cmd.CommandText = "update modelo_acoplado set estado= 'INACTIVO' where  id='" + ModeloAcoplado.Id + "'";
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + conexion.Usuario + "','" + " Dió de baja al Modelo Acoplado id=" + ModeloAcoplado.Id + "')";
                        cmd.ExecuteNonQuery();
                        respuesta = null;
                        sqlTran.Commit();
                    }
                    else
                    {
                        respuesta = "Error al dar de baja Modelo de Acoplado";
                    }

                    conexion.Cnn.Close();
                }
                catch
                {
                    respuesta = "Error al dar de baja Modelo de Acoplado";
                    sqlTran.Rollback();

                }
                conexion.Cnn.Close();
            }
            
            return respuesta;


        }
        public string Modificacion()
        {
            string respuesta;
            SqlTransaction sqlTran;
            using (conexion.Cnn)
            {
                conexion.Cnn.Open();
                sqlTran = conexion.Cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = conexion.Cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza el modelo de camion**********/
                    cmd.CommandText = "update modelo_acoplado set estado= '" + ModeloAcoplado.Estado + "' ,modelo='" + ModeloAcoplado.Modelo + "' where  id='" + ModeloAcoplado.Id + "'";
                    if (cmd.ExecuteNonQuery() > 0)
                    {
                        cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + conexion.Usuario + "','" + " Modificó Modelo de Acoplado id=" + ModeloAcoplado.Id + "')";                        cmd.ExecuteNonQuery();
                        respuesta = null;
                        sqlTran.Commit();
                    }
                    else
                    {
                        respuesta = "Error al modificar Modelo de Acoplado";
                    }

                    conexion.Cnn.Close();
                }
                catch
                {
                    respuesta = "Error al modificar Modelo de Acoplado";
                    sqlTran.Rollback();

                }
                conexion.Cnn.Close();
            }
            
            return respuesta;
        }
        public clsModeloAcoplado RetornaModeloAcoplado(string id)
        {
            try
            {

                SqlCommand cmd = new SqlCommand();
                conexion.Cnn.Open();
                cmd.Connection = conexion.Cnn;
                cmd.CommandText = "Select id,modelo,estado from modelo_acoplado where id = '" + ModeloAcoplado.Id + "' ";
                cmd.CommandType = CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ModeloAcoplado.Id = Convert.ToInt32(dr[0].ToString());
                    ModeloAcoplado.Modelo = dr[1].ToString();
                    ModeloAcoplado.Estado = dr[2].ToString();

                }
                conexion.Cnn.Close();
                return ModeloAcoplado;
            }
            catch (Exception)
            {
                return null;
            }
        } //Funcion que retorna un objeto acoplado completo
        public DataTable ListarTodos()
        {
            try
            {
                conexion.Cnn.Open();
                da = new SqlDataAdapter("select id,modelo,estado from modelo_acoplado", conexion.Cnn);
                dt = new DataTable();
                da.Fill(dt);
                conexion.Cnn.Close();
                return dt;
            }
            catch (Exception)
            {
                conexion.Cnn.Close();
                return null;
            }
        } //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public DataTable ListarInactivos()
        {
            try
            {
                conexion.Cnn.Open();
                da = new SqlDataAdapter("select id,modelo,estado from modelo_acoplado where estado ='INACTIVO' ", conexion.Cnn);
                dt = new DataTable();
                da.Fill(dt);
                conexion.Cnn.Close();
                return dt;
            }
            catch (Exception)
            {
                conexion.Cnn.Close();
                return null;
            }
        }
        public DataTable ListarActivos()
        {
            try
            {
                conexion.Cnn.Open();
                da = new SqlDataAdapter("select id,modelo,estado from modelo_acoplado where estado ='ACTIVO' and id_marca='"+ModeloAcoplado.IdMarca+"' ", conexion.Cnn);
                dt = new DataTable();
                da.Fill(dt);
                conexion.Cnn.Close();
                return dt;
            }
            catch (Exception)
            {
                conexion.Cnn.Close();
                return null;
            }
        }

        public List<String> ListaComboBox(string idMarca)//Para listar en los combobox ,el modelo segun la marca (relacion marcaModelo)
        {
            List<String> lista = new List<string>();
            conexion.Cnn.Open();
            SqlCommand cmd = new SqlCommand("Select modelo From modelo_acoplado where id='" + idMarca + "' and estado='ACTIVO'", conexion.Cnn);
            SqlDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                lista.Add(dr[0].ToString());
            }
            conexion.Cnn.Close();
            return lista;
        }
        public bool Existe(string name)
        {
            bool respuesta = false;
            try
            {

                SqlCommand cmd = new SqlCommand();
                conexion.Cnn.Open();
                cmd.Connection = conexion.Cnn;
                cmd.CommandText = "Select * from modelo_acoplado where tipo ='" + name + "' and estado='ACTIVO' ";
                cmd.CommandType = CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows)
                {
                    respuesta = true;
                }
                else
                {
                    respuesta = false;
                }

                conexion.Cnn.Close();
                return respuesta;
            }
            catch (Exception)
            {
                return respuesta;
            }
        }
        public string RetornaId(string name)
        {
            string respuesta = null;
            try
            {

                SqlCommand cmd = new SqlCommand();
                conexion.Cnn.Open();
                cmd.Connection = conexion.Cnn;
                cmd.CommandText = "Select id from modelo_acoplado where modelo = '" + name + "' and estado='ACTIVO' ";
                cmd.CommandType = CommandType.Text;
                SqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {

                    respuesta = dr[0].ToString();

                }
                conexion.Cnn.Close();
                return respuesta;
            }
            catch (Exception)
            {
                return null;
            }
        }



    }
}
