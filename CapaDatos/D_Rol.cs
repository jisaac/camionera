﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;
using CapaEntidades;

namespace CapaDatos
{
    public class D_Rol
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Rol e_rol;
        D_Usuario d_usuario;
        E_Usuario e_usuario;
        string usuario;
        public D_Rol(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_rol = new E_Rol();
        }
        public void insertar(E_Rol e_rol)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "insert into roles (rol,estado) values ('" + e_rol.rol + "','HABILITADO');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto rol id  " + e_rol.id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Rol", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Rol", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Rol", e);
                }
                cnn.Close();
            }
        } //Funcion insertar un nuevo rol
        public int modificar(E_Rol e_rol, String aux)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                int num_rows = -1;
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.CommandText = "update roles set rol='" + e_rol.rol + "', estado='" + e_rol.estado + "' where rol='" + aux + "';SELECT Scope_Identity();";
                    num_rows=cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se modifico rol " + aux + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Rol", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Rol", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Rol", e);
                }
            }
                
            // todas las funciones  de abm y otras se ejecutan con la propiedades y el login q pasamos por constructor 
        } //Funcion para modificar algun campo del rol
        public int baja(E_Rol e_rol)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                int num_rows = -1;
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "update roles set estado='DESHABILITADO' where rol='" + e_rol.rol + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se dio de baja rol" + e_rol.rol + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Rol", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Rol", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Rol", e);
                }
            }
        } //Funcion para dar de baja un usuario
        public E_Rol retornaRol(string rol)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    E_Rol obRol = new E_Rol();
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select id,rol,estado from roles where rol = '" + rol + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        obRol.id = Convert.ToInt32(dr[0]);
                        obRol.rol = dr[1].ToString();
                        obRol.estado = dr[2].ToString();
                    }
                    cnn.Close();
                    return obRol;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar Rol", e);
                }
            }
                
        } //Retorna objeto del tipo rol
        public DataTable listar_roles()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter("Select rol as 'Rol',estado as 'Estado' from roles", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar roles", e);
                }
            }
                
        }  //Carga todos los datos de la BD a un DataTable
        public DataView busca_rol(string rol)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "rol like '" + rol + "%'";
                    return dv;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar rol", e);
                }
            }
        }  //Busca roles
        public int retornaID_rol(string rol)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int retorno = -1;
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select id from roles where rol='" + rol + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        retorno = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                    return retorno;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID rol", e);
                }
            }
        } //Funcion que retorna el ID de rol segun sea su nombre

        public int cantidad_usuarios_rol(string rol)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int retorno = -1;
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select count(r.id) " +
                                      $"from roles r join usuarios u on r.id=u.id_rol " +
                                      $"where r.rol like '{rol}' " +
                                      $"group by u.id_rol";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        retorno = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                    return retorno;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retorar usuarios de un Rol", e);
                }
            }

        }
    }
}
