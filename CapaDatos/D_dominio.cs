﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
   public class D_Dominio
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;

        string usuario;
        public D_Dominio(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            
        }
        public int retornaId(string dominio)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    int retorno = -1;
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = $"select id as 'ID' " +
                                      $" from dominio where dominio like '{dominio}' ";
                    
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            retorno = Convert.ToInt32(dr[0].ToString());
                        }
                        

                    }
                    cnn.Close();
                    return retorno;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID dominio", e);
                }
            }
        }
        public string retornaDominio(int id)
        { 
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string respuesta = String.Empty;
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select dominio " +
                                        $" from dominio where id like '{id}' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            respuesta = dr[0].ToString();
                        }
                    }
                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar Dominio", e);
                }
            }   

        }
    }
}
