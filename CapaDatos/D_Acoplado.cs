﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
    public class D_Acoplado
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Acoplado e_acoplado;
        string usuario;
        public D_Acoplado(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_acoplado = new E_Acoplado();
        }

        #region funciones en uso
        public void insertar(E_Acoplado e_acoplado, string dominio)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    /*********** se inserta el DOMINIO en la tabla dominio  ***********/
                    cmd.CommandText = "insert into dominio values ('" + dominio + "');SELECT Scope_Identity();";
                    int dominio_insertado = Convert.ToInt32(cmd.ExecuteScalar());
                    /*********** se inserta el acoplado en la tabla acoplado***********/
                    cmd.CommandText = "insert into acoplado values ('" + dominio_insertado + "','" + e_acoplado.Marca + "','" + e_acoplado.Modelo + "','" + e_acoplado.Año + "','" + e_acoplado.Tipo_carga + "','" + e_acoplado.Tipo_acoplado + "'," +
                        " '" + e_acoplado.Tara.ToString().Replace(',', '.') + "','" + e_acoplado.Capacidad_carga.ToString().Replace(',', '.') + "','" + e_acoplado.Volumen.ToString().Replace(',', '.') + "','" + e_acoplado.Km_unidad.ToString().Replace(',', '.') + "'," +
                        " '" + e_acoplado.Km_service.ToString().Replace(',', '.') + "','" + e_acoplado.Km_cambio_neumaticos.ToString().Replace(',', '.') + "','" + e_acoplado.Km_service.ToString().Replace(',', '.') + "'," +
                        " '" + e_acoplado.Km_cambio_neumaticos.ToString().Replace(',', '.') + "','" + e_acoplado.Altura.ToString().Replace(',', '.') + "'," +
                        " '" + e_acoplado.Ancho_interior.ToString().Replace(',', '.') + "','" + e_acoplado.Ancho_exterior.ToString().Replace(',', '.') + "','" + e_acoplado.Longitud.ToString().Replace(',', '.') + "','" + e_acoplado.Fecha_alta.ToShortDateString() + "','HABILITADO')";
                    cmd.ExecuteNonQuery();
                    /*********** se inserta auditoria***********/
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto acoplado con Dominio " + dominio + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar unidad", e);
                        default: throw new System.Exception("Ups, algo paso al insertar unidad", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar unidad", e);
                }
                cnn.Close();
            }
        }
        public int baja(int id_dominio)
        {
            SqlTransaction sqlTran;
            int num_rows = -1;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    /*********** se hace un borrado logico marcando el estado de la unidad  como DESHABILITADO  ***********/
                    cmd.CommandText = "update acoplado set estado='DESHABILITADO' where id_dominio = '" + id_dominio + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    /***********se inserta auditoria ***********/
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se dio de baja al acoplado con Dominio " + id_dominio + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar unidad", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar unidad", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar unidad", e);
                }
                cnn.Close();
            }
            return num_rows;
        }
        public int modificar(E_Acoplado e_acoplado, string dominio, string motivo)
        {   // estas variables se utilizan para comprobar la correcta insercion en todas las tablas, si no, rollback
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    /*********** se actualiza el DOMINIO en la tabla dominio  ***********/
                    if(e_acoplado.Id_Dominio > 0 )
                    {
                        cmd.CommandText = "update dominio set dominio='" + dominio + "' where id='" + e_acoplado.Id_Dominio + "';SELECT Scope_Identity();";
                        cmd.ExecuteNonQuery();
                    }
                    /*************se actualiza acoplado*****************/
                    cmd.CommandText = "update acoplado set marca='" + e_acoplado.Marca + "' , modelo='" + e_acoplado.Modelo + "' , año = '" + e_acoplado.Año + "' , tipo_carga= '" + e_acoplado.Tipo_carga + "', tipo_acoplado= '" + e_acoplado.Tipo_acoplado + "', tara ='" + e_acoplado.Tara.ToString().Replace(',', '.') + "' , capacidad_carga='" + e_acoplado.Capacidad_carga.ToString().Replace(',', '.') + "' ,Volumen='" + e_acoplado.Volumen.ToString().Replace(',', '.') + "' ,  km_unidad = '" + e_acoplado.Km_unidad.ToString().Replace(',', '.') + "' , km_service = '" + e_acoplado.Km_service.ToString().Replace(',', '.') + "', km_cambio_neumaticos = '" + e_acoplado.Km_cambio_neumaticos.ToString().Replace(',', '.') + "', km_service_suma = '" + e_acoplado.Km_service_suma.ToString().Replace(',', '.') + "', km_cambio_neumaticos_suma = '" + e_acoplado.Km_cambio_neumaticos_suma.ToString().Replace(',', '.') + "' , altura='" + e_acoplado.Altura.ToString().Replace(',', '.') + "' , ancho_interior='" + e_acoplado.Ancho_interior.ToString().Replace(',', '.') + "' ,  ancho_exterior = '" + e_acoplado.Ancho_exterior.ToString().Replace(',', '.') + "' , longitud='" + e_acoplado.Longitud.ToString().Replace(',', '.') + "' ,fecha_alta='" + e_acoplado.Fecha_alta.ToShortDateString() + "' , estado='" + e_acoplado.Estado + "' where id_dominio = '" + e_acoplado.Id_Dominio + "'";
                    num_rows = cmd.ExecuteNonQuery();
                    /*************se inserta auditoria*****************/
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se actualizo el acoplado con Dominio " + dominio + ", motivo: " + motivo + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();

                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar unidad", e);
                        default: throw new System.Exception("Ups, algo paso al modificar unidad", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar unidad", e);
                }
                cnn.Close();
            }
            return num_rows;
        }
        public E_Acoplado retornaAcoplado(int id_dominio)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select id_dominio, marca, modelo, año, tipo_carga, tipo_acoplado," +
                                      $" tara, capacidad_carga, volumen, km_unidad, km_service, km_cambio_neumaticos, km_service_suma, km_cambio_neumaticos_suma," +
                                      $" altura, ancho_interior, ancho_exterior, longitud, fecha_alta, estado" +
                                      $" from acoplado a join dominio d on a.id_dominio=d.id where a.id_dominio = '{id_dominio}' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            e_acoplado.Id_Dominio = Convert.ToInt32(dr[0].ToString());
                            e_acoplado.Marca = Convert.ToInt32(dr[1].ToString());
                            e_acoplado.Modelo = Convert.ToInt32(dr[2].ToString());
                            e_acoplado.Año = Convert.ToInt32(dr[3].ToString());
                            e_acoplado.Tipo_carga = Convert.ToInt32(dr[4].ToString());
                            e_acoplado.Tipo_acoplado = dr[5].ToString();
                            e_acoplado.Tara = float.Parse(dr[6].ToString());
                            e_acoplado.Capacidad_carga = float.Parse(dr[7].ToString());
                            e_acoplado.Volumen = float.Parse(dr[8].ToString());
                            e_acoplado.Km_unidad = float.Parse(dr[9].ToString());
                            e_acoplado.Km_service = float.Parse(dr[10].ToString());
                            e_acoplado.Km_cambio_neumaticos = float.Parse(dr[11].ToString());
                            e_acoplado.Km_service_suma = float.Parse(dr[12].ToString());
                            e_acoplado.Km_cambio_neumaticos_suma = float.Parse(dr[13].ToString());
                            e_acoplado.Altura = float.Parse(dr[14].ToString());
                            e_acoplado.Ancho_interior = float.Parse(dr[15].ToString());
                            e_acoplado.Ancho_exterior = float.Parse(dr[16].ToString());
                            e_acoplado.Longitud = float.Parse(dr[17].ToString());
                            e_acoplado.Fecha_alta = Convert.ToDateTime(dr[18].ToString());
                            e_acoplado.Estado = dr[19].ToString();
                        }
                    }
                    else
                    {
                        e_acoplado = null;
                    }

                    cnn.Close();
                    return e_acoplado;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar acoplado", e);
                }
            }
               
        } //Funcion que retorna un objeto acoplado completo
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    cnn.Open();
                    da = new SqlDataAdapter($"select a.id_dominio 'ID Dominio', d.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',a.año 'Año',t.tipo 'Tipo de carga'," +
                                            $"a.tipo_acoplado 'Tipo de acoplado',a.tara 'Tara',a.capacidad_carga 'Capacidad de carga'," +
                                            $"a.volumen 'Volumen',a.km_unidad 'Km unidad',a.km_service 'Km service',a.km_cambio_neumaticos 'Km neumaticos'," +
                                            $"a.km_service_suma 'Km service suma',a.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',a.altura 'Altura'," +
                                            $"a.ancho_interior  'Ancho interior',a.ancho_exterior 'Ancho exterior' ,a.longitud  'Longitud'," +
                                            $"a.fecha_alta 'Fecha de alta',a.estado 'Estado' " +
                                            $"from acoplado a join tipo_carga t on a.tipo_carga=t.id " +
                                            $" join marca_acoplado ma on a.marca=ma.id " +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join dominio d on a.id_dominio=d.id" , cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los acoplados ", e);
                }
            }   
        } //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter($"select a.id_dominio 'ID Dominio', d.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',a.año 'Año',t.tipo 'Tipo de carga'," +
                                            $"a.tipo_acoplado 'Tipo de acoplado',a.tara 'Tara',a.capacidad_carga 'Capacidad de carga'," +
                                            $"a.volumen 'Volumen',a.km_unidad 'Km unidad',a.km_service 'Km service',a.km_cambio_neumaticos 'Km neumaticos'," +
                                            $"a.km_service_suma 'Km service suma',a.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',a.altura 'Altura'," +
                                            $"a.ancho_interior  'Ancho interior',a.ancho_exterior 'Ancho exterior' ,a.longitud  'Longitud'," +
                                            $"a.fecha_alta 'Fecha de alta',a.estado 'Estado' " +
                                            $"from acoplado a join tipo_carga t on a.tipo_carga=t.id " +
                                            $" join marca_acoplado ma on a.marca=ma.id " +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join dominio d on a.id_dominio=d.id" +
                                            $" where a.estado='HABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los acoplados habilitados ", e);
                }
            }
               
        } //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {

                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter($"select a.id_dominio 'ID Dominio', d.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',a.año 'Año',t.tipo 'Tipo de carga'," +
                                            $"a.tipo_acoplado 'Tipo de acoplado',a.tara 'Tara',a.capacidad_carga 'Capacidad de carga'," +
                                            $"a.volumen 'Volumen',a.km_unidad 'Km unidad',a.km_service 'Km service',a.km_cambio_neumaticos 'Km neumaticos'," +
                                            $"a.km_service_suma 'Km service suma',a.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',a.altura 'Altura'," +
                                            $"a.ancho_interior  'Ancho interior',a.ancho_exterior 'Ancho exterior' ,a.longitud  'Longitud'," +
                                            $"a.fecha_alta 'Fecha de alta',a.estado 'Estado' " +
                                            $"from acoplado a join tipo_carga t on a.tipo_carga=t.id " +
                                            $" join marca_acoplado ma on a.marca=ma.id " +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join dominio d on a.id_dominio=d.id" +
                                            $" where a.estado='DESHABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los acoplados deshabilitados ", e);
                }
            }
        } //Funcion que carga a un DataView todo los datos que hay en el DataTable
        public DataTable listarPorFechaAltaHabilitados(string desde, string hasta)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter($"select a.id_dominio 'ID Dominio', d.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',a.año 'Año',t.tipo 'Tipo de carga'," +
                                            $"a.tipo_acoplado 'Tipo de acoplado',a.tara 'Tara',a.capacidad_carga 'Capacidad de carga'," +
                                            $"a.volumen 'Volumen',a.km_unidad 'Km unidad',a.km_service 'Km service',a.km_cambio_neumaticos 'Km neumaticos'," +
                                            $"a.km_service_suma 'Km service suma',a.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',a.altura 'Altura'," +
                                            $"a.ancho_interior  'Ancho interior',a.ancho_exterior 'Ancho exterior' ,a.longitud  'Longitud'," +
                                            $"a.fecha_alta 'Fecha de alta',a.estado 'Estado' " +
                                            $"from acoplado a join tipo_carga t on a.tipo_carga=t.id " +
                                            $" join marca_acoplado ma on a.marca=ma.id " +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join dominio d on a.id_dominio=d.id" +
                                            $" where estado='HABILITADO' and TRY_PARSE(fecha_alta as datetime) between '" + desde + "' and '" + hasta + "'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los acoplados por fecha ", e);
                }
            }
        }
        public DataTable busquedaPorEntradaDeTexto(string palabra)//busca por dominio ,chasis marca o tipo de acoplado
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter($"select a.id_dominio 'ID Dominio', d.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',a.año 'Año',t.tipo 'Tipo de carga'," +
                                            $"a.tipo_acoplado 'Tipo de acoplado',a.tara 'Tara',a.capacidad_carga 'Capacidad de carga'," +
                                            $"a.volumen 'Volumen',a.km_unidad 'Km unidad',a.km_service 'Km service',a.km_cambio_neumaticos 'Km neumaticos'," +
                                            $"a.km_service_suma 'Km service suma',a.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',a.altura 'Altura'," +
                                            $"a.ancho_interior  'Ancho interior',a.ancho_exterior 'Ancho exterior' ,a.longitud  'Longitud'," +
                                            $"a.fecha_alta 'Fecha de alta',a.estado 'Estado' " +
                                            $"from acoplado a join tipo_carga t on a.tipo_carga=t.id " +
                                            $" join marca_acoplado ma on a.marca=ma.id " +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join dominio d on a.id_dominio=d.id" +
                                            $" where d.dominio like '%{palabra}%'  or ma.marca like '%{palabra}%' or a.tipo_acoplado like '%{palabra}%' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al realizar la busqueda", e);
                }
            }
                
        }
        public DataTable busquedaPorRangoDeFechaDeAlta(string desde, string hasta)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter($"select a.id_dominio 'ID Dominio', d.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',a.año 'Año',t.tipo 'Tipo de carga'," +
                                            $"a.tipo_acoplado 'Tipo de acoplado',a.tara 'Tara',a.capacidad_carga 'Capacidad de carga'," +
                                            $"a.volumen 'Volumen',a.km_unidad 'Km unidad',a.km_service 'Km service',a.km_cambio_neumaticos 'Km neumaticos'," +
                                            $"a.km_service_suma 'Km service suma',a.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',a.altura 'Altura'," +
                                            $"a.ancho_interior  'Ancho interior',a.ancho_exterior 'Ancho exterior' ,a.longitud  'Longitud'," +
                                            $"a.fecha_alta 'Fecha de alta',a.estado 'Estado' " +
                                            $"from acoplado a join tipo_carga t on a.tipo_carga=t.id " +
                                            $" join marca_acoplado ma on a.marca=ma.id " +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join dominio d on a.id_dominio=d.id" +
                                            $" where r.fecha_alta between TRY_PARSE('{desde}' as datetime)  and TRY_PARSE('{hasta}' as datetime) ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar por fecha ", e);
                }
            }
           
        } //Funcion filtra busqueda por fecha alta
        public DataTable listar_remolque_semiremolque_pedido(float km_viaje, float volumen, float altura, float ancho_int, float ancho_ext, float longitud, string tipo_carga, string tipo_acoplado, DateTime fecha_salida, DateTime fecha_regreso)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();

                    da = new SqlDataAdapter($"select distinct(a.id_dominio) as 'ID Dominio' , " +
                                            $"d.dominio as 'Dominio'," +
                                            $"ma.marca as 'Marca'," +
                                            $"mo.modelo as 'Modelo'," +
                                            $"a.año as 'Año'," +
                                            $"t.tipo as 'Tipo carga', " +
                                            $"a.tipo_acoplado as 'Tipo Acoplado'," +
                                            $"a.tara as 'Tara'," +
                                            $"a.capacidad_carga 'Capacidad carga' ," +
                                            $"a.volumen as 'Volumen'," +
                                            $"a.km_unidad as 'KM Unidad'," +
                                            $"a.km_service as 'KM Service'," +
                                            $"a.km_cambio_neumaticos as 'KM Cambio Neumaticos'," +
                                            $"a.km_service_suma as 'KM Service Suma'," +
                                            $"a.km_cambio_neumaticos_suma as 'KM Cambio Neumaticos Suma'," +
                                            $"a.altura as 'Altura'," +
                                            $"a.ancho_interior as 'Ancho interior'," +
                                            $"a.ancho_exterior as 'Ancho exterior', " +
                                            $"a.longitud as 'Longitud'," +
                                            $"a.fecha_alta as 'Fecha Alta'," +
                                            $"a.estado as 'Estado'" +


                                            $"from acoplado a join dominio d on a.id_dominio=d.id" +
                                            $" join marca_acoplado ma on a.marca=ma.id" +
                                            $" join modelo_acoplado mo on a.modelo=mo.id" +
                                            $" join tipo_carga t on a.tipo_carga=t.id" +
                                            $" left join unidades_viaje uv on a.id_dominio=uv.id_acoplado" +
                                            $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                            $" left join pedido p on p.id_pedido=v.id_pedido " +
                                            $" left join taller ta on a.id_dominio=ta.id_unidad " +


                                            $" where (a.km_cambio_neumaticos_suma) - { km_viaje.ToString().Replace(',', '.')  } >= 0 " +
                                            $" and a.km_service_suma - { km_viaje.ToString().Replace(',', '.')  } >=  0" +
                                            $" and a.volumen >=  { volumen.ToString().Replace(',', '.') }  and t.tipo = '{ tipo_carga }' and a.tipo_acoplado = '{ tipo_acoplado }' and a.altura >=  { altura.ToString().Replace(',', '.') } " +
                                            $" and a.ancho_interior >=  { ancho_int.ToString().Replace(',', '.') }  and a.ancho_exterior >=  { ancho_ext.ToString().Replace(',', '.') } " +
                                            $" and a.longitud >=  { longitud.ToString().Replace(',', '.') } and ( (ta.estado NOT LIKE 'POR_REVISAR' AND ta.estado NOT LIKE 'EN_PROCESO' ) or ta.estado is null)" +
                                            $" and ( (v.estado NOT LIKE 'EN CURSO' and v.estado NOT LIKE 'PENDIENTE' and v.estado NOT LIKE 'SUSPENDIDO' ) or v.estado is null)" +
                                            $" and a.estado = 'HABILITADO' " +
                                            $" and a.id_dominio not in (select distinct(a.id_dominio)" +
                                                                     $"from acoplado a join dominio d on a.id_dominio=d.id" +
                                                                     $" join marca_acoplado ma on a.marca=ma.id" +
                                                                     $" join modelo_acoplado mo on a.modelo=mo.id" +
                                                                     $" join tipo_carga t on a.tipo_carga=t.id" +
                                                                     $" left join unidades_viaje uv on a.id_dominio=uv.id_acoplado" +
                                                                     $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                                                     $" left join pedido p on p.id_pedido=v.id_pedido " +
                                                                     $" left join taller ta on a.id_dominio=ta.id_unidad " +
                                                                    $" where (p.fecha_salida between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}'" +
                                                                    $" or p.fecha_regreso between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}') and" +
                                                                    $" (v.estado like 'EN CURSO' or v.estado like 'PENDIENTE') and p.estado like 'CARGADO')", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar sugerencias sobre acoplado ", e);
                }
            }
                
        } //Funcion que lista todos los remolques y semiremolques para pedidos

        #endregion
        /****La siguente funcion podra listar todos los camiones inclusive los que estan Inhabilitados (esta la utilizara el admin)*/
    }
}
