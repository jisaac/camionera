﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
    public class D_MarcaCamion
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_MarcaCamion e_marcaCamion;
        string usuario;
        public D_MarcaCamion(string usuario)
        {
            
            this.usuario = usuario;
            e_marcaCamion = new E_MarcaCamion();
        }
        public void insertar(E_MarcaCamion e_marcaCamion)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta la nueva marca_camion en la tabla marca_camion**************/
                    cmd.CommandText = "insert into marca_camion values ('" + e_marcaCamion.Marca + "','HABILITADO');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto la Marca camion id=" + e_marcaCamion.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Marca Camion", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Marca Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Marca Camion", e);
                }
                cnn.Close();
            }
         }
        public int baja(E_MarcaCamion e_marcaCamion)
        {
            int num_rows = -1; 
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja la marca de camion,se marca el estado como DESHABILITADO**********/
                    cmd.CommandText = "delete from marca_camion where  id='"+e_marcaCamion.Id+ "'; SELECT Scope_Identity(); ";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja a la Marca Camion id=" + e_marcaCamion.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar  Marca Camion", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar  Marca Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar  Marca Camion", e);
                }
            }       
        }
        public int modificar(E_MarcaCamion e_marcaCamion)
        {
            int num_rows;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza la marca de camion**********/
                    cmd.CommandText = "update  marca_camion set estado= '"+ e_marcaCamion.Estado+ "' ,marca='"+ e_marcaCamion.Marca+"' where  id='" + e_marcaCamion.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó a la Marca Camion id=" + e_marcaCamion.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Marca Camion", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Marca Camion", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Marca Camion", e);
                }
            }
        }
        public E_MarcaCamion retornaMarcaCamion(int id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id,marca,estado from marca_camion where id = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_marcaCamion.Id = Convert.ToInt32(dr[0].ToString());
                        e_marcaCamion.Marca = dr[1].ToString();
                        e_marcaCamion.Estado = dr[2].ToString();

                    }
                    cnn.Close();
                    return e_marcaCamion;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar marca de camion", e);
                }
            }
                
        } //Funcion que retorna un objeto marcaCamion completo
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,marca as 'Marca',estado from marca_camion ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos las Marcas de Camiones", e);
                }
            }
           
        } 
        public List<String> listarComboBox()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    List<String> lista = new List<string>();
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("Select marca From marca_camion ", cnn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        lista.Add(dr[0].ToString());
                    }
                    cnn.Close();
                    return lista;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al devolver listado de combobox", e);
                }
            }
        }
        public bool existe(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                bool respuesta = false;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select * from marca_camion where marca ='" + name + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        respuesta = true;
                    }
                    else
                    {
                        respuesta = false;
                    }

                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar si existe marca", e);
                }
            }
        }
        public string retornaId(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string respuesta = null;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id from marca_camion where marca = '" + name + "'  ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        respuesta = dr[0].ToString();

                    }
                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID Marca Camion", e);
                }
            }
        }

    }


}

