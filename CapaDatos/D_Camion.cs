﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
    public class D_Camion
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Rigido e_rigido;
        E_Tractocamion e_tractocamion;
        string usuario;
        public D_Camion(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_rigido = new E_Rigido();
            e_tractocamion = new E_Tractocamion();
        }
        #region funciones en uso

        public void insertar(Object tipoCamion, string dominio)
        {
            // estas variables se utilizan para comprobar la correcta insercion en todas las tablas, si no, rollback
            SqlTransaction sqlTran;
            if (tipoCamion is E_Rigido)
            {
                e_rigido = (E_Rigido)tipoCamion;
                /*************SE CONTROLA QUE NO EXISTAN EL NUMERO DE CHASIS*****************/

                using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
                {
                    cnn.Open();
                    sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = cnn;
                        cmd.Transaction = sqlTran;
                        cmd.CommandType = CommandType.Text;
                        /*********** se inserta el DOMINIO en la tabla dominio  ***********/
                        cmd.CommandText = "insert into dominio values ('" + dominio + "');SELECT Scope_Identity();";
                        int id_insertado = Convert.ToInt32(cmd.ExecuteScalar());//controla si se inserto o no el dominio
                                                                 /*********** se inserta el camion en la tabla camion***********/
                        cmd.CommandText = "insert into camion values ('" + id_insertado + "','" + e_rigido.Marca + "','" + e_rigido.Modelo + "','" + e_rigido.Año + "','" + e_rigido.Tara.ToString().Replace(',', '.') + "','" + e_rigido.Km_unidad.ToString().Replace(',', '.') + "'," +
                            " '" + e_rigido.Km_service.ToString().Replace(',', '.') + "','" + e_rigido.Km_cambio_neumaticos.ToString().Replace(',', '.') + "','" + e_rigido.Km_service.ToString().Replace(',', '.') + "','" + e_rigido.Km_cambio_neumaticos.ToString().Replace(',', '.') + "'," +
                            " '" + e_rigido.Fecha_alta.ToShortDateString() + "','HABILITADO')";
                        cmd.ExecuteNonQuery();
                        /*********** se inserta el camion rigido ***********/
                        cmd.CommandText = "insert into rigido values ('" + id_insertado + "','" + e_rigido.Tipo_carga + "','" + e_rigido.Altura.ToString().Replace(',', '.') + "','" + e_rigido.Ancho_interior.ToString().Replace(',', '.') + "'," +
                            " '" + e_rigido.Ancho_exterior.ToString().Replace(',', '.') + "','" + e_rigido.Longitud.ToString().Replace(',', '.') + "','" + e_rigido.Volumen.ToString().Replace(',', '.') + "'," +
                            " '" + e_rigido.Capacidad_carga.ToString().Replace(',', '.') + "'," + Convert.ToInt32(e_rigido.Enganche) + ")";
                        cmd.ExecuteNonQuery();
                        /*********** se inserta en auditoria ***********/
                        cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto camion rigido con Dominio " + dominio + "')";
                        cmd.ExecuteNonQuery();
                        sqlTran.Commit();
                    }
                    catch (SqlException e)
                    {
                        SqlError err = e.Errors[0];
                        sqlTran.Rollback();
                        cnn.Close();
                        switch (err.Number)
                        {
                            case 2627: throw new System.Exception("No es posible Insertar unidad", e);
                            default: throw new System.Exception("Ups, algo paso al insertar unidad", e);
                        }
                    }
                    catch (Exception e)
                    {
                        sqlTran.Rollback();
                        cnn.Close();
                        throw new System.Exception("Ups, algo paso al insertar unidad", e);
                    }
                    cnn.Close();
                }
            }
            else
            {

                e_tractocamion = (E_Tractocamion)tipoCamion;
                /*************SE CONTROLA QUE NO EXISTA EL NUMERO DE CHASIS*****************/
                using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
                {
                    cnn.Open();
                    sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                    try
                    {
                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = cnn;
                        cmd.Transaction = sqlTran;
                        cmd.CommandType = CommandType.Text;
                        /*********** se inserta el DOMINIO en la tabla dominio  ***********/
                        cmd.CommandText = $"insert into dominio values ('{dominio}');SELECT Scope_Identity();";
                        int ultimo_insertado = Convert.ToInt32(cmd.ExecuteScalar());
                        /*********** se inserta el camion en la tabla camion***********/
                        cmd.CommandText = "insert into camion values ('" + ultimo_insertado + "','" + e_tractocamion.Marca + "','" + e_tractocamion.Modelo + "'," + e_tractocamion.Año + "," + e_tractocamion.Tara.ToString().Replace(',', '.') + "," + e_tractocamion.Km_unidad.ToString().Replace(',', '.') + "," + e_tractocamion.Km_service.ToString().Replace(',', '.') + "," + e_tractocamion.Km_cambio_neumaticos.ToString().Replace(',', '.') + "," + e_tractocamion.Km_service.ToString().Replace(',', '.') + "," + e_tractocamion.Km_cambio_neumaticos.ToString().Replace(',', '.') + ",'" + e_tractocamion.Fecha_alta.ToShortDateString() + "','HABILITADO')";
                        cmd.ExecuteNonQuery();
                        /*********** se inserta el tractocamion en tabla tractocamion***********/
                        cmd.CommandText = "insert into tractocamion values ('" + ultimo_insertado + "')";
                        cmd.CommandType = CommandType.Text;
                        cmd.ExecuteNonQuery();
                        /*********** se inserta la auditoria***********/
                        cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto camion tractocamion con Dominio " + dominio + "')";
                        cmd.ExecuteNonQuery();
                        sqlTran.Commit();
                    }
                    catch (SqlException e)
                    {
                        SqlError err = e.Errors[0];
                        sqlTran.Rollback();
                        cnn.Close();
                        switch (err.Number)
                        {
                            case 2627: throw new System.Exception("No es posible insertar unidad dado que ya existe ese dominio registrado en el sistema", e);
                            default: throw new System.Exception("Ups, algo paso al insertar unidad", e);
                        }
                    }
                    catch (Exception e)
                    {
                        sqlTran.Rollback();
                        cnn.Close();
                        throw new System.Exception("Ups, algo paso al insertar unidad", e);
                    }
                    cnn.Close();
                }
            }
        }
        public int baja(int id_dominio)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    /*********** se hace un borrado logico marcando el estado de la unidad  como DESHABILITADO  ***********/
                    cmd.CommandText = "update camion set estado='DESHABILITADO' where id_dominio = '" + id_dominio + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    /*********** se inserta la auditoria ***********/
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se dio de baja al camion rigido con id dominio " + id_dominio + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar unidad", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar unidad", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar unidad", e);
                }
                cnn.Close();
            }
            return num_rows;

        }
        public int modificar(Object tipoCamion, string dominio, string motivo)///el string dominio se utiliza para identificar el dominio en la actualizacion,ya que este puede ser modificado
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            if (tipoCamion is E_Rigido)
            {
                e_rigido = (E_Rigido)tipoCamion;
                using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
                {
                    cnn.Open();
                    sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                    try
                    {



                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = cnn;
                        cmd.Transaction = sqlTran;
                        cmd.CommandType = CommandType.Text;
                        if (e_rigido.Id_Dominio > 0)
                        {
                            /*********** se actualiza el DOMINIO en la tabla dominio  ***********/
                            cmd.CommandText = "update dominio set dominio='" + dominio + "' where id='" + e_rigido.Id_Dominio + "';SELECT Scope_Identity();";
                            cmd.ExecuteNonQuery();
                        }
                        /************se actualiza camion*****************************/
                        cmd.CommandText = "update camion set  marca='" + e_rigido.Marca + "' , modelo='" + e_rigido.Modelo + "', año = '" + e_rigido.Año + "' , tara ='" + e_rigido.Tara.ToString().Replace(',', '.') + "' ,  km_unidad = '" + e_rigido.Km_unidad.ToString().Replace(',', '.') + "' , km_service = '" + e_rigido.Km_service.ToString().Replace(',', '.') + "' , km_cambio_neumaticos = '" + e_rigido.Km_cambio_neumaticos.ToString().Replace(',', '.') + "' , km_service_suma = '" + e_rigido.Km_service_suma.ToString().Replace(',', '.') + "' , km_cambio_neumaticos_suma = '" + e_rigido.Km_cambio_neumaticos_suma.ToString().Replace(',', '.') + "', fecha_alta='" + e_rigido.Fecha_alta.ToShortDateString() + "' , estado='" + e_rigido.Estado + "' where id_dominio = '" + e_rigido.Id_Dominio + "'";
                        cmd.ExecuteNonQuery();
                        /************se actualiza tabla rigido*****************************/
                        cmd.CommandText = "update rigido set tipo_carga='" + e_rigido.Tipo_carga + "' , altura='" + e_rigido.Altura.ToString().Replace(',', '.') + "' , ancho_interior='" + e_rigido.Ancho_interior.ToString().Replace(',', '.') + "' , ancho_exterior='" + e_rigido.Ancho_exterior.ToString().Replace(',', '.') + "' , longitud= '" + e_rigido.Longitud.ToString().Replace(',', '.') + "', volumen= '" + e_rigido.Volumen.ToString().Replace(',', '.') + "', capacidad_carga= '" + e_rigido.Capacidad_carga.ToString().Replace(',', '.') + "', enganche= '" + e_rigido.Enganche + "' where id_dominio = '" + e_rigido.Id_Dominio + "' ";
                        num_rows = cmd.ExecuteNonQuery();
                        /************se INSERTA la auditoria*****************************/
                        cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se actualizo el camion rigido con Dominio " + dominio + ", motivo: " + motivo + "')";
                        cmd.ExecuteNonQuery();
                        sqlTran.Commit();

                    }
                    catch (SqlException e)
                    {
                        SqlError err = e.Errors[0];
                        sqlTran.Rollback();
                        cnn.Close();
                        switch (err.Number)
                        {
                            case 547: throw new System.Exception("No es posible modificar unidad", e);
                            default: throw new System.Exception("Ups, algo paso al modificar unidad", e);
                        }
                    }
                    catch (Exception e)
                    {
                        sqlTran.Rollback();
                        cnn.Close();
                        throw new System.Exception("Ups, algo paso al modificar unidad", e);
                    }
                    cnn.Close();
                }
                return num_rows;
            }
            else if (tipoCamion is E_Tractocamion) /// Modifica camion rigido
            {
                e_tractocamion = (E_Tractocamion)tipoCamion;
                /*************SE CONTROLA QUE NO EXISTA EL NUMERO DE CHASIS*****************/
                using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
                {
                    cnn.Open();
                    sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                    try
                    {

                        SqlCommand cmd = new SqlCommand();
                        cmd.Connection = cnn;
                        cmd.Transaction = sqlTran;
                        cmd.CommandType = CommandType.Text;
                        if (e_tractocamion.Id_Dominio > 0)
                        {
                            /*********** se actualiza el DOMINIO en la tabla dominio  ***********/
                            cmd.CommandText = "update dominio set dominio='" + dominio + "' where id='" + e_tractocamion.Id_Dominio + "';SELECT Scope_Identity();";
                            num_rows = cmd.ExecuteNonQuery();
                        }
                        /************se actualiza camion y tractocamion ya que solo se guarda el id y este se actualiza en cascade*****************************/
                        cmd.CommandText = "update camion set  marca='" + e_tractocamion.Marca + "' , modelo='" + e_tractocamion.Modelo + "', año = '" + e_tractocamion.Año + "' , tara ='" + e_tractocamion.Tara.ToString().Replace(',', '.') + "' ,  km_unidad = '" + e_tractocamion.Km_unidad.ToString().Replace(',', '.') + "' , km_service = '" + e_tractocamion.Km_service.ToString().Replace(',', '.') + "' , km_cambio_neumaticos = '" + e_tractocamion.Km_cambio_neumaticos.ToString().Replace(',', '.') + "' , km_service_suma = '" + e_tractocamion.Km_service_suma.ToString().Replace(',', '.') + "' , km_cambio_neumaticos_suma = '" + e_tractocamion.Km_cambio_neumaticos_suma.ToString().Replace(',', '.') + "' ,fecha_alta='" + e_tractocamion.Fecha_alta + "' , estado='" + e_tractocamion.Estado + "' where id_dominio = '" + e_tractocamion.Id_Dominio + "'";
                        cmd.ExecuteNonQuery();

                        /*********** se inserta auditoria ***********/
                        cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se actualizo el tractocamion con ID Dominio " + e_tractocamion.Id_Dominio + "')";
                        cmd.ExecuteNonQuery();
                        sqlTran.Commit();

                    }
                    catch (SqlException e)
                    {
                        SqlError err = e.Errors[0];
                        sqlTran.Rollback();
                        cnn.Close();
                        switch (err.Number)
                        {
                            case 547: throw new System.Exception("No es posible modificar unidad", e);
                            default: throw new System.Exception("Ups, algo paso al modificar unidad", e);
                        }
                    }
                    catch (Exception e)
                    {
                        sqlTran.Rollback();
                        cnn.Close();
                        throw new System.Exception("Ups, algo paso al modificar unidad", e);
                    }
                    cnn.Close();
                }
                return num_rows;
            }
            return -1;
        }
        public Object RetornaCamion(int id_dominio)//Utiliza para retornar cualquier tipo de camion
        {//Como trabajamos con herencia podemos traer por eejemplo "camion rigido" y como hereda de CAMION podemos darle los atributos de la clase propia y de la heredada,por eso se crea esta funcion 
         // que sirve para consultar cualquier tipo de camion ,siempre retorna un "Object" que puede ser rigido o tracto bso

            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    SqlDataReader dr;
                    cnn.Open();

                    cmd.Connection = cnn;
                    cmd.CommandText = $"select c.id_dominio, c.marca, c.modelo, c.año, c.tara, c.km_unidad, c.km_service, c.km_cambio_neumaticos," +
                                      $" c.km_service_suma, c.km_cambio_neumaticos_suma, c.fecha_alta, c.estado, ri.tipo_carga , ri.altura, ri.ancho_interior," +
                                      $" ri.ancho_exterior ,ri.longitud, ri.volumen, ri.capacidad_carga, ri.enganche " +
                                      $" from camion c join dominio d on c.id_dominio=d.id" +
                                      $" join marca_camion ma on c.marca=ma.id " +
                                      $" join modelo_camion mo on c.modelo=mo.id  join rigido ri on ri.id_dominio=c.id_dominio where c.id_dominio = '{id_dominio}'";


                    dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            e_rigido.Id_Dominio = Convert.ToInt32(dr[0].ToString());
                            e_rigido.Marca = Convert.ToInt32(dr[1].ToString());
                            e_rigido.Modelo = Convert.ToInt32(dr[2].ToString());
                            e_rigido.Año = Convert.ToInt32(dr[3].ToString());
                            e_rigido.Tara = float.Parse(dr[4].ToString());
                            e_rigido.Km_unidad = float.Parse(dr[5].ToString());
                            e_rigido.Km_service = float.Parse(dr[6].ToString());
                            e_rigido.Km_cambio_neumaticos = float.Parse(dr[7].ToString());
                            e_rigido.Km_service_suma = float.Parse(dr[8].ToString());
                            e_rigido.Km_cambio_neumaticos_suma = float.Parse(dr[9].ToString());
                            e_rigido.Fecha_alta = Convert.ToDateTime(dr[10].ToString());
                            e_rigido.Estado = dr[11].ToString();
                            //hasta aca trae lo de camion
                            e_rigido.Tipo_carga = Convert.ToInt32(dr[12].ToString());
                            e_rigido.Altura = float.Parse(dr[13].ToString());
                            e_rigido.Ancho_interior = float.Parse(dr[14].ToString());
                            e_rigido.Ancho_exterior = float.Parse(dr[15].ToString());
                            e_rigido.Longitud = float.Parse(dr[16].ToString());
                            e_rigido.Volumen = float.Parse(dr[17].ToString());
                            e_rigido.Capacidad_carga = float.Parse(dr[18].ToString());
                            e_rigido.Enganche = Convert.ToBoolean(dr[19]);
                        }
                        cnn.Close();
                        return e_rigido;
                    }
                    else
                    {
                        cnn.Close();
                        cmd = new SqlCommand();
                        cnn.Open();
                        cmd.Connection = cnn;

                        cmd.CommandText = $"select c.id_dominio , c.marca , c.modelo,c.año ,c.tara ,c.km_unidad ,c.km_service, " +
                                          $"c.km_cambio_neumaticos,c.km_service_suma,c.km_cambio_neumaticos_suma,c.fecha_alta, c.estado " +
                                          $"from camion c join marca_camion ma on c.marca=ma.id join modelo_camion mo on c.modelo=mo.id  join tractocamion tr on tr.id_dominio=c.id_dominio  where c.id_dominio= '{id_dominio}' ";
                        cmd.CommandType = CommandType.Text;
                        dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                e_tractocamion.Id_Dominio = Convert.ToInt32(dr[0].ToString());
                                e_tractocamion.Marca = Convert.ToInt32(dr[1].ToString());
                                e_tractocamion.Modelo = Convert.ToInt32(dr[2].ToString());
                                e_tractocamion.Año = Convert.ToInt32(dr[3].ToString());
                                e_tractocamion.Tara = float.Parse(dr[4].ToString());
                                e_tractocamion.Km_unidad = float.Parse(dr[5].ToString());
                                e_tractocamion.Km_service = float.Parse(dr[6].ToString());
                                e_tractocamion.Km_cambio_neumaticos = float.Parse(dr[7].ToString());
                                e_tractocamion.Km_service_suma = float.Parse(dr[8].ToString());
                                e_tractocamion.Km_cambio_neumaticos_suma = float.Parse(dr[9].ToString());
                                e_tractocamion.Fecha_alta = Convert.ToDateTime(dr[10].ToString());
                                e_tractocamion.Estado = dr[11].ToString();
                            }
                            cnn.Close();
                            return e_tractocamion;
                        }
                        else
                        {
                            cnn.Close();
                            return null;
                        }
                    }

                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar camion", e);
                }
            }
        }
        public DataTable listarTodos()//utiliza isNull(rigido.dominio,'tracto') en el caso de que el dominio de rigido swea null pordra 'tracto'.Esto es utilizado para poder poner el boton 'VER DETALLES' en los DGV listar
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio = c.id_dominio ", cnn);
                    //join tipo_carga t on c.tipo_carga=t.id", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los camiones", e);
                }
            }
        } //Lista todos los camiones sin filtrar si es rigido o tractocamion
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio =c.id_dominio" +

                                            $" where c.estado='HABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los camiones habilitados", e);
                }
            }
        } //Lista todos los camiones habilitados sin filtrar si es rigido o tractocamion
        public DataTable listarInhabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio =c.id_dominio" +

                                            $" where c.estado='DESHABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los camiones deshabilitados", e);
                }
            }
        } //Lista todos los camiones deshabilitados sin filtrar si es rigido o tractocamion
        public DataTable listarRigidos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio =c.id_dominio" +

                                            $" where c.estado='HABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los camiones rigidos", e);
                }
            }
        } //Lista todos los camiones deshabilitados sin filtrar si es rigido o tractocamion
        public DataTable listarTractos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" join tractocamion tc on tc.id_dominio =c.id_dominio" +

                                            $" where tc.id_dominio is NULL and c.estado='HABILITADO'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los camiones tractocamiones", e);
                }
            }
        } //Lista detalles de camion rigido (btn ver detalles en tabla camiones en capavista)
        public DataTable listaPorFechaAlta(string desde, string hasta)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio =c.id_dominio" +

                                            $" where TRY_PARSE(c.fecha_alta as datetime) between '{desde}' and '{hasta}'", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los camiones por fecha", e);
                }
            }
        }//Lista todos los camiones por fecha alta habilitados
        public DataTable busquedaPorEntradaDeTexto(string palabra)// puede filtrar por nro motor ,chasis ,marca , modelo
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio =c.id_dominio" +

                                            $" where d.dominio like '%{palabra}%' or ma.marca like '%{palabra}%' or mo.modelo like '%{palabra}%' or d.dominio like '%{palabra}%' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al realizar la busqueda de camiones", e);
                }
            }
        }
        public DataTable busquedaPorRangoDeFechaDeAlta(string desde, string hasta)//busca por rango de fecha de alta
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select c.id_dominio 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (ri.id_dominio as varchar(20)),'NULL')" +

                                            $" from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left outer join rigido ri on ri.id_dominio =c.id_dominio" +

                                            $" where c.fecha_alta between TRY_PARSE('{desde}' as datetime)  and TRY_PARSE('{hasta}' as datetime) ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al realizar la busqueda de camiones por fecha de alta", e);
                }
            }
        }
        public DataTable listar_tractocamiones_pedido(float km_viaje, float volumen, string tipo_carga, float altura, float ancho_int, float ancho_ext, float longitud, DateTime fecha_salida, DateTime fecha_regreso)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open(); //JOINEAR CON TRACTO Y NO RIGIDO.. QUITAR ATRIBUTOS DE RIGIDO Y ENGANCHE

                    da = new SqlDataAdapter($"select distinct(c.id_dominio) 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca 'Marca'," +
                                            $"mo.modelo 'Modelo'," +
                                            $"c.año 'Año'," +
                                            $"c.tara 'Tara'," +
                                            $"c.km_unidad 'Km Unidad'," +
                                            $"c.km_service 'Km Service'," +
                                            $"c.km_cambio_neumaticos 'Km Cambio Neumaticos'," +
                                            $"c.km_service_suma 'Km Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'Km Cambio Neumaticos Suma'," +
                                            $"c.fecha_alta 'Fecha de alta'," +
                                            $"c.estado 'Estado'," +
                                            $"isnull(cast (c.id_dominio as varchar(20)),'NULL') " +

                                            $"from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join tractocamion tr on c.id_dominio=tr.id_dominio" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" left join unidades_viaje uv on c.id_dominio=uv.id_camion" +
                                            $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                            $" left join pedido p on p.id_pedido=v.id_pedido " +
                                            $" left join taller ta on c.id_dominio=ta.id_unidad " +


                                            $" where (c.km_cambio_neumaticos_suma) - { km_viaje.ToString().Replace(',', '.')  } >= 0 " +
                                            $" and c.km_service_suma - { km_viaje.ToString().Replace(',', '.')  } >=  0" +
                                            $" and ( (ta.estado NOT LIKE 'POR_REVISAR' AND ta.estado NOT LIKE 'EN_PROCESO' ) or ta.estado is null)" +
                                            $" and ( (v.estado NOT LIKE 'EN CURSO' and v.estado NOT LIKE 'PENDIENTE' and v.estado NOT LIKE 'SUSPENDIDO' ) or v.estado is null)" +
                                            $" and c.estado = 'HABILITADO'" +
                                            $" and c.id_dominio not in (select distinct(c.id_dominio)" +
                                                                    $"from camion c join dominio d on c.id_dominio=d.id" +
                                                                    $" join tractocamion tr on c.id_dominio=tr.id_dominio" +
                                                                    $" join marca_camion ma on c.marca=ma.id" +
                                                                    $" join modelo_camion mo on c.modelo=mo.id" +
                                                                    $" left join unidades_viaje uv on c.id_dominio=uv.id_camion" +
                                                                    $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                                                    $" left join pedido p on p.id_pedido=v.id_pedido " +
                                                                    $" left join taller ta on c.id_dominio=ta.id_unidad " +
                                                                    $" where (p.fecha_salida between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}'" +
                                                                    $" or p.fecha_regreso between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}') and" +
                                                                    $" (v.estado like 'EN CURSO' or v.estado like 'PENDIENTE') and p.estado like 'CARGADO')", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso con la sugerencia de tractocamiones disponibles", e);
                }
            }

        }//Lista todos los tractocamiones
        public DataTable listar_rigidos_pedido(float km_viaje, float volumen, string tipo_carga, float altura, float ancho_int, float ancho_ext, float longitud, DateTime fecha_salida, DateTime fecha_regreso)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open(); //QUERY PARA CAMIONES RIGIDOS SIN ENGANCHE PARA SEMIREMOLQUE
                                //da = new SqlDataAdapter($"select c.id_dominio as 'ID Dominio'," +
                                //               $"d.dominio as 'Dominio'," +
                    da = new SqlDataAdapter($"select distinct(c.id_dominio) 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca as 'Marca'," +
                                            $"mo.modelo as 'Modelo'," +
                                            $"c.año as 'Año'," +
                                            $"c.tara as 'Tara'," +
                                            $"c.km_unidad as 'KM Unidad'," +
                                            $"c.km_service as 'KM Service'," +
                                            $"c.km_cambio_neumaticos as 'KM Cambio Neumaticos'," +
                                            $"c.km_service_suma as 'KM Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'KM Cambio Neumaticos Suma'," +
                                            $"t.tipo as 'Tipo carga'," +
                                            $"r.altura as 'Altura'," +
                                            $"r.ancho_interior as 'Ancho interior'," +
                                            $"r.ancho_exterior as 'Ancho exterior'," +
                                            $"r.longitud as 'Longitud'," +
                                            $"r.volumen as 'Volumen', " +
                                            $"r.capacidad_carga 'Capacidad carga'," +
                                            $"c.fecha_alta as 'Fecha Alta'," +
                                            $"c.estado as 'Estado'," +
                                            $"r.enganche as 'Enganche'" +


                                            $"from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join rigido r on c.id_dominio=r.id_dominio" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" join tipo_carga t on r.tipo_carga=t.id" +
                                            $" left join unidades_viaje uv on c.id_dominio=uv.id_camion" +
                                            $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                            $" left join pedido p on p.id_pedido=v.id_pedido " +
                                            $" left join taller ta on c.id_dominio=ta.id_unidad " +


                                            $" where (c.km_cambio_neumaticos_suma) - { km_viaje.ToString().Replace(',', '.')  } >= 0 " +
                                            $" and c.km_service_suma - { km_viaje.ToString().Replace(',', '.')  } >=  0" +
                                            $" and r.volumen >=  { volumen.ToString().Replace(',', '.')  }  and t.tipo = '{ tipo_carga }' and r.altura >=  { altura.ToString().Replace(',', '.')  } " +
                                            $" and r.ancho_interior >=  { ancho_int.ToString().Replace(',', '.')  }  and r.ancho_exterior >=  { ancho_ext } " +
                                            $" and ( (v.estado NOT LIKE 'EN CURSO' and v.estado NOT LIKE 'PENDIENTE' and v.estado NOT LIKE 'SUSPENDIDO' ) or v.estado is null)" +
                                            $" and r.longitud >=  { longitud.ToString().Replace(',', '.')  } and ( (ta.estado NOT LIKE 'POR_REVISAR' AND ta.estado NOT LIKE 'EN_PROCESO' ) or ta.estado is null)" +
                                            $" and c.estado = 'HABILITADO' and r.enganche = 0" +
                                            $" and c.id_dominio not in (select distinct(c.id_dominio)" +
                                                                    $"from camion c join dominio d on c.id_dominio=d.id" +
                                                                    $" join rigido r on c.id_dominio=r.id_dominio" +
                                                                    $" join marca_camion ma on c.marca=ma.id" +
                                                                    $" join modelo_camion mo on c.modelo=mo.id" +
                                                                    $" join tipo_carga t on r.tipo_carga=t.id" +
                                                                    $" left join unidades_viaje uv on c.id_dominio=uv.id_camion" +
                                                                    $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                                                    $" left join pedido p on p.id_pedido=v.id_pedido " +
                                                                    $" left join taller ta on c.id_dominio=ta.id_unidad " +
                                                                    $" where (p.fecha_salida between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}'" +
                                                                    $" or p.fecha_regreso between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}') and" +
                                                                    $" (v.estado like 'EN CURSO' or v.estado like 'PENDIENTE') and p.estado like 'CARGADO')", cnn);


                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso con la sugerencia de camiones para semiremolques disponibles", e);
                }
            }
        }  //Lista todos los camiones Rigidos habilitados
        public DataTable listar_rigidos_enganche_pedido(float km_viaje, float volumen, string tipo_carga, float altura, float ancho_int, float ancho_ext, float longitud, string enganche, DateTime fecha_salida, DateTime fecha_regreso)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open(); //QUERY PARA CAMIONES RIGIDOS CON ENGANCHE PARA REMOLQUE

                    //da = new SqlDataAdapter($"select c.id_dominio as 'ID Dominio', " +
                    //                     $"d.dominio as 'Dominio'," +
                    da = new SqlDataAdapter($"select distinct(c.id_dominio) 'ID Dominio'," +
                                            $"d.dominio 'Dominio'," +
                                            $"ma.marca as 'Marca'," +
                                            $"mo.modelo as 'Modelo'," +
                                            $"c.año as 'Año'," +
                                            $"c.tara as 'Tara'," +
                                            $"c.km_unidad as 'KM Unidad'," +
                                            $"c.km_service as 'KM Service'," +
                                            $"c.km_cambio_neumaticos as 'KM Cambio Neumaticos'," +
                                            $"c.km_service_suma as 'KM Service Suma'," +
                                            $"(c.km_cambio_neumaticos - c.km_cambio_neumaticos_suma) 'KM Cambio Neumaticos Suma'," +
                                            $"t.tipo as 'Tipo carga'," +
                                            $"r.altura as 'Altura'," +
                                            $"r.ancho_interior as 'Ancho interior'," +
                                            $" r.ancho_exterior as 'Ancho exterior'," +
                                            $" r.longitud as 'Longitud'," +
                                            $"r.volumen as 'Volumen', " +
                                            $"r.capacidad_carga 'Capacidad carga'," +
                                            $"c.fecha_alta as 'Fecha Alta'," +
                                            $"c.estado as 'Estado'," +
                                            $"r.enganche as 'Enganche'" +


                                            $"from camion c join dominio d on c.id_dominio=d.id" +
                                            $" join rigido r on c.id_dominio=r.id_dominio" +
                                            $" join marca_camion ma on c.marca=ma.id" +
                                            $" join modelo_camion mo on c.modelo=mo.id" +
                                            $" join tipo_carga t on r.tipo_carga=t.id" +
                                            $" left join unidades_viaje uv on c.id_dominio=uv.id_camion" +
                                            $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                            $" left join pedido p on p.id_pedido=v.id_pedido " +
                                            $" left join taller ta on c.id_dominio=ta.id_unidad " +


                                            $" where (c.km_cambio_neumaticos_suma) - { km_viaje.ToString().Replace(',', '.')  } >= 0 " +
                                            $" and c.km_service_suma - { km_viaje.ToString().Replace(',', '.')  } >=  0" +
                                            $" and r.volumen >=  { volumen.ToString().Replace(',', '.')  }  and t.tipo = '{ tipo_carga }' and r.altura >=  { altura.ToString().Replace(',', '.')  } " +
                                            $" and r.ancho_interior >=  { ancho_int.ToString().Replace(',', '.')  }  and r.ancho_exterior >=  { ancho_ext.ToString().Replace(',', '.')  } " +
                                            $" and r.longitud >=  { longitud.ToString().Replace(',', '.')  } and ( (ta.estado NOT LIKE 'POR_REVISAR' AND ta.estado NOT LIKE 'EN_PROCESO' ) or ta.estado is null)" +
                                            $" and ( (v.estado NOT LIKE 'EN CURSO' and v.estado NOT LIKE 'PENDIENTE' and v.estado NOT LIKE 'SUSPENDIDO' ) or v.estado is null)" +
                                            $" and c.estado = 'HABILITADO' and r.enganche = 1" +
                                            $" and c.id_dominio not in (select distinct(c.id_dominio)" +
                                                                    $"from camion c join dominio d on c.id_dominio=d.id" +
                                                                    $" join rigido r on c.id_dominio=r.id_dominio" +
                                                                    $" join marca_camion ma on c.marca=ma.id" +
                                                                    $" join modelo_camion mo on c.modelo=mo.id" +
                                                                    $" join tipo_carga t on r.tipo_carga=t.id" +
                                                                    $" left join unidades_viaje uv on c.id_dominio=uv.id_camion" +
                                                                    $" left join viaje v on v.id_unidades_viaje=uv.id" +
                                                                    $" left join pedido p on p.id_pedido=v.id_pedido " +
                                                                    $" left join taller ta on c.id_dominio=ta.id_unidad " +
                                                                    $" where (p.fecha_salida between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}'" +
                                                                    $" or p.fecha_regreso between '{fecha_salida.ToShortDateString()}' and '{fecha_regreso.ToShortDateString()}') and" +
                                                                    $" (v.estado like 'EN CURSO' or v.estado like 'PENDIENTE') and p.estado like 'CARGADO')", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso con la sugerencia de camiones para remolques disponibles", e);
                }
            }
        } //Lista todos los camiones rigidos con enganche

        #endregion
    }
}
