﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;


namespace CapaDatos
{
    public class D_Taller
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Taller e_taller;
        string usuario;
        public D_Taller(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_taller = new E_Taller();
        }
        public void insertar(E_Taller e_taller)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta la nueva tipo_acoplado en la tabla tipo_acoplado**************/
                    cmd.CommandText = "insert into taller values ('" + e_taller.usuario + "' , '" + e_taller.Entrada_taller + "', '" + e_taller.Salida_aprox + "', '" + e_taller.Salida + "','" + e_taller.Observaciones + "','" + e_taller.id_dominio + "','" + e_taller.Tipo + "','POR_REVISAR')";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto el Taller id=" + e_taller.Id_taller + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible insertar unidad a Taller", e);
                        default: throw new System.Exception("Ups, algo paso al insertar unidad a Taller", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar unidad al Taller", e);
                }
                cnn.Close();
            }
        }
        //public string baja(E_Taller e_taller)
        //{
        //    string respuesta;
        //    SqlTransaction sqlTran;
        //    using (cnn)
        //    {
        //        cnn.Open();
        //        sqlTran = cnn.BeginTransaction();
        //        try
        //        {
        //            SqlCommand cmd = new SqlCommand();
        //            cmd.Connection = cnn;
        //            cmd.Transaction = sqlTran;
        //            /**********Se da de baja al tip de acoplado,se marca el estado como DESHABILITADO**********/
        //            cmd.CommandText = "update taller set estado= 'DESHABILITADO' where  id_taller='" + e_taller.Id_taller + "'";
        //            if (cmd.ExecuteNonQuery() > 0)
        //            {
        //                cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja al Taller id=" + e_taller.Id_taller + "')";
        //                cmd.ExecuteNonQuery();
        //                respuesta = null;
        //                sqlTran.Commit();
        //            }
        //            else
        //            {
        //                respuesta = "Error al dar de baja a pedido en Taller";
        //            }

        //            cnn.Close();
        //        }
        //        catch
        //        {
        //            respuesta = "Error al dar de baja a pedido en Taller";
        //            sqlTran.Rollback();

        //        }
        //        cnn.Close();
        //    }

        //    return respuesta;


        //}
        public int modificar(E_Taller e_taller)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza taller ,ojo!!! taller id_unidad no se puede modificar**********/
                    cmd.CommandText = "update taller set usuario='" + e_taller.usuario + "',entrada_taller = '" + e_taller.Entrada_taller + "',salida_aprox='" + e_taller.Salida_aprox + "',salida='" + e_taller.Salida + "',observaciones='" + e_taller.Observaciones + "',tipo='" + e_taller.Tipo + "',estado='" + e_taller.Estado + "'  where  id_taller='" + e_taller.Id_taller + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó el Taller id_taller=" + e_taller.Id_taller + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Taller ", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Taller", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Taller", e);
                }
            }
        }
        public E_Taller retornaTaller(string id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id_taller,usuario,entrada_taller,salida_aprox,salida,observaciones,id_unidad,tipo,estado  from taller where id_taller = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        e_taller.Id_taller = Convert.ToInt32(dr[0].ToString());
                        e_taller.usuario = dr[1].ToString();
                        e_taller.Entrada_taller = Convert.ToDateTime(dr[2].ToString());
                        e_taller.Salida_aprox = Convert.ToDateTime(dr[3].ToString());
                        e_taller.Salida = Convert.ToDateTime(dr[4].ToString());
                        e_taller.Observaciones = dr[5].ToString();
                        e_taller.id_dominio = Convert.ToInt32(dr[6].ToString());
                        e_taller.Tipo = dr[7].ToString();
                        e_taller.Estado = dr[8].ToString();

                    }
                    cnn.Close();
                    return e_taller;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar taller", e);
                }
            }
        } //Funcion que retorna un objeto tipo acoplado
        public DataTable listar_por_Estado_Camion(string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();

                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado camion' ," +
                                                $" case(ri.id_dominio)when null then 'Tracto' else 'Rigido' end as 'Tipo Camion'," +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado'" +
                                                $" ,t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller'," +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from camion c join marca_camion ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_camion mo on c.modelo=mo.id" +
                                                $" left outer join rigido ri on ri.id_dominio =c.id_dominio " +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                                                $" WHERE t.estado LIKE '{estado}' ", cnn);


                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar por estado de camion", e);
                }
            }
        }
        public DataTable listar_por_Texto_Camion(string estado, string texto)//Busca marca,dominio observaciones
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado camion' ," +
                                                $" case(ri.id_dominio)when null then 'Tracto' else 'Rigido' end as 'Tipo Camion'," +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller'," +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from camion c join marca_camion ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_camion mo on c.modelo=mo.id" +
                                                $" left outer join rigido ri on ri.id_dominio =c.id_dominio " +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                                                $" WHERE ma.marca like '%{texto}%' or dom.dominio like '%{texto}%' or t.observaciones like '%{texto}%' or t.id_Taller like '%{texto}%' and t.estado LIKE '{estado}' ", cnn);


                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar por texto camión", e);
                }
            }
        }
        public DataTable listar_por_Fecha_Entrada_Camion(string desde, string hasta, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado camion' ," +
                                                $" case(ri.id_dominio)when null then 'Tracto' else 'Rigido' end as 'Tipo Camion'," +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller'," +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from camion c join marca_camion ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_camion mo on c.modelo=mo.id" +
                                                $" left outer join rigido ri on ri.id_dominio =c.id_dominio " +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                            $" WHERE t.entrada_taller between '{desde}' and '{hasta}' and t.estado LIKE '{estado}' ", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha de entrada de camión", e);
                }
            }

        }
        public DataTable listar_por_Fecha_Salida_Camion(string desde, string hasta, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado camion' ," +
                                                $" case(ri.id_dominio)when null then 'Tracto' else 'Rigido' end as 'Tipo Camion'," +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller'," +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from camion c join marca_camion ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_camion mo on c.modelo=mo.id" +
                                                $" left outer join rigido ri on ri.id_dominio =c.id_dominio " +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                            $" WHERE t.salida_aprox between '{desde}' and '{hasta}' and t.estado LIKE '{estado}' ", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha de salida de camión", e);
                }
            }
        }
        public DataTable listar_por_Estado_Acoplado(string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado acoplado' ,  " +
                                                $" c.tipo_acoplado as 'Tipo Acoplado' ,  " +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller', " +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from acoplado c join marca_acoplado ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_acoplado mo on c.modelo=mo.id" +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                            $" WHERE t.estado LIKE '{estado}' ", cnn);
                    //0 Ticket 1 Dominio 2 Marca 3 Modelo 4 Año
                    //5 Tipo de acoplado 6 Tara 
                    //7 Km unidad 8 Km service 9 Km cambio neumaticos
                    //10 Km service suma 11 Km cambio neumaticos suma 12 Altura
                    //13 Estado 14 Fecha de entrada 15 Entrega aprox
                    //16 Entregado(fecha de allta seria) 17 Observaciones 18 Tipo taller
                    //19 Estado taller 20 Num legajo 21 Usuario
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por estado acoplado", e);
                }
            }
        }
        public DataTable listar_por_Texto_Acoplado(string estado, string texto)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado acoplado' ,  " +
                                                $" c.tipo_acoplado as 'Tipo Acoplado' ,  " +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller', " +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from acoplado c join marca_acoplado ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_acoplado mo on c.modelo=mo.id" +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                       $" WHERE ma.marca like '%{texto}%' or dom.dominio like '%{texto}%' or t.observaciones like '%{texto}%' or  t.id_Taller like '%{texto}%'  and t.estado LIKE '{estado}' ", cnn);



                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por texto acoplado", e);
                }
            }
        }
        //public DataTable Listar_vencidos_camion()//TESTEAR EL TEMA DE LA FECHA
        //{
        //    try
        //    {
        //        cnn.Open();
        //        da = new SqlDataAdapter($"select t.id_taller 'Ticket',c.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
        //                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
        //                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado camion' ," +
        //                $" case(ri.dominio)when null then 'Tracto' else 'Rigido' end as 'Tipo Camion'," +
        //                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo',t.estado 'Tipo taller'," +
        //                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
        //                $" from camion c join marca_camion ma on c.marca=ma.id" +
        //                $" join modelo_camion mo on c.modelo=mo.id" +
        //                $" left outer join rigido ri on ri.dominio =c.dominio " +
        //                $" join taller t on t.id_unidad=c.dominio" +
        //                $" left outer join usuarios u on u.usuario = t.usuario" +
        //                $" WHERE t.salida_aprox < '{DateTime.Now.ToShortTimeString()}' and t.estado LIKE 'EN_PROCESO'  ", cnn);

        //        dt = new DataTable();
        //        da.Fill(dt);
        //        cnn.Close();
        //        return dt;
        //    }
        //    catch (Exception)
        //    {
        //        cnn.Close();
        //        return null;
        //    }
        //}

        public DataTable listar_por_Fecha_Entrada_Acoplado(string desde, string hasta, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado acoplado' ,  " +
                                                $" c.tipo_acoplado as 'Tipo Acoplado' ,  " +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller', " +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from acoplado c join marca_acoplado ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_acoplado mo on c.modelo=mo.id" +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                       $" WHERE t.entrada_taller between '{desde}' and '{hasta}' and t.estado LIKE '{estado}' ", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha de entrada acoplado", e);
                }
            }
        }
        public DataTable listar_por_Fecha_Salida_Acoplado(string desde, string hasta, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter($"select t.id_taller 'Ticket',dom.dominio 'Dominio',ma.marca 'Marca',mo.modelo 'Modelo',c.año 'Año', " +
                                                $" c.tara 'Tara',c.km_unidad 'Km Unidad',c.km_service 'Km service',c.km_cambio_neumaticos 'Km cambio de neumaticos'," +
                                                $" c.km_service_suma as 'Km service suma', c.km_cambio_neumaticos_suma 'Km neumaticos suma' ,c.estado 'Estado acoplado' ,  " +
                                                $" c.tipo_acoplado as 'Tipo Acoplado' ,  " +
                                                $" t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller', " +
                                                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
                                                $" from acoplado c join marca_acoplado ma on c.marca=ma.id" +
                                                $" join dominio dom on dom.id=c.id_dominio" +
                                                $" join modelo_acoplado mo on c.modelo=mo.id" +
                                                $" join taller t on t.id_unidad=c.id_dominio" +
                                                $" left outer join usuarios u on u.usuario = t.usuario" +
                            $" WHERE t.salida_aprox between '{desde}' and '{hasta}' and t.estado LIKE '{estado}' ", cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha de salida acoplado", e);
                }
            }
        }

        //public DataTable Listar_vencidos_acoplado()

        //{
        //    try
        //    {
        //        cnn.Open();
        //        da = new SqlDataAdapter($"Select  t.id_taller 'Ticket', r.dominio 'Dominio',m.marca 'Marca',r.modelo 'Modelo',r.año 'Año'," +
        //                $"r.tipo_acoplado 'Tipo de acoplado',r.tara 'Tara',r.capacidad_carga 'Capacidad de carga'," +
        //                $"r.km_unidad 'Km unidad',r.km_service 'Km service',r.km_cambio_neumaticos 'Km cambio neumaticos'," +
        //                $"r.km_service_suma 'Km service suma',r.km_cambio_neumaticos_suma 'Km cambio neumaticos suma',r.altura 'Altura'," +
        //                $"r.longitud  'Longitud'," +
        //                $"r.cant_ejes 'Cantidad ejes',r.estado 'Estado'," +
        //                $"t.entrada_taller 'Fecha de entrada',t.salida_aprox 'Entrega aprox',t.salida 'Entregado',t.observaciones 'Observaciones',t.tipo 'Tipo taller',t.estado 'Estado taller'," +
        //                $" isnull(u.usuario,'----') as 'Num legajo', isnull(u.usuario,'Sin asignar') as 'Usuario' " +
        //                $"from acoplado r join tipo_carga_acoplado t on r.tipo_carga=t.id join marca_acoplado m on r.marca=m.id" +
        //                $" join taller t on t.id_unidad=c.dominio" +
        //                $" left outer join usuarios u on u.usuario = t.usuario" +
        //                $" WHERE t.salida_aprox < '{DateTime.Now.ToShortTimeString()}' and t.estado LIKE 'EN_PROCESO' ", cnn);

        //        dt = new DataTable();
        //        da.Fill(dt);
        //        cnn.Close();
        //        return dt;
        //    }
        //    catch (Exception)
        //    {
        //        cnn.Close();
        //        return null;
        //    }
        //}
        public String verifica_si_unidad_puede_ingresar_a_taller(string dominio)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string resp = null;
                try
                {
                    // Observamos si existe algun camion con x dominio que no este en viaje
                    this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    //bien, averiguamos si esa unidad esta en taller
                    da = new SqlDataAdapter($"select * " +
                   $"from  taller ta" +
                   $" WHERE    ta.estado  in ('por_revisar', 'en_proceso')     and  ta.id_unidad  in (Select id " +
                   $"                                                                                  from dominio where dominio like '{dominio}' ) ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    if (dt.Rows.Count > 0)
                    {
                        resp = "Esta unidad se encuentra en mantenimiento";
                    }
                    else
                    {
                        // Observamos si existe algun camion con x dominio que no este en viaje
                        da = new SqlDataAdapter($"select * " +
                                           $"from  unidades_viaje uv " +
                                           $"INNER JOIN viaje v on uv.id=v.id_unidades_viaje  " +
                                           $"INNER JOIN camion c on uv.id_camion=c.id_dominio  " +
                                           $"WHERE   v.estado  in ('PENDIENTE' , 'EN CURSO') and  c.id_dominio  in (Select id " +
                                           $"                                                                                  from dominio where dominio like '{dominio}')  ", cnn);
                        dt = new DataTable();
                        da.Fill(dt);
                        if (dt.Rows.Count > 0)
                        {
                            resp = "Este camion debe ser cargado a mantenimiento por el encargado de logistica";
                        }
                        else
                        {
                            //Observamos si se trata de un acoplado y si este tampoco esta en viaje
                            da = new SqlDataAdapter($"select * " +
                                $"from  unidades_viaje uv " +
                                $"INNER JOIN viaje v on uv.id=v.id_unidades_viaje  " +
                                $"INNER JOIN acoplado acop on uv.id_acoplado=acop.id_dominio  " +
                                $"WHERE   v.estado  in ('PENDIENTE' , 'EN CURSO')   and  acop.id_dominio  in (Select id " +
                                $"                                                                                  from dominio where dominio like '{dominio}' ) ", cnn);
                            dt = new DataTable();
                            da.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                resp = "Este remolque debe ser cargado a mantenimiento por el encargado de logistica";
                            }
                            else
                            {//Si llego hasta aca es por que tampoco esta en manteniento
                             //ahora averiguamos si se trata de un camion y si este existe y esta habilitado
                                da = new SqlDataAdapter($"select * " +
                                                        $"from camion where  estado like 'HABILITADO' and  id_dominio  in (Select id " +
                                $"                                                                                  from dominio where dominio like '{dominio}' ) ", cnn);
                                da.Fill(dt);
                                if (dt.Rows.Count > 0)//vemos si el camion existe y esta habilitado
                                {
                                    resp = "CAMION";
                                }
                                else
                                {//no se trataba de un camion, averiguamos si es un acoplado, y si esta habilitado
                                    da = new SqlDataAdapter($"select * " +
                                               $"from  acoplado where dominio like '{dominio}' and estado like 'HABILITADO'  "
                                              , cnn);
                                    da = new SqlDataAdapter($"select * " +
                                              $"from acoplado where  estado like 'HABILITADO' and id_dominio  in (Select id " +
                                              $"                                                                                  from dominio where dominio like '{dominio}' ) ", cnn);
                                    da.Fill(dt);
                                    if (dt.Rows.Count > 0)
                                    {
                                        resp = "ACOPLADO";
                                    }
                                    else
                                    {//La unidad no existe o tal vez, se encuentra inhabilitada
                                        resp = "La unidad no existe o esta INHABILITADA";
                                    }


                                }
                            }
                        }
                    }


                    cnn.Close();
                    return resp;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al verificar unidad para ingresar al taller", e);
                }
            }
        }
        //
        //ACOPLADO
        //   DOMINIO
        //   MARCA
        //   MODELO
        //   TIPO CARGA
        //   TIPO ACOPLADO
        //   TARA
        //   CAPACIDAD CARGA
        //   KM SERVICE----------------- CADA CUANTO SE LE HACEN LOS SERVICES?

        //   KM_CAMBIO _NEUMATICOS------ CADA CUANTO CAMBIAMOS LOS NEUMATICOS?
        //   KM SERVICE_SUMA----------------- CUANTO SE PASO DEL SERVICE?
        //   KM_CAMBIO _NEUMATICOS_SUMA----CUANTO SE PASO?
        //   ALLTURA 
        //   ANCHO EXTERIOR     
        //   LONGITUD ----------DONDE LO TENEMOS QUE ARREGLAR? ESPACIO
        //   CANT_EJES
        //ESTADO



    }
}
