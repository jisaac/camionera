﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaEntidades;
using System.Configuration;


namespace CapaDatos
{
    public class D_TipoCarga
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_TipoCarga e_tipo_carga;
        string usuario;
        public D_TipoCarga(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_tipo_carga = new E_TipoCarga();
        }
        public void insertar(E_TipoCarga e_tipo_carga)
        {
            SqlTransaction sqlTran;
            using (cnn=new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /*********se inserta la nueva tipo_carga en la tabla tipo_carga**************/
                    cmd.CommandText = "insert into tipo_carga values ('" + e_tipo_carga.Tipo + "','HABILITADO');SELECT Scope_Identity();";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto el tipo de carga id=" + e_tipo_carga.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Tipo de carga", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Tipo de carga", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Tipo de carga", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_TipoCarga e_tipo_carga)
        {
            int num_rows = -1 ;
            SqlTransaction sqlTran;
            using (cnn= new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********Se da de baja al tipo de carga,se marca el estado como DESHABILITADO**********/
                    cmd.CommandText = "delete from tipo_carga where  id='" + e_tipo_carga.Id + "';SELECT Scope_Identity();";
                    num_rows=cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Dió de baja al tipo de carga id=" + e_tipo_carga.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Tipo de Carga", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Tipo de Carga", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Tipo de Carga", e);
                }
            }
        }
        public int modificar(E_TipoCarga e_tipo_carga)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn= new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    /**********se actualiza el tipo de carga**********/
                    cmd.CommandText = "update tipo_carga set estado= '" + e_tipo_carga.Estado + "' ,tipo='" + e_tipo_carga.Tipo + "' where  id='" + e_tipo_carga.Id + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó el tipo de carga id=" + e_tipo_carga.Id + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Tipo de carga", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Tip de carga", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Tipo de carga", e);
                }
            }
        }
        public E_TipoCarga retornaTipoCarga(int id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id,tipo,estado from tipo_carga where id = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_tipo_carga.Id = Convert.ToInt32(dr[0].ToString());
                        e_tipo_carga.Tipo = dr[1].ToString();
                        e_tipo_carga.Estado = dr[2].ToString();

                    }
                    cnn.Close();
                    return e_tipo_carga;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar tipo de carga", e);
                }
            }
              
        } //Funcion que retorna un objeto tipo de carga
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
                    cnn.Open();
                    da = new SqlDataAdapter("select id,tipo as 'Tipo de carga',estado from tipo_carga ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los tipos de carga", e);
                }
            }
        }
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,tipo as 'Tipo de carga',estado from tipo_carga where estado ='HABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar tipos de carga habilitados", e);
                }
            }
        }
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select id,tipo as 'Tipo de carga',estado from tipo_carga where estado ='DESHABILITADO' ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar tipos de carga deshabilitados", e);
                }
            }
        }
        public List<String> listarComboBox()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    List<String> lista = new List<string>();
                    cnn.Open();
                    SqlCommand cmd = new SqlCommand("Select tipo From tipo_carga where estado='HABILITADO'", cnn);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        lista.Add(dr[0].ToString());
                    }
                    cnn.Close();
                    return lista;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar lista de combobox", e);
                }
            }
        }
        public bool existe(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                bool respuesta = false;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select * from tipo_carga where tipo ='" + name + "' and estado='HABILITADO' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    if (dr.HasRows)
                    {
                        respuesta = true;
                    }
                    else
                    {
                        respuesta = false;
                    }

                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar si existe tipo de carga", e);
                }
            }
        }
        public int retornaId(string name)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                int respuesta = -1;
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "Select id from tipo_carga where tipo= '" + name + "' and estado='HABILITADO' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {

                        respuesta = Convert.ToInt32(dr[0]);

                    }
                    cnn.Close();
                    return respuesta;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID de tipo de carga", e);
                }
            }
                
        }
    }
}
