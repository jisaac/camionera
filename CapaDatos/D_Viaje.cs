﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CapaEntidades;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    public class D_Viaje
    {
        public D_Viaje(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
        }
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        string usuario;
        public void insertar(E_Viaje e_viaje, E_UnidadesViaje e_unidadesViaje)
        {
            int idUnidadesViaje = -1;
            int controlaInsertViaje;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    //---------------insertamos en unidades viaje------------
                    if(e_unidadesViaje.Id_acoplado != -1)
                        cmd.CommandText = $"insert into unidades_viaje values ('{e_unidadesViaje.Id_camion}' ,'{e_unidadesViaje.Id_acoplado}');SELECT Scope_Identity();";
                    else
                        cmd.CommandText = $"insert into unidades_viaje (id_camion) values ('{e_unidadesViaje.Id_camion}');SELECT Scope_Identity();";
                    idUnidadesViaje = Convert.ToInt32(cmd.ExecuteScalar()); //si no pudo insertar traera nullreferenceexception VER ID ULTIMO 
                    //---------------insertamos en viaje------------ 
                    cmd.CommandText = $"insert into viaje values ('{e_viaje.Id_pedido}','{idUnidadesViaje}','{e_viaje.Fecha_salida}','{e_viaje.Fecha_regreso}','0','{e_viaje.Estado}','{e_viaje.Observaciones}');SELECT Scope_Identity();";
                     controlaInsertViaje= cmd.ExecuteNonQuery();
                    //----------------insertamos en auditoria---------------------
                    cmd.CommandText = $"insert into auditoria values ('{(DateTime.Now).ToString()}','{usuario}','se creo viaje con id {controlaInsertViaje}')";
                    cmd.ExecuteNonQuery();
               
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible realizar la acción, el viaje ya existe", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    throw new System.Exception("Ups, algo salio mal al insertar unidad", e);
                }
                cnn.Close();
            }

        }
        public int modificarViaje(E_Viaje e_viaje)//modifica solo el viaje
        {
            int controlaInsertViaje=-1;
            int controlaInsertAuditoria;
            string respuesta = string.Empty;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    //--------------modificamos viaje-------------
                    cmd.CommandText = $"update viaje set fecha_salida='{e_viaje.Fecha_salida}' , fecha_regreso='{e_viaje.Fecha_regreso}' ,km_cierre= '{e_viaje.Km_cierre}', estado='{e_viaje.Estado}',observaciones='{e_viaje.Observaciones}' where id_viaje= {e_viaje.Id_viaje.ToString()}";
                    controlaInsertViaje = Convert.ToInt32(cmd.ExecuteNonQuery());
                    //----------------insertamos en auditoria---------------------
                    cmd.CommandText = $"insert into auditoria values ('{(DateTime.Now).ToString()}','{usuario}','modifico viaje con id {e_viaje.Id_viaje.ToString()}')";
                    controlaInsertAuditoria = cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return controlaInsertViaje;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible realizar la acción, esta unidad ya se encuentra en el sistema", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    throw new System.Exception("Ups, algo salio mal al modificar viaje", e);
                }
                cnn.Close();
                return controlaInsertViaje;
            }
        }
        public int modificarUnidades(E_Viaje e_viaje, E_UnidadesViaje e_unidadesViaje)
        {
            int controlaInsertUnidades = -1;
            string respuesta = string.Empty;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    //--------------- modificamos unidades viaje-------------
                    if(e_unidadesViaje.Id_acoplado >= 0)
                        cmd.CommandText = $"update unidades_viaje set id_camion='{e_unidadesViaje.Id_camion}' , id_acoplado='{e_unidadesViaje.Id_acoplado}'  where id = {e_viaje.Id_unidades_viaje.ToString()}";
                    else
                        cmd.CommandText = $"update unidades_viaje set id_camion='{e_unidadesViaje.Id_camion}' where id = {e_viaje.Id_unidades_viaje.ToString()}";
                    controlaInsertUnidades = cmd.ExecuteNonQuery();
                    cmd.CommandText = $"insert into auditoria values ('{(DateTime.Now).ToString()}','{usuario}','modifico unidades viaje con id viaje {e_viaje.Id_viaje.ToString()}')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible realizar la acción, esta unidad ya se encuentra en el sistema", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    throw new System.Exception("Ups, algo salio mal al modificar unidades", e);
                }
                cnn.Close();
            }
            return controlaInsertUnidades;
        }
        public int modificarEstado(E_Viaje e_viaje)//modifica El estado del viaje
        {
            int controlaInsertViaje=-1;
            string respuesta = string.Empty;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();//para manejo de transacciones
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandType = CommandType.Text;
                    //--------------modificamos viaje-------------
                    cmd.CommandText = $"update viaje set fecha_salida='{e_viaje.Fecha_salida}' , fecha_regreso='{e_viaje.Fecha_regreso}' ,km_cierre={e_viaje.Km_cierre}, estado='{e_viaje.Estado}',observaciones='{e_viaje.Observaciones}' where id_viaje= {e_viaje.Id_viaje.ToString()}";
                    controlaInsertViaje = Convert.ToInt32(cmd.ExecuteNonQuery());
                    //----------------insertamos en auditoria---------------------
                    cmd.CommandText = $"insert into auditoria values ('{(DateTime.Now).ToString()}','{usuario}','cambió  viaje  con id {e_viaje.Id_viaje.ToString()}  a estado {e_viaje.Estado}')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return controlaInsertViaje;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible realizar la acción", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    throw new System.Exception("Ups, algo salio mal al modificar estado", e);
                }
                cnn.Close();
                return controlaInsertViaje;
            }
        }
        public E_Viaje retornaViaje(string id_viaje)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                E_Viaje e_viaje = new E_Viaje();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select * from viaje where  id_viaje = {id_viaje}";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_viaje.Id_viaje = Convert.ToInt32(dr[0]);
                        e_viaje.Id_pedido = Convert.ToInt32(dr[1]);
                        e_viaje.Id_unidades_viaje = Convert.ToInt32(dr[2]);
                        if (dr[3].ToString().ToUpper() == "NULL" || dr[3].ToString().ToUpper().Equals(string.Empty))
                        {
                            e_viaje.Fecha_salida = null;
                        }
                        else
                        {
                            e_viaje.Fecha_salida = Convert.ToDateTime(dr[3]);
                        }
                        if (dr[4].ToString().ToUpper() == "NULL" || dr[4].ToString().ToUpper().Equals(string.Empty))
                        {
                            e_viaje.Fecha_regreso = null;
                        }
                        else
                        {
                            e_viaje.Fecha_regreso = Convert.ToDateTime(dr[4]);
                        }
                        e_viaje.Km_cierre = float.Parse(dr[5].ToString());
                        e_viaje.Estado = dr[6].ToString();
                        if (dr[7].ToString().ToUpper() == "NULL" || dr[7].ToString().ToUpper().Equals(string.Empty))
                        {
                            e_viaje.Observaciones = "";
                        }
                        else
                        {
                            e_viaje.Observaciones = dr[7].ToString();
                        }

                    }

                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar viaje", e);
                }
                cnn.Close();
                return e_viaje;
            }
        }
        public E_UnidadesViaje retornaUnidadesViaje(E_Viaje e_viaje)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                E_UnidadesViaje e_unidadesViaje = new E_UnidadesViaje();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"select * from unidades_viaje where  id = {e_viaje.Id_unidades_viaje}";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_unidadesViaje.Id = Convert.ToInt32(dr[0]);
                        e_unidadesViaje.Id_camion = Convert.ToInt32(dr[1]);
                        if (dr[2] != DBNull.Value)
                            e_unidadesViaje.Id_acoplado = Convert.ToInt32(dr[2]);
                        else
                            e_unidadesViaje.Id_acoplado = -1;
                    }
                    cnn.Close();
                    return e_unidadesViaje;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar unidades de un viaje", e);
                }
            }
        }
        /**********************************  Para listar "todos los estados" el parametro seria "%_%"          **************************************/
        public DataTable listar_por_Estado(string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();

                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
                                            $"WHERE v.estado like '{estado}' "

                                           , cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por estado", e);
                }
            }
        }
        public DataTable listar_Fecha_salida_pedido(string desde,string hasta,string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();


                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
             $" where p.fecha_salida  between '{desde}' and '{hasta}'  and v.estado like '{estado}' "

                                           , cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha salida pedido", e);
                }
            }
        }
        public DataTable listar_Fecha_Regreso_pedido(string desde, string hasta, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();


                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
                            $"WHERE p.fecha_regreso between '{desde}' and '{hasta}'  and v.estado like '{estado}' "

                                               , cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha regreso pedido", e);
                }
            }
        }
        public DataTable listar_Fecha_Regreso_viaje(string desde, string hasta,string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();


                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
              $"WHERE v.fecha_regreso between '{desde}' and '{hasta}' and v.estado like '{estado}' "

                                                   , cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha regreso viaje", e);
                }
            }
        }
        public DataTable listar_Fecha_salida_viaje(string desde, string hasta, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();

                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
             $"WHERE v.fecha_regreso  between '{desde}' and '{hasta}'  and v.estado like '{estado}' "

                                                       , cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por fecha salida viaje", e);
                }
            }
        }
        public DataTable listar_Texto(string texto, string estado)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();


                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
                               $"WHERE v.id_viaje like '%{texto}%' or domc.dominio like '%{texto}%' or doma.dominio like '%{texto}%' or " +
                                            $"CONCAT(ch.nombre,' ',ch.apellido ) like '%{texto}%'  or p.nombre_cliente like '%{texto}%' or p.observaciones like '%{texto}%' or v.observaciones like '%{texto}%' and v.estado like '{estado}' "
                                       , cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por busqueda de texto", e);
                }
            }
        }
        public DataTable listar_Unidades_viaje(string dominio)/////PROBAR ESTA FUNCIONNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN 
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();

                    da = new SqlDataAdapter($"select v.id_viaje as 'ID' , p.fecha_salida as 'SALIDA APROX',v.fecha_salida as 'SALIDA',v.fecha_regreso as 'REGRESO',p.fecha_regreso as 'REGRESO APROX',v.km_cierre as 'KM REALIZADOS',ISNULL(v.observaciones,'-----') as 'OBSERVACIONES VIAJE',v.estado as 'ESTADO',  " +
                                            $"ISNULL(p.nombre_cliente,'-----') as 'CLIENTE',ISNULL(p.cuil_cliente,'-----') as 'CUIL CLIENTE',p.fecha_pedido as 'FECHA PEDIDO',p.peso_neto as 'PESO NETO',p.volumen as 'VOLUMEN',p.km_viaje as 'KM VIAJE',p.observaciones as 'OBSERVACIONES PEDIDO'," +
                                            $"ISNULL(domc.dominio,'-----') as 'DOMINIO CAMION',ISNULL(cm.marca,'-----') as 'MARCA CAMION' ," +
                                            $"ISNULL(moca.modelo,'-----') as 'MODELO CAMION', CASE ISNULL(cast (ri.id_dominio as varchar(20)),'NULL') when 'NULL'  THEN 'TRACTOCAMION' ELSE 'RIGIDO' END as 'TIPO CAMION' ," +
                                            $"ISNULL(doma.dominio,'-----') as 'DOM ACOPLADO',ISNULL(am.marca,'-----') as 'MARCA ACOPLADO',ISNULL(amod.modelo,'-----') as 'MOD ACOPLADO', " +
                                            $"ISNULL(ch.num_legajo,-1) as 'LEGAJO'  ,concat (ISNULL(ch.nombre,'-----') ,', ',ISNULL(ch.apellido,'-----')) as 'NOMBRE Y APELLIDO',ISNULL(ch.direccion,'-----') as 'DIRECCION',ISNULL(ch.tel_personal,'-----') 'TELEFONO PERSONAL' ,ISNULL(ch.tel_trabajo,'-----') AS  'TELEFONO TRABAJO'  ,p.id_pedido as 'IDP'" + //id pedido
                                            $" from viaje v INNER JOIN pedido p on p.id_pedido=v.id_pedido  " +
                                            $"INNER JOIN unidades_viaje uv on uv.id=v.id_unidades_viaje  " +
                                            $"INNER JOIN camion c on uv.id_camion=c.id_dominio " +
                                            $"INNER JOIN dominio domc on domc.id=c.id_dominio " +
                                            $"INNER JOIN marca_camion cm on cm.id=c.marca " +
                                            $"INNER JOIN modelo_camion moca on moca.id=c.modelo " +
                                            $"LEFT OUTER JOIN  acoplado a on a.id_dominio=uv.id_acoplado " +
                                            $"LEFT OUTER JOIN  dominio doma on a.id_dominio=doma.id " +
                                            $"LEFT OUTER  JOIN marca_acoplado am on am.id=a.marca LEFT OUTER  JOIN modelo_acoplado amod on amod.id=a.modelo LEFT OUTER JOIN  chofer ch on p.id_chofer = ch.num_legajo LEFT OUTER JOIN rigido ri on ri.id_dominio =c.id_dominio  " +
                                    $"WHERE ( v.estado like 'PENDIENTE' or v.estado like 'EN CURSO' ) and   uv.id in ( select univ.id" +
                                            $"                                                 from unidades_viaje univ" +
                                            $"                                                 INNER JOIN dominio domi on univ.id_acoplado = domi.id or univ.id_camion=domi.id" +
                                            $"                                                 where domi.dominio like '{dominio}' )"

                                           , cnn);

                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar por busqueda de texto", e);
                }
            }
        }
    }
}