﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using CapaEntidades;
using System.Configuration;

namespace CapaDatos
{
    public class D_Pedido
    {
        SqlConnection cnn;
        SqlDataAdapter da;
        DataTable dt;
        E_Pedido e_pedido;
        string usuario;
        public D_Pedido(string usuario)
        {
            this.cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString);
            this.usuario = usuario;
            e_pedido = new E_Pedido();
            dt = this.listarTodos();
        }
        public void insertar(E_Pedido e_pedido)
        {
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;

                    cmd.CommandText = "insert into pedido values ('" + e_pedido.Id_chofer + "' , '" + e_pedido.Fecha_pedido + "','" + e_pedido.Fecha_salida + "'," +
                        "'" + e_pedido.Fecha_regreso + "','" + e_pedido.Nombre_cliente + "','" + e_pedido.Cuil_cliente + "','" + e_pedido.Peso_neto + "'," +
                        "'" + e_pedido.Volumen.ToString().Replace(',', '.') + "','" + e_pedido.Km_viaje.ToString().Replace(',', '.') + "','" + e_pedido.Enganche + "','" + e_pedido.Tipo_carga + "'," +
                        "'" + e_pedido.Altura_camion.ToString().Replace(',', '.') + "','" + e_pedido.Ancho_interior_camion.ToString().Replace(',', '.') + "','" + e_pedido.Ancho_exterior_camion.ToString().Replace(',', '.') + "'," +
                        "'" + e_pedido.Longitud_camion.ToString().Replace(',', '.') + "'," +
                        "'" + e_pedido.Altura_acoplado + "','" + e_pedido.Ancho_interior_acoplado + "','" + e_pedido.Ancho_exterior_acoplado + "'," +
                        "'" + e_pedido.Longitud_acoplado.ToString().Replace(',', '.') + "'," +
                        "'" + e_pedido.Observaciones + "','" + e_pedido.Prioridad + "','" + e_pedido.Estado + "')";
                    cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + "se inserto Pedido id=" + e_pedido.Id_pedido + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 2627: throw new System.Exception("No es posible Insertar Pedido", e);
                        default: throw new System.Exception("Ups, algo paso al insertar Pedido", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al insertar Pedido", e);
                }
                cnn.Close();
            }
        }
        public int baja(E_Pedido e_pedido)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;
                    cmd.CommandText = "update pedido set estado= 'CANCELADO' where id_pedido='" + e_pedido.Id_pedido + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " dio de baja Pedido id=" + e_pedido.Id_pedido + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible deshabilitar Pedido", e);
                        default: throw new System.Exception("Ups, algo paso al deshabilitar Pedido", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al deshabilitar Pedido", e);
                }
            }
        }
        public int modificar(E_Pedido e_pedido)
        {
            int num_rows = -1;
            SqlTransaction sqlTran;
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                cnn.Open();
                sqlTran = cnn.BeginTransaction();
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cmd.Connection = cnn;
                    cmd.Transaction = sqlTran;

                    cmd.CommandText = "update pedido set id_chofer='" + e_pedido.Id_chofer + "',fecha_pedido = '" + e_pedido.Fecha_pedido + "'," +
                        "fecha_salida='" + e_pedido.Fecha_salida + "',fecha_regreso='" + e_pedido.Fecha_regreso + "',nombre_cliente='" + e_pedido.Nombre_cliente + "'," +
                        "cuil_cliente='" + e_pedido.Cuil_cliente + "',peso_neto='" + e_pedido.Peso_neto.ToString().Replace(',', '.') + "',volumen='" + e_pedido.Volumen.ToString().Replace(',', '.') + "'," +
                        "km_viaje='" + e_pedido.Km_viaje.ToString().Replace(',', '.') + "',enganche='" + e_pedido.Enganche + "',tipo_carga='" + e_pedido.Tipo_carga + "'," +
                        "altura_camion='" + e_pedido.Altura_camion.ToString().Replace(',', '.') + "',ancho_interior_camion='" + e_pedido.Ancho_interior_camion.ToString().Replace(',', '.') + "'," +
                        "ancho_exterior_camion='" + e_pedido.Ancho_exterior_camion.ToString().Replace(',', '.') + "',longitud_camion='" + e_pedido.Longitud_camion.ToString().Replace(',', '.') + "'," +
                        "altura_acoplado='" + e_pedido.Altura_acoplado.ToString().Replace(',', '.') + "',ancho_interior_acoplado='" + e_pedido.Ancho_interior_acoplado.ToString().Replace(',', '.') + "'," +
                        "ancho_exterior_acoplado='" + e_pedido.Ancho_exterior_acoplado + "',longitud_acoplado='" + e_pedido.Longitud_acoplado.ToString().Replace(',', '.') + "'," +
                        "observaciones='" + e_pedido.Observaciones + "'," +
                        "prioridad='" + e_pedido.Prioridad + "',estado='" + e_pedido.Estado + "'" +
                        "where  id_pedido='" + e_pedido.Id_pedido + "';SELECT Scope_Identity();";
                    num_rows = cmd.ExecuteNonQuery();
                    cmd.CommandText = "insert into auditoria values ('" + (DateTime.Now).ToString() + "','" + usuario + "','" + " Modificó el Pedido id_pedido=" + e_pedido.Id_pedido + "')";
                    cmd.ExecuteNonQuery();
                    sqlTran.Commit();
                    cnn.Close();
                    return num_rows;
                }
                catch (SqlException e)
                {
                    SqlError err = e.Errors[0];
                    sqlTran.Rollback();
                    cnn.Close();
                    switch (err.Number)
                    {
                        case 547: throw new System.Exception("No es posible modificar Pedido ", e);
                        default: throw new System.Exception("Ups, algo paso al modificar Pedido", e);
                    }
                }
                catch (Exception e)
                {
                    sqlTran.Rollback();
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al modificar Pedido", e);
                }
            }
        }
        public E_Pedido retornaPedido(int id)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {

                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "select * from pedido where id_pedido = '" + id + "' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        e_pedido.Id_pedido = Convert.ToInt32(dr[0].ToString());
                        e_pedido.Id_chofer = Convert.ToInt32(dr[1].ToString());
                        e_pedido.Fecha_pedido = Convert.ToDateTime(dr[2].ToString());
                        e_pedido.Fecha_salida = Convert.ToDateTime(dr[3].ToString());
                        e_pedido.Fecha_regreso = Convert.ToDateTime(dr[4].ToString());
                        e_pedido.Nombre_cliente = dr[5].ToString();
                        e_pedido.Cuil_cliente = dr[6].ToString();
                        e_pedido.Peso_neto = Convert.ToInt64(dr[7]);
                        e_pedido.Volumen = Convert.ToInt64(dr[8]);
                        e_pedido.Km_viaje = Convert.ToInt64(dr[9]);
                        e_pedido.Enganche = dr[10].ToString();
                        e_pedido.Tipo_carga = dr[11].ToString();
                        e_pedido.Altura_camion = Convert.ToInt64(dr[12]);
                        e_pedido.Ancho_interior_camion = Convert.ToInt64(dr[13]);
                        e_pedido.Ancho_exterior_camion = Convert.ToInt64(dr[14]);
                        e_pedido.Longitud_camion = Convert.ToInt64(dr[15]);
                        e_pedido.Altura_acoplado = Convert.ToInt64(dr[16]);
                        e_pedido.Ancho_interior_acoplado = Convert.ToInt64(dr[17]);
                        e_pedido.Ancho_exterior_acoplado = Convert.ToInt64(dr[18]);
                        e_pedido.Longitud_acoplado = Convert.ToInt64(dr[19]);
                        e_pedido.Observaciones = dr[20].ToString();
                        e_pedido.Prioridad = dr[21].ToString();
                        e_pedido.Estado = dr[22].ToString();

                    }
                    cnn.Close();
                    return e_pedido;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retonar pedido", e);
                }
            }
        } //Funcion que retorna un objeto tipo acoplado
        public DataTable listarTodos()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select p.id_pedido 'Nº Pedido',c.num_legajo 'Nº Legajo',c.nombre + ' ' + c.apellido 'Nombre chofer', " +
                        "p.fecha_pedido 'Fecha pedido', p.fecha_salida 'Fecha salida', p.fecha_regreso 'Fecha regreso', p.nombre_cliente 'Nombre cliente',p.cuil_cliente 'Cuil'," +
                        "p.peso_neto 'Peso neto', p.volumen 'Volumen', p.km_viaje 'Km viaje', p.enganche 'Enganche',p.tipo_carga 'Tipo carga'," +
                        "p.altura_camion 'Altura camion',p.ancho_interior_camion 'Ancho interior camion',p.ancho_exterior_camion 'Ancho exterior camion'," +
                        "p.longitud_camion 'Longitud camion'," +
                        "p.altura_acoplado 'Altura acoplado',p.ancho_interior_acoplado 'Ancho interior acoplado',p.ancho_exterior_acoplado 'Ancho exterior acoplado'," +
                        "p.longitud_acoplado 'Longitud acoplado',p.observaciones 'Observaciones'," +
                        "p.prioridad 'Prioridad',p.estado 'Estado', p.id_pedido " +
                        "from pedido p join chofer c on p.id_chofer=c.num_legajo", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los pedidos", e);
                }
            }
        }
        public DataTable listarPendientes()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select p.id_pedido 'Nº Pedido',c.num_legajo 'Nº Legajo',c.nombre + ' ' + c.apellido 'Nombre chofer', " +
                        "p.fecha_pedido 'Fecha pedido', p.fecha_salida 'Fecha salida', p.fecha_regreso 'Fecha regreso', p.nombre_cliente 'Nombre cliente',p.cuil_cliente 'Cuil'," +
                        "p.peso_neto 'Peso neto', p.volumen 'Volumen', p.km_viaje 'Km viaje', p.enganche 'Enganche',p.tipo_carga 'Tipo carga'," +
                        "p.altura_camion 'Altura camion',p.ancho_interior_camion 'Ancho interior camion',p.ancho_exterior_camion 'Ancho exterior camion'," +
                        "p.longitud_camion 'Longitud camion'," +
                        "p.altura_acoplado 'Altura acoplado',p.ancho_interior_acoplado 'Ancho interior acoplado',p.ancho_exterior_acoplado 'Ancho exterior acoplado'," +
                        "p.longitud_acoplado 'Longitud acoplado',p.observaciones 'Observaciones'," +
                        "p.prioridad 'Prioridad',p.estado 'Estado', p.id_pedido " +
                        "from pedido p join chofer c on p.id_chofer=c.num_legajo where p.estado = 'PENDIENTE'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los pedidos pendientes", e);
                }
            }
        }
        public DataTable listarHabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select p.id_pedido 'Nº Pedido',c.num_legajo 'Nº Legajo',c.nombre + ' ' + c.apellido 'Nombre chofer', " +
                        "p.fecha_pedido 'Fecha pedido', p.fecha_salida 'Fecha salida', p.fecha_regreso 'Fecha regreso', p.nombre_cliente 'Nombre cliente',p.cuil_cliente 'Cuil'," +
                        "p.peso_neto 'Peso neto', p.volumen 'Volumen', p.km_viaje 'Km viaje', p.enganche 'Enganche',p.tipo_carga 'Tipo carga'," +
                        "p.altura_camion 'Altura camion',p.ancho_interior_camion 'Ancho interior camion',p.ancho_exterior_camion 'Ancho exterior camion'," +
                        "p.longitud_camion 'Longitud camion'," +
                        "p.altura_acoplado 'Altura acoplado',p.ancho_interior_acoplado 'Ancho interior acoplado',p.ancho_exterior_acoplado 'Ancho exterior acoplado'," +
                        "p.longitud_acoplado 'Longitud acoplado',p.observaciones 'Observaciones'," +
                        "p.prioridad 'Prioridad',p.estado 'Estado', p.id_pedido " +
                        "from pedido where estado = HABILITADO", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los pedidos habilitados", e);
                }
            }
        }
        public DataTable listarDeshabilitados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select p.id_pedido 'Nº Pedido',c.num_legajo 'Nº Legajo',c.nombre + ' ' + c.apellido 'Nombre chofer', " +
                        "p.fecha_pedido 'Fecha pedido', p.fecha_salida 'Fecha salida', p.fecha_regreso 'Fecha regreso', p.nombre_cliente 'Nombre cliente',p.cuil_cliente 'Cuil'," +
                        "p.peso_neto 'Peso neto', p.volumen 'Volumen', p.km_viaje 'Km viaje', p.enganche 'Enganche',p.tipo_carga 'Tipo carga'," +
                        "p.altura_camion 'Altura camion',p.ancho_interior_camion 'Ancho interior camion',p.ancho_exterior_camion 'Ancho exterior camion'," +
                        "p.longitud_camion 'Longitud camion'," +
                        "p.altura_acoplado 'Altura acoplado',p.ancho_interior_acoplado 'Ancho interior acoplado',p.ancho_exterior_acoplado 'Ancho exterior acoplado'," +
                        "p.longitud_acoplado 'Longitud acoplado',p.observaciones 'Observaciones'," +
                        "p.prioridad 'Prioridad',p.estado 'Estado', p.id_pedido " +
                        "from pedido where estado = DESHABILITADO", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al listar todos los pedidos deshabilitados", e);
                }
            }
        }
        public DataView buscar_pedido(int num_pedido)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "id_pedido = '" + num_pedido + "'";
                    return dv;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar pedido por numero de pedido", e);
                }
            }
        }  //Busca pedido
        public DataTable buscar_pedido_desde_hasta(string desde, string hasta)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select p.id_pedido 'Nº Pedido',c.num_legajo 'Nº Legajo',c.nombre + ' ' + c.apellido 'Nombre chofer'," +
                        "p.fecha_pedido 'Fecha pedido', p.fecha_salida 'Fecha salida', p.fecha_regreso 'Fecha regreso'," +
                        "p.nombre_cliente 'Nombre cliente',p.cuil_cliente 'Cuil'," +
                        "p.peso_neto 'Peso neto', p.volumen 'Volumen', p.km_viaje 'Km viaje', p.enganche 'Enganche',p.tipo_carga 'Tipo carga'," +
                        "p.altura_camion 'Altura camion',p.ancho_interior_camion 'Ancho interior camion',p.ancho_exterior_camion 'Ancho exterior camion'," +
                        "p.longitud_camion 'Longitud camion'," +
                        "p.altura_acoplado 'Altura acoplado',p.ancho_interior_acoplado 'Ancho interior acoplado',p.ancho_exterior_acoplado 'Ancho exterior acoplado'," +
                        "p.longitud_acoplado 'Longitud acoplado',p.observaciones 'Observaciones'," +
                        "p.prioridad 'Prioridad',p.estado 'Estado', p.id_pedido " +
                        "from pedido p join chofer c on p.id_chofer=c.num_legajo where p.fecha_pedido between TRY_PARSE('" + desde + "' AS datetime) and TRY_PARSE('" + hasta + "' AS datetime) ", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar pedido por fecha", e);
                }
            }
        }
        public DataView buscar_pedido_pendiente(int num_pedido)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "id_pedido = '" + num_pedido + "' and Estado = 'PENDIENTE'";
                    return dv;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar pedidos pendientes", e);
                }
            }

        }  //Busca pedido
        public DataTable buscar_pedido_desde_hasta_pendientes(string desde, string hasta)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    da = new SqlDataAdapter("select p.id_pedido 'Nº Pedido',c.num_legajo 'Nº Legajo',c.nombre + ' ' + c.apellido 'Nombre chofer'," +
                       "p.fecha_pedido 'Fecha pedido', p.fecha_salida 'Fecha salida', p.fecha_regreso 'Fecha regreso'," +
                       "p.nombre_cliente 'Nombre cliente',p.cuil_cliente 'Cuil'," +
                       "p.peso_neto 'Peso neto', p.volumen 'Volumen', p.km_viaje 'Km viaje', p.enganche 'Enganche',p.tipo_carga 'Tipo carga'," +
                       "p.altura_camion 'Altura camion',p.ancho_interior_camion 'Ancho interior camion',p.ancho_exterior_camion 'Ancho exterior camion'," +
                       "p.longitud_camion 'Longitud camion'," +
                       "p.altura_acoplado 'Altura acoplado',p.ancho_interior_acoplado 'Ancho interior acoplado',p.ancho_exterior_acoplado 'Ancho exterior acoplado'," +
                       "p.longitud_acoplado 'Longitud acoplado',p.observaciones 'Observaciones'," +
                       "p.prioridad 'Prioridad',p.estado 'Estado', p.id_pedido " +
                       "from pedido p join chofer c on p.id_chofer=c.num_legajo where p.fecha_pedido between TRY_PARSE('" + desde + "' AS datetime) and TRY_PARSE('" + hasta + "' AS datetime) and p.estado = 'PENDIENTE'", cnn);
                    dt = new DataTable();
                    da.Fill(dt);
                    cnn.Close();
                    return dt;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al buscar pedido pendientes por fecha", e);
                }
            }
        }
        public List<string> retornaPrioridades()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    List<string> prioridades = new List<string>();
                    prioridades.Add("ALTA");
                    prioridades.Add("NORMAL");
                    prioridades.Add("BAJA");

                    return prioridades;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar prioridades", e);
                }
            }
        }
        public List<string> retornaEstados()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                try
                {
                    cnn.Open();
                    List<string> estados = new List<string>();
                    estados.Add("PENDIENTE");
                    estados.Add("CARGADO");
                    estados.Add("CANCELADO");
                    cnn.Close();
                    return estados;
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar prioridades", e);
                }
            }

        }
        public int retornaIdUltimoPedido()
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                int resp = 0;
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = "SELECT top 1 * FROM pedido ORDER BY id_pedido DESC ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        resp = Convert.ToInt32(dr[0]);
                    }
                    cnn.Close();
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID ultimo pedido", e);
                }
                return resp;
            }
        }
        public string retornaEstado(int id_pedido)
        {
            using (cnn = new SqlConnection(ConfigurationManager.ConnectionStrings["sqlCon"].ConnectionString))
            {
                string estado = String.Empty;
                try
                {
                    SqlCommand cmd = new SqlCommand();
                    cnn.Open();
                    cmd.Connection = cnn;
                    cmd.CommandText = $"SELECT estado  FROM pedido WHERE id_pedido = '{id_pedido}' ";
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        estado = dr[0].ToString();
                    }
                    cnn.Close();
                }
                catch (Exception e)
                {
                    cnn.Close();
                    throw new System.Exception("Ups, algo paso al retornar ID ultimo pedido", e);
                }
                return estado;
            }
        }
    }
}
