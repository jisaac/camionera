USE [logistica]
GO
/**CARGA ROLES**/
/**Roles -> (nombre, estado)**/
INSERT INTO roles VALUES('OPERACIONES','HABILITADO');
INSERT INTO roles VALUES('LOGISTICA','HABILITADO');
INSERT INTO roles VALUES('MANTENIMIENTO','HABILITADO');

/**CARGA USUARIOS**/
/**Usuarios -> (nombre,password,num_legajo,id_rol,estado)**/
INSERT INTO usuarios VALUES('O1','O1',1,1,'HABILITADO');
INSERT INTO usuarios VALUES('O2','O2',2,1,'HABILITADO');
INSERT INTO usuarios VALUES('O3','O3',3,1,'HABILITADO');
INSERT INTO usuarios VALUES('L1','L1',4,2,'HABILITADO');
INSERT INTO usuarios VALUES('L2','L2',5,2,'HABILITADO');
INSERT INTO usuarios VALUES('L3','L3',6,2,'HABILITADO');
INSERT INTO usuarios VALUES('M1','M1',7,3,'HABILITADO');
INSERT INTO usuarios VALUES('M2','M2',8,3,'HABILITADO');
INSERT INTO usuarios VALUES('M3','M3',9,3,'HABILITADO');

/**CARGA CHOFERES**/
/**Choferes -> (num_legajo,nombre,apellido,num_cedula,direcci�n,tel_personal,tel_trabajo,estado)**/
INSERT INTO chofer VALUES(1,'JUAN','QUIROGA','CH0001','ITALIA 403','2664302010','460503','HABILITADO');
INSERT INTO chofer VALUES(2,'TOMAS','FERNANDEZ', 'CH0002','BELGICA 1506','2664031034','459293','HABILITADO');
INSERT INTO chofer VALUES(3,'LUIS','FUNES','CH0003','FRANCIA 430','2664039194','469129','HABILITADO');
INSERT INTO chofer VALUES(4,'DAMIAN','PEREZ','CH0004','BRASIL 3500','2657348134','489634','HABILITADO');
INSERT INTO chofer VALUES(5,'SANTIAGO','ALBA','CH0005','ALAMOS 290','2657838143','454245','HABILITADO');
INSERT INTO chofer VALUES(6,'ALAN','VILLALVA','CH0006','PERU 1140','2657913481','452234','HABILITADO');
INSERT INTO chofer VALUES(7,'FEDERICO','SOLOA','CH0007','BOLIVIA 340','2664991395','435353','HABILITADO');
INSERT INTO chofer VALUES(8,'DARIO','ROCA','CH0008','KOSLAY 100','2664501035','452123','HABILITADO');
INSERT INTO chofer VALUES(9,'FRANCO','MARTINEZ','CH0009','SAN LUIS 9341','2664959534','491892','HABILITADO');
INSERT INTO chofer VALUES(10,'JUAN','GODOY','CH00010','CHICAGO 953','2657384192','443234','HABILITADO');


/**CARGA TIPO DE CARGA **/
/**Tipo de carga -> (tipo,estado)**/
INSERT INTO tipo_carga VALUES('MAIZ','HABILITADO');
INSERT INTO tipo_carga VALUES('TRIGO','HABILITADO');
INSERT INTO tipo_carga VALUES('SOJA','HABILITADO');
INSERT INTO tipo_carga VALUES('GARBANZO','HABILITADO');
INSERT INTO tipo_carga VALUES('VACUNO','HABILITADO');
INSERT INTO tipo_carga VALUES('CHANCHOS','HABILITADO');
INSERT INTO tipo_carga VALUES('HIERRO AL8','HABILITADO');
INSERT INTO tipo_carga VALUES('TRIZZETERO','HABILITADO');


/**REGION ACOPLADOS**/

/**CARGA MARCA ACOPLADOS**/
/**Marca Acoplados -> (marca,estado)**/
INSERT INTO marca_acoplado VALUES('AGRALE','HABILITADO');
INSERT INTO marca_acoplado VALUES('SEILER','HABILITADO');
INSERT INTO marca_acoplado VALUES('METALPAMP','HABILITADO');
INSERT INTO marca_acoplado VALUES('BONANO','HABILITADO');
INSERT INTO marca_acoplado VALUES('LAMBERT','HABILITADO');
INSERT INTO marca_acoplado VALUES('RAMBERT','HABILITADO');
INSERT INTO marca_acoplado VALUES('RANDOM','HABILITADO');
INSERT INTO marca_acoplado VALUES('MONTENEGRO','HABILITADO');
INSERT INTO marca_acoplado VALUES('HELVETICA','HABILITADO');
INSERT INTO marca_acoplado VALUES('SALTO','HABILITADO');

/**CARGA MODELO ACOPLADOS**/
/**Modelo acoplado -> (modelo,estado,id_marca)**/
INSERT INTO modelo_acoplado VALUES('AG303','HABILITADO',1);
INSERT INTO modelo_acoplado VALUES('AG506','HABILITADO',1);
INSERT INTO modelo_acoplado VALUES('SE402','HABILITADO',2);
INSERT INTO modelo_acoplado VALUES('SE102','HABILITADO',2);
INSERT INTO modelo_acoplado VALUES('ME042','HABILITADO',3);
INSERT INTO modelo_acoplado VALUES('ME103','HABILITADO',3);
INSERT INTO modelo_acoplado VALUES('BO593','HABILITADO',4);
INSERT INTO modelo_acoplado VALUES('BO489','HABILITADO',4);
INSERT INTO modelo_acoplado VALUES('LA301','HABILITADO',5);
INSERT INTO modelo_acoplado VALUES('LA034','HABILITADO',5);
INSERT INTO modelo_acoplado VALUES('RAM032','HABILITADO',6);
INSERT INTO modelo_acoplado VALUES('RAM942','HABILITADO',6);
INSERT INTO modelo_acoplado VALUES('RA690','HABILITADO',7);
INSERT INTO modelo_acoplado VALUES('RA875','HABILITADO',7);
INSERT INTO modelo_acoplado VALUES('MO942','HABILITADO',8);
INSERT INTO modelo_acoplado VALUES('MO756','HABILITADO',8);
INSERT INTO modelo_acoplado VALUES('HE764','HABILITADO',9);
INSERT INTO modelo_acoplado VALUES('HE908','HABILITADO',9);
INSERT INTO modelo_acoplado VALUES('SA653','HABILITADO',10);
INSERT INTO modelo_acoplado VALUES('SA254','HABILITADO',10);


/**CARGA ACOPLADO**/
/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('AA000PO');
/**Acoplado -> (dominio,nro_chasis,marca,modelo,a�o,tipo_carga,tipo_acoplado,tara,capacidad_carga,volume,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,km_cambio_neumaticos_suma,altura,ancho_interior,anchor_exterior,longitud,cant_ejes,fecha_alta,estado)**/
INSERT INTO acoplado VALUES(1,1,2,2018,1,'REMOLQUE',50000,50000,50000,0,100000,50000,100000,50000,2,3,4,10,'05-05-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('BB304TT');
/**Acoplado -> (dominio,nro_chasis,marca,modelo,a�o,tipo_carga,tipo_acoplado,tara,capacidad_carga,volume,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,km_cambio_neumaticos_suma,altura,ancho_interior,anchor_exterior,longitud,cant_ejes,fecha_alta,estado)**/
INSERT INTO acoplado VALUES(2,2,3,2018,1,'REMOLQUE',50000,50000,50000,0,100000,50000,100000,50000,2,3,4,10,'03-02-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('LO945IW');
/**Acoplado -> (dominio,nro_chasis,marca,modelo,a�o,tipo_carga,tipo_acoplado,tara,capacidad_carga,volume,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,km_cambio_neumaticos_suma,altura,ancho_interior,anchor_exterior,longitud,cant_ejes,fecha_alta,estado)**/
INSERT INTO acoplado VALUES(3,1,1,2018,2,'REMOLQUE',50000,50000,50000,0,100000,50000,100000,50000,2,3,4,10,'10-02-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('AB031XC');
/**Acoplado -> (dominio,nro_chasis,marca,modelo,a�o,tipo_carga,tipo_acoplado,tara,capacidad_carga,volume,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,km_cambio_neumaticos_suma,altura,ancho_interior,anchor_exterior,longitud,cant_ejes,fecha_alta,estado)**/
INSERT INTO acoplado VALUES(4,3,5,2018,3,'SEMIREMOLQUE',50000,50000,50000,0,100000,50000,100000,50000,2,3,4,10,'15-06-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('AB331XC');
/**Acoplado -> (dominio,nro_chasis,marca,modelo,a�o,tipo_carga,tipo_acoplado,tara,capacidad_carga,volume,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,km_cambio_neumaticos_suma,altura,ancho_interior,anchor_exterior,longitud,cant_ejes,fecha_alta,estado)**/
INSERT INTO acoplado VALUES(5,4,7,2018,3,'SEMIREMOLQUE',50000,50000,50000,0,100000,50000,100000,50000,2,3,4,10,'03-05-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('AD331XC');
/**Acoplado -> (dominio,nro_chasis,marca,modelo,a�o,tipo_carga,tipo_acoplado,tara,capacidad_carga,volume,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,km_cambio_neumaticos_suma,altura,ancho_interior,anchor_exterior,longitud,cant_ejes,fecha_alta,estado)**/
INSERT INTO acoplado VALUES(6,1,2,2018,1,'SEMIREMOLQUE',50000,50000,50000,0,100000,50000,100000,50000,2,3,4,10,'08-05-2018','HABILITADO');


/**FIN REGION ACOPLADOS**/


/**REGION CAMIONES**/

/**MARCA CAMIONES**/
/**Marca Camion -> (nombre,estado)**/
INSERT INTO marca_camion VALUES('FORD','HABILITADO');
INSERT INTO marca_camion VALUES('SCANIA','HABILITADO');
INSERT INTO marca_camion VALUES('IVECO','HABILITADO');
INSERT INTO marca_camion VALUES('MERCEDES BENZ','HABILITADO');
INSERT INTO marca_camion VALUES('RENAULT','HABILITADO');
INSERT INTO marca_camion VALUES('VOLKSWAGEN','HABILITADO');

/**MODELO CAMIONES**/
/**Modelo camiones -> (modelo,estado,id_marca)**/
INSERT INTO modelo_camion VALUES('FO023','HABILITADO',1);
INSERT INTO modelo_camion VALUES('FO204','HABILITADO',1);
INSERT INTO modelo_camion VALUES('SC231','HABILITADO',2);
INSERT INTO modelo_camion VALUES('SC403','HABILITADO',2);
INSERT INTO modelo_camion VALUES('IV013','HABILITADO',3);
INSERT INTO modelo_camion VALUES('IV453','HABILITADO',3);
INSERT INTO modelo_camion VALUES('MB931','HABILITADO',4);
INSERT INTO modelo_camion VALUES('MB945','HABILITADO',4);
INSERT INTO modelo_camion VALUES('RE193','HABILITADO',5);
INSERT INTO modelo_camion VALUES('RE021','HABILITADO',5);
INSERT INTO modelo_camion VALUES('VW012','HABILITADO',6);
INSERT INTO modelo_camion VALUES('VW912','HABILITADO',6);


/**CARGA CAMION**/
/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('AC030PR');
/**Camion -> (dominio,nro_chasis,nro_motor,marca,modelo,a�o,tara,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,fecha_alta,estado)**/
INSERT INTO camion VALUES(7,1,2,2018,5000,0,100000,80000,100000,80000,'01-02-2018','HABILITADO');

/**camion agregado por el marcos carbajal**/
/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('KLO111');
/**Camion -> (dominio,nro_chasis,nro_motor,marca,modelo,a�o,tara,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,fecha_alta,estado)**/
INSERT INTO camion VALUES(8,1,2,2018,5000,0,100000,80000,100000,80000,'01-02-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('BR304DR');
/**Camion -> (dominio,nro_chasis,nro_motor,marca,modelo,a�o,tara,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,fecha_alta,estado)**/
INSERT INTO camion VALUES(9,1,1,2018,5000,0,100000,80000,100000,80000,'07-04-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('BT564GR');
/**Camion -> (dominio,nro_chasis,nro_motor,marca,modelo,a�o,tara,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,fecha_alta,estado)**/
INSERT INTO camion VALUES(10,2,3,2018,5000,0,100000,80000,100000,80000,'17-04-2018','HABILITADO');

/**Dominio -> (dominio)**/
INSERT INTO dominio VALUES('CW341TG');
/**Camion -> (dominio,nro_chasis,nro_motor,marca_modelo,a�o,tara,km_unidad,km_service,km_cambio_neumaticos,km_service_suma,fecha_alta,estado)**/
INSERT INTO camion VALUES(11,3,5,2018,5000,0,100000,80000,100000,80000,'20-08-2018','HABILITADO');

/**CARGA RIGIDO**/
/**Rigido -> (dominio,tipo_carga,altura,ancho_interior,ancho_exterior,longitud,volumen,capacidad_carga,enganche)**/
INSERT INTO rigido VALUES(11,1,2,3,3.4,10,5000,5000,0);
INSERT INTO rigido VALUES(10,1,2,3,3.4,10,5000,5000,1);
INSERT INTO rigido VALUES(9,1,2,3,3.4,10,5000,5000,0);

/**CARGA TRACTOCAMION**/
/**Tractocamion -> (dominio)**/
INSERT INTO tractocamion VALUES(7);
INSERT INTO tractocamion VALUES(8);
/**FIN REGION CAMIONES**/

/**CARGA PEDIDOS**/
/** PEDIDO -> (id_chofer,fecha_pedido,fecha_salida,fecha_regreso,nombre_cliente,cuil_cliente,peso_neto,volumen,km_viaje,enganche,tipo_carga,altura_camion,ancho_interior_camion,
ancho_exterior_camion,longitud_camion,altura_acoplado,ancho_interior_acoplado,ancho_exterior_acoplado,longitud_acoplado,observaciones,prioridad,estado**/
INSERT INTO pedido VALUES(1,'10-07-2018','10-07-2018','15-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','MAIZ',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(2,'10-07-2018','16-07-2018','25-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','MAIZ',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(3,'10-07-2018','26-07-2018','10-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','MAIZ',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(4,'10-08-2018','10-08-2018','15-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','MAIZ',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(5,'10-08-2018','10-08-2018','15-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','MAIZ',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(3,'10-08-2018','10-08-2018','15-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','SOJA',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(1,'10-08-2018','10-08-2018','15-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','SOJA',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(3,'10-08-2018','26-08-2018','10-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','SOJA',2,3,3,5,'','','','','','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(4,'10-08-2018','26-08-2018','10-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE RIGIDO','SOJA',2,3,3,5,'','','','','','NORMAL','PENDIENTE');

INSERT INTO pedido VALUES(1,'25-07-2018','10-07-2018','20-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(2,'25-07-2018','15-07-2018','25-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(3,'25-07-2018','10-07-2018','25-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(1,'25-08-2018','10-08-2018','19-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(2,'25-08-2018','20-08-2018','25-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(4,'25-08-2018','19-08-2018','25-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(5,'25-08-2018','15-08-2018','20-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'SIN ENGANCHE SEMIREMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');

INSERT INTO pedido VALUES(1,'02-07-2018','10-07-2018','20-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(2,'02-07-2018','15-07-2018','25-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(3,'02-07-2018','10-07-2018','25-07-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(1,'02-08-2018','10-08-2018','19-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','MAIZ',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(2,'02-08-2018','20-08-2018','25-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(4,'02-08-2018','19-08-2018','25-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');
INSERT INTO pedido VALUES(5,'02-08-2018','15-08-2018','20-08-2018','MIGUEL PEREZ','20403020302',500,500,500,'ENGANCHE CON REMOLQUE','SOJA',2,3,3,5,2,3,3,7,'','NORMAL','PENDIENTE');


