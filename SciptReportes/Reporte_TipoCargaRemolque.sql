CREATE PROCEDURE Reporte_TipoCargaRemolque
@fecha1 datetime, @fecha2 datetime
AS
BEGIN
	Select count(t.tipo) as 'Cantidad de Unidades', t.tipo 'Tipo de Carga' 
	from tipo_carga t join acoplado a on a.tipo_carga=t.id 
	join unidades_viaje uv on uv.id_acoplado=a.id_dominio 
	join viaje v on v.id_unidades_viaje=uv.id 
	where a.tipo_acoplado like 'SEMIREMOLQUE' and v.estado like 'CONCLUIDO' and v.fecha_salida > @fecha1 and v.fecha_regreso < @fecha2
	group by t.tipo
END
GO
