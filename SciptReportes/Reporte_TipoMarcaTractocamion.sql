CREATE PROCEDURE Reporte_TipoMarcaTractocamion
@fecha1 datetime, @fecha2 datetime
AS
BEGIN
	Select count(m.marca) as 'Cantidad de Unidades', m.marca 'Marca' 
	from marca_camion m join camion c on c.marca=m.id
	join tractocamion tr on tr.id_dominio=c.id_dominio
	join unidades_viaje uv on uv.id_camion=c.id_dominio 
	join viaje v on v.id_unidades_viaje=uv.id 
	where v.estado like 'CONCLUIDO' and v.fecha_salida > @fecha1 and v.fecha_regreso < @fecha2
	group by m.marca
END
GO