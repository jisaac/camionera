CREATE PROCEDURE Reporte_TiposCargasMasSolicitados
@fecha1 datetime, @fecha2 datetime
AS
BEGIN
	select count(p.tipo_carga) as 'Cantidad de unidades', p.tipo_carga 'Tipo de carga'
	from viaje v join unidades_viaje uv on v.id_unidades_viaje=uv.id
	join pedido p on v.id_pedido=v.id_pedido
	where p.estado like 'CARGADO' and v.estado like 'CONCLUIDO' and v.fecha_salida > @fecha1 and v.fecha_regreso < @fecha2
	group by (p.tipo_carga)
END
GO