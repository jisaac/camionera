CREATE PROCEDURE Reporte_UnidadesTaller
AS
BEGIN
	select count(ta.id_taller) 'Cantidad de veces',d.dominio 'Dominio', c.km_unidad as 'KM Unidad'
	from taller ta join camion c on ta.id_unidad=c.id_dominio
	join dominio d on d.id=c.id_dominio
	group by d.dominio,c.km_unidad
	order by c.km_unidad,d.dominio asc
END
GO