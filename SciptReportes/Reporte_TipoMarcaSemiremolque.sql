CREATE PROCEDURE Reporte_TipoMarcaSemiremolque
@fecha1 datetime, @fecha2 datetime
AS
BEGIN
	Select count(m.marca) as 'Cantidad de Unidades', m.marca 'Marca' 
	from marca_acoplado m join acoplado a on a.marca=m.id
	join unidades_viaje uv on uv.id_acoplado=a.id_dominio 
	join viaje v on v.id_unidades_viaje=uv.id 
	where a.tipo_acoplado like 'SEMIREMOLQUE' and v.estado like 'CONCLUIDO' and v.fecha_salida > @fecha1 and v.fecha_regreso < @fecha2
	group by m.marca
END
GO
