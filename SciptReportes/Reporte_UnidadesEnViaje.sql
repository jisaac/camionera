CREATE PROCEDURE Reporte_UnidadesEnViaje
@fecha1 datetime, @fecha2 datetime
AS
BEGIN
	(select d.dominio as 'Dominio', mc.marca as 'Marca', moc.modelo as 'Modelo', c.a�o as 'A�o', v.fecha_salida as 'Fecha salida', v.fecha_regreso as 'Fecha regreso'
	from camion c join unidades_viaje uv on c.id_dominio=uv.id_camion
	join viaje v on v.id_unidades_viaje=uv.id
	join marca_camion mc on c.marca=mc.id
	join modelo_camion moc on c.modelo=moc.id
	join dominio d on c.id_dominio=d.id
	where v.estado like 'EN CURSO' and v.fecha_salida > @fecha1 and v.fecha_regreso < @fecha2
	)
	union all
	(select d.dominio as 'Dominio', ma.marca as 'Marca', moa.modelo as 'Modelo', a.a�o as 'A�o', v.fecha_salida as 'Fecha salida', v.fecha_regreso as 'Fecha regreso'
	from acoplado a join unidades_viaje uv on a.id_dominio=uv.id_acoplado
	join viaje v on v.id_unidades_viaje=uv.id
	join marca_acoplado ma on a.marca=ma.id
	join modelo_acoplado moa on a.modelo=moa.id
	join dominio d on a.id_dominio=d.id
	where v.estado like 'EN CURSO' and v.fecha_salida > @fecha1 and v.fecha_regreso < @fecha2)
END
GO