USE [master]
GO
/****** Object:  Database [logistica]    Script Date: 6/6/2018 19:25:30 ******/
CREATE DATABASE [logistica]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'logistica', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\logistica.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'logistica_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\logistica_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [logistica] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [logistica].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [logistica] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [logistica] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [logistica] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [logistica] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [logistica] SET ARITHABORT OFF 
GO
ALTER DATABASE [logistica] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [logistica] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [logistica] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [logistica] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [logistica] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [logistica] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [logistica] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [logistica] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [logistica] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [logistica] SET  DISABLE_BROKER 
GO
ALTER DATABASE [logistica] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [logistica] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [logistica] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [logistica] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [logistica] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [logistica] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [logistica] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [logistica] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [logistica] SET  MULTI_USER 
GO
ALTER DATABASE [logistica] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [logistica] SET DB_CHAINING OFF 
GO
ALTER DATABASE [logistica] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [logistica] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [logistica] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [logistica] SET QUERY_STORE = OFF
GO
USE [logistica]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [logistica]
GO
/****** Object:  Table [dbo].[acoplado]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[acoplado](
	[id_dominio] [int] NOT NULL,
	[marca] [int] NOT NULL,
	[modelo] [int] NOT NULL,
	[año] [int] NULL,
	[tipo_carga] [int] NOT NULL,
	[tipo_acoplado] [nvarchar](20) NOT NULL,
	[tara] [float] NULL,
	[capacidad_carga] [float] NOT NULL,
	[volumen] [float] NOT NULL,
	[km_unidad] [float] NOT NULL,
	[km_service] [float] NOT NULL,
	[km_cambio_neumaticos] [float] NOT NULL,
	[km_service_suma] [float] NOT NULL,
	[km_cambio_neumaticos_suma] [float] NOT NULL,
	[altura] [float] NOT NULL,
	[ancho_interior] [float] NOT NULL,
	[ancho_exterior] [float] NOT NULL,
	[longitud] [float] NOT NULL,
	[fecha_alta] [datetime] NULL,
	[estado] [nvarchar](30) NOT NULL,
 CONSTRAINT [AK_dominio_acoplado] UNIQUE NONCLUSTERED 
(
	[id_dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[auditoria]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[auditoria](
	[fecha_hr] [nvarchar](50) NOT NULL,
	[usuario_auditoria] [nvarchar](50) NOT NULL,
	[accion] [nvarchar](200) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[camion]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[camion](
	[id_dominio] [int] NOT NULL,
	[marca] [int] NOT NULL,
	[modelo] [int] NOT NULL,
	[año] [int] NULL,
	[tara] [float] NULL,
	[km_unidad] [float] NOT NULL,
	[km_service] [float] NOT NULL,
	[km_cambio_neumaticos] [float] NOT NULL,
	[km_service_suma] [float] NOT NULL,
	[km_cambio_neumaticos_suma] [float] NOT NULL,
	[fecha_alta] [datetime] NULL,
	[estado] [nvarchar](30) NULL,
 CONSTRAINT [AK_dominio_camion] UNIQUE NONCLUSTERED 
(
	[id_dominio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[chofer]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[chofer](
	[num_legajo] [int] NOT NULL,
	[nombre] [nvarchar](20) NOT NULL,
	[apellido] [nvarchar](20) NOT NULL,
	[num_cedula] [nvarchar](20) NOT NULL,
	[direccion] [nvarchar](20) NOT NULL,
	[tel_personal] [nvarchar](30) NOT NULL,
	[tel_trabajo] [nvarchar](30) NULL,
	[estado] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_NumCedula] UNIQUE NONCLUSTERED 
(
	[num_cedula] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[dominio]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dominio](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[dominio] [nvarchar](20) UNIQUE NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[marca_acoplado]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[marca_acoplado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[marca] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_MarcaAcoplado] UNIQUE NONCLUSTERED 
(
	[marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[marca_camion]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[marca_camion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[marca] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_MarcaCamion] UNIQUE NONCLUSTERED 
(
	[marca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[mecanico]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[mecanico](
	[num_legajo] [int] NOT NULL,
	[nombre] [nvarchar](20) NOT NULL,
	[apellido] [nvarchar](20) NOT NULL,
	[direccion] [nvarchar](30) NOT NULL,
	[telefono1] [nvarchar](20) NOT NULL,
	[telefono2] [nvarchar](20) NULL,
	[estado] [nvarchar](30) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modelo_acoplado]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modelo_acoplado](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modelo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
	[id_marca] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_ModeloAcoplado] UNIQUE NONCLUSTERED 
(
	[modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[modelo_camion]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[modelo_camion](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[modelo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
	[id_marca] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_ModeloCamion] UNIQUE NONCLUSTERED 
(
	[modelo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pedido]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pedido](
	[id_pedido] [int] IDENTITY(1,1) NOT NULL,
	[id_chofer] [int] NOT NULL,
	[fecha_pedido] [datetime] NOT NULL,
	[fecha_salida] [datetime] NOT NULL,
	[fecha_regreso] [datetime] NOT NULL,
	[nombre_cliente] [nvarchar](50) NOT NULL,
	[cuil_cliente] [nvarchar](15) NOT NULL,
	[peso_neto] [float] NOT NULL,
	[volumen] [float] NOT NULL,
	[km_viaje] [float] NOT NULL,
	[enganche] [nvarchar](35) NULL,
	[tipo_carga] [nvarchar](20) NULL,
	[altura_camion] [float] NULL,
	[ancho_interior_camion] [float] NULL,
	[ancho_exterior_camion] [float] NULL,
	[longitud_camion] [float] NULL,
	[altura_acoplado] [float] NULL,
	[ancho_interior_acoplado] [float] NULL,
	[ancho_exterior_acoplado] [float] NULL,
	[longitud_acoplado] [float] NULL,
	[observaciones] [text] NULL,
	[prioridad] [nvarchar](20) NOT NULL,
	[estado] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_pedido] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[rigido]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[rigido](
	[id_dominio] [int] NOT NULL,
	[tipo_carga] [int] NOT NULL,
	[altura] [float] NOT NULL,
	[ancho_interior] [float] NOT NULL,
	[ancho_exterior] [float] NOT NULL,
	[longitud] [float] NOT NULL,
	[volumen] [float] NOT NULL,
	[capacidad_carga] [float] NOT NULL,
	[enganche] [bit] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[roles]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[roles](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[rol] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_Rol] UNIQUE NONCLUSTERED 
(
	[rol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[taller]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[taller](
	[id_taller] [int] IDENTITY(1,1) NOT NULL,
	[usuario] [nvarchar](50) NOT NULL,
	[entrada_taller] [datetime] NOT NULL,
	[salida_aprox] [datetime] NOT NULL,
	[salida] [datetime] NULL,
	[observaciones] [text] NULL,
	[id_unidad][int] ,
    [Tipo] [nvarchar] (300) NULL,
	[estado] [nvarchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id_taller] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tipo_carga]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tipo_carga](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[tipo] [nvarchar](50) NOT NULL,
	[estado] [nvarchar](30) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_TipoCarga] UNIQUE NONCLUSTERED 
(
	[tipo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tractocamion]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tractocamion](
	[id_dominio] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[unidades_viaje]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[unidades_viaje](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_camion] [int] NOT NULL,
	[id_acoplado] [int] NULL,
 CONSTRAINT [PK__unidades__3213E83F546EADE7] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[usuarios]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[usuarios](
	[usuario] [nvarchar](50) NOT NULL,
	[password] [nvarchar](50) NOT NULL,
	[num_legajo] [int] NOT NULL,
	[id_rol] [int] NOT NULL,
	[estado] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[usuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_NumLegajo] UNIQUE NONCLUSTERED 
(
	[num_legajo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[viaje]    Script Date: 6/6/2018 19:25:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[viaje](
	[id_viaje] [int] IDENTITY(1,1) NOT NULL,
	[id_pedido] [int] NOT NULL,
	[id_unidades_viaje] [int] NOT NULL,
	[fecha_salida] [datetime] NULL,
	[fecha_regreso] [datetime] NULL,
	[km_cierre] [float] NULL,
	[estado] [nvarchar](20) NULL,
	[observaciones] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[id_viaje] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [FK_dominio_acoplado] FOREIGN KEY([id_dominio])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [FK_dominio_acoplado]
GO
ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [FK_marca_acoplado] FOREIGN KEY([marca])
REFERENCES [dbo].[marca_acoplado] ([id])
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [FK_marca_acoplado]
GO
ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [FK_modelo_acoplado] FOREIGN KEY([modelo])
REFERENCES [dbo].[modelo_acoplado] ([id])
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [FK_modelo_acoplado]
GO
ALTER TABLE [dbo].[acoplado]  WITH CHECK ADD  CONSTRAINT [FK_tipoCarga_acoplado] FOREIGN KEY([tipo_carga])
REFERENCES [dbo].[tipo_carga] ([id])
GO
ALTER TABLE [dbo].[acoplado] CHECK CONSTRAINT [FK_tipoCarga_acoplado]
GO
ALTER TABLE [dbo].[camion]  WITH CHECK ADD  CONSTRAINT [FK_dominio_camion] FOREIGN KEY([id_dominio])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[camion] CHECK CONSTRAINT [FK_dominio_camion]
GO
ALTER TABLE [dbo].[camion]  WITH CHECK ADD  CONSTRAINT [FK_marca_camion] FOREIGN KEY([marca])
REFERENCES [dbo].[marca_camion] ([id])
GO
ALTER TABLE [dbo].[camion] CHECK CONSTRAINT [FK_marca_camion]
GO
ALTER TABLE [dbo].[camion]  WITH CHECK ADD  CONSTRAINT [FK_modelo_camion] FOREIGN KEY([modelo])
REFERENCES [dbo].[modelo_camion] ([id])
GO
ALTER TABLE [dbo].[camion] CHECK CONSTRAINT [FK_modelo_camion]
GO
ALTER TABLE [dbo].[modelo_acoplado]  WITH CHECK ADD  CONSTRAINT [FK_IdMarcaAcoplado] FOREIGN KEY([id_marca])
REFERENCES [dbo].[marca_acoplado] ([id])
GO
ALTER TABLE [dbo].[modelo_acoplado] CHECK CONSTRAINT [FK_IdMarcaAcoplado]
GO
ALTER TABLE [dbo].[modelo_camion]  WITH CHECK ADD  CONSTRAINT [FK_IdMarcaCamion] FOREIGN KEY([id_marca])
REFERENCES [dbo].[marca_camion] ([id])
GO
ALTER TABLE [dbo].[modelo_camion] CHECK CONSTRAINT [FK_IdMarcaCamion]
GO
ALTER TABLE [dbo].[pedido]  WITH CHECK ADD  CONSTRAINT [FK_IdChofer] FOREIGN KEY([id_chofer])
REFERENCES [dbo].[chofer] ([num_legajo])
GO
ALTER TABLE [dbo].[pedido] CHECK CONSTRAINT [FK_IdChofer]
GO
ALTER TABLE [dbo].[rigido]  WITH CHECK ADD  CONSTRAINT [FK_dominioRigido] FOREIGN KEY([id_dominio])
REFERENCES [dbo].[camion] ([id_dominio])
GO
ALTER TABLE [dbo].[rigido] CHECK CONSTRAINT [FK_dominioRigido]
GO
ALTER TABLE [dbo].[rigido]  WITH CHECK ADD  CONSTRAINT [FK_tipoCargaRigido] FOREIGN KEY([tipo_carga])
REFERENCES [dbo].[tipo_carga] ([id])
GO
ALTER TABLE [dbo].[rigido] CHECK CONSTRAINT [FK_tipoCargaRigido]
GO
ALTER TABLE [dbo].[taller]  WITH CHECK ADD  CONSTRAINT [FK_idUnidad] FOREIGN KEY([id_unidad])
REFERENCES [dbo].[dominio] ([id])
GO
ALTER TABLE [dbo].[taller] CHECK CONSTRAINT [FK_idUnidad]
GO
ALTER TABLE [dbo].[tractocamion]  WITH CHECK ADD  CONSTRAINT [FK_dominioTractocamion] FOREIGN KEY([id_dominio])
REFERENCES [dbo].[camion] ([id_dominio])
GO
ALTER TABLE [dbo].[tractocamion] CHECK CONSTRAINT [FK_dominioTractocamion]
GO
ALTER TABLE [dbo].[usuarios]  WITH CHECK ADD  CONSTRAINT [FK_idRol] FOREIGN KEY([id_rol])
REFERENCES [dbo].[roles] ([id])
GO
ALTER TABLE [dbo].[usuarios] CHECK CONSTRAINT [FK_idRol]
GO
ALTER TABLE [dbo].[viaje]  WITH CHECK ADD  CONSTRAINT [FK_IdPedido] FOREIGN KEY([id_pedido])
REFERENCES [dbo].[pedido] ([id_pedido])
GO
ALTER TABLE [dbo].[viaje] CHECK CONSTRAINT [FK_IdPedido]
GO
USE [master]
GO
ALTER DATABASE [logistica] SET  READ_WRITE 
GO
