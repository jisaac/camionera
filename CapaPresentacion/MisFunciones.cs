﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace CapaPresentacion
{
    public class MisFunciones
    {
        public void controlReadOnly(Control a)//con esta funcion recursiva aseguro los campos en readonly para "ver unidad"
        {
            foreach (Control control in a.Controls)
            {

                if (control is TextBox)
                {
                    (control as TextBox).ReadOnly = true;

                }
                else if (control is ComboBox || control is CheckBox)
                {
                    control.Enabled = false;
                }
                else
                    foreach (Control hijo in control.Controls)
                    {
                        controlReadOnly(control);

                    }


            }
        }
        public void esconderFiltroBusquedaFechaUnidades(bool accion, Panel fecha, Panel texto)//si el parametro es true se esconde el panel de busqueda fecha y se ve el de busqueda texto y si es false viceversa
        {
            if (accion == true)
            {
                fecha.Visible = false;
                fecha.Enabled = false;
                texto.Visible = true;
                texto.Enabled = true;
                fecha.Location = new System.Drawing.Point(7, 43);
                texto.Location = new System.Drawing.Point(432, 63);

            }
            else
            {
                texto.Enabled = false;
                texto.Visible = false;
                fecha.Enabled = true;
                fecha.Visible = true;
                texto.Location = new System.Drawing.Point(7, 36);
                fecha.Location = new System.Drawing.Point(355, 68);
            }



        }
        public void esconderFiltroBusquedaFechaViaje(bool accion, Panel fecha, Panel texto)//si el parametro es true se esconde el panel de busqueda fecha y se ve el de busqueda texto y si es false viceversa
        {
            if (accion == true)
            {
                fecha.Visible = false;
                fecha.Enabled = false;
                texto.Visible = true;
                texto.Enabled = true;
                fecha.Location = new System.Drawing.Point(0, 0);
                texto.Location = new System.Drawing.Point(527, 36);

            }
            else
            {
                texto.Enabled = false;
                texto.Visible = false;
                fecha.Enabled = true;
                fecha.Visible = true;
                texto.Location = new System.Drawing.Point(0, 0);
                fecha.Location = new System.Drawing.Point(496, 39);
            }

        }
        public void esconderFiltroBusquedaFechaTallerCamion(bool accion, Panel fecha, TextBox texto)//si el parametro es true se esconde el panel de busqueda fecha y se ve el de busqueda texto y si es false viceversa
        {
            if (accion == true)
            {
                fecha.Visible = false;
                fecha.Enabled = false;
                texto.Visible = true;
                texto.Enabled = true;
                //fecha.Location = new System.Drawing.Point(629, 4);
                //texto.Location = new System.Drawing.Point(405, 18);

            }
            else
            {
                texto.Enabled = false;
                texto.Visible = false;
                fecha.Enabled = true;
                fecha.Visible = true;
                //texto.Location = new System.Drawing.Point(0, 0);
                //fecha.Location = new System.Drawing.Point(365, 15);
            }

        }
        public void esconderFiltroBusquedaFechaTallerAcoplado(bool accion, Panel fecha, TextBox texto)//si el parametro es true se esconde el panel de busqueda fecha y se ve el de busqueda texto y si es false viceversa
        {
            if (accion == true)
            {
                fecha.Visible = false;
                fecha.Enabled = false;
                texto.Visible = true;
                texto.Enabled = true;
                //fecha.Location = new System.Drawing.Point(981, 6);
                //texto.Location = new System.Drawing.Point(405, 18);

            }
            else
            {
                texto.Enabled = false;
                texto.Visible = false;
                fecha.Enabled = true;
                fecha.Visible = true;
                //texto.Location = new System.Drawing.Point(31, 1);
                //fecha.Location = new System.Drawing.Point(366, 14);
            }

        }
        /// <summary> Teclas permitidas en el TextBox
        ///(char)46 pulsan .
        ///(char)8 pulsan Borrar
        ///(char)13 pulsan enter
        ///(char)37 pulsan Izquierda
        ///(char)38 pulsan Arriba
        ///(char)39 pulsan Derecha
        ///(char)40 pulsan Abajo
        ///(char)48 - 57  pulsan Los números del 0 al 9
        /// </summary>
        public List<int> valores_permitidos = new List<int>() { 8, 13, 37, 38, 39, 40, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 44 };
        public void solo_numero(object sender, KeyPressEventArgs e)
        {
            TextBox textbox = (TextBox)sender;

            char signo_decimal = (char)44; //Si pulsan la (,) coma
            
            if (char.IsNumber(e.KeyChar) | valores_permitidos.Contains(e.KeyChar) |
                e.KeyChar == (char)Keys.Escape | e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false; // No hacemos nada y dejamos que el sistema controle la pulsación de tecla
                return;
            }
            else if (e.KeyChar == signo_decimal)
            {
                //Si no hay caracteres, o si ya hay un punto, no dejaremos poner el punto(.)
                if (textbox.Text.Length == 0 | textbox.Text.LastIndexOf(signo_decimal) >= 0)
                {
                    e.Handled = true; // Interceptamos la pulsación para que no permitirla.
                }
                else //Si hay caracteres continuamos las comprobaciones
                {
                    //Cambiamos la pulsación al separador decimal definido por el sistema 
                    e.KeyChar = Convert.ToChar(System.Globalization.NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator);
                    e.Handled = false; // No hacemos nada y dejamos que el sistema controle la pulsación de tecla
                }
                return;
            }
            else if (e.KeyChar == (char)13) // Si es un enter
            {
                e.Handled = true; //Interceptamos la pulsación para que no la permita.
                SendKeys.Send("{TAB}"); //Pulsamos la tecla Tabulador por código
            }
            else //Para el resto de las teclas
            {
                e.Handled = true; // Interceptamos la pulsación para que no tenga lugar
            }
        } //Controla que solo pueda ingresar numeros

        public void solo_letras(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        } //Controla que solo pueda ingresar letras

        public string valida_dominio(string dom)
        {
            Regex rgx = new Regex(@"^([A-Z]{3}[0-9]{3})|([A-Z]{2}[0-9]{3}[A-Z]{2})$");
            if (rgx.IsMatch(dom))
                return "CORRECTO";
            else
                return "INCORRECTO";
        }
    }
}
