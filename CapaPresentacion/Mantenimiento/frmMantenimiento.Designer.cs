﻿namespace CapaPresentacion.Mantenimiento
{
    partial class frmMantenimiento
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMantenimiento));
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAcopladoEstado = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.lblAcopladoDtpEntregado = new System.Windows.Forms.Label();
            this.lblAcopladoDtpSalidaAprox = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblAcopladoDtpEntrada = new System.Windows.Forms.Label();
            this.gbAcopladoTipoMantenimiento = new System.Windows.Forms.GroupBox();
            this.cbAcopladoNeumaticos = new System.Windows.Forms.CheckBox();
            this.cbAcopladoOtros = new System.Windows.Forms.CheckBox();
            this.cbAcopladoService = new System.Windows.Forms.CheckBox();
            this.btnAcopladoAsignar = new System.Windows.Forms.Button();
            this.btnAcopladoAlta = new System.Windows.Forms.Button();
            this.btnAcopladoEditar = new System.Windows.Forms.Button();
            this.lblAcopladoDiasAFavor = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblAcopladoSalidaAproxText = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbAcopladoObservaciones = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblCamionEstado = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lblCamionDtpEntregado = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblCamionDtpSalidaAprox = new System.Windows.Forms.Label();
            this.lblCamionDtpEntrada = new System.Windows.Forms.Label();
            this.gbCamionTipoMantenimiento = new System.Windows.Forms.GroupBox();
            this.cbCamionNeumaticos = new System.Windows.Forms.CheckBox();
            this.cbCamionOtros = new System.Windows.Forms.CheckBox();
            this.cbCamionService = new System.Windows.Forms.CheckBox();
            this.btnCamionAsignar = new System.Windows.Forms.Button();
            this.lblCamionDiasAFavor = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnCamionAlta = new System.Windows.Forms.Button();
            this.lblCamionSalidaAproxText = new System.Windows.Forms.Label();
            this.btnCamionEditar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tbCamionObservaciones = new System.Windows.Forms.TextBox();
            this.gpRemolque = new System.Windows.Forms.GroupBox();
            this.pnlAcopladoBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpAcopladoBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.dtpAcopladoBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAcopladoVer = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.lblAcopladoKm = new System.Windows.Forms.Label();
            this.lblAcopladoAño = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.lblKmAcopladoNeum = new System.Windows.Forms.Label();
            this.lblAcopladoKmNeumSuma = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label20 = new System.Windows.Forms.Label();
            this.lblAcopladoKmService = new System.Windows.Forms.Label();
            this.lblAcopladoKmServiceSuma = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.cbFiltroAcoplado2 = new System.Windows.Forms.ComboBox();
            this.cbFiltroAcoplado = new System.Windows.Forms.ComboBox();
            this.tbBusquedaAcoplado = new System.Windows.Forms.TextBox();
            this.dgvAcoplado = new System.Windows.Forms.DataGridView();
            this.gpCamion = new System.Windows.Forms.GroupBox();
            this.btnCamionVer = new System.Windows.Forms.Button();
            this.pnlCamionBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpCamionBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpbCamionBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.label7 = new System.Windows.Forms.Label();
            this.gbCamionKm = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.lblCamionKm = new System.Windows.Forms.Label();
            this.lblCamionAño = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lblKmCamionNeum = new System.Windows.Forms.Label();
            this.lblKmCamionNeumSuma = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.gbService = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.lblCamionKmService = new System.Windows.Forms.Label();
            this.lblKmCamionServiceSuma = new System.Windows.Forms.Label();
            this.label403 = new System.Windows.Forms.Label();
            this.cbFiltroCamion2 = new System.Windows.Forms.ComboBox();
            this.cbFiltroCamion = new System.Windows.Forms.ComboBox();
            this.tbBusquedaCamion = new System.Windows.Forms.TextBox();
            this.dgvCamion = new System.Windows.Forms.DataGridView();
            this.tbBuscaUnidad = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.btnBuscaUnidad = new System.Windows.Forms.Button();
            this.btnIngresarUnidadAtaller = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.gbAcopladoTipoMantenimiento.SuspendLayout();
            this.panel1.SuspendLayout();
            this.gbCamionTipoMantenimiento.SuspendLayout();
            this.gpRemolque.SuspendLayout();
            this.pnlAcopladoBusquedaFecha.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcoplado)).BeginInit();
            this.gpCamion.SuspendLayout();
            this.pnlCamionBusquedaFecha.SuspendLayout();
            this.gbCamionKm.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbService.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamion)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightYellow;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblAcopladoEstado);
            this.panel2.Controls.Add(this.label25);
            this.panel2.Controls.Add(this.lblAcopladoDtpEntregado);
            this.panel2.Controls.Add(this.lblAcopladoDtpSalidaAprox);
            this.panel2.Controls.Add(this.label22);
            this.panel2.Controls.Add(this.lblAcopladoDtpEntrada);
            this.panel2.Controls.Add(this.gbAcopladoTipoMantenimiento);
            this.panel2.Controls.Add(this.btnAcopladoAsignar);
            this.panel2.Controls.Add(this.btnAcopladoAlta);
            this.panel2.Controls.Add(this.btnAcopladoEditar);
            this.panel2.Controls.Add(this.lblAcopladoDiasAFavor);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.lblAcopladoSalidaAproxText);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.tbAcopladoObservaciones);
            this.panel2.Location = new System.Drawing.Point(830, 387);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(528, 330);
            this.panel2.TabIndex = 70;
            // 
            // lblAcopladoEstado
            // 
            this.lblAcopladoEstado.BackColor = System.Drawing.Color.White;
            this.lblAcopladoEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcopladoEstado.Location = new System.Drawing.Point(417, 293);
            this.lblAcopladoEstado.Name = "lblAcopladoEstado";
            this.lblAcopladoEstado.Size = new System.Drawing.Size(102, 23);
            this.lblAcopladoEstado.TabIndex = 88;
            this.lblAcopladoEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Location = new System.Drawing.Point(417, 270);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(94, 23);
            this.label25.TabIndex = 88;
            this.label25.Text = "Estado";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDtpEntregado
            // 
            this.lblAcopladoDtpEntregado.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDtpEntregado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDtpEntregado.Location = new System.Drawing.Point(238, 293);
            this.lblAcopladoDtpEntregado.Name = "lblAcopladoDtpEntregado";
            this.lblAcopladoDtpEntregado.Size = new System.Drawing.Size(107, 23);
            this.lblAcopladoDtpEntregado.TabIndex = 87;
            this.lblAcopladoDtpEntregado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDtpSalidaAprox
            // 
            this.lblAcopladoDtpSalidaAprox.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDtpSalidaAprox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDtpSalidaAprox.Location = new System.Drawing.Point(124, 293);
            this.lblAcopladoDtpSalidaAprox.Name = "lblAcopladoDtpSalidaAprox";
            this.lblAcopladoDtpSalidaAprox.Size = new System.Drawing.Size(107, 23);
            this.lblAcopladoDtpSalidaAprox.TabIndex = 85;
            this.lblAcopladoDtpSalidaAprox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(253, 270);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(72, 23);
            this.label22.TabIndex = 86;
            this.label22.Text = "Entregado";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDtpEntrada
            // 
            this.lblAcopladoDtpEntrada.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDtpEntrada.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDtpEntrada.Location = new System.Drawing.Point(7, 293);
            this.lblAcopladoDtpEntrada.Name = "lblAcopladoDtpEntrada";
            this.lblAcopladoDtpEntrada.Size = new System.Drawing.Size(107, 23);
            this.lblAcopladoDtpEntrada.TabIndex = 84;
            this.lblAcopladoDtpEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbAcopladoTipoMantenimiento
            // 
            this.gbAcopladoTipoMantenimiento.Controls.Add(this.cbAcopladoNeumaticos);
            this.gbAcopladoTipoMantenimiento.Controls.Add(this.cbAcopladoOtros);
            this.gbAcopladoTipoMantenimiento.Controls.Add(this.cbAcopladoService);
            this.gbAcopladoTipoMantenimiento.Location = new System.Drawing.Point(238, 12);
            this.gbAcopladoTipoMantenimiento.Name = "gbAcopladoTipoMantenimiento";
            this.gbAcopladoTipoMantenimiento.Size = new System.Drawing.Size(283, 38);
            this.gbAcopladoTipoMantenimiento.TabIndex = 82;
            this.gbAcopladoTipoMantenimiento.TabStop = false;
            this.gbAcopladoTipoMantenimiento.Text = "Tipo de mantenimiento";
            // 
            // cbAcopladoNeumaticos
            // 
            this.cbAcopladoNeumaticos.AutoSize = true;
            this.cbAcopladoNeumaticos.Location = new System.Drawing.Point(97, 14);
            this.cbAcopladoNeumaticos.Name = "cbAcopladoNeumaticos";
            this.cbAcopladoNeumaticos.Size = new System.Drawing.Size(118, 17);
            this.cbAcopladoNeumaticos.TabIndex = 79;
            this.cbAcopladoNeumaticos.Text = "Cambio neumaticos";
            this.cbAcopladoNeumaticos.UseVisualStyleBackColor = true;
            // 
            // cbAcopladoOtros
            // 
            this.cbAcopladoOtros.AutoSize = true;
            this.cbAcopladoOtros.Location = new System.Drawing.Point(221, 14);
            this.cbAcopladoOtros.Name = "cbAcopladoOtros";
            this.cbAcopladoOtros.Size = new System.Drawing.Size(51, 17);
            this.cbAcopladoOtros.TabIndex = 78;
            this.cbAcopladoOtros.Text = "Otros";
            this.cbAcopladoOtros.UseVisualStyleBackColor = true;
            // 
            // cbAcopladoService
            // 
            this.cbAcopladoService.AutoSize = true;
            this.cbAcopladoService.Location = new System.Drawing.Point(17, 14);
            this.cbAcopladoService.Name = "cbAcopladoService";
            this.cbAcopladoService.Size = new System.Drawing.Size(62, 17);
            this.cbAcopladoService.TabIndex = 77;
            this.cbAcopladoService.Text = "Service";
            this.cbAcopladoService.UseVisualStyleBackColor = true;
            // 
            // btnAcopladoAsignar
            // 
            this.btnAcopladoAsignar.Location = new System.Drawing.Point(159, 12);
            this.btnAcopladoAsignar.Name = "btnAcopladoAsignar";
            this.btnAcopladoAsignar.Size = new System.Drawing.Size(72, 38);
            this.btnAcopladoAsignar.TabIndex = 82;
            this.btnAcopladoAsignar.Text = "Asignarme unidad";
            this.btnAcopladoAsignar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAcopladoAsignar.UseVisualStyleBackColor = true;
            this.btnAcopladoAsignar.Click += new System.EventHandler(this.btnAcopladoAsignar_Click);
            // 
            // btnAcopladoAlta
            // 
            this.btnAcopladoAlta.Location = new System.Drawing.Point(81, 12);
            this.btnAcopladoAlta.Name = "btnAcopladoAlta";
            this.btnAcopladoAlta.Size = new System.Drawing.Size(72, 38);
            this.btnAcopladoAlta.TabIndex = 81;
            this.btnAcopladoAlta.Text = "Alta";
            this.btnAcopladoAlta.UseVisualStyleBackColor = true;
            this.btnAcopladoAlta.Click += new System.EventHandler(this.btnAcopladoAsignar_Click);
            // 
            // btnAcopladoEditar
            // 
            this.btnAcopladoEditar.Location = new System.Drawing.Point(3, 12);
            this.btnAcopladoEditar.Name = "btnAcopladoEditar";
            this.btnAcopladoEditar.Size = new System.Drawing.Size(72, 38);
            this.btnAcopladoEditar.TabIndex = 80;
            this.btnAcopladoEditar.Text = "Editar";
            this.btnAcopladoEditar.UseVisualStyleBackColor = true;
            this.btnAcopladoEditar.Click += new System.EventHandler(this.btnAcopladoAsignar_Click);
            // 
            // lblAcopladoDiasAFavor
            // 
            this.lblAcopladoDiasAFavor.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDiasAFavor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDiasAFavor.Location = new System.Drawing.Point(350, 293);
            this.lblAcopladoDiasAFavor.Name = "lblAcopladoDiasAFavor";
            this.lblAcopladoDiasAFavor.Size = new System.Drawing.Size(61, 23);
            this.lblAcopladoDiasAFavor.TabIndex = 77;
            this.lblAcopladoDiasAFavor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(347, 270);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 23);
            this.label6.TabIndex = 76;
            this.label6.Text = "Dias a favor";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoSalidaAproxText
            // 
            this.lblAcopladoSalidaAproxText.Location = new System.Drawing.Point(140, 270);
            this.lblAcopladoSalidaAproxText.Name = "lblAcopladoSalidaAproxText";
            this.lblAcopladoSalidaAproxText.Size = new System.Drawing.Size(72, 23);
            this.lblAcopladoSalidaAproxText.TabIndex = 74;
            this.lblAcopladoSalidaAproxText.Text = "Salida aprox";
            this.lblAcopladoSalidaAproxText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(29, 270);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 23);
            this.label8.TabIndex = 72;
            this.label8.Text = "Ingresó";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbAcopladoObservaciones
            // 
            this.tbAcopladoObservaciones.Location = new System.Drawing.Point(3, 54);
            this.tbAcopladoObservaciones.Multiline = true;
            this.tbAcopladoObservaciones.Name = "tbAcopladoObservaciones";
            this.tbAcopladoObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbAcopladoObservaciones.Size = new System.Drawing.Size(516, 210);
            this.tbAcopladoObservaciones.TabIndex = 65;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightYellow;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.lblCamionEstado);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.lblCamionDtpEntregado);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.lblCamionDtpSalidaAprox);
            this.panel1.Controls.Add(this.lblCamionDtpEntrada);
            this.panel1.Controls.Add(this.gbCamionTipoMantenimiento);
            this.panel1.Controls.Add(this.btnCamionAsignar);
            this.panel1.Controls.Add(this.lblCamionDiasAFavor);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.btnCamionAlta);
            this.panel1.Controls.Add(this.lblCamionSalidaAproxText);
            this.panel1.Controls.Add(this.btnCamionEditar);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.tbCamionObservaciones);
            this.panel1.Location = new System.Drawing.Point(830, 52);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(528, 330);
            this.panel1.TabIndex = 69;
            // 
            // lblCamionEstado
            // 
            this.lblCamionEstado.BackColor = System.Drawing.Color.White;
            this.lblCamionEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamionEstado.Location = new System.Drawing.Point(416, 291);
            this.lblCamionEstado.Name = "lblCamionEstado";
            this.lblCamionEstado.Size = new System.Drawing.Size(102, 23);
            this.lblCamionEstado.TabIndex = 87;
            this.lblCamionEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(416, 270);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(94, 23);
            this.label24.TabIndex = 86;
            this.label24.Text = "Estado";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDtpEntregado
            // 
            this.lblCamionDtpEntregado.BackColor = System.Drawing.Color.White;
            this.lblCamionDtpEntregado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDtpEntregado.Location = new System.Drawing.Point(236, 291);
            this.lblCamionDtpEntregado.Name = "lblCamionDtpEntregado";
            this.lblCamionDtpEntregado.Size = new System.Drawing.Size(107, 23);
            this.lblCamionDtpEntregado.TabIndex = 85;
            this.lblCamionDtpEntregado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(251, 270);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 23);
            this.label21.TabIndex = 84;
            this.label21.Text = "Entregado";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDtpSalidaAprox
            // 
            this.lblCamionDtpSalidaAprox.BackColor = System.Drawing.Color.White;
            this.lblCamionDtpSalidaAprox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDtpSalidaAprox.Location = new System.Drawing.Point(123, 291);
            this.lblCamionDtpSalidaAprox.Name = "lblCamionDtpSalidaAprox";
            this.lblCamionDtpSalidaAprox.Size = new System.Drawing.Size(107, 23);
            this.lblCamionDtpSalidaAprox.TabIndex = 83;
            this.lblCamionDtpSalidaAprox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDtpEntrada
            // 
            this.lblCamionDtpEntrada.BackColor = System.Drawing.Color.White;
            this.lblCamionDtpEntrada.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDtpEntrada.Location = new System.Drawing.Point(6, 291);
            this.lblCamionDtpEntrada.Name = "lblCamionDtpEntrada";
            this.lblCamionDtpEntrada.Size = new System.Drawing.Size(107, 23);
            this.lblCamionDtpEntrada.TabIndex = 82;
            this.lblCamionDtpEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbCamionTipoMantenimiento
            // 
            this.gbCamionTipoMantenimiento.Controls.Add(this.cbCamionNeumaticos);
            this.gbCamionTipoMantenimiento.Controls.Add(this.cbCamionOtros);
            this.gbCamionTipoMantenimiento.Controls.Add(this.cbCamionService);
            this.gbCamionTipoMantenimiento.Location = new System.Drawing.Point(238, 12);
            this.gbCamionTipoMantenimiento.Name = "gbCamionTipoMantenimiento";
            this.gbCamionTipoMantenimiento.Size = new System.Drawing.Size(282, 38);
            this.gbCamionTipoMantenimiento.TabIndex = 81;
            this.gbCamionTipoMantenimiento.TabStop = false;
            this.gbCamionTipoMantenimiento.Text = "Tipo de mantenimiento";
            // 
            // cbCamionNeumaticos
            // 
            this.cbCamionNeumaticos.AutoSize = true;
            this.cbCamionNeumaticos.Location = new System.Drawing.Point(97, 14);
            this.cbCamionNeumaticos.Name = "cbCamionNeumaticos";
            this.cbCamionNeumaticos.Size = new System.Drawing.Size(118, 17);
            this.cbCamionNeumaticos.TabIndex = 79;
            this.cbCamionNeumaticos.Text = "Cambio neumaticos";
            this.cbCamionNeumaticos.UseVisualStyleBackColor = true;
            // 
            // cbCamionOtros
            // 
            this.cbCamionOtros.AutoSize = true;
            this.cbCamionOtros.Location = new System.Drawing.Point(221, 14);
            this.cbCamionOtros.Name = "cbCamionOtros";
            this.cbCamionOtros.Size = new System.Drawing.Size(51, 17);
            this.cbCamionOtros.TabIndex = 78;
            this.cbCamionOtros.Text = "Otros";
            this.cbCamionOtros.UseVisualStyleBackColor = true;
            // 
            // cbCamionService
            // 
            this.cbCamionService.AutoSize = true;
            this.cbCamionService.Location = new System.Drawing.Point(17, 14);
            this.cbCamionService.Name = "cbCamionService";
            this.cbCamionService.Size = new System.Drawing.Size(62, 17);
            this.cbCamionService.TabIndex = 77;
            this.cbCamionService.Text = "Service";
            this.cbCamionService.UseVisualStyleBackColor = true;
            // 
            // btnCamionAsignar
            // 
            this.btnCamionAsignar.Location = new System.Drawing.Point(159, 12);
            this.btnCamionAsignar.Name = "btnCamionAsignar";
            this.btnCamionAsignar.Size = new System.Drawing.Size(72, 38);
            this.btnCamionAsignar.TabIndex = 76;
            this.btnCamionAsignar.Text = "Asignarme unidad";
            this.btnCamionAsignar.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnCamionAsignar.UseVisualStyleBackColor = true;
            this.btnCamionAsignar.Click += new System.EventHandler(this.btnCamionAsignar_Click);
            // 
            // lblCamionDiasAFavor
            // 
            this.lblCamionDiasAFavor.BackColor = System.Drawing.Color.White;
            this.lblCamionDiasAFavor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDiasAFavor.Location = new System.Drawing.Point(349, 290);
            this.lblCamionDiasAFavor.Name = "lblCamionDiasAFavor";
            this.lblCamionDiasAFavor.Size = new System.Drawing.Size(61, 23);
            this.lblCamionDiasAFavor.TabIndex = 71;
            this.lblCamionDiasAFavor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(346, 269);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 23);
            this.label3.TabIndex = 70;
            this.label3.Text = "Dias a favor";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCamionAlta
            // 
            this.btnCamionAlta.Location = new System.Drawing.Point(81, 12);
            this.btnCamionAlta.Name = "btnCamionAlta";
            this.btnCamionAlta.Size = new System.Drawing.Size(72, 38);
            this.btnCamionAlta.TabIndex = 75;
            this.btnCamionAlta.Text = "Alta";
            this.btnCamionAlta.UseVisualStyleBackColor = true;
            this.btnCamionAlta.Click += new System.EventHandler(this.btnCamionAsignar_Click);
            // 
            // lblCamionSalidaAproxText
            // 
            this.lblCamionSalidaAproxText.Location = new System.Drawing.Point(139, 270);
            this.lblCamionSalidaAproxText.Name = "lblCamionSalidaAproxText";
            this.lblCamionSalidaAproxText.Size = new System.Drawing.Size(72, 23);
            this.lblCamionSalidaAproxText.TabIndex = 68;
            this.lblCamionSalidaAproxText.Text = "Salida aprox";
            this.lblCamionSalidaAproxText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCamionEditar
            // 
            this.btnCamionEditar.Location = new System.Drawing.Point(3, 12);
            this.btnCamionEditar.Name = "btnCamionEditar";
            this.btnCamionEditar.Size = new System.Drawing.Size(72, 38);
            this.btnCamionEditar.TabIndex = 74;
            this.btnCamionEditar.Text = "Editar";
            this.btnCamionEditar.UseVisualStyleBackColor = true;
            this.btnCamionEditar.Click += new System.EventHandler(this.btnCamionAsignar_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(28, 270);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 23);
            this.label1.TabIndex = 66;
            this.label1.Text = "Ingresó";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tbCamionObservaciones
            // 
            this.tbCamionObservaciones.Location = new System.Drawing.Point(3, 54);
            this.tbCamionObservaciones.Multiline = true;
            this.tbCamionObservaciones.Name = "tbCamionObservaciones";
            this.tbCamionObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbCamionObservaciones.Size = new System.Drawing.Size(516, 210);
            this.tbCamionObservaciones.TabIndex = 65;
            // 
            // gpRemolque
            // 
            this.gpRemolque.Controls.Add(this.pnlAcopladoBusquedaFecha);
            this.gpRemolque.Controls.Add(this.btnAcopladoVer);
            this.gpRemolque.Controls.Add(this.groupBox2);
            this.gpRemolque.Controls.Add(this.groupBox3);
            this.gpRemolque.Controls.Add(this.groupBox4);
            this.gpRemolque.Controls.Add(this.cbFiltroAcoplado2);
            this.gpRemolque.Controls.Add(this.cbFiltroAcoplado);
            this.gpRemolque.Controls.Add(this.tbBusquedaAcoplado);
            this.gpRemolque.Controls.Add(this.dgvAcoplado);
            this.gpRemolque.Location = new System.Drawing.Point(12, 382);
            this.gpRemolque.Name = "gpRemolque";
            this.gpRemolque.Size = new System.Drawing.Size(812, 330);
            this.gpRemolque.TabIndex = 68;
            this.gpRemolque.TabStop = false;
            this.gpRemolque.Text = "REMOLQUE";
            // 
            // pnlAcopladoBusquedaFecha
            // 
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.dtpAcopladoBusquedaHasta);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.label12);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.dtpAcopladoBusquedaDesde);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.label14);
            this.pnlAcopladoBusquedaFecha.Location = new System.Drawing.Point(371, 14);
            this.pnlAcopladoBusquedaFecha.Name = "pnlAcopladoBusquedaFecha";
            this.pnlAcopladoBusquedaFecha.Size = new System.Drawing.Size(327, 28);
            this.pnlAcopladoBusquedaFecha.TabIndex = 85;
            // 
            // dtpAcopladoBusquedaHasta
            // 
            this.dtpAcopladoBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAcopladoBusquedaHasta.Location = new System.Drawing.Point(228, 6);
            this.dtpAcopladoBusquedaHasta.Name = "dtpAcopladoBusquedaHasta";
            this.dtpAcopladoBusquedaHasta.Size = new System.Drawing.Size(104, 20);
            this.dtpAcopladoBusquedaHasta.TabIndex = 4;
            this.dtpAcopladoBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpAcopladoBusquedaDesde_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(190, 7);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(33, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "hasta";
            // 
            // dtpAcopladoBusquedaDesde
            // 
            this.dtpAcopladoBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAcopladoBusquedaDesde.Location = new System.Drawing.Point(80, 6);
            this.dtpAcopladoBusquedaDesde.Name = "dtpAcopladoBusquedaDesde";
            this.dtpAcopladoBusquedaDesde.Size = new System.Drawing.Size(104, 20);
            this.dtpAcopladoBusquedaDesde.TabIndex = 2;
            this.dtpAcopladoBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpAcopladoBusquedaDesde_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Buscar  desde ";
            // 
            // btnAcopladoVer
            // 
            this.btnAcopladoVer.Location = new System.Drawing.Point(712, 16);
            this.btnAcopladoVer.Name = "btnAcopladoVer";
            this.btnAcopladoVer.Size = new System.Drawing.Size(96, 24);
            this.btnAcopladoVer.TabIndex = 81;
            this.btnAcopladoVer.Text = "Ver Detalles";
            this.btnAcopladoVer.UseVisualStyleBackColor = true;
            this.btnAcopladoVer.Click += new System.EventHandler(this.btnCamionVer_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.lblAcopladoKm);
            this.groupBox2.Controls.Add(this.lblAcopladoAño);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(552, 272);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(258, 50);
            this.groupBox2.TabIndex = 86;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Camion";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(6, 28);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(49, 13);
            this.label10.TabIndex = 53;
            this.label10.Text = "Km Total";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoKm
            // 
            this.lblAcopladoKm.BackColor = System.Drawing.Color.White;
            this.lblAcopladoKm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAcopladoKm.Location = new System.Drawing.Point(58, 18);
            this.lblAcopladoKm.Name = "lblAcopladoKm";
            this.lblAcopladoKm.Size = new System.Drawing.Size(88, 23);
            this.lblAcopladoKm.TabIndex = 54;
            this.lblAcopladoKm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoAño
            // 
            this.lblAcopladoAño.BackColor = System.Drawing.Color.White;
            this.lblAcopladoAño.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAcopladoAño.Location = new System.Drawing.Point(174, 18);
            this.lblAcopladoAño.Name = "lblAcopladoAño";
            this.lblAcopladoAño.Size = new System.Drawing.Size(77, 23);
            this.lblAcopladoAño.TabIndex = 60;
            this.lblAcopladoAño.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(149, 27);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(26, 13);
            this.label15.TabIndex = 59;
            this.label15.Text = "Año";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.lblKmAcopladoNeum);
            this.groupBox3.Controls.Add(this.lblAcopladoKmNeumSuma);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(285, 272);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(262, 50);
            this.groupBox3.TabIndex = 85;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Cambio de neumaticos (Km)";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(6, 28);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(32, 13);
            this.label16.TabIndex = 53;
            this.label16.Text = "Cada";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmAcopladoNeum
            // 
            this.lblKmAcopladoNeum.BackColor = System.Drawing.Color.White;
            this.lblKmAcopladoNeum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKmAcopladoNeum.Location = new System.Drawing.Point(38, 18);
            this.lblKmAcopladoNeum.Name = "lblKmAcopladoNeum";
            this.lblKmAcopladoNeum.Size = new System.Drawing.Size(88, 23);
            this.lblKmAcopladoNeum.TabIndex = 54;
            this.lblKmAcopladoNeum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoKmNeumSuma
            // 
            this.lblAcopladoKmNeumSuma.BackColor = System.Drawing.Color.White;
            this.lblAcopladoKmNeumSuma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAcopladoKmNeumSuma.Location = new System.Drawing.Point(167, 19);
            this.lblAcopladoKmNeumSuma.Name = "lblAcopladoKmNeumSuma";
            this.lblAcopladoKmNeumSuma.Size = new System.Drawing.Size(88, 23);
            this.lblAcopladoKmNeumSuma.TabIndex = 60;
            this.lblAcopladoKmNeumSuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(127, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(41, 13);
            this.label19.TabIndex = 59;
            this.label19.Text = "A favor";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label20);
            this.groupBox4.Controls.Add(this.lblAcopladoKmService);
            this.groupBox4.Controls.Add(this.lblAcopladoKmServiceSuma);
            this.groupBox4.Controls.Add(this.label23);
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(15, 272);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(264, 50);
            this.groupBox4.TabIndex = 84;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Service (Km)";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(6, 29);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 53;
            this.label20.Text = "Cada";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoKmService
            // 
            this.lblAcopladoKmService.BackColor = System.Drawing.Color.White;
            this.lblAcopladoKmService.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAcopladoKmService.Location = new System.Drawing.Point(38, 19);
            this.lblAcopladoKmService.Name = "lblAcopladoKmService";
            this.lblAcopladoKmService.Size = new System.Drawing.Size(88, 23);
            this.lblAcopladoKmService.TabIndex = 54;
            this.lblAcopladoKmService.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoKmServiceSuma
            // 
            this.lblAcopladoKmServiceSuma.BackColor = System.Drawing.Color.White;
            this.lblAcopladoKmServiceSuma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAcopladoKmServiceSuma.Location = new System.Drawing.Point(167, 19);
            this.lblAcopladoKmServiceSuma.Name = "lblAcopladoKmServiceSuma";
            this.lblAcopladoKmServiceSuma.Size = new System.Drawing.Size(88, 23);
            this.lblAcopladoKmServiceSuma.TabIndex = 60;
            this.lblAcopladoKmServiceSuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(127, 29);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 13);
            this.label23.TabIndex = 59;
            this.label23.Text = "A favor";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbFiltroAcoplado2
            // 
            this.cbFiltroAcoplado2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroAcoplado2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroAcoplado2.FormattingEnabled = true;
            this.cbFiltroAcoplado2.Location = new System.Drawing.Point(120, 18);
            this.cbFiltroAcoplado2.Name = "cbFiltroAcoplado2";
            this.cbFiltroAcoplado2.Size = new System.Drawing.Size(240, 23);
            this.cbFiltroAcoplado2.TabIndex = 83;
            this.cbFiltroAcoplado2.SelectedIndexChanged += new System.EventHandler(this.cbFiltroAcoplado_SelectedIndexChanged);
            // 
            // cbFiltroAcoplado
            // 
            this.cbFiltroAcoplado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroAcoplado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroAcoplado.FormattingEnabled = true;
            this.cbFiltroAcoplado.Items.AddRange(new object[] {
            "Todos",
            "Por revisar",
            "En proceso",
            "Entregados"});
            this.cbFiltroAcoplado.Location = new System.Drawing.Point(15, 18);
            this.cbFiltroAcoplado.Name = "cbFiltroAcoplado";
            this.cbFiltroAcoplado.Size = new System.Drawing.Size(98, 23);
            this.cbFiltroAcoplado.TabIndex = 31;
            this.cbFiltroAcoplado.SelectedIndexChanged += new System.EventHandler(this.cbFiltroAcoplado_SelectedIndexChanged);
            // 
            // tbBusquedaAcoplado
            // 
            this.tbBusquedaAcoplado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBusquedaAcoplado.Location = new System.Drawing.Point(405, 18);
            this.tbBusquedaAcoplado.Multiline = true;
            this.tbBusquedaAcoplado.Name = "tbBusquedaAcoplado";
            this.tbBusquedaAcoplado.Size = new System.Drawing.Size(248, 24);
            this.tbBusquedaAcoplado.TabIndex = 29;
            this.tbBusquedaAcoplado.TextChanged += new System.EventHandler(this.tbBusquedaAcoplado_TextChanged);
            this.tbBusquedaAcoplado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBuscaUnidad_KeyPress);
            // 
            // dgvAcoplado
            // 
            this.dgvAcoplado.AllowUserToAddRows = false;
            this.dgvAcoplado.AllowUserToDeleteRows = false;
            this.dgvAcoplado.AllowUserToOrderColumns = true;
            this.dgvAcoplado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAcoplado.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAcoplado.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvAcoplado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAcoplado.Location = new System.Drawing.Point(6, 47);
            this.dgvAcoplado.MultiSelect = false;
            this.dgvAcoplado.Name = "dgvAcoplado";
            this.dgvAcoplado.ReadOnly = true;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.MediumSeaGreen;
            this.dgvAcoplado.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAcoplado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcoplado.Size = new System.Drawing.Size(795, 220);
            this.dgvAcoplado.TabIndex = 24;
            this.dgvAcoplado.CurrentCellChanged += new System.EventHandler(this.dgvAcoplado_CurrentCellChanged);
            // 
            // gpCamion
            // 
            this.gpCamion.Controls.Add(this.btnCamionVer);
            this.gpCamion.Controls.Add(this.pnlCamionBusquedaFecha);
            this.gpCamion.Controls.Add(this.gbCamionKm);
            this.gpCamion.Controls.Add(this.groupBox1);
            this.gpCamion.Controls.Add(this.gbService);
            this.gpCamion.Controls.Add(this.cbFiltroCamion2);
            this.gpCamion.Controls.Add(this.cbFiltroCamion);
            this.gpCamion.Controls.Add(this.tbBusquedaCamion);
            this.gpCamion.Controls.Add(this.dgvCamion);
            this.gpCamion.Location = new System.Drawing.Point(12, 52);
            this.gpCamion.Name = "gpCamion";
            this.gpCamion.Size = new System.Drawing.Size(812, 330);
            this.gpCamion.TabIndex = 67;
            this.gpCamion.TabStop = false;
            this.gpCamion.Text = "CAMION";
            // 
            // btnCamionVer
            // 
            this.btnCamionVer.Location = new System.Drawing.Point(712, 16);
            this.btnCamionVer.Name = "btnCamionVer";
            this.btnCamionVer.Size = new System.Drawing.Size(96, 24);
            this.btnCamionVer.TabIndex = 80;
            this.btnCamionVer.Text = "Ver Detalles";
            this.btnCamionVer.UseVisualStyleBackColor = true;
            this.btnCamionVer.Click += new System.EventHandler(this.btnCamionVer_Click);
            // 
            // pnlCamionBusquedaFecha
            // 
            this.pnlCamionBusquedaFecha.Controls.Add(this.dtpCamionBusquedaHasta);
            this.pnlCamionBusquedaFecha.Controls.Add(this.label2);
            this.pnlCamionBusquedaFecha.Controls.Add(this.dtpbCamionBusquedaDesde);
            this.pnlCamionBusquedaFecha.Controls.Add(this.label7);
            this.pnlCamionBusquedaFecha.Location = new System.Drawing.Point(371, 14);
            this.pnlCamionBusquedaFecha.Name = "pnlCamionBusquedaFecha";
            this.pnlCamionBusquedaFecha.Size = new System.Drawing.Size(333, 28);
            this.pnlCamionBusquedaFecha.TabIndex = 84;
            // 
            // dtpCamionBusquedaHasta
            // 
            this.dtpCamionBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCamionBusquedaHasta.Location = new System.Drawing.Point(228, 6);
            this.dtpCamionBusquedaHasta.Name = "dtpCamionBusquedaHasta";
            this.dtpCamionBusquedaHasta.Size = new System.Drawing.Size(104, 20);
            this.dtpCamionBusquedaHasta.TabIndex = 4;
            this.dtpCamionBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpbCamionBusquedaDesde_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(190, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "hasta";
            // 
            // dtpbCamionBusquedaDesde
            // 
            this.dtpbCamionBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpbCamionBusquedaDesde.Location = new System.Drawing.Point(80, 6);
            this.dtpbCamionBusquedaDesde.Name = "dtpbCamionBusquedaDesde";
            this.dtpbCamionBusquedaDesde.Size = new System.Drawing.Size(104, 20);
            this.dtpbCamionBusquedaDesde.TabIndex = 2;
            this.dtpbCamionBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpbCamionBusquedaDesde_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(3, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Buscar  desde ";
            // 
            // gbCamionKm
            // 
            this.gbCamionKm.Controls.Add(this.label5);
            this.gbCamionKm.Controls.Add(this.lblCamionKm);
            this.gbCamionKm.Controls.Add(this.lblCamionAño);
            this.gbCamionKm.Controls.Add(this.label13);
            this.gbCamionKm.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCamionKm.Location = new System.Drawing.Point(552, 272);
            this.gbCamionKm.Name = "gbCamionKm";
            this.gbCamionKm.Size = new System.Drawing.Size(258, 50);
            this.gbCamionKm.TabIndex = 66;
            this.gbCamionKm.TabStop = false;
            this.gbCamionKm.Text = "Camion";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(6, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 53;
            this.label5.Text = "Km Total";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionKm
            // 
            this.lblCamionKm.BackColor = System.Drawing.Color.White;
            this.lblCamionKm.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCamionKm.Location = new System.Drawing.Point(58, 19);
            this.lblCamionKm.Name = "lblCamionKm";
            this.lblCamionKm.Size = new System.Drawing.Size(88, 23);
            this.lblCamionKm.TabIndex = 54;
            this.lblCamionKm.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionAño
            // 
            this.lblCamionAño.BackColor = System.Drawing.Color.White;
            this.lblCamionAño.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCamionAño.Location = new System.Drawing.Point(174, 19);
            this.lblCamionAño.Name = "lblCamionAño";
            this.lblCamionAño.Size = new System.Drawing.Size(77, 23);
            this.lblCamionAño.TabIndex = 60;
            this.lblCamionAño.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(149, 24);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(26, 13);
            this.label13.TabIndex = 59;
            this.label13.Text = "Año";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.lblKmCamionNeum);
            this.groupBox1.Controls.Add(this.lblKmCamionNeumSuma);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(285, 272);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(262, 50);
            this.groupBox1.TabIndex = 65;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Cambio de neumaticos (Km)";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(6, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(32, 13);
            this.label4.TabIndex = 53;
            this.label4.Text = "Cada";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmCamionNeum
            // 
            this.lblKmCamionNeum.BackColor = System.Drawing.Color.White;
            this.lblKmCamionNeum.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKmCamionNeum.Location = new System.Drawing.Point(38, 19);
            this.lblKmCamionNeum.Name = "lblKmCamionNeum";
            this.lblKmCamionNeum.Size = new System.Drawing.Size(88, 23);
            this.lblKmCamionNeum.TabIndex = 54;
            this.lblKmCamionNeum.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmCamionNeumSuma
            // 
            this.lblKmCamionNeumSuma.BackColor = System.Drawing.Color.White;
            this.lblKmCamionNeumSuma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKmCamionNeumSuma.Location = new System.Drawing.Point(167, 19);
            this.lblKmCamionNeumSuma.Name = "lblKmCamionNeumSuma";
            this.lblKmCamionNeumSuma.Size = new System.Drawing.Size(88, 23);
            this.lblKmCamionNeumSuma.TabIndex = 60;
            this.lblKmCamionNeumSuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(127, 29);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(41, 13);
            this.label11.TabIndex = 59;
            this.label11.Text = "A favor";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbService
            // 
            this.gbService.Controls.Add(this.label42);
            this.gbService.Controls.Add(this.lblCamionKmService);
            this.gbService.Controls.Add(this.lblKmCamionServiceSuma);
            this.gbService.Controls.Add(this.label403);
            this.gbService.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbService.Location = new System.Drawing.Point(15, 272);
            this.gbService.Name = "gbService";
            this.gbService.Size = new System.Drawing.Size(264, 50);
            this.gbService.TabIndex = 64;
            this.gbService.TabStop = false;
            this.gbService.Text = "Service (Km)";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(6, 29);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(32, 13);
            this.label42.TabIndex = 53;
            this.label42.Text = "Cada";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionKmService
            // 
            this.lblCamionKmService.BackColor = System.Drawing.Color.White;
            this.lblCamionKmService.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCamionKmService.Location = new System.Drawing.Point(38, 19);
            this.lblCamionKmService.Name = "lblCamionKmService";
            this.lblCamionKmService.Size = new System.Drawing.Size(88, 23);
            this.lblCamionKmService.TabIndex = 54;
            this.lblCamionKmService.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmCamionServiceSuma
            // 
            this.lblKmCamionServiceSuma.BackColor = System.Drawing.Color.White;
            this.lblKmCamionServiceSuma.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKmCamionServiceSuma.Location = new System.Drawing.Point(167, 19);
            this.lblKmCamionServiceSuma.Name = "lblKmCamionServiceSuma";
            this.lblKmCamionServiceSuma.Size = new System.Drawing.Size(88, 23);
            this.lblKmCamionServiceSuma.TabIndex = 60;
            this.lblKmCamionServiceSuma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label403
            // 
            this.label403.AutoSize = true;
            this.label403.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label403.Location = new System.Drawing.Point(127, 29);
            this.label403.Name = "label403";
            this.label403.Size = new System.Drawing.Size(41, 13);
            this.label403.TabIndex = 59;
            this.label403.Text = "A favor";
            this.label403.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbFiltroCamion2
            // 
            this.cbFiltroCamion2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroCamion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroCamion2.FormattingEnabled = true;
            this.cbFiltroCamion2.Items.AddRange(new object[] {
            "Id Ticket",
            "Observaciones ticket",
            "Vencidos",
            "Fecha de entrada",
            "Fecha de entrega",
            "Dominio camion",
            "Marca camion"});
            this.cbFiltroCamion2.Location = new System.Drawing.Point(120, 18);
            this.cbFiltroCamion2.Name = "cbFiltroCamion2";
            this.cbFiltroCamion2.Size = new System.Drawing.Size(249, 23);
            this.cbFiltroCamion2.TabIndex = 63;
            this.cbFiltroCamion2.SelectedIndexChanged += new System.EventHandler(this.cbFiltroCamion_SelectedIndexChanged);
            // 
            // cbFiltroCamion
            // 
            this.cbFiltroCamion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroCamion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroCamion.FormattingEnabled = true;
            this.cbFiltroCamion.Items.AddRange(new object[] {
            "Todos",
            "Por revisar",
            "En proceso",
            "Entregados"});
            this.cbFiltroCamion.Location = new System.Drawing.Point(15, 18);
            this.cbFiltroCamion.Name = "cbFiltroCamion";
            this.cbFiltroCamion.Size = new System.Drawing.Size(98, 23);
            this.cbFiltroCamion.TabIndex = 30;
            this.cbFiltroCamion.SelectedIndexChanged += new System.EventHandler(this.cbFiltroCamion_SelectedIndexChanged);
            // 
            // tbBusquedaCamion
            // 
            this.tbBusquedaCamion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBusquedaCamion.Location = new System.Drawing.Point(414, 17);
            this.tbBusquedaCamion.Multiline = true;
            this.tbBusquedaCamion.Name = "tbBusquedaCamion";
            this.tbBusquedaCamion.Size = new System.Drawing.Size(248, 23);
            this.tbBusquedaCamion.TabIndex = 29;
            this.tbBusquedaCamion.TextChanged += new System.EventHandler(this.tbBusquedaCamion_TextChanged);
            this.tbBusquedaCamion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBuscaUnidad_KeyPress);
            // 
            // dgvCamion
            // 
            this.dgvCamion.AllowUserToAddRows = false;
            this.dgvCamion.AllowUserToDeleteRows = false;
            this.dgvCamion.AllowUserToOrderColumns = true;
            this.dgvCamion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCamion.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCamion.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvCamion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCamion.Location = new System.Drawing.Point(15, 46);
            this.dgvCamion.MultiSelect = false;
            this.dgvCamion.Name = "dgvCamion";
            this.dgvCamion.ReadOnly = true;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumSeaGreen;
            this.dgvCamion.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCamion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCamion.Size = new System.Drawing.Size(795, 220);
            this.dgvCamion.TabIndex = 24;
            this.dgvCamion.CurrentCellChanged += new System.EventHandler(this.dgvCamion_CurrentCellChanged);
            // 
            // tbBuscaUnidad
            // 
            this.tbBuscaUnidad.Location = new System.Drawing.Point(424, 17);
            this.tbBuscaUnidad.MaxLength = 7;
            this.tbBuscaUnidad.Multiline = true;
            this.tbBuscaUnidad.Name = "tbBuscaUnidad";
            this.tbBuscaUnidad.ShortcutsEnabled = false;
            this.tbBuscaUnidad.Size = new System.Drawing.Size(122, 19);
            this.tbBuscaUnidad.TabIndex = 78;
            this.tbBuscaUnidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBuscaUnidad_KeyPress);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(349, 18);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(69, 16);
            this.label9.TabIndex = 79;
            this.label9.Text = "Dominio:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(15, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(237, 16);
            this.label17.TabIndex = 81;
            this.label17.Text = "Ingresar unidad a mantenimiento ";
            // 
            // btnBuscaUnidad
            // 
            this.btnBuscaUnidad.Image = global::CapaPresentacion.Properties.Resources.search;
            this.btnBuscaUnidad.Location = new System.Drawing.Point(266, 8);
            this.btnBuscaUnidad.Name = "btnBuscaUnidad";
            this.btnBuscaUnidad.Size = new System.Drawing.Size(77, 37);
            this.btnBuscaUnidad.TabIndex = 77;
            this.btnBuscaUnidad.UseVisualStyleBackColor = true;
            this.btnBuscaUnidad.Click += new System.EventHandler(this.btnBuscaUnidad_Click);
            // 
            // btnIngresarUnidadAtaller
            // 
            this.btnIngresarUnidadAtaller.AccessibleDescription = "GUARDAR";
            this.btnIngresarUnidadAtaller.Enabled = false;
            this.btnIngresarUnidadAtaller.Image = ((System.Drawing.Image)(resources.GetObject("btnIngresarUnidadAtaller.Image")));
            this.btnIngresarUnidadAtaller.Location = new System.Drawing.Point(564, 8);
            this.btnIngresarUnidadAtaller.Name = "btnIngresarUnidadAtaller";
            this.btnIngresarUnidadAtaller.Size = new System.Drawing.Size(77, 37);
            this.btnIngresarUnidadAtaller.TabIndex = 80;
            this.btnIngresarUnidadAtaller.UseVisualStyleBackColor = true;
            this.btnIngresarUnidadAtaller.Visible = false;
            this.btnIngresarUnidadAtaller.Click += new System.EventHandler(this.btnBuscaUnidad_Click);
            // 
            // frmMantenimiento
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightBlue;
            this.ClientSize = new System.Drawing.Size(1364, 721);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.btnBuscaUnidad);
            this.Controls.Add(this.btnIngresarUnidadAtaller);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.tbBuscaUnidad);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gpRemolque);
            this.Controls.Add(this.gpCamion);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMantenimiento";
            this.Text = "ADMINISTRACIÓN MANTENIMIENTO";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMantenimiento_FormClosing);
            this.Load += new System.EventHandler(this.frmMantenimiento_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbAcopladoTipoMantenimiento.ResumeLayout(false);
            this.gbAcopladoTipoMantenimiento.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.gbCamionTipoMantenimiento.ResumeLayout(false);
            this.gbCamionTipoMantenimiento.PerformLayout();
            this.gpRemolque.ResumeLayout(false);
            this.gpRemolque.PerformLayout();
            this.pnlAcopladoBusquedaFecha.ResumeLayout(false);
            this.pnlAcopladoBusquedaFecha.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcoplado)).EndInit();
            this.gpCamion.ResumeLayout(false);
            this.gpCamion.PerformLayout();
            this.pnlCamionBusquedaFecha.ResumeLayout(false);
            this.pnlCamionBusquedaFecha.PerformLayout();
            this.gbCamionKm.ResumeLayout(false);
            this.gbCamionKm.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.gbService.ResumeLayout(false);
            this.gbService.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamion)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblAcopladoDiasAFavor;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblAcopladoSalidaAproxText;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbAcopladoObservaciones;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblCamionDiasAFavor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblCamionSalidaAproxText;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbCamionObservaciones;
        private System.Windows.Forms.GroupBox gpRemolque;
        private System.Windows.Forms.ComboBox cbFiltroAcoplado;
        private System.Windows.Forms.TextBox tbBusquedaAcoplado;
        private System.Windows.Forms.DataGridView dgvAcoplado;
        private System.Windows.Forms.GroupBox gpCamion;
        private System.Windows.Forms.ComboBox cbFiltroCamion;
        private System.Windows.Forms.Label label403;
        private System.Windows.Forms.Label lblKmCamionServiceSuma;
        private System.Windows.Forms.TextBox tbBusquedaCamion;
        private System.Windows.Forms.DataGridView dgvCamion;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label lblCamionKmService;
        private System.Windows.Forms.Button btnAcopladoAsignar;
        private System.Windows.Forms.Button btnAcopladoAlta;
        private System.Windows.Forms.Button btnAcopladoEditar;
        private System.Windows.Forms.Button btnCamionAsignar;
        private System.Windows.Forms.Button btnCamionAlta;
        private System.Windows.Forms.Button btnCamionEditar;
        private System.Windows.Forms.ComboBox cbFiltroAcoplado2;
        private System.Windows.Forms.ComboBox cbFiltroCamion2;
        private System.Windows.Forms.Button btnBuscaUnidad;
        private System.Windows.Forms.TextBox tbBuscaUnidad;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.GroupBox gbAcopladoTipoMantenimiento;
        private System.Windows.Forms.CheckBox cbAcopladoNeumaticos;
        private System.Windows.Forms.CheckBox cbAcopladoOtros;
        private System.Windows.Forms.CheckBox cbAcopladoService;
        private System.Windows.Forms.GroupBox gbCamionTipoMantenimiento;
        private System.Windows.Forms.CheckBox cbCamionNeumaticos;
        private System.Windows.Forms.CheckBox cbCamionOtros;
        private System.Windows.Forms.CheckBox cbCamionService;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblKmCamionNeum;
        private System.Windows.Forms.Label lblKmCamionNeumSuma;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox gbService;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblAcopladoKm;
        private System.Windows.Forms.Label lblAcopladoAño;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblKmAcopladoNeum;
        private System.Windows.Forms.Label lblAcopladoKmNeumSuma;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblAcopladoKmService;
        private System.Windows.Forms.Label lblAcopladoKmServiceSuma;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.GroupBox gbCamionKm;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblCamionKm;
        private System.Windows.Forms.Label lblCamionAño;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnAcopladoVer;
        private System.Windows.Forms.Button btnCamionVer;
        private System.Windows.Forms.Label lblAcopladoDtpSalidaAprox;
        private System.Windows.Forms.Label lblAcopladoDtpEntrada;
        private System.Windows.Forms.Label lblCamionDtpSalidaAprox;
        private System.Windows.Forms.Label lblCamionDtpEntrada;
        private System.Windows.Forms.Panel pnlCamionBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpCamionBusquedaHasta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpbCamionBusquedaDesde;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel pnlAcopladoBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpAcopladoBusquedaHasta;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dtpAcopladoBusquedaDesde;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnIngresarUnidadAtaller;
        private System.Windows.Forms.Label lblAcopladoDtpEntregado;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblCamionDtpEntregado;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lblAcopladoEstado;
        private System.Windows.Forms.Label lblCamionEstado;
    }
}