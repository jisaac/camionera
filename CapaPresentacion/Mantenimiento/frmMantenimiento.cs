﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.Logistica;
using CapaPresentacion.Operaciones;
using CapaPresentacion.FormsMsj;
//iba a poner la funcion vencidos pero es mucho quilobo..que el user filtre por en proceso y de ahi que juegue con los dtp por fecha entrega taller
//VOY A INGRESAR UNA UNIDAD A TALLER DESDE MANTENIMIENTO, INGRESANDO EN EL C CAMPO OBSERVACIONES EL NOMBRE DEL USUARIO QUE LO INGRESO,
//Y LUEGO LISTO BUSCANDO ESE TICKET,EL USUARIO DEBERA ASIGNARSELO PARA CAMBIAR LA OBSERVACION JIJIJI

namespace CapaPresentacion.Mantenimiento
{
    public partial class frmMantenimiento : Form
    {

        public frmMantenimiento(string usuario)
        {
            InitializeComponent();
            n_taller = new N_Taller(usuario);
            this.usuario = usuario;
            this.funciones = new MisFunciones();
            n_camion = new N_Camion(usuario);
            n_acoplado = new N_Acoplado(usuario);
            n_dominio = new N_Dominio(usuario);
        }
        EstadoTaller estadoTallerCamion;
        EstadoTaller estadoTallerAcoplado;
        E_Taller e_taller;
        N_Taller n_taller;
        string usuario;
        string resp = string.Empty;
        DataTable dtTallerCamion;
        DataTable dtTallerAcoplado;
        MisFunciones funciones;
        N_Camion n_camion;
        N_Acoplado n_acoplado;
        N_Dominio n_dominio;
        List<string> listResp = new List<string>();
        frmMensajes mensaje;
        private void frmMantenimiento_Load(object sender, EventArgs e)
        {

            funciones.esconderFiltroBusquedaFechaTallerCamion(true, pnlCamionBusquedaFecha, tbBusquedaCamion);//escondemos dtp Camion busqueda
            funciones.esconderFiltroBusquedaFechaTallerAcoplado(true, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);//escondemos dtp Camion busqueda
            //Cargamos cbfiltrocamion2
            
            cbFiltroCamion.SelectedIndex = 0;
            cbFiltroAcoplado.SelectedIndex = 0;
            btnBuscaUnidad.Tag = "Buscar";
            alinear_columnas_mantenimiento();
        }
        void RecibeFecha(E_Taller e_taller)
        {
            this.e_taller = e_taller;
        }
        void Carga_cb_tipo_de_mantenimiento(string taller_tipo, CheckBox service, CheckBox neumaticos, CheckBox otros)
        {
            service.Checked = false;
            neumaticos.Checked = false;
            otros.Checked = false;
            string[] Separador = new string[] { "+" };

            string[] tipos = taller_tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
            foreach (string tipo in tipos)
            {
                if (tipo.ToUpper().Equals("SERVICE"))
                {
                    service.Checked = true;
                }
                if (tipo.ToUpper().Equals("NEUMATICOS"))
                {
                    neumaticos.Checked = true;
                }
                if (tipo.ToUpper().Equals("OTROS"))
                {
                    otros.Checked = true;
                }
            }
        }

        private void alinear_columnas_mantenimiento()
        {
            dgvCamion.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCamion.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCamion.Columns[20].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            dgvAcoplado.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAcoplado.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAcoplado.Columns[20].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        private void btnCamionAsignar_Click(object sender, EventArgs e)
        {
            try
            {


                List<string> resp = new List<string>();
                e_taller = n_taller.retornaTaller(dgvCamion.CurrentRow.Cells[0].Value.ToString());
                e_taller.Observaciones = tbCamionObservaciones.Text;
                if (sender == btnCamionAsignar)//asignar responsable de ticket o trabajo
                {

                    if (e_taller != null)
                    {
                        frmDtpSalida frmDtp = new frmDtpSalida(e_taller);
                        frmDtp.enviaFecha += RecibeFecha;
                        frmDtp.ShowDialog();
                        //falta agregarle el legajo del usuario y luego modificamos no mas 
                        e_taller.usuario = this.usuario;
                    }
                    if (e_taller.Salida_aprox != null)
                    {
                        listResp = n_taller.modificar(e_taller, EstadoTaller.EnProceso, cbCamionService.Checked, cbCamionNeumaticos.Checked, cbCamionOtros.Checked);
                        new frmMensajes(listResp[0], listResp[1]).ShowDialog();//esto vamos a tener que modificarlo para que tambien refresque todo y que siga seleccionado este ticketa si el mant 
                                                                               //puede editar el mismo.Que se refrezque unicamente este ticket usando campo de busqueda ; )
                    }

                    cbFiltroCamion.SelectedIndex = 0;
                    cbFiltroCamion_SelectedIndexChanged(cbFiltroCamion, null);
                }
                if (sender == btnCamionEditar)//editar ticket
                {
                    if (e_taller != null)
                    {

                        if (e_taller.Estado.Equals("EN_PROCESO"))
                        {
                            resp = n_taller.modificar(e_taller, EstadoTaller.EnProceso, cbCamionService.Checked, cbCamionNeumaticos.Checked, cbCamionOtros.Checked);
                            if (resp != null)
                            {
                                new frmMensajes("Exito", "Ticket en estado 'EN PROCESO', editado con exito !").ShowDialog();
                            }
                            else
                            {
                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                            }
                        }
                        if (e_taller.Estado.Equals("CONCLUIDO"))
                        {
                            resp = n_taller.modificar(e_taller, EstadoTaller.Concluido, cbCamionService.Checked, cbCamionNeumaticos.Checked, cbCamionOtros.Checked);
                            if (resp != null)
                            {
                                new frmMensajes("Exito", "Ticket en estado 'CONCLUIDO', editado con exito !").ShowDialog();
                            }
                            else
                            {
                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                            }
                        }

                    }
                    cbFiltroCamion.SelectedIndex = 0;
                    cbFiltroCamion_SelectedIndexChanged(cbFiltroCamion, null);
                }
                if (sender == btnCamionAlta)//concluir ticket/ liberar unidades
                {
                    this.mensaje = new frmMensajes("Pregunta", "Dar de alta \n Desea agregar observaciones ? ");
                    mensaje.SioNo += (bool respuesta) =>
                    {
                        if (!respuesta)
                        {
                            frmMensajes men = new frmMensajes("Pregunta", "Seguro desea dar de alta la unidad ? \n El alta de la unidad podria generar cambios en algunos datos de la misma  ");
                            men.SioNo += (bool r) =>
                            {
                                if (r)
                                {

                                    resp = n_taller.modificar(e_taller, EstadoTaller.Concluido, cbCamionService.Checked, cbCamionNeumaticos.Checked, cbCamionOtros.Checked);
                                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                                }
                            };
                            men.ShowDialog();





                        };

                    };
                    mensaje.ShowDialog();



                    cbFiltroCamion.SelectedIndex = 0;
                    cbFiltroCamion_SelectedIndexChanged(cbFiltroCamion, null);
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", ex.Message).ShowDialog();
            }
        }



        #region bindings y todo eso CAMION

        private void columnas_invisibles_camion()
        {
            try
            {
                dgvCamion.Columns[4].Visible = false; //Año
                dgvCamion.Columns[5].Visible = false;//Tara
                dgvCamion.Columns[6].Visible = false; //Km Unidad
                dgvCamion.Columns[7].Visible = false; //Km service
                dgvCamion.Columns[8].Visible = false; //Km cambio de neumaticos
                dgvCamion.Columns[9].Visible = false; //Km service suma
                dgvCamion.Columns[10].Visible = false;//Km neumaticos suma
                dgvCamion.Columns[11].Visible = false;//Estado
                dgvCamion.Columns[13].Visible = false;//Fecha de entrada
                dgvCamion.Columns[14].Visible = false;//Entrega aprox
                dgvCamion.Columns[15].Visible = false;//Entregado
                dgvCamion.Columns[16].Visible = false;//Observaciones
                dgvCamion.Columns[17].Visible = false;//Tipo de taller
                dgvCamion.Columns[18].Visible = false;//Estado taller
                dgvCamion.Columns[19].Visible = false;//Num legajo
                                                      //dgvCamion.Columns[20].Visible = false;//Usuario
                                                      //Camion

                //                0 Ticket
                //                1 Dominio 
                //                2 Marca  
                //                3 Modelo
                //                4 Año   --false -- > label
                //                5 Tara
                //                6 Km Unidad   --false -- > label
                //                7 Km service  --false -- >label
                //                8 Km cambio de neumaticos-- false -- > label
                //                9 Km service suma -- false -- > label
                //                10 Km neumaticos suma-- false -- > label
                //                11 Estado camion -- false -- > que hacemos con esto ?
                //                12 Tipo camion 
                //                13 Fecha de entrada --false --> Dtp
                //                14 Entrega aprox --false -- > Dtp
                //                15 Entregado  --false -- > Dtp
                //                16 Observaciones --false -- > tb
                //                17 Tipo taller--false -- > Checkbox
                //                18 Estado taller--false -- quiza lo manejemos por colores
                //                19 Num legajo-- false--label
                //                20 Usuario -- false label
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }

        }
        private void carga_datos_label_camion()
        {
            try
            {
                this.limpiaBindingCamion();
                this.controlaBindingCountCamion();

                if (dtTallerCamion != null && dtTallerCamion.Rows.Count >= 0)
                {
                    this.columnas_invisibles_camion();


                    lblCamionAño.DataBindings.Add(new Binding("Text", dtTallerCamion, "Año"));
                    lblCamionKm.DataBindings.Add(new Binding("Text", dtTallerCamion, "Km Unidad"));
                    lblCamionKmService.DataBindings.Add(new Binding("Text", dtTallerCamion, "Km service"));
                    lblKmCamionNeum.DataBindings.Add(new Binding("Text", dtTallerCamion, "Km cambio de neumaticos"));
                    lblKmCamionServiceSuma.DataBindings.Add(new Binding("Text", dtTallerCamion, "Km service suma"));
                    lblKmCamionNeumSuma.DataBindings.Add(new Binding("Text", dtTallerCamion, "Km neumaticos suma"));
                    tbCamionObservaciones.DataBindings.Add(new Binding("Text", dtTallerCamion, "Observaciones"));
                    gbCamionTipoMantenimiento.Visible = true;



                }
                else
                {
                    gbCamionTipoMantenimiento.Visible = false;
                    dgvCamion.BackgroundColor = Color.LightGray;
                }
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }



        }
        private void controlaBindingCountCamion()
        {
            try
            {
                if (lblCamionAño.DataBindings.Count == 0)
                    lblCamionAño.Text = "-----";
                if (lblCamionKm.DataBindings.Count == 0)
                    lblCamionKm.Text = "-----";
                if (lblCamionKmService.DataBindings.Count == 0)
                    lblCamionKmService.Text = "-----";
                if (lblKmCamionNeum.DataBindings.Count == 0)
                    lblKmCamionNeum.Text = "-----";
                if (lblKmCamionServiceSuma.DataBindings.Count == 0)
                    lblKmCamionServiceSuma.Text = "-----";
                if (lblKmCamionNeumSuma.DataBindings.Count == 0)
                    lblKmCamionNeumSuma.Text = "-----";
                if (tbCamionObservaciones.DataBindings.Count == 0)
                    tbCamionObservaciones.Text = "-----";
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }

        }
        private void limpiaBindingCamion()
        {
            try
            {
                lblCamionAño.DataBindings.Clear();
                lblCamionKm.DataBindings.Clear();
                lblCamionKmService.DataBindings.Clear();
                lblKmCamionNeum.DataBindings.Clear();
                lblKmCamionServiceSuma.DataBindings.Clear();
                lblKmCamionNeumSuma.DataBindings.Clear();
                tbCamionObservaciones.DataBindings.Clear();
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }

        }
        private void limpiaTextboxBindingCamion()
        {
            lblCamionAño.Text = "-----";
            lblCamionKm.Text = "-----";
            lblCamionKmService.Text = "-----";
            lblKmCamionNeum.Text = "-----";
            lblKmCamionServiceSuma.Text = "-----";
            lblKmCamionNeumSuma.Text = "-----";
            tbCamionObservaciones.Text = "-----";
        }
        #endregion
        #region bindings y todo eso ACOPLADO
        private void columnas_invisibles_Acoplado()
        {
            try
            {
                //dgvAcoplado.Columns[0].Visible = false; //Ticket
                //dgvAcoplado.Columns[1].Visible = false; //Dominio
                //dgvAcoplado.Columns[2].Visible = false; //Marca
                //dgvAcoplado.Columns[3].Visible = false; //Modelo
                dgvAcoplado.Columns[4].Visible = false; //Año
                dgvAcoplado.Columns[5].Visible = false;//Tara
                dgvAcoplado.Columns[6].Visible = false; //Km unidad
                dgvAcoplado.Columns[7].Visible = false; //Km service
                dgvAcoplado.Columns[8].Visible = false; //km cambio neumaticos
                dgvAcoplado.Columns[9].Visible = false; //Km service suma
                dgvAcoplado.Columns[10].Visible = false; //Km cambio neumaticos suma
                dgvAcoplado.Columns[11].Visible = false;//Estado acoplado
                                                        //dgvAcoplado.Columns[12].Visible = false;//Tipo de acoplado
                dgvAcoplado.Columns[13].Visible = false;//Fecha de entrada
                dgvAcoplado.Columns[14].Visible = false;//Entrega Aprox
                dgvAcoplado.Columns[15].Visible = false;//Entregado
                dgvAcoplado.Columns[16].Visible = false;//Observaciones
                dgvAcoplado.Columns[17].Visible = false;//Tipo taller
                dgvAcoplado.Columns[18].Visible = false;//Estado 
                dgvAcoplado.Columns[19].Visible = false;//Num legajo
                                                        //dgvAcoplado.Columns[20].Visible = false;//Usuario

                //            Acoplado
                //0 Ticket 1 Dominio 2 Marca 3 Modelo 4 Año
                //5 Tipo de acoplado 6 Tara 
                //7 Km unidad 8 Km service 9 Km cambio neumaticos
                //10 Km service suma 11 Km cambio neumaticos suma 
                //12 Estado 13 Fecha de entrada 14 Entrega aprox
                //15 Entregado(fecha de allta seria) 16 Observaciones 17 Tipo taller
                //18 Estado taller 19 Num legajo 20 Usuario
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }



        }
        private void carga_datos_label_Acoplado()
        {
            try
            {
                this.limpiaBindingAcoplado();
                this.controlaBindingCountAcoplado();

                if (dtTallerAcoplado != null && dtTallerAcoplado.Rows.Count >= 0)
                {
                    this.columnas_invisibles_Acoplado();
                    lblAcopladoAño.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Año"));
                    lblAcopladoKm.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Km unidad"));
                    lblAcopladoKmService.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Km service"));
                    lblKmAcopladoNeum.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Km cambio de neumaticos"));
                    lblAcopladoKmServiceSuma.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Km service suma"));
                    lblAcopladoKmNeumSuma.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Km neumaticos suma"));
                    tbAcopladoObservaciones.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Observaciones"));
                    gbAcopladoTipoMantenimiento.Visible = true;

                }
                else
                {
                    dgvAcoplado.BackgroundColor = Color.LightGray;
                    gbCamionTipoMantenimiento.Visible = false;
                }
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }
        }
        private void controlaBindingCountAcoplado()
        {
            try
            {
                if (lblAcopladoAño.DataBindings.Count == 0)
                    lblAcopladoAño.Text = "-----";
                if (lblAcopladoKm.DataBindings.Count == 0)
                    lblAcopladoKm.Text = "-----";
                if (lblAcopladoKmService.DataBindings.Count == 0)
                    lblAcopladoKmService.Text = "-----";
                if (lblKmAcopladoNeum.DataBindings.Count == 0)
                    lblKmAcopladoNeum.Text = "-----";
                if (lblAcopladoKmServiceSuma.DataBindings.Count == 0)
                    lblAcopladoKmServiceSuma.Text = "-----";
                if (lblAcopladoKmNeumSuma.DataBindings.Count == 0)
                    lblAcopladoKmNeumSuma.Text = "-----";
                if (tbAcopladoObservaciones.DataBindings.Count == 0)
                    tbAcopladoObservaciones.Text = "-----";
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }

        }
        private void limpiaBindingAcoplado()
        {
            try
            {
                lblAcopladoAño.DataBindings.Clear();
                lblAcopladoKm.DataBindings.Clear();
                lblAcopladoKmService.DataBindings.Clear();
                lblKmAcopladoNeum.DataBindings.Clear();
                lblAcopladoKmServiceSuma.DataBindings.Clear();
                lblAcopladoKmNeumSuma.DataBindings.Clear();
                tbAcopladoObservaciones.DataBindings.Clear();
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }

        }
        private void limpiaTextboxBindingAcoplado()
        {
            lblAcopladoAño.Text = "-----";
            lblAcopladoKm.Text = "-----";
            lblAcopladoKmService.Text = "-----";
            lblKmAcopladoNeum.Text = "-----";
            lblAcopladoKmServiceSuma.Text = "-----";
            lblAcopladoKmNeumSuma.Text = "-----";
            tbAcopladoObservaciones.Text = "-----";
        }
        #endregion

        private void cbFiltroCamion_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (sender == cbFiltroCamion)
                {
                    if (cbFiltroCamion.SelectedIndex == 1)//POR REVISAR
                    {
                        cbFiltroCamion2.Items.Clear();
                        cbFiltroCamion2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada" });
                        cbFiltroCamion2.SelectedIndex = 0;
                    }
                    else
                    {
                        cbFiltroCamion2.Items.Clear();
                        cbFiltroCamion2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada", "Fecha de entrega" });
                        cbFiltroCamion2.SelectedIndex = 0;
                    }
                    switch (cbFiltroCamion.SelectedIndex)
                    {
                        case 0:
                            estadoTallerCamion = EstadoTaller.Todos;
                            dtTallerCamion = n_taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvCamion.DataSource = dtTallerCamion;
                            carga_datos_label_camion();
                            //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                            //dtpCamionBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaCamion.Clear();
                            break;
                        case 1:
                            estadoTallerCamion = EstadoTaller.PorRevisar;
                            dtTallerCamion = n_taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvCamion.DataSource = dtTallerCamion;
                            carga_datos_label_camion();
                            //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                            //dtpCamionBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaCamion.Clear();
                            break;
                        case 2:
                            estadoTallerCamion = EstadoTaller.EnProceso;
                            dtTallerCamion = n_taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvCamion.DataSource = dtTallerCamion;
                            carga_datos_label_camion();
                            //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                            //dtpCamionBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaCamion.Clear();
                            break;
                        case 3:
                            estadoTallerCamion = EstadoTaller.Concluido;
                            dtTallerCamion = n_taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvCamion.DataSource = dtTallerCamion;
                            carga_datos_label_camion();
                            //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                            //dtpCamionBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaCamion.Clear();
                            break;
                    }
                }

                if (sender == cbFiltroCamion2) //si el sender  es cb filtro camion 2
                {
                    switch (cbFiltroCamion2.SelectedIndex)//controlo si muestro los dtp o el textbox del segundo filtro de camion
                    {
                        case 0:
                            funciones.esconderFiltroBusquedaFechaTallerCamion(true, pnlCamionBusquedaFecha, tbBusquedaCamion);
                            break;
                        case 1:
                            funciones.esconderFiltroBusquedaFechaTallerCamion(false, pnlCamionBusquedaFecha, tbBusquedaCamion);
                            break;
                        case 2:
                            funciones.esconderFiltroBusquedaFechaTallerCamion(false, pnlCamionBusquedaFecha, tbBusquedaCamion);
                            break;

                    }
                }
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }


        }

        private void cbFiltroAcoplado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (sender == cbFiltroAcoplado)
                {
                    if (cbFiltroAcoplado.SelectedIndex == 1)//POR REVISAR
                    {
                        cbFiltroAcoplado2.Items.Clear();
                        cbFiltroAcoplado2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada" });
                        cbFiltroAcoplado2.SelectedIndex = 0;
                    }
                    else
                    {
                        cbFiltroAcoplado2.Items.Clear();
                        cbFiltroAcoplado2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada", "Fecha de entrega" });
                        cbFiltroAcoplado2.SelectedIndex = 0;
                    }
                    switch (cbFiltroAcoplado.SelectedIndex)
                    {
                        case 0:
                            estadoTallerAcoplado = EstadoTaller.Todos;
                            dtTallerAcoplado = n_taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvAcoplado.DataSource = dtTallerAcoplado;
                            carga_datos_label_Acoplado();
                            //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                            //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaAcoplado.Clear();
                            break;
                        case 1:
                            estadoTallerAcoplado = EstadoTaller.PorRevisar;
                            dtTallerAcoplado = n_taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvAcoplado.DataSource = dtTallerAcoplado;
                            carga_datos_label_Acoplado();
                            //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                            //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaAcoplado.Clear();
                            break;
                        case 2:
                            estadoTallerAcoplado = EstadoTaller.EnProceso;
                            dtTallerAcoplado = n_taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvAcoplado.DataSource = dtTallerAcoplado;
                            carga_datos_label_Acoplado();
                            //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                            //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaAcoplado.Clear();
                            break;
                        case 3:
                            estadoTallerAcoplado = EstadoTaller.Concluido;
                            dtTallerAcoplado = n_taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                            dgvAcoplado.DataSource = dtTallerAcoplado;
                            carga_datos_label_Acoplado();
                            //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                            //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                            tbBusquedaAcoplado.Clear();
                            break;
                    }
                }

                if (sender == cbFiltroAcoplado2) //si el sender  es cb filtro Acoplado 2
                {
                    switch (cbFiltroAcoplado2.SelectedIndex)//controlo si muestro los dtp o el textbox del segundo filtro de Acoplado
                    {
                        case 0:
                            funciones.esconderFiltroBusquedaFechaTallerAcoplado(true, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);
                            break;
                        case 1:
                            funciones.esconderFiltroBusquedaFechaTallerAcoplado(false, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);
                            break;
                        case 2:
                            funciones.esconderFiltroBusquedaFechaTallerAcoplado(false, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);
                            break;

                    }
                }
            }
            catch
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }


        }

        private void dtpbCamionBusquedaDesde_ValueChanged(object sender, EventArgs e)//Evento de DATE TIME PICKER CAMION
        {
            try
            {
                if (dtpbCamionBusquedaDesde.Value <= dtpCamionBusquedaHasta.Value.AddSeconds(60))
                {
                    switch (cbFiltroCamion2.SelectedItem.ToString())
                    {
                        case "Fecha de entrada":
                            dtTallerCamion = n_taller.Listar_por_Fecha_Entrada_Camion(dtpbCamionBusquedaDesde.Value.ToShortDateString(), dtpCamionBusquedaHasta.Value.ToShortDateString(), estadoTallerCamion);
                            dgvCamion.DataSource = dtTallerCamion;
                            carga_datos_label_camion();
                            break;
                        case "Fecha de entrega":
                            dtTallerCamion = n_taller.Listar_por_Fecha_Salida_Camion(dtpbCamionBusquedaDesde.Value.ToShortDateString(), dtpCamionBusquedaHasta.Value.ToShortDateString(), estadoTallerCamion);
                            dgvCamion.DataSource = dtTallerCamion;
                            carga_datos_label_camion();
                            break;
                    }
                }
                else
                {
                    dtpbCamionBusquedaDesde.Refresh();
                    dtpCamionBusquedaHasta.Refresh();
                    new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
                    dtpCamionBusquedaHasta.Focus();
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", ex.Message).ShowDialog();
            }

        }

        private void dtpAcopladoBusquedaDesde_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (dtpAcopladoBusquedaDesde.Value <= dtpAcopladoBusquedaHasta.Value.AddSeconds(60))
                {
                    switch (cbFiltroAcoplado2.SelectedItem.ToString())
                    {
                        case "Fecha de entrada":
                            dtTallerAcoplado = n_taller.Listar_por_Fecha_Entrada_Acoplado(dtpAcopladoBusquedaDesde.Value.ToShortDateString(), dtpAcopladoBusquedaHasta.Value.ToShortDateString(), estadoTallerAcoplado);
                            dgvAcoplado.DataSource = dtTallerAcoplado;
                            carga_datos_label_Acoplado();
                            break;
                        case "Fecha de entrega":
                            dtTallerAcoplado = n_taller.Listar_por_Fecha_Salida_Acoplado(dtpAcopladoBusquedaDesde.Value.ToShortDateString(), dtpAcopladoBusquedaHasta.Value.ToShortDateString(), estadoTallerAcoplado);
                            dgvAcoplado.DataSource = dtTallerAcoplado;
                            carga_datos_label_Acoplado();
                            break;
                    }
                }
                else
                {
                    dtpAcopladoBusquedaDesde.Refresh();
                    dtpAcopladoBusquedaHasta.Refresh();
                    new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
                    dtpAcopladoBusquedaHasta.Focus();
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", ex.Message).ShowDialog();
            }

        }//Evento de DATE TIME PICKER ACOPLADO

        private void tbBusquedaCamion_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbBusquedaCamion.Text.Length >= 1)
                {
                    dtTallerCamion = n_taller.Listar_por_Texto_Camion(estadoTallerCamion, tbBusquedaCamion.Text.Trim());
                    dgvCamion.DataSource = dtTallerCamion;
                    carga_datos_label_camion();
                }
                else
                {
                    dtTallerCamion = n_taller.Listar_por_Estado_Camion(estadoTallerCamion);
                    dgvCamion.DataSource = dtTallerCamion;
                    carga_datos_label_camion();
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", ex.Message).ShowDialog();
            }


        }

        private void tbBusquedaAcoplado_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (tbBusquedaAcoplado.Text.Length >= 1)
                {
                    dtTallerAcoplado = n_taller.Listar_por_Texto_Acoplado(estadoTallerAcoplado, tbBusquedaAcoplado.Text.Trim());
                    dgvAcoplado.DataSource = dtTallerAcoplado;
                    carga_datos_label_Acoplado();
                }
                else
                {
                    dtTallerAcoplado = n_taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);
                    dgvAcoplado.DataSource = dtTallerAcoplado;
                    carga_datos_label_Acoplado();
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", ex.Message).ShowDialog();
            }


        }

        private void btnBuscaUnidad_Click(object sender, EventArgs e)
        {
            try
            {
                string ResultadoBusqueda = string.Empty;
                if (sender == btnBuscaUnidad)
                {
                    if (!btnBuscaUnidad.Tag.Equals("Cancelar"))
                    {
                        if (!(tbBuscaUnidad.Text == string.Empty))
                        {
                            tbBuscaUnidad.Text = tbBuscaUnidad.Text.ToUpper();
                            if (funciones.valida_dominio(tbBuscaUnidad.Text).Equals("CORRECTO"))
                            {
                                ResultadoBusqueda = n_taller.Verifica_si_unidad_puede_ingresar_a_talle(tbBuscaUnidad.Text.ToUpper().Trim());
                                if (ResultadoBusqueda.Equals("CAMION"))
                                {//hago visible el btn ingresar unidad
                                    btnIngresarUnidadAtaller.Visible = true;
                                    btnIngresarUnidadAtaller.Enabled = true;
                                    tbBuscaUnidad.Enabled = false;// desactivo el ingreso de texto
                                    new frmMensajes("Informacion", "Se encontro camion con dominio " + tbBuscaUnidad.Text + " !").ShowDialog();
                                    btnBuscaUnidad.Tag = "Cancelar";
                                    btnBuscaUnidad.Image = CapaPresentacion.Properties.Resources.cancel;
                                }
                                else if (ResultadoBusqueda.Equals("ACOPLADO"))
                                {
                                    //hago visible el btn ingresar unidad
                                    btnIngresarUnidadAtaller.Visible = true;
                                    btnIngresarUnidadAtaller.Enabled = true;
                                    tbBuscaUnidad.Enabled = false;// desactivo el ingreso de texto
                                    btnBuscaUnidad.Tag = "Cancelar";
                                    btnBuscaUnidad.Image = CapaPresentacion.Properties.Resources.cancel;

                                    new frmMensajes("Informacion", "Se encontro remolque con dominio " + tbBuscaUnidad.Text + " !").ShowDialog();
                                }
                                else
                                {
                                    new frmMensajes("Informacion", ResultadoBusqueda).ShowDialog();
                                    tbBuscaUnidad.Focus();
                                }
                            }
                            else
                            {
                                new frmMensajes("Error", "Formato de dominio incorrecto, recuerde que el formato es AA999AA ó AAA999").ShowDialog();
                                tbBuscaUnidad.Clear();
                                tbBuscaUnidad.Focus();
                            }
                        }
                        else
                        {
                            new frmMensajes("Error", "Debe completar el campo de busqueda !").ShowDialog();
                        }



                    }
                    else
                    {
                        btnIngresarUnidadAtaller.Visible = false;
                        btnIngresarUnidadAtaller.Enabled = false;
                        tbBuscaUnidad.Clear();
                        tbBuscaUnidad.Enabled = true;//activo el ingreso de texto
                        btnBuscaUnidad.Tag = "Buscar";
                        btnBuscaUnidad.Image = CapaPresentacion.Properties.Resources.search;
                    }

                }
                if (sender == btnIngresarUnidadAtaller)//probar si se toma los cambios de indice en los selected index
                {
                    e_taller = new E_Taller();
                    e_taller.Observaciones = "ticket generado por " + usuario + "/Fecha:" + DateTime.Now.ToShortDateString() + "";

                    listResp = n_taller.insertarSinSaberId_Dominio(e_taller, tbBuscaUnidad.Text.ToUpper().Trim());//INSERTAMOS UNIDAD A  TALLER
                    if (listResp[0].Equals("Exito"))
                    {
                        new frmMensajes(listResp[0], listResp[1]).ShowDialog();

                        cbFiltroCamion.SelectedIndex = 0;
                        cbFiltroCamion_SelectedIndexChanged(cbFiltroCamion, null);
                        tbBusquedaCamion.Text = e_taller.Id_taller.ToString();
                        cbFiltroAcoplado.SelectedIndex = 0;
                        cbFiltroAcoplado_SelectedIndexChanged(cbFiltroAcoplado, null);
                        tbBusquedaAcoplado.Text = e_taller.Id_taller.ToString();

                        btnIngresarUnidadAtaller.Visible = false;
                        btnIngresarUnidadAtaller.Enabled = false;
                        tbBuscaUnidad.Clear();
                        tbBuscaUnidad.Enabled = true;//activo el ingreso de texto
                        btnBuscaUnidad.Image = CapaPresentacion.Properties.Resources.search;
                        btnBuscaUnidad.Tag = "Buscar";
                    }
                    else
                    {
                        new frmMensajes(listResp[0], listResp[1]).ShowDialog();
                    }





                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Informacion","Ups! "+Environment.NewLine+" No encontramos la unidad que buscabas").ShowDialog();
            }


            }

        private void btnCamionVer_Click(object sender, EventArgs e)
        {
            try
            {
                object unidad;
                if (sender == btnCamionVer)
                {
                    unidad = null;
                    unidad = n_camion.retornaCamion(n_dominio.Retorna_Id_Dominio(dgvCamion.CurrentRow.Cells[1].Value.ToString()));
                    new frmNuevoCamion(usuario, unidad, true).ShowDialog();
                }
                if (sender == btnAcopladoVer)
                {
                    unidad = null;
                    unidad = n_acoplado.retornaAcoplado(n_dominio.Retorna_Id_Dominio(dgvAcoplado.CurrentRow.Cells[1].Value.ToString()));
                    new frmNuevoAcoplado(usuario, unidad, true).ShowDialog();
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Tiene que haber un ticket seleccionado para ver detalles de la unidad" ).ShowDialog();
            }
        

        }

        private void btnAcopladoAsignar_Click(object sender, EventArgs e)
        {
            try{
                List<string> resp = new List<string>();
                e_taller = n_taller.retornaTaller(dgvAcoplado.CurrentRow.Cells[0].Value.ToString());
                e_taller.Observaciones = tbAcopladoObservaciones.Text;
                if (sender == btnAcopladoAsignar)//asignar responsable de ticket o trabajo
                {

                    if (e_taller != null)
                    {
                        frmDtpSalida frmDtp = new frmDtpSalida(e_taller);
                        frmDtp.enviaFecha += RecibeFecha;
                        frmDtp.ShowDialog();
                        //falta agregarle el legajo del usuario y luego modificamos no mas 
                        e_taller.usuario = this.usuario;
                    }
                    if (e_taller.Salida_aprox != null)
                    {
                        listResp = n_taller.modificar(e_taller, EstadoTaller.EnProceso, cbAcopladoService.Checked, cbAcopladoNeumaticos.Checked, cbAcopladoOtros.Checked);
                        new frmMensajes(listResp[0], listResp[1]).ShowDialog();//esto vamos a tener que modificarlo para que tambien refresque todo y que siga seleccionado este ticketa si el mant 
                                                                               //puede editar el mismo.Que se refrezque unicamente este ticket usando campo de busqueda ; )
                    }

                    cbFiltroAcoplado.SelectedIndex = 0;
                    cbFiltroAcoplado_SelectedIndexChanged(cbFiltroAcoplado, null);
                }
                if (sender == btnAcopladoEditar)//editar ticket
                {
                    if (e_taller != null)
                    {

                        if (e_taller.Estado.Equals("EN_PROCESO"))
                        {
                            resp = n_taller.modificar(e_taller, EstadoTaller.EnProceso, cbAcopladoService.Checked, cbAcopladoNeumaticos.Checked, cbAcopladoOtros.Checked);
                            if (resp != null)
                            {
                                new frmMensajes("Exito", "Ticket en estado 'EN PROCESO', editado con exito !").ShowDialog();
                            }
                            else
                            {
                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                            }
                        }
                        if (e_taller.Estado.Equals("CONCLUIDO"))
                        {
                            resp = n_taller.modificar(e_taller, EstadoTaller.Concluido, cbAcopladoService.Checked, cbAcopladoNeumaticos.Checked, cbAcopladoOtros.Checked);
                            if (resp != null)
                            {
                                new frmMensajes("Exito", "Ticket en estado 'CONCLUIDO', editado con exito !").ShowDialog();
                            }
                            else
                            {
                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                            }

                        }

                    }
                    cbFiltroAcoplado.SelectedIndex = 0;// listo de vuelta
                    cbFiltroAcoplado_SelectedIndexChanged(cbFiltroAcoplado, null);
                }

                if (sender == btnAcopladoAlta)//concluir ticket/ liberar unidades
                {

                    this.mensaje = new frmMensajes("Pregunta", "Dar de alta \n Desea agregar observaciones ? ");
                    mensaje.SioNo += (bool respuesta) =>
                    {
                        if (!respuesta)
                        {
                            frmMensajes men = new frmMensajes("Pregunta", "Seguro desea dar de alta la unidad ? \n El alta de la unidad podria generar cambios en algunos datos de la misma  ");
                            men.SioNo += (bool r) =>
                            {
                                if (r)
                                {

                                    resp = n_taller.modificar(e_taller, EstadoTaller.Concluido, cbAcopladoService.Checked, cbAcopladoNeumaticos.Checked, cbAcopladoOtros.Checked);
                                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                                }
                            };
                            men.ShowDialog();





                        };

                    };
                    mensaje.ShowDialog();
                    cbFiltroAcoplado.SelectedIndex = 0;// listo de vuelta
                    cbFiltroAcoplado_SelectedIndexChanged(cbFiltroAcoplado, null);
                }

            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Ups,  Algo salio mal!" + Environment.NewLine + "No pudimos editar este ticket").ShowDialog();
            }
            
        }

        private void dgvAcoplado_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvAcoplado.CurrentRow != null)
                {


                    //if (dgvAcoplado.CurrentRow.Cells[20].Value.Equals("----"))
                    //{
                    switch (dgvAcoplado.CurrentRow.Cells[18].Value.ToString())
                    {
                        case "POR_REVISAR":
                            lblAcopladoEstado.Text = "POR REVISAR";
                            lblAcopladoEstado.ForeColor = Color.Red;
                            break;
                        case "EN_PROCESO":
                            lblAcopladoEstado.Text = "EN PROCESO";
                            lblAcopladoEstado.ForeColor = Color.Orange;
                            break;
                        case "CONCLUIDO":
                            lblAcopladoEstado.Text = "CONCLUIDO";
                            lblAcopladoEstado.ForeColor = Color.ForestGreen;
                            break;
                    }
                    if (!dgvAcoplado.CurrentRow.Cells[13].Value.ToString().Equals("1/1/1900 00:00:00"))
                    {
                        lblAcopladoDtpEntrada.Text = Convert.ToDateTime(dgvAcoplado.CurrentRow.Cells[13].Value.ToString()).ToShortDateString();
                    }
                    else
                    {
                        lblAcopladoDtpEntrada.Text = "----";
                    }
                    if (!dgvAcoplado.CurrentRow.Cells[15].Value.ToString().Equals("1/1/1900 00:00:00"))
                    {
                        lblAcopladoDtpEntregado.Text = Convert.ToDateTime(dgvAcoplado.CurrentRow.Cells[15].Value.ToString()).ToShortDateString();
                    }
                    else
                    {
                        lblAcopladoDtpEntregado.Text = "----";
                    }
                    //cargamos DTPS
                    if (!dgvAcoplado.CurrentRow.Cells[14].Value.ToString().Equals("1/1/1900 00:00:00"))// esto es porque el ticket  TIENE respondable ASIGNADO
                    { // si el campo salida aproximada tiene valor que no sea null
                        lblAcopladoDtpSalidaAprox.Visible = true;
                        lblAcopladoSalidaAproxText.Visible = true;
                        lblAcopladoDtpSalidaAprox.Text = Convert.ToDateTime(dgvAcoplado.CurrentRow.Cells[14].Value.ToString()).ToShortDateString();
                        TimeSpan intervalo = Convert.ToDateTime(dgvAcoplado.CurrentRow.Cells[14].Value.ToString()).Date - Convert.ToDateTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                        lblAcopladoDiasAFavor.Text = intervalo.Days.ToString();//cargamos el label dias a favor
                    }
                    else
                    {
                        lblAcopladoDtpSalidaAprox.Visible = false;
                        lblAcopladoSalidaAproxText.Visible = false;
                        lblAcopladoDiasAFavor.Text = "----";
                    }//ahora cargamos los chbox
                    Carga_cb_tipo_de_mantenimiento(dgvAcoplado.CurrentRow.Cells[17].Value.ToString().ToUpper(), cbAcopladoService, cbAcopladoNeumaticos, cbAcopladoOtros);
                    if (dgvAcoplado.CurrentRow.Cells[20].Value.ToString().Equals("Sin asignar"))//habilitamos botonera de edicion de ticket
                    {
                        btnAcopladoEditar.Enabled = false;
                        btnAcopladoAsignar.Enabled = true;
                        btnAcopladoAlta.Enabled = false;
                        cbAcopladoNeumaticos.Enabled = false;
                        cbAcopladoService.Enabled = false;
                        cbAcopladoOtros.Enabled = false;
                        tbAcopladoObservaciones.ReadOnly = true;
                        gbAcopladoTipoMantenimiento.Enabled = false;//INHABILITAMOS LOS CHECKBOX
                    }
                    else
                    {
                        btnAcopladoEditar.Enabled = true;
                        btnAcopladoAsignar.Enabled = false;
                        btnAcopladoAlta.Enabled = true;
                        cbAcopladoNeumaticos.Enabled = true;
                        cbAcopladoService.Enabled = true;
                        cbAcopladoOtros.Enabled = true;
                        tbAcopladoObservaciones.ReadOnly = false;
                        gbAcopladoTipoMantenimiento.Enabled = true;//INHABILITAMOS LOS CHECKBOX
                    }//si el estado de taller ya es concluido entonces no debemos permitir que se vea boton asignar ni dar de alta
                    if (dgvAcoplado.CurrentRow.Cells[18].Value.ToString().ToUpper().Equals(EstadoTaller.Concluido.ToString().ToUpper()))
                    {
                        btnAcopladoAlta.Visible = false;
                        btnAcopladoAsignar.Visible = false;
                        gbAcopladoTipoMantenimiento.Enabled = false;//INHABILITAMOS LOS CHECKBOX
                        TimeSpan intervalo = Convert.ToDateTime(dgvAcoplado.CurrentRow.Cells[14].Value.ToString()).Date - Convert.ToDateTime(dgvAcoplado.CurrentRow.Cells[15].Value.ToString()).Date;
                        lblAcopladoDiasAFavor.Text = intervalo.Days.ToString();//cargamos el label dias a favor
                    }
                    else
                    {
                        btnAcopladoAlta.Visible = true;
                        btnAcopladoAsignar.Visible = true;
                        gbAcopladoTipoMantenimiento.Enabled = true;//INHABILITAMOS LOS CHECKBOX
                    }

                    //}
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }



        }

        private void dgvCamion_CurrentCellChanged(object sender, EventArgs e)
        {
            try
            {
                if (dgvCamion.CurrentRow != null)
                {
                    switch (dgvCamion.CurrentRow.Cells[18].Value.ToString())
                    {
                        case "POR_REVISAR":
                            lblCamionEstado.Text = "POR REVISAR";
                            lblCamionEstado.ForeColor = Color.Red;
                            break;
                        case "EN_PROCESO":
                            lblCamionEstado.Text = "EN PROCESO";
                            lblCamionEstado.ForeColor = Color.Orange;
                            break;
                        case "CONCLUIDO":
                            lblCamionEstado.Text = "CONCLUIDO";
                            lblCamionEstado.ForeColor = Color.ForestGreen;
                            break;
                    }
                    if (!dgvCamion.CurrentRow.Cells[13].Value.ToString().Equals("1/1/1900 00:00:00"))
                    {
                        lblCamionDtpEntrada.Text = Convert.ToDateTime(dgvCamion.CurrentRow.Cells[13].Value.ToString()).ToShortDateString();
                    }
                    else
                    {
                        lblCamionDtpEntrada.Text = "----";
                    }
                    if (!dgvCamion.CurrentRow.Cells[15].Value.ToString().Equals("1/1/1900 00:00:00"))
                    {
                        lblCamionDtpEntregado.Text = Convert.ToDateTime(dgvCamion.CurrentRow.Cells[15].Value.ToString()).ToShortDateString();
                    }
                    else
                    {
                        lblCamionDtpEntregado.Text = "----";
                    }
                    if (dgvCamion.CurrentRow.Cells[13].Value == null)
                    {
                        lblCamionDtpEntrada.Text = Convert.ToDateTime(dgvCamion.CurrentRow.Cells[13].Value.ToString()).ToShortDateString();
                    }//cargamos DTPSSSS
                    if (!dgvCamion.CurrentRow.Cells[14].Value.ToString().Equals("1/1/1900 00:00:00"))
                    { // si el campo salida aproximada tiene valor que no sea null
                        lblCamionDtpSalidaAprox.Visible = true;
                        lblCamionSalidaAproxText.Visible = true;
                        lblCamionDtpSalidaAprox.Text = Convert.ToDateTime(dgvCamion.CurrentRow.Cells[14].Value.ToString()).ToShortDateString();
                        TimeSpan intervalo = Convert.ToDateTime(dgvCamion.CurrentRow.Cells[14].Value.ToString()).Date - Convert.ToDateTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                        lblCamionDiasAFavor.Text = intervalo.Days.ToString();//cargamos el label dias a favor
                    }
                    else
                    {
                        lblCamionDtpSalidaAprox.Visible = false;
                        lblCamionSalidaAproxText.Visible = false;
                        lblCamionDiasAFavor.Text = "----";
                    }//ahora cargamos los chbox
                    Carga_cb_tipo_de_mantenimiento(dgvCamion.CurrentRow.Cells[17].Value.ToString().ToUpper(), cbCamionService, cbCamionNeumaticos, cbCamionOtros);
                    if (dgvCamion.CurrentRow.Cells[20].Value.ToString().Equals("Sin asignar"))//habilitamos botonera de edicion de ticket
                    {
                        btnCamionEditar.Enabled = false;
                        btnCamionAsignar.Enabled = true;
                        btnCamionAlta.Enabled = false;
                        cbCamionNeumaticos.Enabled = false;
                        cbCamionService.Enabled = false;
                        cbCamionOtros.Enabled = false;
                        tbCamionObservaciones.ReadOnly = true;
                        gbCamionTipoMantenimiento.Enabled = false;//INHABILITAMOS LOS CHECKBOX
                    }
                    else
                    {
                        btnCamionEditar.Enabled = true;
                        btnCamionAsignar.Enabled = false;
                        btnCamionAlta.Enabled = true;
                        cbCamionNeumaticos.Enabled = true;
                        cbCamionService.Enabled = true;
                        cbCamionOtros.Enabled = true;
                        tbCamionObservaciones.ReadOnly = false;
                        gbCamionTipoMantenimiento.Enabled = true;//HABILITAMOS LOS CHECKBOX
                    }//si el estado de taller ya es concluido entonces no debemos permitir que se vea boton asignar ni dar de alta
                    if (dgvCamion.CurrentRow.Cells[18].Value.ToString().ToUpper().Equals(EstadoTaller.Concluido.ToString().ToUpper()))
                    {
                        btnCamionAlta.Visible = false;
                        btnCamionAsignar.Visible = false;
                        gbCamionTipoMantenimiento.Enabled = false;//INHABILITAMOS LOS CHECKBOX
                        TimeSpan intervalo = Convert.ToDateTime(dgvCamion.CurrentRow.Cells[14].Value.ToString()).Date - Convert.ToDateTime(dgvCamion.CurrentRow.Cells[15].Value.ToString()).Date;
                        lblCamionDiasAFavor.Text = intervalo.Days.ToString();//cargamos el label dias a favor

                    }
                    else
                    {
                        btnCamionAlta.Visible = true;
                        btnCamionAsignar.Visible = true;
                        gbCamionTipoMantenimiento.Enabled = true;//INHABILITAMOS LOS CHECKBOX
                    }
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }


        }

        private void tbBuscaUnidad_KeyPress(object sender, KeyPressEventArgs e)
        {

            e.KeyChar = char.ToUpper(e.KeyChar);

        }

        private void frmMantenimiento_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }


    }
}





//                0 Ticket
//                1 Dominio 
//                2 Marca  
//                3 Modelo
//                4 Año   
//                5 Tara
//                6 Km Unidad   --false -- > label
//                7 Km service  --false -- >label
//                8 Km cambio de neumaticos-- false -- > label
//                9 Km service suma -- false -- > label
//                10 Km neumaticos suma-- false -- > label
//                11 Estado -- false -- > que hacemos con esto ?
//                12 Tipo Camion 
//                13 Fecha de entrada --false --> Dtp
//                14 Entrega aprox --false -- > Dtp
//                15 Observaciones --false -- > tb
//                16 Tipo --false -- > Checkbox
//                17 Estado taller--false -- quiza lo manejemos por colores
//                18 Num legajo-- false--label
//                19 Usuario -- false label
//        Acoplado

//                0 Ticket
//                1 Dominio
//                2 Marca
//                3 Modelo
//                4 Año
//                5 Tipo de acoplado
//                6 Tara
//                7 Km Unidad  --false -- > label
//                8 Km service  --false -- >label
//                9 Km cambio de neumaticos-- false -- > label
//                10 Km service suma-- false -- > label
//                11 Km neumaticos_suma-- false -- > label
//                12 Altura
//                13 Longitud
//                14 Estado -- false -- > que hacemos con esto ?
//                15 Fecha de entrada
//                16 Entrega aprox
//                17 Entregado
//                18 Observaciones
//                19 Tipo
//                20 Estado taller
//                21 Num legajo
//                22 Usuario
//voy a poner unos checkbox y segun lo que seleccione armo el parametro para cambio neumatico o service o lo que sea
//El user que se asigne la unida deja el ticket como asignado pero la unidad aun no tiene fecha de entrega y el estado del ticket sigue siendo por revisar
//Una vez que este ponga fecha de entrega este pasa a en mantenimiento
// vpy a utilizar el forms de mostrarr  unidades de admin para ver detalle total de unidades



// si la dgv no trae nada , poner panel con no hay informacion