﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.Logistica;

namespace CapaPresentacion.Mantenimiento
{

    public partial class frmDtpSalida : Form
    {
        public delegate void EnviaFecha(E_Taller e_taller);
        public event EnviaFecha enviaFecha;
        public frmDtpSalida(E_Taller e_taller)
        {
            InitializeComponent();
            this.e_taller = e_taller;
        }
        E_Taller e_taller;

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (sender == btnOk)
            {
                if (dtp.Value < e_taller.Entrada_taller)
                {
                    MessageBox.Show("Ha ingresado una fecha menor a la fecha de entrada del pedido (" + e_taller.Entrada_taller + ")!");
                }
                else if (new DateTime(dtp.Value.Year,dtp.Value.Month,dtp.Value.Day) < new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day))
                {
                    MessageBox.Show("Ha ingresado una fecha menor a la fecha actual");
                }
                else
                {
                    e_taller.Salida_aprox = new DateTime(dtp.Value.Year, dtp.Value.Month, dtp.Value.Day);
                    enviaFecha(e_taller);
                    this.Dispose();
                }
                
            }
            if (sender == btnCancel)
            {
                this.Close();
            }
        }

        private void frmDtpSalida_Load(object sender, EventArgs e)
        {

        }
    }
}
