﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GestionTransporte
{
    public partial class frmCalendario : Form
    {
        public frmCalendario(DataTable viajes)
        {
            InitializeComponent();
            Application.EnableVisualStyles();
            this.viajes = viajes;         
        }

#region propiedades
        DataTable viajes;
        DateTime actual = DateTime.Now;
#endregion
        private void Form1_Load(object sender, EventArgs e) {       
            cargarCalendario(actual);         
        }   //carga calendario llama a la funcion carga viajes 
        void cargarCalendario(DateTime mes)
        {
            //saca la cantidad de dia del mes 
            int cantidadDeDias = System.DateTime.DaysInMonth(mes.Year,mes.Month);
            DateTime PrimerDiaDelMes = new DateTime(mes.Year,mes.Month,1);
            int ultimoDiaDelMes = DateTime.DaysInMonth(mes.Year,mes.Month);
            int diaDeLaPrimerSemana=Convert.ToInt32(PrimerDiaDelMes.DayOfWeek);
            
            lblMes.Text = mes.ToString("MMMM").ToUpper();
            foreach (Control a in panel1.Controls)
            {
                int tag=Convert.ToInt32(a.Tag.ToString());
                int controlador = tag - diaDeLaPrimerSemana;
                if (tag > diaDeLaPrimerSemana && tag <= (diaDeLaPrimerSemana + cantidadDeDias))
                {
                    a.Text = controlador.ToString();
                    a.BackColor = Color.White;
                    a.Visible = true;
                }
                else
                {
                    a.Visible = false;
                }
            }
            cargaViajes();
        }
        private void btnAtras_Click(object sender, EventArgs e)
        {
            tbFechas.Clear();//Limpiamos textbox con datos de viaje
            if (sender == btnAtras) 
            {
                actual= actual.AddMonths(-1);
                cargarCalendario(actual);
            }
            if (sender == btnAdelante)
            {
                actual = actual.AddMonths(1);
                cargarCalendario(actual);
            }
        }
        private void lblMes_Click(object sender, EventArgs e)//al hacer click sobre lso labels con los dias del mes ,si este contiene un viaje .mostrara informacion en el pied del form
        {
            tbFechas.Clear();//Limpiamos textbox con datos de viaje
            //traigo la fecha del sender (label) clickeado
            DateTime diaDelControl = new DateTime(actual.Year, actual.Month, Convert.ToInt32((sender as Label).Text.ToString()));

            foreach (DataRow fila in viajes.Rows)
            {
                DateTime salida = new DateTime(Convert.ToDateTime(fila[1].ToString()).Year, Convert.ToDateTime(fila[1].ToString()).Month, Convert.ToDateTime(fila[1].ToString()).Day); // 1 es el  campo de fecha de salida aproximada
                DateTime regreso = new DateTime(Convert.ToDateTime(fila[4].ToString()).Year, Convert.ToDateTime(fila[4].ToString()).Month, Convert.ToDateTime(fila[4].ToString()).Day); // 4 es el  campo de fecha de salida aproximada
                if (diaDelControl >= salida && diaDelControl <= regreso)
                {  
                     tbFechas.Text = tbFechas.Text + " ID-VIAJE:  "  + fila[0].ToString() + Environment.NewLine + "************************" + Environment.NewLine + " Observaciones viaje:  " + fila[6].ToString().Trim() + Environment.NewLine +
                        " Observaciones pedido:  " + fila[13].ToString().Trim() + Environment.NewLine +
                                       " Cliente: " + fila[8].ToString().Trim() + Environment.NewLine
                                      +"Salida aprox.: "+ fila[1].ToString() + Environment.NewLine + "Regreso aprox.: " + fila[4].ToString() + Environment.NewLine + "************************" + Environment.NewLine;
                }
            }
        }
        void cargaViajes()
        {
            foreach (DataRow fila in viajes.Rows)
            {
                //string valor = fila["NombreDeLaColumna"].ToString();
                //string valor = fila[0].ToString();
                DateTime salida=new DateTime( Convert.ToDateTime(fila[1].ToString()).Year, Convert.ToDateTime(fila[1].ToString()).Month, Convert.ToDateTime(fila[1].ToString()).Day); // 1 es el  campo de fecha de salida aproximada
                DateTime regreso= new DateTime(Convert.ToDateTime(fila[4].ToString()).Year, Convert.ToDateTime(fila[4].ToString()).Month, Convert.ToDateTime(fila[4].ToString()).Day); // 4 es el  campo de fecha de salida aproximada
                int  diaSalida=salida.Day;
                int diaRegreso = regreso.Day; 

                foreach(Control a in panel1.Controls)
                {
                    //ejemplo: un viaje va de 26/10 al 2/11 ...puede pasar que al recorrer por cada label
                    //se marque la fecha 2/10,entonces con "actual" me aseguro de que eso no pase
                    if (a.Visible) {
                        DateTime diaDelControl = new DateTime(actual.Year, actual.Month, Convert.ToInt32(a.Text.ToString()));
                        if (salida == diaDelControl || regreso == diaDelControl)
                        {
                            a.BackColor = Color.LightGreen;
                        }
                        if (diaDelControl > salida && diaDelControl < regreso)
                        {
                            a.BackColor = Color.LightSalmon;

                        }
                    }
                }
            }
        }
    }
}
