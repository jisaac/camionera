﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.FormsMsj
{
    public partial class frmMensajes : Form
    {
        public delegate void ResultadoDeDialogo(bool sIoNo);
        public event ResultadoDeDialogo SioNo;
        public frmMensajes(string tipo, string mensaje)
        {
            InitializeComponent();
            this.tipo = tipo;
            this.mensaje = mensaje;
        }
        string tipo;
        string mensaje;
        private void frmMensajes_Load(object sender, EventArgs e)
        {

            //https://stackoverflow.com/questions/1192054/load-image-from-resources-area-of-project-in-c-sharp  
            //https://www.flaticon.com/free-icon/info_189664
            //https://www.flaticon.com/free-icon/error_148766
            //https://www.flaticon.com/free-icon/success_148767
            //https://www.flaticon.com/free-icon/warning_148768
            this.verifica_tamaño();

            switch (tipo)
            {
                case "Exito":
                    this.BackColor = Color.LightGreen;
                    this.lbMsj.Text = mensaje;
                    this.Text = tipo;
                    btnNo.Visible = false;
                    btnSi.Visible = false;
                    BtnOk.Visible = true;
                    pbIcon.Image = Properties.Resources.success;
                    break;
                case "Error":
                    this.BackColor = Color.Salmon;
                    this.lbMsj.Text = mensaje;
                    this.Text = tipo;
                    btnNo.Visible = false;
                    btnSi.Visible = false;
                    BtnOk.Visible = true;
                    pbIcon.Image = Properties.Resources.error;
                    break;
                case "Advertencia":
                    this.BackColor = Color.LightYellow;
                    this.lbMsj.Text = mensaje;
                    this.Text = tipo;
                    btnNo.Visible = true;
                    btnSi.Visible = true;
                    BtnOk.Visible = false;
                    pbIcon.Image = Properties.Resources.warning;
                    break;
                case "Pregunta":
                    this.BackColor = Color.Beige;
                    this.lbMsj.Text = mensaje;
                    this.Text = tipo;
                    btnNo.Visible = true;
                    btnSi.Visible = true;
                    BtnOk.Visible = false;
                    pbIcon.Image = Properties.Resources.question;
                    break;
                    
                case "Informacion":
                    this.BackColor = Color.Beige;
                    this.lbMsj.Text = mensaje;
                    this.Text = tipo;
                    btnNo.Visible = false;
                    btnSi.Visible = false;
                    BtnOk.Visible = true;
                    pbIcon.Image = Properties.Resources.info;
                    break;
                default:
                    this.BackColor = Color.Salmon;
                    this.Text = "Error";
                    btnNo.Visible = false;
                    btnSi.Visible = false;
                    BtnOk.Visible = true;
                    pbIcon.Image = Properties.Resources.error;
                    lbMsj.Text = "Ups, Error al mostrar mensaje";
                    break;
                    

            }
        }

        private void btnSi_Click(object sender, EventArgs e)
        {
            if(sender == BtnOk)
            {
                this.Dispose();
            }
            if (sender== btnNo)
            {
                SioNo(false);
                this.Dispose();
            }
            if(sender == btnSi)
            {
                SioNo(true);
                this.Dispose();
            }
        }

        private void verifica_tamaño()
        {
            int length = mensaje.Length;
            if(length>20) 
                lbMsj.Font = new Font("Trebuchet MS", 14, FontStyle.Bold);
            else if (length > 20)
                lbMsj.Font = new Font("Trebuchet MS", 12, FontStyle.Bold);
        }
    }
}
