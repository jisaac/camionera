﻿namespace CapaPresentacion.FormsMsj
{
    partial class Exito
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Exito));
            this.lbAccion = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbMsj = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // lbAccion
            // 
            this.lbAccion.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAccion.Location = new System.Drawing.Point(14, 52);
            this.lbAccion.Name = "lbAccion";
            this.lbAccion.Size = new System.Drawing.Size(299, 26);
            this.lbAccion.TabIndex = 8;
            this.lbAccion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(146, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(33, 33);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // lbMsj
            // 
            this.lbMsj.Font = new System.Drawing.Font("Ubuntu Condensed", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMsj.Location = new System.Drawing.Point(12, 83);
            this.lbMsj.Name = "lbMsj";
            this.lbMsj.Size = new System.Drawing.Size(301, 30);
            this.lbMsj.TabIndex = 6;
            this.lbMsj.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Exito
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(176)))), ((int)(((byte)(253)))), ((int)(((byte)(170)))));
            this.ClientSize = new System.Drawing.Size(325, 118);
            this.Controls.Add(this.lbAccion);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.lbMsj);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Exito";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Exito";
            this.Load += new System.EventHandler(this.Exito_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbAccion;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbMsj;
    }
}