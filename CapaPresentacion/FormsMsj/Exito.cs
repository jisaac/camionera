﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.FormsMsj
{
    public partial class Exito : Form
    {
        string msj;
        string acción;
        public Exito(string acción, string msj)
        {
            this.acción = acción;
            this.msj = msj;
            InitializeComponent();
        }
        private void Exito_Load(object sender, EventArgs e)
        {
            lbAccion.Text = acción;
            lbMsj.Text = msj;
        }
    }
}
