﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.FormsMsj
{
    public partial class Error : Form
    {
        string msj;
        string acción;
        public Error(string acción, string msj)
        {
            this.acción = acción;
            this.msj = msj;
            InitializeComponent();
        }

        private void Error_Load(object sender, EventArgs e)
        {
            lbAccion.Text = acción;
            lbMsj.Text = msj;
        }
    }
}
