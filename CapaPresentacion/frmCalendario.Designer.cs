﻿namespace GestionTransporte
{
    partial class frmCalendario
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.l42 = new System.Windows.Forms.Label();
            this.l41 = new System.Windows.Forms.Label();
            this.l40 = new System.Windows.Forms.Label();
            this.l39 = new System.Windows.Forms.Label();
            this.l38 = new System.Windows.Forms.Label();
            this.l37 = new System.Windows.Forms.Label();
            this.l36 = new System.Windows.Forms.Label();
            this.l35 = new System.Windows.Forms.Label();
            this.l34 = new System.Windows.Forms.Label();
            this.l33 = new System.Windows.Forms.Label();
            this.l32 = new System.Windows.Forms.Label();
            this.l31 = new System.Windows.Forms.Label();
            this.l30 = new System.Windows.Forms.Label();
            this.l29 = new System.Windows.Forms.Label();
            this.l28 = new System.Windows.Forms.Label();
            this.l27 = new System.Windows.Forms.Label();
            this.l26 = new System.Windows.Forms.Label();
            this.l25 = new System.Windows.Forms.Label();
            this.l24 = new System.Windows.Forms.Label();
            this.l23 = new System.Windows.Forms.Label();
            this.l22 = new System.Windows.Forms.Label();
            this.l21 = new System.Windows.Forms.Label();
            this.l20 = new System.Windows.Forms.Label();
            this.l19 = new System.Windows.Forms.Label();
            this.l18 = new System.Windows.Forms.Label();
            this.l17 = new System.Windows.Forms.Label();
            this.l16 = new System.Windows.Forms.Label();
            this.l15 = new System.Windows.Forms.Label();
            this.l14 = new System.Windows.Forms.Label();
            this.l13 = new System.Windows.Forms.Label();
            this.l12 = new System.Windows.Forms.Label();
            this.l11 = new System.Windows.Forms.Label();
            this.l10 = new System.Windows.Forms.Label();
            this.l9 = new System.Windows.Forms.Label();
            this.l8 = new System.Windows.Forms.Label();
            this.l7 = new System.Windows.Forms.Label();
            this.l6 = new System.Windows.Forms.Label();
            this.l5 = new System.Windows.Forms.Label();
            this.l4 = new System.Windows.Forms.Label();
            this.l3 = new System.Windows.Forms.Label();
            this.l2 = new System.Windows.Forms.Label();
            this.l1 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblMes = new System.Windows.Forms.Label();
            this.tbFechas = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnAdelante = new System.Windows.Forms.Button();
            this.btnAtras = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // l42
            // 
            this.l42.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l42.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l42.ForeColor = System.Drawing.Color.DarkGreen;
            this.l42.Location = new System.Drawing.Point(439, 241);
            this.l42.Name = "l42";
            this.l42.Size = new System.Drawing.Size(65, 39);
            this.l42.TabIndex = 42;
            this.l42.Tag = "42";
            this.l42.Text = "label49";
            this.l42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l42.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l41
            // 
            this.l41.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l41.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l41.ForeColor = System.Drawing.Color.DarkGreen;
            this.l41.Location = new System.Drawing.Point(368, 241);
            this.l41.Name = "l41";
            this.l41.Size = new System.Drawing.Size(65, 39);
            this.l41.TabIndex = 41;
            this.l41.Tag = "41";
            this.l41.Text = "label48";
            this.l41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l41.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l40
            // 
            this.l40.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l40.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l40.ForeColor = System.Drawing.Color.DarkGreen;
            this.l40.Location = new System.Drawing.Point(296, 241);
            this.l40.Name = "l40";
            this.l40.Size = new System.Drawing.Size(65, 39);
            this.l40.TabIndex = 40;
            this.l40.Tag = "40";
            this.l40.Text = "label47";
            this.l40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l40.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l39
            // 
            this.l39.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l39.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l39.ForeColor = System.Drawing.Color.DarkGreen;
            this.l39.Location = new System.Drawing.Point(224, 241);
            this.l39.Name = "l39";
            this.l39.Size = new System.Drawing.Size(65, 39);
            this.l39.TabIndex = 38;
            this.l39.Tag = "39";
            this.l39.Text = "label46";
            this.l39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l39.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l38
            // 
            this.l38.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l38.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l38.ForeColor = System.Drawing.Color.DarkGreen;
            this.l38.Location = new System.Drawing.Point(152, 241);
            this.l38.Name = "l38";
            this.l38.Size = new System.Drawing.Size(65, 39);
            this.l38.TabIndex = 37;
            this.l38.Tag = "38";
            this.l38.Text = "label45";
            this.l38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l38.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l37
            // 
            this.l37.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l37.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l37.ForeColor = System.Drawing.Color.DarkGreen;
            this.l37.Location = new System.Drawing.Point(80, 241);
            this.l37.Name = "l37";
            this.l37.Size = new System.Drawing.Size(65, 39);
            this.l37.TabIndex = 36;
            this.l37.Tag = "37";
            this.l37.Text = "label44";
            this.l37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l37.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l36
            // 
            this.l36.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l36.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l36.ForeColor = System.Drawing.Color.DarkGreen;
            this.l36.Location = new System.Drawing.Point(9, 241);
            this.l36.Name = "l36";
            this.l36.Size = new System.Drawing.Size(65, 39);
            this.l36.TabIndex = 35;
            this.l36.Tag = "36";
            this.l36.Text = "label43";
            this.l36.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l36.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l35
            // 
            this.l35.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l35.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l35.ForeColor = System.Drawing.Color.DarkGreen;
            this.l35.Location = new System.Drawing.Point(439, 195);
            this.l35.Name = "l35";
            this.l35.Size = new System.Drawing.Size(65, 39);
            this.l35.TabIndex = 34;
            this.l35.Tag = "35";
            this.l35.Text = "label42";
            this.l35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l35.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l34
            // 
            this.l34.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l34.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l34.ForeColor = System.Drawing.Color.DarkGreen;
            this.l34.Location = new System.Drawing.Point(368, 195);
            this.l34.Name = "l34";
            this.l34.Size = new System.Drawing.Size(65, 39);
            this.l34.TabIndex = 33;
            this.l34.Tag = "34";
            this.l34.Text = "label41";
            this.l34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l34.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l33
            // 
            this.l33.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l33.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l33.ForeColor = System.Drawing.Color.DarkGreen;
            this.l33.Location = new System.Drawing.Point(296, 195);
            this.l33.Name = "l33";
            this.l33.Size = new System.Drawing.Size(65, 39);
            this.l33.TabIndex = 32;
            this.l33.Tag = "33";
            this.l33.Text = "label40";
            this.l33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l33.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l32
            // 
            this.l32.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l32.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l32.ForeColor = System.Drawing.Color.DarkGreen;
            this.l32.Location = new System.Drawing.Point(224, 195);
            this.l32.Name = "l32";
            this.l32.Size = new System.Drawing.Size(65, 39);
            this.l32.TabIndex = 31;
            this.l32.Tag = "32";
            this.l32.Text = "label39";
            this.l32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l32.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l31
            // 
            this.l31.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l31.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l31.ForeColor = System.Drawing.Color.DarkGreen;
            this.l31.Location = new System.Drawing.Point(152, 195);
            this.l31.Name = "l31";
            this.l31.Size = new System.Drawing.Size(65, 39);
            this.l31.TabIndex = 30;
            this.l31.Tag = "31";
            this.l31.Text = "label38";
            this.l31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l31.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l30
            // 
            this.l30.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l30.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l30.ForeColor = System.Drawing.Color.DarkGreen;
            this.l30.Location = new System.Drawing.Point(80, 195);
            this.l30.Name = "l30";
            this.l30.Size = new System.Drawing.Size(65, 39);
            this.l30.TabIndex = 29;
            this.l30.Tag = "30";
            this.l30.Text = "label37";
            this.l30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l30.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l29
            // 
            this.l29.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l29.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l29.ForeColor = System.Drawing.Color.DarkGreen;
            this.l29.Location = new System.Drawing.Point(9, 195);
            this.l29.Name = "l29";
            this.l29.Size = new System.Drawing.Size(65, 39);
            this.l29.TabIndex = 28;
            this.l29.Tag = "29";
            this.l29.Text = "label36";
            this.l29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l29.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l28
            // 
            this.l28.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l28.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l28.ForeColor = System.Drawing.Color.DarkGreen;
            this.l28.Location = new System.Drawing.Point(439, 149);
            this.l28.Name = "l28";
            this.l28.Size = new System.Drawing.Size(65, 39);
            this.l28.TabIndex = 27;
            this.l28.Tag = "28";
            this.l28.Text = "label35";
            this.l28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l28.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l27
            // 
            this.l27.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l27.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l27.ForeColor = System.Drawing.Color.DarkGreen;
            this.l27.Location = new System.Drawing.Point(368, 149);
            this.l27.Name = "l27";
            this.l27.Size = new System.Drawing.Size(65, 39);
            this.l27.TabIndex = 26;
            this.l27.Tag = "27";
            this.l27.Text = "label34";
            this.l27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l27.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l26
            // 
            this.l26.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l26.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l26.ForeColor = System.Drawing.Color.DarkGreen;
            this.l26.Location = new System.Drawing.Point(296, 149);
            this.l26.Name = "l26";
            this.l26.Size = new System.Drawing.Size(65, 39);
            this.l26.TabIndex = 25;
            this.l26.Tag = "26";
            this.l26.Text = "label33";
            this.l26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l26.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l25
            // 
            this.l25.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l25.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l25.ForeColor = System.Drawing.Color.DarkGreen;
            this.l25.Location = new System.Drawing.Point(224, 149);
            this.l25.Name = "l25";
            this.l25.Size = new System.Drawing.Size(65, 39);
            this.l25.TabIndex = 24;
            this.l25.Tag = "25";
            this.l25.Text = "label32";
            this.l25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l25.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l24
            // 
            this.l24.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l24.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l24.ForeColor = System.Drawing.Color.DarkGreen;
            this.l24.Location = new System.Drawing.Point(152, 149);
            this.l24.Name = "l24";
            this.l24.Size = new System.Drawing.Size(65, 39);
            this.l24.TabIndex = 23;
            this.l24.Tag = "24";
            this.l24.Text = "label31";
            this.l24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l24.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l23
            // 
            this.l23.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l23.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l23.ForeColor = System.Drawing.Color.DarkGreen;
            this.l23.Location = new System.Drawing.Point(80, 149);
            this.l23.Name = "l23";
            this.l23.Size = new System.Drawing.Size(65, 39);
            this.l23.TabIndex = 22;
            this.l23.Tag = "23";
            this.l23.Text = "label30";
            this.l23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l23.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l22
            // 
            this.l22.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l22.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l22.ForeColor = System.Drawing.Color.DarkGreen;
            this.l22.Location = new System.Drawing.Point(9, 149);
            this.l22.Name = "l22";
            this.l22.Size = new System.Drawing.Size(65, 39);
            this.l22.TabIndex = 21;
            this.l22.Tag = "22";
            this.l22.Text = "label29";
            this.l22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l22.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l21
            // 
            this.l21.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l21.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l21.ForeColor = System.Drawing.Color.DarkGreen;
            this.l21.Location = new System.Drawing.Point(439, 103);
            this.l21.Name = "l21";
            this.l21.Size = new System.Drawing.Size(65, 39);
            this.l21.TabIndex = 20;
            this.l21.Tag = "21";
            this.l21.Text = "label28";
            this.l21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l21.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l20
            // 
            this.l20.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l20.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l20.ForeColor = System.Drawing.Color.DarkGreen;
            this.l20.Location = new System.Drawing.Point(368, 103);
            this.l20.Name = "l20";
            this.l20.Size = new System.Drawing.Size(65, 39);
            this.l20.TabIndex = 19;
            this.l20.Tag = "20";
            this.l20.Text = "label27";
            this.l20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l20.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l19
            // 
            this.l19.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l19.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l19.ForeColor = System.Drawing.Color.DarkGreen;
            this.l19.Location = new System.Drawing.Point(296, 103);
            this.l19.Name = "l19";
            this.l19.Size = new System.Drawing.Size(65, 39);
            this.l19.TabIndex = 18;
            this.l19.Tag = "19";
            this.l19.Text = "label26";
            this.l19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l19.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l18
            // 
            this.l18.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l18.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l18.ForeColor = System.Drawing.Color.DarkGreen;
            this.l18.Location = new System.Drawing.Point(224, 103);
            this.l18.Name = "l18";
            this.l18.Size = new System.Drawing.Size(65, 39);
            this.l18.TabIndex = 17;
            this.l18.Tag = "18";
            this.l18.Text = "label25";
            this.l18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l18.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l17
            // 
            this.l17.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l17.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l17.ForeColor = System.Drawing.Color.DarkGreen;
            this.l17.Location = new System.Drawing.Point(152, 103);
            this.l17.Name = "l17";
            this.l17.Size = new System.Drawing.Size(65, 39);
            this.l17.TabIndex = 16;
            this.l17.Tag = "17";
            this.l17.Text = "label24";
            this.l17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l17.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l16
            // 
            this.l16.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l16.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l16.ForeColor = System.Drawing.Color.DarkGreen;
            this.l16.Location = new System.Drawing.Point(80, 103);
            this.l16.Name = "l16";
            this.l16.Size = new System.Drawing.Size(65, 39);
            this.l16.TabIndex = 15;
            this.l16.Tag = "16";
            this.l16.Text = "label23";
            this.l16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l16.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l15
            // 
            this.l15.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l15.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l15.ForeColor = System.Drawing.Color.DarkGreen;
            this.l15.Location = new System.Drawing.Point(9, 103);
            this.l15.Name = "l15";
            this.l15.Size = new System.Drawing.Size(65, 39);
            this.l15.TabIndex = 14;
            this.l15.Tag = "15";
            this.l15.Text = "label22";
            this.l15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l15.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l14
            // 
            this.l14.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l14.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l14.ForeColor = System.Drawing.Color.DarkGreen;
            this.l14.Location = new System.Drawing.Point(439, 57);
            this.l14.Name = "l14";
            this.l14.Size = new System.Drawing.Size(65, 39);
            this.l14.TabIndex = 13;
            this.l14.Tag = "14";
            this.l14.Text = "label21";
            this.l14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l14.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l13
            // 
            this.l13.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l13.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l13.ForeColor = System.Drawing.Color.DarkGreen;
            this.l13.Location = new System.Drawing.Point(368, 57);
            this.l13.Name = "l13";
            this.l13.Size = new System.Drawing.Size(65, 39);
            this.l13.TabIndex = 12;
            this.l13.Tag = "13";
            this.l13.Text = "label20";
            this.l13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l13.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l12
            // 
            this.l12.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l12.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l12.ForeColor = System.Drawing.Color.DarkGreen;
            this.l12.Location = new System.Drawing.Point(296, 57);
            this.l12.Name = "l12";
            this.l12.Size = new System.Drawing.Size(65, 39);
            this.l12.TabIndex = 11;
            this.l12.Tag = "12";
            this.l12.Text = "label19";
            this.l12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l12.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l11
            // 
            this.l11.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l11.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l11.ForeColor = System.Drawing.Color.DarkGreen;
            this.l11.Location = new System.Drawing.Point(224, 57);
            this.l11.Name = "l11";
            this.l11.Size = new System.Drawing.Size(65, 39);
            this.l11.TabIndex = 10;
            this.l11.Tag = "11";
            this.l11.Text = "label18";
            this.l11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l11.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l10
            // 
            this.l10.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l10.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l10.ForeColor = System.Drawing.Color.DarkGreen;
            this.l10.Location = new System.Drawing.Point(152, 57);
            this.l10.Name = "l10";
            this.l10.Size = new System.Drawing.Size(65, 39);
            this.l10.TabIndex = 9;
            this.l10.Tag = "10";
            this.l10.Text = "label17";
            this.l10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l10.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l9
            // 
            this.l9.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l9.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l9.ForeColor = System.Drawing.Color.DarkGreen;
            this.l9.Location = new System.Drawing.Point(80, 57);
            this.l9.Name = "l9";
            this.l9.Size = new System.Drawing.Size(65, 39);
            this.l9.TabIndex = 8;
            this.l9.Tag = "9";
            this.l9.Text = "label16";
            this.l9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l9.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l8
            // 
            this.l8.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l8.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l8.ForeColor = System.Drawing.Color.DarkGreen;
            this.l8.Location = new System.Drawing.Point(9, 57);
            this.l8.Name = "l8";
            this.l8.Size = new System.Drawing.Size(65, 39);
            this.l8.TabIndex = 7;
            this.l8.Tag = "8";
            this.l8.Text = "label15";
            this.l8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l8.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l7
            // 
            this.l7.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l7.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l7.ForeColor = System.Drawing.Color.DarkGreen;
            this.l7.Location = new System.Drawing.Point(439, 11);
            this.l7.Name = "l7";
            this.l7.Size = new System.Drawing.Size(65, 39);
            this.l7.TabIndex = 6;
            this.l7.Tag = "7";
            this.l7.Text = "label14";
            this.l7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l7.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l6
            // 
            this.l6.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l6.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l6.ForeColor = System.Drawing.Color.DarkGreen;
            this.l6.Location = new System.Drawing.Point(368, 11);
            this.l6.Name = "l6";
            this.l6.Size = new System.Drawing.Size(65, 39);
            this.l6.TabIndex = 5;
            this.l6.Tag = "6";
            this.l6.Text = "label13";
            this.l6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l6.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l5
            // 
            this.l5.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l5.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l5.ForeColor = System.Drawing.Color.DarkGreen;
            this.l5.Location = new System.Drawing.Point(296, 11);
            this.l5.Name = "l5";
            this.l5.Size = new System.Drawing.Size(65, 39);
            this.l5.TabIndex = 4;
            this.l5.Tag = "5";
            this.l5.Text = "label12";
            this.l5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l5.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l4
            // 
            this.l4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l4.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l4.ForeColor = System.Drawing.Color.DarkGreen;
            this.l4.Location = new System.Drawing.Point(224, 11);
            this.l4.Name = "l4";
            this.l4.Size = new System.Drawing.Size(65, 39);
            this.l4.TabIndex = 3;
            this.l4.Tag = "4";
            this.l4.Text = "label11";
            this.l4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l4.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l3
            // 
            this.l3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l3.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l3.ForeColor = System.Drawing.Color.DarkGreen;
            this.l3.Location = new System.Drawing.Point(152, 11);
            this.l3.Name = "l3";
            this.l3.Size = new System.Drawing.Size(65, 39);
            this.l3.TabIndex = 2;
            this.l3.Tag = "3";
            this.l3.Text = "label10";
            this.l3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l3.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l2
            // 
            this.l2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l2.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l2.ForeColor = System.Drawing.Color.DarkGreen;
            this.l2.Location = new System.Drawing.Point(80, 11);
            this.l2.Name = "l2";
            this.l2.Size = new System.Drawing.Size(65, 39);
            this.l2.TabIndex = 1;
            this.l2.Tag = "2";
            this.l2.Text = "label9";
            this.l2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l2.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // l1
            // 
            this.l1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.l1.Font = new System.Drawing.Font("Lucida Bright", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.l1.ForeColor = System.Drawing.Color.DarkGreen;
            this.l1.Location = new System.Drawing.Point(9, 11);
            this.l1.Name = "l1";
            this.l1.Size = new System.Drawing.Size(65, 39);
            this.l1.TabIndex = 0;
            this.l1.Tag = "1";
            this.l1.Text = "label8";
            this.l1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.l1.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DarkCyan;
            this.label1.Location = new System.Drawing.Point(80, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Domingo";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.DarkCyan;
            this.label2.Location = new System.Drawing.Point(154, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 16);
            this.label2.TabIndex = 1;
            this.label2.Text = "Lunes";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.DarkCyan;
            this.label3.Location = new System.Drawing.Point(223, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 2;
            this.label3.Text = "Martes";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.DarkCyan;
            this.label4.Location = new System.Drawing.Point(295, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(73, 16);
            this.label4.TabIndex = 3;
            this.label4.Text = "Miercoles";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.DarkCyan;
            this.label5.Location = new System.Drawing.Point(374, 51);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 16);
            this.label5.TabIndex = 4;
            this.label5.Text = "Jueves";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.DarkCyan;
            this.label6.Location = new System.Drawing.Point(439, 51);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(60, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Viernes";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Century", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.DarkCyan;
            this.label7.Location = new System.Drawing.Point(510, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 16);
            this.label7.TabIndex = 6;
            this.label7.Text = "Sabado";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.l1);
            this.panel1.Controls.Add(this.l42);
            this.panel1.Controls.Add(this.l7);
            this.panel1.Controls.Add(this.l25);
            this.panel1.Controls.Add(this.l16);
            this.panel1.Controls.Add(this.l34);
            this.panel1.Controls.Add(this.l33);
            this.panel1.Controls.Add(this.l15);
            this.panel1.Controls.Add(this.l24);
            this.panel1.Controls.Add(this.l6);
            this.panel1.Controls.Add(this.l41);
            this.panel1.Controls.Add(this.l8);
            this.panel1.Controls.Add(this.l32);
            this.panel1.Controls.Add(this.l17);
            this.panel1.Controls.Add(this.l35);
            this.panel1.Controls.Add(this.l26);
            this.panel1.Controls.Add(this.l14);
            this.panel1.Controls.Add(this.l9);
            this.panel1.Controls.Add(this.l23);
            this.panel1.Controls.Add(this.l40);
            this.panel1.Controls.Add(this.l22);
            this.panel1.Controls.Add(this.l5);
            this.panel1.Controls.Add(this.l18);
            this.panel1.Controls.Add(this.l36);
            this.panel1.Controls.Add(this.l31);
            this.panel1.Controls.Add(this.l13);
            this.panel1.Controls.Add(this.l10);
            this.panel1.Controls.Add(this.l27);
            this.panel1.Controls.Add(this.l39);
            this.panel1.Controls.Add(this.l21);
            this.panel1.Controls.Add(this.l4);
            this.panel1.Controls.Add(this.l19);
            this.panel1.Controls.Add(this.l37);
            this.panel1.Controls.Add(this.l29);
            this.panel1.Controls.Add(this.l30);
            this.panel1.Controls.Add(this.l12);
            this.panel1.Controls.Add(this.l11);
            this.panel1.Controls.Add(this.l3);
            this.panel1.Controls.Add(this.l28);
            this.panel1.Controls.Add(this.l38);
            this.panel1.Controls.Add(this.l20);
            this.panel1.Controls.Add(this.l2);
            this.panel1.Location = new System.Drawing.Point(74, 81);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(513, 282);
            this.panel1.TabIndex = 43;
            // 
            // lblMes
            // 
            this.lblMes.Font = new System.Drawing.Font("Lucida Bright", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMes.ForeColor = System.Drawing.Color.Gold;
            this.lblMes.Location = new System.Drawing.Point(157, 9);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(350, 42);
            this.lblMes.TabIndex = 44;
            this.lblMes.Text = "label8";
            this.lblMes.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblMes.Click += new System.EventHandler(this.lblMes_Click);
            // 
            // tbFechas
            // 
            this.tbFechas.BackColor = System.Drawing.Color.WhiteSmoke;
            this.tbFechas.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tbFechas.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbFechas.ForeColor = System.Drawing.Color.Gray;
            this.tbFechas.Location = new System.Drawing.Point(12, 364);
            this.tbFechas.Multiline = true;
            this.tbFechas.Name = "tbFechas";
            this.tbFechas.ReadOnly = true;
            this.tbFechas.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbFechas.Size = new System.Drawing.Size(643, 118);
            this.tbFechas.TabIndex = 45;
            this.tbFechas.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.LightGreen;
            this.label8.Location = new System.Drawing.Point(246, 491);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(33, 15);
            this.label8.TabIndex = 46;
            // 
            // label9
            // 
            this.label9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(285, 491);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(150, 15);
            this.label9.TabIndex = 47;
            this.label9.Text = ": Cominezo/Fin de viaje";
            // 
            // btnAdelante
            // 
            this.btnAdelante.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdelante.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAdelante.Location = new System.Drawing.Point(584, 184);
            this.btnAdelante.Name = "btnAdelante";
            this.btnAdelante.Size = new System.Drawing.Size(56, 35);
            this.btnAdelante.TabIndex = 8;
            this.btnAdelante.UseVisualStyleBackColor = false;
            this.btnAdelante.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // btnAtras
            // 
            this.btnAtras.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAtras.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnAtras.Location = new System.Drawing.Point(21, 184);
            this.btnAtras.Name = "btnAtras";
            this.btnAtras.Size = new System.Drawing.Size(56, 35);
            this.btnAtras.TabIndex = 7;
            this.btnAtras.UseVisualStyleBackColor = false;
            this.btnAtras.Click += new System.EventHandler(this.btnAtras_Click);
            // 
            // frmCalendario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(667, 515);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tbFechas);
            this.Controls.Add(this.lblMes);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnAdelante);
            this.Controls.Add(this.btnAtras);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmCalendario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Calendario de viajes unidades - Logistica";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label l42;
        private System.Windows.Forms.Label l41;
        private System.Windows.Forms.Label l40;
        private System.Windows.Forms.Label l39;
        private System.Windows.Forms.Label l38;
        private System.Windows.Forms.Label l37;
        private System.Windows.Forms.Label l36;
        private System.Windows.Forms.Label l35;
        private System.Windows.Forms.Label l34;
        private System.Windows.Forms.Label l33;
        private System.Windows.Forms.Label l32;
        private System.Windows.Forms.Label l31;
        private System.Windows.Forms.Label l30;
        private System.Windows.Forms.Label l29;
        private System.Windows.Forms.Label l28;
        private System.Windows.Forms.Label l27;
        private System.Windows.Forms.Label l26;
        private System.Windows.Forms.Label l25;
        private System.Windows.Forms.Label l24;
        private System.Windows.Forms.Label l23;
        private System.Windows.Forms.Label l22;
        private System.Windows.Forms.Label l21;
        private System.Windows.Forms.Label l20;
        private System.Windows.Forms.Label l19;
        private System.Windows.Forms.Label l18;
        private System.Windows.Forms.Label l17;
        private System.Windows.Forms.Label l16;
        private System.Windows.Forms.Label l15;
        private System.Windows.Forms.Label l14;
        private System.Windows.Forms.Label l13;
        private System.Windows.Forms.Label l12;
        private System.Windows.Forms.Label l11;
        private System.Windows.Forms.Label l10;
        private System.Windows.Forms.Label l9;
        private System.Windows.Forms.Label l8;
        private System.Windows.Forms.Label l7;
        private System.Windows.Forms.Label l6;
        private System.Windows.Forms.Label l5;
        private System.Windows.Forms.Label l4;
        private System.Windows.Forms.Label l3;
        private System.Windows.Forms.Label l2;
        private System.Windows.Forms.Label l1;
        private System.Windows.Forms.Button btnAtras;
        private System.Windows.Forms.Button btnAdelante;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lblMes;
        private System.Windows.Forms.TextBox tbFechas;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
    }
}

