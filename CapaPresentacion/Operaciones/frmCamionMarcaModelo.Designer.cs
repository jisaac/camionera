﻿namespace CapaPresentacion
{
    partial class frmCamionMarcaModeloTipo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.gbGestionMarcas = new System.Windows.Forms.GroupBox();
            this.tbMarca = new System.Windows.Forms.TextBox();
            this.pnlMarcaAbm = new System.Windows.Forms.Panel();
            this.btnMarcaEliminar = new System.Windows.Forms.Button();
            this.btnMarcaModificar = new System.Windows.Forms.Button();
            this.dgvMarca = new System.Windows.Forms.DataGridView();
            this.btnMarcaNueva = new System.Windows.Forms.Button();
            this.gbGestionModelo = new System.Windows.Forms.GroupBox();
            this.tbModelo = new System.Windows.Forms.TextBox();
            this.pnlModeloAbm = new System.Windows.Forms.Panel();
            this.btnModeloEliminar = new System.Windows.Forms.Button();
            this.btnModeloModificar = new System.Windows.Forms.Button();
            this.btnModeloNuevo = new System.Windows.Forms.Button();
            this.dgvModelo = new System.Windows.Forms.DataGridView();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.gbGestionMarcas.SuspendLayout();
            this.pnlMarcaAbm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarca)).BeginInit();
            this.gbGestionModelo.SuspendLayout();
            this.pnlModeloAbm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelo)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(12, 2);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(339, 509);
            this.tabControl1.TabIndex = 6;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage1.Controls.Add(this.gbGestionMarcas);
            this.tabPage1.Controls.Add(this.gbGestionModelo);
            this.tabPage1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(331, 483);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Marcas y Modelos";
            // 
            // gbGestionMarcas
            // 
            this.gbGestionMarcas.Controls.Add(this.tbMarca);
            this.gbGestionMarcas.Controls.Add(this.pnlMarcaAbm);
            this.gbGestionMarcas.Controls.Add(this.dgvMarca);
            this.gbGestionMarcas.Controls.Add(this.btnMarcaNueva);
            this.gbGestionMarcas.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGestionMarcas.Location = new System.Drawing.Point(6, 5);
            this.gbGestionMarcas.Name = "gbGestionMarcas";
            this.gbGestionMarcas.Size = new System.Drawing.Size(317, 235);
            this.gbGestionMarcas.TabIndex = 1;
            this.gbGestionMarcas.TabStop = false;
            this.gbGestionMarcas.Text = "Gestion de marcas";
            // 
            // tbMarca
            // 
            this.tbMarca.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMarca.Location = new System.Drawing.Point(6, 201);
            this.tbMarca.MaxLength = 500;
            this.tbMarca.Multiline = true;
            this.tbMarca.Name = "tbMarca";
            this.tbMarca.ShortcutsEnabled = false;
            this.tbMarca.Size = new System.Drawing.Size(304, 28);
            this.tbMarca.TabIndex = 3;
            // 
            // pnlMarcaAbm
            // 
            this.pnlMarcaAbm.Controls.Add(this.btnMarcaEliminar);
            this.pnlMarcaAbm.Controls.Add(this.btnMarcaModificar);
            this.pnlMarcaAbm.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlMarcaAbm.Location = new System.Drawing.Point(88, 167);
            this.pnlMarcaAbm.Name = "pnlMarcaAbm";
            this.pnlMarcaAbm.Size = new System.Drawing.Size(222, 27);
            this.pnlMarcaAbm.TabIndex = 2;
            // 
            // btnMarcaEliminar
            // 
            this.btnMarcaEliminar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcaEliminar.Location = new System.Drawing.Point(147, 4);
            this.btnMarcaEliminar.Name = "btnMarcaEliminar";
            this.btnMarcaEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnMarcaEliminar.TabIndex = 2;
            this.btnMarcaEliminar.Text = "Eliminar";
            this.btnMarcaEliminar.UseVisualStyleBackColor = true;
            this.btnMarcaEliminar.Click += new System.EventHandler(this.btnMarcaNueva_Click);
            // 
            // btnMarcaModificar
            // 
            this.btnMarcaModificar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcaModificar.Location = new System.Drawing.Point(33, 4);
            this.btnMarcaModificar.Name = "btnMarcaModificar";
            this.btnMarcaModificar.Size = new System.Drawing.Size(75, 23);
            this.btnMarcaModificar.TabIndex = 1;
            this.btnMarcaModificar.Text = "Modificar";
            this.btnMarcaModificar.UseVisualStyleBackColor = true;
            this.btnMarcaModificar.Click += new System.EventHandler(this.btnMarcaNueva_Click);
            // 
            // dgvMarca
            // 
            this.dgvMarca.AllowUserToAddRows = false;
            this.dgvMarca.AllowUserToDeleteRows = false;
            this.dgvMarca.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMarca.Location = new System.Drawing.Point(6, 19);
            this.dgvMarca.MultiSelect = false;
            this.dgvMarca.Name = "dgvMarca";
            this.dgvMarca.ReadOnly = true;
            this.dgvMarca.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvMarca.Size = new System.Drawing.Size(304, 142);
            this.dgvMarca.TabIndex = 0;
            this.dgvMarca.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMarca_CellClick);
            // 
            // btnMarcaNueva
            // 
            this.btnMarcaNueva.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarcaNueva.Location = new System.Drawing.Point(6, 171);
            this.btnMarcaNueva.Name = "btnMarcaNueva";
            this.btnMarcaNueva.Size = new System.Drawing.Size(75, 23);
            this.btnMarcaNueva.TabIndex = 0;
            this.btnMarcaNueva.Text = "Nueva";
            this.btnMarcaNueva.UseVisualStyleBackColor = true;
            this.btnMarcaNueva.Click += new System.EventHandler(this.btnMarcaNueva_Click);
            // 
            // gbGestionModelo
            // 
            this.gbGestionModelo.Controls.Add(this.tbModelo);
            this.gbGestionModelo.Controls.Add(this.pnlModeloAbm);
            this.gbGestionModelo.Controls.Add(this.btnModeloNuevo);
            this.gbGestionModelo.Controls.Add(this.dgvModelo);
            this.gbGestionModelo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGestionModelo.Location = new System.Drawing.Point(6, 246);
            this.gbGestionModelo.Name = "gbGestionModelo";
            this.gbGestionModelo.Size = new System.Drawing.Size(317, 231);
            this.gbGestionModelo.TabIndex = 4;
            this.gbGestionModelo.TabStop = false;
            this.gbGestionModelo.Text = "Gestion de modelos";
            // 
            // tbModelo
            // 
            this.tbModelo.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbModelo.Location = new System.Drawing.Point(6, 196);
            this.tbModelo.MaxLength = 500;
            this.tbModelo.Multiline = true;
            this.tbModelo.Name = "tbModelo";
            this.tbModelo.ShortcutsEnabled = false;
            this.tbModelo.Size = new System.Drawing.Size(304, 28);
            this.tbModelo.TabIndex = 3;
            // 
            // pnlModeloAbm
            // 
            this.pnlModeloAbm.Controls.Add(this.btnModeloEliminar);
            this.pnlModeloAbm.Controls.Add(this.btnModeloModificar);
            this.pnlModeloAbm.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlModeloAbm.Location = new System.Drawing.Point(121, 167);
            this.pnlModeloAbm.Name = "pnlModeloAbm";
            this.pnlModeloAbm.Size = new System.Drawing.Size(189, 29);
            this.pnlModeloAbm.TabIndex = 2;
            // 
            // btnModeloEliminar
            // 
            this.btnModeloEliminar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModeloEliminar.Location = new System.Drawing.Point(114, 0);
            this.btnModeloEliminar.Name = "btnModeloEliminar";
            this.btnModeloEliminar.Size = new System.Drawing.Size(75, 23);
            this.btnModeloEliminar.TabIndex = 2;
            this.btnModeloEliminar.Text = "Eliminar";
            this.btnModeloEliminar.UseVisualStyleBackColor = true;
            this.btnModeloEliminar.Click += new System.EventHandler(this.btnModeloNuevo_Click);
            // 
            // btnModeloModificar
            // 
            this.btnModeloModificar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModeloModificar.Location = new System.Drawing.Point(3, 0);
            this.btnModeloModificar.Name = "btnModeloModificar";
            this.btnModeloModificar.Size = new System.Drawing.Size(75, 23);
            this.btnModeloModificar.TabIndex = 1;
            this.btnModeloModificar.Text = "Modificar";
            this.btnModeloModificar.UseVisualStyleBackColor = true;
            this.btnModeloModificar.Click += new System.EventHandler(this.btnModeloNuevo_Click);
            // 
            // btnModeloNuevo
            // 
            this.btnModeloNuevo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnModeloNuevo.Location = new System.Drawing.Point(6, 167);
            this.btnModeloNuevo.Name = "btnModeloNuevo";
            this.btnModeloNuevo.Size = new System.Drawing.Size(75, 23);
            this.btnModeloNuevo.TabIndex = 0;
            this.btnModeloNuevo.Text = "Nuevo";
            this.btnModeloNuevo.UseVisualStyleBackColor = true;
            this.btnModeloNuevo.Click += new System.EventHandler(this.btnModeloNuevo_Click);
            // 
            // dgvModelo
            // 
            this.dgvModelo.AllowUserToAddRows = false;
            this.dgvModelo.AllowUserToDeleteRows = false;
            this.dgvModelo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvModelo.Location = new System.Drawing.Point(6, 19);
            this.dgvModelo.MultiSelect = false;
            this.dgvModelo.Name = "dgvModelo";
            this.dgvModelo.ReadOnly = true;
            this.dgvModelo.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvModelo.Size = new System.Drawing.Size(304, 142);
            this.dgvModelo.TabIndex = 0;
            this.dgvModelo.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvMarca_CellClick);
            // 
            // frmCamionMarcaModeloTipo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(358, 514);
            this.Controls.Add(this.tabControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmCamionMarcaModeloTipo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ABM Marca y Modelo Camion - Operaciones";
            this.Load += new System.EventHandler(this.frmCamionMarcaModeloTipo_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.gbGestionMarcas.ResumeLayout(false);
            this.gbGestionMarcas.PerformLayout();
            this.pnlMarcaAbm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMarca)).EndInit();
            this.gbGestionModelo.ResumeLayout(false);
            this.gbGestionModelo.PerformLayout();
            this.pnlModeloAbm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvModelo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox gbGestionMarcas;
        private System.Windows.Forms.TextBox tbMarca;
        private System.Windows.Forms.Panel pnlMarcaAbm;
        private System.Windows.Forms.Button btnMarcaEliminar;
        private System.Windows.Forms.Button btnMarcaModificar;
        private System.Windows.Forms.DataGridView dgvMarca;
        private System.Windows.Forms.Button btnMarcaNueva;
        private System.Windows.Forms.GroupBox gbGestionModelo;
        private System.Windows.Forms.TextBox tbModelo;
        private System.Windows.Forms.Panel pnlModeloAbm;
        private System.Windows.Forms.Button btnModeloEliminar;
        private System.Windows.Forms.Button btnModeloModificar;
        private System.Windows.Forms.Button btnModeloNuevo;
        private System.Windows.Forms.DataGridView dgvModelo;
    }
}