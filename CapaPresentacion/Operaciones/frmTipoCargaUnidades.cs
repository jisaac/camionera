﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    public partial class frmTipoCargaUnidades : Form
    {
        string usuario;
        N_TipoCarga n_tipoCarga;
        E_TipoCarga e_tipoCarga;

        public frmTipoCargaUnidades(string usuario)
        {
            InitializeComponent();
            this.e_tipoCarga = new E_TipoCarga();
            n_tipoCarga = new N_TipoCarga(usuario);
        }

        private void frmTipoCargaUnidades_Load(object sender, EventArgs e)
        {
            pnlAbmTipoCarga.Enabled = false;
            tipoCargaTraerTodo();
        }

        //EN DGV todos los tipos de carga 
        void tipoCargaTraerTodo()
        {
            DataTable dt = n_tipoCarga.listarTodos();
            if (dt.Rows.Count > 0)
            {
                dgvTipoCarga.DataSource = dt;
                dgvTipoCarga.Columns[0].Visible = false;
                dgvTipoCarga.Columns[2].Visible = false;
                dgvTipoCarga.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            pnlAbmTipoCarga.Enabled = false;
        }

        private void btnTipoCargaNueva_Click(object sender, EventArgs e)
        {
            List<string> resp;
            if (sender == btnTipoCargaNueva)
            {
                try
                {
                    e_tipoCarga.Tipo = tbTipoCarga.Text;
                    resp = n_tipoCarga.insertar(e_tipoCarga);
                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
                tipoCargaTraerTodo();
            }
            if (sender == btnTipoCargaModificar)
            {
                try
                {
                    e_tipoCarga.Id = Convert.ToInt32(dgvTipoCarga.CurrentRow.Cells[0].Value.ToString());
                    e_tipoCarga.Tipo = tbTipoCarga.Text;
                    resp = n_tipoCarga.modificar(e_tipoCarga);
                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
                tipoCargaTraerTodo();
            }
            if (sender == tbTipoCargaEliminar)
            {
                try
                {
                    e_tipoCarga.Id = Convert.ToInt32(dgvTipoCarga.CurrentRow.Cells[0].Value.ToString());
                    resp = n_tipoCarga.baja(e_tipoCarga);
                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
                tipoCargaTraerTodo();
                tbTipoCarga.Clear();
            }
        }

        private void dgvTipoCarga_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (sender == dgvTipoCarga)
            {
                if (dgvTipoCarga.CurrentRow != null)
                {
                    pnlAbmTipoCarga.Enabled = true;
                }
                else
                {
                    pnlAbmTipoCarga.Enabled = false;
                }
            }
        }
    }
}
