﻿namespace CapaPresentacion.Operaciones
{
    partial class frmPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbObservaciones = new System.Windows.Forms.Label();
            this.lbPrioridad = new System.Windows.Forms.Label();
            this.tbObservaciones = new System.Windows.Forms.TextBox();
            this.cbPrioridad = new System.Windows.Forms.ComboBox();
            this.lbKmViaje = new System.Windows.Forms.Label();
            this.tbKmViaje = new System.Windows.Forms.TextBox();
            this.tbVolumen = new System.Windows.Forms.TextBox();
            this.lbVolumen = new System.Windows.Forms.Label();
            this.lbPesoNeto = new System.Windows.Forms.Label();
            this.tbPesoNeto = new System.Windows.Forms.TextBox();
            this.lbFechaRegreso = new System.Windows.Forms.Label();
            this.dtpRegreso = new System.Windows.Forms.DateTimePicker();
            this.lbFechaSalida = new System.Windows.Forms.Label();
            this.dtpSalida = new System.Windows.Forms.DateTimePicker();
            this.tbNumPedido = new System.Windows.Forms.Label();
            this.lbFechaPedido = new System.Windows.Forms.Label();
            this.dtpPedido = new System.Windows.Forms.DateTimePicker();
            this.lbIdPedido = new System.Windows.Forms.Label();
            this.lbChofer = new System.Windows.Forms.Label();
            this.gpCliente = new System.Windows.Forms.GroupBox();
            this.lbCliente = new System.Windows.Forms.Label();
            this.tbCliente = new System.Windows.Forms.TextBox();
            this.lbCuilCliente = new System.Windows.Forms.Label();
            this.tbCuilCliente = new System.Windows.Forms.TextBox();
            this.lbEstado = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.cbChofer = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cbEstado = new System.Windows.Forms.ComboBox();
            this.gbAcoplado = new System.Windows.Forms.GroupBox();
            this.lbAlturaRemolque = new System.Windows.Forms.Label();
            this.tbLongitudAcoplado = new System.Windows.Forms.TextBox();
            this.tbAlturaAcoplado = new System.Windows.Forms.TextBox();
            this.lbLongitudAcoplado = new System.Windows.Forms.Label();
            this.lbAnchoIntRemolque = new System.Windows.Forms.Label();
            this.tbAnchoExtAcoplado = new System.Windows.Forms.TextBox();
            this.tbAnchoIntAcoplado = new System.Windows.Forms.TextBox();
            this.lbAnchoExtRemolque = new System.Windows.Forms.Label();
            this.gpDatosCamionRigido = new System.Windows.Forms.GroupBox();
            this.lbAltCamion = new System.Windows.Forms.Label();
            this.tbLongitudCamion = new System.Windows.Forms.TextBox();
            this.tbAltCamion = new System.Windows.Forms.TextBox();
            this.lbLongCamion = new System.Windows.Forms.Label();
            this.lbAnchoIntCamion = new System.Windows.Forms.Label();
            this.tbAnchoExtCamion = new System.Windows.Forms.TextBox();
            this.tbAnchoIntCamion = new System.Windows.Forms.TextBox();
            this.lbAnchoExtCamion = new System.Windows.Forms.Label();
            this.cbTipoCarga = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbSS_Enganche = new System.Windows.Forms.RadioButton();
            this.rbC_Enganche = new System.Windows.Forms.RadioButton();
            this.gbEngancheCamion = new System.Windows.Forms.GroupBox();
            this.rbSR_Enganche = new System.Windows.Forms.RadioButton();
            this.gpCliente.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.gbAcoplado.SuspendLayout();
            this.gpDatosCamionRigido.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.gbEngancheCamion.SuspendLayout();
            this.SuspendLayout();
            // 
            // lbObservaciones
            // 
            this.lbObservaciones.AutoSize = true;
            this.lbObservaciones.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbObservaciones.Location = new System.Drawing.Point(14, 189);
            this.lbObservaciones.Name = "lbObservaciones";
            this.lbObservaciones.Size = new System.Drawing.Size(81, 13);
            this.lbObservaciones.TabIndex = 74;
            this.lbObservaciones.Text = "Observaciones";
            // 
            // lbPrioridad
            // 
            this.lbPrioridad.AutoSize = true;
            this.lbPrioridad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPrioridad.Location = new System.Drawing.Point(15, 152);
            this.lbPrioridad.Name = "lbPrioridad";
            this.lbPrioridad.Size = new System.Drawing.Size(54, 13);
            this.lbPrioridad.TabIndex = 71;
            this.lbPrioridad.Text = "Prioridad";
            // 
            // tbObservaciones
            // 
            this.tbObservaciones.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbObservaciones.Location = new System.Drawing.Point(98, 190);
            this.tbObservaciones.Multiline = true;
            this.tbObservaciones.Name = "tbObservaciones";
            this.tbObservaciones.Size = new System.Drawing.Size(344, 65);
            this.tbObservaciones.TabIndex = 73;
            this.tbObservaciones.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCliente_KeyPress);
            // 
            // cbPrioridad
            // 
            this.cbPrioridad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbPrioridad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbPrioridad.FormattingEnabled = true;
            this.cbPrioridad.Location = new System.Drawing.Point(98, 148);
            this.cbPrioridad.Name = "cbPrioridad";
            this.cbPrioridad.Size = new System.Drawing.Size(126, 21);
            this.cbPrioridad.TabIndex = 72;
            // 
            // lbKmViaje
            // 
            this.lbKmViaje.AutoSize = true;
            this.lbKmViaje.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKmViaje.Location = new System.Drawing.Point(14, 118);
            this.lbKmViaje.Name = "lbKmViaje";
            this.lbKmViaje.Size = new System.Drawing.Size(49, 13);
            this.lbKmViaje.TabIndex = 69;
            this.lbKmViaje.Text = "KM Viaje";
            // 
            // tbKmViaje
            // 
            this.tbKmViaje.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKmViaje.Location = new System.Drawing.Point(98, 115);
            this.tbKmViaje.MaxLength = 8;
            this.tbKmViaje.Name = "tbKmViaje";
            this.tbKmViaje.ShortcutsEnabled = false;
            this.tbKmViaje.Size = new System.Drawing.Size(126, 21);
            this.tbKmViaje.TabIndex = 70;
            this.tbKmViaje.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // tbVolumen
            // 
            this.tbVolumen.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVolumen.Location = new System.Drawing.Point(309, 115);
            this.tbVolumen.MaxLength = 10;
            this.tbVolumen.Name = "tbVolumen";
            this.tbVolumen.ShortcutsEnabled = false;
            this.tbVolumen.Size = new System.Drawing.Size(134, 21);
            this.tbVolumen.TabIndex = 67;
            this.tbVolumen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbVolumen
            // 
            this.lbVolumen.AutoSize = true;
            this.lbVolumen.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVolumen.Location = new System.Drawing.Point(233, 118);
            this.lbVolumen.Name = "lbVolumen";
            this.lbVolumen.Size = new System.Drawing.Size(49, 13);
            this.lbVolumen.TabIndex = 68;
            this.lbVolumen.Text = "Volumen";
            // 
            // lbPesoNeto
            // 
            this.lbPesoNeto.AutoSize = true;
            this.lbPesoNeto.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPesoNeto.Location = new System.Drawing.Point(233, 84);
            this.lbPesoNeto.Name = "lbPesoNeto";
            this.lbPesoNeto.Size = new System.Drawing.Size(58, 13);
            this.lbPesoNeto.TabIndex = 65;
            this.lbPesoNeto.Text = "Peso Neto";
            // 
            // tbPesoNeto
            // 
            this.tbPesoNeto.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbPesoNeto.Location = new System.Drawing.Point(310, 81);
            this.tbPesoNeto.MaxLength = 10;
            this.tbPesoNeto.Name = "tbPesoNeto";
            this.tbPesoNeto.ShortcutsEnabled = false;
            this.tbPesoNeto.Size = new System.Drawing.Size(133, 21);
            this.tbPesoNeto.TabIndex = 66;
            this.tbPesoNeto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbFechaRegreso
            // 
            this.lbFechaRegreso.AutoSize = true;
            this.lbFechaRegreso.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaRegreso.Location = new System.Drawing.Point(14, 84);
            this.lbFechaRegreso.Name = "lbFechaRegreso";
            this.lbFechaRegreso.Size = new System.Drawing.Size(80, 13);
            this.lbFechaRegreso.TabIndex = 64;
            this.lbFechaRegreso.Text = "Fecha Regreso";
            // 
            // dtpRegreso
            // 
            this.dtpRegreso.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpRegreso.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpRegreso.Location = new System.Drawing.Point(98, 81);
            this.dtpRegreso.Name = "dtpRegreso";
            this.dtpRegreso.Size = new System.Drawing.Size(126, 21);
            this.dtpRegreso.TabIndex = 63;
            // 
            // lbFechaSalida
            // 
            this.lbFechaSalida.AutoSize = true;
            this.lbFechaSalida.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaSalida.Location = new System.Drawing.Point(14, 50);
            this.lbFechaSalida.Name = "lbFechaSalida";
            this.lbFechaSalida.Size = new System.Drawing.Size(69, 13);
            this.lbFechaSalida.TabIndex = 62;
            this.lbFechaSalida.Text = "Fecha Salida";
            // 
            // dtpSalida
            // 
            this.dtpSalida.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpSalida.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpSalida.Location = new System.Drawing.Point(98, 47);
            this.dtpSalida.Name = "dtpSalida";
            this.dtpSalida.Size = new System.Drawing.Size(126, 21);
            this.dtpSalida.TabIndex = 61;
            // 
            // tbNumPedido
            // 
            this.tbNumPedido.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tbNumPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNumPedido.Location = new System.Drawing.Point(98, 13);
            this.tbNumPedido.Name = "tbNumPedido";
            this.tbNumPedido.Size = new System.Drawing.Size(126, 20);
            this.tbNumPedido.TabIndex = 60;
            // 
            // lbFechaPedido
            // 
            this.lbFechaPedido.AutoSize = true;
            this.lbFechaPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaPedido.Location = new System.Drawing.Point(230, 16);
            this.lbFechaPedido.Name = "lbFechaPedido";
            this.lbFechaPedido.Size = new System.Drawing.Size(72, 13);
            this.lbFechaPedido.TabIndex = 59;
            this.lbFechaPedido.Text = "Fecha Pedido";
            // 
            // dtpPedido
            // 
            this.dtpPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpPedido.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpPedido.Location = new System.Drawing.Point(309, 13);
            this.dtpPedido.Name = "dtpPedido";
            this.dtpPedido.Size = new System.Drawing.Size(133, 21);
            this.dtpPedido.TabIndex = 58;
            // 
            // lbIdPedido
            // 
            this.lbIdPedido.AutoSize = true;
            this.lbIdPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIdPedido.Location = new System.Drawing.Point(14, 16);
            this.lbIdPedido.Name = "lbIdPedido";
            this.lbIdPedido.Size = new System.Drawing.Size(83, 13);
            this.lbIdPedido.TabIndex = 55;
            this.lbIdPedido.Text = "Numero Pedido";
            // 
            // lbChofer
            // 
            this.lbChofer.AutoSize = true;
            this.lbChofer.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChofer.Location = new System.Drawing.Point(233, 50);
            this.lbChofer.Name = "lbChofer";
            this.lbChofer.Size = new System.Drawing.Size(40, 13);
            this.lbChofer.TabIndex = 56;
            this.lbChofer.Text = "Chofer";
            // 
            // gpCliente
            // 
            this.gpCliente.Controls.Add(this.lbCliente);
            this.gpCliente.Controls.Add(this.tbCliente);
            this.gpCliente.Controls.Add(this.lbCuilCliente);
            this.gpCliente.Controls.Add(this.tbCuilCliente);
            this.gpCliente.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpCliente.Location = new System.Drawing.Point(12, 286);
            this.gpCliente.Name = "gpCliente";
            this.gpCliente.Size = new System.Drawing.Size(723, 65);
            this.gpCliente.TabIndex = 71;
            this.gpCliente.TabStop = false;
            this.gpCliente.Text = "Datos cliente";
            // 
            // lbCliente
            // 
            this.lbCliente.AutoSize = true;
            this.lbCliente.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCliente.Location = new System.Drawing.Point(6, 31);
            this.lbCliente.Name = "lbCliente";
            this.lbCliente.Size = new System.Drawing.Size(42, 13);
            this.lbCliente.TabIndex = 32;
            this.lbCliente.Text = "Cliente";
            // 
            // tbCliente
            // 
            this.tbCliente.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCliente.Location = new System.Drawing.Point(51, 28);
            this.tbCliente.Name = "tbCliente";
            this.tbCliente.ShortcutsEnabled = false;
            this.tbCliente.Size = new System.Drawing.Size(299, 21);
            this.tbCliente.TabIndex = 35;
            this.tbCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbCliente_KeyPress);
            // 
            // lbCuilCliente
            // 
            this.lbCuilCliente.AutoSize = true;
            this.lbCuilCliente.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCuilCliente.Location = new System.Drawing.Point(371, 31);
            this.lbCuilCliente.Name = "lbCuilCliente";
            this.lbCuilCliente.Size = new System.Drawing.Size(26, 13);
            this.lbCuilCliente.TabIndex = 39;
            this.lbCuilCliente.Text = "Cuil";
            // 
            // tbCuilCliente
            // 
            this.tbCuilCliente.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCuilCliente.Location = new System.Drawing.Point(401, 28);
            this.tbCuilCliente.MaxLength = 12;
            this.tbCuilCliente.Name = "tbCuilCliente";
            this.tbCuilCliente.ShortcutsEnabled = false;
            this.tbCuilCliente.Size = new System.Drawing.Size(314, 21);
            this.tbCuilCliente.TabIndex = 36;
            this.tbCuilCliente.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbEstado
            // 
            this.lbEstado.AutoSize = true;
            this.lbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstado.Location = new System.Drawing.Point(233, 152);
            this.lbEstado.Name = "lbEstado";
            this.lbEstado.Size = new System.Drawing.Size(42, 13);
            this.lbEstado.TabIndex = 40;
            this.lbEstado.Text = "Estado";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(741, 297);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(249, 46);
            this.btnGuardar.TabIndex = 67;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // cbChofer
            // 
            this.cbChofer.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbChofer.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbChofer.FormattingEnabled = true;
            this.cbChofer.Location = new System.Drawing.Point(309, 46);
            this.cbChofer.Name = "cbChofer";
            this.cbChofer.Size = new System.Drawing.Size(133, 21);
            this.cbChofer.TabIndex = 57;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.cbEstado);
            this.groupBox3.Controls.Add(this.lbObservaciones);
            this.groupBox3.Controls.Add(this.tbObservaciones);
            this.groupBox3.Controls.Add(this.lbPrioridad);
            this.groupBox3.Controls.Add(this.cbPrioridad);
            this.groupBox3.Controls.Add(this.lbKmViaje);
            this.groupBox3.Controls.Add(this.tbKmViaje);
            this.groupBox3.Controls.Add(this.tbVolumen);
            this.groupBox3.Controls.Add(this.lbVolumen);
            this.groupBox3.Controls.Add(this.lbPesoNeto);
            this.groupBox3.Controls.Add(this.lbEstado);
            this.groupBox3.Controls.Add(this.tbPesoNeto);
            this.groupBox3.Controls.Add(this.lbFechaRegreso);
            this.groupBox3.Controls.Add(this.dtpRegreso);
            this.groupBox3.Controls.Add(this.lbFechaSalida);
            this.groupBox3.Controls.Add(this.dtpSalida);
            this.groupBox3.Controls.Add(this.tbNumPedido);
            this.groupBox3.Controls.Add(this.lbFechaPedido);
            this.groupBox3.Controls.Add(this.dtpPedido);
            this.groupBox3.Controls.Add(this.lbIdPedido);
            this.groupBox3.Controls.Add(this.lbChofer);
            this.groupBox3.Controls.Add(this.cbChofer);
            this.groupBox3.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(468, 268);
            this.groupBox3.TabIndex = 70;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Datos Pedido";
            // 
            // cbEstado
            // 
            this.cbEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEstado.FormattingEnabled = true;
            this.cbEstado.Location = new System.Drawing.Point(309, 148);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(133, 21);
            this.cbEstado.TabIndex = 75;
            // 
            // gbAcoplado
            // 
            this.gbAcoplado.Controls.Add(this.lbAlturaRemolque);
            this.gbAcoplado.Controls.Add(this.tbLongitudAcoplado);
            this.gbAcoplado.Controls.Add(this.tbAlturaAcoplado);
            this.gbAcoplado.Controls.Add(this.lbLongitudAcoplado);
            this.gbAcoplado.Controls.Add(this.lbAnchoIntRemolque);
            this.gbAcoplado.Controls.Add(this.tbAnchoExtAcoplado);
            this.gbAcoplado.Controls.Add(this.tbAnchoIntAcoplado);
            this.gbAcoplado.Controls.Add(this.lbAnchoExtRemolque);
            this.gbAcoplado.Enabled = false;
            this.gbAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAcoplado.Location = new System.Drawing.Point(741, 96);
            this.gbAcoplado.Name = "gbAcoplado";
            this.gbAcoplado.Size = new System.Drawing.Size(249, 184);
            this.gbAcoplado.TabIndex = 69;
            this.gbAcoplado.TabStop = false;
            this.gbAcoplado.Text = "Datos Remolque - Semiremolque";
            // 
            // lbAlturaRemolque
            // 
            this.lbAlturaRemolque.AutoSize = true;
            this.lbAlturaRemolque.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAlturaRemolque.Location = new System.Drawing.Point(13, 28);
            this.lbAlturaRemolque.Name = "lbAlturaRemolque";
            this.lbAlturaRemolque.Size = new System.Drawing.Size(38, 13);
            this.lbAlturaRemolque.TabIndex = 76;
            this.lbAlturaRemolque.Text = "Altura";
            // 
            // tbLongitudAcoplado
            // 
            this.tbLongitudAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLongitudAcoplado.Location = new System.Drawing.Point(101, 151);
            this.tbLongitudAcoplado.MaxLength = 4;
            this.tbLongitudAcoplado.Name = "tbLongitudAcoplado";
            this.tbLongitudAcoplado.ShortcutsEnabled = false;
            this.tbLongitudAcoplado.Size = new System.Drawing.Size(142, 21);
            this.tbLongitudAcoplado.TabIndex = 81;
            this.tbLongitudAcoplado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // tbAlturaAcoplado
            // 
            this.tbAlturaAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAlturaAcoplado.Location = new System.Drawing.Point(101, 25);
            this.tbAlturaAcoplado.MaxLength = 4;
            this.tbAlturaAcoplado.Name = "tbAlturaAcoplado";
            this.tbAlturaAcoplado.ShortcutsEnabled = false;
            this.tbAlturaAcoplado.Size = new System.Drawing.Size(142, 21);
            this.tbAlturaAcoplado.TabIndex = 75;
            this.tbAlturaAcoplado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbLongitudAcoplado
            // 
            this.lbLongitudAcoplado.AutoSize = true;
            this.lbLongitudAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLongitudAcoplado.Location = new System.Drawing.Point(13, 154);
            this.lbLongitudAcoplado.Name = "lbLongitudAcoplado";
            this.lbLongitudAcoplado.Size = new System.Drawing.Size(50, 13);
            this.lbLongitudAcoplado.TabIndex = 82;
            this.lbLongitudAcoplado.Text = "Longitud";
            // 
            // lbAnchoIntRemolque
            // 
            this.lbAnchoIntRemolque.AutoSize = true;
            this.lbAnchoIntRemolque.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoIntRemolque.Location = new System.Drawing.Point(13, 70);
            this.lbAnchoIntRemolque.Name = "lbAnchoIntRemolque";
            this.lbAnchoIntRemolque.Size = new System.Drawing.Size(77, 13);
            this.lbAnchoIntRemolque.TabIndex = 78;
            this.lbAnchoIntRemolque.Text = "Ancho Interior";
            // 
            // tbAnchoExtAcoplado
            // 
            this.tbAnchoExtAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoExtAcoplado.Location = new System.Drawing.Point(101, 109);
            this.tbAnchoExtAcoplado.MaxLength = 4;
            this.tbAnchoExtAcoplado.Name = "tbAnchoExtAcoplado";
            this.tbAnchoExtAcoplado.ShortcutsEnabled = false;
            this.tbAnchoExtAcoplado.Size = new System.Drawing.Size(142, 21);
            this.tbAnchoExtAcoplado.TabIndex = 79;
            this.tbAnchoExtAcoplado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // tbAnchoIntAcoplado
            // 
            this.tbAnchoIntAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoIntAcoplado.Location = new System.Drawing.Point(101, 67);
            this.tbAnchoIntAcoplado.MaxLength = 4;
            this.tbAnchoIntAcoplado.Name = "tbAnchoIntAcoplado";
            this.tbAnchoIntAcoplado.ShortcutsEnabled = false;
            this.tbAnchoIntAcoplado.Size = new System.Drawing.Size(142, 21);
            this.tbAnchoIntAcoplado.TabIndex = 77;
            this.tbAnchoIntAcoplado.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbAnchoExtRemolque
            // 
            this.lbAnchoExtRemolque.AutoSize = true;
            this.lbAnchoExtRemolque.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoExtRemolque.Location = new System.Drawing.Point(13, 112);
            this.lbAnchoExtRemolque.Name = "lbAnchoExtRemolque";
            this.lbAnchoExtRemolque.Size = new System.Drawing.Size(81, 13);
            this.lbAnchoExtRemolque.TabIndex = 80;
            this.lbAnchoExtRemolque.Text = "Ancho Exterior";
            // 
            // gpDatosCamionRigido
            // 
            this.gpDatosCamionRigido.Controls.Add(this.lbAltCamion);
            this.gpDatosCamionRigido.Controls.Add(this.tbLongitudCamion);
            this.gpDatosCamionRigido.Controls.Add(this.tbAltCamion);
            this.gpDatosCamionRigido.Controls.Add(this.lbLongCamion);
            this.gpDatosCamionRigido.Controls.Add(this.lbAnchoIntCamion);
            this.gpDatosCamionRigido.Controls.Add(this.tbAnchoExtCamion);
            this.gpDatosCamionRigido.Controls.Add(this.tbAnchoIntCamion);
            this.gpDatosCamionRigido.Controls.Add(this.lbAnchoExtCamion);
            this.gpDatosCamionRigido.Enabled = false;
            this.gpDatosCamionRigido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpDatosCamionRigido.Location = new System.Drawing.Point(486, 96);
            this.gpDatosCamionRigido.Name = "gpDatosCamionRigido";
            this.gpDatosCamionRigido.Size = new System.Drawing.Size(249, 184);
            this.gpDatosCamionRigido.TabIndex = 87;
            this.gpDatosCamionRigido.TabStop = false;
            this.gpDatosCamionRigido.Text = "Datos camion rigido";
            // 
            // lbAltCamion
            // 
            this.lbAltCamion.AutoSize = true;
            this.lbAltCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAltCamion.Location = new System.Drawing.Point(13, 28);
            this.lbAltCamion.Name = "lbAltCamion";
            this.lbAltCamion.Size = new System.Drawing.Size(38, 13);
            this.lbAltCamion.TabIndex = 56;
            this.lbAltCamion.Text = "Altura";
            // 
            // tbLongitudCamion
            // 
            this.tbLongitudCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLongitudCamion.Location = new System.Drawing.Point(99, 151);
            this.tbLongitudCamion.MaxLength = 4;
            this.tbLongitudCamion.Name = "tbLongitudCamion";
            this.tbLongitudCamion.ShortcutsEnabled = false;
            this.tbLongitudCamion.Size = new System.Drawing.Size(142, 21);
            this.tbLongitudCamion.TabIndex = 61;
            this.tbLongitudCamion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // tbAltCamion
            // 
            this.tbAltCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAltCamion.Location = new System.Drawing.Point(101, 25);
            this.tbAltCamion.MaxLength = 4;
            this.tbAltCamion.Name = "tbAltCamion";
            this.tbAltCamion.ShortcutsEnabled = false;
            this.tbAltCamion.Size = new System.Drawing.Size(142, 21);
            this.tbAltCamion.TabIndex = 55;
            this.tbAltCamion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbLongCamion
            // 
            this.lbLongCamion.AutoSize = true;
            this.lbLongCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLongCamion.Location = new System.Drawing.Point(13, 154);
            this.lbLongCamion.Name = "lbLongCamion";
            this.lbLongCamion.Size = new System.Drawing.Size(50, 13);
            this.lbLongCamion.TabIndex = 62;
            this.lbLongCamion.Text = "Longitud";
            // 
            // lbAnchoIntCamion
            // 
            this.lbAnchoIntCamion.AutoSize = true;
            this.lbAnchoIntCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoIntCamion.Location = new System.Drawing.Point(13, 70);
            this.lbAnchoIntCamion.Name = "lbAnchoIntCamion";
            this.lbAnchoIntCamion.Size = new System.Drawing.Size(77, 13);
            this.lbAnchoIntCamion.TabIndex = 58;
            this.lbAnchoIntCamion.Text = "Ancho Interior";
            // 
            // tbAnchoExtCamion
            // 
            this.tbAnchoExtCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoExtCamion.Location = new System.Drawing.Point(100, 109);
            this.tbAnchoExtCamion.MaxLength = 4;
            this.tbAnchoExtCamion.Name = "tbAnchoExtCamion";
            this.tbAnchoExtCamion.ShortcutsEnabled = false;
            this.tbAnchoExtCamion.Size = new System.Drawing.Size(141, 21);
            this.tbAnchoExtCamion.TabIndex = 59;
            this.tbAnchoExtCamion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // tbAnchoIntCamion
            // 
            this.tbAnchoIntCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoIntCamion.Location = new System.Drawing.Point(100, 67);
            this.tbAnchoIntCamion.MaxLength = 4;
            this.tbAnchoIntCamion.Name = "tbAnchoIntCamion";
            this.tbAnchoIntCamion.ShortcutsEnabled = false;
            this.tbAnchoIntCamion.Size = new System.Drawing.Size(142, 21);
            this.tbAnchoIntCamion.TabIndex = 57;
            this.tbAnchoIntCamion.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbPesoNeto_KeyPress);
            // 
            // lbAnchoExtCamion
            // 
            this.lbAnchoExtCamion.AutoSize = true;
            this.lbAnchoExtCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoExtCamion.Location = new System.Drawing.Point(13, 112);
            this.lbAnchoExtCamion.Name = "lbAnchoExtCamion";
            this.lbAnchoExtCamion.Size = new System.Drawing.Size(81, 13);
            this.lbAnchoExtCamion.TabIndex = 60;
            this.lbAnchoExtCamion.Text = "Ancho Exterior";
            // 
            // cbTipoCarga
            // 
            this.cbTipoCarga.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoCarga.FormattingEnabled = true;
            this.cbTipoCarga.Location = new System.Drawing.Point(5, 34);
            this.cbTipoCarga.Name = "cbTipoCarga";
            this.cbTipoCarga.Size = new System.Drawing.Size(237, 21);
            this.cbTipoCarga.TabIndex = 96;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cbTipoCarga);
            this.groupBox1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(486, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(249, 83);
            this.groupBox1.TabIndex = 89;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Seleccione Tipo Carga Viaje";
            // 
            // rbSS_Enganche
            // 
            this.rbSS_Enganche.AutoSize = true;
            this.rbSS_Enganche.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSS_Enganche.Location = new System.Drawing.Point(6, 58);
            this.rbSS_Enganche.Name = "rbSS_Enganche";
            this.rbSS_Enganche.Size = new System.Drawing.Size(165, 17);
            this.rbSS_Enganche.TabIndex = 77;
            this.rbSS_Enganche.TabStop = true;
            this.rbSS_Enganche.Text = "Sin Enganche Semiremolque";
            this.rbSS_Enganche.UseVisualStyleBackColor = true;
            this.rbSS_Enganche.CheckedChanged += new System.EventHandler(this.rbC_Enganche_CheckedChanged);
            // 
            // rbC_Enganche
            // 
            this.rbC_Enganche.AutoSize = true;
            this.rbC_Enganche.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbC_Enganche.Location = new System.Drawing.Point(6, 12);
            this.rbC_Enganche.Name = "rbC_Enganche";
            this.rbC_Enganche.Size = new System.Drawing.Size(145, 17);
            this.rbC_Enganche.TabIndex = 0;
            this.rbC_Enganche.TabStop = true;
            this.rbC_Enganche.Text = "Con Enganche Remolque";
            this.rbC_Enganche.UseVisualStyleBackColor = true;
            this.rbC_Enganche.CheckedChanged += new System.EventHandler(this.rbC_Enganche_CheckedChanged);
            // 
            // gbEngancheCamion
            // 
            this.gbEngancheCamion.Controls.Add(this.rbSS_Enganche);
            this.gbEngancheCamion.Controls.Add(this.rbSR_Enganche);
            this.gbEngancheCamion.Controls.Add(this.rbC_Enganche);
            this.gbEngancheCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEngancheCamion.Location = new System.Drawing.Point(741, 12);
            this.gbEngancheCamion.Name = "gbEngancheCamion";
            this.gbEngancheCamion.Size = new System.Drawing.Size(249, 83);
            this.gbEngancheCamion.TabIndex = 88;
            this.gbEngancheCamion.TabStop = false;
            this.gbEngancheCamion.Text = "Seleccione tipo Enganche";
            // 
            // rbSR_Enganche
            // 
            this.rbSR_Enganche.AutoSize = true;
            this.rbSR_Enganche.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSR_Enganche.Location = new System.Drawing.Point(6, 35);
            this.rbSR_Enganche.Name = "rbSR_Enganche";
            this.rbSR_Enganche.Size = new System.Drawing.Size(124, 17);
            this.rbSR_Enganche.TabIndex = 78;
            this.rbSR_Enganche.TabStop = true;
            this.rbSR_Enganche.Text = "Sin Enganche Rigido";
            this.rbSR_Enganche.UseVisualStyleBackColor = true;
            this.rbSR_Enganche.CheckedChanged += new System.EventHandler(this.rbC_Enganche_CheckedChanged);
            // 
            // frmPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(999, 355);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.gbEngancheCamion);
            this.Controls.Add(this.gpDatosCamionRigido);
            this.Controls.Add(this.gbAcoplado);
            this.Controls.Add(this.gpCliente);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.groupBox3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPedido";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alta Pedido - Operaciones";
            this.gpCliente.ResumeLayout(false);
            this.gpCliente.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.gbAcoplado.ResumeLayout(false);
            this.gbAcoplado.PerformLayout();
            this.gpDatosCamionRigido.ResumeLayout(false);
            this.gpDatosCamionRigido.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.gbEngancheCamion.ResumeLayout(false);
            this.gbEngancheCamion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lbObservaciones;
        private System.Windows.Forms.Label lbPrioridad;
        private System.Windows.Forms.TextBox tbObservaciones;
        private System.Windows.Forms.ComboBox cbPrioridad;
        private System.Windows.Forms.Label lbKmViaje;
        private System.Windows.Forms.TextBox tbKmViaje;
        private System.Windows.Forms.TextBox tbVolumen;
        private System.Windows.Forms.Label lbVolumen;
        private System.Windows.Forms.Label lbPesoNeto;
        private System.Windows.Forms.TextBox tbPesoNeto;
        private System.Windows.Forms.Label lbFechaRegreso;
        private System.Windows.Forms.DateTimePicker dtpRegreso;
        private System.Windows.Forms.Label lbFechaSalida;
        private System.Windows.Forms.DateTimePicker dtpSalida;
        private System.Windows.Forms.Label tbNumPedido;
        private System.Windows.Forms.Label lbFechaPedido;
        private System.Windows.Forms.DateTimePicker dtpPedido;
        private System.Windows.Forms.Label lbIdPedido;
        private System.Windows.Forms.Label lbChofer;
        private System.Windows.Forms.GroupBox gpCliente;
        private System.Windows.Forms.Label lbCliente;
        private System.Windows.Forms.TextBox tbCliente;
        private System.Windows.Forms.Label lbCuilCliente;
        private System.Windows.Forms.TextBox tbCuilCliente;
        private System.Windows.Forms.Label lbEstado;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.ComboBox cbChofer;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox gbAcoplado;
        private System.Windows.Forms.Label lbAlturaRemolque;
        private System.Windows.Forms.TextBox tbLongitudAcoplado;
        private System.Windows.Forms.TextBox tbAlturaAcoplado;
        private System.Windows.Forms.Label lbLongitudAcoplado;
        private System.Windows.Forms.Label lbAnchoIntRemolque;
        private System.Windows.Forms.TextBox tbAnchoExtAcoplado;
        private System.Windows.Forms.TextBox tbAnchoIntAcoplado;
        private System.Windows.Forms.Label lbAnchoExtRemolque;
        private System.Windows.Forms.GroupBox gpDatosCamionRigido;
        private System.Windows.Forms.Label lbAltCamion;
        private System.Windows.Forms.TextBox tbLongitudCamion;
        private System.Windows.Forms.TextBox tbAltCamion;
        private System.Windows.Forms.Label lbLongCamion;
        private System.Windows.Forms.Label lbAnchoIntCamion;
        private System.Windows.Forms.TextBox tbAnchoExtCamion;
        private System.Windows.Forms.TextBox tbAnchoIntCamion;
        private System.Windows.Forms.Label lbAnchoExtCamion;
        private System.Windows.Forms.ComboBox cbEstado;
        private System.Windows.Forms.ComboBox cbTipoCarga;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbSS_Enganche;
        private System.Windows.Forms.RadioButton rbC_Enganche;
        private System.Windows.Forms.GroupBox gbEngancheCamion;
        private System.Windows.Forms.RadioButton rbSR_Enganche;
    }
}