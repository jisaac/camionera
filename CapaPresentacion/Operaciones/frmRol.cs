﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    public partial class frmRol : Form
    {
        E_Rol e_rol;
        N_Rol n_rol;
        string usuario;
        MisFunciones funcion;
        actualizaGrillaRoles actualizaRoles;
        string aux;
        public frmRol(string usuario, E_Rol e_rol, Delegate actualizaRoles)
        {
            InitializeComponent();
            this.usuario = usuario;
            funcion = new MisFunciones();
            this.e_rol = e_rol;
            this.actualizaRoles = (actualizaGrillaRoles)actualizaRoles;
            if (e_rol != null)
                cargaRolModifica(e_rol);
            else
            {
                n_rol = new N_Rol(usuario);
                cbEstado.Checked = true;
                cbEstado.Enabled = false;
            }
        }
        private void cargaRolModifica(E_Rol e_rol)
        {
            aux = e_rol.rol;
            tbRol.Text = e_rol.rol;
            if (e_rol.estado == "HABILITADO")
                cbEstado.Checked = true;
            else
                cbEstado.Checked = false;
        }
        private void btnGuardarRol_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbRol.Text))
                new frmMensajes("Error", "No es posible realizar la operación sin antes completar todos los campos").ShowDialog();
            else
            {
                if (e_rol != null)
                {
                    try
                    {
                        n_rol = new N_Rol(usuario);
                        e_rol.rol = tbRol.Text.ToUpper();
                        if (cbEstado.Checked)
                            e_rol.estado = "HABILITADO";
                        else
                            e_rol.estado = "DESHABILITADO";
                        List<string> resp = n_rol.modificar(e_rol, aux);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        new frmMensajes("Error", ex.Message).ShowDialog();
                    }
                }
                else
                {
                    try
                    {
                        e_rol = new E_Rol();
                        e_rol.rol = tbRol.Text.ToUpper();
                        List<string> resp = n_rol.insertar(e_rol);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        new frmMensajes("Error", ex.Message).ShowDialog();
                    }
                }
                this.actualizaRoles();
                tbRol.Clear();
            }
            
        }

        private void tbRol_KeyPress(object sender, KeyPressEventArgs e)
        {
            funcion.solo_letras(sender, e);
        }
    }
}
