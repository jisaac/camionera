﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    public partial class frmNuevoCamion : Form
    {//la variable ver sirve para que "ver unidad",si ver es true entonces no es modificar,es "ver unidad" 
        public frmNuevoCamion(string usuario,object camion,Boolean ver)
        {
            InitializeComponent();
            this.usuario = usuario;
            n_marca = new N_MarcaCamion(usuario);
            n_modelo = new N_ModeloCamion(usuario);
            n_camion = new N_Camion(usuario);
            n_tipo_carga = new N_TipoCarga(usuario);
            n_dominio = new N_Dominio(usuario);
            this.camion = camion;
            this.ver = ver;
        }
        object camion;////SI ESTE ES DISTINTO DE NULL ENTONCES LA ACCION SERA MODIFICAR,YA QUE ES ENVIA POR PARAMETRO EL CAMION A MODIFICAR
        string usuario;
        string dominio_viejo;//Se almacena el dominio viejo por si se modifica
        Boolean ver;
        MisFunciones funciones = new MisFunciones();
        E_MarcaCamion e_marca;
        E_ModeloCamion e_modelo;
        N_MarcaCamion n_marca;
        N_ModeloCamion n_modelo;
        E_Tractocamion tracto;
        E_Rigido rigido;
        N_Camion n_camion;
        E_TipoCarga e_tipo_carga;
        N_TipoCarga n_tipo_carga;
        N_Dominio n_dominio;



        private void frmNuevoCamion_Load(object sender, EventArgs e)
        {
            cbModelo.DropDownStyle = ComboBoxStyle.DropDownList;//hace que el combobox sea solo de lectura
            cbMarca.DropDownStyle = ComboBoxStyle.DropDownList;//hace que el combobox sea solo de lectura
            cbTipoCarga.DropDownStyle = ComboBoxStyle.DropDownList;//hace que el combobox sea solo de lectura
            cbMarca.DataSource = n_marca.listarComboBox();
            cbModelo.DataSource = n_modelo.listarComboBox(cbMarca.Text);
            cbTipoCarga.DataSource = n_tipo_carga.listarComboBox();
            rbTracto.Checked = true;
            gbCamionRigido.Enabled = false;//el groupbox de rigido se cargara enabled false
            if (camion!=null)
            {

                if( camion is E_Rigido)
                {
                    #region manejo de checkbox groupbox para rigido 
                    gbCamionRigido.Enabled = true;
                    rbRigido.Checked = true;
                    rbTracto.Visible = false;
                    #endregion/////////
                    rigido = (E_Rigido)camion;
                    this.dominio_viejo = n_dominio.Retorna_Dominio(rigido.Id_Dominio);
                    #region Manejo de combobox
                    e_marca = n_marca.retornaMarcaCamion(rigido.Marca);
                    cbMarca.SelectedIndex = cbMarca.Items.IndexOf (e_marca.Marca);
                    cbModelo.DataSource = n_modelo.listarComboBox(cbMarca.Text);
                    e_modelo = n_modelo.retornaModeloCamion(rigido.Modelo);
                    cbModelo.SelectedIndex = cbModelo.Items.IndexOf(e_modelo.Modelo);
                    e_tipo_carga = n_tipo_carga.retornaTipoCarga(rigido.Tipo_carga);
                    cbTipoCarga.SelectedIndex = cbTipoCarga.Items.IndexOf(e_tipo_carga.Tipo);
                    #endregion//////////
                    tbDominio.Text = n_dominio.Retorna_Dominio(rigido.Id_Dominio);
                    tbAño.Text =rigido.Año.ToString() ;
                    tbTara.Text= rigido.Tara.ToString() ;
                    tbKmUnidad.Text=rigido.Km_unidad.ToString();
                    tbKmCambioNeumaticos.Text = rigido.Km_cambio_neumaticos.ToString();
                    tbService.Text = rigido.Km_service.ToString();
                    rigido.Fecha_alta = DateTime.Now;
                    tbAltura.Text= rigido.Altura.ToString();
                    tbAnchoInterior.Text=rigido.Ancho_interior.ToString();
                    tbAnchoExterior.Text= rigido.Ancho_exterior.ToString();
                    tbLongitud.Text =  rigido.Longitud.ToString();
                    tbVolumen.Text = rigido.Volumen.ToString();
                    tbCapacidad.Text = rigido.Capacidad_carga.ToString();
                    if (rigido.Enganche == true)
                    {
                        chEnganche.Checked=true;
                        
                    }
                    else
                    {
                        chEnganche.Checked = false;
                    }
                }
                else
                {
                    #region manejo de checkbox groupbox para rigido 
                    gbCamionRigido.Enabled = false;
                    rbTracto.Checked = true;
                    rbRigido.Visible = false;
#endregion
                    tracto = (E_Tractocamion)camion;
                    this.dominio_viejo = n_dominio.Retorna_Dominio(tracto.Id_Dominio);
                    #region Manejo de combobox
                    e_marca = n_marca.retornaMarcaCamion(tracto.Marca);
                    cbMarca.SelectedIndex = cbMarca.Items.IndexOf(e_marca.Marca);
                    cbModelo.DataSource = n_modelo.listarComboBox(cbMarca.Text);
                    e_modelo = n_modelo.retornaModeloCamion(tracto.Modelo);
                    cbModelo.SelectedIndex = cbModelo.Items.IndexOf(e_modelo.Modelo);
                    #endregion
                    tbDominio.Text = n_dominio.Retorna_Dominio(tracto.Id_Dominio);
                    tbAño.Text = tracto.Año.ToString();
                    tbTara.Text = tracto.Tara.ToString();
                    tbKmUnidad.Text = tracto.Km_unidad.ToString();
                    tbKmCambioNeumaticos.Text = tracto.Km_cambio_neumaticos.ToString();
                    tbService.Text = tracto.Km_service.ToString();

                    tracto.Fecha_alta = DateTime.Now;
                }
                if (ver)
                {
                    funciones.controlReadOnly(this);  //mostrar boton ok si se utiliza este form para mmostrar una unidad
                    btnOk.Text = "Ok";
                    btnSalir.Enabled = false;
                    btnSalir.Visible = false;
                    
                }
            }
          
        }
      
        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!ver)
            {
                if (string.IsNullOrWhiteSpace(tbDominio.Text) || string.IsNullOrWhiteSpace(tbAño.Text) || string.IsNullOrWhiteSpace(tbTara.Text) || string.IsNullOrWhiteSpace(tbKmUnidad.Text) || 
                    string.IsNullOrWhiteSpace(tbKmCambioNeumaticos.Text) || string.IsNullOrWhiteSpace(tbService.Text))
                    new frmMensajes("Error", "No es posible realizar la operación sin antes completar todos los campos").ShowDialog();
                else
                {
                    string dominio_ok = funciones.valida_dominio(tbDominio.Text);
                    if (dominio_ok == "CORRECTO")
                    {
                        List<string> resp = new List<string>();
                        int year = DateTime.Now.Year;
                        int diferencia = year - 50;
                        if (tbAño.TextLength == 4 && Convert.ToInt32(tbAño.Text) <= year && Convert.ToInt32(tbAño.Text) > diferencia)
                        {
                            if (camion == null)//ME FIJO SI LA ACCION ES INSERTAR UN CAMION NUEVO (CAMION==NULL) O SI ES MODIFICAR UN CAMION
                            {
                                if (sender == btnOk)
                                {
                                    if (rbTracto.Checked)
                                    {
                                        try
                                        {
                                            tracto = new E_Tractocamion();
                                            tracto.Marca = int.Parse(n_marca.retornaId(cbMarca.Text));
                                            tracto.Modelo = int.Parse(n_modelo.retornaId(cbModelo.Text));
                                            tracto.Año = int.Parse(tbAño.Text);
                                            tracto.Tara = int.Parse(tbTara.Text);
                                            tracto.Km_unidad = float.Parse(tbKmUnidad.Text);
                                            tracto.Km_cambio_neumaticos = float.Parse(tbKmCambioNeumaticos.Text);
                                            tracto.Km_service = float.Parse(tbService.Text);
                                            tracto.Km_service_suma = float.Parse(tbService.Text);
                                            tracto.Km_cambio_neumaticos_suma = float.Parse(tbKmCambioNeumaticos.Text);
                                            tracto.Fecha_alta = DateTime.Now;
                                            resp = n_camion.insertar(tracto, tbDominio.Text);
                                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                                            this.Hide();
                                        }
                                        catch (Exception ex)
                                        {
                                            new frmMensajes("Error", ex.Message).ShowDialog();
                                        }
                                    }
                                    else
                                    {
                                        try
                                        {
                                            rigido = new E_Rigido();
                                            rigido.Marca = int.Parse(n_marca.retornaId(cbMarca.Text));
                                            rigido.Modelo = int.Parse(n_modelo.retornaId(cbModelo.Text));
                                            rigido.Año = int.Parse(tbAño.Text);
                                            rigido.Tara = int.Parse(tbTara.Text);
                                            rigido.Km_unidad = float.Parse(tbKmUnidad.Text);
                                            rigido.Km_cambio_neumaticos = float.Parse(tbKmCambioNeumaticos.Text);
                                            rigido.Km_service = float.Parse(tbService.Text);
                                            rigido.Fecha_alta = DateTime.Now;
                                            rigido.Tipo_carga = n_tipo_carga.retornaId(cbTipoCarga.Text);
                                            rigido.Altura = float.Parse(tbAltura.Text);
                                            rigido.Ancho_interior = float.Parse(tbAnchoInterior.Text);
                                            rigido.Ancho_exterior = float.Parse(tbAnchoExterior.Text);
                                            rigido.Longitud = float.Parse(tbLongitud.Text);
                                            rigido.Volumen = float.Parse(tbVolumen.Text);
                                            rigido.Capacidad_carga = float.Parse(tbCapacidad.Text);
                                            if (chEnganche.Checked)
                                            {
                                                rigido.Enganche = true;
                                            }
                                            else
                                            {
                                                rigido.Enganche = false;
                                            }
                                            resp = n_camion.insertar(rigido, tbDominio.Text);
                                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                                            this.Hide();
                                        }
                                        catch (Exception ex)
                                        {
                                            new frmMensajes("Error", ex.Message).ShowDialog();
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (camion is E_Rigido)
                                {
                                    try
                                    {
                                        rigido.Marca = int.Parse(n_marca.retornaId(cbMarca.Text));
                                        rigido.Modelo = int.Parse(n_modelo.retornaId(cbModelo.Text));
                                        rigido.Año = int.Parse(tbAño.Text);
                                        rigido.Tara = int.Parse(tbTara.Text);
                                        rigido.Km_unidad = float.Parse(tbKmUnidad.Text);
                                        rigido.Km_cambio_neumaticos = float.Parse(tbKmCambioNeumaticos.Text);
                                        rigido.Km_service = float.Parse(tbService.Text);
                                        rigido.Fecha_alta = DateTime.Now;
                                        rigido.Tipo_carga = n_tipo_carga.retornaId(cbTipoCarga.Text);
                                        rigido.Altura = float.Parse(tbAltura.Text);
                                        rigido.Ancho_interior = float.Parse(tbAnchoInterior.Text);
                                        rigido.Ancho_exterior = float.Parse(tbAnchoExterior.Text);
                                        rigido.Longitud = float.Parse(tbLongitud.Text);
                                        rigido.Capacidad_carga = float.Parse(tbCapacidad.Text);
                                        if (chEnganche.Checked)
                                        {
                                            rigido.Enganche = true;
                                        }
                                        else
                                        {
                                            rigido.Enganche = false;
                                        }
                                        resp = n_camion.modificar(rigido, tbDominio.Text, "modificacion de datos por administrador");
                                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                                        this.Hide();
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }

                                }
                                else
                                {
                                    try
                                    {
                                        tracto.Marca = int.Parse(n_marca.retornaId(cbMarca.Text));
                                        tracto.Modelo = int.Parse(n_modelo.retornaId(cbModelo.Text));
                                        tracto.Año = int.Parse(tbAño.Text);
                                        tracto.Tara = int.Parse(tbTara.Text);
                                        tracto.Km_unidad = float.Parse(tbKmUnidad.Text);
                                        tracto.Km_cambio_neumaticos = float.Parse(tbKmCambioNeumaticos.Text);
                                        tracto.Km_service = float.Parse(tbService.Text);
                                        tracto.Fecha_alta = DateTime.Now;
                                        resp = n_camion.modificar(tracto, tbDominio.Text, "modificacion de datos por administrador");
                                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                                        this.Hide();
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }
                                }
                            }
                        }
                        else
                            new frmMensajes("Error", "Año ingresado invalido, recuerdo que debe ser mayor a "+diferencia+" y menor o igual que "+year+" ").ShowDialog();
                    }
                    else
                        new frmMensajes("Error", "Formato de dominio incorrecto, recuerde que el formato es AA999AA ó AAA999").ShowDialog();
                }
            }
            else
            {
                this.Close();
            }
      
            
        }
        //evento que controla el cambio del COMBOBOX    MARCA
        private void cbMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
           cbModelo.DataSource= n_modelo.listarComboBox(cbMarca.Text);
        }

        private void rbTracto_CheckedChanged(object sender, EventArgs e)
        {
            if (rbRigido.Checked)
            {
                gbCamionRigido.Enabled = true;
            }
            else
            {
                gbCamionRigido.Enabled = false;
            }
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbAño_KeyPress(object sender, KeyPressEventArgs e)
        {
            funciones.solo_numero(sender, e);
        }


    }
}
