﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    public partial class frmUsuario : Form
    {
        E_Usuario e_usuario;
        N_Usuario n_usuario;
        N_Rol n_rol;
        actualizaGrillaUsuarios actualizaUsuarios;
        string usuario;
        MisFunciones funcion;
        public frmUsuario(string usuario,E_Usuario e_usuario, Delegate actualizaUsuarios)
        {
            InitializeComponent();
            n_rol = new N_Rol(usuario);
            this.e_usuario = e_usuario;
            this.usuario = usuario;
            this.actualizaUsuarios = (actualizaGrillaUsuarios)actualizaUsuarios;
            this.cargaComboRoles();
            funcion = new MisFunciones();
            n_usuario = new N_Usuario(usuario);
            if (e_usuario != null)
                cargaUsuarioModifica(e_usuario);
            else
            {
                cbEstado.Checked = true;
                cbEstado.Enabled = false;
            }
        }
        private void cargaUsuarioModifica(E_Usuario e_usuario)
        {
            tbNumLegajo.ReadOnly = true;
            tbUsuario.ReadOnly = true;
            tbUsuario.Text = e_usuario.usuario;
            tbClave.Text = e_usuario.clave;
            tbNumLegajo.Text = e_usuario.num_legajo.ToString();
            cbRol.SelectedIndex = e_usuario.id_rol;
            if (e_usuario.estado == "HABILITADO")
                cbEstado.Checked = true;
            else
                cbEstado.Checked = false;
        }
        private void cargaComboRoles()
        {
            cbRol.Items.Add("SELECCIONE ROL");
            cbRol.SelectedIndex = 0;
            foreach (DataRow fila in n_rol.listar_roles().Rows)
            {
                cbRol.Items.Add(fila["rol"].ToString());
            }
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbUsuario.Text) || string.IsNullOrWhiteSpace(tbClave.Text) || string.IsNullOrWhiteSpace(tbNumLegajo.Text))
                new frmMensajes("Error", "No es posible realizar la operación sin antes completar todos los campos").ShowDialog();
            else
            {
                int idRol = n_rol.retornaId(cbRol.SelectedItem.ToString().ToUpper().Trim());
                if (idRol != -1)
                {
                    if (e_usuario != null)
                    {
                        try
                        {
                            e_usuario.clave = tbClave.Text.ToUpper();
                            e_usuario.id_rol = idRol;
                            e_usuario.num_legajo = Convert.ToInt32(tbNumLegajo.Text);
                            if (cbEstado.Checked)
                                e_usuario.estado = "HABILITADO";
                            else
                                e_usuario.estado = "DESHABILITADO";
                            List<string> resp = n_usuario.modificar(e_usuario);
                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            new frmMensajes("Error", ex.Message).ShowDialog();
                        }
                    }
                    else
                    {
                        try
                        {
                            e_usuario = new E_Usuario();
                            e_usuario.usuario = tbUsuario.Text.ToUpper();
                            e_usuario.clave = tbClave.Text.ToUpper();
                            e_usuario.num_legajo = Convert.ToInt32(tbNumLegajo.Text);
                            e_usuario.id_rol = idRol;
                            List<string> resp = n_usuario.insertar(e_usuario);
                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            new frmMensajes("Error", ex.Message).ShowDialog();
                        }
                    }
                    this.actualizaUsuarios();
                }
                else
                    MessageBox.Show("Por favor, seleccione rol");
                tbUsuario.Clear();
                tbClave.Clear();
                tbNumLegajo.Clear();
                cbRol.SelectedIndex = 0;
            }
            
        }

        private void tbNumLegajo_KeyPress(object sender, KeyPressEventArgs e)
        {
            funcion.solo_numero(sender, e);
        }
    }
}
