﻿namespace CapaPresentacion.Operaciones
{
    partial class frmNuevoCamion
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.rbRigido = new System.Windows.Forms.RadioButton();
            this.rbTracto = new System.Windows.Forms.RadioButton();
            this.lblDominio = new System.Windows.Forms.Label();
            this.tbDominio = new System.Windows.Forms.TextBox();
            this.tbAño = new System.Windows.Forms.TextBox();
            this.lblAño = new System.Windows.Forms.Label();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblMarca = new System.Windows.Forms.Label();
            this.tbKmCambioNeumaticos = new System.Windows.Forms.TextBox();
            this.lblUsoNeumaticos = new System.Windows.Forms.Label();
            this.tbKmUnidad = new System.Windows.Forms.TextBox();
            this.lblKmUnidad = new System.Windows.Forms.Label();
            this.tbTara = new System.Windows.Forms.TextBox();
            this.lblTara = new System.Windows.Forms.Label();
            this.cbMarca = new System.Windows.Forms.ComboBox();
            this.gbCamionRigido = new System.Windows.Forms.GroupBox();
            this.tbVolumen = new System.Windows.Forms.TextBox();
            this.lblVolumen = new System.Windows.Forms.Label();
            this.chEnganche = new System.Windows.Forms.CheckBox();
            this.cbTipoCarga = new System.Windows.Forms.ComboBox();
            this.tbAltura = new System.Windows.Forms.TextBox();
            this.lblTipoCarga = new System.Windows.Forms.Label();
            this.lblAltura = new System.Windows.Forms.Label();
            this.tbCapacidad = new System.Windows.Forms.TextBox();
            this.lblAnchoInterior = new System.Windows.Forms.Label();
            this.lblCapacidad = new System.Windows.Forms.Label();
            this.tbAnchoInterior = new System.Windows.Forms.TextBox();
            this.tbLongitud = new System.Windows.Forms.TextBox();
            this.lblAnchoExterior = new System.Windows.Forms.Label();
            this.lblLongitud = new System.Windows.Forms.Label();
            this.tbAnchoExterior = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.cbModelo = new System.Windows.Forms.ComboBox();
            this.btnSalir = new System.Windows.Forms.Button();
            this.tbService = new System.Windows.Forms.TextBox();
            this.lblKmService = new System.Windows.Forms.Label();
            this.gbCamionRigido.SuspendLayout();
            this.SuspendLayout();
            // 
            // rbRigido
            // 
            this.rbRigido.AutoSize = true;
            this.rbRigido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbRigido.Location = new System.Drawing.Point(478, 6);
            this.rbRigido.Name = "rbRigido";
            this.rbRigido.Size = new System.Drawing.Size(102, 17);
            this.rbRigido.TabIndex = 12;
            this.rbRigido.Text = "Camion - Rigido";
            this.rbRigido.UseVisualStyleBackColor = true;
            this.rbRigido.CheckedChanged += new System.EventHandler(this.rbTracto_CheckedChanged);
            // 
            // rbTracto
            // 
            this.rbTracto.AutoSize = true;
            this.rbTracto.Checked = true;
            this.rbTracto.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbTracto.Location = new System.Drawing.Point(329, 5);
            this.rbTracto.Name = "rbTracto";
            this.rbTracto.Size = new System.Drawing.Size(137, 17);
            this.rbTracto.TabIndex = 11;
            this.rbTracto.TabStop = true;
            this.rbTracto.Text = "Camion - Tractocamion";
            this.rbTracto.UseVisualStyleBackColor = true;
            this.rbTracto.CheckedChanged += new System.EventHandler(this.rbTracto_CheckedChanged);
            // 
            // lblDominio
            // 
            this.lblDominio.AutoSize = true;
            this.lblDominio.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDominio.Location = new System.Drawing.Point(12, 9);
            this.lblDominio.Name = "lblDominio";
            this.lblDominio.Size = new System.Drawing.Size(49, 13);
            this.lblDominio.TabIndex = 4;
            this.lblDominio.Text = "Dominio:";
            // 
            // tbDominio
            // 
            this.tbDominio.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDominio.Location = new System.Drawing.Point(140, 6);
            this.tbDominio.Name = "tbDominio";
            this.tbDominio.ShortcutsEnabled = false;
            this.tbDominio.Size = new System.Drawing.Size(156, 21);
            this.tbDominio.TabIndex = 1;
            // 
            // tbAño
            // 
            this.tbAño.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAño.Location = new System.Drawing.Point(140, 107);
            this.tbAño.MaxLength = 4;
            this.tbAño.Name = "tbAño";
            this.tbAño.ShortcutsEnabled = false;
            this.tbAño.Size = new System.Drawing.Size(156, 21);
            this.tbAño.TabIndex = 6;
            this.tbAño.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAño
            // 
            this.lblAño.AutoSize = true;
            this.lblAño.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAño.Location = new System.Drawing.Point(15, 109);
            this.lblAño.Name = "lblAño";
            this.lblAño.Size = new System.Drawing.Size(28, 13);
            this.lblAño.TabIndex = 24;
            this.lblAño.Text = "Año:";
            // 
            // lblModelo
            // 
            this.lblModelo.AutoSize = true;
            this.lblModelo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModelo.Location = new System.Drawing.Point(15, 76);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(45, 13);
            this.lblModelo.TabIndex = 22;
            this.lblModelo.Text = "Modelo:";
            // 
            // lblMarca
            // 
            this.lblMarca.AutoSize = true;
            this.lblMarca.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarca.Location = new System.Drawing.Point(12, 42);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(40, 13);
            this.lblMarca.TabIndex = 20;
            this.lblMarca.Text = "Marca:";
            // 
            // tbKmCambioNeumaticos
            // 
            this.tbKmCambioNeumaticos.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKmCambioNeumaticos.Location = new System.Drawing.Point(140, 206);
            this.tbKmCambioNeumaticos.MaxLength = 6;
            this.tbKmCambioNeumaticos.Name = "tbKmCambioNeumaticos";
            this.tbKmCambioNeumaticos.ShortcutsEnabled = false;
            this.tbKmCambioNeumaticos.Size = new System.Drawing.Size(156, 21);
            this.tbKmCambioNeumaticos.TabIndex = 9;
            this.tbKmCambioNeumaticos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblUsoNeumaticos
            // 
            this.lblUsoNeumaticos.AutoSize = true;
            this.lblUsoNeumaticos.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUsoNeumaticos.Location = new System.Drawing.Point(12, 209);
            this.lblUsoNeumaticos.Name = "lblUsoNeumaticos";
            this.lblUsoNeumaticos.Size = new System.Drawing.Size(125, 13);
            this.lblUsoNeumaticos.TabIndex = 30;
            this.lblUsoNeumaticos.Text = "Km Cambio Neumaticos:";
            // 
            // tbKmUnidad
            // 
            this.tbKmUnidad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKmUnidad.Location = new System.Drawing.Point(140, 173);
            this.tbKmUnidad.MaxLength = 6;
            this.tbKmUnidad.Name = "tbKmUnidad";
            this.tbKmUnidad.ShortcutsEnabled = false;
            this.tbKmUnidad.Size = new System.Drawing.Size(156, 21);
            this.tbKmUnidad.TabIndex = 8;
            this.tbKmUnidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblKmUnidad
            // 
            this.lblKmUnidad.AutoSize = true;
            this.lblKmUnidad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKmUnidad.Location = new System.Drawing.Point(12, 176);
            this.lblKmUnidad.Name = "lblKmUnidad";
            this.lblKmUnidad.Size = new System.Drawing.Size(61, 13);
            this.lblKmUnidad.TabIndex = 28;
            this.lblKmUnidad.Text = "Km Unidad:";
            // 
            // tbTara
            // 
            this.tbTara.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTara.Location = new System.Drawing.Point(140, 140);
            this.tbTara.MaxLength = 6;
            this.tbTara.Name = "tbTara";
            this.tbTara.ShortcutsEnabled = false;
            this.tbTara.Size = new System.Drawing.Size(156, 21);
            this.tbTara.TabIndex = 7;
            this.tbTara.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblTara
            // 
            this.lblTara.AutoSize = true;
            this.lblTara.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTara.Location = new System.Drawing.Point(12, 143);
            this.lblTara.Name = "lblTara";
            this.lblTara.Size = new System.Drawing.Size(31, 13);
            this.lblTara.TabIndex = 26;
            this.lblTara.Text = "Tara:";
            // 
            // cbMarca
            // 
            this.cbMarca.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMarca.FormattingEnabled = true;
            this.cbMarca.Location = new System.Drawing.Point(140, 39);
            this.cbMarca.Name = "cbMarca";
            this.cbMarca.Size = new System.Drawing.Size(156, 21);
            this.cbMarca.TabIndex = 4;
            this.cbMarca.SelectedIndexChanged += new System.EventHandler(this.cbMarca_SelectedIndexChanged);
            // 
            // gbCamionRigido
            // 
            this.gbCamionRigido.Controls.Add(this.tbVolumen);
            this.gbCamionRigido.Controls.Add(this.lblVolumen);
            this.gbCamionRigido.Controls.Add(this.chEnganche);
            this.gbCamionRigido.Controls.Add(this.cbTipoCarga);
            this.gbCamionRigido.Controls.Add(this.tbAltura);
            this.gbCamionRigido.Controls.Add(this.lblTipoCarga);
            this.gbCamionRigido.Controls.Add(this.lblAltura);
            this.gbCamionRigido.Controls.Add(this.tbCapacidad);
            this.gbCamionRigido.Controls.Add(this.lblAnchoInterior);
            this.gbCamionRigido.Controls.Add(this.lblCapacidad);
            this.gbCamionRigido.Controls.Add(this.tbAnchoInterior);
            this.gbCamionRigido.Controls.Add(this.tbLongitud);
            this.gbCamionRigido.Controls.Add(this.lblAnchoExterior);
            this.gbCamionRigido.Controls.Add(this.lblLongitud);
            this.gbCamionRigido.Controls.Add(this.tbAnchoExterior);
            this.gbCamionRigido.Enabled = false;
            this.gbCamionRigido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCamionRigido.Location = new System.Drawing.Point(307, 32);
            this.gbCamionRigido.Name = "gbCamionRigido";
            this.gbCamionRigido.Size = new System.Drawing.Size(291, 231);
            this.gbCamionRigido.TabIndex = 11;
            this.gbCamionRigido.TabStop = false;
            this.gbCamionRigido.Text = "Camion Rigido";
            // 
            // tbVolumen
            // 
            this.tbVolumen.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVolumen.Location = new System.Drawing.Point(114, 152);
            this.tbVolumen.MaxLength = 6;
            this.tbVolumen.Name = "tbVolumen";
            this.tbVolumen.ShortcutsEnabled = false;
            this.tbVolumen.Size = new System.Drawing.Size(156, 21);
            this.tbVolumen.TabIndex = 18;
            this.tbVolumen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblVolumen
            // 
            this.lblVolumen.AutoSize = true;
            this.lblVolumen.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolumen.Location = new System.Drawing.Point(19, 155);
            this.lblVolumen.Name = "lblVolumen";
            this.lblVolumen.Size = new System.Drawing.Size(51, 13);
            this.lblVolumen.TabIndex = 49;
            this.lblVolumen.Text = "Volumen:";
            // 
            // chEnganche
            // 
            this.chEnganche.AutoSize = true;
            this.chEnganche.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chEnganche.ForeColor = System.Drawing.Color.Red;
            this.chEnganche.Location = new System.Drawing.Point(114, 204);
            this.chEnganche.Name = "chEnganche";
            this.chEnganche.Size = new System.Drawing.Size(94, 17);
            this.chEnganche.TabIndex = 20;
            this.chEnganche.Text = "Con enganche";
            this.chEnganche.UseVisualStyleBackColor = true;
            // 
            // cbTipoCarga
            // 
            this.cbTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoCarga.FormattingEnabled = true;
            this.cbTipoCarga.ItemHeight = 13;
            this.cbTipoCarga.Location = new System.Drawing.Point(114, 22);
            this.cbTipoCarga.Name = "cbTipoCarga";
            this.cbTipoCarga.Size = new System.Drawing.Size(156, 21);
            this.cbTipoCarga.TabIndex = 47;
            // 
            // tbAltura
            // 
            this.tbAltura.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAltura.Location = new System.Drawing.Point(114, 49);
            this.tbAltura.MaxLength = 4;
            this.tbAltura.Name = "tbAltura";
            this.tbAltura.ShortcutsEnabled = false;
            this.tbAltura.Size = new System.Drawing.Size(156, 21);
            this.tbAltura.TabIndex = 14;
            this.tbAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblTipoCarga
            // 
            this.lblTipoCarga.AutoSize = true;
            this.lblTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCarga.Location = new System.Drawing.Point(19, 30);
            this.lblTipoCarga.Name = "lblTipoCarga";
            this.lblTipoCarga.Size = new System.Drawing.Size(73, 13);
            this.lblTipoCarga.TabIndex = 34;
            this.lblTipoCarga.Text = "Tipo de carga:";
            // 
            // lblAltura
            // 
            this.lblAltura.AutoSize = true;
            this.lblAltura.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(19, 52);
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.Size = new System.Drawing.Size(40, 13);
            this.lblAltura.TabIndex = 36;
            this.lblAltura.Text = "Altura:";
            // 
            // tbCapacidad
            // 
            this.tbCapacidad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCapacidad.Location = new System.Drawing.Point(114, 178);
            this.tbCapacidad.MaxLength = 6;
            this.tbCapacidad.Name = "tbCapacidad";
            this.tbCapacidad.ShortcutsEnabled = false;
            this.tbCapacidad.Size = new System.Drawing.Size(156, 21);
            this.tbCapacidad.TabIndex = 19;
            this.tbCapacidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAnchoInterior
            // 
            this.lblAnchoInterior.AutoSize = true;
            this.lblAnchoInterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnchoInterior.Location = new System.Drawing.Point(19, 77);
            this.lblAnchoInterior.Name = "lblAnchoInterior";
            this.lblAnchoInterior.Size = new System.Drawing.Size(78, 13);
            this.lblAnchoInterior.TabIndex = 37;
            this.lblAnchoInterior.Text = "Ancho Interior:";
            // 
            // lblCapacidad
            // 
            this.lblCapacidad.AutoSize = true;
            this.lblCapacidad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapacidad.Location = new System.Drawing.Point(19, 181);
            this.lblCapacidad.Name = "lblCapacidad";
            this.lblCapacidad.Size = new System.Drawing.Size(60, 13);
            this.lblCapacidad.TabIndex = 43;
            this.lblCapacidad.Text = "Capacidad:";
            // 
            // tbAnchoInterior
            // 
            this.tbAnchoInterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoInterior.Location = new System.Drawing.Point(114, 74);
            this.tbAnchoInterior.MaxLength = 4;
            this.tbAnchoInterior.Name = "tbAnchoInterior";
            this.tbAnchoInterior.ShortcutsEnabled = false;
            this.tbAnchoInterior.Size = new System.Drawing.Size(156, 21);
            this.tbAnchoInterior.TabIndex = 15;
            this.tbAnchoInterior.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // tbLongitud
            // 
            this.tbLongitud.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLongitud.Location = new System.Drawing.Point(114, 126);
            this.tbLongitud.MaxLength = 4;
            this.tbLongitud.Name = "tbLongitud";
            this.tbLongitud.ShortcutsEnabled = false;
            this.tbLongitud.Size = new System.Drawing.Size(156, 21);
            this.tbLongitud.TabIndex = 17;
            this.tbLongitud.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAnchoExterior
            // 
            this.lblAnchoExterior.AutoSize = true;
            this.lblAnchoExterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnchoExterior.Location = new System.Drawing.Point(19, 103);
            this.lblAnchoExterior.Name = "lblAnchoExterior";
            this.lblAnchoExterior.Size = new System.Drawing.Size(82, 13);
            this.lblAnchoExterior.TabIndex = 39;
            this.lblAnchoExterior.Text = "Ancho Exterior:";
            // 
            // lblLongitud
            // 
            this.lblLongitud.AutoSize = true;
            this.lblLongitud.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLongitud.Location = new System.Drawing.Point(19, 129);
            this.lblLongitud.Name = "lblLongitud";
            this.lblLongitud.Size = new System.Drawing.Size(52, 13);
            this.lblLongitud.TabIndex = 41;
            this.lblLongitud.Text = "Longitud:";
            // 
            // tbAnchoExterior
            // 
            this.tbAnchoExterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoExterior.Location = new System.Drawing.Point(114, 100);
            this.tbAnchoExterior.MaxLength = 4;
            this.tbAnchoExterior.Name = "tbAnchoExterior";
            this.tbAnchoExterior.ShortcutsEnabled = false;
            this.tbAnchoExterior.Size = new System.Drawing.Size(156, 21);
            this.tbAnchoExterior.TabIndex = 16;
            this.tbAnchoExterior.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(523, 269);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 21;
            this.btnOk.Text = "Guardar";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // cbModelo
            // 
            this.cbModelo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModelo.FormattingEnabled = true;
            this.cbModelo.Location = new System.Drawing.Point(140, 73);
            this.cbModelo.Name = "cbModelo";
            this.cbModelo.Size = new System.Drawing.Size(156, 21);
            this.cbModelo.TabIndex = 5;
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(432, 269);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(75, 23);
            this.btnSalir.TabIndex = 22;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // tbService
            // 
            this.tbService.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbService.Location = new System.Drawing.Point(140, 239);
            this.tbService.MaxLength = 6;
            this.tbService.Name = "tbService";
            this.tbService.ShortcutsEnabled = false;
            this.tbService.Size = new System.Drawing.Size(156, 21);
            this.tbService.TabIndex = 10;
            this.tbService.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblKmService
            // 
            this.lblKmService.AutoSize = true;
            this.lblKmService.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKmService.Location = new System.Drawing.Point(12, 242);
            this.lblKmService.Name = "lblKmService";
            this.lblKmService.Size = new System.Drawing.Size(63, 13);
            this.lblKmService.TabIndex = 65;
            this.lblKmService.Text = "Km Service";
            // 
            // frmNuevoCamion
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(611, 298);
            this.Controls.Add(this.tbService);
            this.Controls.Add(this.lblKmService);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.cbModelo);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.gbCamionRigido);
            this.Controls.Add(this.cbMarca);
            this.Controls.Add(this.tbKmCambioNeumaticos);
            this.Controls.Add(this.lblUsoNeumaticos);
            this.Controls.Add(this.tbKmUnidad);
            this.Controls.Add(this.lblKmUnidad);
            this.Controls.Add(this.tbTara);
            this.Controls.Add(this.lblTara);
            this.Controls.Add(this.tbAño);
            this.Controls.Add(this.lblAño);
            this.Controls.Add(this.lblModelo);
            this.Controls.Add(this.lblMarca);
            this.Controls.Add(this.tbDominio);
            this.Controls.Add(this.lblDominio);
            this.Controls.Add(this.rbRigido);
            this.Controls.Add(this.rbTracto);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNuevoCamion";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alta y Modificación Camión - Operaciones";
            this.Load += new System.EventHandler(this.frmNuevoCamion_Load);
            this.gbCamionRigido.ResumeLayout(false);
            this.gbCamionRigido.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton rbRigido;
        private System.Windows.Forms.RadioButton rbTracto;
        private System.Windows.Forms.Label lblDominio;
        private System.Windows.Forms.TextBox tbDominio;
        private System.Windows.Forms.TextBox tbAño;
        private System.Windows.Forms.Label lblAño;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.TextBox tbKmCambioNeumaticos;
        private System.Windows.Forms.Label lblUsoNeumaticos;
        private System.Windows.Forms.TextBox tbKmUnidad;
        private System.Windows.Forms.Label lblKmUnidad;
        private System.Windows.Forms.TextBox tbTara;
        private System.Windows.Forms.Label lblTara;
        private System.Windows.Forms.ComboBox cbMarca;
        private System.Windows.Forms.GroupBox gbCamionRigido;
        private System.Windows.Forms.CheckBox chEnganche;
        private System.Windows.Forms.ComboBox cbTipoCarga;
        private System.Windows.Forms.TextBox tbAltura;
        private System.Windows.Forms.Label lblTipoCarga;
        private System.Windows.Forms.Label lblAltura;
        private System.Windows.Forms.TextBox tbCapacidad;
        private System.Windows.Forms.Label lblAnchoInterior;
        private System.Windows.Forms.Label lblCapacidad;
        private System.Windows.Forms.TextBox tbAnchoInterior;
        private System.Windows.Forms.TextBox tbLongitud;
        private System.Windows.Forms.Label lblAnchoExterior;
        private System.Windows.Forms.Label lblLongitud;
        private System.Windows.Forms.TextBox tbAnchoExterior;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.ComboBox cbModelo;
        private System.Windows.Forms.TextBox tbVolumen;
        private System.Windows.Forms.Label lblVolumen;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.TextBox tbService;
        private System.Windows.Forms.Label lblKmService;
    }
}