﻿namespace CapaPresentacion.Operaciones
{
    partial class frmChofer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbNum_legajo = new System.Windows.Forms.TextBox();
            this.lbNumLegajo = new System.Windows.Forms.Label();
            this.tbTel_Trabajo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.tbTel_personal = new System.Windows.Forms.TextBox();
            this.lbTelPersonal = new System.Windows.Forms.Label();
            this.tbDireccion = new System.Windows.Forms.TextBox();
            this.lbDireccion = new System.Windows.Forms.Label();
            this.cbEstado = new System.Windows.Forms.CheckBox();
            this.lbEstado = new System.Windows.Forms.Label();
            this.tbNum_cedula = new System.Windows.Forms.TextBox();
            this.lbNombre = new System.Windows.Forms.Label();
            this.lbNumCedula = new System.Windows.Forms.Label();
            this.lbApellido = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.tbNombre = new System.Windows.Forms.TextBox();
            this.tbApellido = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // tbNum_legajo
            // 
            this.tbNum_legajo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNum_legajo.Location = new System.Drawing.Point(95, 6);
            this.tbNum_legajo.MaxLength = 5;
            this.tbNum_legajo.Name = "tbNum_legajo";
            this.tbNum_legajo.ShortcutsEnabled = false;
            this.tbNum_legajo.Size = new System.Drawing.Size(158, 21);
            this.tbNum_legajo.TabIndex = 1;
            this.tbNum_legajo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNum_legajo_KeyPress);
            // 
            // lbNumLegajo
            // 
            this.lbNumLegajo.AutoSize = true;
            this.lbNumLegajo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumLegajo.Location = new System.Drawing.Point(7, 9);
            this.lbNumLegajo.Name = "lbNumLegajo";
            this.lbNumLegajo.Size = new System.Drawing.Size(82, 13);
            this.lbNumLegajo.TabIndex = 66;
            this.lbNumLegajo.Text = "Numero Legajo";
            // 
            // tbTel_Trabajo
            // 
            this.tbTel_Trabajo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTel_Trabajo.Location = new System.Drawing.Point(95, 204);
            this.tbTel_Trabajo.MaxLength = 11;
            this.tbTel_Trabajo.Name = "tbTel_Trabajo";
            this.tbTel_Trabajo.ShortcutsEnabled = false;
            this.tbTel_Trabajo.Size = new System.Drawing.Size(158, 21);
            this.tbTel_Trabajo.TabIndex = 7;
            this.tbTel_Trabajo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNum_legajo_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(7, 207);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 13);
            this.label1.TabIndex = 64;
            this.label1.Text = "Tel. Trabajo";
            // 
            // tbTel_personal
            // 
            this.tbTel_personal.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTel_personal.Location = new System.Drawing.Point(95, 171);
            this.tbTel_personal.MaxLength = 11;
            this.tbTel_personal.Name = "tbTel_personal";
            this.tbTel_personal.ShortcutsEnabled = false;
            this.tbTel_personal.Size = new System.Drawing.Size(158, 21);
            this.tbTel_personal.TabIndex = 6;
            this.tbTel_personal.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNum_legajo_KeyPress);
            // 
            // lbTelPersonal
            // 
            this.lbTelPersonal.AutoSize = true;
            this.lbTelPersonal.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTelPersonal.Location = new System.Drawing.Point(7, 174);
            this.lbTelPersonal.Name = "lbTelPersonal";
            this.lbTelPersonal.Size = new System.Drawing.Size(69, 13);
            this.lbTelPersonal.TabIndex = 62;
            this.lbTelPersonal.Text = "Tel. Personal";
            // 
            // tbDireccion
            // 
            this.tbDireccion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDireccion.Location = new System.Drawing.Point(95, 138);
            this.tbDireccion.MaxLength = 100;
            this.tbDireccion.Name = "tbDireccion";
            this.tbDireccion.ShortcutsEnabled = false;
            this.tbDireccion.Size = new System.Drawing.Size(158, 21);
            this.tbDireccion.TabIndex = 5;
            // 
            // lbDireccion
            // 
            this.lbDireccion.AutoSize = true;
            this.lbDireccion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDireccion.Location = new System.Drawing.Point(7, 141);
            this.lbDireccion.Name = "lbDireccion";
            this.lbDireccion.Size = new System.Drawing.Size(53, 13);
            this.lbDireccion.TabIndex = 60;
            this.lbDireccion.Text = "Direccion";
            // 
            // cbEstado
            // 
            this.cbEstado.AutoSize = true;
            this.cbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEstado.Location = new System.Drawing.Point(84, 237);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(15, 14);
            this.cbEstado.TabIndex = 8;
            this.cbEstado.UseVisualStyleBackColor = true;
            // 
            // lbEstado
            // 
            this.lbEstado.AutoSize = true;
            this.lbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstado.Location = new System.Drawing.Point(7, 234);
            this.lbEstado.Name = "lbEstado";
            this.lbEstado.Size = new System.Drawing.Size(42, 13);
            this.lbEstado.TabIndex = 57;
            this.lbEstado.Text = "Estado";
            // 
            // tbNum_cedula
            // 
            this.tbNum_cedula.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNum_cedula.Location = new System.Drawing.Point(95, 105);
            this.tbNum_cedula.MaxLength = 100;
            this.tbNum_cedula.Name = "tbNum_cedula";
            this.tbNum_cedula.ShortcutsEnabled = false;
            this.tbNum_cedula.Size = new System.Drawing.Size(158, 21);
            this.tbNum_cedula.TabIndex = 4;
            // 
            // lbNombre
            // 
            this.lbNombre.AutoSize = true;
            this.lbNombre.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombre.Location = new System.Drawing.Point(7, 42);
            this.lbNombre.Name = "lbNombre";
            this.lbNombre.Size = new System.Drawing.Size(47, 13);
            this.lbNombre.TabIndex = 50;
            this.lbNombre.Text = "Nombre";
            // 
            // lbNumCedula
            // 
            this.lbNumCedula.AutoSize = true;
            this.lbNumCedula.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumCedula.Location = new System.Drawing.Point(7, 108);
            this.lbNumCedula.Name = "lbNumCedula";
            this.lbNumCedula.Size = new System.Drawing.Size(83, 13);
            this.lbNumCedula.TabIndex = 56;
            this.lbNumCedula.Text = "Numero Cedula";
            // 
            // lbApellido
            // 
            this.lbApellido.AutoSize = true;
            this.lbApellido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbApellido.Location = new System.Drawing.Point(7, 75);
            this.lbApellido.Name = "lbApellido";
            this.lbApellido.Size = new System.Drawing.Size(47, 13);
            this.lbApellido.TabIndex = 51;
            this.lbApellido.Text = "Apellido";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(123, 253);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(130, 29);
            this.btnGuardar.TabIndex = 9;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // tbNombre
            // 
            this.tbNombre.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNombre.Location = new System.Drawing.Point(95, 39);
            this.tbNombre.MaxLength = 100;
            this.tbNombre.Name = "tbNombre";
            this.tbNombre.ShortcutsEnabled = false;
            this.tbNombre.Size = new System.Drawing.Size(158, 21);
            this.tbNombre.TabIndex = 2;
            this.tbNombre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // tbApellido
            // 
            this.tbApellido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbApellido.Location = new System.Drawing.Point(95, 72);
            this.tbApellido.MaxLength = 100;
            this.tbApellido.Name = "tbApellido";
            this.tbApellido.ShortcutsEnabled = false;
            this.tbApellido.Size = new System.Drawing.Size(158, 21);
            this.tbApellido.TabIndex = 3;
            this.tbApellido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNombre_KeyPress);
            // 
            // frmChofer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(254, 285);
            this.Controls.Add(this.tbNum_legajo);
            this.Controls.Add(this.lbNumLegajo);
            this.Controls.Add(this.tbTel_Trabajo);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbTel_personal);
            this.Controls.Add(this.lbTelPersonal);
            this.Controls.Add(this.tbDireccion);
            this.Controls.Add(this.lbDireccion);
            this.Controls.Add(this.cbEstado);
            this.Controls.Add(this.lbEstado);
            this.Controls.Add(this.tbNum_cedula);
            this.Controls.Add(this.lbNombre);
            this.Controls.Add(this.lbNumCedula);
            this.Controls.Add(this.lbApellido);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.tbNombre);
            this.Controls.Add(this.tbApellido);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChofer";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alta Chofer - Operaciones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbNum_legajo;
        private System.Windows.Forms.Label lbNumLegajo;
        private System.Windows.Forms.TextBox tbTel_Trabajo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbTel_personal;
        private System.Windows.Forms.Label lbTelPersonal;
        private System.Windows.Forms.TextBox tbDireccion;
        private System.Windows.Forms.Label lbDireccion;
        private System.Windows.Forms.CheckBox cbEstado;
        private System.Windows.Forms.Label lbEstado;
        private System.Windows.Forms.TextBox tbNum_cedula;
        private System.Windows.Forms.Label lbNombre;
        private System.Windows.Forms.Label lbNumCedula;
        private System.Windows.Forms.Label lbApellido;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.TextBox tbNombre;
        private System.Windows.Forms.TextBox tbApellido;
    }
}