﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    public partial class frmPedido : Form
    {
        N_Pedido n_pedido;
        N_TipoCarga n_tipo_carga;
        N_Chofer n_chofer;
        E_Pedido e_pedido;
        E_Viaje e_viaje;
        string usuario;
        actualizaGrillaPedidos actualizaPedidos;
        MisFunciones funcion;
        public frmPedido(string usuario, E_Pedido e_pedido, Delegate actualizaPedidos)
        {
            InitializeComponent();
            n_pedido = new N_Pedido(usuario);
            this.e_pedido = e_pedido;
            e_viaje = new E_Viaje();
            this.usuario = usuario;
            n_chofer = new N_Chofer(usuario);
            n_tipo_carga = new N_TipoCarga(usuario);
            this.actualizaPedidos = (actualizaGrillaPedidos)actualizaPedidos;
            this.cargaComboBoxs();
            funcion = new MisFunciones();
            if (e_pedido != null)
                cargaPedidoModificar(e_pedido);
            else
            {
                n_pedido = new N_Pedido(usuario);
                cbEstado.SelectedIndex = 1;
                cbEstado.Enabled = false;
                if (n_pedido.listarTodos().Rows.Count > 0)
                    tbNumPedido.Text = (n_pedido.retornaIdUltimoPedido() + 1).ToString();
                else
                    tbNumPedido.Text = "0";
            }
        }
        private void rbC_Enganche_CheckedChanged(object sender, EventArgs e)
        {
            if (rbC_Enganche.Checked)
            {
                gpDatosCamionRigido.Enabled = true;
                gbAcoplado.Enabled = true;
            }
            else if(rbSR_Enganche.Checked)
            {
                gpDatosCamionRigido.Enabled = true;
                gbAcoplado.Enabled = false;
            }
            else if(rbSS_Enganche.Checked)
            {
                gpDatosCamionRigido.Enabled = false;
                gbAcoplado.Enabled = true;
            }
        }
        private string radioButtonCheked()
        {
            if (rbC_Enganche.Checked)
                return "ENGANCHE CON REMOLQUE";
            else if (rbSR_Enganche.Checked)
                return "SIN ENGANCHE RIGIDO";
            else
                return "SIN ENGANCHE SEMIREMOLQUE";
        }
        private void checkedRadioButton(string enganche)
        {
            if (enganche == "ENGANCHE CON REMOLQUE")
                rbC_Enganche.Checked = true;
            else if (enganche == "SIN ENGANCHE RIGIDO")
                rbSR_Enganche.Checked = true;
            else
                rbSS_Enganche.Checked = true;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (cbChofer.SelectedItem.ToString() == "SELECCIONE CHOFER")
                new frmMensajes("Error", "Por favor, seleccione chofer para continuar").ShowDialog();
            else if(DateTime.Compare(Convert.ToDateTime(dtpRegreso.Value.ToShortDateString()), Convert.ToDateTime(dtpSalida.Value.ToShortDateString())) <0)
                new frmMensajes("Error", "Por favor, seleccione fecha correcta para continuar").ShowDialog();
            else if(string.IsNullOrWhiteSpace(tbPesoNeto.Text) || string.IsNullOrWhiteSpace(tbVolumen.Text)
                || string.IsNullOrWhiteSpace(tbKmViaje.Text) || string.IsNullOrWhiteSpace(tbCliente.Text) || string.IsNullOrWhiteSpace(tbCuilCliente.Text))
                new frmMensajes("Error", "No es posible realizar la operación sin antes llenar los campos faltantes").ShowDialog();
            else if(cbTipoCarga.SelectedItem.ToString() == "SELECCIONE TIPO DE CARGA")
                new frmMensajes("Error", "Por favor, seleccione tipo de carga para continuar").ShowDialog();
            else if (cbPrioridad.SelectedItem.ToString() == "SELECCIONE PRIORIDAD")
                new frmMensajes("Error", "Por favor, seleccione prioridad para continuar").ShowDialog();
            else if(!rbC_Enganche.Checked && !rbSR_Enganche.Checked && !rbSS_Enganche.Checked)
                new frmMensajes("Error", "Por favor, seleccione tipo de enganche para continuar").ShowDialog();
            else
            {
                bool chofer = n_chofer.chofer_libre(n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim()), dtpSalida.Value, dtpRegreso.Value);
                if (e_pedido != null)
                {
                    if (n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim()) != e_pedido.Id_chofer)
                    {
                        if(chofer)
                        {
                            try
                            {
                                if (e_pedido.Estado.ToString().Trim() == "PENDIENTE" || e_pedido.Estado.ToString().Trim() == "CANCELADO")
                                {
                                    try
                                    {
                                        e_pedido.Id_chofer = n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim());
                                        e_pedido.Fecha_pedido = Convert.ToDateTime(dtpPedido.Value.ToShortDateString());
                                        e_pedido.Fecha_salida = Convert.ToDateTime(dtpSalida.Value.ToShortDateString());
                                        e_pedido.Fecha_regreso = Convert.ToDateTime(dtpRegreso.Value.ToShortDateString());
                                        e_pedido.Peso_neto = float.Parse(tbPesoNeto.Text);
                                        e_pedido.Volumen = float.Parse(tbVolumen.Text);
                                        e_pedido.Km_viaje = float.Parse(tbKmViaje.Text);
                                        e_pedido.Prioridad = cbPrioridad.SelectedItem.ToString().ToUpper();
                                        e_pedido.Estado = cbEstado.SelectedItem.ToString().ToUpper();
                                        e_pedido.Observaciones = tbObservaciones.Text;
                                        e_pedido.Nombre_cliente = tbCliente.Text.ToUpper();
                                        e_pedido.Cuil_cliente = tbCuilCliente.Text.ToUpper();
                                        e_pedido.Tipo_carga = n_tipo_carga.retornaTipoCarga(n_tipo_carga.retornaId(cbTipoCarga.SelectedItem.ToString())).Tipo.ToString();
                                        e_pedido.Enganche = radioButtonCheked();
                                        if (e_pedido.Enganche == "ENGANCHE CON REMOLQUE")
                                        {
                                            if (Convert.ToInt32(tbAltCamion.Text) == 0 || Convert.ToInt32(tbAnchoIntCamion.Text) == 0
                                                || Convert.ToInt32(tbAnchoExtCamion.Text) == 0 || Convert.ToInt32(tbLongitudCamion.Text) == 0
                                                || Convert.ToInt32(tbAlturaAcoplado.Text) == 0 || Convert.ToInt32(tbAnchoIntAcoplado.Text) == 0
                                                || Convert.ToInt32(tbAnchoExtAcoplado.Text) == 0 || Convert.ToInt32(tbLongitudAcoplado.Text) == 0)
                                                new frmMensajes("Error", "Por favor, debe insertar datos correspondiente a la unidad").ShowDialog();
                                            else
                                            {
                                                e_pedido.Altura_camion = float.Parse(tbAltCamion.Text);
                                                e_pedido.Ancho_interior_camion = float.Parse(tbAnchoIntCamion.Text);
                                                e_pedido.Ancho_exterior_camion = float.Parse(tbAnchoExtCamion.Text);
                                                e_pedido.Longitud_camion = float.Parse(tbLongitudCamion.Text);
                                                e_pedido.Altura_acoplado = float.Parse(tbAlturaAcoplado.Text);
                                                e_pedido.Ancho_interior_acoplado = float.Parse(tbAnchoIntAcoplado.Text);
                                                e_pedido.Ancho_exterior_acoplado = float.Parse(tbAnchoExtAcoplado.Text);
                                                e_pedido.Longitud_acoplado = float.Parse(tbLongitudAcoplado.Text);
                                                List<string> resp = n_pedido.modificar(e_pedido);
                                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                                            }
                                        }
                                        else if (e_pedido.Enganche == "SIN ENGANCHE RIGIDO")
                                        {
                                            if (Convert.ToInt32(tbAltCamion.Text) == 0 || Convert.ToInt32(tbAnchoIntCamion.Text) == 0
                                                || Convert.ToInt32(tbAnchoExtCamion.Text) == 0 || Convert.ToInt32(tbLongitudCamion.Text) == 0)
                                                MessageBox.Show("Por favor, debe insertar datos correspondiente a la unidad");
                                            else
                                            {
                                                e_pedido.Altura_camion = float.Parse(tbAltCamion.Text);
                                                e_pedido.Ancho_interior_camion = float.Parse(tbAnchoIntCamion.Text);
                                                e_pedido.Ancho_exterior_camion = float.Parse(tbAnchoExtCamion.Text);
                                                e_pedido.Longitud_camion = float.Parse(tbLongitudCamion.Text);
                                                List<string> resp = n_pedido.modificar(e_pedido);
                                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                                            }
                                        }
                                        else if (e_pedido.Enganche == "SIN ENGANCHE SEMIREMOLQUE")
                                        {
                                            if (Convert.ToInt32(tbAlturaAcoplado.Text) == 0 || Convert.ToInt32(tbAnchoIntAcoplado.Text) == 0
                                                || Convert.ToInt32(tbAnchoExtAcoplado.Text) == 0 || Convert.ToInt32(tbLongitudAcoplado.Text) == 0)
                                                MessageBox.Show("Por favor, debe insertar datos correspondiente a la unidad");
                                            else
                                            {
                                                e_pedido.Altura_acoplado = float.Parse(tbAlturaAcoplado.Text);
                                                e_pedido.Ancho_interior_acoplado = float.Parse(tbAnchoIntAcoplado.Text);
                                                e_pedido.Ancho_exterior_acoplado = float.Parse(tbAnchoExtAcoplado.Text);
                                                e_pedido.Longitud_acoplado = float.Parse(tbLongitudAcoplado.Text);
                                                List<string> resp = n_pedido.modificar(e_pedido);
                                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                                            }
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }
                                }
                                else if (e_pedido.Estado.ToString().Trim() == "CARGADO" && e_viaje.Estado.ToString().Trim() != "EN TRANSCURSO")
                                {
                                    try
                                    {
                                        e_pedido.Id_chofer = n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim());
                                        e_pedido.Fecha_salida = Convert.ToDateTime(dtpSalida.Value.ToShortDateString());
                                        e_pedido.Fecha_regreso = Convert.ToDateTime(dtpRegreso.Value.ToShortDateString());
                                        List<string> resp = n_pedido.modificar(e_pedido);
                                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }
                                }
                                this.Hide();
                            }
                            catch (Exception ex)
                            {
                                new frmMensajes("Error", ex.Message).ShowDialog();
                            }
                        }
                        else
                            new frmMensajes("Error", "Chofer seleccionado ya se encuentra seleccionado para otro viaje en la fecha seleccionada").ShowDialog();
                    }
                    else
                    {
                        try
                        {
                            if (e_pedido.Estado.ToString().Trim() == "PENDIENTE" || e_pedido.Estado.ToString().Trim() == "CANCELADO")
                            {
                                try
                                {
                                    e_pedido.Id_chofer = n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim());
                                    e_pedido.Fecha_pedido = Convert.ToDateTime(dtpPedido.Value.ToShortDateString());
                                    e_pedido.Fecha_salida = Convert.ToDateTime(dtpSalida.Value.ToShortDateString());
                                    e_pedido.Fecha_regreso = Convert.ToDateTime(dtpRegreso.Value.ToShortDateString());
                                    e_pedido.Peso_neto = float.Parse(tbPesoNeto.Text);
                                    e_pedido.Volumen = float.Parse(tbVolumen.Text);
                                    e_pedido.Km_viaje = float.Parse(tbKmViaje.Text);
                                    e_pedido.Prioridad = cbPrioridad.SelectedItem.ToString().ToUpper();
                                    e_pedido.Estado = cbEstado.SelectedItem.ToString().ToUpper();
                                    e_pedido.Observaciones = tbObservaciones.Text;
                                    e_pedido.Nombre_cliente = tbCliente.Text.ToUpper();
                                    e_pedido.Cuil_cliente = tbCuilCliente.Text.ToUpper();
                                    e_pedido.Tipo_carga = n_tipo_carga.retornaTipoCarga(n_tipo_carga.retornaId(cbTipoCarga.SelectedItem.ToString())).Tipo.ToString();
                                    e_pedido.Enganche = radioButtonCheked();
                                    if (e_pedido.Enganche == "ENGANCHE CON REMOLQUE")
                                    {
                                        if (Convert.ToInt32(tbAltCamion.Text) == 0 || Convert.ToInt32(tbAnchoIntCamion.Text) == 0
                                            || Convert.ToInt32(tbAnchoExtCamion.Text) == 0 || Convert.ToInt32(tbLongitudCamion.Text) == 0
                                            || Convert.ToInt32(tbAlturaAcoplado.Text) == 0 || Convert.ToInt32(tbAnchoIntAcoplado.Text) == 0
                                            || Convert.ToInt32(tbAnchoExtAcoplado.Text) == 0 || Convert.ToInt32(tbLongitudAcoplado.Text) == 0)
                                            new frmMensajes("Error", "Por favor, debe insertar datos correspondiente a la unidad").ShowDialog();
                                        else
                                        {
                                            e_pedido.Altura_camion = float.Parse(tbAltCamion.Text);
                                            e_pedido.Ancho_interior_camion = float.Parse(tbAnchoIntCamion.Text);
                                            e_pedido.Ancho_exterior_camion = float.Parse(tbAnchoExtCamion.Text);
                                            e_pedido.Longitud_camion = float.Parse(tbLongitudCamion.Text);
                                            e_pedido.Altura_acoplado = float.Parse(tbAlturaAcoplado.Text);
                                            e_pedido.Ancho_interior_acoplado = float.Parse(tbAnchoIntAcoplado.Text);
                                            e_pedido.Ancho_exterior_acoplado = float.Parse(tbAnchoExtAcoplado.Text);
                                            e_pedido.Longitud_acoplado = float.Parse(tbLongitudAcoplado.Text);
                                            List<string> resp = n_pedido.modificar(e_pedido);
                                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                                        }
                                    }
                                    else if (e_pedido.Enganche == "SIN ENGANCHE RIGIDO")
                                    {
                                        if (Convert.ToInt32(tbAltCamion.Text) == 0 || Convert.ToInt32(tbAnchoIntCamion.Text) == 0
                                            || Convert.ToInt32(tbAnchoExtCamion.Text) == 0 || Convert.ToInt32(tbLongitudCamion.Text) == 0)
                                            MessageBox.Show("Por favor, debe insertar datos correspondiente a la unidad");
                                        else
                                        {
                                            e_pedido.Altura_camion = float.Parse(tbAltCamion.Text);
                                            e_pedido.Ancho_interior_camion = float.Parse(tbAnchoIntCamion.Text);
                                            e_pedido.Ancho_exterior_camion = float.Parse(tbAnchoExtCamion.Text);
                                            e_pedido.Longitud_camion = float.Parse(tbLongitudCamion.Text);
                                            List<string> resp = n_pedido.modificar(e_pedido);
                                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                                        }
                                    }
                                    else if (e_pedido.Enganche == "SIN ENGANCHE SEMIREMOLQUE")
                                    {
                                        if (Convert.ToInt32(tbAlturaAcoplado.Text) == 0 || Convert.ToInt32(tbAnchoIntAcoplado.Text) == 0
                                            || Convert.ToInt32(tbAnchoExtAcoplado.Text) == 0 || Convert.ToInt32(tbLongitudAcoplado.Text) == 0)
                                            MessageBox.Show("Por favor, debe insertar datos correspondiente a la unidad");
                                        else
                                        {
                                            e_pedido.Altura_acoplado = float.Parse(tbAlturaAcoplado.Text);
                                            e_pedido.Ancho_interior_acoplado = float.Parse(tbAnchoIntAcoplado.Text);
                                            e_pedido.Ancho_exterior_acoplado = float.Parse(tbAnchoExtAcoplado.Text);
                                            e_pedido.Longitud_acoplado = float.Parse(tbLongitudAcoplado.Text);
                                            List<string> resp = n_pedido.modificar(e_pedido);
                                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    new frmMensajes("Error", ex.Message).ShowDialog();
                                }
                            }
                            else if (e_pedido.Estado.ToString().Trim() == "CARGADO" && e_viaje.Estado.ToString().Trim() != "EN TRANSCURSO")
                            {
                                try
                                {
                                    e_pedido.Id_chofer = n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim());
                                    e_pedido.Fecha_salida = Convert.ToDateTime(dtpSalida.Value.ToShortDateString());
                                    e_pedido.Fecha_regreso = Convert.ToDateTime(dtpRegreso.Value.ToShortDateString());
                                    List<string> resp = n_pedido.modificar(e_pedido);
                                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                                }
                                catch (Exception ex)
                                {
                                    new frmMensajes("Error", ex.Message).ShowDialog();
                                }
                            }
                            this.Hide();
                        }
                        catch (Exception ex)
                        {
                            new frmMensajes("Error", ex.Message).ShowDialog();
                        }
                    }
                }
                else
                {
                    if (chofer)
                    {
                        try
                        {
                            e_pedido = new E_Pedido();
                            if (n_pedido.listarTodos().Rows.Count > 0)
                                tbNumPedido.Text = (n_pedido.retornaIdUltimoPedido() + 1).ToString();
                            else
                                tbNumPedido.Text = (n_pedido.retornaIdUltimoPedido()).ToString();
                            e_pedido.Id_chofer = n_chofer.retornaNumLegajoChofer(cbChofer.SelectedItem.ToString().ToUpper().Trim());
                            e_pedido.Fecha_pedido = Convert.ToDateTime(dtpPedido.Value.ToShortDateString());
                            e_pedido.Fecha_salida = Convert.ToDateTime(dtpSalida.Value.ToShortDateString());
                            e_pedido.Fecha_regreso = Convert.ToDateTime(dtpRegreso.Value.ToShortDateString());
                            e_pedido.Peso_neto = float.Parse(tbPesoNeto.Text);
                            e_pedido.Volumen = float.Parse(tbVolumen.Text);
                            e_pedido.Km_viaje = float.Parse(tbKmViaje.Text);
                            e_pedido.Prioridad = cbPrioridad.SelectedItem.ToString().ToUpper();
                            cbEstado.Enabled = false;
                            e_pedido.Estado = cbEstado.SelectedItem.ToString().ToUpper();
                            e_pedido.Observaciones = tbObservaciones.Text;
                            e_pedido.Nombre_cliente = tbCliente.Text.ToUpper();
                            e_pedido.Cuil_cliente = tbCuilCliente.Text.ToUpper();
                            e_pedido.Enganche = radioButtonCheked();
                            e_pedido.Tipo_carga = n_tipo_carga.retornaTipoCarga(n_tipo_carga.retornaId(cbTipoCarga.SelectedItem.ToString())).Tipo.ToString();
                            if (e_pedido.Enganche == "ENGANCHE CON REMOLQUE")
                            {
                                if (string.IsNullOrWhiteSpace(tbAltCamion.Text) || string.IsNullOrWhiteSpace(tbAnchoIntCamion.Text)
                                    || string.IsNullOrWhiteSpace(tbAnchoExtCamion.Text) || string.IsNullOrWhiteSpace(tbLongitudCamion.Text)
                                    || string.IsNullOrWhiteSpace(tbAlturaAcoplado.Text) || string.IsNullOrWhiteSpace(tbAnchoIntAcoplado.Text)
                                    || string.IsNullOrWhiteSpace(tbAnchoExtAcoplado.Text) || string.IsNullOrWhiteSpace(tbLongitudAcoplado.Text))
                                    new frmMensajes("Error", "Por favor, debe insertar datos correspondiente a la unidad").ShowDialog();
                                else
                                {
                                    try
                                    {
                                        e_pedido.Altura_camion = float.Parse(tbAltCamion.Text);
                                        e_pedido.Ancho_interior_camion = float.Parse(tbAnchoIntCamion.Text);
                                        e_pedido.Ancho_exterior_camion = float.Parse(tbAnchoExtCamion.Text);
                                        e_pedido.Longitud_camion = float.Parse(tbLongitudCamion.Text);
                                        e_pedido.Altura_acoplado = float.Parse(tbAlturaAcoplado.Text);
                                        e_pedido.Ancho_interior_acoplado = float.Parse(tbAnchoIntAcoplado.Text);
                                        e_pedido.Ancho_exterior_acoplado = float.Parse(tbAnchoExtAcoplado.Text);
                                        e_pedido.Longitud_acoplado = float.Parse(tbLongitudAcoplado.Text);
                                        List<string> resp = n_pedido.insertar(e_pedido);
                                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }
                                }
                            }
                            else if (e_pedido.Enganche == "SIN ENGANCHE RIGIDO")
                            {
                                if (string.IsNullOrWhiteSpace(tbAltCamion.Text) || string.IsNullOrWhiteSpace(tbAnchoIntCamion.Text)
                                    || string.IsNullOrWhiteSpace(tbAnchoExtCamion.Text) || string.IsNullOrWhiteSpace(tbLongitudCamion.Text))
                                    new frmMensajes("Error", "Por favor, debe insertar datos correspondiente a la unidad").ShowDialog();
                                else
                                {
                                    try
                                    {
                                        e_pedido.Altura_camion = float.Parse(tbAltCamion.Text);
                                        e_pedido.Ancho_interior_camion = float.Parse(tbAnchoIntCamion.Text);
                                        e_pedido.Ancho_exterior_camion = float.Parse(tbAnchoExtCamion.Text);
                                        e_pedido.Longitud_camion = float.Parse(tbLongitudCamion.Text);
                                        List<string> resp = n_pedido.insertar(e_pedido);
                                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }
                                }
                            }
                            else if (e_pedido.Enganche == "SIN ENGANCHE SEMIREMOLQUE")
                            {
                                if (string.IsNullOrWhiteSpace(tbAlturaAcoplado.Text) || string.IsNullOrWhiteSpace(tbAnchoIntAcoplado.Text)
                                    || string.IsNullOrWhiteSpace(tbAnchoExtAcoplado.Text) || string.IsNullOrWhiteSpace(tbLongitudAcoplado.Text))
                                    new frmMensajes("Error", "Por favor, debe insertar datos correspondiente a la unidad").ShowDialog();
                                else
                                {
                                    try
                                    {
                                        e_pedido.Altura_acoplado = float.Parse(tbAlturaAcoplado.Text);
                                        e_pedido.Ancho_interior_acoplado = float.Parse(tbAnchoIntAcoplado.Text);
                                        e_pedido.Ancho_exterior_acoplado = float.Parse(tbAnchoExtAcoplado.Text);
                                        e_pedido.Longitud_acoplado = float.Parse(tbLongitudAcoplado.Text);
                                        List<string> resp = n_pedido.insertar(e_pedido);
                                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                                    }
                                    catch (Exception ex)
                                    {
                                        new frmMensajes("Error", ex.Message).ShowDialog();
                                    }
                                }
                            }
                            this.Hide();
                        }
                        catch (Exception ex)
                        {
                            new frmMensajes("Error", ex.Message).ShowDialog();
                        }
                    }
                    else
                        new frmMensajes("Error", "Chofer seleccionado ya se encuentra seleccionado para otro viaje en la fecha seleccionada").ShowDialog();
                }
                this.actualizaPedidos();
            }
        }
        private void cargaComboBoxs()
        {
            cbChofer.Items.Add("SELECCIONE CHOFER");
            cbChofer.SelectedIndex = 0;
            foreach (DataRow fila in n_chofer.listarTodos().Rows)
            {
                cbChofer.Items.Add(fila["nombre"].ToString() + ' ' + fila["apellido"].ToString());
            }
            cbPrioridad.Items.Add("SELECCIONE PRIORIDAD");
            cbPrioridad.SelectedIndex = 0;
            foreach (String fila in n_pedido.retornaPrioridades())
            {
                cbPrioridad.Items.Add(fila);
            }
            cbEstado.Items.Add("SELECCIONE ESTADO");
            foreach (String fila in n_pedido.retornaEstados())
            {
                cbEstado.Items.Add(fila);
            }
            cbEstado.SelectedIndex = 0;
            cbTipoCarga.Items.Add("SELECCIONE TIPO DE CARGA");
            cbTipoCarga.SelectedIndex = 0;
            foreach(DataRow fila in n_tipo_carga.listarHabilitados().Rows)
            {
                cbTipoCarga.Items.Add(fila["Tipo de carga"].ToString());
            }
        }
        private void cargaPedidoModificar(E_Pedido e_pedido)
        {
            if(e_pedido.Estado.ToString().Trim() == "PENDIENTE" || e_pedido.Estado.ToString().Trim() == "CANCELADO")
            {
                tbNumPedido.Text = e_pedido.Id_pedido.ToString();
                cbChofer.SelectedIndex = e_pedido.Id_chofer;
                dtpPedido.Value = e_pedido.Fecha_pedido;
                dtpPedido.Enabled = false;
                dtpSalida.Value = e_pedido.Fecha_salida;
                dtpRegreso.Value = e_pedido.Fecha_regreso;
                tbPesoNeto.Text = e_pedido.Peso_neto.ToString();
                tbVolumen.Text = e_pedido.Volumen.ToString();
                tbKmViaje.Text = e_pedido.Km_viaje.ToString();
                cbPrioridad.SelectedItem = e_pedido.Prioridad.ToString().Trim();
                cbEstado.SelectedItem = e_pedido.Estado.ToString().Trim();
                tbObservaciones.Text = e_pedido.Observaciones;
                tbCliente.Text = e_pedido.Nombre_cliente;
                tbCuilCliente.Text = e_pedido.Cuil_cliente;
                cbTipoCarga.SelectedItem = e_pedido.Tipo_carga;
                this.checkedRadioButton(e_pedido.Enganche);
                tbAltCamion.Text = e_pedido.Altura_camion.ToString();
                tbAnchoIntCamion.Text = e_pedido.Ancho_interior_camion.ToString();
                tbAnchoExtCamion.Text = e_pedido.Ancho_exterior_camion.ToString();
                tbLongitudCamion.Text = e_pedido.Longitud_camion.ToString();
                tbAlturaAcoplado.Text = e_pedido.Altura_acoplado.ToString();
                tbAnchoIntAcoplado.Text = e_pedido.Ancho_interior_acoplado.ToString();
                tbAnchoExtAcoplado.Text = e_pedido.Ancho_exterior_acoplado.ToString();
                tbLongitudAcoplado.Text = e_pedido.Longitud_acoplado.ToString();
            }
            else
            {
                tbNumPedido.Text = e_pedido.Id_pedido.ToString();
                cbChofer.SelectedItem = e_pedido.Id_chofer;
                dtpPedido.Value = e_pedido.Fecha_pedido;
                dtpSalida.Value = e_pedido.Fecha_salida;
                dtpRegreso.Value = e_pedido.Fecha_regreso;
                tbPesoNeto.Text = e_pedido.Peso_neto.ToString();
                tbVolumen.Text = e_pedido.Volumen.ToString();
                tbKmViaje.Text = e_pedido.Km_viaje.ToString();
                cbPrioridad.SelectedItem = e_pedido.Prioridad;
                cbEstado.SelectedItem = e_pedido.Estado;
                tbObservaciones.Text = e_pedido.Observaciones;
                tbCliente.Text = e_pedido.Nombre_cliente;
                tbCuilCliente.Text = e_pedido.Cuil_cliente;
                cbTipoCarga.SelectedItem = e_pedido.Tipo_carga;
                this.checkedRadioButton(e_pedido.Enganche);
                tbAltCamion.Text = e_pedido.Altura_camion.ToString();
                tbAnchoIntCamion.Text = e_pedido.Ancho_interior_camion.ToString();
                tbAnchoExtCamion.Text = e_pedido.Ancho_exterior_camion.ToString();
                tbLongitudCamion.Text = e_pedido.Longitud_camion.ToString();
                tbAlturaAcoplado.Text = e_pedido.Altura_acoplado.ToString();
                tbAnchoIntAcoplado.Text = e_pedido.Ancho_interior_acoplado.ToString();
                tbAnchoExtAcoplado.Text = e_pedido.Ancho_exterior_acoplado.ToString();
                tbLongitudAcoplado.Text = e_pedido.Longitud_acoplado.ToString();
                dtpPedido.Enabled = false;
                tbPesoNeto.Enabled = false;
                tbVolumen.Enabled = false;
                tbKmViaje.Enabled = false;
                cbEstado.Enabled = false;
                tbCliente.Enabled = false;
                tbCuilCliente.Enabled = false;
                cbTipoCarga.Enabled = false;
                tbAltCamion.Enabled = false;
                tbAnchoIntCamion.Enabled = false;
                tbAnchoExtCamion.Enabled = false;
                tbLongitudCamion.Enabled = false;
                tbAlturaAcoplado.Enabled = false;
                tbAnchoIntAcoplado.Enabled = false;
                tbAnchoExtAcoplado.Enabled = false;
                tbLongitudAcoplado.Enabled = false;
            }

        }

        private void tbPesoNeto_KeyPress(object sender, KeyPressEventArgs e)
        {
            funcion.solo_numero(sender, e);
        }

        private void tbCliente_KeyPress(object sender, KeyPressEventArgs e)
        {
            funcion.solo_letras(sender, e);
        }
    }
}
