﻿namespace CapaPresentacion.Operaciones
{
    partial class frmRol
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbEstado = new System.Windows.Forms.CheckBox();
            this.lbEstado = new System.Windows.Forms.Label();
            this.btnGuardarRol = new System.Windows.Forms.Button();
            this.lbRol_roles = new System.Windows.Forms.Label();
            this.tbRol = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbEstado
            // 
            this.cbEstado.AutoSize = true;
            this.cbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEstado.Location = new System.Drawing.Point(51, 40);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(15, 14);
            this.cbEstado.TabIndex = 37;
            this.cbEstado.UseVisualStyleBackColor = true;
            // 
            // lbEstado
            // 
            this.lbEstado.AutoSize = true;
            this.lbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstado.Location = new System.Drawing.Point(9, 41);
            this.lbEstado.Name = "lbEstado";
            this.lbEstado.Size = new System.Drawing.Size(42, 13);
            this.lbEstado.TabIndex = 36;
            this.lbEstado.Text = "Estado";
            // 
            // btnGuardarRol
            // 
            this.btnGuardarRol.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardarRol.Location = new System.Drawing.Point(140, 52);
            this.btnGuardarRol.Name = "btnGuardarRol";
            this.btnGuardarRol.Size = new System.Drawing.Size(130, 23);
            this.btnGuardarRol.TabIndex = 35;
            this.btnGuardarRol.Text = "GUARDAR";
            this.btnGuardarRol.UseVisualStyleBackColor = true;
            this.btnGuardarRol.Click += new System.EventHandler(this.btnGuardarRol_Click);
            // 
            // lbRol_roles
            // 
            this.lbRol_roles.AutoSize = true;
            this.lbRol_roles.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRol_roles.Location = new System.Drawing.Point(9, 11);
            this.lbRol_roles.Name = "lbRol_roles";
            this.lbRol_roles.Size = new System.Drawing.Size(23, 13);
            this.lbRol_roles.TabIndex = 33;
            this.lbRol_roles.Text = "Rol";
            // 
            // tbRol
            // 
            this.tbRol.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbRol.Location = new System.Drawing.Point(51, 8);
            this.tbRol.MaxLength = 100;
            this.tbRol.Name = "tbRol";
            this.tbRol.ShortcutsEnabled = false;
            this.tbRol.Size = new System.Drawing.Size(219, 21);
            this.tbRol.TabIndex = 34;
            this.tbRol.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbRol_KeyPress);
            // 
            // frmRol
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 87);
            this.Controls.Add(this.cbEstado);
            this.Controls.Add(this.lbEstado);
            this.Controls.Add(this.btnGuardarRol);
            this.Controls.Add(this.lbRol_roles);
            this.Controls.Add(this.tbRol);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRol";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alta y Modificación Roles - Operaciones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbEstado;
        private System.Windows.Forms.Label lbEstado;
        private System.Windows.Forms.Button btnGuardarRol;
        private System.Windows.Forms.Label lbRol_roles;
        private System.Windows.Forms.TextBox tbRol;
    }
}