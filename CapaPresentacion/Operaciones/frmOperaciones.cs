﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.Operaciones;
using CapaPresentacion._Reportes;
using CapaPresentacion.FormsMsj;

//NOTAS:
//1-asegurarme de que cuando cargue usuario nuevo camion o nuevo acoplado 
//haya marcas,modelos y tipos de carga
//2-controlar que haya seleccionado algo en los dgv de unidades (modificar,baja,ver detalles necesitan dominio de una fila)
namespace CapaPresentacion
{
    public delegate void actualizaGrillaUsuarios();
    public delegate void actualizaGrillaRoles();
    public delegate void actualizaGrillaChoferes();
    public delegate void actualizaGrillaPedidos();
    public partial class frmOperaciones : Form
    {


        public frmOperaciones(String usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            n_camion = new N_Camion(usuario);
            n_acoplado = new N_Acoplado(usuario);
            n_usuario = new N_Usuario(usuario);
            n_chofer = new N_Chofer(usuario);
            n_rol = new N_Rol(usuario);
            n_pedido = new N_Pedido(usuario);
            dgvUsuarios.DataSource = n_usuario.listar_usuarios();
            dgvRoles.DataSource = n_rol.listar_roles();
            dgvChofer.DataSource = n_chofer.listarTodos();
            carga_pedido_total();
            dtUsuarios = new DataTable();
            dtRoles = new DataTable();
            dtChofer = new DataTable();
            dtPedidos = new DataTable();
            dtCamion = new DataTable();
            dtAcoplado = new DataTable();
            funciones = new MisFunciones();
            actualizaUsuarios = delegate ()//metodo anonimo para actualizar lista de usuarios despues de agregar/modificar una unidad
            {
                dgvUsuarios.DataSource = n_usuario.listar_usuarios();
            };
            actualizaRoles = delegate ()//metodo anonimo para actualizar lista de roles despues de agregar/modificar una unidad
            {
                dgvRoles.DataSource = n_rol.listar_roles();
            };
            actualizaChoferes = delegate ()//metodo anonimo para actualizar lista de chofers despues de agregar/modificar una unidad
            {
                dgvChofer.DataSource = n_chofer.listarTodos();
            };
            actualizaPedidos = delegate ()//metodo anonimo para actualizar lista de pedidos despues de agregar/modificar una unidad
            {
                carga_pedido_total();
            };

            //viaje = new N_Viaje(usuario);
            //viaje.insertar();
        }
        /// <summary>
        /// PRUEBA INSERT VIAJE
        /// </summary>
        /// 
        //N_Viaje viaje;
        String usuario;
        N_Camion n_camion;
        N_Acoplado n_acoplado;
        N_Usuario n_usuario;
        N_Chofer n_chofer;
        N_Rol n_rol;
        N_Pedido n_pedido;
        DataTable dtUsuarios;
        DataTable dtRoles;
        DataTable dtChofer;
        DataTable dtPedidos;
        DataTable dtCamion;
        DataTable dtAcoplado;
        actualizaGrillaUsuarios actualizaUsuarios;
        actualizaGrillaRoles actualizaRoles;
        actualizaGrillaChoferes actualizaChoferes;
        actualizaGrillaPedidos actualizaPedidos;
        MisFunciones funciones;

        private void frmGerencia_Load(object sender, EventArgs e)
        {
            this.cargaTbBusqueda();
            dgvChofer.Columns[8].Visible = false;
            dgvChofer.Columns[9].Visible = false;
            #region unidades
            //SE CARGAN TODAS LAS UNIDADES
            cbAcopladoFiltrar.SelectedIndex = 0;
            cbCamionFiltros.SelectedIndex = 0;
            #region  carga dgv's
            dgvAcoplado.DataSource = n_acoplado.listarHabilitados ();//cargo el dgv con acoplados
            //carga_datos_label_camion();
            dgvCamion.DataSource = n_camion.listarHabilitados();//cargo el dgv con camiones
            columnas_camion_invisible();
            columnas_acoplado_invisible();
            #endregion
            funciones.esconderFiltroBusquedaFechaUnidades(true,pnlCamionBusquedaFecha,pnlCamionBusquedaTexto);//escondemos filtros camion
            funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);//escondemos filtros camion

            #endregion
            this.alinear_grillas();
        }

        private void alinear_grillas()
        {
            this.alinear_columnas_usuario();
            this.alinear_columnas_chofer();
            this.alinear_columnas_roles();
            this.alinear_columnas_pedidos();
            this.alinear_columnas_camion();
            this.alinear_columnas_acoplado();
        }

        private void cargaTbBusqueda()
        {
            if (tbBuscarUsuario.Text == String.Empty)
                tbBuscarUsuario.Text = "BUSCAR USUARIO..";
            if (tbBuscarRol.Text == String.Empty)
                tbBuscarRol.Text = "BUSCAR ROL..";
            if (tbBuscarChofer.Text == String.Empty)
                tbBuscarChofer.Text = "BUSCAR CHOFER..";
            if (tbBuscarPedido.Text == String.Empty)
                tbBuscarPedido.Text = "BUSCAR PEDIDO..";
        }
        #region Usuarios
        private void btnAltaUsuario_Click(object sender, EventArgs e)
        {
            new Operaciones.frmUsuario(usuario, null, actualizaUsuarios).ShowDialog();
        }
        private void btnModificarUsuario_Click(object sender, EventArgs e)
        {
            if (n_usuario.listar_usuarios().Rows.Count > 0)
                new Operaciones.frmUsuario(usuario, n_usuario.retornaUsuario(dgvUsuarios.CurrentRow.Cells[0].Value.ToString()), actualizaUsuarios).ShowDialog();
            else
                new frmMensajes("Error", "No hay registros para modificar").ShowDialog();
        }
        private void btnEliminarUsuario_Click(object sender, EventArgs e)
        {
            if (n_usuario.listar_usuarios().Rows.Count > 0)
            {
                E_Usuario e_usuario = n_usuario.retornaNumLegajo(Convert.ToInt32(dgvUsuarios.CurrentRow.Cells[2].Value));
                Boolean usuario_en_uso = n_usuario.enUso(Convert.ToInt32(dgvUsuarios.CurrentRow.Cells[2].Value));
                if (!usuario_en_uso)
                {
                    if (e_usuario.estado == "HABILITADO")
                    {
                        List<string> resp = n_usuario.baja(n_usuario.retornaUsuario(dgvUsuarios.CurrentRow.Cells[0].Value.ToString()));
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    else
                        new frmMensajes("Error", "Usuario seleccionado ya se encuentra deshabilitado").ShowDialog();
                }
                else
                    new frmMensajes("Error", "Usuario seleccionado esta logeado al sistema, no es posible desahabilitar").ShowDialog();
            }
            else
                new frmMensajes("Error", "No se poseen registros para eliminar").ShowDialog();
            dgvUsuarios.DataSource = n_usuario.listar_usuarios();
        }
        private void tbBuscarUsuario_TextChanged(object sender, EventArgs e)
        {
            if (tbBuscarUsuario.Text != "BUSCAR USUARIO..")
            {
                if (n_usuario.listar_usuarios().Rows.Count>0)
                {
                    dtUsuarios.Clear();
                    dtUsuarios = n_usuario.buscar_usuario(tbBuscarUsuario.Text).ToTable();
                    dgvUsuarios.DataSource = dtUsuarios;
                }
                else
                {
                    new frmMensajes("Error", "No se poseen registros para realizar la busqueda").ShowDialog();
                    dgvUsuarios.DataSource = null;
                    dgvUsuarios.DataSource = n_usuario.listar_usuarios();
                }
            }
        }
        private void tbBuscarUsuario_Click(object sender, EventArgs e)
        {
            tbBuscarUsuario.Text = "";
        }
        private void tbBuscarUsuario_Leave(object sender, EventArgs e)
        {
            if (tbBuscarUsuario.Text == String.Empty)
                tbBuscarUsuario.Text = "BUSCAR USUARIO..";
        }
        private void alinear_columnas_usuario()
        {
            dgvUsuarios.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvUsuarios.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvUsuarios.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvUsuarios.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvUsuarios.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        #endregion
        #region Roles
        private void btnAñadirRol_Click(object sender, EventArgs e)
        {
            new Operaciones.frmRol(usuario, null, actualizaRoles).ShowDialog();
        }
        private void btnModificarRol_Click(object sender, EventArgs e)
        {
            if (n_rol.listar_roles().Rows.Count > 0)
                new Operaciones.frmRol(usuario, n_rol.retornaRol(dgvRoles.CurrentRow.Cells[0].Value.ToString()), actualizaRoles).ShowDialog();
            else
                new frmMensajes("Error", "No se posee registros para modificar").ShowDialog();
        }
        private void btnEliminarRol_Click(object sender, EventArgs e)
        {
            E_Rol rol= n_rol.retornaRol(dgvRoles.CurrentRow.Cells[0].Value.ToString());
            if (n_rol.listar_roles().Rows.Count > 0)
            {
                int cantidad_usuarios_rol=n_rol.cantidad_usuarios_rol(dgvRoles.CurrentRow.Cells[0].Value.ToString());
                if(rol.estado == "HABILITADO")
                {
                    if (cantidad_usuarios_rol <= 0)
                    {
                        List<string> resp = n_rol.baja(rol);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    else
                        new frmMensajes("Error", "No es posible eliminar, rol seleccinado tiene usuarios asociados en uso").ShowDialog();
                }
                else
                    new frmMensajes("Error","Rol que intentas desahabilitar ya se encuentra deshabilitado").ShowDialog();
            }
            else
                new frmMensajes("Error", "No es posible realizar la acción, no hay registros para eliminar").ShowDialog();
            dgvRoles.DataSource = null;
            dgvRoles.DataSource = n_rol.listar_roles();
        }
        private void tbBuscarRol_TextChanged(object sender, EventArgs e)
        {

            if (tbBuscarRol.Text != "BUSCAR ROL..")
            {
                if (n_rol.listar_roles().Rows.Count > 0)
                {
                    dtRoles.Clear();
                    dtRoles = n_rol.buscar_rol(tbBuscarRol.Text).ToTable();
                    dgvRoles.DataSource = dtRoles;
                }
                else
                {
                    new frmMensajes("Error", "No es posible realizar la acción, no hay registros para realizar la busqueda").ShowDialog();
                    dgvRoles.DataSource = n_rol.listar_roles();
                }                    
            }
        }
        private void tbBuscarRol_Leave(object sender, EventArgs e)
        {
            if (tbBuscarRol.Text == String.Empty)
                tbBuscarRol.Text = "BUSCAR ROL..";
        }
        private void tbBuscarRol_Click(object sender, EventArgs e)
        {
            tbBuscarRol.Text = "";
        }
        private void alinear_columnas_roles()
        {
            dgvRoles.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvRoles.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        #endregion
        #region Chofer
        private void btnAñadirChofer_Click(object sender, EventArgs e)
        {
            new Operaciones.frmChofer(usuario, null, actualizaChoferes).ShowDialog();
        }
        private void btnModificarChofer_Click(object sender, EventArgs e)
        {
            if (n_chofer.listarTodos().Rows.Count > 0)
                new Operaciones.frmChofer(usuario, n_chofer.retornaChofer(dgvChofer.CurrentRow.Cells[0].Value.ToString()), actualizaChoferes).ShowDialog();
            else
                new frmMensajes("Error", "No se posee registros para modificar").ShowDialog();
        }
        private void btnEliminarChofer_Click(object sender, EventArgs e)
        {
            if (n_chofer.listarTodos().Rows.Count > 0)
            {
                E_Chofer e_chofer = n_chofer.retornaChofer(dgvChofer.CurrentRow.Cells[0].Value.ToString());
                if (n_chofer.chofer_en_viaje(e_chofer.Num_legajo) && e_chofer.Estado == "HABILITADO")
                {
                    List<string> resp = n_chofer.baja(e_chofer);
                    new frmMensajes(resp[0],resp[1]).ShowDialog();
                }
                else if(e_chofer.Estado == "DESHABILITADO")
                    new frmMensajes("Error", "Chofer selecconado ya se encuentra deshabilitado").ShowDialog();
                else
                    new frmMensajes("Error", "Chofer seleccionado se encuentra en uso").ShowDialog();
            }
            else
                new frmMensajes("Error", "No hay pedidos para eliminar").ShowDialog();
            dgvChofer.DataSource = null;
            dgvChofer.DataSource = n_chofer.listarTodos();
            dgvChofer.Columns[8].Visible = false;
            dgvChofer.Columns[9].Visible = false;
        }
        private void tbBuscarChofer_TextChanged(object sender, EventArgs e)
        {
            if (tbBuscarChofer.Text != "BUSCAR CHOFER..")
            {
                if (n_chofer.listarTodos().Rows.Count > 0)
                {
                    dtChofer.Clear();
                    if (n_chofer.buscar_chofer(tbBuscarChofer.Text) != null)
                    {
                        dtChofer = n_chofer.buscar_chofer(tbBuscarChofer.Text).ToTable();
                        dgvChofer.DataSource = dtChofer;
                        dgvChofer.Columns[8].Visible = false;
                        dgvChofer.Columns[9].Visible = false;
                    }
                }
                else
                {
                    new frmMensajes("Error", "No se poseen registros para realizar busqueda").ShowDialog();
                    dgvChofer.DataSource = n_chofer.listarTodos();
                }
            }       
        }
        private void tbBuscarChofer_Click(object sender, EventArgs e)
        {
            tbBuscarChofer.Text = "";
        }
        private void tbBuscarChofer_Leave(object sender, EventArgs e)
        {
            if (tbBuscarChofer.Text == String.Empty)
                tbBuscarChofer.Text = "BUSCAR CHOFER..";
        }
        private void alinear_columnas_chofer()
        {
            dgvChofer.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvChofer.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvChofer.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvChofer.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvChofer.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvChofer.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvChofer.Columns[6].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvChofer.Columns[7].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        #endregion
        #region Pedidos
        private void btnAñadirPedido_Click(object sender, EventArgs e)
        {
            new Operaciones.frmPedido(usuario, null, actualizaPedidos).ShowDialog();
        }
        private void btnModificarPedido_Click(object sender, EventArgs e)
        {
            if (n_pedido.listarTodos().Rows.Count > 0)
                new Operaciones.frmPedido(usuario, n_pedido.retornaPedido(Convert.ToInt32(dgvPedidos.CurrentRow.Cells[0].Value)), actualizaPedidos).ShowDialog();
            else
                new frmMensajes("Error", "No hay pedidos para modificar").ShowDialog();
            this.carga_pedido_total();
        }
        private void btnEliminarPedido_Click(object sender, EventArgs e)
        {
            if (n_pedido.listarTodos().Rows.Count > 0)
            {
                string estado=n_pedido.retornaEstado(Convert.ToInt32(dgvPedidos.CurrentRow.Cells[0].Value));
                if (estado == "CANCELADO")
                    new frmMensajes("Error", "Pedido seleccionado ya se encuentra cancelado").ShowDialog();
                else if(estado == "PENDIENTE")
                {
                    List<string> resp = n_pedido.baja(n_pedido.retornaPedido(Convert.ToInt32(dgvPedidos.CurrentRow.Cells[0].Value)));
                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                }
                else
                    new frmMensajes("Error", "No es posible eliminar pedido, el mismo ya fue cargado para un viaje").ShowDialog();
            }
            else
                new frmMensajes("Error", "No hay pedidos para eliminar").ShowDialog();
            this.carga_pedido_total();
        }
        private void carga_pedido_total()
        {
            dtPedidos = n_pedido.listarTodos();
            dgvPedidos.DataSource = dtPedidos;
            verificaDGVPedidosVacio();
            columnas_invisibles_pedido();
            carga_datos_label_pedido();
        }
        private void verificaDGVPedidosVacio()
        {
            if (dgvPedidos.Rows.Count < 1)
            {
                btnEliminarPedido.Enabled = false;
                btnModificarPedido.Enabled = false;

                dgvPedidos.BackgroundColor = Color.LightGray;
                limpiaTextboxBinding(); 
            }
            else
            {
                btnEliminarPedido.Enabled = true;
                btnModificarPedido.Enabled = true;
                dgvPedidos.BackgroundColor = Color.DarkGray;

            }

        }
        private void columnas_invisibles_pedido()
        {
            dgvPedidos.Columns[1].Visible = false;
            dgvPedidos.Columns[2].Visible = false;
            dgvPedidos.Columns[6].Visible = false;
            dgvPedidos.Columns[7].Visible = false;
            dgvPedidos.Columns[8].Visible = false;
            dgvPedidos.Columns[9].Visible = false;
            dgvPedidos.Columns[10].Visible = false;
            dgvPedidos.Columns[11].Visible = false;
            dgvPedidos.Columns[12].Visible = false;
            dgvPedidos.Columns[13].Visible = false;
            dgvPedidos.Columns[14].Visible = false;
            dgvPedidos.Columns[15].Visible = false;
            dgvPedidos.Columns[16].Visible = false;
            dgvPedidos.Columns[17].Visible = false;
            dgvPedidos.Columns[18].Visible = false;
            dgvPedidos.Columns[19].Visible = false;
            dgvPedidos.Columns[20].Visible = false;
            dgvPedidos.Columns[21].Visible = false;
            dgvPedidos.Columns[22].Visible = false;
            dgvPedidos.Columns[24].Visible = false;
        }
        private void carga_datos_label_pedido()
        {
            limpiaBindingPedidos();
            this.controlaBindingCount();
            if (dtPedidos != null)
            {
                
                lbNumLegajoChofer.DataBindings.Add(new Binding("Text", dtPedidos, "Nº Legajo"));
                lbNombreChoferPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Nombre chofer"));
                lbNombreClientePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Nombre cliente"));
                lbCuilClientePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Cuil"));
                lbEnganchePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Enganche"));
                lbPesoNetoPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Peso neto"));
                lbVolumenPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Volumen"));
                lbKmViajePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Km viaje"));
                lbTipoCarga.DataBindings.Add(new Binding("Text", dtPedidos, "Tipo carga"));
                lbAlturaCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Altura camion"));
                if (lbAlturaCamionPedido.Text == "0")
                    lbAlturaCamionPedido.Text = "-----";
                lbAnchoIntCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho interior camion"));
                if (lbAnchoIntCamionPedido.Text == "0")
                    lbAnchoIntCamionPedido.Text = "-----";
                lbAnchoExtCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho exterior camion"));
                if (lbAnchoExtCamionPedido.Text == "0")
                    lbAnchoExtCamionPedido.Text = "-----";
                lbLongitudCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Longitud camion"));
                if (lbLongitudCamionPedido.Text == "0")
                    lbLongitudCamionPedido.Text = "-----";
                lbAlturaRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Altura acoplado"));
                if (lbAlturaRemolquePedido.Text == "0")
                    lbAlturaRemolquePedido.Text = "-----";
                lbAnchoIntRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho interior acoplado"));
                if (lbAnchoIntRemolquePedido.Text == "0")
                    lbAnchoIntRemolquePedido.Text = "-----";
                lbAnchoExtRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho exterior acoplado"));
                if (lbAnchoExtRemolquePedido.Text == "0")
                    lbAnchoExtRemolquePedido.Text = "-----";
                lbLongitudRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Longitud acoplado"));
                if (lbLongitudRemolquePedido.Text == "0")
                    lbLongitudRemolquePedido.Text = "-----";
                lbObservacionesPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Observaciones"));
            }

        }
        private void controlaBindingCount()
        {
            if (lbNumLegajoChofer.DataBindings.Count == 0)
                lbNumLegajoChofer.Text = "-----";
            if (lbNombreChoferPedido.DataBindings.Count == 0)
                lbNombreChoferPedido.Text = "-----";
            if (lbNombreClientePedido.DataBindings.Count == 0)
                lbNombreClientePedido.Text = "-----";
            if (lbCuilClientePedido.DataBindings.Count == 0)
                lbCuilClientePedido.Text = "-----";
            if (lbEnganchePedido.DataBindings.Count == 0)
                lbEnganchePedido.Text = "-----";
            if (lbPesoNetoPedido.DataBindings.Count == 0)
                lbPesoNetoPedido.Text = "-----";
            if (lbVolumenPedido.DataBindings.Count == 0)
                lbVolumenPedido.Text = "-----";
            if (lbKmViajePedido.DataBindings.Count == 0)
                lbKmViajePedido.Text = "-----";
            if (lbTipoCarga.DataBindings.Count == 0)
                lbTipoCarga.Text = "-----";
            if (lbAlturaCamionPedido.DataBindings.Count == 0)
                lbAlturaCamionPedido.Text = "-----";
            if (lbAnchoIntCamionPedido.DataBindings.Count == 0)
                lbAnchoIntCamionPedido.Text = "-----";
            if (lbAnchoExtCamionPedido.DataBindings.Count == 0)
                lbAnchoExtCamionPedido.Text = "-----";
            if (lbLongitudCamionPedido.DataBindings.Count == 0)
                lbLongitudCamionPedido.Text = "-----";
            if (lbAlturaRemolquePedido.DataBindings.Count == 0)
                lbAlturaRemolquePedido.Text = "-----";
            if (lbAnchoIntRemolquePedido.DataBindings.Count == 0)
                lbAnchoIntRemolquePedido.Text = "-----";
            if (lbAnchoExtRemolquePedido.DataBindings.Count == 0)
                lbAnchoExtRemolquePedido.Text = "-----";
            if (lbLongitudRemolquePedido.DataBindings.Count == 0)
                lbLongitudRemolquePedido.Text = "-----";
            if (lbObservacionesPedido.DataBindings.Count == 0)
                lbObservacionesPedido.Text = "-----";

        }
        private void limpiaBindingPedidos()
        {
            lbNumLegajoChofer.DataBindings.Clear();
            lbNombreChoferPedido.DataBindings.Clear();
            lbNombreClientePedido.DataBindings.Clear();
            lbCuilClientePedido.DataBindings.Clear();
            lbEnganchePedido.DataBindings.Clear();
            lbPesoNetoPedido.DataBindings.Clear();
            lbVolumenPedido.DataBindings.Clear();
            lbKmViajePedido.DataBindings.Clear();
            lbTipoCarga.DataBindings.Clear();
            lbAlturaCamionPedido.DataBindings.Clear();
            lbAnchoIntCamionPedido.DataBindings.Clear();
            lbAnchoExtCamionPedido.DataBindings.Clear();
            lbLongitudCamionPedido.DataBindings.Clear();
            lbAlturaRemolquePedido.DataBindings.Clear();
            lbAnchoIntRemolquePedido.DataBindings.Clear();
            lbAnchoExtRemolquePedido.DataBindings.Clear();
            lbLongitudRemolquePedido.DataBindings.Clear();
            lbObservacionesPedido.DataBindings.Clear();
        }
        private void limpiaTextboxBinding()
        {
            lbNumLegajoChofer.Text = "-----";
            lbNombreChoferPedido.Text = "-----";
            lbNombreClientePedido.Text = "-----";
            lbCuilClientePedido.Text = "-----";
            lbEnganchePedido.Text = "-----";
            lbPesoNetoPedido.Text = "-----";
            lbVolumenPedido.Text = "-----";
            lbKmViajePedido.Text = "-----";
            lbTipoCarga.Text = "-----";
            lbAlturaCamionPedido.Text = "-----";
            lbAnchoIntCamionPedido.Text = "-----";
            lbAnchoExtCamionPedido.Text = "-----";
            lbLongitudCamionPedido.Text = "-----";
            lbAlturaRemolquePedido.Text = "-----";
            lbAnchoIntRemolquePedido.Text = "-----";
            lbAnchoExtRemolquePedido.Text = "-----";
            lbLongitudRemolquePedido.Text = "-----";
            lbObservacionesPedido.Text = "-----";
        }
        private void tbBuscarPedido_TextChanged(object sender, EventArgs e)
        {
            dtPedidos = new DataTable();
            if (tbBuscarPedido.Text != "BUSCAR PEDIDO..")
            {
                if (n_pedido.listarTodos().Rows.Count > 0)
                {
                    if(tbBuscarPedido.Text!=string.Empty)
                    {
                        dtPedidos.Clear();
                        dtPedidos = n_pedido.buscar_pedido(Convert.ToInt32(tbBuscarPedido.Text));
                        dgvPedidos.DataSource = dtPedidos;
                        columnas_invisibles_pedido();
                        carga_datos_label_pedido();
                    }
                    else
                    {
                        carga_pedido_total();
                    }
                }
                else
                {
                    new frmMensajes("Error", "No se poseen registros para realizar la busqueda").ShowDialog();
                    dgvPedidos.DataSource = n_pedido.listarTodos();
                }
            }
        }
        private void tbBuscarPedido_Click(object sender, EventArgs e)
        {
            tbBuscarPedido.Text = "";
        }
        private void tbBuscarPedido_Leave(object sender, EventArgs e)
        {
            if (tbBuscarPedido.Text == String.Empty)
                tbBuscarPedido.Text = "BUSCAR PEDIDO..";
        }
        private void dtpDesdePedido_ValueChanged(object sender, EventArgs e)
        {
            dtPedidos = new DataTable();
            if(dtpDesdePedido.Value <= dtpHastaPedido.Value.AddSeconds(60)){
                dtPedidos.Clear();
                dtPedidos = n_pedido.buscar_pedido_desde_hasta(dtpDesdePedido.Value.ToString("yyyy-MM-dd"),dtpHastaPedido.Value.ToString("yyyy-MM-dd"));
                dgvPedidos.DataSource = dtPedidos;
                columnas_invisibles_pedido();
                carga_datos_label_pedido();
                verificaDGVPedidosVacio();
            }
        }
        private void tbBuscarPedido_KeyPress(object sender, KeyPressEventArgs e)
        {
            funciones.solo_numero(sender, e);
        }

        private void alinear_columnas_pedidos()
        { 
            dgvPedidos.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[23].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }

        #endregion
        #region Administracion de unidades 
        #region  Camion
        void columnas_camion_invisible()
        {
            if (dgvCamion.Rows.Count >= 0)
            {
                dgvCamion.Columns[0].Visible = false;
                dgvCamion.Columns[5].Visible = false;
                dgvCamion.Columns[6].Visible = false;
                dgvCamion.Columns[7].Visible = false;
                dgvCamion.Columns[8].Visible = false;
                dgvCamion.Columns[9].Visible = false;
                dgvCamion.Columns[10].Visible = false;
                dgvCamion.Columns[13].Visible = false;
            }
        }
        //SE MANEJA ABM CAMION
        private void btnCamionNuevo_Click(object sender, EventArgs e)
        {
            List<string> resp = new List<string>();
            if (sender == btnCamionNuevo)
            {
                new frmNuevoCamion(this.usuario, null,false).ShowDialog();//parametro null porque voy a cargar un nuevo camion
                cbCamionFiltros.SelectedIndex = 1;
                dgvCamion.DataSource = null;
                dtCamion = n_camion.listarTodos();
                dgvCamion.DataSource = dtCamion;
                this.columnas_camion_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
            }
            if(sender== btnCamionModificar)
            {
                if (dgvCamion.Rows.Count > 0)
                {
                    if (dgvCamion.CurrentRow == null)
                    {
                        object camion = n_camion.retornaCamion(Convert.ToInt32(dgvCamion.Rows[0].Cells[0].Value.ToString()));
                        new frmNuevoCamion(usuario, camion, false).ShowDialog();
                    }
                    else
                    {
                        object camion = n_camion.retornaCamion(Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value));
                        new frmNuevoCamion(usuario, camion, false).ShowDialog();
                    }
                }
                else
                    new frmMensajes("Error", "No hay registros seleccionados para ver modificar").ShowDialog();
                cbCamionFiltros.SelectedIndex = 1;
                dgvCamion.DataSource = null;
                dtCamion = n_camion.listarTodos();
                dgvCamion.DataSource = dtCamion;
                this.columnas_camion_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
            }
            {
                if (sender == btnCamionVerDetalles)
                { 
                    if (dgvCamion.Rows.Count > 0)
                    {
                        if (dgvCamion.CurrentRow == null)
                        {
                            object camion = n_camion.retornaCamion(Convert.ToInt32(dgvCamion.Rows[0].Cells[0].Value.ToString()));
                            new frmNuevoCamion(usuario, camion, true).ShowDialog();
                        }
                        else
                        {
                            object camion = n_camion.retornaCamion(Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value));
                            new frmNuevoCamion(usuario, camion, true).ShowDialog();
                        }
                    }
                    else
                        new frmMensajes("Error", "No hay registros seleccionados para ver detalle").ShowDialog();

                    //el parametro true es porque deseo ver una unidad
                }
                if(sender == btnCamionEliminar)
                {
                    if (dgvCamion.Rows.Count > 0)
                    {
                        if (dgvCamion.CurrentRow == null)
                        {
                            E_Camion camion = (E_Camion)n_camion.retornaCamion(Convert.ToInt32(dgvCamion.Rows[0].Cells[0].Value.ToString()));
                            if (camion.Estado == "HABILITADO")
                            {
                                resp = n_camion.baja(Convert.ToInt32(dgvCamion.Rows[0].Cells[0].Value.ToString()));
                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                            }
                            else
                                new frmMensajes("Error", "Unidad seleccionada ya se encuentra deshabilitada").ShowDialog();
                        }
                        else
                        {
                            E_Camion camion = (E_Camion)n_camion.retornaCamion(Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value));
                            if (camion.Estado == "HABILITADO")
                            {
                                resp = n_camion.baja(Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value));
                                new frmMensajes(resp[0], resp[1]).ShowDialog();
                            }
                            else
                                new frmMensajes("Error", "Unidad seleccionada ya se encuentra deshabilitada").ShowDialog();
                        }
                    }
                    else
                        new frmMensajes("Error", "No hay registros seleccionados para eliminar").ShowDialog();



                    cbCamionFiltros.SelectedIndex = 1;
                    dgvCamion.DataSource = null;
                    dtCamion = n_camion.listarTodos();
                    dgvCamion.DataSource = dtCamion;
                    this.columnas_camion_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
                }
            }
        }
        //se abre el formulario de gestion de MARCA MODELO Y TIPO DE CARGA
        private void btnMarcaYtipo_Click(object sender, EventArgs e)
        {
            if (sender == btnCamionMarcaYtipo)
            {
                new frmCamionMarcaModeloTipo(this.usuario).ShowDialog();
            }
        }
        private void cbCamionFiltros_SelectedIndexChanged(object sender, EventArgs e)
        {
            //HABILITADOS        
            //TODOS
            //DADOS DE BAJA
            //FECHA DE ALTA
            //TRACTO CAMION
            //RIGIDO
           
            switch (cbCamionFiltros.SelectedIndex)
            {
                case 0:
                    dgvCamion.DataSource = null;
                    dtCamion = n_camion.listarHabilitados();
                    dgvCamion.DataSource = dtCamion;
                    this.columnas_camion_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades (true,pnlCamionBusquedaFecha,pnlCamionBusquedaTexto);
                    break;
                case 1:
                    dgvCamion.DataSource = null;
                    dtCamion = n_camion.listarTodos();
                    dgvCamion.DataSource = dtCamion;
                    this.columnas_camion_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
                    break;
                case 2:
                    dgvCamion.DataSource = null;
                    dtCamion = n_camion.listarDeshabilitados();
                    dgvCamion.DataSource = dtCamion;
                    this.columnas_camion_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
                    break;
                case 3:
                    dgvCamion.DataSource = null;
                    dtCamion = n_camion.listarTodos();
                    dgvCamion.DataSource = dtCamion;
                    this.columnas_camion_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(false, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
                    break;
            }
        }
        private void dgvCamion_CurrentCellChanged(object sender, EventArgs e)//esta evento es utilizado para habilitar o deshabilitar el boton "ver detalles"
        { 
            if (dgvCamion.CurrentRow != null)
            {
                if (!dgvCamion.CurrentRow.Cells[11].Value.ToString().Equals("tracto"))
                {
                    btnCamionVerDetalles.Visible = true;
                    btnCamionVerDetalles.Enabled = true;
                }
                else
                {
                    btnCamionVerDetalles.Visible = false;
                    btnCamionVerDetalles.Enabled = false;
                }
            }
        }
        private void tbCamionBuscarTexto_TextChanged(object sender, EventArgs e)
        {
            if (tbBusquedaCamion.Text.Length > 2)
                dgvCamion.DataSource = n_camion.busquedaEntradaDeTexto(tbBusquedaCamion.Text.ToString().ToUpper());
            else
            {
                cbCamionFiltros.SelectedIndex = 1;
                dgvCamion.DataSource = null;
                dtCamion = n_camion.listarTodos();
                dgvCamion.DataSource = dtCamion;
                this.columnas_camion_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlCamionBusquedaFecha, pnlCamionBusquedaTexto);
            }
        }
        private void dtpCamionBusquedaDesde_ValueChanged(object sender, EventArgs e)
        {
            if (dtpCamionBusquedaDesde.Value < dtpCamionBusquedaHasta.Value.AddSeconds(60))
            {
                dgvCamion.DataSource = n_camion.BusquedaPorRangoDeFechaDeAlta(dtpCamionBusquedaDesde.Value.ToShortDateString(), dtpCamionBusquedaHasta.Value.ToShortDateString());
            }
            else
            {
                dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                dtpCamionBusquedaHasta.Value = DateTime.Now;
                new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
            }

        }
        private void alinear_columnas_camion()
        {
            dgvCamion.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamion.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCamion.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCamion.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        #endregion
        #region Acoplado
        void columnas_acoplado_invisible()
        {
            if (dgvAcoplado.Rows.Count >=0 )
            {
                dgvAcoplado.Columns[0].Visible = false;
                dgvAcoplado.Columns[5].Visible = false;
                dgvAcoplado.Columns[6].Visible = false;
                dgvAcoplado.Columns[7].Visible = false;
                dgvAcoplado.Columns[8].Visible = false;
                dgvAcoplado.Columns[9].Visible = false;
                dgvAcoplado.Columns[10].Visible = false;
                dgvAcoplado.Columns[11].Visible = false;
                dgvAcoplado.Columns[12].Visible = false;
                dgvAcoplado.Columns[13].Visible = false;
                dgvAcoplado.Columns[14].Visible = false;
                dgvAcoplado.Columns[15].Visible = false;
                dgvAcoplado.Columns[16].Visible = false;
                dgvAcoplado.Columns[17].Visible = false;
                dgvAcoplado.Columns[18].Visible = false;

            }
        }
        private void btnAcopladoNuevo_Click(object sender, EventArgs e)
        {
            List<string> resp = new List<string>();
            if (sender == btnAcopladoNuevo)
            {
                new frmNuevoAcoplado(usuario,null,false).ShowDialog();
                cbAcopladoFiltrar.SelectedIndex = 1;
                dgvAcoplado.DataSource = null;
                dtAcoplado = n_acoplado.listarTodos();
                dgvAcoplado.DataSource = dtAcoplado;
                this.columnas_acoplado_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
            }
            if (sender==btnAcopladoModificar)
            {
                if (dgvAcoplado.Rows.Count > 0)
                {
                    if (dgvAcoplado.CurrentRow == null)
                    {
                        object acoplado = n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.Rows[0].Cells[0].Value.ToString()));
                        new frmNuevoAcoplado(usuario, acoplado, false).ShowDialog();
                    }
                    else
                    {
                        object acoplado = n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.CurrentRow.Cells[0].Value));
                        new frmNuevoAcoplado(usuario, acoplado, false).ShowDialog();
                    }
                }
                else
                    new frmMensajes("Error", "No hay registros seleccionados para ver modificar").ShowDialog();
                cbAcopladoFiltrar.SelectedIndex = 1;
                dgvAcoplado.DataSource = null;
                dtAcoplado = n_acoplado.listarTodos();
                dgvAcoplado.DataSource = dtAcoplado;
                this.columnas_acoplado_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
            }
            if (sender==btnAcopladoEliminar)
            {
                if (dgvAcoplado.Rows.Count > 0)
                {
                    if (dgvAcoplado.CurrentRow == null)
                    {
                        E_Acoplado acoplado = (E_Acoplado)n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.Rows[0].Cells[0].Value.ToString()));
                        if (acoplado.Estado == "HABILITADO")
                        {
                            resp = n_acoplado.baja(Convert.ToInt32(dgvAcoplado.Rows[0].Cells[0].Value.ToString()));
                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                        }
                        else
                            new frmMensajes("Error", "Unidad seleccionada ya se encuentra deshabilitada").ShowDialog();
                    }
                    else
                    {
                        E_Acoplado acoplado = (E_Acoplado)n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.CurrentRow.Cells[0].Value));
                        if (acoplado.Estado == "HABILITADO")
                        {
                            resp = n_acoplado.baja(Convert.ToInt32(dgvAcoplado.CurrentRow.Cells[0].Value));
                            new frmMensajes(resp[0], resp[1]).ShowDialog();
                        }
                        else
                            new frmMensajes("Error", "Unidad seleccionada ya se encuentra deshabilitada").ShowDialog();
                    }
                }
                else
                    new frmMensajes("Error", "No hay registros seleccionados para eliminar").ShowDialog();
                cbAcopladoFiltrar.SelectedIndex = 1;
                dgvAcoplado.DataSource = null;
                dtAcoplado = n_acoplado.listarTodos();
                dgvAcoplado.DataSource = dtAcoplado;
                this.columnas_acoplado_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
            }
            if (btnAcopladoVerDetalle==sender)
            {
                if(dgvAcoplado.Rows.Count > 0)
                {
                    if (dgvAcoplado.CurrentRow == null)
                    {
                        object acoplado = n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.Rows[0].Cells[0].Value.ToString()));
                        new frmNuevoAcoplado(usuario, acoplado, true).ShowDialog();
                    }
                    else
                    {
                        object acoplado = n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.CurrentRow.Cells[0].Value));
                        new frmNuevoAcoplado(usuario, acoplado, true).ShowDialog();
                    }
                }
                else
                    new frmMensajes("Error", "No hay registros seleccionados para ver detalle").ShowDialog();
            }

        }
        private void btnAcopladoMarcaYtipo_Click(object sender, EventArgs e)
        {
            new frmAcopladoMarcaModeloTipo(usuario).ShowDialog();
        }
        private void cbAcopladoFiltrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            //HABILITADOS
            //TODOS
            //DADOS DE BAJA
            //FECHA DE ALTA
            switch (cbAcopladoFiltrar.SelectedIndex)
            {
                case 0: 
                    dgvAcoplado.DataSource = null;
                    dtAcoplado = n_acoplado.listarHabilitados();
                    dgvAcoplado.DataSource = dtAcoplado;
                    this.columnas_acoplado_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
                    break;
                case 1:
                    dgvAcoplado.DataSource = null;
                    dtAcoplado = n_acoplado.listarTodos();
                    dgvAcoplado.DataSource = dtAcoplado;
                    this.columnas_acoplado_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
                    break;
                case 2:
                    dgvAcoplado.DataSource = null;
                    dtAcoplado = n_acoplado.listarDeshabilitados();
                    dgvAcoplado.DataSource = dtAcoplado;
                    this.columnas_acoplado_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
                    break;
                case 3:
                    dgvAcoplado.DataSource = null;
                    dtAcoplado = n_acoplado.listarTodos();
                    dgvAcoplado.DataSource = dtAcoplado;
                    this.columnas_acoplado_invisible();
                    funciones.esconderFiltroBusquedaFechaUnidades(false, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
                    break;
          
            }
        }
        private void tbBusquedaUnidad_TextChanged(object sender, EventArgs e)
        {
            if (tbBusquedaAcoplado.Text.Length > 2)
            {
                dgvAcoplado.DataSource = n_acoplado.busquedaPorEntradaDeTexto(tbBusquedaAcoplado.Text);
            }
            else
            {
                dgvAcoplado.DataSource = null;
                dtAcoplado = n_acoplado.listarTodos();
                dgvAcoplado.DataSource = dtAcoplado;
                this.columnas_acoplado_invisible();
                funciones.esconderFiltroBusquedaFechaUnidades(true, pnlAcopladoBusquedaFecha, pnlAcopladoBusquedaTexto);
            }
        }
        private void dtpAcopladoBusquedaDesde_ValueChanged(object sender, EventArgs e)//busqueda rango de fechas de alta
        {
            if (dtpAcopladoBusquedaDesde.Value < dtpAcopladoBusquedaHasta.Value.AddSeconds(60))
            {
                dgvAcoplado.DataSource = n_acoplado.BusquedaPorRangoDeFechaDeAlta(dtpAcopladoBusquedaDesde.Value.ToShortDateString(), dtpAcopladoBusquedaHasta.Value.ToShortDateString());
            }
            else
            {
                dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
            }
        }

        private void alinear_columnas_acoplado()
        {
            dgvAcoplado.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcoplado.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAcoplado.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAcoplado.Columns[20].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
        }
        #endregion
        #region Tipo Carga Ambos

        private void btnTipoCargaCamion_Click(object sender, EventArgs e)
        {
            if (sender == btnTipoCargaCamion || sender == btnTipoCargaAcoplado)
            {
                new frmTipoCargaUnidades(this.usuario).ShowDialog();
            }
        }

        #endregion

        #endregion
        #region Reportes
        private void btnReporte1_Click(object sender, EventArgs e)
        {
            if (sender == btnReporte1)
            {
                frmReporte frmReprote = new frmReporte(1);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte2)
            {
                frmReporte frmReprote = new frmReporte(2);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte3)
            {
                frmReporte frmReprote = new frmReporte(3);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte4)
            {
                frmReporte frmReprote = new frmReporte(4);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte5)
            {
                frmReporte frmReprote = new frmReporte(5);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte6)
            {
                frmReporte frmReprote = new frmReporte(6);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte7)
            {
                frmReporte frmReprote = new frmReporte(7);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte8)
            {
                frmReporte frmReprote = new frmReporte(8);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte9)
            {
                frmReporte frmReprote = new frmReporte(9);
                frmReprote.ShowDialog();
            }
            if (sender == btnReporte10)
            {
                frmReporte frmReprote = new frmReporte(10);
                frmReprote.ShowDialog();
            }
        }



        #endregion

        private void frmOperaciones_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }
    }
}
 