﻿namespace CapaPresentacion.Operaciones
{
    partial class frmNuevoAcoplado
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbModelo = new System.Windows.Forms.ComboBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.tbVolumen = new System.Windows.Forms.TextBox();
            this.lblVolumen = new System.Windows.Forms.Label();
            this.tbAltura = new System.Windows.Forms.TextBox();
            this.lblAltura = new System.Windows.Forms.Label();
            this.tbCapacidad = new System.Windows.Forms.TextBox();
            this.lblAnchoInterior = new System.Windows.Forms.Label();
            this.lblCapacidad = new System.Windows.Forms.Label();
            this.tbAnchoInterior = new System.Windows.Forms.TextBox();
            this.tbLongitud = new System.Windows.Forms.TextBox();
            this.lblAnchoExterior = new System.Windows.Forms.Label();
            this.lblLongitud = new System.Windows.Forms.Label();
            this.tbAnchoExterior = new System.Windows.Forms.TextBox();
            this.cbMarca = new System.Windows.Forms.ComboBox();
            this.tbKmService = new System.Windows.Forms.TextBox();
            this.lbKmService = new System.Windows.Forms.Label();
            this.tbKmReales = new System.Windows.Forms.TextBox();
            this.lblKmReales = new System.Windows.Forms.Label();
            this.lblTara = new System.Windows.Forms.Label();
            this.tbAño = new System.Windows.Forms.TextBox();
            this.lblAño = new System.Windows.Forms.Label();
            this.lblModelo = new System.Windows.Forms.Label();
            this.lblMarca = new System.Windows.Forms.Label();
            this.tbDominio = new System.Windows.Forms.TextBox();
            this.lblDominio = new System.Windows.Forms.Label();
            this.lblTipoCarga = new System.Windows.Forms.Label();
            this.cbTipoCarga = new System.Windows.Forms.ComboBox();
            this.tbTara = new System.Windows.Forms.TextBox();
            this.lblTipoAcoplado = new System.Windows.Forms.Label();
            this.btnSalir = new System.Windows.Forms.Button();
            this.tbKmCambioNeumaticos = new System.Windows.Forms.TextBox();
            this.lbKmCambioNeumaticos = new System.Windows.Forms.Label();
            this.rbRemolque = new System.Windows.Forms.RadioButton();
            this.rbSemiremolque = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbModelo
            // 
            this.cbModelo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbModelo.FormattingEnabled = true;
            this.cbModelo.Location = new System.Drawing.Point(107, 58);
            this.cbModelo.Name = "cbModelo";
            this.cbModelo.Size = new System.Drawing.Size(174, 21);
            this.cbModelo.TabIndex = 4;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(316, 212);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(116, 22);
            this.btnOk.TabIndex = 18;
            this.btnOk.Text = "Guardar";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // tbVolumen
            // 
            this.tbVolumen.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbVolumen.Location = new System.Drawing.Point(107, 214);
            this.tbVolumen.MaxLength = 6;
            this.tbVolumen.Name = "tbVolumen";
            this.tbVolumen.ShortcutsEnabled = false;
            this.tbVolumen.Size = new System.Drawing.Size(174, 21);
            this.tbVolumen.TabIndex = 10;
            this.tbVolumen.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblVolumen
            // 
            this.lblVolumen.AutoSize = true;
            this.lblVolumen.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblVolumen.Location = new System.Drawing.Point(12, 217);
            this.lblVolumen.Name = "lblVolumen";
            this.lblVolumen.Size = new System.Drawing.Size(51, 13);
            this.lblVolumen.TabIndex = 49;
            this.lblVolumen.Text = "Volumen:";
            // 
            // tbAltura
            // 
            this.tbAltura.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAltura.Location = new System.Drawing.Point(412, 84);
            this.tbAltura.MaxLength = 6;
            this.tbAltura.Name = "tbAltura";
            this.tbAltura.ShortcutsEnabled = false;
            this.tbAltura.Size = new System.Drawing.Size(156, 21);
            this.tbAltura.TabIndex = 14;
            this.tbAltura.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAltura
            // 
            this.lblAltura.AutoSize = true;
            this.lblAltura.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAltura.Location = new System.Drawing.Point(287, 87);
            this.lblAltura.Name = "lblAltura";
            this.lblAltura.Size = new System.Drawing.Size(40, 13);
            this.lblAltura.TabIndex = 36;
            this.lblAltura.Text = "Altura:";
            // 
            // tbCapacidad
            // 
            this.tbCapacidad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbCapacidad.Location = new System.Drawing.Point(107, 188);
            this.tbCapacidad.MaxLength = 6;
            this.tbCapacidad.Name = "tbCapacidad";
            this.tbCapacidad.ShortcutsEnabled = false;
            this.tbCapacidad.Size = new System.Drawing.Size(174, 21);
            this.tbCapacidad.TabIndex = 9;
            this.tbCapacidad.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAnchoInterior
            // 
            this.lblAnchoInterior.AutoSize = true;
            this.lblAnchoInterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnchoInterior.Location = new System.Drawing.Point(287, 113);
            this.lblAnchoInterior.Name = "lblAnchoInterior";
            this.lblAnchoInterior.Size = new System.Drawing.Size(78, 13);
            this.lblAnchoInterior.TabIndex = 37;
            this.lblAnchoInterior.Text = "Ancho Interior:";
            // 
            // lblCapacidad
            // 
            this.lblCapacidad.AutoSize = true;
            this.lblCapacidad.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCapacidad.Location = new System.Drawing.Point(12, 191);
            this.lblCapacidad.Name = "lblCapacidad";
            this.lblCapacidad.Size = new System.Drawing.Size(60, 13);
            this.lblCapacidad.TabIndex = 43;
            this.lblCapacidad.Text = "Capacidad:";
            // 
            // tbAnchoInterior
            // 
            this.tbAnchoInterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoInterior.Location = new System.Drawing.Point(412, 110);
            this.tbAnchoInterior.MaxLength = 6;
            this.tbAnchoInterior.Name = "tbAnchoInterior";
            this.tbAnchoInterior.ShortcutsEnabled = false;
            this.tbAnchoInterior.Size = new System.Drawing.Size(156, 21);
            this.tbAnchoInterior.TabIndex = 15;
            this.tbAnchoInterior.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // tbLongitud
            // 
            this.tbLongitud.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbLongitud.Location = new System.Drawing.Point(412, 162);
            this.tbLongitud.MaxLength = 6;
            this.tbLongitud.Name = "tbLongitud";
            this.tbLongitud.ShortcutsEnabled = false;
            this.tbLongitud.Size = new System.Drawing.Size(156, 21);
            this.tbLongitud.TabIndex = 17;
            this.tbLongitud.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAnchoExterior
            // 
            this.lblAnchoExterior.AutoSize = true;
            this.lblAnchoExterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAnchoExterior.Location = new System.Drawing.Point(287, 139);
            this.lblAnchoExterior.Name = "lblAnchoExterior";
            this.lblAnchoExterior.Size = new System.Drawing.Size(82, 13);
            this.lblAnchoExterior.TabIndex = 39;
            this.lblAnchoExterior.Text = "Ancho Exterior:";
            // 
            // lblLongitud
            // 
            this.lblLongitud.AutoSize = true;
            this.lblLongitud.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLongitud.Location = new System.Drawing.Point(287, 165);
            this.lblLongitud.Name = "lblLongitud";
            this.lblLongitud.Size = new System.Drawing.Size(52, 13);
            this.lblLongitud.TabIndex = 41;
            this.lblLongitud.Text = "Longitud:";
            // 
            // tbAnchoExterior
            // 
            this.tbAnchoExterior.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnchoExterior.Location = new System.Drawing.Point(412, 136);
            this.tbAnchoExterior.MaxLength = 6;
            this.tbAnchoExterior.Name = "tbAnchoExterior";
            this.tbAnchoExterior.ShortcutsEnabled = false;
            this.tbAnchoExterior.Size = new System.Drawing.Size(156, 21);
            this.tbAnchoExterior.TabIndex = 16;
            this.tbAnchoExterior.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // cbMarca
            // 
            this.cbMarca.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMarca.FormattingEnabled = true;
            this.cbMarca.Location = new System.Drawing.Point(107, 32);
            this.cbMarca.Name = "cbMarca";
            this.cbMarca.Size = new System.Drawing.Size(174, 21);
            this.cbMarca.TabIndex = 3;
            this.cbMarca.SelectedIndexChanged += new System.EventHandler(this.cbMarca_SelectedIndexChanged);
            // 
            // tbKmService
            // 
            this.tbKmService.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKmService.Location = new System.Drawing.Point(412, 32);
            this.tbKmService.MaxLength = 6;
            this.tbKmService.Name = "tbKmService";
            this.tbKmService.ShortcutsEnabled = false;
            this.tbKmService.Size = new System.Drawing.Size(156, 21);
            this.tbKmService.TabIndex = 12;
            this.tbKmService.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lbKmService
            // 
            this.lbKmService.AutoSize = true;
            this.lbKmService.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKmService.Location = new System.Drawing.Point(287, 35);
            this.lbKmService.Name = "lbKmService";
            this.lbKmService.Size = new System.Drawing.Size(63, 13);
            this.lbKmService.TabIndex = 53;
            this.lbKmService.Text = "Km Service";
            // 
            // tbKmReales
            // 
            this.tbKmReales.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKmReales.Location = new System.Drawing.Point(412, 6);
            this.tbKmReales.MaxLength = 6;
            this.tbKmReales.Name = "tbKmReales";
            this.tbKmReales.ShortcutsEnabled = false;
            this.tbKmReales.Size = new System.Drawing.Size(156, 21);
            this.tbKmReales.TabIndex = 11;
            this.tbKmReales.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblKmReales
            // 
            this.lblKmReales.AutoSize = true;
            this.lblKmReales.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKmReales.Location = new System.Drawing.Point(287, 9);
            this.lblKmReales.Name = "lblKmReales";
            this.lblKmReales.Size = new System.Drawing.Size(61, 13);
            this.lblKmReales.TabIndex = 51;
            this.lblKmReales.Text = "Km Reales:";
            // 
            // lblTara
            // 
            this.lblTara.AutoSize = true;
            this.lblTara.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTara.Location = new System.Drawing.Point(12, 165);
            this.lblTara.Name = "lblTara";
            this.lblTara.Size = new System.Drawing.Size(31, 13);
            this.lblTara.TabIndex = 49;
            this.lblTara.Text = "Tara:";
            // 
            // tbAño
            // 
            this.tbAño.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAño.Location = new System.Drawing.Point(107, 84);
            this.tbAño.MaxLength = 4;
            this.tbAño.Name = "tbAño";
            this.tbAño.ShortcutsEnabled = false;
            this.tbAño.Size = new System.Drawing.Size(174, 21);
            this.tbAño.TabIndex = 5;
            this.tbAño.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblAño
            // 
            this.lblAño.AutoSize = true;
            this.lblAño.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAño.Location = new System.Drawing.Point(12, 87);
            this.lblAño.Name = "lblAño";
            this.lblAño.Size = new System.Drawing.Size(28, 13);
            this.lblAño.TabIndex = 47;
            this.lblAño.Text = "Año:";
            // 
            // lblModelo
            // 
            this.lblModelo.AutoSize = true;
            this.lblModelo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblModelo.Location = new System.Drawing.Point(12, 61);
            this.lblModelo.Name = "lblModelo";
            this.lblModelo.Size = new System.Drawing.Size(45, 13);
            this.lblModelo.TabIndex = 46;
            this.lblModelo.Text = "Modelo:";
            // 
            // lblMarca
            // 
            this.lblMarca.AutoSize = true;
            this.lblMarca.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarca.Location = new System.Drawing.Point(12, 35);
            this.lblMarca.Name = "lblMarca";
            this.lblMarca.Size = new System.Drawing.Size(40, 13);
            this.lblMarca.TabIndex = 45;
            this.lblMarca.Text = "Marca:";
            // 
            // tbDominio
            // 
            this.tbDominio.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbDominio.Location = new System.Drawing.Point(107, 6);
            this.tbDominio.MaxLength = 7;
            this.tbDominio.Name = "tbDominio";
            this.tbDominio.ShortcutsEnabled = false;
            this.tbDominio.Size = new System.Drawing.Size(174, 21);
            this.tbDominio.TabIndex = 1;
            // 
            // lblDominio
            // 
            this.lblDominio.AutoSize = true;
            this.lblDominio.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDominio.Location = new System.Drawing.Point(12, 9);
            this.lblDominio.Name = "lblDominio";
            this.lblDominio.Size = new System.Drawing.Size(49, 13);
            this.lblDominio.TabIndex = 39;
            this.lblDominio.Text = "Dominio:";
            // 
            // lblTipoCarga
            // 
            this.lblTipoCarga.AutoSize = true;
            this.lblTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoCarga.Location = new System.Drawing.Point(12, 113);
            this.lblTipoCarga.Name = "lblTipoCarga";
            this.lblTipoCarga.Size = new System.Drawing.Size(73, 13);
            this.lblTipoCarga.TabIndex = 34;
            this.lblTipoCarga.Text = "Tipo de carga:";
            // 
            // cbTipoCarga
            // 
            this.cbTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbTipoCarga.FormattingEnabled = true;
            this.cbTipoCarga.Location = new System.Drawing.Point(107, 110);
            this.cbTipoCarga.Name = "cbTipoCarga";
            this.cbTipoCarga.Size = new System.Drawing.Size(174, 21);
            this.cbTipoCarga.TabIndex = 6;
            // 
            // tbTara
            // 
            this.tbTara.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTara.Location = new System.Drawing.Point(107, 162);
            this.tbTara.MaxLength = 6;
            this.tbTara.Name = "tbTara";
            this.tbTara.ShortcutsEnabled = false;
            this.tbTara.Size = new System.Drawing.Size(174, 21);
            this.tbTara.TabIndex = 8;
            this.tbTara.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lblTipoAcoplado
            // 
            this.lblTipoAcoplado.AutoSize = true;
            this.lblTipoAcoplado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoAcoplado.Location = new System.Drawing.Point(12, 139);
            this.lblTipoAcoplado.Name = "lblTipoAcoplado";
            this.lblTipoAcoplado.Size = new System.Drawing.Size(90, 13);
            this.lblTipoAcoplado.TabIndex = 59;
            this.lblTipoAcoplado.Text = "Tipo de Acoplado:";
            // 
            // btnSalir
            // 
            this.btnSalir.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalir.Location = new System.Drawing.Point(450, 212);
            this.btnSalir.Name = "btnSalir";
            this.btnSalir.Size = new System.Drawing.Size(116, 22);
            this.btnSalir.TabIndex = 19;
            this.btnSalir.Text = "Salir";
            this.btnSalir.UseVisualStyleBackColor = true;
            this.btnSalir.Click += new System.EventHandler(this.btnSalir_Click);
            // 
            // tbKmCambioNeumaticos
            // 
            this.tbKmCambioNeumaticos.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKmCambioNeumaticos.Location = new System.Drawing.Point(412, 58);
            this.tbKmCambioNeumaticos.MaxLength = 6;
            this.tbKmCambioNeumaticos.Name = "tbKmCambioNeumaticos";
            this.tbKmCambioNeumaticos.ShortcutsEnabled = false;
            this.tbKmCambioNeumaticos.Size = new System.Drawing.Size(156, 21);
            this.tbKmCambioNeumaticos.TabIndex = 13;
            this.tbKmCambioNeumaticos.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbAño_KeyPress);
            // 
            // lbKmCambioNeumaticos
            // 
            this.lbKmCambioNeumaticos.AutoSize = true;
            this.lbKmCambioNeumaticos.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKmCambioNeumaticos.Location = new System.Drawing.Point(287, 61);
            this.lbKmCambioNeumaticos.Name = "lbKmCambioNeumaticos";
            this.lbKmCambioNeumaticos.Size = new System.Drawing.Size(123, 13);
            this.lbKmCambioNeumaticos.TabIndex = 64;
            this.lbKmCambioNeumaticos.Text = "Km Cambio Neumaticos";
            // 
            // rbRemolque
            // 
            this.rbRemolque.AutoSize = true;
            this.rbRemolque.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbRemolque.Location = new System.Drawing.Point(3, 3);
            this.rbRemolque.Name = "rbRemolque";
            this.rbRemolque.Size = new System.Drawing.Size(74, 17);
            this.rbRemolque.TabIndex = 65;
            this.rbRemolque.Text = "Remolque";
            this.rbRemolque.UseVisualStyleBackColor = true;
            // 
            // rbSemiremolque
            // 
            this.rbSemiremolque.AutoSize = true;
            this.rbSemiremolque.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbSemiremolque.Location = new System.Drawing.Point(83, 3);
            this.rbSemiremolque.Name = "rbSemiremolque";
            this.rbSemiremolque.Size = new System.Drawing.Size(97, 17);
            this.rbSemiremolque.TabIndex = 66;
            this.rbSemiremolque.Text = "Semiremolque";
            this.rbSemiremolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.rbSemiremolque.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbRemolque);
            this.panel1.Controls.Add(this.rbSemiremolque);
            this.panel1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(107, 136);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(174, 22);
            this.panel1.TabIndex = 67;
            // 
            // frmNuevoAcoplado
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(578, 239);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tbKmCambioNeumaticos);
            this.Controls.Add(this.lbKmCambioNeumaticos);
            this.Controls.Add(this.btnSalir);
            this.Controls.Add(this.lblTipoAcoplado);
            this.Controls.Add(this.tbVolumen);
            this.Controls.Add(this.cbModelo);
            this.Controls.Add(this.lblVolumen);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tbAltura);
            this.Controls.Add(this.cbTipoCarga);
            this.Controls.Add(this.lblAltura);
            this.Controls.Add(this.tbCapacidad);
            this.Controls.Add(this.cbMarca);
            this.Controls.Add(this.lblAnchoInterior);
            this.Controls.Add(this.lblTipoCarga);
            this.Controls.Add(this.lblCapacidad);
            this.Controls.Add(this.tbKmService);
            this.Controls.Add(this.tbAnchoInterior);
            this.Controls.Add(this.lbKmService);
            this.Controls.Add(this.tbLongitud);
            this.Controls.Add(this.tbKmReales);
            this.Controls.Add(this.lblAnchoExterior);
            this.Controls.Add(this.lblKmReales);
            this.Controls.Add(this.lblLongitud);
            this.Controls.Add(this.tbTara);
            this.Controls.Add(this.tbAnchoExterior);
            this.Controls.Add(this.lblTara);
            this.Controls.Add(this.tbAño);
            this.Controls.Add(this.lblAño);
            this.Controls.Add(this.lblModelo);
            this.Controls.Add(this.lblMarca);
            this.Controls.Add(this.tbDominio);
            this.Controls.Add(this.lblDominio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNuevoAcoplado";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alta y Modificación Acoplado - Operaciones";
            this.Load += new System.EventHandler(this.frmNuevoAcoplado_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbModelo;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.TextBox tbVolumen;
        private System.Windows.Forms.Label lblVolumen;
        private System.Windows.Forms.TextBox tbAltura;
        private System.Windows.Forms.Label lblAltura;
        private System.Windows.Forms.TextBox tbCapacidad;
        private System.Windows.Forms.Label lblAnchoInterior;
        private System.Windows.Forms.Label lblCapacidad;
        private System.Windows.Forms.TextBox tbAnchoInterior;
        private System.Windows.Forms.TextBox tbLongitud;
        private System.Windows.Forms.Label lblAnchoExterior;
        private System.Windows.Forms.Label lblLongitud;
        private System.Windows.Forms.TextBox tbAnchoExterior;
        private System.Windows.Forms.ComboBox cbMarca;
        private System.Windows.Forms.TextBox tbKmService;
        private System.Windows.Forms.Label lbKmService;
        private System.Windows.Forms.TextBox tbKmReales;
        private System.Windows.Forms.Label lblKmReales;
        private System.Windows.Forms.Label lblTara;
        private System.Windows.Forms.TextBox tbAño;
        private System.Windows.Forms.Label lblAño;
        private System.Windows.Forms.Label lblModelo;
        private System.Windows.Forms.Label lblMarca;
        private System.Windows.Forms.TextBox tbDominio;
        private System.Windows.Forms.Label lblDominio;
        private System.Windows.Forms.Label lblTipoCarga;
        private System.Windows.Forms.ComboBox cbTipoCarga;
        private System.Windows.Forms.TextBox tbTara;
        private System.Windows.Forms.Label lblTipoAcoplado;
        private System.Windows.Forms.Button btnSalir;
        private System.Windows.Forms.TextBox tbKmCambioNeumaticos;
        private System.Windows.Forms.Label lbKmCambioNeumaticos;
        private System.Windows.Forms.RadioButton rbRemolque;
        private System.Windows.Forms.RadioButton rbSemiremolque;
        private System.Windows.Forms.Panel panel1;
    }
}