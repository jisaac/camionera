﻿namespace CapaPresentacion
{
    partial class frmOperaciones
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tppPedidos = new System.Windows.Forms.TabPage();
            this.gbDatosPedido = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbTipoCarga = new System.Windows.Forms.Label();
            this.lbKmViajePedido = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbVolumenPedido = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbPesoNetoPedido = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbEnganchePedido = new System.Windows.Forms.Label();
            this.gbClientePedido = new System.Windows.Forms.GroupBox();
            this.lbCuilClientePedido = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lbNombreClientePedido = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.gbObservacionPedido = new System.Windows.Forms.GroupBox();
            this.lbObservacionesPedido = new System.Windows.Forms.Label();
            this.gbAcopladoPedido = new System.Windows.Forms.GroupBox();
            this.lbAnchoExtRemolquePedido = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbAnchoIntRemolquePedido = new System.Windows.Forms.Label();
            this.lbAlturaRemolquePedido = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lbLongitudRemolquePedido = new System.Windows.Forms.Label();
            this.gbCamionPedido = new System.Windows.Forms.GroupBox();
            this.lbAnchoExtCamionPedido = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbAlturaCamionPedido = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbLongitudCamionPedido = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbAnchoIntCamionPedido = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gbChoferPedido = new System.Windows.Forms.GroupBox();
            this.lbNombreChoferPedido = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbNumLegajoChofer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gpPedidos = new System.Windows.Forms.GroupBox();
            this.lbFechaHastaPedido = new System.Windows.Forms.Label();
            this.lbFechaDesdePedido = new System.Windows.Forms.Label();
            this.dtpDesdePedido = new System.Windows.Forms.DateTimePicker();
            this.dtpHastaPedido = new System.Windows.Forms.DateTimePicker();
            this.tbBuscarPedido = new System.Windows.Forms.TextBox();
            this.btnEliminarPedido = new System.Windows.Forms.Button();
            this.btnModificarPedido = new System.Windows.Forms.Button();
            this.btnAñadirPedido = new System.Windows.Forms.Button();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.tpPedidos = new System.Windows.Forms.TabControl();
            this.tpPersonal = new System.Windows.Forms.TabPage();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.dgvChofer = new System.Windows.Forms.DataGridView();
            this.tbBuscarChofer = new System.Windows.Forms.TextBox();
            this.btnEliminarChofer = new System.Windows.Forms.Button();
            this.btnModificarChofer = new System.Windows.Forms.Button();
            this.btnAñadirChofer = new System.Windows.Forms.Button();
            this.gpUsuarios = new System.Windows.Forms.GroupBox();
            this.tbBuscarUsuario = new System.Windows.Forms.TextBox();
            this.btnEliminarUsuario = new System.Windows.Forms.Button();
            this.btnModificarUsuario = new System.Windows.Forms.Button();
            this.btnAltaUsuario = new System.Windows.Forms.Button();
            this.dgvUsuarios = new System.Windows.Forms.DataGridView();
            this.gpRoles = new System.Windows.Forms.GroupBox();
            this.tbBuscarRol = new System.Windows.Forms.TextBox();
            this.btnEliminarRol = new System.Windows.Forms.Button();
            this.btnModificarRol = new System.Windows.Forms.Button();
            this.btnAñadirRol = new System.Windows.Forms.Button();
            this.dgvRoles = new System.Windows.Forms.DataGridView();
            this.tbUnidades = new System.Windows.Forms.TabPage();
            this.tcCamiones = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnTipoCargaCamion = new System.Windows.Forms.Button();
            this.btnCamionVerDetalles = new System.Windows.Forms.Button();
            this.btnCamionMarcaYtipo = new System.Windows.Forms.Button();
            this.pnlCamionBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpCamionBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.lblBuscarCamionBuscarFechaHasta = new System.Windows.Forms.Label();
            this.dtpCamionBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.lblCamionBuscarFechaDesde = new System.Windows.Forms.Label();
            this.dgvCamion = new System.Windows.Forms.DataGridView();
            this.pnlCamionBusquedaTexto = new System.Windows.Forms.Panel();
            this.lblCamionBuscarTexto = new System.Windows.Forms.Label();
            this.tbBusquedaCamion = new System.Windows.Forms.TextBox();
            this.btnCamionNuevo = new System.Windows.Forms.Button();
            this.btnCamionEliminar = new System.Windows.Forms.Button();
            this.cbCamionFiltros = new System.Windows.Forms.ComboBox();
            this.btnCamionModificar = new System.Windows.Forms.Button();
            this.tcAcoplado = new System.Windows.Forms.TabPage();
            this.btnTipoCargaAcoplado = new System.Windows.Forms.Button();
            this.pnlAcopladoBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpAcopladoBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpAcopladoBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlAcopladoBusquedaTexto = new System.Windows.Forms.Panel();
            this.tbBusquedaAcoplado = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.btnAcopladoVerDetalle = new System.Windows.Forms.Button();
            this.btnAcopladoMarcaYtipo = new System.Windows.Forms.Button();
            this.dgvAcoplado = new System.Windows.Forms.DataGridView();
            this.btnAcopladoNuevo = new System.Windows.Forms.Button();
            this.btnAcopladoEliminar = new System.Windows.Forms.Button();
            this.cbAcopladoFiltrar = new System.Windows.Forms.ComboBox();
            this.btnAcopladoModificar = new System.Windows.Forms.Button();
            this.tbReportes = new System.Windows.Forms.TabPage();
            this.btnReporte9 = new System.Windows.Forms.Button();
            this.btnReporte8 = new System.Windows.Forms.Button();
            this.btnReporte7 = new System.Windows.Forms.Button();
            this.btnReporte6 = new System.Windows.Forms.Button();
            this.btnReporte5 = new System.Windows.Forms.Button();
            this.btnReporte4 = new System.Windows.Forms.Button();
            this.btnReporte3 = new System.Windows.Forms.Button();
            this.btnReporte10 = new System.Windows.Forms.Button();
            this.btnReporte2 = new System.Windows.Forms.Button();
            this.btnReporte1 = new System.Windows.Forms.Button();
            this.tppPedidos.SuspendLayout();
            this.gbDatosPedido.SuspendLayout();
            this.gbClientePedido.SuspendLayout();
            this.gbObservacionPedido.SuspendLayout();
            this.gbAcopladoPedido.SuspendLayout();
            this.gbCamionPedido.SuspendLayout();
            this.gbChoferPedido.SuspendLayout();
            this.gpPedidos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.tpPedidos.SuspendLayout();
            this.tpPersonal.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChofer)).BeginInit();
            this.gpUsuarios.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).BeginInit();
            this.gpRoles.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoles)).BeginInit();
            this.tbUnidades.SuspendLayout();
            this.tcCamiones.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.pnlCamionBusquedaFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamion)).BeginInit();
            this.pnlCamionBusquedaTexto.SuspendLayout();
            this.tcAcoplado.SuspendLayout();
            this.pnlAcopladoBusquedaFecha.SuspendLayout();
            this.pnlAcopladoBusquedaTexto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcoplado)).BeginInit();
            this.tbReportes.SuspendLayout();
            this.SuspendLayout();
            // 
            // tppPedidos
            // 
            this.tppPedidos.Controls.Add(this.gbDatosPedido);
            this.tppPedidos.Controls.Add(this.gbClientePedido);
            this.tppPedidos.Controls.Add(this.gbObservacionPedido);
            this.tppPedidos.Controls.Add(this.gbAcopladoPedido);
            this.tppPedidos.Controls.Add(this.gbCamionPedido);
            this.tppPedidos.Controls.Add(this.gbChoferPedido);
            this.tppPedidos.Controls.Add(this.gpPedidos);
            this.tppPedidos.Location = new System.Drawing.Point(4, 29);
            this.tppPedidos.Name = "tppPedidos";
            this.tppPedidos.Size = new System.Drawing.Size(813, 492);
            this.tppPedidos.TabIndex = 3;
            this.tppPedidos.Text = "ADMINISTRACION PEDIDOS";
            this.tppPedidos.UseVisualStyleBackColor = true;
            // 
            // gbDatosPedido
            // 
            this.gbDatosPedido.Controls.Add(this.label4);
            this.gbDatosPedido.Controls.Add(this.lbTipoCarga);
            this.gbDatosPedido.Controls.Add(this.lbKmViajePedido);
            this.gbDatosPedido.Controls.Add(this.label14);
            this.gbDatosPedido.Controls.Add(this.lbVolumenPedido);
            this.gbDatosPedido.Controls.Add(this.label17);
            this.gbDatosPedido.Controls.Add(this.lbPesoNetoPedido);
            this.gbDatosPedido.Controls.Add(this.label10);
            this.gbDatosPedido.Controls.Add(this.label6);
            this.gbDatosPedido.Controls.Add(this.lbEnganchePedido);
            this.gbDatosPedido.Location = new System.Drawing.Point(9, 200);
            this.gbDatosPedido.Name = "gbDatosPedido";
            this.gbDatosPedido.Size = new System.Drawing.Size(405, 75);
            this.gbDatosPedido.TabIndex = 28;
            this.gbDatosPedido.TabStop = false;
            this.gbDatosPedido.Text = "Pedido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 50);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Tipo Carga";
            // 
            // lbTipoCarga
            // 
            this.lbTipoCarga.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbTipoCarga.Location = new System.Drawing.Point(62, 43);
            this.lbTipoCarga.Name = "lbTipoCarga";
            this.lbTipoCarga.Size = new System.Drawing.Size(177, 26);
            this.lbTipoCarga.TabIndex = 25;
            this.lbTipoCarga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbKmViajePedido
            // 
            this.lbKmViajePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbKmViajePedido.Location = new System.Drawing.Point(311, 47);
            this.lbKmViajePedido.Name = "lbKmViajePedido";
            this.lbKmViajePedido.Size = new System.Drawing.Size(90, 19);
            this.lbKmViajePedido.TabIndex = 23;
            this.lbKmViajePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(248, 50);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Km Viaje";
            // 
            // lbVolumenPedido
            // 
            this.lbVolumenPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbVolumenPedido.Location = new System.Drawing.Point(311, 27);
            this.lbVolumenPedido.Name = "lbVolumenPedido";
            this.lbVolumenPedido.Size = new System.Drawing.Size(90, 19);
            this.lbVolumenPedido.TabIndex = 21;
            this.lbVolumenPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(248, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Volumen";
            // 
            // lbPesoNetoPedido
            // 
            this.lbPesoNetoPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbPesoNetoPedido.Location = new System.Drawing.Point(311, 8);
            this.lbPesoNetoPedido.Name = "lbPesoNetoPedido";
            this.lbPesoNetoPedido.Size = new System.Drawing.Size(90, 19);
            this.lbPesoNetoPedido.TabIndex = 19;
            this.lbPesoNetoPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(1, 18);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Enganche";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(248, 11);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Peso Neto";
            // 
            // lbEnganchePedido
            // 
            this.lbEnganchePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbEnganchePedido.Location = new System.Drawing.Point(62, 11);
            this.lbEnganchePedido.Name = "lbEnganchePedido";
            this.lbEnganchePedido.Size = new System.Drawing.Size(177, 26);
            this.lbEnganchePedido.TabIndex = 17;
            this.lbEnganchePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbClientePedido
            // 
            this.gbClientePedido.Controls.Add(this.lbCuilClientePedido);
            this.gbClientePedido.Controls.Add(this.label31);
            this.gbClientePedido.Controls.Add(this.lbNombreClientePedido);
            this.gbClientePedido.Controls.Add(this.label33);
            this.gbClientePedido.Location = new System.Drawing.Point(416, 200);
            this.gbClientePedido.Name = "gbClientePedido";
            this.gbClientePedido.Size = new System.Drawing.Size(391, 75);
            this.gbClientePedido.TabIndex = 28;
            this.gbClientePedido.TabStop = false;
            this.gbClientePedido.Text = "Cliente";
            // 
            // lbCuilClientePedido
            // 
            this.lbCuilClientePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCuilClientePedido.Location = new System.Drawing.Point(106, 45);
            this.lbCuilClientePedido.Name = "lbCuilClientePedido";
            this.lbCuilClientePedido.Size = new System.Drawing.Size(279, 23);
            this.lbCuilClientePedido.TabIndex = 3;
            this.lbCuilClientePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(13, 50);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Cuil";
            // 
            // lbNombreClientePedido
            // 
            this.lbNombreClientePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNombreClientePedido.Location = new System.Drawing.Point(106, 14);
            this.lbNombreClientePedido.Name = "lbNombreClientePedido";
            this.lbNombreClientePedido.Size = new System.Drawing.Size(279, 23);
            this.lbNombreClientePedido.TabIndex = 1;
            this.lbNombreClientePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(8, 18);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Nombre y Apellido";
            // 
            // gbObservacionPedido
            // 
            this.gbObservacionPedido.Controls.Add(this.lbObservacionesPedido);
            this.gbObservacionPedido.Location = new System.Drawing.Point(9, 431);
            this.gbObservacionPedido.Name = "gbObservacionPedido";
            this.gbObservacionPedido.Size = new System.Drawing.Size(798, 56);
            this.gbObservacionPedido.TabIndex = 30;
            this.gbObservacionPedido.TabStop = false;
            this.gbObservacionPedido.Text = "Observaciones";
            // 
            // lbObservacionesPedido
            // 
            this.lbObservacionesPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbObservacionesPedido.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbObservacionesPedido.Location = new System.Drawing.Point(9, 16);
            this.lbObservacionesPedido.Name = "lbObservacionesPedido";
            this.lbObservacionesPedido.Size = new System.Drawing.Size(759, 36);
            this.lbObservacionesPedido.TabIndex = 16;
            this.lbObservacionesPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbAcopladoPedido
            // 
            this.gbAcopladoPedido.Controls.Add(this.lbAnchoExtRemolquePedido);
            this.gbAcopladoPedido.Controls.Add(this.label24);
            this.gbAcopladoPedido.Controls.Add(this.label18);
            this.gbAcopladoPedido.Controls.Add(this.lbAnchoIntRemolquePedido);
            this.gbAcopladoPedido.Controls.Add(this.lbAlturaRemolquePedido);
            this.gbAcopladoPedido.Controls.Add(this.label22);
            this.gbAcopladoPedido.Controls.Add(this.label20);
            this.gbAcopladoPedido.Controls.Add(this.lbLongitudRemolquePedido);
            this.gbAcopladoPedido.Location = new System.Drawing.Point(405, 336);
            this.gbAcopladoPedido.Name = "gbAcopladoPedido";
            this.gbAcopladoPedido.Size = new System.Drawing.Size(402, 89);
            this.gbAcopladoPedido.TabIndex = 29;
            this.gbAcopladoPedido.TabStop = false;
            this.gbAcopladoPedido.Text = "Datos Remolque - Semiremolque";
            // 
            // lbAnchoExtRemolquePedido
            // 
            this.lbAnchoExtRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoExtRemolquePedido.Location = new System.Drawing.Point(281, 21);
            this.lbAnchoExtRemolquePedido.Name = "lbAnchoExtRemolquePedido";
            this.lbAnchoExtRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoExtRemolquePedido.TabIndex = 25;
            this.lbAnchoExtRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(19, 28);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Ancho Int";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(199, 28);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Ancho Ext";
            // 
            // lbAnchoIntRemolquePedido
            // 
            this.lbAnchoIntRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoIntRemolquePedido.Location = new System.Drawing.Point(78, 21);
            this.lbAnchoIntRemolquePedido.Name = "lbAnchoIntRemolquePedido";
            this.lbAnchoIntRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoIntRemolquePedido.TabIndex = 19;
            this.lbAnchoIntRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAlturaRemolquePedido
            // 
            this.lbAlturaRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAlturaRemolquePedido.Location = new System.Drawing.Point(281, 49);
            this.lbAlturaRemolquePedido.Name = "lbAlturaRemolquePedido";
            this.lbAlturaRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbAlturaRemolquePedido.TabIndex = 23;
            this.lbAlturaRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(19, 56);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "Longitud";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(199, 56);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Altura";
            // 
            // lbLongitudRemolquePedido
            // 
            this.lbLongitudRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbLongitudRemolquePedido.Location = new System.Drawing.Point(78, 49);
            this.lbLongitudRemolquePedido.Name = "lbLongitudRemolquePedido";
            this.lbLongitudRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbLongitudRemolquePedido.TabIndex = 21;
            this.lbLongitudRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbCamionPedido
            // 
            this.gbCamionPedido.Controls.Add(this.lbAnchoExtCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label13);
            this.gbCamionPedido.Controls.Add(this.lbAlturaCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label11);
            this.gbCamionPedido.Controls.Add(this.lbLongitudCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label9);
            this.gbCamionPedido.Controls.Add(this.lbAnchoIntCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label7);
            this.gbCamionPedido.Location = new System.Drawing.Point(9, 336);
            this.gbCamionPedido.Name = "gbCamionPedido";
            this.gbCamionPedido.Size = new System.Drawing.Size(390, 89);
            this.gbCamionPedido.TabIndex = 28;
            this.gbCamionPedido.TabStop = false;
            this.gbCamionPedido.Text = "Datos Camion";
            // 
            // lbAnchoExtCamionPedido
            // 
            this.lbAnchoExtCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoExtCamionPedido.Location = new System.Drawing.Point(268, 21);
            this.lbAnchoExtCamionPedido.Name = "lbAnchoExtCamionPedido";
            this.lbAnchoExtCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoExtCamionPedido.TabIndex = 13;
            this.lbAnchoExtCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(189, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Ancho Ext";
            // 
            // lbAlturaCamionPedido
            // 
            this.lbAlturaCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAlturaCamionPedido.Location = new System.Drawing.Point(268, 52);
            this.lbAlturaCamionPedido.Name = "lbAlturaCamionPedido";
            this.lbAlturaCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbAlturaCamionPedido.TabIndex = 11;
            this.lbAlturaCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(189, 56);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Altura";
            // 
            // lbLongitudCamionPedido
            // 
            this.lbLongitudCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbLongitudCamionPedido.Location = new System.Drawing.Point(68, 49);
            this.lbLongitudCamionPedido.Name = "lbLongitudCamionPedido";
            this.lbLongitudCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbLongitudCamionPedido.TabIndex = 9;
            this.lbLongitudCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Longitud";
            // 
            // lbAnchoIntCamionPedido
            // 
            this.lbAnchoIntCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoIntCamionPedido.Location = new System.Drawing.Point(68, 21);
            this.lbAnchoIntCamionPedido.Name = "lbAnchoIntCamionPedido";
            this.lbAnchoIntCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoIntCamionPedido.TabIndex = 7;
            this.lbAnchoIntCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 28);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ancho Int";
            // 
            // gbChoferPedido
            // 
            this.gbChoferPedido.Controls.Add(this.lbNombreChoferPedido);
            this.gbChoferPedido.Controls.Add(this.label3);
            this.gbChoferPedido.Controls.Add(this.lbNumLegajoChofer);
            this.gbChoferPedido.Controls.Add(this.label1);
            this.gbChoferPedido.Location = new System.Drawing.Point(9, 281);
            this.gbChoferPedido.Name = "gbChoferPedido";
            this.gbChoferPedido.Size = new System.Drawing.Size(798, 49);
            this.gbChoferPedido.TabIndex = 27;
            this.gbChoferPedido.TabStop = false;
            this.gbChoferPedido.Text = "Chofer";
            // 
            // lbNombreChoferPedido
            // 
            this.lbNombreChoferPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNombreChoferPedido.Location = new System.Drawing.Point(346, 13);
            this.lbNombreChoferPedido.Name = "lbNombreChoferPedido";
            this.lbNombreChoferPedido.Size = new System.Drawing.Size(446, 26);
            this.lbNombreChoferPedido.TabIndex = 3;
            this.lbNombreChoferPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(248, 18);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre y Apellido";
            // 
            // lbNumLegajoChofer
            // 
            this.lbNumLegajoChofer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNumLegajoChofer.Location = new System.Drawing.Point(62, 11);
            this.lbNumLegajoChofer.Name = "lbNumLegajoChofer";
            this.lbNumLegajoChofer.Size = new System.Drawing.Size(177, 26);
            this.lbNumLegajoChofer.TabIndex = 1;
            this.lbNumLegajoChofer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nº Legajo";
            // 
            // gpPedidos
            // 
            this.gpPedidos.Controls.Add(this.lbFechaHastaPedido);
            this.gpPedidos.Controls.Add(this.lbFechaDesdePedido);
            this.gpPedidos.Controls.Add(this.dtpDesdePedido);
            this.gpPedidos.Controls.Add(this.dtpHastaPedido);
            this.gpPedidos.Controls.Add(this.tbBuscarPedido);
            this.gpPedidos.Controls.Add(this.btnEliminarPedido);
            this.gpPedidos.Controls.Add(this.btnModificarPedido);
            this.gpPedidos.Controls.Add(this.btnAñadirPedido);
            this.gpPedidos.Controls.Add(this.dgvPedidos);
            this.gpPedidos.Location = new System.Drawing.Point(3, 3);
            this.gpPedidos.Name = "gpPedidos";
            this.gpPedidos.Size = new System.Drawing.Size(804, 198);
            this.gpPedidos.TabIndex = 26;
            this.gpPedidos.TabStop = false;
            this.gpPedidos.Text = "PEDIDOS";
            // 
            // lbFechaHastaPedido
            // 
            this.lbFechaHastaPedido.AutoSize = true;
            this.lbFechaHastaPedido.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaHastaPedido.Location = new System.Drawing.Point(421, 25);
            this.lbFechaHastaPedido.Name = "lbFechaHastaPedido";
            this.lbFechaHastaPedido.Size = new System.Drawing.Size(54, 18);
            this.lbFechaHastaPedido.TabIndex = 33;
            this.lbFechaHastaPedido.Text = "HASTA";
            // 
            // lbFechaDesdePedido
            // 
            this.lbFechaDesdePedido.AutoSize = true;
            this.lbFechaDesdePedido.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaDesdePedido.Location = new System.Drawing.Point(251, 25);
            this.lbFechaDesdePedido.Name = "lbFechaDesdePedido";
            this.lbFechaDesdePedido.Size = new System.Drawing.Size(55, 18);
            this.lbFechaDesdePedido.TabIndex = 32;
            this.lbFechaDesdePedido.Text = "DESDE";
            // 
            // dtpDesdePedido
            // 
            this.dtpDesdePedido.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDesdePedido.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpDesdePedido.Location = new System.Drawing.Point(317, 22);
            this.dtpDesdePedido.Name = "dtpDesdePedido";
            this.dtpDesdePedido.Size = new System.Drawing.Size(94, 26);
            this.dtpDesdePedido.TabIndex = 31;
            this.dtpDesdePedido.ValueChanged += new System.EventHandler(this.dtpDesdePedido_ValueChanged);
            // 
            // dtpHastaPedido
            // 
            this.dtpHastaPedido.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHastaPedido.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpHastaPedido.Location = new System.Drawing.Point(483, 22);
            this.dtpHastaPedido.Name = "dtpHastaPedido";
            this.dtpHastaPedido.Size = new System.Drawing.Size(94, 26);
            this.dtpHastaPedido.TabIndex = 30;
            this.dtpHastaPedido.ValueChanged += new System.EventHandler(this.dtpDesdePedido_ValueChanged);
            // 
            // tbBuscarPedido
            // 
            this.tbBuscarPedido.Font = new System.Drawing.Font("Bahnschrift", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBuscarPedido.Location = new System.Drawing.Point(583, 22);
            this.tbBuscarPedido.MaxLength = 100;
            this.tbBuscarPedido.Multiline = true;
            this.tbBuscarPedido.Name = "tbBuscarPedido";
            this.tbBuscarPedido.ShortcutsEnabled = false;
            this.tbBuscarPedido.Size = new System.Drawing.Size(215, 24);
            this.tbBuscarPedido.TabIndex = 29;
            this.tbBuscarPedido.Click += new System.EventHandler(this.tbBuscarPedido_Click);
            this.tbBuscarPedido.TextChanged += new System.EventHandler(this.tbBuscarPedido_TextChanged);
            this.tbBuscarPedido.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbBuscarPedido_KeyPress);
            this.tbBuscarPedido.Leave += new System.EventHandler(this.tbBuscarPedido_Leave);
            // 
            // btnEliminarPedido
            // 
            this.btnEliminarPedido.Location = new System.Drawing.Point(170, 19);
            this.btnEliminarPedido.Name = "btnEliminarPedido";
            this.btnEliminarPedido.Size = new System.Drawing.Size(75, 29);
            this.btnEliminarPedido.TabIndex = 27;
            this.btnEliminarPedido.Text = "Eliminar";
            this.btnEliminarPedido.UseVisualStyleBackColor = true;
            this.btnEliminarPedido.Click += new System.EventHandler(this.btnEliminarPedido_Click);
            // 
            // btnModificarPedido
            // 
            this.btnModificarPedido.Location = new System.Drawing.Point(89, 19);
            this.btnModificarPedido.Name = "btnModificarPedido";
            this.btnModificarPedido.Size = new System.Drawing.Size(75, 29);
            this.btnModificarPedido.TabIndex = 26;
            this.btnModificarPedido.Text = "Modificar";
            this.btnModificarPedido.UseVisualStyleBackColor = true;
            this.btnModificarPedido.Click += new System.EventHandler(this.btnModificarPedido_Click);
            // 
            // btnAñadirPedido
            // 
            this.btnAñadirPedido.Location = new System.Drawing.Point(6, 19);
            this.btnAñadirPedido.Name = "btnAñadirPedido";
            this.btnAñadirPedido.Size = new System.Drawing.Size(75, 29);
            this.btnAñadirPedido.TabIndex = 25;
            this.btnAñadirPedido.Text = "Añadir";
            this.btnAñadirPedido.UseVisualStyleBackColor = true;
            this.btnAñadirPedido.Click += new System.EventHandler(this.btnAñadirPedido_Click);
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.AllowUserToAddRows = false;
            this.dgvPedidos.AllowUserToDeleteRows = false;
            this.dgvPedidos.AllowUserToOrderColumns = true;
            this.dgvPedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPedidos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvPedidos.Location = new System.Drawing.Point(6, 54);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.ReadOnly = true;
            this.dgvPedidos.Size = new System.Drawing.Size(792, 137);
            this.dgvPedidos.TabIndex = 24;
            // 
            // tpPedidos
            // 
            this.tpPedidos.Controls.Add(this.tpPersonal);
            this.tpPedidos.Controls.Add(this.tppPedidos);
            this.tpPedidos.Controls.Add(this.tbUnidades);
            this.tpPedidos.Controls.Add(this.tbReportes);
            this.tpPedidos.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tpPedidos.ItemSize = new System.Drawing.Size(231, 25);
            this.tpPedidos.Location = new System.Drawing.Point(7, 3);
            this.tpPedidos.Name = "tpPedidos";
            this.tpPedidos.SelectedIndex = 0;
            this.tpPedidos.Size = new System.Drawing.Size(821, 525);
            this.tpPedidos.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.tpPedidos.TabIndex = 5;
            // 
            // tpPersonal
            // 
            this.tpPersonal.Controls.Add(this.groupBox3);
            this.tpPersonal.Controls.Add(this.gpUsuarios);
            this.tpPersonal.Controls.Add(this.gpRoles);
            this.tpPersonal.Location = new System.Drawing.Point(4, 29);
            this.tpPersonal.Name = "tpPersonal";
            this.tpPersonal.Padding = new System.Windows.Forms.Padding(3);
            this.tpPersonal.Size = new System.Drawing.Size(813, 492);
            this.tpPersonal.TabIndex = 0;
            this.tpPersonal.Text = "ADMINISTRACION PERSONAL";
            this.tpPersonal.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgvChofer);
            this.groupBox3.Controls.Add(this.tbBuscarChofer);
            this.groupBox3.Controls.Add(this.btnEliminarChofer);
            this.groupBox3.Controls.Add(this.btnModificarChofer);
            this.groupBox3.Controls.Add(this.btnAñadirChofer);
            this.groupBox3.Location = new System.Drawing.Point(4, 239);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(793, 234);
            this.groupBox3.TabIndex = 27;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "CHOFERES";
            // 
            // dgvChofer
            // 
            this.dgvChofer.AllowUserToAddRows = false;
            this.dgvChofer.AllowUserToDeleteRows = false;
            this.dgvChofer.AllowUserToOrderColumns = true;
            this.dgvChofer.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvChofer.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvChofer.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvChofer.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvChofer.Location = new System.Drawing.Point(6, 54);
            this.dgvChofer.Name = "dgvChofer";
            this.dgvChofer.ReadOnly = true;
            this.dgvChofer.Size = new System.Drawing.Size(781, 168);
            this.dgvChofer.TabIndex = 24;
            // 
            // tbBuscarChofer
            // 
            this.tbBuscarChofer.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBuscarChofer.Location = new System.Drawing.Point(439, 20);
            this.tbBuscarChofer.MaxLength = 100;
            this.tbBuscarChofer.Multiline = true;
            this.tbBuscarChofer.Name = "tbBuscarChofer";
            this.tbBuscarChofer.ShortcutsEnabled = false;
            this.tbBuscarChofer.Size = new System.Drawing.Size(348, 28);
            this.tbBuscarChofer.TabIndex = 29;
            this.tbBuscarChofer.Click += new System.EventHandler(this.tbBuscarChofer_Click);
            this.tbBuscarChofer.TextChanged += new System.EventHandler(this.tbBuscarChofer_TextChanged);
            this.tbBuscarChofer.Leave += new System.EventHandler(this.tbBuscarChofer_Leave);
            // 
            // btnEliminarChofer
            // 
            this.btnEliminarChofer.Location = new System.Drawing.Point(300, 20);
            this.btnEliminarChofer.Name = "btnEliminarChofer";
            this.btnEliminarChofer.Size = new System.Drawing.Size(133, 29);
            this.btnEliminarChofer.TabIndex = 27;
            this.btnEliminarChofer.Text = "Eliminar";
            this.btnEliminarChofer.UseVisualStyleBackColor = true;
            this.btnEliminarChofer.Click += new System.EventHandler(this.btnEliminarChofer_Click);
            // 
            // btnModificarChofer
            // 
            this.btnModificarChofer.Location = new System.Drawing.Point(153, 20);
            this.btnModificarChofer.Name = "btnModificarChofer";
            this.btnModificarChofer.Size = new System.Drawing.Size(133, 29);
            this.btnModificarChofer.TabIndex = 26;
            this.btnModificarChofer.Text = "Modificar";
            this.btnModificarChofer.UseVisualStyleBackColor = true;
            this.btnModificarChofer.Click += new System.EventHandler(this.btnModificarChofer_Click);
            // 
            // btnAñadirChofer
            // 
            this.btnAñadirChofer.Location = new System.Drawing.Point(6, 20);
            this.btnAñadirChofer.Name = "btnAñadirChofer";
            this.btnAñadirChofer.Size = new System.Drawing.Size(133, 29);
            this.btnAñadirChofer.TabIndex = 25;
            this.btnAñadirChofer.Text = "Añadir";
            this.btnAñadirChofer.UseVisualStyleBackColor = true;
            this.btnAñadirChofer.Click += new System.EventHandler(this.btnAñadirChofer_Click);
            // 
            // gpUsuarios
            // 
            this.gpUsuarios.Controls.Add(this.tbBuscarUsuario);
            this.gpUsuarios.Controls.Add(this.btnEliminarUsuario);
            this.gpUsuarios.Controls.Add(this.btnModificarUsuario);
            this.gpUsuarios.Controls.Add(this.btnAltaUsuario);
            this.gpUsuarios.Controls.Add(this.dgvUsuarios);
            this.gpUsuarios.Location = new System.Drawing.Point(7, 6);
            this.gpUsuarios.Name = "gpUsuarios";
            this.gpUsuarios.Size = new System.Drawing.Size(551, 227);
            this.gpUsuarios.TabIndex = 25;
            this.gpUsuarios.TabStop = false;
            this.gpUsuarios.Text = "USUARIOS";
            // 
            // tbBuscarUsuario
            // 
            this.tbBuscarUsuario.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBuscarUsuario.Location = new System.Drawing.Point(252, 20);
            this.tbBuscarUsuario.MaxLength = 100;
            this.tbBuscarUsuario.Multiline = true;
            this.tbBuscarUsuario.Name = "tbBuscarUsuario";
            this.tbBuscarUsuario.ShortcutsEnabled = false;
            this.tbBuscarUsuario.Size = new System.Drawing.Size(293, 28);
            this.tbBuscarUsuario.TabIndex = 29;
            this.tbBuscarUsuario.Click += new System.EventHandler(this.tbBuscarUsuario_Click);
            this.tbBuscarUsuario.TextChanged += new System.EventHandler(this.tbBuscarUsuario_TextChanged);
            this.tbBuscarUsuario.Leave += new System.EventHandler(this.tbBuscarUsuario_Leave);
            // 
            // btnEliminarUsuario
            // 
            this.btnEliminarUsuario.Location = new System.Drawing.Point(170, 19);
            this.btnEliminarUsuario.Name = "btnEliminarUsuario";
            this.btnEliminarUsuario.Size = new System.Drawing.Size(75, 29);
            this.btnEliminarUsuario.TabIndex = 27;
            this.btnEliminarUsuario.Text = "Eliminar";
            this.btnEliminarUsuario.UseVisualStyleBackColor = true;
            this.btnEliminarUsuario.Click += new System.EventHandler(this.btnEliminarUsuario_Click);
            // 
            // btnModificarUsuario
            // 
            this.btnModificarUsuario.Location = new System.Drawing.Point(89, 19);
            this.btnModificarUsuario.Name = "btnModificarUsuario";
            this.btnModificarUsuario.Size = new System.Drawing.Size(75, 29);
            this.btnModificarUsuario.TabIndex = 26;
            this.btnModificarUsuario.Text = "Modificar";
            this.btnModificarUsuario.UseVisualStyleBackColor = true;
            this.btnModificarUsuario.Click += new System.EventHandler(this.btnModificarUsuario_Click);
            // 
            // btnAltaUsuario
            // 
            this.btnAltaUsuario.Location = new System.Drawing.Point(6, 19);
            this.btnAltaUsuario.Name = "btnAltaUsuario";
            this.btnAltaUsuario.Size = new System.Drawing.Size(75, 29);
            this.btnAltaUsuario.TabIndex = 25;
            this.btnAltaUsuario.Text = "Añadir";
            this.btnAltaUsuario.UseVisualStyleBackColor = true;
            this.btnAltaUsuario.Click += new System.EventHandler(this.btnAltaUsuario_Click);
            // 
            // dgvUsuarios
            // 
            this.dgvUsuarios.AllowUserToAddRows = false;
            this.dgvUsuarios.AllowUserToDeleteRows = false;
            this.dgvUsuarios.AllowUserToOrderColumns = true;
            this.dgvUsuarios.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvUsuarios.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvUsuarios.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvUsuarios.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvUsuarios.Location = new System.Drawing.Point(6, 54);
            this.dgvUsuarios.Name = "dgvUsuarios";
            this.dgvUsuarios.ReadOnly = true;
            this.dgvUsuarios.Size = new System.Drawing.Size(539, 159);
            this.dgvUsuarios.TabIndex = 24;
            // 
            // gpRoles
            // 
            this.gpRoles.Controls.Add(this.tbBuscarRol);
            this.gpRoles.Controls.Add(this.btnEliminarRol);
            this.gpRoles.Controls.Add(this.btnModificarRol);
            this.gpRoles.Controls.Add(this.btnAñadirRol);
            this.gpRoles.Controls.Add(this.dgvRoles);
            this.gpRoles.Location = new System.Drawing.Point(564, 6);
            this.gpRoles.Name = "gpRoles";
            this.gpRoles.Size = new System.Drawing.Size(229, 227);
            this.gpRoles.TabIndex = 24;
            this.gpRoles.TabStop = false;
            this.gpRoles.Text = "ROLES";
            // 
            // tbBuscarRol
            // 
            this.tbBuscarRol.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBuscarRol.Location = new System.Drawing.Point(6, 54);
            this.tbBuscarRol.MaxLength = 100;
            this.tbBuscarRol.Multiline = true;
            this.tbBuscarRol.Name = "tbBuscarRol";
            this.tbBuscarRol.ShortcutsEnabled = false;
            this.tbBuscarRol.Size = new System.Drawing.Size(217, 28);
            this.tbBuscarRol.TabIndex = 34;
            this.tbBuscarRol.Click += new System.EventHandler(this.tbBuscarRol_Click);
            this.tbBuscarRol.TextChanged += new System.EventHandler(this.tbBuscarRol_TextChanged);
            this.tbBuscarRol.Leave += new System.EventHandler(this.tbBuscarRol_Leave);
            // 
            // btnEliminarRol
            // 
            this.btnEliminarRol.Location = new System.Drawing.Point(154, 19);
            this.btnEliminarRol.Name = "btnEliminarRol";
            this.btnEliminarRol.Size = new System.Drawing.Size(69, 29);
            this.btnEliminarRol.TabIndex = 32;
            this.btnEliminarRol.Text = "Eliminar";
            this.btnEliminarRol.UseVisualStyleBackColor = true;
            this.btnEliminarRol.Click += new System.EventHandler(this.btnEliminarRol_Click);
            // 
            // btnModificarRol
            // 
            this.btnModificarRol.Location = new System.Drawing.Point(80, 19);
            this.btnModificarRol.Name = "btnModificarRol";
            this.btnModificarRol.Size = new System.Drawing.Size(69, 29);
            this.btnModificarRol.TabIndex = 31;
            this.btnModificarRol.Text = "Modificar";
            this.btnModificarRol.UseVisualStyleBackColor = true;
            this.btnModificarRol.Click += new System.EventHandler(this.btnModificarRol_Click);
            // 
            // btnAñadirRol
            // 
            this.btnAñadirRol.Location = new System.Drawing.Point(6, 19);
            this.btnAñadirRol.Name = "btnAñadirRol";
            this.btnAñadirRol.Size = new System.Drawing.Size(69, 29);
            this.btnAñadirRol.TabIndex = 30;
            this.btnAñadirRol.Text = "Añadir";
            this.btnAñadirRol.UseVisualStyleBackColor = true;
            this.btnAñadirRol.Click += new System.EventHandler(this.btnAñadirRol_Click);
            // 
            // dgvRoles
            // 
            this.dgvRoles.AllowUserToAddRows = false;
            this.dgvRoles.AllowUserToDeleteRows = false;
            this.dgvRoles.AllowUserToOrderColumns = true;
            this.dgvRoles.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvRoles.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvRoles.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvRoles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvRoles.Location = new System.Drawing.Point(6, 88);
            this.dgvRoles.Name = "dgvRoles";
            this.dgvRoles.ReadOnly = true;
            this.dgvRoles.Size = new System.Drawing.Size(217, 125);
            this.dgvRoles.TabIndex = 29;
            // 
            // tbUnidades
            // 
            this.tbUnidades.Controls.Add(this.tcCamiones);
            this.tbUnidades.Location = new System.Drawing.Point(4, 29);
            this.tbUnidades.Name = "tbUnidades";
            this.tbUnidades.Size = new System.Drawing.Size(813, 492);
            this.tbUnidades.TabIndex = 4;
            this.tbUnidades.Text = "ADMINISTRACION DE UNIDADES";
            // 
            // tcCamiones
            // 
            this.tcCamiones.Controls.Add(this.tabPage1);
            this.tcCamiones.Controls.Add(this.tcAcoplado);
            this.tcCamiones.Location = new System.Drawing.Point(12, 19);
            this.tcCamiones.Name = "tcCamiones";
            this.tcCamiones.SelectedIndex = 0;
            this.tcCamiones.Size = new System.Drawing.Size(785, 451);
            this.tcCamiones.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnTipoCargaCamion);
            this.tabPage1.Controls.Add(this.btnCamionVerDetalles);
            this.tabPage1.Controls.Add(this.btnCamionMarcaYtipo);
            this.tabPage1.Controls.Add(this.pnlCamionBusquedaFecha);
            this.tabPage1.Controls.Add(this.dgvCamion);
            this.tabPage1.Controls.Add(this.pnlCamionBusquedaTexto);
            this.tabPage1.Controls.Add(this.btnCamionNuevo);
            this.tabPage1.Controls.Add(this.btnCamionEliminar);
            this.tabPage1.Controls.Add(this.cbCamionFiltros);
            this.tabPage1.Controls.Add(this.btnCamionModificar);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(777, 425);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "CAMIONES";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // btnTipoCargaCamion
            // 
            this.btnTipoCargaCamion.Location = new System.Drawing.Point(583, 33);
            this.btnTipoCargaCamion.Name = "btnTipoCargaCamion";
            this.btnTipoCargaCamion.Size = new System.Drawing.Size(186, 21);
            this.btnTipoCargaCamion.TabIndex = 33;
            this.btnTipoCargaCamion.Text = "Gestion Tipo de Carga - Rigido";
            this.btnTipoCargaCamion.UseVisualStyleBackColor = true;
            this.btnTipoCargaCamion.Click += new System.EventHandler(this.btnTipoCargaCamion_Click);
            // 
            // btnCamionVerDetalles
            // 
            this.btnCamionVerDetalles.Location = new System.Drawing.Point(695, 68);
            this.btnCamionVerDetalles.Name = "btnCamionVerDetalles";
            this.btnCamionVerDetalles.Size = new System.Drawing.Size(75, 31);
            this.btnCamionVerDetalles.TabIndex = 8;
            this.btnCamionVerDetalles.Text = "Ver detalles";
            this.btnCamionVerDetalles.UseVisualStyleBackColor = true;
            this.btnCamionVerDetalles.Click += new System.EventHandler(this.btnCamionNuevo_Click);
            // 
            // btnCamionMarcaYtipo
            // 
            this.btnCamionMarcaYtipo.Location = new System.Drawing.Point(583, 6);
            this.btnCamionMarcaYtipo.Name = "btnCamionMarcaYtipo";
            this.btnCamionMarcaYtipo.Size = new System.Drawing.Size(186, 21);
            this.btnCamionMarcaYtipo.TabIndex = 9;
            this.btnCamionMarcaYtipo.Text = "Gestion de Marcas y Modelos";
            this.btnCamionMarcaYtipo.UseVisualStyleBackColor = true;
            this.btnCamionMarcaYtipo.Click += new System.EventHandler(this.btnMarcaYtipo_Click);
            // 
            // pnlCamionBusquedaFecha
            // 
            this.pnlCamionBusquedaFecha.Controls.Add(this.dtpCamionBusquedaHasta);
            this.pnlCamionBusquedaFecha.Controls.Add(this.lblBuscarCamionBuscarFechaHasta);
            this.pnlCamionBusquedaFecha.Controls.Add(this.dtpCamionBusquedaDesde);
            this.pnlCamionBusquedaFecha.Controls.Add(this.lblCamionBuscarFechaDesde);
            this.pnlCamionBusquedaFecha.Location = new System.Drawing.Point(358, 69);
            this.pnlCamionBusquedaFecha.Name = "pnlCamionBusquedaFecha";
            this.pnlCamionBusquedaFecha.Size = new System.Drawing.Size(333, 27);
            this.pnlCamionBusquedaFecha.TabIndex = 3;
            // 
            // dtpCamionBusquedaHasta
            // 
            this.dtpCamionBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCamionBusquedaHasta.Location = new System.Drawing.Point(226, 3);
            this.dtpCamionBusquedaHasta.Name = "dtpCamionBusquedaHasta";
            this.dtpCamionBusquedaHasta.Size = new System.Drawing.Size(104, 21);
            this.dtpCamionBusquedaHasta.TabIndex = 4;
            this.dtpCamionBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpCamionBusquedaDesde_ValueChanged);
            // 
            // lblBuscarCamionBuscarFechaHasta
            // 
            this.lblBuscarCamionBuscarFechaHasta.AutoSize = true;
            this.lblBuscarCamionBuscarFechaHasta.Location = new System.Drawing.Point(190, 7);
            this.lblBuscarCamionBuscarFechaHasta.Name = "lblBuscarCamionBuscarFechaHasta";
            this.lblBuscarCamionBuscarFechaHasta.Size = new System.Drawing.Size(35, 13);
            this.lblBuscarCamionBuscarFechaHasta.TabIndex = 3;
            this.lblBuscarCamionBuscarFechaHasta.Text = "hasta";
            // 
            // dtpCamionBusquedaDesde
            // 
            this.dtpCamionBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCamionBusquedaDesde.Location = new System.Drawing.Point(80, 3);
            this.dtpCamionBusquedaDesde.Name = "dtpCamionBusquedaDesde";
            this.dtpCamionBusquedaDesde.Size = new System.Drawing.Size(104, 21);
            this.dtpCamionBusquedaDesde.TabIndex = 2;
            this.dtpCamionBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpCamionBusquedaDesde_ValueChanged);
            // 
            // lblCamionBuscarFechaDesde
            // 
            this.lblCamionBuscarFechaDesde.AutoSize = true;
            this.lblCamionBuscarFechaDesde.Location = new System.Drawing.Point(3, 7);
            this.lblCamionBuscarFechaDesde.Name = "lblCamionBuscarFechaDesde";
            this.lblCamionBuscarFechaDesde.Size = new System.Drawing.Size(78, 13);
            this.lblCamionBuscarFechaDesde.TabIndex = 1;
            this.lblCamionBuscarFechaDesde.Text = "Buscar  desde ";
            // 
            // dgvCamion
            // 
            this.dgvCamion.AllowUserToAddRows = false;
            this.dgvCamion.AllowUserToDeleteRows = false;
            this.dgvCamion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCamion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvCamion.Location = new System.Drawing.Point(6, 105);
            this.dgvCamion.MultiSelect = false;
            this.dgvCamion.Name = "dgvCamion";
            this.dgvCamion.ReadOnly = true;
            this.dgvCamion.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCamion.Size = new System.Drawing.Size(763, 314);
            this.dgvCamion.TabIndex = 4;
            this.dgvCamion.CurrentCellChanged += new System.EventHandler(this.dgvCamion_CurrentCellChanged);
            // 
            // pnlCamionBusquedaTexto
            // 
            this.pnlCamionBusquedaTexto.Controls.Add(this.lblCamionBuscarTexto);
            this.pnlCamionBusquedaTexto.Controls.Add(this.tbBusquedaCamion);
            this.pnlCamionBusquedaTexto.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.pnlCamionBusquedaTexto.Location = new System.Drawing.Point(7, 39);
            this.pnlCamionBusquedaTexto.Name = "pnlCamionBusquedaTexto";
            this.pnlCamionBusquedaTexto.Size = new System.Drawing.Size(257, 32);
            this.pnlCamionBusquedaTexto.TabIndex = 2;
            // 
            // lblCamionBuscarTexto
            // 
            this.lblCamionBuscarTexto.AutoSize = true;
            this.lblCamionBuscarTexto.Location = new System.Drawing.Point(3, 14);
            this.lblCamionBuscarTexto.Name = "lblCamionBuscarTexto";
            this.lblCamionBuscarTexto.Size = new System.Drawing.Size(48, 13);
            this.lblCamionBuscarTexto.TabIndex = 1;
            this.lblCamionBuscarTexto.Text = "Buscar : ";
            // 
            // tbBusquedaCamion
            // 
            this.tbBusquedaCamion.Location = new System.Drawing.Point(58, 11);
            this.tbBusquedaCamion.MaxLength = 100;
            this.tbBusquedaCamion.Name = "tbBusquedaCamion";
            this.tbBusquedaCamion.ShortcutsEnabled = false;
            this.tbBusquedaCamion.Size = new System.Drawing.Size(196, 21);
            this.tbBusquedaCamion.TabIndex = 0;
            this.tbBusquedaCamion.TextChanged += new System.EventHandler(this.tbCamionBuscarTexto_TextChanged);
            // 
            // btnCamionNuevo
            // 
            this.btnCamionNuevo.Location = new System.Drawing.Point(6, 6);
            this.btnCamionNuevo.Name = "btnCamionNuevo";
            this.btnCamionNuevo.Size = new System.Drawing.Size(75, 31);
            this.btnCamionNuevo.TabIndex = 5;
            this.btnCamionNuevo.Text = "Nuevo";
            this.btnCamionNuevo.UseVisualStyleBackColor = true;
            this.btnCamionNuevo.Click += new System.EventHandler(this.btnCamionNuevo_Click);
            // 
            // btnCamionEliminar
            // 
            this.btnCamionEliminar.Location = new System.Drawing.Point(168, 6);
            this.btnCamionEliminar.Name = "btnCamionEliminar";
            this.btnCamionEliminar.Size = new System.Drawing.Size(75, 31);
            this.btnCamionEliminar.TabIndex = 7;
            this.btnCamionEliminar.Text = "Dar de baja";
            this.btnCamionEliminar.UseVisualStyleBackColor = true;
            this.btnCamionEliminar.Click += new System.EventHandler(this.btnCamionNuevo_Click);
            // 
            // cbCamionFiltros
            // 
            this.cbCamionFiltros.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCamionFiltros.FormattingEnabled = true;
            this.cbCamionFiltros.Items.AddRange(new object[] {
            "HABILITADOS",
            "TODOS",
            "DADOS DE BAJA",
            "FECHA DE ALTA"});
            this.cbCamionFiltros.Location = new System.Drawing.Point(7, 74);
            this.cbCamionFiltros.Name = "cbCamionFiltros";
            this.cbCamionFiltros.Size = new System.Drawing.Size(271, 21);
            this.cbCamionFiltros.TabIndex = 1;
            this.cbCamionFiltros.SelectedIndexChanged += new System.EventHandler(this.cbCamionFiltros_SelectedIndexChanged);
            // 
            // btnCamionModificar
            // 
            this.btnCamionModificar.Location = new System.Drawing.Point(87, 6);
            this.btnCamionModificar.Name = "btnCamionModificar";
            this.btnCamionModificar.Size = new System.Drawing.Size(75, 31);
            this.btnCamionModificar.TabIndex = 6;
            this.btnCamionModificar.Text = "Modificar";
            this.btnCamionModificar.UseVisualStyleBackColor = true;
            this.btnCamionModificar.Click += new System.EventHandler(this.btnCamionNuevo_Click);
            // 
            // tcAcoplado
            // 
            this.tcAcoplado.Controls.Add(this.btnTipoCargaAcoplado);
            this.tcAcoplado.Controls.Add(this.pnlAcopladoBusquedaFecha);
            this.tcAcoplado.Controls.Add(this.pnlAcopladoBusquedaTexto);
            this.tcAcoplado.Controls.Add(this.btnAcopladoVerDetalle);
            this.tcAcoplado.Controls.Add(this.btnAcopladoMarcaYtipo);
            this.tcAcoplado.Controls.Add(this.dgvAcoplado);
            this.tcAcoplado.Controls.Add(this.btnAcopladoNuevo);
            this.tcAcoplado.Controls.Add(this.btnAcopladoEliminar);
            this.tcAcoplado.Controls.Add(this.cbAcopladoFiltrar);
            this.tcAcoplado.Controls.Add(this.btnAcopladoModificar);
            this.tcAcoplado.Location = new System.Drawing.Point(4, 22);
            this.tcAcoplado.Name = "tcAcoplado";
            this.tcAcoplado.Padding = new System.Windows.Forms.Padding(3);
            this.tcAcoplado.Size = new System.Drawing.Size(777, 425);
            this.tcAcoplado.TabIndex = 1;
            this.tcAcoplado.Text = "ACOPLADOS";
            this.tcAcoplado.UseVisualStyleBackColor = true;
            // 
            // btnTipoCargaAcoplado
            // 
            this.btnTipoCargaAcoplado.Location = new System.Drawing.Point(583, 33);
            this.btnTipoCargaAcoplado.Name = "btnTipoCargaAcoplado";
            this.btnTipoCargaAcoplado.Size = new System.Drawing.Size(186, 21);
            this.btnTipoCargaAcoplado.TabIndex = 36;
            this.btnTipoCargaAcoplado.Text = "Gestion de Tipo Carga - Acoplado";
            this.btnTipoCargaAcoplado.UseVisualStyleBackColor = true;
            this.btnTipoCargaAcoplado.Click += new System.EventHandler(this.btnTipoCargaCamion_Click);
            // 
            // pnlAcopladoBusquedaFecha
            // 
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.dtpAcopladoBusquedaHasta);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.label2);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.dtpAcopladoBusquedaDesde);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.label8);
            this.pnlAcopladoBusquedaFecha.Location = new System.Drawing.Point(358, 69);
            this.pnlAcopladoBusquedaFecha.Name = "pnlAcopladoBusquedaFecha";
            this.pnlAcopladoBusquedaFecha.Size = new System.Drawing.Size(333, 27);
            this.pnlAcopladoBusquedaFecha.TabIndex = 20;
            // 
            // dtpAcopladoBusquedaHasta
            // 
            this.dtpAcopladoBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAcopladoBusquedaHasta.Location = new System.Drawing.Point(226, 3);
            this.dtpAcopladoBusquedaHasta.Name = "dtpAcopladoBusquedaHasta";
            this.dtpAcopladoBusquedaHasta.Size = new System.Drawing.Size(104, 21);
            this.dtpAcopladoBusquedaHasta.TabIndex = 4;
            this.dtpAcopladoBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpAcopladoBusquedaDesde_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(190, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "hasta";
            // 
            // dtpAcopladoBusquedaDesde
            // 
            this.dtpAcopladoBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAcopladoBusquedaDesde.Location = new System.Drawing.Point(80, 3);
            this.dtpAcopladoBusquedaDesde.Name = "dtpAcopladoBusquedaDesde";
            this.dtpAcopladoBusquedaDesde.Size = new System.Drawing.Size(104, 21);
            this.dtpAcopladoBusquedaDesde.TabIndex = 2;
            this.dtpAcopladoBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpAcopladoBusquedaDesde_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Buscar  desde ";
            // 
            // pnlAcopladoBusquedaTexto
            // 
            this.pnlAcopladoBusquedaTexto.Controls.Add(this.tbBusquedaAcoplado);
            this.pnlAcopladoBusquedaTexto.Controls.Add(this.label12);
            this.pnlAcopladoBusquedaTexto.Location = new System.Drawing.Point(7, 39);
            this.pnlAcopladoBusquedaTexto.Name = "pnlAcopladoBusquedaTexto";
            this.pnlAcopladoBusquedaTexto.Size = new System.Drawing.Size(257, 32);
            this.pnlAcopladoBusquedaTexto.TabIndex = 19;
            // 
            // tbBusquedaAcoplado
            // 
            this.tbBusquedaAcoplado.Location = new System.Drawing.Point(58, 11);
            this.tbBusquedaAcoplado.MaxLength = 100;
            this.tbBusquedaAcoplado.Name = "tbBusquedaAcoplado";
            this.tbBusquedaAcoplado.ShortcutsEnabled = false;
            this.tbBusquedaAcoplado.Size = new System.Drawing.Size(196, 21);
            this.tbBusquedaAcoplado.TabIndex = 2;
            this.tbBusquedaAcoplado.TextChanged += new System.EventHandler(this.tbBusquedaUnidad_TextChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 14);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Buscar : ";
            // 
            // btnAcopladoVerDetalle
            // 
            this.btnAcopladoVerDetalle.Location = new System.Drawing.Point(694, 68);
            this.btnAcopladoVerDetalle.Name = "btnAcopladoVerDetalle";
            this.btnAcopladoVerDetalle.Size = new System.Drawing.Size(75, 31);
            this.btnAcopladoVerDetalle.TabIndex = 17;
            this.btnAcopladoVerDetalle.Text = "Ver detalles";
            this.btnAcopladoVerDetalle.UseVisualStyleBackColor = true;
            this.btnAcopladoVerDetalle.Click += new System.EventHandler(this.btnAcopladoNuevo_Click);
            // 
            // btnAcopladoMarcaYtipo
            // 
            this.btnAcopladoMarcaYtipo.Location = new System.Drawing.Point(582, 6);
            this.btnAcopladoMarcaYtipo.Name = "btnAcopladoMarcaYtipo";
            this.btnAcopladoMarcaYtipo.Size = new System.Drawing.Size(186, 21);
            this.btnAcopladoMarcaYtipo.TabIndex = 18;
            this.btnAcopladoMarcaYtipo.Text = "Gestion de Marcas y Modelos";
            this.btnAcopladoMarcaYtipo.UseVisualStyleBackColor = true;
            this.btnAcopladoMarcaYtipo.Click += new System.EventHandler(this.btnAcopladoMarcaYtipo_Click);
            // 
            // dgvAcoplado
            // 
            this.dgvAcoplado.AllowUserToAddRows = false;
            this.dgvAcoplado.AllowUserToDeleteRows = false;
            this.dgvAcoplado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAcoplado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAcoplado.Location = new System.Drawing.Point(6, 105);
            this.dgvAcoplado.MultiSelect = false;
            this.dgvAcoplado.Name = "dgvAcoplado";
            this.dgvAcoplado.ReadOnly = true;
            this.dgvAcoplado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcoplado.Size = new System.Drawing.Size(763, 314);
            this.dgvAcoplado.TabIndex = 13;
            // 
            // btnAcopladoNuevo
            // 
            this.btnAcopladoNuevo.Location = new System.Drawing.Point(6, 6);
            this.btnAcopladoNuevo.Name = "btnAcopladoNuevo";
            this.btnAcopladoNuevo.Size = new System.Drawing.Size(75, 31);
            this.btnAcopladoNuevo.TabIndex = 14;
            this.btnAcopladoNuevo.Text = "Nuevo";
            this.btnAcopladoNuevo.UseVisualStyleBackColor = true;
            this.btnAcopladoNuevo.Click += new System.EventHandler(this.btnAcopladoNuevo_Click);
            // 
            // btnAcopladoEliminar
            // 
            this.btnAcopladoEliminar.Location = new System.Drawing.Point(168, 6);
            this.btnAcopladoEliminar.Name = "btnAcopladoEliminar";
            this.btnAcopladoEliminar.Size = new System.Drawing.Size(75, 31);
            this.btnAcopladoEliminar.TabIndex = 16;
            this.btnAcopladoEliminar.Text = "Dar de baja";
            this.btnAcopladoEliminar.UseVisualStyleBackColor = true;
            this.btnAcopladoEliminar.Click += new System.EventHandler(this.btnAcopladoNuevo_Click);
            // 
            // cbAcopladoFiltrar
            // 
            this.cbAcopladoFiltrar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbAcopladoFiltrar.FormattingEnabled = true;
            this.cbAcopladoFiltrar.Items.AddRange(new object[] {
            "HABILITADOS",
            "TODOS",
            "DADOS DE BAJA",
            "FECHA DE ALTA"});
            this.cbAcopladoFiltrar.Location = new System.Drawing.Point(7, 74);
            this.cbAcopladoFiltrar.Name = "cbAcopladoFiltrar";
            this.cbAcopladoFiltrar.Size = new System.Drawing.Size(271, 21);
            this.cbAcopladoFiltrar.TabIndex = 10;
            this.cbAcopladoFiltrar.SelectedIndexChanged += new System.EventHandler(this.cbAcopladoFiltrar_SelectedIndexChanged);
            // 
            // btnAcopladoModificar
            // 
            this.btnAcopladoModificar.Location = new System.Drawing.Point(87, 6);
            this.btnAcopladoModificar.Name = "btnAcopladoModificar";
            this.btnAcopladoModificar.Size = new System.Drawing.Size(75, 31);
            this.btnAcopladoModificar.TabIndex = 15;
            this.btnAcopladoModificar.Text = "Modificar";
            this.btnAcopladoModificar.UseVisualStyleBackColor = true;
            this.btnAcopladoModificar.Click += new System.EventHandler(this.btnAcopladoNuevo_Click);
            // 
            // tbReportes
            // 
            this.tbReportes.Controls.Add(this.btnReporte9);
            this.tbReportes.Controls.Add(this.btnReporte8);
            this.tbReportes.Controls.Add(this.btnReporte7);
            this.tbReportes.Controls.Add(this.btnReporte6);
            this.tbReportes.Controls.Add(this.btnReporte5);
            this.tbReportes.Controls.Add(this.btnReporte4);
            this.tbReportes.Controls.Add(this.btnReporte3);
            this.tbReportes.Controls.Add(this.btnReporte10);
            this.tbReportes.Controls.Add(this.btnReporte2);
            this.tbReportes.Controls.Add(this.btnReporte1);
            this.tbReportes.Location = new System.Drawing.Point(4, 29);
            this.tbReportes.Name = "tbReportes";
            this.tbReportes.Padding = new System.Windows.Forms.Padding(3);
            this.tbReportes.Size = new System.Drawing.Size(813, 492);
            this.tbReportes.TabIndex = 5;
            this.tbReportes.Text = "REPORTES";
            this.tbReportes.UseVisualStyleBackColor = true;
            // 
            // btnReporte9
            // 
            this.btnReporte9.BackColor = System.Drawing.Color.LemonChiffon;
            this.btnReporte9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte9.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte9.Location = new System.Drawing.Point(605, 279);
            this.btnReporte9.Name = "btnReporte9";
            this.btnReporte9.Size = new System.Drawing.Size(185, 62);
            this.btnReporte9.TabIndex = 8;
            this.btnReporte9.Text = "MARCAS MAS UTILIZADAS - SEMIREMOLQUE";
            this.btnReporte9.UseVisualStyleBackColor = false;
            this.btnReporte9.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte8
            // 
            this.btnReporte8.BackColor = System.Drawing.Color.LemonChiffon;
            this.btnReporte8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte8.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte8.Location = new System.Drawing.Point(409, 279);
            this.btnReporte8.Name = "btnReporte8";
            this.btnReporte8.Size = new System.Drawing.Size(185, 62);
            this.btnReporte8.TabIndex = 7;
            this.btnReporte8.Text = "MARCAS MAS UTILIZADAS - REMOLQUE";
            this.btnReporte8.UseVisualStyleBackColor = false;
            this.btnReporte8.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte7
            // 
            this.btnReporte7.BackColor = System.Drawing.Color.LemonChiffon;
            this.btnReporte7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte7.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte7.Location = new System.Drawing.Point(213, 279);
            this.btnReporte7.Name = "btnReporte7";
            this.btnReporte7.Size = new System.Drawing.Size(185, 62);
            this.btnReporte7.TabIndex = 6;
            this.btnReporte7.Text = "MARCAS MAS UTILIZADAS - RIGIDO";
            this.btnReporte7.UseVisualStyleBackColor = false;
            this.btnReporte7.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte6
            // 
            this.btnReporte6.BackColor = System.Drawing.Color.LemonChiffon;
            this.btnReporte6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte6.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte6.Location = new System.Drawing.Point(17, 279);
            this.btnReporte6.Name = "btnReporte6";
            this.btnReporte6.Size = new System.Drawing.Size(185, 62);
            this.btnReporte6.TabIndex = 5;
            this.btnReporte6.Text = "MARCAS MAS UTILIZADAS - TRACTOCAMION";
            this.btnReporte6.UseVisualStyleBackColor = false;
            this.btnReporte6.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte5
            // 
            this.btnReporte5.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.btnReporte5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte5.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte5.Location = new System.Drawing.Point(549, 211);
            this.btnReporte5.Name = "btnReporte5";
            this.btnReporte5.Size = new System.Drawing.Size(241, 62);
            this.btnReporte5.TabIndex = 4;
            this.btnReporte5.Text = "TIPOS DE CARGA MAS SOLICITADOS - SEMIREMOLQUE";
            this.btnReporte5.UseVisualStyleBackColor = false;
            this.btnReporte5.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte4
            // 
            this.btnReporte4.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.btnReporte4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte4.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte4.Location = new System.Drawing.Point(283, 211);
            this.btnReporte4.Name = "btnReporte4";
            this.btnReporte4.Size = new System.Drawing.Size(241, 62);
            this.btnReporte4.TabIndex = 3;
            this.btnReporte4.Text = "TIPOS DE CARGA MAS SOLICITADOS - REMOLQUE";
            this.btnReporte4.UseVisualStyleBackColor = false;
            this.btnReporte4.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte3
            // 
            this.btnReporte3.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.btnReporte3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte3.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte3.Location = new System.Drawing.Point(17, 211);
            this.btnReporte3.Name = "btnReporte3";
            this.btnReporte3.Size = new System.Drawing.Size(241, 62);
            this.btnReporte3.TabIndex = 2;
            this.btnReporte3.Text = "TIPOS DE CARGA MAS SOLICITADOS - RIGIDO";
            this.btnReporte3.UseVisualStyleBackColor = false;
            this.btnReporte3.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte10
            // 
            this.btnReporte10.BackColor = System.Drawing.Color.Azure;
            this.btnReporte10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte10.Font = new System.Drawing.Font("Bahnschrift Condensed", 36F);
            this.btnReporte10.Location = new System.Drawing.Point(17, 347);
            this.btnReporte10.Name = "btnReporte10";
            this.btnReporte10.Size = new System.Drawing.Size(774, 132);
            this.btnReporte10.TabIndex = 9;
            this.btnReporte10.Text = "UNIDADES EN VIAJE";
            this.btnReporte10.UseVisualStyleBackColor = false;
            this.btnReporte10.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte2
            // 
            this.btnReporte2.BackColor = System.Drawing.Color.DarkKhaki;
            this.btnReporte2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte2.Font = new System.Drawing.Font("Bahnschrift", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte2.Location = new System.Drawing.Point(17, 152);
            this.btnReporte2.Name = "btnReporte2";
            this.btnReporte2.Size = new System.Drawing.Size(773, 53);
            this.btnReporte2.TabIndex = 1;
            this.btnReporte2.Text = "UNIDADES QUE MAS ASISTIERON A TALLER";
            this.btnReporte2.UseVisualStyleBackColor = false;
            this.btnReporte2.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // btnReporte1
            // 
            this.btnReporte1.BackColor = System.Drawing.Color.NavajoWhite;
            this.btnReporte1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnReporte1.Font = new System.Drawing.Font("Bahnschrift Condensed", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnReporte1.Location = new System.Drawing.Point(17, 14);
            this.btnReporte1.Name = "btnReporte1";
            this.btnReporte1.Size = new System.Drawing.Size(774, 132);
            this.btnReporte1.TabIndex = 0;
            this.btnReporte1.Text = "TIPOS DE CARGA MAS SOLICITADOS";
            this.btnReporte1.UseVisualStyleBackColor = false;
            this.btnReporte1.Click += new System.EventHandler(this.btnReporte1_Click);
            // 
            // frmOperaciones
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 527);
            this.Controls.Add(this.tpPedidos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOperaciones";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADMINISTRADOR OPERACIONES";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmOperaciones_FormClosing);
            this.Load += new System.EventHandler(this.frmGerencia_Load);
            this.tppPedidos.ResumeLayout(false);
            this.gbDatosPedido.ResumeLayout(false);
            this.gbDatosPedido.PerformLayout();
            this.gbClientePedido.ResumeLayout(false);
            this.gbClientePedido.PerformLayout();
            this.gbObservacionPedido.ResumeLayout(false);
            this.gbAcopladoPedido.ResumeLayout(false);
            this.gbAcopladoPedido.PerformLayout();
            this.gbCamionPedido.ResumeLayout(false);
            this.gbCamionPedido.PerformLayout();
            this.gbChoferPedido.ResumeLayout(false);
            this.gbChoferPedido.PerformLayout();
            this.gpPedidos.ResumeLayout(false);
            this.gpPedidos.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.tpPedidos.ResumeLayout(false);
            this.tpPersonal.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvChofer)).EndInit();
            this.gpUsuarios.ResumeLayout(false);
            this.gpUsuarios.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvUsuarios)).EndInit();
            this.gpRoles.ResumeLayout(false);
            this.gpRoles.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRoles)).EndInit();
            this.tbUnidades.ResumeLayout(false);
            this.tcCamiones.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.pnlCamionBusquedaFecha.ResumeLayout(false);
            this.pnlCamionBusquedaFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamion)).EndInit();
            this.pnlCamionBusquedaTexto.ResumeLayout(false);
            this.pnlCamionBusquedaTexto.PerformLayout();
            this.tcAcoplado.ResumeLayout(false);
            this.pnlAcopladoBusquedaFecha.ResumeLayout(false);
            this.pnlAcopladoBusquedaFecha.PerformLayout();
            this.pnlAcopladoBusquedaTexto.ResumeLayout(false);
            this.pnlAcopladoBusquedaTexto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcoplado)).EndInit();
            this.tbReportes.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TabPage tppPedidos;
        private System.Windows.Forms.GroupBox gpPedidos;
        private System.Windows.Forms.Label lbFechaHastaPedido;
        private System.Windows.Forms.Label lbFechaDesdePedido;
        private System.Windows.Forms.DateTimePicker dtpDesdePedido;
        private System.Windows.Forms.DateTimePicker dtpHastaPedido;
        private System.Windows.Forms.TextBox tbBuscarPedido;
        private System.Windows.Forms.Button btnEliminarPedido;
        private System.Windows.Forms.Button btnModificarPedido;
        private System.Windows.Forms.Button btnAñadirPedido;
        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.TabControl tpPedidos;
        private System.Windows.Forms.TabPage tpPersonal;
        private System.Windows.Forms.GroupBox gpUsuarios;
        private System.Windows.Forms.TextBox tbBuscarUsuario;
        private System.Windows.Forms.Button btnEliminarUsuario;
        private System.Windows.Forms.Button btnModificarUsuario;
        private System.Windows.Forms.Button btnAltaUsuario;
        private System.Windows.Forms.DataGridView dgvUsuarios;
        private System.Windows.Forms.GroupBox gpRoles;
        private System.Windows.Forms.TextBox tbBuscarRol;
        private System.Windows.Forms.Button btnEliminarRol;
        private System.Windows.Forms.Button btnModificarRol;
        private System.Windows.Forms.Button btnAñadirRol;
        private System.Windows.Forms.DataGridView dgvRoles;
        private System.Windows.Forms.GroupBox gbAcopladoPedido;
        private System.Windows.Forms.GroupBox gbCamionPedido;
        private System.Windows.Forms.GroupBox gbChoferPedido;
        private System.Windows.Forms.GroupBox gbObservacionPedido;
        private System.Windows.Forms.Label lbAnchoIntCamionPedido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbNombreChoferPedido;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbNumLegajoChofer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gbClientePedido;
        private System.Windows.Forms.Label lbCuilClientePedido;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lbNombreClientePedido;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lbAnchoExtCamionPedido;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbAlturaCamionPedido;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbLongitudCamionPedido;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbAnchoExtRemolquePedido;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lbAnchoIntRemolquePedido;
        private System.Windows.Forms.Label lbAlturaRemolquePedido;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lbLongitudRemolquePedido;
        private System.Windows.Forms.Label lbObservacionesPedido;
        private System.Windows.Forms.GroupBox gbDatosPedido;
        private System.Windows.Forms.Label lbKmViajePedido;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbVolumenPedido;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbPesoNetoPedido;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbEnganchePedido;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox tbBuscarChofer;
        private System.Windows.Forms.Button btnEliminarChofer;
        private System.Windows.Forms.Button btnModificarChofer;
        private System.Windows.Forms.Button btnAñadirChofer;
        private System.Windows.Forms.DataGridView dgvChofer;
        private System.Windows.Forms.TabPage tbUnidades;
        private System.Windows.Forms.TabControl tcCamiones;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Button btnCamionVerDetalles;
        private System.Windows.Forms.Button btnCamionMarcaYtipo;
        private System.Windows.Forms.DataGridView dgvCamion;
        private System.Windows.Forms.Panel pnlCamionBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpCamionBusquedaHasta;
        private System.Windows.Forms.Label lblBuscarCamionBuscarFechaHasta;
        private System.Windows.Forms.DateTimePicker dtpCamionBusquedaDesde;
        private System.Windows.Forms.Label lblCamionBuscarFechaDesde;
        private System.Windows.Forms.Button btnCamionNuevo;
        private System.Windows.Forms.Button btnCamionEliminar;
        private System.Windows.Forms.ComboBox cbCamionFiltros;
        private System.Windows.Forms.Button btnCamionModificar;
        private System.Windows.Forms.Panel pnlCamionBusquedaTexto;
        private System.Windows.Forms.Label lblCamionBuscarTexto;
        private System.Windows.Forms.TextBox tbBusquedaCamion;
        private System.Windows.Forms.Button btnTipoCargaCamion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbTipoCarga;
        private System.Windows.Forms.TabPage tbReportes;
        private System.Windows.Forms.Button btnReporte10;
        private System.Windows.Forms.Button btnReporte1;
        private System.Windows.Forms.Button btnReporte9;
        private System.Windows.Forms.Button btnReporte8;
        private System.Windows.Forms.Button btnReporte7;
        private System.Windows.Forms.Button btnReporte6;
        private System.Windows.Forms.Button btnReporte5;
        private System.Windows.Forms.Button btnReporte4;
        private System.Windows.Forms.Button btnReporte3;
        private System.Windows.Forms.TabPage tcAcoplado;
        private System.Windows.Forms.Button btnTipoCargaAcoplado;
        private System.Windows.Forms.Panel pnlAcopladoBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpAcopladoBusquedaHasta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpAcopladoBusquedaDesde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlAcopladoBusquedaTexto;
        private System.Windows.Forms.TextBox tbBusquedaAcoplado;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button btnAcopladoVerDetalle;
        private System.Windows.Forms.Button btnAcopladoMarcaYtipo;
        private System.Windows.Forms.DataGridView dgvAcoplado;
        private System.Windows.Forms.Button btnAcopladoNuevo;
        private System.Windows.Forms.Button btnAcopladoEliminar;
        private System.Windows.Forms.ComboBox cbAcopladoFiltrar;
        private System.Windows.Forms.Button btnAcopladoModificar;
        private System.Windows.Forms.Button btnReporte2;
    }
}