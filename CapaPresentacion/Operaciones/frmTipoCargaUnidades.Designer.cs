﻿namespace CapaPresentacion.Operaciones
{
    partial class frmTipoCargaUnidades
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbTipoCarga = new System.Windows.Forms.TextBox();
            this.pnlAbmTipoCarga = new System.Windows.Forms.Panel();
            this.tbTipoCargaEliminar = new System.Windows.Forms.Button();
            this.btnTipoCargaModificar = new System.Windows.Forms.Button();
            this.dgvTipoCarga = new System.Windows.Forms.DataGridView();
            this.btnTipoCargaNueva = new System.Windows.Forms.Button();
            this.pnlAbmTipoCarga.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipoCarga)).BeginInit();
            this.SuspendLayout();
            // 
            // tbTipoCarga
            // 
            this.tbTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTipoCarga.Location = new System.Drawing.Point(12, 346);
            this.tbTipoCarga.MaxLength = 100;
            this.tbTipoCarga.Multiline = true;
            this.tbTipoCarga.Name = "tbTipoCarga";
            this.tbTipoCarga.ShortcutsEnabled = false;
            this.tbTipoCarga.Size = new System.Drawing.Size(304, 28);
            this.tbTipoCarga.TabIndex = 13;
            // 
            // pnlAbmTipoCarga
            // 
            this.pnlAbmTipoCarga.Controls.Add(this.tbTipoCargaEliminar);
            this.pnlAbmTipoCarga.Controls.Add(this.btnTipoCargaModificar);
            this.pnlAbmTipoCarga.Location = new System.Drawing.Point(93, 306);
            this.pnlAbmTipoCarga.Name = "pnlAbmTipoCarga";
            this.pnlAbmTipoCarga.Size = new System.Drawing.Size(223, 34);
            this.pnlAbmTipoCarga.TabIndex = 12;
            // 
            // tbTipoCargaEliminar
            // 
            this.tbTipoCargaEliminar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbTipoCargaEliminar.Location = new System.Drawing.Point(148, 4);
            this.tbTipoCargaEliminar.Name = "tbTipoCargaEliminar";
            this.tbTipoCargaEliminar.Size = new System.Drawing.Size(75, 23);
            this.tbTipoCargaEliminar.TabIndex = 2;
            this.tbTipoCargaEliminar.Text = "Eliminar";
            this.tbTipoCargaEliminar.UseVisualStyleBackColor = true;
            this.tbTipoCargaEliminar.Click += new System.EventHandler(this.btnTipoCargaNueva_Click);
            // 
            // btnTipoCargaModificar
            // 
            this.btnTipoCargaModificar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoCargaModificar.Location = new System.Drawing.Point(36, 4);
            this.btnTipoCargaModificar.Name = "btnTipoCargaModificar";
            this.btnTipoCargaModificar.Size = new System.Drawing.Size(75, 23);
            this.btnTipoCargaModificar.TabIndex = 1;
            this.btnTipoCargaModificar.Text = "Modificar";
            this.btnTipoCargaModificar.UseVisualStyleBackColor = true;
            this.btnTipoCargaModificar.Click += new System.EventHandler(this.btnTipoCargaNueva_Click);
            // 
            // dgvTipoCarga
            // 
            this.dgvTipoCarga.AllowUserToAddRows = false;
            this.dgvTipoCarga.AllowUserToDeleteRows = false;
            this.dgvTipoCarga.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTipoCarga.Location = new System.Drawing.Point(12, 12);
            this.dgvTipoCarga.MultiSelect = false;
            this.dgvTipoCarga.Name = "dgvTipoCarga";
            this.dgvTipoCarga.ReadOnly = true;
            this.dgvTipoCarga.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvTipoCarga.Size = new System.Drawing.Size(304, 288);
            this.dgvTipoCarga.TabIndex = 9;
            this.dgvTipoCarga.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTipoCarga_CellClick);
            // 
            // btnTipoCargaNueva
            // 
            this.btnTipoCargaNueva.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTipoCargaNueva.Location = new System.Drawing.Point(12, 310);
            this.btnTipoCargaNueva.Name = "btnTipoCargaNueva";
            this.btnTipoCargaNueva.Size = new System.Drawing.Size(75, 23);
            this.btnTipoCargaNueva.TabIndex = 10;
            this.btnTipoCargaNueva.Text = "Nueva";
            this.btnTipoCargaNueva.UseVisualStyleBackColor = true;
            this.btnTipoCargaNueva.Click += new System.EventHandler(this.btnTipoCargaNueva_Click);
            // 
            // frmTipoCargaUnidades
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(329, 378);
            this.Controls.Add(this.tbTipoCarga);
            this.Controls.Add(this.pnlAbmTipoCarga);
            this.Controls.Add(this.dgvTipoCarga);
            this.Controls.Add(this.btnTipoCargaNueva);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmTipoCargaUnidades";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "ABM Tipo Carga Unidades - Operaciones";
            this.Load += new System.EventHandler(this.frmTipoCargaUnidades_Load);
            this.pnlAbmTipoCarga.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTipoCarga)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbTipoCarga;
        private System.Windows.Forms.Panel pnlAbmTipoCarga;
        private System.Windows.Forms.Button tbTipoCargaEliminar;
        private System.Windows.Forms.Button btnTipoCargaModificar;
        private System.Windows.Forms.DataGridView dgvTipoCarga;
        private System.Windows.Forms.Button btnTipoCargaNueva;
    }
}