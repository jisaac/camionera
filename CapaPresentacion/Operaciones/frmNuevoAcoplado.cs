﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    //falta el label para mostrar la fecha de alta al modificar ,osea al cargar un acoplado en los lables o textbox
    public partial class frmNuevoAcoplado : Form
    {
        public frmNuevoAcoplado(string usuario,object acoplado,Boolean ver)
        {
            InitializeComponent();
            this.usuario = usuario;
            n_marca = new N_MarcaAcoplado(usuario);
            n_modelo = new N_ModeloAcoplado(usuario);
            n_acoplado= new N_Acoplado(usuario);
            n_tipoCarga = new N_TipoCarga(usuario);
            n_dominio = new N_Dominio(usuario);
            this.acopladoObjeto = acoplado;
            this.ver = ver;
        }
        object acopladoObjeto;////SI ESTE ES DISTINTO DE NULL ENTONCES LA ACCION SERA MODIFICAR,YA QUE ES ENVIA POR PARAMETRO EL ACOPLADO A MODIFICAR
        string dominio_viejo;//Se almacena el dominio viejo por si se modifica
        string usuario;
        Boolean ver;
        MisFunciones funciones = new MisFunciones();
        E_Acoplado acoplado;
        E_MarcaAcoplado e_marca;
        E_ModeloAcoplado e_modelo;
        N_MarcaAcoplado n_marca;
        N_ModeloAcoplado n_modelo;
        N_Acoplado n_acoplado;
        E_TipoCarga e_tipoCarga;
        N_TipoCarga n_tipoCarga;
        N_Dominio n_dominio;

        private void frmNuevoAcoplado_Load(object sender, EventArgs e)
        {
            cbModelo.DropDownStyle = ComboBoxStyle.DropDownList;//hace que el combobox sea solo de lectura
            cbMarca.DropDownStyle = ComboBoxStyle.DropDownList;//hace que el combobox sea solo de lectura
            cbTipoCarga.DropDownStyle = ComboBoxStyle.DropDownList;//hace que el combobox sea solo de lectura
            cbMarca.DataSource = n_marca.listarComboBox();
            cbModelo.DataSource = n_modelo.listarComboBox(cbMarca.Text);
            cbTipoCarga.DataSource = n_tipoCarga.listarComboBox();
            if (acopladoObjeto != null)
            {
                acoplado = (E_Acoplado)acopladoObjeto;
                this.dominio_viejo = n_dominio.Retorna_Dominio(acoplado.Id_Dominio);
                #region Manejo de combobox
                e_marca = n_marca.retornaMarcaAcoplado(acoplado.Marca);
                cbMarca.SelectedIndex = cbMarca.Items.IndexOf(e_marca.Marca);
                cbModelo.DataSource = n_modelo.listarComboBox(cbMarca.Text);
                e_modelo = n_modelo.retornaModeloAcoplado(acoplado.Modelo);
                cbModelo.SelectedIndex = cbModelo.Items.IndexOf(e_modelo.Modelo);
                e_tipoCarga = n_tipoCarga.retornaTipoCarga(acoplado.Tipo_carga);
                cbTipoCarga.SelectedIndex = cbTipoCarga.Items.IndexOf(e_tipoCarga.Tipo);
                #endregion//////////
                tbDominio.Text = n_dominio.Retorna_Dominio(acoplado.Id_Dominio);
                tbAño.Text = acoplado.Año.ToString();
                if (acoplado.Tipo_acoplado == "REMOLQUE")
                    rbRemolque.Checked = true;
                else if (acoplado.Tipo_acoplado == "SEMIREMOLQUE")
                    rbSemiremolque.Checked = true;
                tbTara.Text = acoplado.Tara.ToString();
                tbKmReales.Text = acoplado.Km_unidad.ToString();
                tbKmService.Text = acoplado.Km_service.ToString();
                tbKmCambioNeumaticos.Text = acoplado.Km_cambio_neumaticos.ToString();
                tbAltura.Text = acoplado.Altura.ToString();
                tbAnchoInterior.Text = acoplado.Ancho_interior.ToString();
                tbAnchoExterior.Text = acoplado.Ancho_exterior.ToString();
                tbLongitud.Text = acoplado.Longitud.ToString();
                tbVolumen.Text = acoplado.Volumen.ToString();
                tbCapacidad.Text = acoplado.Capacidad_carga.ToString();
                if (ver)
                {
                    funciones.controlReadOnly(this);  //mostrar boton ok si se utiliza este form para mmostrar una unidad
                    btnOk.Text = "Ok";
                    btnSalir.Enabled =false;
                    btnSalir.Visible = false;
                }
            }
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (!ver)
            {
                if (string.IsNullOrWhiteSpace(tbDominio.Text) || string.IsNullOrWhiteSpace(tbAño.Text) ||
                    string.IsNullOrWhiteSpace(tbTara.Text) || string.IsNullOrWhiteSpace(tbCapacidad.Text) || string.IsNullOrWhiteSpace(tbKmReales.Text) ||
                    string.IsNullOrWhiteSpace(tbKmService.Text) || string.IsNullOrWhiteSpace(tbKmCambioNeumaticos.Text) || string.IsNullOrWhiteSpace(tbKmCambioNeumaticos.Text) ||
                    string.IsNullOrWhiteSpace(tbAltura.Text) || string.IsNullOrWhiteSpace(tbAnchoInterior.Text) || string.IsNullOrWhiteSpace(tbAnchoExterior.Text) ||
                    string.IsNullOrWhiteSpace(tbLongitud.Text) || string.IsNullOrWhiteSpace(tbVolumen.Text))
                    new frmMensajes("Error", "No es posible realizar la operación sin antes completar todos los campos").ShowDialog();
                else
                {
                    string dominio_ok = funciones.valida_dominio(tbDominio.Text);
                    if (dominio_ok == "CORRECTO")
                    {
                        List<string> resp = new List<string>();
                        int year = DateTime.Now.Year;
                        int diferencia = year - 50;
                        if (tbAño.TextLength == 4 && Convert.ToInt32(tbAño.Text) <= year && Convert.ToInt32(tbAño.Text) > diferencia)
                        {
                            if (acoplado == null)//crea una entidad acoplado si se inserta un nuevo acoplado
                            {
                                try
                                {
                                    acoplado = new E_Acoplado();
                                    acoplado.Marca = int.Parse(n_marca.retornaId(cbMarca.Text));
                                    acoplado.Modelo = int.Parse(n_modelo.retornaId(cbModelo.Text));
                                    acoplado.Año = Convert.ToInt32(tbAño.Text);
                                    acoplado.Tipo_carga = n_tipoCarga.retornaId(cbTipoCarga.Text);
                                    if (rbRemolque.Checked)
                                        acoplado.Tipo_acoplado = "REMOLQUE";
                                    else if (rbSemiremolque.Checked)
                                        acoplado.Tipo_acoplado = "SEMIREMOLQUE";
                                    acoplado.Tara = float.Parse(tbTara.Text);
                                    acoplado.Km_unidad = float.Parse(tbKmReales.Text);
                                    acoplado.Km_service = float.Parse(tbKmService.Text);
                                    acoplado.Km_cambio_neumaticos = float.Parse(tbKmCambioNeumaticos.Text);
                                    acoplado.Altura = float.Parse(tbAltura.Text);
                                    acoplado.Ancho_interior = float.Parse(tbAnchoInterior.Text);
                                    acoplado.Ancho_exterior = float.Parse(tbAnchoExterior.Text);
                                    acoplado.Longitud = float.Parse(tbLongitud.Text);
                                    acoplado.Volumen = float.Parse(tbVolumen.Text);
                                    acoplado.Capacidad_carga = float.Parse(tbCapacidad.Text);
                                    acoplado.Fecha_alta = DateTime.Now;
                                    resp = n_acoplado.insertar(acoplado, tbDominio.Text);
                                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                                    this.Hide();
                                }
                                catch (Exception ex)
                                {
                                    new frmMensajes("Error", ex.Message).ShowDialog();
                                }
                            }
                            else
                            {
                                try
                                {
                                    this.dominio_viejo = tbDominio.Text;
                                    acoplado.Marca = int.Parse(n_marca.retornaId(cbMarca.Text));
                                    acoplado.Modelo = int.Parse(n_modelo.retornaId(cbModelo.Text));
                                    acoplado.Año = Convert.ToInt32(tbAño.Text);
                                    acoplado.Tipo_carga = n_tipoCarga.retornaId(cbTipoCarga.Text);
                                    if (rbRemolque.Checked)
                                        acoplado.Tipo_acoplado = "REMOLQUE";
                                    else if(rbSemiremolque.Checked)
                                        acoplado.Tipo_acoplado = "SEMIREMOLQUE";
                                    acoplado.Tara = float.Parse(tbTara.Text);
                                    acoplado.Km_unidad = float.Parse(tbKmReales.Text);
                                    acoplado.Km_service = float.Parse(tbKmService.Text);
                                    acoplado.Km_cambio_neumaticos = float.Parse(tbKmCambioNeumaticos.Text);
                                    acoplado.Altura = float.Parse(tbAltura.Text);
                                    acoplado.Ancho_interior = float.Parse(tbAnchoInterior.Text);
                                    acoplado.Ancho_exterior = float.Parse(tbAnchoExterior.Text);
                                    acoplado.Longitud = float.Parse(tbLongitud.Text);
                                    acoplado.Volumen = float.Parse(tbVolumen.Text);
                                    acoplado.Capacidad_carga = float.Parse(tbCapacidad.Text);
                                    resp = n_acoplado.modificar(acoplado, dominio_viejo, "modificacion de datos por administrador");
                                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                                    this.Hide();
                                }
                                catch (Exception ex)
                                {
                                    new frmMensajes("Error", ex.Message).ShowDialog();
                                }
                            }
                        }
                        else
                            new frmMensajes("Error", "Año ingresado invalido, recuerdo que debe ser mayor a "+diferencia+" y menor o igual que "+year+" ").ShowDialog();
                    }
                    else
                        new frmMensajes("Error", "Formato incorrecto, recuerde que el formato es AA999AA ó AAA999").ShowDialog();
                }    
            }
            else
            {
                this.Close();
            }
    
        }

        private void cbMarca_SelectedIndexChanged(object sender, EventArgs e)
        {
            cbModelo.DataSource = n_modelo.listarComboBox(cbMarca.Text);
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbAño_KeyPress(object sender, KeyPressEventArgs e)
        {
            funciones.solo_numero(sender, e);
        }
    }
}
