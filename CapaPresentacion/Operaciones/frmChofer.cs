﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaEntidades;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Operaciones
{
    public partial class frmChofer : Form
    {
        string usuario;
        N_Chofer n_chofer;
        E_Chofer e_chofer;
        actualizaGrillaChoferes actualizaChoferes;
        MisFunciones funcion;
        public frmChofer(string usuario, E_Chofer e_chofer, Delegate actualizaChoferes)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.e_chofer = e_chofer;
            n_chofer = new N_Chofer(usuario);
            funcion = new MisFunciones();
            this.actualizaChoferes = (actualizaGrillaChoferes)actualizaChoferes;
            if (e_chofer != null)
                cargaChoferModifica(e_chofer);
            else
            {
                cbEstado.Checked = true;
                cbEstado.Enabled = false;
            }
        }
        private void cargaChoferModifica(E_Chofer e_chofer)
        {
            tbNum_legajo.Text = e_chofer.Num_legajo.ToString();
            tbNombre.Text = e_chofer.Nombre;
            tbApellido.Text = e_chofer.Apellido;
            tbNum_cedula.Text = e_chofer.Num_cedula;
            tbDireccion.Text = e_chofer.Direccion;
            tbTel_personal.Text = e_chofer.Tel_personal;
            tbTel_Trabajo.Text = e_chofer.Tel_trabajo;
            if (e_chofer.Estado == "HABILITADO")
                cbEstado.Checked = true;
            else
                cbEstado.Checked = false;
            tbNum_cedula.Enabled = false;
            tbNum_legajo.Enabled = false;
        }
        private void btnGuardar_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(tbNum_legajo.Text) || string.IsNullOrWhiteSpace(tbNombre.Text) || string.IsNullOrWhiteSpace(tbApellido.Text) || string.IsNullOrWhiteSpace(tbNum_cedula.Text) 
                || string.IsNullOrWhiteSpace(tbDireccion.Text) || string.IsNullOrWhiteSpace(tbTel_personal.Text) || string.IsNullOrWhiteSpace(tbTel_Trabajo.Text))
                new frmMensajes("Error", "No es posible realizar la operación sin antes completar todos los campos").ShowDialog();
            else
            {
                if (e_chofer != null)
                {
                    try
                    {
                        e_chofer.Num_legajo = Convert.ToInt32(tbNum_legajo.Text);
                        e_chofer.Nombre = tbNombre.Text.ToUpper();
                        e_chofer.Apellido = tbApellido.Text.ToUpper();
                        e_chofer.Num_cedula = tbNum_cedula.Text.ToUpper();
                        e_chofer.Direccion = tbDireccion.Text.ToUpper();
                        e_chofer.Tel_personal = tbTel_personal.Text.ToUpper();
                        e_chofer.Tel_trabajo = tbTel_Trabajo.Text.ToUpper();
                        if (cbEstado.Checked)
                            e_chofer.Estado = "HABILITADO";
                        else
                            e_chofer.Estado = "DESHABILITADO";
                        List<string> resp = n_chofer.modificar(e_chofer);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        new frmMensajes("Error", ex.Message).ShowDialog();
                    }
                    tbNum_cedula.Enabled = true;
                    tbNum_legajo.Enabled = true;
                }
                else
                {
                    try
                    {
                        e_chofer = new E_Chofer();
                        e_chofer.Num_legajo = Convert.ToInt32(tbNum_legajo.Text);
                        e_chofer.Nombre = tbNombre.Text.ToUpper();
                        e_chofer.Apellido = tbApellido.Text.ToUpper();
                        e_chofer.Num_cedula = tbNum_cedula.Text.ToUpper();
                        e_chofer.Direccion = tbDireccion.Text.ToUpper();
                        e_chofer.Tel_personal = tbTel_personal.Text.ToUpper();
                        e_chofer.Tel_trabajo = tbTel_Trabajo.Text.ToUpper();
                        List<string> resp = n_chofer.insertar(e_chofer);
                        new frmMensajes(resp[0],resp[1]).ShowDialog();
                    }
                    catch (Exception ex)
                    {
                        new frmMensajes("Error", ex.Message).ShowDialog();
                    }
                }
                this.actualizaChoferes();
                tbNum_legajo.Clear();
                tbNombre.Clear();
                tbApellido.Clear();
                tbNum_cedula.Clear();
                tbDireccion.Clear();
                tbTel_personal.Clear();
                tbTel_Trabajo.Clear();
                tbNum_legajo.Focus();
            }
        }

        private void tbNum_legajo_KeyPress(object sender, KeyPressEventArgs e)
        {
            funcion.solo_numero(sender, e);
        }

        private void tbNombre_KeyPress(object sender, KeyPressEventArgs e)
        {
            funcion.solo_letras(sender, e);
        }
    }
}
