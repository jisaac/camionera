﻿namespace CapaPresentacion.Operaciones
{
    partial class frmUsuario
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbEstado = new System.Windows.Forms.CheckBox();
            this.lbEstado = new System.Windows.Forms.Label();
            this.tbNumLegajo = new System.Windows.Forms.TextBox();
            this.lbUsuario = new System.Windows.Forms.Label();
            this.lbNumLegajo = new System.Windows.Forms.Label();
            this.lbClave = new System.Windows.Forms.Label();
            this.btnGuardar = new System.Windows.Forms.Button();
            this.lbRol = new System.Windows.Forms.Label();
            this.cbRol = new System.Windows.Forms.ComboBox();
            this.tbUsuario = new System.Windows.Forms.TextBox();
            this.tbClave = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // cbEstado
            // 
            this.cbEstado.AutoSize = true;
            this.cbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbEstado.Location = new System.Drawing.Point(89, 116);
            this.cbEstado.Name = "cbEstado";
            this.cbEstado.Size = new System.Drawing.Size(15, 14);
            this.cbEstado.TabIndex = 5;
            this.cbEstado.UseVisualStyleBackColor = true;
            // 
            // lbEstado
            // 
            this.lbEstado.AutoSize = true;
            this.lbEstado.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEstado.Location = new System.Drawing.Point(12, 117);
            this.lbEstado.Name = "lbEstado";
            this.lbEstado.Size = new System.Drawing.Size(42, 13);
            this.lbEstado.TabIndex = 40;
            this.lbEstado.Text = "Estado";
            // 
            // tbNumLegajo
            // 
            this.tbNumLegajo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbNumLegajo.Location = new System.Drawing.Point(89, 58);
            this.tbNumLegajo.MaxLength = 100;
            this.tbNumLegajo.Name = "tbNumLegajo";
            this.tbNumLegajo.ShortcutsEnabled = false;
            this.tbNumLegajo.Size = new System.Drawing.Size(158, 21);
            this.tbNumLegajo.TabIndex = 3;
            this.tbNumLegajo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbNumLegajo_KeyPress);
            // 
            // lbUsuario
            // 
            this.lbUsuario.AutoSize = true;
            this.lbUsuario.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsuario.Location = new System.Drawing.Point(12, 9);
            this.lbUsuario.Name = "lbUsuario";
            this.lbUsuario.Size = new System.Drawing.Size(46, 13);
            this.lbUsuario.TabIndex = 31;
            this.lbUsuario.Text = "Usuario";
            // 
            // lbNumLegajo
            // 
            this.lbNumLegajo.AutoSize = true;
            this.lbNumLegajo.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumLegajo.Location = new System.Drawing.Point(12, 61);
            this.lbNumLegajo.Name = "lbNumLegajo";
            this.lbNumLegajo.Size = new System.Drawing.Size(82, 13);
            this.lbNumLegajo.TabIndex = 39;
            this.lbNumLegajo.Text = "Numero Legajo";
            // 
            // lbClave
            // 
            this.lbClave.AutoSize = true;
            this.lbClave.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbClave.Location = new System.Drawing.Point(12, 35);
            this.lbClave.Name = "lbClave";
            this.lbClave.Size = new System.Drawing.Size(35, 13);
            this.lbClave.TabIndex = 32;
            this.lbClave.Text = "Clave";
            // 
            // btnGuardar
            // 
            this.btnGuardar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.Location = new System.Drawing.Point(117, 134);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(130, 29);
            this.btnGuardar.TabIndex = 6;
            this.btnGuardar.Text = "Guardar";
            this.btnGuardar.UseVisualStyleBackColor = true;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // lbRol
            // 
            this.lbRol.AutoSize = true;
            this.lbRol.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbRol.Location = new System.Drawing.Point(12, 89);
            this.lbRol.Name = "lbRol";
            this.lbRol.Size = new System.Drawing.Size(23, 13);
            this.lbRol.TabIndex = 34;
            this.lbRol.Text = "Rol";
            // 
            // cbRol
            // 
            this.cbRol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbRol.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbRol.FormattingEnabled = true;
            this.cbRol.Location = new System.Drawing.Point(89, 86);
            this.cbRol.Name = "cbRol";
            this.cbRol.Size = new System.Drawing.Size(158, 21);
            this.cbRol.TabIndex = 4;
            // 
            // tbUsuario
            // 
            this.tbUsuario.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbUsuario.Location = new System.Drawing.Point(89, 6);
            this.tbUsuario.MaxLength = 100;
            this.tbUsuario.Name = "tbUsuario";
            this.tbUsuario.ShortcutsEnabled = false;
            this.tbUsuario.Size = new System.Drawing.Size(158, 21);
            this.tbUsuario.TabIndex = 1;
            this.tbUsuario.Tag = "1";
            // 
            // tbClave
            // 
            this.tbClave.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbClave.Location = new System.Drawing.Point(89, 32);
            this.tbClave.MaxLength = 100;
            this.tbClave.Name = "tbClave";
            this.tbClave.PasswordChar = '*';
            this.tbClave.ShortcutsEnabled = false;
            this.tbClave.Size = new System.Drawing.Size(158, 21);
            this.tbClave.TabIndex = 2;
            // 
            // frmUsuario
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(255, 170);
            this.Controls.Add(this.cbEstado);
            this.Controls.Add(this.lbEstado);
            this.Controls.Add(this.tbNumLegajo);
            this.Controls.Add(this.lbUsuario);
            this.Controls.Add(this.lbNumLegajo);
            this.Controls.Add(this.lbClave);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lbRol);
            this.Controls.Add(this.cbRol);
            this.Controls.Add(this.tbUsuario);
            this.Controls.Add(this.tbClave);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmUsuario";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Alta Usuario - Operaciones";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox cbEstado;
        private System.Windows.Forms.Label lbEstado;
        private System.Windows.Forms.TextBox tbNumLegajo;
        private System.Windows.Forms.Label lbUsuario;
        private System.Windows.Forms.Label lbNumLegajo;
        private System.Windows.Forms.Label lbClave;
        private System.Windows.Forms.Button btnGuardar;
        private System.Windows.Forms.Label lbRol;
        private System.Windows.Forms.ComboBox cbRol;
        private System.Windows.Forms.TextBox tbUsuario;
        private System.Windows.Forms.TextBox tbClave;
    }
}