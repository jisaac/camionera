﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

/// 1. falta controlar los botones de abm en modelo y marca (currentrow null)
/// 2.busqueda en marca 
/// 3.busqueda en modelo 
///4. que no permita abrir nuevo camion si no hay marcas cargadas 
namespace CapaPresentacion
{
    public partial class frmCamionMarcaModeloTipo : Form
    {
        public frmCamionMarcaModeloTipo(string usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.e_marca = new E_MarcaCamion();
            this.e_modelo = new E_ModeloCamion();
            n_marca = new N_MarcaCamion(usuario);// si no esta instancioado lo instancio
            n_modelo = new N_ModeloCamion(usuario);
            n_tipoCarga = new N_TipoCarga(usuario);
        }
        string usuario;
        N_MarcaCamion n_marca;
        N_ModeloCamion n_modelo;
        N_TipoCarga n_tipoCarga;
        E_ModeloCamion e_modelo;
        E_MarcaCamion e_marca;


        private void frmCamionMarcaModeloTipo_Load(object sender, EventArgs e)
        {
            pnlMarcaAbm.Enabled = false;//invisible la opcion de mod o bajar una marca si aun no se ha hecho un click en alguna fila
            pnlModeloAbm.Enabled = false;//invisible la opcion de mod o bajar un modelo si aun no se ha hecho un click en alguna fila

            marcaTraerTodo();
        }
        #region funciones internas
        //EN DGV todas las marcas haciendo uso de modeloTraerTodo
        void marcaTraerTodo()
        {
            DataTable dt = n_marca.listarTodos();
            if (dt != null && dt.Rows.Count > 0)
            {

                dgvMarca.DataSource = dt;
                dgvMarca.Rows[0].Selected = true;
                dgvMarca.Columns[0].Visible = false;
                dgvMarca.Columns[2].Visible = false;
                dgvMarca.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                //selecciono la celda actual
                modeloTraerTodo(dgvMarca.Rows[0].Cells[0].Value.ToString());
            }
            pnlMarcaAbm.Enabled = false;
            pnlModeloAbm.Enabled = false;


        }
        //EN DGV todos los modelos de una marca especifica
        void modeloTraerTodo(string idMarca)
        {
            if (dgvMarca.Rows.Count > 0)
            {
                DataTable dt = n_modelo.listarTodos(idMarca);

                if (dt.Rows.Count > 0)
                {
                    dgvModelo.DataSource = dt;
                    dgvModelo.Rows[0].Selected = true;
                    dgvModelo.Columns[0].Visible = false;
                    dgvModelo.Columns[2].Visible = false;
                    dgvModelo.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
                pnlModeloAbm.Enabled = false;
            }
        }
        #endregion

        #region marca
        private void btnMarcaNueva_Click(object sender, EventArgs e)
        {
            List<string> resp;//para almacenar la respuesta del resultado de cada accion
            if (sender == btnMarcaNueva)
            {
                try
                {
                    e_marca.Marca = tbMarca.Text.ToUpper();
                    if (tbMarca.Text != String.Empty)
                    {
                        resp = n_marca.insertar(e_marca);
                        new frmMensajes(resp[0], tbMarca.Text.ToUpper() + " insertada").ShowDialog();
                    }
                    else
                    {
                        new frmMensajes("Error", "Verifque que los campos no esten vacios").ShowDialog();
                    }
                    marcaTraerTodo();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
               
            }
            if (sender == btnMarcaModificar)
            {
                try
                {
                    e_marca.Id = Convert.ToInt32(dgvMarca.CurrentRow.Cells[0].Value.ToString());
                    e_marca.Marca = tbMarca.Text.ToUpper();
                    if (tbMarca.Text != String.Empty)
                    {
                        resp = n_marca.modificar(e_marca);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    else
                    {
                        new frmMensajes("Error", "Verifque que los campos no esten vacios").ShowDialog();
                    }
                    marcaTraerTodo();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }

            }
        
            if (sender == btnMarcaEliminar)
            {
                try
                {
                    e_marca.Id = Convert.ToInt32(dgvMarca.CurrentRow.Cells[0].Value.ToString());
                    resp = n_marca.baja(e_marca);
                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                    marcaTraerTodo();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }

            }
            tbMarca.Clear();
        }
        #endregion
        #region modelo
        private void btnModeloNuevo_Click(object sender, EventArgs e)
        {
            List<string> resp;//para almacenar la respuesta del resultado de cada accion
            if (sender == btnModeloNuevo)
            {
                try
                {
                    if (dgvMarca.CurrentRow == null)
                        e_modelo.IdMarca = Convert.ToInt32(dgvMarca.Rows[0].Cells[0].Value.ToString());
                    else
                        e_modelo.IdMarca = Convert.ToInt32(dgvMarca.CurrentRow.Cells[0].Value.ToString());
                    e_modelo.Modelo = tbModelo.Text.ToUpper();
                    if (tbModelo.Text != String.Empty)
                    {
                        resp = n_modelo.insertar(e_modelo);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    else
                        new frmMensajes("Error", "Verifque que los campos no esten vacios").ShowDialog();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
                marcaTraerTodo();
            }
            if (sender == btnModeloModificar)
            {
                try
                {
                    e_modelo.Modelo = tbModelo.Text.ToUpper();
                    e_modelo.Id = Convert.ToInt32(dgvModelo.CurrentRow.Cells[0].Value.ToString());
                    if (tbModelo.Text != String.Empty)
                    {
                        resp = n_modelo.modificar(e_modelo);
                        new frmMensajes(resp[0], resp[1]).ShowDialog();
                    }
                    else
                        new frmMensajes("Error", "Verifque que los campos no esten vacios").ShowDialog();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
                marcaTraerTodo();
            }
            if (sender == btnModeloEliminar)
            {
                try
                {
                    e_modelo.Id = Convert.ToInt32(dgvModelo.CurrentRow.Cells[0].Value.ToString());
                    resp = n_modelo.baja(e_modelo);
                    new frmMensajes(resp[0], resp[1]).ShowDialog();
                }
                catch (Exception ex)
                {
                    new frmMensajes("Error", ex.Message).ShowDialog();
                }
                marcaTraerTodo();
            }
            tbModelo.Clear();
        }

        #endregion

     
        #region click en marca y modelo
            private void dgvMarca_CellClick(object sender, DataGridViewCellEventArgs e)
            {
                if (sender == dgvMarca)
                {
                    if (dgvMarca.CurrentRow != null)
                    {
                        pnlMarcaAbm.Enabled = true;
                        modeloTraerTodo(dgvMarca.CurrentRow.Cells[0].Value.ToString());
                    }
                    else
                    {
                        pnlMarcaAbm.Enabled = false;
                    }

                }
                if (sender == dgvModelo)
                {
                    if (dgvMarca.CurrentRow != null && dgvModelo.CurrentRow != null)
                    {
                        pnlModeloAbm.Enabled = true;
                    }
                    else
                    {
                        pnlModeloAbm.Enabled = false;
                    }

                }
            }
            #endregion
        
    }
}
