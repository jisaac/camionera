﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaEntidades;
using CapaPresentacion.Logistica;
using CapaPresentacion.Operaciones;
using CapaPresentacion.Mantenimiento;
using CapaPresentacion.FormsMsj;


namespace CapaPresentacion
{
    public partial class frmIngreso : Form
    {
        N_Usuario n_usuario;
        List<string> lista_usuario;
        public frmIngreso()
        {
            InitializeComponent();
            lista_usuario = new List<string>();
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            if (!new validaFunciones().Validar_Controles(this))
            {
                new frmMensajes("Advertencia", "Complete todos los campos para ingresar al sistema").ShowDialog();
            }
            else
            {
                n_usuario = new N_Usuario();
                lista_usuario = n_usuario.ingresar(tbUsuario.Text.ToUpper(), tbClave.Text.ToUpper());
                if (lista_usuario.Count>0 && lista_usuario[0] != null)
                {
                    if (lista_usuario[1] == "OPERACIONES" && lista_usuario[2] == "HABILITADO")
                    {
                        pnBtnIngresar.Visible = false;
                        pnBtnAdmin.Visible = true;
                    }
                    else if (lista_usuario[1] == "OPERACIONES" && lista_usuario[2] == "DESHABILITADO")
                        new frmMensajes("Error", "Usuario insertado se encuentra deshabilitado, no es posible acceder al sistema").ShowDialog();
                    else if (lista_usuario[1] == "LOGISTICA" && lista_usuario[2] == "HABILITADO")
                    {
                        this.Hide();
                        frmLogistica frmLogistica = new frmLogistica(lista_usuario[0]);
                        frmLogistica.FormClosing += new FormClosingEventHandler(open_login);
                        frmLogistica.ShowDialog();
                    }
                    else if (lista_usuario[1] == "LOGISTICA" && lista_usuario[2] == "DESHABILITADO")
                        new frmMensajes("Error", "Usuario insertado se encuentra deshabilitado, no es posible acceder al sistema").ShowDialog();
                    else if (lista_usuario[1] == "MANTENIMIENTO" && lista_usuario[2] == "HABILITADO")
                    {
                        this.Hide();
                        frmMantenimiento frmmantenimiento = new frmMantenimiento(lista_usuario[0]);
                        frmmantenimiento.FormClosing += new FormClosingEventHandler(open_login);
                        frmmantenimiento.ShowDialog();
                    }
                    else if (lista_usuario[1] == "MANTENIMIENTO" && lista_usuario[2] == "DESHABILITADO")
                        new frmMensajes("Error", "Usuario insertado se encuentra deshabilitado, no es posible acceder al sistema").ShowDialog();
                }
                else
                {
                    new frmMensajes("Error", "Ha insertado un usuario o clave incorrectos, por favor verifique").ShowDialog();
                    tbUsuario.Clear();
                    tbClave.Clear();
                }
            }     
        }

        private void btnLogistica_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogistica frmLogistica = new frmLogistica(lista_usuario[0]);
            frmLogistica.FormClosing += new FormClosingEventHandler(open_login);
            frmLogistica.ShowDialog();
            tbUsuario.Clear();
            tbClave.Clear();
            ocultar_paneles();
        }

        private void btnGerencia_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmOperaciones frmOperaciones = new frmOperaciones(lista_usuario[0]);
            frmOperaciones.FormClosing += new FormClosingEventHandler(open_login);
            frmOperaciones.ShowDialog();
            tbUsuario.Clear();
            tbClave.Clear();
            ocultar_paneles();
        }

        private void btnMantenimiento_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmMantenimiento frmMantenimiento = new frmMantenimiento(lista_usuario[0]);
            frmMantenimiento.FormClosing += new FormClosingEventHandler(open_login);
            frmMantenimiento.ShowDialog();
            tbUsuario.Clear();
            tbClave.Clear();
            ocultar_paneles();
        }

        private void btnSalir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void tbUsuario_Click(object sender, EventArgs e)
        {
            if (sender == tbUsuario)
            {
                tbUsuario.BackColor = Color.White;
                if(tbClave.TextLength==0 && tbUsuario.TextLength>0)
                    tbClave.BackColor = Color.IndianRed;
            }
            else if (sender == tbClave)
            {
                tbClave.BackColor = Color.White;
                if (tbUsuario.TextLength == 0 && tbClave.TextLength >0)
                    tbUsuario.BackColor = Color.IndianRed;
            }

        }
        private void ocultar_paneles()
        {
            pnBtnAdmin.Visible = false;
            pnBtnIngresar.Visible = true;
        }
        private void habilitar_paneles()
        {
            pnBtnAdmin.Visible = true;
            pnBtnIngresar.Visible = false;
        }

        private void frmIngreso_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        void open_login(object sender, FormClosingEventArgs e)
        {
            tbUsuario.Clear();
            tbClave.Clear();
            tbUsuario.Focus();
            this.Show();
        }
    }
}
