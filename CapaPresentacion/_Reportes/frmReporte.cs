﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

namespace CapaPresentacion._Reportes
{
    public partial class frmReporte : Form
    {
        int num_reporte;
        public frmReporte(int num_reporte)
        {
            InitializeComponent();
            this.num_reporte = num_reporte;
        }

        private void frmReporte_Load(object sender, EventArgs e)
        {
            ReportDocument crystalRep = new ReportDocument();
            if (num_reporte == 1)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rtTipoCargaTodos.rpt");
            else if (num_reporte == 2)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rpUnidadesTaller.rpt");
            else if (num_reporte == 3)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rpTipoCargaRigido.rpt");
            else if (num_reporte == 4)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rpTipoCargaRemolque.rpt");
            else if (num_reporte == 5)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rpTipoCargaSemiremolque.rpt");
            else if (num_reporte == 6)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rtMarcaTractocamion.rpt");
            else if (num_reporte == 7)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rtMarcaRigido.rpt");
            else if (num_reporte == 8)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rtMarcaRemolque.rpt");
            else if (num_reporte == 9)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rtMarcaSemiremolque.rpt");
            else if (num_reporte == 10)
                crystalRep.Load(@"C:\Users\jppab\Desktop\GitLab\camionera\CapaPresentacion\_Reportes\rtUnidadesEnViaje.rpt");
            crystalRep.SetDatabaseLogon("admin", "admin", @"ASUS-Pablo\SQLEXPRESS","logistica");
            crystalReportViewer1.ReportSource = crystalRep;
            crystalReportViewer1.Refresh();
        }
    }
}
