﻿namespace CapaPresentacion.Logistica
{
    partial class frmPedido
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbDatosPedido = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.lbTipoCarga = new System.Windows.Forms.Label();
            this.lbKmViajePedido = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbVolumenPedido = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lbPesoNetoPedido = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbEnganchePedido = new System.Windows.Forms.Label();
            this.gbClientePedido = new System.Windows.Forms.GroupBox();
            this.lbCuilClientePedido = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lbNombreClientePedido = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.gbObservacionPedido = new System.Windows.Forms.GroupBox();
            this.lbObservacionesPedido = new System.Windows.Forms.Label();
            this.gbAcopladoPedido = new System.Windows.Forms.GroupBox();
            this.lbAnchoExtRemolquePedido = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.lbAnchoIntRemolquePedido = new System.Windows.Forms.Label();
            this.lbAlturaRemolquePedido = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lbLongitudRemolquePedido = new System.Windows.Forms.Label();
            this.gbCamionPedido = new System.Windows.Forms.GroupBox();
            this.lbAnchoExtCamionPedido = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lbAlturaCamionPedido = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbLongitudCamionPedido = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbAnchoIntCamionPedido = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.gbChoferPedido = new System.Windows.Forms.GroupBox();
            this.lbNombreChoferPedido = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbNumLegajoChofer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.gpPedidos = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.nudPedidoDespues = new System.Windows.Forms.NumericUpDown();
            this.nudPedidosAntes = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.btnSeleccionarPedido = new System.Windows.Forms.Button();
            this.lbFechaHastaPedido = new System.Windows.Forms.Label();
            this.dgvPedidos = new System.Windows.Forms.DataGridView();
            this.dtpHastaPedido = new System.Windows.Forms.DateTimePicker();
            this.lbFechaDesdePedido = new System.Windows.Forms.Label();
            this.tbBuscarPedido = new System.Windows.Forms.TextBox();
            this.dtpDesdePedido = new System.Windows.Forms.DateTimePicker();
            this.gbDatosPedido.SuspendLayout();
            this.gbClientePedido.SuspendLayout();
            this.gbObservacionPedido.SuspendLayout();
            this.gbAcopladoPedido.SuspendLayout();
            this.gbCamionPedido.SuspendLayout();
            this.gbChoferPedido.SuspendLayout();
            this.gpPedidos.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudPedidoDespues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPedidosAntes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).BeginInit();
            this.SuspendLayout();
            // 
            // gbDatosPedido
            // 
            this.gbDatosPedido.Controls.Add(this.label4);
            this.gbDatosPedido.Controls.Add(this.lbTipoCarga);
            this.gbDatosPedido.Controls.Add(this.lbKmViajePedido);
            this.gbDatosPedido.Controls.Add(this.label14);
            this.gbDatosPedido.Controls.Add(this.lbVolumenPedido);
            this.gbDatosPedido.Controls.Add(this.label17);
            this.gbDatosPedido.Controls.Add(this.lbPesoNetoPedido);
            this.gbDatosPedido.Controls.Add(this.label10);
            this.gbDatosPedido.Controls.Add(this.label6);
            this.gbDatosPedido.Controls.Add(this.lbEnganchePedido);
            this.gbDatosPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbDatosPedido.Location = new System.Drawing.Point(8, 249);
            this.gbDatosPedido.Name = "gbDatosPedido";
            this.gbDatosPedido.Size = new System.Drawing.Size(390, 89);
            this.gbDatosPedido.TabIndex = 33;
            this.gbDatosPedido.TabStop = false;
            this.gbDatosPedido.Text = "Pedido";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1, 57);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 24;
            this.label4.Text = "Tipo Carga";
            // 
            // lbTipoCarga
            // 
            this.lbTipoCarga.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbTipoCarga.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTipoCarga.Location = new System.Drawing.Point(62, 50);
            this.lbTipoCarga.Name = "lbTipoCarga";
            this.lbTipoCarga.Size = new System.Drawing.Size(161, 26);
            this.lbTipoCarga.TabIndex = 25;
            this.lbTipoCarga.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbKmViajePedido
            // 
            this.lbKmViajePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbKmViajePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbKmViajePedido.Location = new System.Drawing.Point(294, 60);
            this.lbKmViajePedido.Name = "lbKmViajePedido";
            this.lbKmViajePedido.Size = new System.Drawing.Size(90, 19);
            this.lbKmViajePedido.TabIndex = 23;
            this.lbKmViajePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(233, 63);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(49, 13);
            this.label14.TabIndex = 22;
            this.label14.Text = "Km Viaje";
            // 
            // lbVolumenPedido
            // 
            this.lbVolumenPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbVolumenPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVolumenPedido.Location = new System.Drawing.Point(294, 35);
            this.lbVolumenPedido.Name = "lbVolumenPedido";
            this.lbVolumenPedido.Size = new System.Drawing.Size(90, 19);
            this.lbVolumenPedido.TabIndex = 21;
            this.lbVolumenPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(233, 38);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(49, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Volumen";
            // 
            // lbPesoNetoPedido
            // 
            this.lbPesoNetoPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbPesoNetoPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbPesoNetoPedido.Location = new System.Drawing.Point(294, 10);
            this.lbPesoNetoPedido.Name = "lbPesoNetoPedido";
            this.lbPesoNetoPedido.Size = new System.Drawing.Size(90, 19);
            this.lbPesoNetoPedido.TabIndex = 19;
            this.lbPesoNetoPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(2, 23);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 13);
            this.label10.TabIndex = 16;
            this.label10.Text = "Enganche";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(233, 13);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(58, 13);
            this.label6.TabIndex = 18;
            this.label6.Text = "Peso Neto";
            // 
            // lbEnganchePedido
            // 
            this.lbEnganchePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbEnganchePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbEnganchePedido.Location = new System.Drawing.Point(62, 16);
            this.lbEnganchePedido.Name = "lbEnganchePedido";
            this.lbEnganchePedido.Size = new System.Drawing.Size(161, 26);
            this.lbEnganchePedido.TabIndex = 17;
            this.lbEnganchePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbClientePedido
            // 
            this.gbClientePedido.Controls.Add(this.lbCuilClientePedido);
            this.gbClientePedido.Controls.Add(this.label31);
            this.gbClientePedido.Controls.Add(this.lbNombreClientePedido);
            this.gbClientePedido.Controls.Add(this.label33);
            this.gbClientePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbClientePedido.Location = new System.Drawing.Point(404, 249);
            this.gbClientePedido.Name = "gbClientePedido";
            this.gbClientePedido.Size = new System.Drawing.Size(390, 89);
            this.gbClientePedido.TabIndex = 34;
            this.gbClientePedido.TabStop = false;
            this.gbClientePedido.Text = "Cliente";
            // 
            // lbCuilClientePedido
            // 
            this.lbCuilClientePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbCuilClientePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCuilClientePedido.Location = new System.Drawing.Point(106, 50);
            this.lbCuilClientePedido.Name = "lbCuilClientePedido";
            this.lbCuilClientePedido.Size = new System.Drawing.Size(279, 26);
            this.lbCuilClientePedido.TabIndex = 3;
            this.lbCuilClientePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(9, 57);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 13);
            this.label31.TabIndex = 2;
            this.label31.Text = "Cuil";
            // 
            // lbNombreClientePedido
            // 
            this.lbNombreClientePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNombreClientePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreClientePedido.Location = new System.Drawing.Point(106, 16);
            this.lbNombreClientePedido.Name = "lbNombreClientePedido";
            this.lbNombreClientePedido.Size = new System.Drawing.Size(278, 26);
            this.lbNombreClientePedido.TabIndex = 1;
            this.lbNombreClientePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(8, 23);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "Nombre y Apellido";
            // 
            // gbObservacionPedido
            // 
            this.gbObservacionPedido.Controls.Add(this.lbObservacionesPedido);
            this.gbObservacionPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbObservacionPedido.Location = new System.Drawing.Point(8, 473);
            this.gbObservacionPedido.Name = "gbObservacionPedido";
            this.gbObservacionPedido.Size = new System.Drawing.Size(786, 65);
            this.gbObservacionPedido.TabIndex = 37;
            this.gbObservacionPedido.TabStop = false;
            this.gbObservacionPedido.Text = "Observaciones";
            // 
            // lbObservacionesPedido
            // 
            this.lbObservacionesPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbObservacionesPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbObservacionesPedido.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.lbObservacionesPedido.Location = new System.Drawing.Point(9, 16);
            this.lbObservacionesPedido.Name = "lbObservacionesPedido";
            this.lbObservacionesPedido.Size = new System.Drawing.Size(759, 36);
            this.lbObservacionesPedido.TabIndex = 16;
            this.lbObservacionesPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbAcopladoPedido
            // 
            this.gbAcopladoPedido.Controls.Add(this.lbAnchoExtRemolquePedido);
            this.gbAcopladoPedido.Controls.Add(this.label24);
            this.gbAcopladoPedido.Controls.Add(this.label18);
            this.gbAcopladoPedido.Controls.Add(this.lbAnchoIntRemolquePedido);
            this.gbAcopladoPedido.Controls.Add(this.lbAlturaRemolquePedido);
            this.gbAcopladoPedido.Controls.Add(this.label22);
            this.gbAcopladoPedido.Controls.Add(this.label20);
            this.gbAcopladoPedido.Controls.Add(this.lbLongitudRemolquePedido);
            this.gbAcopladoPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAcopladoPedido.Location = new System.Drawing.Point(404, 399);
            this.gbAcopladoPedido.Name = "gbAcopladoPedido";
            this.gbAcopladoPedido.Size = new System.Drawing.Size(390, 74);
            this.gbAcopladoPedido.TabIndex = 36;
            this.gbAcopladoPedido.TabStop = false;
            this.gbAcopladoPedido.Text = "Datos Remolque - Semiremolque";
            // 
            // lbAnchoExtRemolquePedido
            // 
            this.lbAnchoExtRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoExtRemolquePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoExtRemolquePedido.Location = new System.Drawing.Point(271, 14);
            this.lbAnchoExtRemolquePedido.Name = "lbAnchoExtRemolquePedido";
            this.lbAnchoExtRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoExtRemolquePedido.TabIndex = 25;
            this.lbAnchoExtRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(9, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(52, 13);
            this.label24.TabIndex = 18;
            this.label24.Text = "Ancho Int";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(189, 21);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(56, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Ancho Ext";
            // 
            // lbAnchoIntRemolquePedido
            // 
            this.lbAnchoIntRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoIntRemolquePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoIntRemolquePedido.Location = new System.Drawing.Point(68, 14);
            this.lbAnchoIntRemolquePedido.Name = "lbAnchoIntRemolquePedido";
            this.lbAnchoIntRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoIntRemolquePedido.TabIndex = 19;
            this.lbAnchoIntRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAlturaRemolquePedido
            // 
            this.lbAlturaRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAlturaRemolquePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAlturaRemolquePedido.Location = new System.Drawing.Point(271, 42);
            this.lbAlturaRemolquePedido.Name = "lbAlturaRemolquePedido";
            this.lbAlturaRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbAlturaRemolquePedido.TabIndex = 23;
            this.lbAlturaRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(9, 49);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 13);
            this.label22.TabIndex = 20;
            this.label22.Text = "Longitud";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(189, 49);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(38, 13);
            this.label20.TabIndex = 22;
            this.label20.Text = "Altura";
            // 
            // lbLongitudRemolquePedido
            // 
            this.lbLongitudRemolquePedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbLongitudRemolquePedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLongitudRemolquePedido.Location = new System.Drawing.Point(68, 42);
            this.lbLongitudRemolquePedido.Name = "lbLongitudRemolquePedido";
            this.lbLongitudRemolquePedido.Size = new System.Drawing.Size(113, 26);
            this.lbLongitudRemolquePedido.TabIndex = 21;
            this.lbLongitudRemolquePedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbCamionPedido
            // 
            this.gbCamionPedido.Controls.Add(this.lbAnchoExtCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label13);
            this.gbCamionPedido.Controls.Add(this.lbAlturaCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label11);
            this.gbCamionPedido.Controls.Add(this.lbLongitudCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label9);
            this.gbCamionPedido.Controls.Add(this.lbAnchoIntCamionPedido);
            this.gbCamionPedido.Controls.Add(this.label7);
            this.gbCamionPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbCamionPedido.Location = new System.Drawing.Point(8, 399);
            this.gbCamionPedido.Name = "gbCamionPedido";
            this.gbCamionPedido.Size = new System.Drawing.Size(390, 74);
            this.gbCamionPedido.TabIndex = 35;
            this.gbCamionPedido.TabStop = false;
            this.gbCamionPedido.Text = "Datos Camion";
            // 
            // lbAnchoExtCamionPedido
            // 
            this.lbAnchoExtCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoExtCamionPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoExtCamionPedido.Location = new System.Drawing.Point(271, 14);
            this.lbAnchoExtCamionPedido.Name = "lbAnchoExtCamionPedido";
            this.lbAnchoExtCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoExtCamionPedido.TabIndex = 13;
            this.lbAnchoExtCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(189, 21);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(56, 13);
            this.label13.TabIndex = 12;
            this.label13.Text = "Ancho Ext";
            // 
            // lbAlturaCamionPedido
            // 
            this.lbAlturaCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAlturaCamionPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAlturaCamionPedido.Location = new System.Drawing.Point(271, 42);
            this.lbAlturaCamionPedido.Name = "lbAlturaCamionPedido";
            this.lbAlturaCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbAlturaCamionPedido.TabIndex = 11;
            this.lbAlturaCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(189, 49);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 10;
            this.label11.Text = "Altura";
            // 
            // lbLongitudCamionPedido
            // 
            this.lbLongitudCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbLongitudCamionPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLongitudCamionPedido.Location = new System.Drawing.Point(68, 42);
            this.lbLongitudCamionPedido.Name = "lbLongitudCamionPedido";
            this.lbLongitudCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbLongitudCamionPedido.TabIndex = 9;
            this.lbLongitudCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(9, 49);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 8;
            this.label9.Text = "Longitud";
            // 
            // lbAnchoIntCamionPedido
            // 
            this.lbAnchoIntCamionPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbAnchoIntCamionPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAnchoIntCamionPedido.Location = new System.Drawing.Point(68, 14);
            this.lbAnchoIntCamionPedido.Name = "lbAnchoIntCamionPedido";
            this.lbAnchoIntCamionPedido.Size = new System.Drawing.Size(113, 26);
            this.lbAnchoIntCamionPedido.TabIndex = 7;
            this.lbAnchoIntCamionPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(9, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(52, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Ancho Int";
            // 
            // gbChoferPedido
            // 
            this.gbChoferPedido.Controls.Add(this.lbNombreChoferPedido);
            this.gbChoferPedido.Controls.Add(this.label3);
            this.gbChoferPedido.Controls.Add(this.lbNumLegajoChofer);
            this.gbChoferPedido.Controls.Add(this.label1);
            this.gbChoferPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbChoferPedido.Location = new System.Drawing.Point(8, 344);
            this.gbChoferPedido.Name = "gbChoferPedido";
            this.gbChoferPedido.Size = new System.Drawing.Size(786, 49);
            this.gbChoferPedido.TabIndex = 32;
            this.gbChoferPedido.TabStop = false;
            this.gbChoferPedido.Text = "Chofer";
            // 
            // lbNombreChoferPedido
            // 
            this.lbNombreChoferPedido.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNombreChoferPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNombreChoferPedido.Location = new System.Drawing.Point(331, 16);
            this.lbNombreChoferPedido.Name = "lbNombreChoferPedido";
            this.lbNombreChoferPedido.Size = new System.Drawing.Size(449, 26);
            this.lbNombreChoferPedido.TabIndex = 3;
            this.lbNombreChoferPedido.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(233, 23);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Nombre y Apellido";
            // 
            // lbNumLegajoChofer
            // 
            this.lbNumLegajoChofer.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbNumLegajoChofer.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumLegajoChofer.Location = new System.Drawing.Point(62, 16);
            this.lbNumLegajoChofer.Name = "lbNumLegajoChofer";
            this.lbNumLegajoChofer.Size = new System.Drawing.Size(161, 26);
            this.lbNumLegajoChofer.TabIndex = 1;
            this.lbNumLegajoChofer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(8, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(54, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nº Legajo";
            // 
            // gpPedidos
            // 
            this.gpPedidos.Controls.Add(this.panel1);
            this.gpPedidos.Controls.Add(this.lbFechaHastaPedido);
            this.gpPedidos.Controls.Add(this.dgvPedidos);
            this.gpPedidos.Controls.Add(this.dtpHastaPedido);
            this.gpPedidos.Controls.Add(this.lbFechaDesdePedido);
            this.gpPedidos.Controls.Add(this.tbBuscarPedido);
            this.gpPedidos.Controls.Add(this.dtpDesdePedido);
            this.gpPedidos.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpPedidos.Location = new System.Drawing.Point(2, 1);
            this.gpPedidos.Name = "gpPedidos";
            this.gpPedidos.Size = new System.Drawing.Size(793, 248);
            this.gpPedidos.TabIndex = 31;
            this.gpPedidos.TabStop = false;
            this.gpPedidos.Text = "PEDIDOS";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.nudPedidoDespues);
            this.panel1.Controls.Add(this.nudPedidosAntes);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.btnSeleccionarPedido);
            this.panel1.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(6, 207);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(781, 35);
            this.panel1.TabIndex = 34;
            // 
            // nudPedidoDespues
            // 
            this.nudPedidoDespues.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPedidoDespues.Location = new System.Drawing.Point(342, 5);
            this.nudPedidoDespues.Name = "nudPedidoDespues";
            this.nudPedidoDespues.Size = new System.Drawing.Size(47, 27);
            this.nudPedidoDespues.TabIndex = 43;
            // 
            // nudPedidosAntes
            // 
            this.nudPedidosAntes.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nudPedidosAntes.Location = new System.Drawing.Point(175, 5);
            this.nudPedidosAntes.Name = "nudPedidosAntes";
            this.nudPedidosAntes.Size = new System.Drawing.Size(47, 27);
            this.nudPedidosAntes.TabIndex = 42;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Bahnschrift Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(395, 6);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(264, 22);
            this.label8.TabIndex = 41;
            this.label8.Text = "dias despues de fecha del pedido";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Bahnschrift Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(239, 6);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 22);
            this.label2.TabIndex = 40;
            this.label2.Text = "dias antes y";
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Bahnschrift Condensed", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(5, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(200, 22);
            this.label12.TabIndex = 39;
            this.label12.Text = "Mostrar unidades libres";
            // 
            // btnSeleccionarPedido
            // 
            this.btnSeleccionarPedido.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeleccionarPedido.Location = new System.Drawing.Point(673, 3);
            this.btnSeleccionarPedido.Name = "btnSeleccionarPedido";
            this.btnSeleccionarPedido.Size = new System.Drawing.Size(103, 29);
            this.btnSeleccionarPedido.TabIndex = 25;
            this.btnSeleccionarPedido.Text = "SELECCIONAR";
            this.btnSeleccionarPedido.UseVisualStyleBackColor = true;
            this.btnSeleccionarPedido.Click += new System.EventHandler(this.btnSeleccionarPedido_Click);
            // 
            // lbFechaHastaPedido
            // 
            this.lbFechaHastaPedido.AutoSize = true;
            this.lbFechaHastaPedido.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaHastaPedido.Location = new System.Drawing.Point(292, 17);
            this.lbFechaHastaPedido.Name = "lbFechaHastaPedido";
            this.lbFechaHastaPedido.Size = new System.Drawing.Size(43, 19);
            this.lbFechaHastaPedido.TabIndex = 33;
            this.lbFechaHastaPedido.Text = "HASTA";
            // 
            // dgvPedidos
            // 
            this.dgvPedidos.AllowUserToAddRows = false;
            this.dgvPedidos.AllowUserToDeleteRows = false;
            this.dgvPedidos.AllowUserToOrderColumns = true;
            this.dgvPedidos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvPedidos.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvPedidos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvPedidos.Location = new System.Drawing.Point(5, 41);
            this.dgvPedidos.Name = "dgvPedidos";
            this.dgvPedidos.ReadOnly = true;
            this.dgvPedidos.Size = new System.Drawing.Size(781, 160);
            this.dgvPedidos.TabIndex = 24;
            // 
            // dtpHastaPedido
            // 
            this.dtpHastaPedido.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpHastaPedido.Location = new System.Drawing.Point(350, 11);
            this.dtpHastaPedido.Name = "dtpHastaPedido";
            this.dtpHastaPedido.Size = new System.Drawing.Size(233, 27);
            this.dtpHastaPedido.TabIndex = 30;
            this.dtpHastaPedido.ValueChanged += new System.EventHandler(this.dtpDesdePedido_ValueChanged);
            // 
            // lbFechaDesdePedido
            // 
            this.lbFechaDesdePedido.AutoSize = true;
            this.lbFechaDesdePedido.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFechaDesdePedido.Location = new System.Drawing.Point(8, 14);
            this.lbFechaDesdePedido.Name = "lbFechaDesdePedido";
            this.lbFechaDesdePedido.Size = new System.Drawing.Size(42, 19);
            this.lbFechaDesdePedido.TabIndex = 32;
            this.lbFechaDesdePedido.Text = "DESDE";
            // 
            // tbBuscarPedido
            // 
            this.tbBuscarPedido.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBuscarPedido.Location = new System.Drawing.Point(594, 11);
            this.tbBuscarPedido.MaxLength = 50;
            this.tbBuscarPedido.Multiline = true;
            this.tbBuscarPedido.Name = "tbBuscarPedido";
            this.tbBuscarPedido.ShortcutsEnabled = false;
            this.tbBuscarPedido.Size = new System.Drawing.Size(192, 27);
            this.tbBuscarPedido.TabIndex = 29;
            this.tbBuscarPedido.Text = "BUSCAR N° PEDIDO...";
            this.tbBuscarPedido.Click += new System.EventHandler(this.tbBuscarPedido_Click);
            this.tbBuscarPedido.TextChanged += new System.EventHandler(this.tbBuscarPedido_TextChanged);
            this.tbBuscarPedido.Leave += new System.EventHandler(this.tbBuscarPedido_Leave);
            // 
            // dtpDesdePedido
            // 
            this.dtpDesdePedido.Font = new System.Drawing.Font("Bahnschrift Condensed", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtpDesdePedido.Location = new System.Drawing.Point(56, 11);
            this.dtpDesdePedido.Name = "dtpDesdePedido";
            this.dtpDesdePedido.Size = new System.Drawing.Size(223, 27);
            this.dtpDesdePedido.TabIndex = 31;
            this.dtpDesdePedido.ValueChanged += new System.EventHandler(this.dtpDesdePedido_ValueChanged);
            // 
            // frmPedido
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 569);
            this.Controls.Add(this.gbDatosPedido);
            this.Controls.Add(this.gbClientePedido);
            this.Controls.Add(this.gbObservacionPedido);
            this.Controls.Add(this.gbAcopladoPedido);
            this.Controls.Add(this.gbCamionPedido);
            this.Controls.Add(this.gbChoferPedido);
            this.Controls.Add(this.gpPedidos);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPedido";
            this.Text = "Listado de pedidos - Logistica";
            this.Load += new System.EventHandler(this.frmPedido_Load);
            this.gbDatosPedido.ResumeLayout(false);
            this.gbDatosPedido.PerformLayout();
            this.gbClientePedido.ResumeLayout(false);
            this.gbClientePedido.PerformLayout();
            this.gbObservacionPedido.ResumeLayout(false);
            this.gbAcopladoPedido.ResumeLayout(false);
            this.gbAcopladoPedido.PerformLayout();
            this.gbCamionPedido.ResumeLayout(false);
            this.gbCamionPedido.PerformLayout();
            this.gbChoferPedido.ResumeLayout(false);
            this.gbChoferPedido.PerformLayout();
            this.gpPedidos.ResumeLayout(false);
            this.gpPedidos.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nudPedidoDespues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudPedidosAntes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPedidos)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDatosPedido;
        private System.Windows.Forms.Label lbKmViajePedido;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbVolumenPedido;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbPesoNetoPedido;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbEnganchePedido;
        private System.Windows.Forms.GroupBox gbClientePedido;
        private System.Windows.Forms.Label lbCuilClientePedido;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lbNombreClientePedido;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox gbObservacionPedido;
        private System.Windows.Forms.Label lbObservacionesPedido;
        private System.Windows.Forms.GroupBox gbAcopladoPedido;
        private System.Windows.Forms.Label lbAnchoExtRemolquePedido;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label lbAnchoIntRemolquePedido;
        private System.Windows.Forms.Label lbAlturaRemolquePedido;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lbLongitudRemolquePedido;
        private System.Windows.Forms.GroupBox gbCamionPedido;
        private System.Windows.Forms.Label lbAnchoExtCamionPedido;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lbAlturaCamionPedido;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lbLongitudCamionPedido;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lbAnchoIntCamionPedido;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox gbChoferPedido;
        private System.Windows.Forms.Label lbNombreChoferPedido;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lbNumLegajoChofer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox gpPedidos;
        private System.Windows.Forms.Label lbFechaHastaPedido;
        private System.Windows.Forms.Label lbFechaDesdePedido;
        private System.Windows.Forms.DateTimePicker dtpDesdePedido;
        private System.Windows.Forms.DateTimePicker dtpHastaPedido;
        private System.Windows.Forms.TextBox tbBuscarPedido;
        private System.Windows.Forms.Button btnSeleccionarPedido;
        private System.Windows.Forms.DataGridView dgvPedidos;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown nudPedidoDespues;
        private System.Windows.Forms.NumericUpDown nudPedidosAntes;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lbTipoCarga;
    }
}