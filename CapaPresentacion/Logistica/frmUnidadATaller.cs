﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.Logistica;

namespace CapaPresentacion.Logistica
{
    public partial class frmUnidadATaller : Form
    {
        public delegate void enviaObservacion(string e_viaje);
        public event enviaObservacion enviaObserv;
       
        public frmUnidadATaller()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (sender == btnOk)
            {
                string texto;
                texto = textBox1.Text.Trim();
                if (texto.Length > 0)
                {
                    enviaObserv(texto);
                }
                else
                {
                    MessageBox.Show("Debe ingresar un comentario en el campo de texto");
                    textBox1.Focus();
                }
            }
            if (sender == btnCancelar)
            {
                this.Dispose();
                this.Close();
            }
        }
    }
}
