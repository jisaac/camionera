﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.Logistica;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Logistica
{
  

    public partial class frmInputKmActualCamion : Form
    {
        public delegate void EnviarKm(float km);
        public event EnviarKm enviaKm;
        public delegate void EnviaCancelacion();
        public event EnviaCancelacion enviaCancelacion;
        

        public frmInputKmActualCamion(E_Camion e_Camion, string usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            this.e_Camion = e_Camion;
            n_dominio = new N_Dominio(usuario);
            tbKmActual.Text = e_Camion.Km_unidad.ToString();//seteamos el textbox con el km de la unidad
            lblMensajeKm.Text = "Ingrese Kilometraje actual de CAMION con dominio " + n_dominio.Retorna_Dominio(e_Camion.Id_Dominio).ToString() + "";
        }
        E_Camion e_Camion;
        N_Dominio n_dominio;
        string usuario;
        private void btnOk_Click(object sender, EventArgs e)
        {
            if(btnOk == sender)
            {
                float km = float.Parse(tbKmActual.Text);
                if (e_Camion.Km_unidad > km)
                    new frmMensajes("Error", "Esta ingresando un kilometraje actual menor al que ya posee el CAMION(" + e_Camion.Km_unidad.ToString() + " KM) !");
                else
                {
                    enviaKm(float.Parse(tbKmActual.Text));
                    this.Dispose();
                }
            }
            if (sender == btnCancelar)
            {
                enviaCancelacion();
                this.Dispose();
            }
            
        }
    }
}
