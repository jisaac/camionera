﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Logistica
{
    public partial class frmPedido : Form
    {
        public delegate void actualizaCamiones(DataTable camiones, E_Pedido e_pedido);
        public delegate void actualizaAcoplados(DataTable acoplados, E_Pedido e_pedido);
        public event actualizaCamiones enviaCamiones;
        public event actualizaAcoplados enviaAcoplados;

        N_Pedido n_pedido;
        N_TipoCarga n_tipo_carga;
        E_Pedido e_pedido;
        E_Viaje e_viaje;
        DataTable dtPedidos;
        string usuario;
        public frmPedido(string usuario, E_Pedido e_pedido)
        {
            InitializeComponent();
            n_pedido = new N_Pedido(usuario);
            this.e_pedido = e_pedido;
            e_viaje = new E_Viaje();
            this.usuario = usuario;
            n_tipo_carga = new N_TipoCarga(usuario);
        }
        private void btnSeleccionarPedido_Click(object sender, EventArgs e)
        {
            List<DataTable> lista = new List<DataTable>();
            E_Pedido e_pedido = n_pedido.retornaPedido(Convert.ToInt32(dgvPedidos.CurrentRow.Cells[0].Value));
            if (n_pedido.listarPendientes().Rows.Count > 0)
            {
                lista = n_pedido.retornaListaUnidadesSugerencia(e_pedido, Convert.ToInt32(nudPedidosAntes.Value), Convert.ToInt32(nudPedidoDespues.Value), null);

                if (lista.Count == 1)
                    if (lista[0].Rows.Count > 0)
                    {
                        enviaCamiones(lista[0], e_pedido);
                        this.Hide();
                    }
                    else
                        new frmMensajes("Error", "No hay unidades disponibles para poder realizar el cambio de unidades para este viaje").ShowDialog();
                else if (lista.Count == 2)
                {
                    if (lista[0].Rows.Count > 0 && lista[1].Rows.Count > 0)
                    {
                        enviaCamiones(lista[0], e_pedido);
                        enviaAcoplados(lista[1], e_pedido);
                        this.Hide();
                    }
                    else
                        new frmMensajes("Error", "No hay unidades disponibles para poder realizar el cambio de unidades para este viaje").ShowDialog();
                }
            }
            else
                new frmMensajes("Error", "No hay pedidos para seleccionar").ShowDialog();
        }
        private void carga_pedido_total()
        {
            dtPedidos = n_pedido.listarPendientes();
            dgvPedidos.DataSource = dtPedidos;
            verificaDGVPedidosVacio();
            columnas_invisibles_pedido();
            carga_datos_label_pedido();
        }
        private void verificaDGVPedidosVacio()
        {
            if (dgvPedidos.Rows.Count < 1)
            {
                dgvPedidos.BackgroundColor = Color.Red;
                limpiaTextboxBinding();
            }
            else
            {
                dgvPedidos.BackgroundColor = Color.Gray;
            }

        }
        private void columnas_invisibles_pedido()
        {
            dgvPedidos.Columns[1].Visible = false;
            dgvPedidos.Columns[2].Visible = false;
            dgvPedidos.Columns[6].Visible = false;
            dgvPedidos.Columns[7].Visible = false;
            dgvPedidos.Columns[8].Visible = false;
            dgvPedidos.Columns[9].Visible = false;
            dgvPedidos.Columns[10].Visible = false;
            dgvPedidos.Columns[11].Visible = false;
            dgvPedidos.Columns[12].Visible = false;
            dgvPedidos.Columns[13].Visible = false;
            dgvPedidos.Columns[14].Visible = false;
            dgvPedidos.Columns[15].Visible = false;
            dgvPedidos.Columns[16].Visible = false;
            dgvPedidos.Columns[17].Visible = false;
            dgvPedidos.Columns[18].Visible = false;
            dgvPedidos.Columns[19].Visible = false;
            dgvPedidos.Columns[20].Visible = false;
            dgvPedidos.Columns[21].Visible = false;
            dgvPedidos.Columns[22].Visible = false;
            dgvPedidos.Columns[24].Visible = false;

            dgvPedidos.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvPedidos.Columns[23].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

        }
        private void carga_datos_label_pedido()
        {
            limpiaBindingPedidos();
            this.controlaBindingCount();
            if (dtPedidos != null)
            {
                lbNumLegajoChofer.DataBindings.Add(new Binding("Text", dtPedidos, "Nº Legajo"));
                lbNombreChoferPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Nombre chofer"));
                lbNombreClientePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Nombre cliente"));
                lbCuilClientePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Cuil"));
                lbEnganchePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Enganche"));
                lbPesoNetoPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Peso neto"));
                lbVolumenPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Volumen"));
                lbKmViajePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Km viaje"));
                lbTipoCarga.DataBindings.Add(new Binding("Text", dtPedidos, "Tipo carga"));
                lbAlturaCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Altura camion"));
                if (lbAlturaCamionPedido.Text == "0")
                    lbAlturaCamionPedido.Text = "-----";
                lbAnchoIntCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho interior camion"));
                if (lbAnchoIntCamionPedido.Text == "0")
                    lbAnchoIntCamionPedido.Text = "-----";
                lbAnchoExtCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho exterior camion"));
                if (lbAnchoExtCamionPedido.Text == "0")
                    lbAnchoExtCamionPedido.Text = "-----";
                lbLongitudCamionPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Longitud camion"));
                if (lbLongitudCamionPedido.Text == "0")
                    lbLongitudCamionPedido.Text = "-----";
                lbAlturaRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Altura acoplado"));
                if (lbAlturaRemolquePedido.Text == "0")
                    lbAlturaRemolquePedido.Text = "-----";
                lbAnchoIntRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho interior acoplado"));
                if (lbAnchoIntRemolquePedido.Text == "0")
                    lbAnchoIntRemolquePedido.Text = "-----";
                lbAnchoExtRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Ancho exterior acoplado"));
                if (lbAnchoExtRemolquePedido.Text == "0")
                    lbAnchoExtRemolquePedido.Text = "-----";
                lbLongitudRemolquePedido.DataBindings.Add(new Binding("Text", dtPedidos, "Longitud acoplado"));
                if (lbLongitudRemolquePedido.Text == "0")
                    lbLongitudRemolquePedido.Text = "-----";
                lbObservacionesPedido.DataBindings.Add(new Binding("Text", dtPedidos, "Observaciones"));
            }

        }
        private void controlaBindingCount()
        {
            if (lbNumLegajoChofer.DataBindings.Count == 0)
                lbNumLegajoChofer.Text = "-----";
            if (lbNombreChoferPedido.DataBindings.Count == 0)
                lbNombreChoferPedido.Text = "-----";
            if (lbNombreClientePedido.DataBindings.Count == 0)
                lbNombreClientePedido.Text = "-----";
            if (lbCuilClientePedido.DataBindings.Count == 0)
                lbCuilClientePedido.Text = "-----";
            if (lbEnganchePedido.DataBindings.Count == 0)
                lbEnganchePedido.Text = "-----";
            if (lbPesoNetoPedido.DataBindings.Count == 0)
                lbPesoNetoPedido.Text = "-----";
            if (lbVolumenPedido.DataBindings.Count == 0)
                lbVolumenPedido.Text = "-----";
            if (lbKmViajePedido.DataBindings.Count == 0)
                lbKmViajePedido.Text = "-----";
            if (lbTipoCarga.DataBindings.Count == 0)
                lbTipoCarga.Text = "-----";
            if (lbAlturaCamionPedido.DataBindings.Count == 0)
                lbAlturaCamionPedido.Text = "-----";
            if (lbAnchoIntCamionPedido.DataBindings.Count == 0)
                lbAnchoIntCamionPedido.Text = "-----";
            if (lbAnchoExtCamionPedido.DataBindings.Count == 0)
                lbAnchoExtCamionPedido.Text = "-----";
            if (lbLongitudCamionPedido.DataBindings.Count == 0)
                lbLongitudCamionPedido.Text = "-----";
            if (lbAlturaRemolquePedido.DataBindings.Count == 0)
                lbAlturaRemolquePedido.Text = "-----";
            if (lbAnchoIntRemolquePedido.DataBindings.Count == 0)
                lbAnchoIntRemolquePedido.Text = "-----";
            if (lbAnchoExtRemolquePedido.DataBindings.Count == 0)
                lbAnchoExtRemolquePedido.Text = "-----";
            if (lbLongitudRemolquePedido.DataBindings.Count == 0)
                lbLongitudRemolquePedido.Text = "-----";
            if (lbObservacionesPedido.DataBindings.Count == 0)
                lbObservacionesPedido.Text = "-----";

        }
        private void limpiaBindingPedidos()
        {
            lbNumLegajoChofer.DataBindings.Clear();
            lbNombreChoferPedido.DataBindings.Clear();
            lbNombreClientePedido.DataBindings.Clear();
            lbCuilClientePedido.DataBindings.Clear();
            lbEnganchePedido.DataBindings.Clear();
            lbPesoNetoPedido.DataBindings.Clear();
            lbVolumenPedido.DataBindings.Clear();
            lbKmViajePedido.DataBindings.Clear();
            lbTipoCarga.DataBindings.Clear();
            lbAlturaCamionPedido.DataBindings.Clear();
            lbAnchoIntCamionPedido.DataBindings.Clear();
            lbAnchoExtCamionPedido.DataBindings.Clear();
            lbLongitudCamionPedido.DataBindings.Clear();
            lbAlturaRemolquePedido.DataBindings.Clear();
            lbAnchoIntRemolquePedido.DataBindings.Clear();
            lbAnchoExtRemolquePedido.DataBindings.Clear();
            lbLongitudRemolquePedido.DataBindings.Clear();
            lbObservacionesPedido.DataBindings.Clear();
        }
        private void limpiaTextboxBinding()
        {
            lbNumLegajoChofer.Text = "-----";
            lbNombreChoferPedido.Text = "-----";
            lbNombreClientePedido.Text = "-----";
            lbCuilClientePedido.Text = "-----";
            lbEnganchePedido.Text = "-----";
            lbPesoNetoPedido.Text = "-----";
            lbVolumenPedido.Text = "-----";
            lbKmViajePedido.Text = "-----";
            lbTipoCarga.Text = "-----";
            lbAlturaCamionPedido.Text = "-----";
            lbAnchoIntCamionPedido.Text = "-----";
            lbAnchoExtCamionPedido.Text = "-----";
            lbLongitudCamionPedido.Text = "-----";
            lbAlturaRemolquePedido.Text = "-----";
            lbAnchoIntRemolquePedido.Text = "-----";
            lbAnchoExtRemolquePedido.Text = "-----";
            lbLongitudRemolquePedido.Text = "-----";
            lbObservacionesPedido.Text = "-----";
        }
        private void frmPedido_Load(object sender, EventArgs e)
        {
            carga_pedido_total();
        }
        private void dtpDesdePedido_ValueChanged(object sender, EventArgs e)
        {
            dtPedidos = new DataTable();
            if (dtpDesdePedido.Value <= dtpHastaPedido.Value)
            {
                dtPedidos.Clear();
                dtPedidos = n_pedido.buscar_pedido_desde_hasta_pendiente(dtpDesdePedido.Value.ToString("yyyy-MM-dd"), dtpHastaPedido.Value.ToString("yyyy-MM-dd"));
                dgvPedidos.DataSource = dtPedidos;
                columnas_invisibles_pedido();
                carga_datos_label_pedido();
                verificaDGVPedidosVacio();
            }
        }
        private void tbBuscarPedido_TextChanged(object sender, EventArgs e)
        {
            dtPedidos = new DataTable();
            if (tbBuscarPedido.Text != "BUSCAR PEDIDO..")
            {
                if (n_pedido.listarTodos().Rows.Count > 0)
                {
                    if (tbBuscarPedido.Text != string.Empty)
                    {
                        dtPedidos.Clear();
                        dtPedidos = n_pedido.buscar_pedido_pendiente(Convert.ToInt32(tbBuscarPedido.Text));
                        dgvPedidos.DataSource = dtPedidos;
                        columnas_invisibles_pedido();
                        carga_datos_label_pedido();
                    }
                    else
                    {
                        carga_pedido_total();
                    }
                }
                else
                {
                    new frmMensajes("Error","No se posee registros para realizar la busqueda").ShowDialog();
                    dgvPedidos.DataSource = n_pedido.listarTodos();
                }
            }
        }
        private void tbBuscarPedido_Click(object sender, EventArgs e)
        {
            tbBuscarPedido.Text = "";
        }
        private void tbBuscarPedido_Leave(object sender, EventArgs e)
        {
            if (tbBuscarPedido.Text == String.Empty)
                tbBuscarPedido.Text = "BUSCAR PEDIDO..";
        }
    }
}
