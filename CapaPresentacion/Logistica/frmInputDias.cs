﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Logistica
{
    public partial class frmInputDias : Form
    {
        public delegate void actualizaCamiones(DataTable camiones, E_Pedido e_pedido);
        public delegate void actualizaAcoplados(DataTable acoplados, E_Pedido e_pedido);
        public event actualizaCamiones enviaCamiones;
        public event actualizaAcoplados enviaAcoplados;
        public delegate void CancelacambioUnidad();
        public event CancelacambioUnidad enviaCancelacion;


        N_Pedido n_pedido;
        string IDpedido;
        E_UnidadesViaje e_unidadesViaje;
        public frmInputDias(string IDpedido, E_UnidadesViaje e_unidadesViaje,string usuario)
        {
            InitializeComponent();
            this.IDpedido = IDpedido;
            this.e_unidadesViaje = e_unidadesViaje;
            n_pedido = new N_Pedido(usuario);
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (sender == btnOk)
            {
                List<DataTable> lista = new List<DataTable>();
                lista = n_pedido.retornaListaUnidadesSugerencia(n_pedido.retornaPedido(Convert.ToInt32(IDpedido)), Convert.ToInt32(nudPedidosAntes.Value), Convert.ToInt32(nudPedidoDespues.Value), e_unidadesViaje);
                if (lista.Count == 1)
                    if (lista[0].Rows.Count > 0)
                        enviaCamiones(lista[0], null);
                    else
                    {
                        new frmMensajes("Error", "No hay unidades disponibles para poder realizar el cambio de unidades para este viaje").ShowDialog();
                        this.Dispose();
                        enviaCancelacion();
                    }
                else if (lista.Count == 2)
                {
                    if (lista[0].Rows.Count > 0 && lista[1].Rows.Count > 0)
                    {
                        enviaCamiones(lista[0], null);
                        enviaAcoplados(lista[1], null);
                    }
                    else
                    {
                        new frmMensajes("Error", "No hay unidades disponibles para poder realizar el cambio de unidades para este viaje").ShowDialog();
                        this.Dispose();
                        enviaCancelacion();
                    }
                }
                this.Dispose();
            }
            if (sender == btnCancel)
            {
                this.Dispose();
                enviaCancelacion();
            }
        }
    }
}
