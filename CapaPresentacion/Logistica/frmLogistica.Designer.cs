﻿namespace CapaPresentacion.Logistica
{
    partial class frmLogistica
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogistica));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.tbViajes = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label47 = new System.Windows.Forms.Label();
            this.lblESTADOVIAJE = new System.Windows.Forms.Label();
            this.lblViajeSalida = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.lblViajeRegreso = new System.Windows.Forms.Label();
            this.pnlViajeAlarmas = new System.Windows.Forms.GroupBox();
            this.btnViajePorSalir = new System.Windows.Forms.Button();
            this.btnViajePorCerrar = new System.Windows.Forms.Button();
            this.btnAlarmaVisible = new System.Windows.Forms.Button();
            this.btnViajeEditar = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnViajeConcluir = new System.Windows.Forms.Button();
            this.btnViajePendiente = new System.Windows.Forms.Button();
            this.btnViajeCancelar = new System.Windows.Forms.Button();
            this.btnViajeSuspender = new System.Windows.Forms.Button();
            this.btnViajeEnCurso = new System.Windows.Forms.Button();
            this.cbViajeFiltrar2 = new System.Windows.Forms.ComboBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.lblViajeClienteCuit = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblViajeClienteNombre = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.tbViajeObservaciones = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tbViajePedidoObservaciones = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.lblViajeChoferLegajo = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.lblViajeChoferDireccion = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.lblViajeChoferNombre = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.lblViajeChoferTelTrabajo = new System.Windows.Forms.Label();
            this.lblViajeChoferTelPersonal = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.btnViajeCalendarAcoplado = new System.Windows.Forms.Button();
            this.lblViajeAcoplado1Dominio = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblViajeAcoplado1Modelo = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lblViajeAcoplado1Marca = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.btnViajeCalendarCamion = new System.Windows.Forms.Button();
            this.lblViajeCamionDominio = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblViajeCamionTipo = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblViajeCamionModelo = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblViajeCamionMarca = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.btnViajeCambiarUnidad = new System.Windows.Forms.Button();
            this.pnlViajeBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpViajeBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.dtpbViajeBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.label8 = new System.Windows.Forms.Label();
            this.pnlViajeBusquedaTexto = new System.Windows.Forms.Panel();
            this.label12 = new System.Windows.Forms.Label();
            this.tbViajeBusquedaTexto = new System.Windows.Forms.TextBox();
            this.cbViajeFiltrar = new System.Windows.Forms.ComboBox();
            this.dgvViaje = new System.Windows.Forms.DataGridView();
            this.tpUnidades = new System.Windows.Forms.TabPage();
            this.btnCrearViaje = new System.Windows.Forms.Button();
            this.gpRemolque = new System.Windows.Forms.GroupBox();
            this.btnCancelarSeleccionRemolque = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.lbTipoUnidad = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.lbKmNeumRemolque = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.lblKmUnidadRemolque = new System.Windows.Forms.Label();
            this.label78 = new System.Windows.Forms.Label();
            this.lbVolumenRemolque = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.lbTaraRemolque = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.lbCapCargaRemolque = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.lbEstadoRemolque = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.lbLongitudRemolque = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.lbAnchoExtRemolque = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.lbAnchoIntRemolque = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.lbAlturaRemolque = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.btnSeleccionarRemolque = new System.Windows.Forms.Button();
            this.dgvAcoplado = new System.Windows.Forms.DataGridView();
            this.gpCamion = new System.Windows.Forms.GroupBox();
            this.btnCancelarSeleccionCamion = new System.Windows.Forms.Button();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.lbKmNeumCamion = new System.Windows.Forms.Label();
            this.label403 = new System.Windows.Forms.Label();
            this.lbEstadoCamion = new System.Windows.Forms.Label();
            this.lblKmUnidadCamion = new System.Windows.Forms.Label();
            this.label77 = new System.Windows.Forms.Label();
            this.lblEngancheCamion = new System.Windows.Forms.Label();
            this.label79 = new System.Windows.Forms.Label();
            this.lbVolumenCamion = new System.Windows.Forms.Label();
            this.label454 = new System.Windows.Forms.Label();
            this.lbTaraCamion = new System.Windows.Forms.Label();
            this.label334 = new System.Windows.Forms.Label();
            this.lbCapCargaCamion = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.lbLongitudCamion = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.lbAnchoExtCamion = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.lbAnchoIntCamion = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.lbAlturaCamion = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.btnSeleccionarCamion = new System.Windows.Forms.Button();
            this.dgvCamion = new System.Windows.Forms.DataGridView();
            this.pnlCambiarDeUnidad = new System.Windows.Forms.Panel();
            this.btnViajeOkCambioDeUnidad = new System.Windows.Forms.Button();
            this.btnViajeCancelarCambioUnidad = new System.Windows.Forms.Button();
            this.btnSeleccionPedido = new System.Windows.Forms.Button();
            this.lblCambiarUnidadesViaje = new System.Windows.Forms.Label();
            this.gbLogistica = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblAcopladoEstado = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.lblAcopladoDtpEntregado = new System.Windows.Forms.Label();
            this.lblAcopladoDtpSalidaAprox = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.lblAcopladoDtpEntrada = new System.Windows.Forms.Label();
            this.lblAcopladoDiasAFavor = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.lblAcopladoSalidaAproxText = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.gbAcopladoTipoMantenimiento = new System.Windows.Forms.GroupBox();
            this.cbAcopladoNeumaticos = new System.Windows.Forms.CheckBox();
            this.cbAcopladoOtros = new System.Windows.Forms.CheckBox();
            this.cbAcopladoService = new System.Windows.Forms.CheckBox();
            this.tbAcopladoObservaciones = new System.Windows.Forms.TextBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCamionEstado = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.lblCamionDtpEntregado = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.lblCamionDtpSalidaAprox = new System.Windows.Forms.Label();
            this.lblCamionDtpEntrada = new System.Windows.Forms.Label();
            this.lblCamionDiasAFavor = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.lblCamionSalidaAproxText = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.gbCamionTipoMantenimiento = new System.Windows.Forms.GroupBox();
            this.cbCamionNeumaticos = new System.Windows.Forms.CheckBox();
            this.cbCamionOtros = new System.Windows.Forms.CheckBox();
            this.cbCamionService = new System.Windows.Forms.CheckBox();
            this.tbCamionObservaciones = new System.Windows.Forms.TextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.pnlAcopladoBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpAcopladoBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.dtpAcopladoBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.btnAcopladoVer = new System.Windows.Forms.Button();
            this.cbFiltroAcoplado2 = new System.Windows.Forms.ComboBox();
            this.cbFiltroAcoplado = new System.Windows.Forms.ComboBox();
            this.tbBusquedaAcoplado = new System.Windows.Forms.TextBox();
            this.dgvAcopladoMant = new System.Windows.Forms.DataGridView();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.btnCamionVer = new System.Windows.Forms.Button();
            this.pnlCamionBusquedaFecha = new System.Windows.Forms.Panel();
            this.dtpCamionBusquedaHasta = new System.Windows.Forms.DateTimePicker();
            this.label36 = new System.Windows.Forms.Label();
            this.dtpbCamionBusquedaDesde = new System.Windows.Forms.DateTimePicker();
            this.label38 = new System.Windows.Forms.Label();
            this.cbFiltroCamion2 = new System.Windows.Forms.ComboBox();
            this.cbFiltroCamion = new System.Windows.Forms.ComboBox();
            this.tbBusquedaCamion = new System.Windows.Forms.TextBox();
            this.dgvCamionMant = new System.Windows.Forms.DataGridView();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.tbViajes.SuspendLayout();
            this.panel4.SuspendLayout();
            this.pnlViajeAlarmas.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.pnlViajeBusquedaFecha.SuspendLayout();
            this.pnlViajeBusquedaTexto.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViaje)).BeginInit();
            this.tpUnidades.SuspendLayout();
            this.gpRemolque.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcoplado)).BeginInit();
            this.gpCamion.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamion)).BeginInit();
            this.pnlCambiarDeUnidad.SuspendLayout();
            this.gbLogistica.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbAcopladoTipoMantenimiento.SuspendLayout();
            this.panel3.SuspendLayout();
            this.gbCamionTipoMantenimiento.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.pnlAcopladoBusquedaFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcopladoMant)).BeginInit();
            this.groupBox14.SuspendLayout();
            this.pnlCamionBusquedaFecha.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamionMant)).BeginInit();
            this.SuspendLayout();
            // 
            // tbViajes
            // 
            this.tbViajes.BackColor = System.Drawing.Color.Transparent;
            this.tbViajes.Controls.Add(this.panel4);
            this.tbViajes.Controls.Add(this.pnlViajeAlarmas);
            this.tbViajes.Controls.Add(this.btnAlarmaVisible);
            this.tbViajes.Controls.Add(this.btnViajeEditar);
            this.tbViajes.Controls.Add(this.panel1);
            this.tbViajes.Controls.Add(this.cbViajeFiltrar2);
            this.tbViajes.Controls.Add(this.groupBox6);
            this.tbViajes.Controls.Add(this.label9);
            this.tbViajes.Controls.Add(this.tbViajeObservaciones);
            this.tbViajes.Controls.Add(this.label3);
            this.tbViajes.Controls.Add(this.tbViajePedidoObservaciones);
            this.tbViajes.Controls.Add(this.groupBox5);
            this.tbViajes.Controls.Add(this.groupBox1);
            this.tbViajes.Controls.Add(this.pnlViajeBusquedaFecha);
            this.tbViajes.Controls.Add(this.pnlViajeBusquedaTexto);
            this.tbViajes.Controls.Add(this.cbViajeFiltrar);
            this.tbViajes.Controls.Add(this.dgvViaje);
            this.tbViajes.Location = new System.Drawing.Point(4, 29);
            this.tbViajes.Name = "tbViajes";
            this.tbViajes.Padding = new System.Windows.Forms.Padding(3);
            this.tbViajes.Size = new System.Drawing.Size(1314, 665);
            this.tbViajes.TabIndex = 0;
            this.tbViajes.Text = " ADMINISTRACION DE VIAJES";
            // 
            // panel4
            // 
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel4.Controls.Add(this.label47);
            this.panel4.Controls.Add(this.lblESTADOVIAJE);
            this.panel4.Controls.Add(this.lblViajeSalida);
            this.panel4.Controls.Add(this.label50);
            this.panel4.Controls.Add(this.label49);
            this.panel4.Controls.Add(this.lblViajeRegreso);
            this.panel4.Location = new System.Drawing.Point(5, 536);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(822, 35);
            this.panel4.TabIndex = 65;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(514, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(77, 16);
            this.label47.TabIndex = 74;
            this.label47.Text = "Estado viaje";
            this.label47.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblESTADOVIAJE
            // 
            this.lblESTADOVIAJE.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblESTADOVIAJE.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblESTADOVIAJE.ForeColor = System.Drawing.Color.White;
            this.lblESTADOVIAJE.Location = new System.Drawing.Point(597, 8);
            this.lblESTADOVIAJE.Name = "lblESTADOVIAJE";
            this.lblESTADOVIAJE.Size = new System.Drawing.Size(188, 20);
            this.lblESTADOVIAJE.TabIndex = 73;
            this.lblESTADOVIAJE.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblViajeSalida
            // 
            this.lblViajeSalida.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeSalida.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViajeSalida.Location = new System.Drawing.Point(53, 8);
            this.lblViajeSalida.Name = "lblViajeSalida";
            this.lblViajeSalida.Size = new System.Drawing.Size(188, 20);
            this.lblViajeSalida.TabIndex = 69;
            this.lblViajeSalida.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(252, 10);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(57, 16);
            this.label50.TabIndex = 72;
            this.label50.Text = "Regreso";
            this.label50.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(3, 10);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(44, 16);
            this.label49.TabIndex = 70;
            this.label49.Text = "Salida";
            this.label49.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblViajeRegreso
            // 
            this.lblViajeRegreso.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeRegreso.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblViajeRegreso.Location = new System.Drawing.Point(315, 8);
            this.lblViajeRegreso.Name = "lblViajeRegreso";
            this.lblViajeRegreso.Size = new System.Drawing.Size(188, 20);
            this.lblViajeRegreso.TabIndex = 71;
            this.lblViajeRegreso.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlViajeAlarmas
            // 
            this.pnlViajeAlarmas.Controls.Add(this.btnViajePorSalir);
            this.pnlViajeAlarmas.Controls.Add(this.btnViajePorCerrar);
            this.pnlViajeAlarmas.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlViajeAlarmas.Location = new System.Drawing.Point(1064, 2);
            this.pnlViajeAlarmas.Name = "pnlViajeAlarmas";
            this.pnlViajeAlarmas.Size = new System.Drawing.Size(199, 56);
            this.pnlViajeAlarmas.TabIndex = 64;
            this.pnlViajeAlarmas.TabStop = false;
            this.pnlViajeAlarmas.Text = "Alarma - Viajes";
            this.pnlViajeAlarmas.Visible = false;
            // 
            // btnViajePorSalir
            // 
            this.btnViajePorSalir.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajePorSalir.Location = new System.Drawing.Point(6, 12);
            this.btnViajePorSalir.Name = "btnViajePorSalir";
            this.btnViajePorSalir.Size = new System.Drawing.Size(188, 20);
            this.btnViajePorSalir.TabIndex = 0;
            this.btnViajePorSalir.Text = "Viajes por salir";
            this.btnViajePorSalir.UseVisualStyleBackColor = true;
            this.btnViajePorSalir.Click += new System.EventHandler(this.btnViajePorSalir_Click);
            // 
            // btnViajePorCerrar
            // 
            this.btnViajePorCerrar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajePorCerrar.Location = new System.Drawing.Point(6, 34);
            this.btnViajePorCerrar.Name = "btnViajePorCerrar";
            this.btnViajePorCerrar.Size = new System.Drawing.Size(188, 20);
            this.btnViajePorCerrar.TabIndex = 1;
            this.btnViajePorCerrar.Text = "Viajes por cerrar";
            this.btnViajePorCerrar.UseVisualStyleBackColor = true;
            this.btnViajePorCerrar.Click += new System.EventHandler(this.btnViajePorSalir_Click);
            // 
            // btnAlarmaVisible
            // 
            this.btnAlarmaVisible.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnAlarmaVisible.BackgroundImage")));
            this.btnAlarmaVisible.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnAlarmaVisible.Location = new System.Drawing.Point(1264, 14);
            this.btnAlarmaVisible.Name = "btnAlarmaVisible";
            this.btnAlarmaVisible.Size = new System.Drawing.Size(43, 42);
            this.btnAlarmaVisible.TabIndex = 2;
            this.btnAlarmaVisible.UseVisualStyleBackColor = true;
            this.btnAlarmaVisible.Click += new System.EventHandler(this.btnAlarmaVisible_Click);
            // 
            // btnViajeEditar
            // 
            this.btnViajeEditar.BackColor = System.Drawing.Color.Silver;
            this.btnViajeEditar.Location = new System.Drawing.Point(834, 111);
            this.btnViajeEditar.Name = "btnViajeEditar";
            this.btnViajeEditar.Size = new System.Drawing.Size(471, 28);
            this.btnViajeEditar.TabIndex = 61;
            this.btnViajeEditar.Text = "Agregar observaciones";
            this.btnViajeEditar.UseVisualStyleBackColor = false;
            this.btnViajeEditar.Click += new System.EventHandler(this.btnViajeEditar_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnViajeConcluir);
            this.panel1.Controls.Add(this.btnViajePendiente);
            this.panel1.Controls.Add(this.btnViajeCancelar);
            this.panel1.Controls.Add(this.btnViajeSuspender);
            this.panel1.Controls.Add(this.btnViajeEnCurso);
            this.panel1.Location = new System.Drawing.Point(835, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(476, 45);
            this.panel1.TabIndex = 63;
            // 
            // btnViajeConcluir
            // 
            this.btnViajeConcluir.BackColor = System.Drawing.Color.LightSeaGreen;
            this.btnViajeConcluir.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnViajeConcluir.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajeConcluir.Location = new System.Drawing.Point(0, 2);
            this.btnViajeConcluir.Name = "btnViajeConcluir";
            this.btnViajeConcluir.Size = new System.Drawing.Size(75, 41);
            this.btnViajeConcluir.TabIndex = 57;
            this.btnViajeConcluir.Text = "Concluir";
            this.btnViajeConcluir.UseVisualStyleBackColor = false;
            this.btnViajeConcluir.Click += new System.EventHandler(this.btnViajeCancelar_Click);
            // 
            // btnViajePendiente
            // 
            this.btnViajePendiente.BackColor = System.Drawing.Color.Gold;
            this.btnViajePendiente.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnViajePendiente.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajePendiente.Location = new System.Drawing.Point(200, 2);
            this.btnViajePendiente.Name = "btnViajePendiente";
            this.btnViajePendiente.Size = new System.Drawing.Size(75, 41);
            this.btnViajePendiente.TabIndex = 58;
            this.btnViajePendiente.Text = "Pendiente";
            this.btnViajePendiente.UseVisualStyleBackColor = false;
            this.btnViajePendiente.Click += new System.EventHandler(this.btnViajeCancelar_Click);
            // 
            // btnViajeCancelar
            // 
            this.btnViajeCancelar.BackColor = System.Drawing.Color.Red;
            this.btnViajeCancelar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnViajeCancelar.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajeCancelar.Location = new System.Drawing.Point(400, 2);
            this.btnViajeCancelar.Name = "btnViajeCancelar";
            this.btnViajeCancelar.Size = new System.Drawing.Size(75, 41);
            this.btnViajeCancelar.TabIndex = 62;
            this.btnViajeCancelar.Text = "Cancelar";
            this.btnViajeCancelar.UseVisualStyleBackColor = false;
            this.btnViajeCancelar.Click += new System.EventHandler(this.btnViajeCancelar_Click);
            // 
            // btnViajeSuspender
            // 
            this.btnViajeSuspender.BackColor = System.Drawing.Color.Orange;
            this.btnViajeSuspender.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnViajeSuspender.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajeSuspender.Location = new System.Drawing.Point(300, 2);
            this.btnViajeSuspender.Name = "btnViajeSuspender";
            this.btnViajeSuspender.Size = new System.Drawing.Size(75, 41);
            this.btnViajeSuspender.TabIndex = 60;
            this.btnViajeSuspender.Text = "Suspender";
            this.btnViajeSuspender.UseVisualStyleBackColor = false;
            this.btnViajeSuspender.Click += new System.EventHandler(this.btnViajeCancelar_Click);
            // 
            // btnViajeEnCurso
            // 
            this.btnViajeEnCurso.BackColor = System.Drawing.Color.DeepSkyBlue;
            this.btnViajeEnCurso.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnViajeEnCurso.Font = new System.Drawing.Font("Bahnschrift SemiBold", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnViajeEnCurso.Location = new System.Drawing.Point(100, 2);
            this.btnViajeEnCurso.Name = "btnViajeEnCurso";
            this.btnViajeEnCurso.Size = new System.Drawing.Size(75, 41);
            this.btnViajeEnCurso.TabIndex = 59;
            this.btnViajeEnCurso.Text = "En Curso";
            this.btnViajeEnCurso.UseVisualStyleBackColor = false;
            this.btnViajeEnCurso.Click += new System.EventHandler(this.btnViajeCancelar_Click);
            // 
            // cbViajeFiltrar2
            // 
            this.cbViajeFiltrar2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbViajeFiltrar2.FormattingEnabled = true;
            this.cbViajeFiltrar2.Items.AddRange(new object[] {
            "Dominio,Cliente,Chofer,Destino",
            "Fecha de salida (Pedido)",
            "Fecha de Regreso (Pedido)",
            "Fecha de salida (Viaje)",
            "Fecha de Regreso (Viaje)"});
            this.cbViajeFiltrar2.Location = new System.Drawing.Point(213, 35);
            this.cbViajeFiltrar2.Name = "cbViajeFiltrar2";
            this.cbViajeFiltrar2.Size = new System.Drawing.Size(271, 21);
            this.cbViajeFiltrar2.TabIndex = 56;
            this.cbViajeFiltrar2.SelectedIndexChanged += new System.EventHandler(this.cbViajeFiltrar2_SelectedIndexChanged);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Azure;
            this.groupBox6.Controls.Add(this.lblViajeClienteCuit);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.lblViajeClienteNombre);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Location = new System.Drawing.Point(835, 145);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(472, 42);
            this.groupBox6.TabIndex = 55;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Cliente";
            // 
            // lblViajeClienteCuit
            // 
            this.lblViajeClienteCuit.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeClienteCuit.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeClienteCuit.Location = new System.Drawing.Point(310, 16);
            this.lblViajeClienteCuit.Name = "lblViajeClienteCuit";
            this.lblViajeClienteCuit.Size = new System.Drawing.Size(148, 20);
            this.lblViajeClienteCuit.TabIndex = 54;
            this.lblViajeClienteCuit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(279, 20);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(27, 13);
            this.label13.TabIndex = 53;
            this.label13.Text = "Cuit";
            // 
            // lblViajeClienteNombre
            // 
            this.lblViajeClienteNombre.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeClienteNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeClienteNombre.Location = new System.Drawing.Point(53, 16);
            this.lblViajeClienteNombre.Name = "lblViajeClienteNombre";
            this.lblViajeClienteNombre.Size = new System.Drawing.Size(216, 20);
            this.lblViajeClienteNombre.TabIndex = 50;
            this.lblViajeClienteNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(47, 13);
            this.label20.TabIndex = 45;
            this.label20.Text = "Nombre";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(685, 583);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(184, 17);
            this.label9.TabIndex = 47;
            this.label9.Text = "ObservacionesViaje";
            // 
            // tbViajeObservaciones
            // 
            this.tbViajeObservaciones.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbViajeObservaciones.Location = new System.Drawing.Point(687, 603);
            this.tbViajeObservaciones.Multiline = true;
            this.tbViajeObservaciones.Name = "tbViajeObservaciones";
            this.tbViajeObservaciones.ReadOnly = true;
            this.tbViajeObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbViajeObservaciones.Size = new System.Drawing.Size(619, 58);
            this.tbViajeObservaciones.TabIndex = 46;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 583);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(184, 17);
            this.label3.TabIndex = 45;
            this.label3.Text = "Observaciones Pedido";
            // 
            // tbViajePedidoObservaciones
            // 
            this.tbViajePedidoObservaciones.Font = new System.Drawing.Font("Bahnschrift", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbViajePedidoObservaciones.Location = new System.Drawing.Point(7, 603);
            this.tbViajePedidoObservaciones.Multiline = true;
            this.tbViajePedidoObservaciones.Name = "tbViajePedidoObservaciones";
            this.tbViajePedidoObservaciones.ReadOnly = true;
            this.tbViajePedidoObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbViajePedidoObservaciones.Size = new System.Drawing.Size(656, 58);
            this.tbViajePedidoObservaciones.TabIndex = 35;
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Azure;
            this.groupBox5.Controls.Add(this.lblViajeChoferLegajo);
            this.groupBox5.Controls.Add(this.label30);
            this.groupBox5.Controls.Add(this.lblViajeChoferDireccion);
            this.groupBox5.Controls.Add(this.label29);
            this.groupBox5.Controls.Add(this.lblViajeChoferNombre);
            this.groupBox5.Controls.Add(this.label23);
            this.groupBox5.Controls.Add(this.lblViajeChoferTelTrabajo);
            this.groupBox5.Controls.Add(this.lblViajeChoferTelPersonal);
            this.groupBox5.Controls.Add(this.label25);
            this.groupBox5.Controls.Add(this.label27);
            this.groupBox5.Location = new System.Drawing.Point(835, 193);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(472, 108);
            this.groupBox5.TabIndex = 34;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Chofer";
            // 
            // lblViajeChoferLegajo
            // 
            this.lblViajeChoferLegajo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeChoferLegajo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeChoferLegajo.Location = new System.Drawing.Point(365, 16);
            this.lblViajeChoferLegajo.Name = "lblViajeChoferLegajo";
            this.lblViajeChoferLegajo.Size = new System.Drawing.Size(94, 20);
            this.lblViajeChoferLegajo.TabIndex = 54;
            this.lblViajeChoferLegajo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(320, 20);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(40, 13);
            this.label30.TabIndex = 53;
            this.label30.Text = "Legajo";
            // 
            // lblViajeChoferDireccion
            // 
            this.lblViajeChoferDireccion.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeChoferDireccion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeChoferDireccion.Location = new System.Drawing.Point(100, 78);
            this.lblViajeChoferDireccion.Name = "lblViajeChoferDireccion";
            this.lblViajeChoferDireccion.Size = new System.Drawing.Size(358, 20);
            this.lblViajeChoferDireccion.TabIndex = 52;
            this.lblViajeChoferDireccion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 82);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(53, 13);
            this.label29.TabIndex = 51;
            this.label29.Text = "Direccion";
            // 
            // lblViajeChoferNombre
            // 
            this.lblViajeChoferNombre.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeChoferNombre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeChoferNombre.Location = new System.Drawing.Point(101, 16);
            this.lblViajeChoferNombre.Name = "lblViajeChoferNombre";
            this.lblViajeChoferNombre.Size = new System.Drawing.Size(216, 20);
            this.lblViajeChoferNombre.TabIndex = 50;
            this.lblViajeChoferNombre.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(6, 20);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(95, 13);
            this.label23.TabIndex = 45;
            this.label23.Text = "Nombre y apellido";
            // 
            // lblViajeChoferTelTrabajo
            // 
            this.lblViajeChoferTelTrabajo.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeChoferTelTrabajo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeChoferTelTrabajo.Location = new System.Drawing.Point(315, 46);
            this.lblViajeChoferTelTrabajo.Name = "lblViajeChoferTelTrabajo";
            this.lblViajeChoferTelTrabajo.Size = new System.Drawing.Size(144, 20);
            this.lblViajeChoferTelTrabajo.TabIndex = 47;
            this.lblViajeChoferTelTrabajo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblViajeChoferTelPersonal
            // 
            this.lblViajeChoferTelPersonal.BackColor = System.Drawing.Color.WhiteSmoke;
            this.lblViajeChoferTelPersonal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeChoferTelPersonal.Location = new System.Drawing.Point(101, 46);
            this.lblViajeChoferTelPersonal.Name = "lblViajeChoferTelPersonal";
            this.lblViajeChoferTelPersonal.Size = new System.Drawing.Size(140, 20);
            this.lblViajeChoferTelPersonal.TabIndex = 49;
            this.lblViajeChoferTelPersonal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(6, 50);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(66, 13);
            this.label25.TabIndex = 48;
            this.label25.Text = "Tel personal";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(248, 50);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(58, 13);
            this.label27.TabIndex = 46;
            this.label27.Text = "Tel trabajo";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Azure;
            this.groupBox1.Controls.Add(this.groupBox3);
            this.groupBox1.Controls.Add(this.groupBox2);
            this.groupBox1.Controls.Add(this.btnViajeCambiarUnidad);
            this.groupBox1.Location = new System.Drawing.Point(834, 307);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(472, 272);
            this.groupBox1.TabIndex = 33;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "UNIDADES/VIAJE";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox3.Controls.Add(this.groupBox13);
            this.groupBox3.Controls.Add(this.lblViajeAcoplado1Dominio);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.lblViajeAcoplado1Modelo);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.lblViajeAcoplado1Marca);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox3.Location = new System.Drawing.Point(5, 127);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(462, 103);
            this.groupBox3.TabIndex = 46;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Remolque / Semiremolque";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.btnViajeCalendarAcoplado);
            this.groupBox13.Location = new System.Drawing.Point(253, 35);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(201, 56);
            this.groupBox13.TabIndex = 65;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Ver Actividad Unidad";
            // 
            // btnViajeCalendarAcoplado
            // 
            this.btnViajeCalendarAcoplado.BackColor = System.Drawing.Color.White;
            this.btnViajeCalendarAcoplado.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnViajeCalendarAcoplado.BackgroundImage")));
            this.btnViajeCalendarAcoplado.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnViajeCalendarAcoplado.Location = new System.Drawing.Point(88, 15);
            this.btnViajeCalendarAcoplado.Name = "btnViajeCalendarAcoplado";
            this.btnViajeCalendarAcoplado.Size = new System.Drawing.Size(75, 41);
            this.btnViajeCalendarAcoplado.TabIndex = 64;
            this.btnViajeCalendarAcoplado.UseVisualStyleBackColor = false;
            this.btnViajeCalendarAcoplado.Click += new System.EventHandler(this.btnViajeCalendarCamion_Click);
            // 
            // lblViajeAcoplado1Dominio
            // 
            this.lblViajeAcoplado1Dominio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeAcoplado1Dominio.Location = new System.Drawing.Point(306, 12);
            this.lblViajeAcoplado1Dominio.Name = "lblViajeAcoplado1Dominio";
            this.lblViajeAcoplado1Dominio.Size = new System.Drawing.Size(148, 20);
            this.lblViajeAcoplado1Dominio.TabIndex = 44;
            this.lblViajeAcoplado1Dominio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(256, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Dominio";
            // 
            // lblViajeAcoplado1Modelo
            // 
            this.lblViajeAcoplado1Modelo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeAcoplado1Modelo.Location = new System.Drawing.Point(46, 29);
            this.lblViajeAcoplado1Modelo.Name = "lblViajeAcoplado1Modelo";
            this.lblViajeAcoplado1Modelo.Size = new System.Drawing.Size(188, 20);
            this.lblViajeAcoplado1Modelo.TabIndex = 43;
            this.lblViajeAcoplado1Modelo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(2, 33);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 42;
            this.label7.Text = "Modelo";
            // 
            // lblViajeAcoplado1Marca
            // 
            this.lblViajeAcoplado1Marca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeAcoplado1Marca.Location = new System.Drawing.Point(45, 54);
            this.lblViajeAcoplado1Marca.Name = "lblViajeAcoplado1Marca";
            this.lblViajeAcoplado1Marca.Size = new System.Drawing.Size(189, 20);
            this.lblViajeAcoplado1Marca.TabIndex = 41;
            this.lblViajeAcoplado1Marca.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(2, 58);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(38, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Marca";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox2.Controls.Add(this.groupBox12);
            this.groupBox2.Controls.Add(this.lblViajeCamionDominio);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.lblViajeCamionTipo);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.lblViajeCamionModelo);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.lblViajeCamionMarca);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.groupBox2.Location = new System.Drawing.Point(5, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(461, 102);
            this.groupBox2.TabIndex = 45;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Camion";
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.btnViajeCalendarCamion);
            this.groupBox12.Location = new System.Drawing.Point(253, 41);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(202, 61);
            this.groupBox12.TabIndex = 64;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Ver Actividad Unidad";
            // 
            // btnViajeCalendarCamion
            // 
            this.btnViajeCalendarCamion.BackColor = System.Drawing.Color.White;
            this.btnViajeCalendarCamion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnViajeCalendarCamion.BackgroundImage")));
            this.btnViajeCalendarCamion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.btnViajeCalendarCamion.Location = new System.Drawing.Point(88, 14);
            this.btnViajeCalendarCamion.Name = "btnViajeCalendarCamion";
            this.btnViajeCalendarCamion.Size = new System.Drawing.Size(75, 41);
            this.btnViajeCalendarCamion.TabIndex = 63;
            this.btnViajeCalendarCamion.UseVisualStyleBackColor = false;
            this.btnViajeCalendarCamion.Click += new System.EventHandler(this.btnViajeCalendarCamion_Click);
            // 
            // lblViajeCamionDominio
            // 
            this.lblViajeCamionDominio.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeCamionDominio.Location = new System.Drawing.Point(303, 12);
            this.lblViajeCamionDominio.Name = "lblViajeCamionDominio";
            this.lblViajeCamionDominio.Size = new System.Drawing.Size(152, 20);
            this.lblViajeCamionDominio.TabIndex = 48;
            this.lblViajeCamionDominio.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(250, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 47;
            this.label1.Text = "Dominio";
            // 
            // lblViajeCamionTipo
            // 
            this.lblViajeCamionTipo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeCamionTipo.Location = new System.Drawing.Point(46, 16);
            this.lblViajeCamionTipo.Name = "lblViajeCamionTipo";
            this.lblViajeCamionTipo.Size = new System.Drawing.Size(188, 20);
            this.lblViajeCamionTipo.TabIndex = 46;
            this.lblViajeCamionTipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 20);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(27, 13);
            this.label6.TabIndex = 45;
            this.label6.Text = "Tipo";
            // 
            // lblViajeCamionModelo
            // 
            this.lblViajeCamionModelo.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeCamionModelo.Location = new System.Drawing.Point(46, 43);
            this.lblViajeCamionModelo.Name = "lblViajeCamionModelo";
            this.lblViajeCamionModelo.Size = new System.Drawing.Size(188, 20);
            this.lblViajeCamionModelo.TabIndex = 43;
            this.lblViajeCamionModelo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(6, 47);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(43, 13);
            this.label11.TabIndex = 42;
            this.label11.Text = "Modelo";
            // 
            // lblViajeCamionMarca
            // 
            this.lblViajeCamionMarca.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblViajeCamionMarca.Location = new System.Drawing.Point(46, 68);
            this.lblViajeCamionMarca.Name = "lblViajeCamionMarca";
            this.lblViajeCamionMarca.Size = new System.Drawing.Size(188, 20);
            this.lblViajeCamionMarca.TabIndex = 41;
            this.lblViajeCamionMarca.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 72);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 38;
            this.label5.Text = "Marca";
            // 
            // btnViajeCambiarUnidad
            // 
            this.btnViajeCambiarUnidad.BackColor = System.Drawing.Color.Silver;
            this.btnViajeCambiarUnidad.Location = new System.Drawing.Point(7, 236);
            this.btnViajeCambiarUnidad.Name = "btnViajeCambiarUnidad";
            this.btnViajeCambiarUnidad.Size = new System.Drawing.Size(462, 36);
            this.btnViajeCambiarUnidad.TabIndex = 37;
            this.btnViajeCambiarUnidad.Text = "Cambiar unidades";
            this.btnViajeCambiarUnidad.UseVisualStyleBackColor = false;
            this.btnViajeCambiarUnidad.Click += new System.EventHandler(this.btnViajeCambiarUnidad_Click);
            // 
            // pnlViajeBusquedaFecha
            // 
            this.pnlViajeBusquedaFecha.Controls.Add(this.dtpViajeBusquedaHasta);
            this.pnlViajeBusquedaFecha.Controls.Add(this.label2);
            this.pnlViajeBusquedaFecha.Controls.Add(this.dtpbViajeBusquedaDesde);
            this.pnlViajeBusquedaFecha.Controls.Add(this.label8);
            this.pnlViajeBusquedaFecha.Location = new System.Drawing.Point(495, 29);
            this.pnlViajeBusquedaFecha.Name = "pnlViajeBusquedaFecha";
            this.pnlViajeBusquedaFecha.Size = new System.Drawing.Size(333, 28);
            this.pnlViajeBusquedaFecha.TabIndex = 29;
            // 
            // dtpViajeBusquedaHasta
            // 
            this.dtpViajeBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpViajeBusquedaHasta.Location = new System.Drawing.Point(228, 5);
            this.dtpViajeBusquedaHasta.Name = "dtpViajeBusquedaHasta";
            this.dtpViajeBusquedaHasta.Size = new System.Drawing.Size(104, 21);
            this.dtpViajeBusquedaHasta.TabIndex = 4;
            this.dtpViajeBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpbViajeBusquedaDesde_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(189, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "hasta";
            // 
            // dtpbViajeBusquedaDesde
            // 
            this.dtpbViajeBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpbViajeBusquedaDesde.Location = new System.Drawing.Point(80, 5);
            this.dtpbViajeBusquedaDesde.Name = "dtpbViajeBusquedaDesde";
            this.dtpbViajeBusquedaDesde.Size = new System.Drawing.Size(104, 21);
            this.dtpbViajeBusquedaDesde.TabIndex = 2;
            this.dtpbViajeBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpbViajeBusquedaDesde_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 7);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 13);
            this.label8.TabIndex = 1;
            this.label8.Text = "Buscar  desde ";
            // 
            // pnlViajeBusquedaTexto
            // 
            this.pnlViajeBusquedaTexto.Controls.Add(this.label12);
            this.pnlViajeBusquedaTexto.Controls.Add(this.tbViajeBusquedaTexto);
            this.pnlViajeBusquedaTexto.Location = new System.Drawing.Point(0, 0);
            this.pnlViajeBusquedaTexto.Name = "pnlViajeBusquedaTexto";
            this.pnlViajeBusquedaTexto.Size = new System.Drawing.Size(333, 28);
            this.pnlViajeBusquedaTexto.TabIndex = 28;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(6, 6);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(48, 13);
            this.label12.TabIndex = 1;
            this.label12.Text = "Buscar : ";
            // 
            // tbViajeBusquedaTexto
            // 
            this.tbViajeBusquedaTexto.Font = new System.Drawing.Font("Bahnschrift", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbViajeBusquedaTexto.Location = new System.Drawing.Point(61, 3);
            this.tbViajeBusquedaTexto.MaxLength = 50;
            this.tbViajeBusquedaTexto.Name = "tbViajeBusquedaTexto";
            this.tbViajeBusquedaTexto.ShortcutsEnabled = false;
            this.tbViajeBusquedaTexto.Size = new System.Drawing.Size(239, 23);
            this.tbViajeBusquedaTexto.TabIndex = 0;
            this.tbViajeBusquedaTexto.TextChanged += new System.EventHandler(this.tbViajeBusquedaTexto_TextChanged);
            // 
            // cbViajeFiltrar
            // 
            this.cbViajeFiltrar.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbViajeFiltrar.FormattingEnabled = true;
            this.cbViajeFiltrar.Items.AddRange(new object[] {
            "Todos",
            "Pendiente",
            "En curso",
            "Concluido",
            "Suspendido",
            "Cancelado"});
            this.cbViajeFiltrar.Location = new System.Drawing.Point(2, 35);
            this.cbViajeFiltrar.Name = "cbViajeFiltrar";
            this.cbViajeFiltrar.Size = new System.Drawing.Size(206, 21);
            this.cbViajeFiltrar.TabIndex = 26;
            this.cbViajeFiltrar.SelectedIndexChanged += new System.EventHandler(this.cbViajeFiltrar_SelectedIndexChanged);
            // 
            // dgvViaje
            // 
            this.dgvViaje.AllowUserToAddRows = false;
            this.dgvViaje.AllowUserToDeleteRows = false;
            this.dgvViaje.AllowUserToOrderColumns = true;
            this.dgvViaje.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvViaje.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvViaje.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvViaje.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvViaje.Location = new System.Drawing.Point(2, 64);
            this.dgvViaje.MultiSelect = false;
            this.dgvViaje.Name = "dgvViaje";
            this.dgvViaje.ReadOnly = true;
            this.dgvViaje.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvViaje.Size = new System.Drawing.Size(826, 473);
            this.dgvViaje.TabIndex = 25;
            this.dgvViaje.CurrentCellChanged += new System.EventHandler(this.dgvViaje_CurrentCellChanged);
            // 
            // tpUnidades
            // 
            this.tpUnidades.Controls.Add(this.btnCrearViaje);
            this.tpUnidades.Controls.Add(this.gpRemolque);
            this.tpUnidades.Controls.Add(this.gpCamion);
            this.tpUnidades.Controls.Add(this.pnlCambiarDeUnidad);
            this.tpUnidades.Location = new System.Drawing.Point(4, 29);
            this.tpUnidades.Name = "tpUnidades";
            this.tpUnidades.Size = new System.Drawing.Size(1314, 665);
            this.tpUnidades.TabIndex = 3;
            this.tpUnidades.Text = "ADMINISTRACION UNIDADES";
            this.tpUnidades.UseVisualStyleBackColor = true;
            // 
            // btnCrearViaje
            // 
            this.btnCrearViaje.BackColor = System.Drawing.Color.Silver;
            this.btnCrearViaje.Enabled = false;
            this.btnCrearViaje.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCrearViaje.Location = new System.Drawing.Point(652, 14);
            this.btnCrearViaje.Name = "btnCrearViaje";
            this.btnCrearViaje.Size = new System.Drawing.Size(647, 42);
            this.btnCrearViaje.TabIndex = 34;
            this.btnCrearViaje.Text = "CREAR VIAJE";
            this.btnCrearViaje.UseVisualStyleBackColor = false;
            this.btnCrearViaje.Click += new System.EventHandler(this.btnCrearViaje_Click);
            // 
            // gpRemolque
            // 
            this.gpRemolque.Controls.Add(this.btnCancelarSeleccionRemolque);
            this.gpRemolque.Controls.Add(this.groupBox8);
            this.gpRemolque.Controls.Add(this.btnSeleccionarRemolque);
            this.gpRemolque.Controls.Add(this.dgvAcoplado);
            this.gpRemolque.Location = new System.Drawing.Point(12, 371);
            this.gpRemolque.Name = "gpRemolque";
            this.gpRemolque.Size = new System.Drawing.Size(1298, 299);
            this.gpRemolque.TabIndex = 35;
            this.gpRemolque.TabStop = false;
            this.gpRemolque.Text = "REMOLQUE";
            // 
            // btnCancelarSeleccionRemolque
            // 
            this.btnCancelarSeleccionRemolque.Enabled = false;
            this.btnCancelarSeleccionRemolque.Location = new System.Drawing.Point(433, 19);
            this.btnCancelarSeleccionRemolque.Name = "btnCancelarSeleccionRemolque";
            this.btnCancelarSeleccionRemolque.Size = new System.Drawing.Size(411, 23);
            this.btnCancelarSeleccionRemolque.TabIndex = 33;
            this.btnCancelarSeleccionRemolque.Text = "CANCELAR SELECCION";
            this.btnCancelarSeleccionRemolque.UseVisualStyleBackColor = true;
            this.btnCancelarSeleccionRemolque.Click += new System.EventHandler(this.btnCancelarSeleccionRemolque_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.lbTipoUnidad);
            this.groupBox8.Controls.Add(this.label33);
            this.groupBox8.Controls.Add(this.lbKmNeumRemolque);
            this.groupBox8.Controls.Add(this.label22);
            this.groupBox8.Controls.Add(this.lblKmUnidadRemolque);
            this.groupBox8.Controls.Add(this.label78);
            this.groupBox8.Controls.Add(this.lbVolumenRemolque);
            this.groupBox8.Controls.Add(this.label31);
            this.groupBox8.Controls.Add(this.lbTaraRemolque);
            this.groupBox8.Controls.Add(this.label35);
            this.groupBox8.Controls.Add(this.lbCapCargaRemolque);
            this.groupBox8.Controls.Add(this.label37);
            this.groupBox8.Controls.Add(this.lbEstadoRemolque);
            this.groupBox8.Controls.Add(this.label39);
            this.groupBox8.Controls.Add(this.lbLongitudRemolque);
            this.groupBox8.Controls.Add(this.label41);
            this.groupBox8.Controls.Add(this.lbAnchoExtRemolque);
            this.groupBox8.Controls.Add(this.label44);
            this.groupBox8.Controls.Add(this.lbAnchoIntRemolque);
            this.groupBox8.Controls.Add(this.label46);
            this.groupBox8.Controls.Add(this.lbAlturaRemolque);
            this.groupBox8.Controls.Add(this.label48);
            this.groupBox8.Location = new System.Drawing.Point(866, 19);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(426, 273);
            this.groupBox8.TabIndex = 32;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "DATOS REMOLQUE";
            // 
            // lbTipoUnidad
            // 
            this.lbTipoUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTipoUnidad.Font = new System.Drawing.Font("Bahnschrift SemiBold", 6.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTipoUnidad.Location = new System.Drawing.Point(120, 198);
            this.lbTipoUnidad.Name = "lbTipoUnidad";
            this.lbTipoUnidad.Size = new System.Drawing.Size(88, 23);
            this.lbTipoUnidad.TabIndex = 90;
            this.lbTipoUnidad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.Location = new System.Drawing.Point(10, 198);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(108, 23);
            this.label33.TabIndex = 89;
            this.label33.Text = "Tipo Unidad";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbKmNeumRemolque
            // 
            this.lbKmNeumRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbKmNeumRemolque.Location = new System.Drawing.Point(332, 198);
            this.lbKmNeumRemolque.Name = "lbKmNeumRemolque";
            this.lbKmNeumRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbKmNeumRemolque.TabIndex = 88;
            this.lbKmNeumRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.Location = new System.Drawing.Point(219, 198);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(108, 23);
            this.label22.TabIndex = 87;
            this.label22.Text = "Km Neumaticos";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmUnidadRemolque
            // 
            this.lblKmUnidadRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKmUnidadRemolque.Location = new System.Drawing.Point(333, 157);
            this.lblKmUnidadRemolque.Name = "lblKmUnidadRemolque";
            this.lblKmUnidadRemolque.Size = new System.Drawing.Size(88, 23);
            this.lblKmUnidadRemolque.TabIndex = 86;
            this.lblKmUnidadRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label78
            // 
            this.label78.Location = new System.Drawing.Point(219, 157);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(108, 23);
            this.label78.TabIndex = 85;
            this.label78.Text = "Km Unidad";
            this.label78.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbVolumenRemolque
            // 
            this.lbVolumenRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbVolumenRemolque.Location = new System.Drawing.Point(333, 116);
            this.lbVolumenRemolque.Name = "lbVolumenRemolque";
            this.lbVolumenRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbVolumenRemolque.TabIndex = 78;
            this.lbVolumenRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.Location = new System.Drawing.Point(219, 116);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(108, 23);
            this.label31.TabIndex = 77;
            this.label31.Text = "Volumen";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTaraRemolque
            // 
            this.lbTaraRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTaraRemolque.Location = new System.Drawing.Point(333, 75);
            this.lbTaraRemolque.Name = "lbTaraRemolque";
            this.lbTaraRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbTaraRemolque.TabIndex = 76;
            this.lbTaraRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label35
            // 
            this.label35.Location = new System.Drawing.Point(219, 75);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(108, 23);
            this.label35.TabIndex = 75;
            this.label35.Text = "Tara";
            this.label35.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCapCargaRemolque
            // 
            this.lbCapCargaRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCapCargaRemolque.Location = new System.Drawing.Point(333, 34);
            this.lbCapCargaRemolque.Name = "lbCapCargaRemolque";
            this.lbCapCargaRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbCapCargaRemolque.TabIndex = 74;
            this.lbCapCargaRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label37
            // 
            this.label37.Location = new System.Drawing.Point(219, 34);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(108, 23);
            this.label37.TabIndex = 73;
            this.label37.Text = "Capacidad Carga";
            this.label37.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbEstadoRemolque
            // 
            this.lbEstadoRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbEstadoRemolque.Location = new System.Drawing.Point(120, 239);
            this.lbEstadoRemolque.Name = "lbEstadoRemolque";
            this.lbEstadoRemolque.Size = new System.Drawing.Size(300, 23);
            this.lbEstadoRemolque.TabIndex = 72;
            this.lbEstadoRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label39
            // 
            this.label39.Location = new System.Drawing.Point(10, 239);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(108, 23);
            this.label39.TabIndex = 71;
            this.label39.Text = "Estado";
            this.label39.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbLongitudRemolque
            // 
            this.lbLongitudRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbLongitudRemolque.Location = new System.Drawing.Point(120, 157);
            this.lbLongitudRemolque.Name = "lbLongitudRemolque";
            this.lbLongitudRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbLongitudRemolque.TabIndex = 70;
            this.lbLongitudRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label41
            // 
            this.label41.Location = new System.Drawing.Point(10, 157);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(108, 23);
            this.label41.TabIndex = 69;
            this.label41.Text = "Longitud";
            this.label41.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAnchoExtRemolque
            // 
            this.lbAnchoExtRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAnchoExtRemolque.Location = new System.Drawing.Point(120, 116);
            this.lbAnchoExtRemolque.Name = "lbAnchoExtRemolque";
            this.lbAnchoExtRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbAnchoExtRemolque.TabIndex = 68;
            this.lbAnchoExtRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label44
            // 
            this.label44.Location = new System.Drawing.Point(6, 116);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(108, 23);
            this.label44.TabIndex = 67;
            this.label44.Text = "Ancho Exterior";
            this.label44.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAnchoIntRemolque
            // 
            this.lbAnchoIntRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAnchoIntRemolque.Location = new System.Drawing.Point(120, 75);
            this.lbAnchoIntRemolque.Name = "lbAnchoIntRemolque";
            this.lbAnchoIntRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbAnchoIntRemolque.TabIndex = 66;
            this.lbAnchoIntRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label46
            // 
            this.label46.Location = new System.Drawing.Point(6, 75);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(108, 23);
            this.label46.TabIndex = 65;
            this.label46.Text = "Ancho Interior";
            this.label46.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAlturaRemolque
            // 
            this.lbAlturaRemolque.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAlturaRemolque.Location = new System.Drawing.Point(120, 34);
            this.lbAlturaRemolque.Name = "lbAlturaRemolque";
            this.lbAlturaRemolque.Size = new System.Drawing.Size(88, 23);
            this.lbAlturaRemolque.TabIndex = 64;
            this.lbAlturaRemolque.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label48
            // 
            this.label48.Location = new System.Drawing.Point(6, 34);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(108, 23);
            this.label48.TabIndex = 63;
            this.label48.Text = "Altura";
            this.label48.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSeleccionarRemolque
            // 
            this.btnSeleccionarRemolque.Enabled = false;
            this.btnSeleccionarRemolque.Location = new System.Drawing.Point(6, 19);
            this.btnSeleccionarRemolque.Name = "btnSeleccionarRemolque";
            this.btnSeleccionarRemolque.Size = new System.Drawing.Size(411, 23);
            this.btnSeleccionarRemolque.TabIndex = 25;
            this.btnSeleccionarRemolque.Text = "SELECCIONAR";
            this.btnSeleccionarRemolque.UseVisualStyleBackColor = true;
            this.btnSeleccionarRemolque.Click += new System.EventHandler(this.btnSeleccionarRemolque_Click);
            // 
            // dgvAcoplado
            // 
            this.dgvAcoplado.AllowUserToAddRows = false;
            this.dgvAcoplado.AllowUserToDeleteRows = false;
            this.dgvAcoplado.AllowUserToOrderColumns = true;
            this.dgvAcoplado.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAcoplado.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAcoplado.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvAcoplado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAcoplado.Location = new System.Drawing.Point(6, 47);
            this.dgvAcoplado.MultiSelect = false;
            this.dgvAcoplado.Name = "dgvAcoplado";
            this.dgvAcoplado.ReadOnly = true;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.MediumSeaGreen;
            this.dgvAcoplado.RowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvAcoplado.Size = new System.Drawing.Size(838, 245);
            this.dgvAcoplado.TabIndex = 24;
            this.dgvAcoplado.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvAcoplado_CellClick);
            // 
            // gpCamion
            // 
            this.gpCamion.Controls.Add(this.btnCancelarSeleccionCamion);
            this.gpCamion.Controls.Add(this.groupBox7);
            this.gpCamion.Controls.Add(this.btnSeleccionarCamion);
            this.gpCamion.Controls.Add(this.dgvCamion);
            this.gpCamion.Location = new System.Drawing.Point(12, 62);
            this.gpCamion.Name = "gpCamion";
            this.gpCamion.Size = new System.Drawing.Size(1298, 299);
            this.gpCamion.TabIndex = 34;
            this.gpCamion.TabStop = false;
            this.gpCamion.Text = "CAMION";
            // 
            // btnCancelarSeleccionCamion
            // 
            this.btnCancelarSeleccionCamion.Enabled = false;
            this.btnCancelarSeleccionCamion.Location = new System.Drawing.Point(434, 19);
            this.btnCancelarSeleccionCamion.Name = "btnCancelarSeleccionCamion";
            this.btnCancelarSeleccionCamion.Size = new System.Drawing.Size(411, 23);
            this.btnCancelarSeleccionCamion.TabIndex = 32;
            this.btnCancelarSeleccionCamion.Text = "CANCELAR SELECCION";
            this.btnCancelarSeleccionCamion.UseVisualStyleBackColor = true;
            this.btnCancelarSeleccionCamion.Click += new System.EventHandler(this.btnCancelarSeleccionCamion_Click);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.lbKmNeumCamion);
            this.groupBox7.Controls.Add(this.label403);
            this.groupBox7.Controls.Add(this.lbEstadoCamion);
            this.groupBox7.Controls.Add(this.lblKmUnidadCamion);
            this.groupBox7.Controls.Add(this.label77);
            this.groupBox7.Controls.Add(this.lblEngancheCamion);
            this.groupBox7.Controls.Add(this.label79);
            this.groupBox7.Controls.Add(this.lbVolumenCamion);
            this.groupBox7.Controls.Add(this.label454);
            this.groupBox7.Controls.Add(this.lbTaraCamion);
            this.groupBox7.Controls.Add(this.label334);
            this.groupBox7.Controls.Add(this.lbCapCargaCamion);
            this.groupBox7.Controls.Add(this.label42);
            this.groupBox7.Controls.Add(this.label34);
            this.groupBox7.Controls.Add(this.lbLongitudCamion);
            this.groupBox7.Controls.Add(this.label32);
            this.groupBox7.Controls.Add(this.lbAnchoExtCamion);
            this.groupBox7.Controls.Add(this.label28);
            this.groupBox7.Controls.Add(this.lbAnchoIntCamion);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.lbAlturaCamion);
            this.groupBox7.Controls.Add(this.label18);
            this.groupBox7.Location = new System.Drawing.Point(866, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(426, 273);
            this.groupBox7.TabIndex = 31;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "DATOS CAMION";
            // 
            // lbKmNeumCamion
            // 
            this.lbKmNeumCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbKmNeumCamion.Location = new System.Drawing.Point(332, 201);
            this.lbKmNeumCamion.Name = "lbKmNeumCamion";
            this.lbKmNeumCamion.Size = new System.Drawing.Size(88, 23);
            this.lbKmNeumCamion.TabIndex = 69;
            this.lbKmNeumCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label403
            // 
            this.label403.Location = new System.Drawing.Point(219, 201);
            this.label403.Name = "label403";
            this.label403.Size = new System.Drawing.Size(108, 23);
            this.label403.TabIndex = 68;
            this.label403.Text = "Km Neumaticos";
            this.label403.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbEstadoCamion
            // 
            this.lbEstadoCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbEstadoCamion.Location = new System.Drawing.Point(120, 243);
            this.lbEstadoCamion.Name = "lbEstadoCamion";
            this.lbEstadoCamion.Size = new System.Drawing.Size(300, 23);
            this.lbEstadoCamion.TabIndex = 67;
            this.lbEstadoCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmUnidadCamion
            // 
            this.lblKmUnidadCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblKmUnidadCamion.Location = new System.Drawing.Point(333, 159);
            this.lblKmUnidadCamion.Name = "lblKmUnidadCamion";
            this.lblKmUnidadCamion.Size = new System.Drawing.Size(88, 23);
            this.lblKmUnidadCamion.TabIndex = 66;
            this.lblKmUnidadCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label77
            // 
            this.label77.Location = new System.Drawing.Point(219, 159);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(108, 23);
            this.label77.TabIndex = 65;
            this.label77.Text = "Km Unidad";
            this.label77.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEngancheCamion
            // 
            this.lblEngancheCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEngancheCamion.Location = new System.Drawing.Point(120, 201);
            this.lblEngancheCamion.Name = "lblEngancheCamion";
            this.lblEngancheCamion.Size = new System.Drawing.Size(88, 23);
            this.lblEngancheCamion.TabIndex = 64;
            this.lblEngancheCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label79
            // 
            this.label79.Location = new System.Drawing.Point(10, 243);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(108, 23);
            this.label79.TabIndex = 63;
            this.label79.Text = "Estado";
            this.label79.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbVolumenCamion
            // 
            this.lbVolumenCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbVolumenCamion.Location = new System.Drawing.Point(333, 117);
            this.lbVolumenCamion.Name = "lbVolumenCamion";
            this.lbVolumenCamion.Size = new System.Drawing.Size(88, 23);
            this.lbVolumenCamion.TabIndex = 58;
            this.lbVolumenCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label454
            // 
            this.label454.Location = new System.Drawing.Point(219, 117);
            this.label454.Name = "label454";
            this.label454.Size = new System.Drawing.Size(108, 23);
            this.label454.TabIndex = 57;
            this.label454.Text = "Volumen";
            this.label454.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbTaraCamion
            // 
            this.lbTaraCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbTaraCamion.Location = new System.Drawing.Point(332, 75);
            this.lbTaraCamion.Name = "lbTaraCamion";
            this.lbTaraCamion.Size = new System.Drawing.Size(88, 23);
            this.lbTaraCamion.TabIndex = 56;
            this.lbTaraCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label334
            // 
            this.label334.Location = new System.Drawing.Point(219, 75);
            this.label334.Name = "label334";
            this.label334.Size = new System.Drawing.Size(108, 23);
            this.label334.TabIndex = 55;
            this.label334.Text = "Tara";
            this.label334.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbCapCargaCamion
            // 
            this.lbCapCargaCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbCapCargaCamion.Location = new System.Drawing.Point(332, 33);
            this.lbCapCargaCamion.Name = "lbCapCargaCamion";
            this.lbCapCargaCamion.Size = new System.Drawing.Size(88, 23);
            this.lbCapCargaCamion.TabIndex = 54;
            this.lbCapCargaCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label42
            // 
            this.label42.Location = new System.Drawing.Point(219, 33);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(108, 23);
            this.label42.TabIndex = 53;
            this.label42.Text = "Capacidad Carga";
            this.label42.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label34
            // 
            this.label34.Location = new System.Drawing.Point(6, 201);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(108, 23);
            this.label34.TabIndex = 51;
            this.label34.Text = "Enganche";
            this.label34.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbLongitudCamion
            // 
            this.lbLongitudCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbLongitudCamion.Location = new System.Drawing.Point(120, 159);
            this.lbLongitudCamion.Name = "lbLongitudCamion";
            this.lbLongitudCamion.Size = new System.Drawing.Size(88, 23);
            this.lbLongitudCamion.TabIndex = 50;
            this.lbLongitudCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.Location = new System.Drawing.Point(6, 159);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(108, 23);
            this.label32.TabIndex = 49;
            this.label32.Text = "Longitud";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAnchoExtCamion
            // 
            this.lbAnchoExtCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAnchoExtCamion.Location = new System.Drawing.Point(120, 117);
            this.lbAnchoExtCamion.Name = "lbAnchoExtCamion";
            this.lbAnchoExtCamion.Size = new System.Drawing.Size(88, 23);
            this.lbAnchoExtCamion.TabIndex = 48;
            this.lbAnchoExtCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.Location = new System.Drawing.Point(10, 117);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(108, 23);
            this.label28.TabIndex = 47;
            this.label28.Text = "Ancho Exterior";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAnchoIntCamion
            // 
            this.lbAnchoIntCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAnchoIntCamion.Location = new System.Drawing.Point(120, 75);
            this.lbAnchoIntCamion.Name = "lbAnchoIntCamion";
            this.lbAnchoIntCamion.Size = new System.Drawing.Size(88, 23);
            this.lbAnchoIntCamion.TabIndex = 46;
            this.lbAnchoIntCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Location = new System.Drawing.Point(10, 75);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(108, 23);
            this.label24.TabIndex = 45;
            this.label24.Text = "Ancho Interior";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbAlturaCamion
            // 
            this.lbAlturaCamion.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lbAlturaCamion.Location = new System.Drawing.Point(120, 33);
            this.lbAlturaCamion.Name = "lbAlturaCamion";
            this.lbAlturaCamion.Size = new System.Drawing.Size(88, 23);
            this.lbAlturaCamion.TabIndex = 44;
            this.lbAlturaCamion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Location = new System.Drawing.Point(10, 33);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(108, 23);
            this.label18.TabIndex = 43;
            this.label18.Text = "Altura";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnSeleccionarCamion
            // 
            this.btnSeleccionarCamion.Enabled = false;
            this.btnSeleccionarCamion.Location = new System.Drawing.Point(6, 18);
            this.btnSeleccionarCamion.Name = "btnSeleccionarCamion";
            this.btnSeleccionarCamion.Size = new System.Drawing.Size(411, 23);
            this.btnSeleccionarCamion.TabIndex = 25;
            this.btnSeleccionarCamion.Text = "SELECCIONAR";
            this.btnSeleccionarCamion.UseVisualStyleBackColor = true;
            this.btnSeleccionarCamion.Click += new System.EventHandler(this.btnSeleccionarCamion_Click);
            // 
            // dgvCamion
            // 
            this.dgvCamion.AllowUserToAddRows = false;
            this.dgvCamion.AllowUserToDeleteRows = false;
            this.dgvCamion.AllowUserToOrderColumns = true;
            this.dgvCamion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCamion.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCamion.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgvCamion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCamion.Location = new System.Drawing.Point(6, 47);
            this.dgvCamion.MultiSelect = false;
            this.dgvCamion.Name = "dgvCamion";
            this.dgvCamion.ReadOnly = true;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.MediumSeaGreen;
            this.dgvCamion.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvCamion.Size = new System.Drawing.Size(838, 245);
            this.dgvCamion.TabIndex = 24;
            this.dgvCamion.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCamion_CellClick);
            // 
            // pnlCambiarDeUnidad
            // 
            this.pnlCambiarDeUnidad.BackColor = System.Drawing.Color.Maroon;
            this.pnlCambiarDeUnidad.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlCambiarDeUnidad.Controls.Add(this.btnViajeOkCambioDeUnidad);
            this.pnlCambiarDeUnidad.Controls.Add(this.btnViajeCancelarCambioUnidad);
            this.pnlCambiarDeUnidad.Controls.Add(this.btnSeleccionPedido);
            this.pnlCambiarDeUnidad.Controls.Add(this.lblCambiarUnidadesViaje);
            this.pnlCambiarDeUnidad.Location = new System.Drawing.Point(12, 14);
            this.pnlCambiarDeUnidad.Name = "pnlCambiarDeUnidad";
            this.pnlCambiarDeUnidad.Size = new System.Drawing.Size(634, 42);
            this.pnlCambiarDeUnidad.TabIndex = 33;
            // 
            // btnViajeOkCambioDeUnidad
            // 
            this.btnViajeOkCambioDeUnidad.Location = new System.Drawing.Point(505, 9);
            this.btnViajeOkCambioDeUnidad.Name = "btnViajeOkCambioDeUnidad";
            this.btnViajeOkCambioDeUnidad.Size = new System.Drawing.Size(75, 23);
            this.btnViajeOkCambioDeUnidad.TabIndex = 33;
            this.btnViajeOkCambioDeUnidad.Text = "Ok";
            this.btnViajeOkCambioDeUnidad.UseVisualStyleBackColor = true;
            this.btnViajeOkCambioDeUnidad.Visible = false;
            this.btnViajeOkCambioDeUnidad.Click += new System.EventHandler(this.btnViajeOkCambioDeUnidad_Click);
            // 
            // btnViajeCancelarCambioUnidad
            // 
            this.btnViajeCancelarCambioUnidad.Location = new System.Drawing.Point(407, 9);
            this.btnViajeCancelarCambioUnidad.Name = "btnViajeCancelarCambioUnidad";
            this.btnViajeCancelarCambioUnidad.Size = new System.Drawing.Size(75, 23);
            this.btnViajeCancelarCambioUnidad.TabIndex = 32;
            this.btnViajeCancelarCambioUnidad.Text = "Cancelar";
            this.btnViajeCancelarCambioUnidad.UseVisualStyleBackColor = true;
            this.btnViajeCancelarCambioUnidad.Visible = false;
            this.btnViajeCancelarCambioUnidad.Click += new System.EventHandler(this.btnViajeCancelarCambioUnidad_Click);
            // 
            // btnSeleccionPedido
            // 
            this.btnSeleccionPedido.BackColor = System.Drawing.Color.Silver;
            this.btnSeleccionPedido.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnSeleccionPedido.Location = new System.Drawing.Point(-1, -1);
            this.btnSeleccionPedido.Name = "btnSeleccionPedido";
            this.btnSeleccionPedido.Size = new System.Drawing.Size(634, 43);
            this.btnSeleccionPedido.TabIndex = 33;
            this.btnSeleccionPedido.Text = "SELECCIONAR PEDIDO";
            this.btnSeleccionPedido.UseVisualStyleBackColor = false;
            this.btnSeleccionPedido.Click += new System.EventHandler(this.btnSeleccionPedido_Click);
            // 
            // lblCambiarUnidadesViaje
            // 
            this.lblCambiarUnidadesViaje.AutoSize = true;
            this.lblCambiarUnidadesViaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCambiarUnidadesViaje.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.lblCambiarUnidadesViaje.Location = new System.Drawing.Point(16, 6);
            this.lblCambiarUnidadesViaje.Name = "lblCambiarUnidadesViaje";
            this.lblCambiarUnidadesViaje.Size = new System.Drawing.Size(297, 25);
            this.lblCambiarUnidadesViaje.TabIndex = 8;
            this.lblCambiarUnidadesViaje.Text = "Cambiar unidad de viaje id 32";
            this.lblCambiarUnidadesViaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCambiarUnidadesViaje.Visible = false;
            // 
            // gbLogistica
            // 
            this.gbLogistica.Controls.Add(this.tbViajes);
            this.gbLogistica.Controls.Add(this.tpUnidades);
            this.gbLogistica.Controls.Add(this.tabPage1);
            this.gbLogistica.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbLogistica.ItemSize = new System.Drawing.Size(231, 25);
            this.gbLogistica.Location = new System.Drawing.Point(12, 3);
            this.gbLogistica.Name = "gbLogistica";
            this.gbLogistica.SelectedIndex = 0;
            this.gbLogistica.Size = new System.Drawing.Size(1322, 698);
            this.gbLogistica.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.gbLogistica.TabIndex = 6;
            this.gbLogistica.Selected += new System.Windows.Forms.TabControlEventHandler(this.gbLogistica_Selected);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.panel2);
            this.tabPage1.Controls.Add(this.panel3);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Controls.Add(this.groupBox14);
            this.tabPage1.Location = new System.Drawing.Point(4, 29);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(2);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1314, 665);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "CONTROL DE MANTENIMIENTO DE UNIDADES";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.LightYellow;
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.lblAcopladoEstado);
            this.panel2.Controls.Add(this.label16);
            this.panel2.Controls.Add(this.lblAcopladoDtpEntregado);
            this.panel2.Controls.Add(this.lblAcopladoDtpSalidaAprox);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.lblAcopladoDtpEntrada);
            this.panel2.Controls.Add(this.lblAcopladoDiasAFavor);
            this.panel2.Controls.Add(this.label43);
            this.panel2.Controls.Add(this.lblAcopladoSalidaAproxText);
            this.panel2.Controls.Add(this.label45);
            this.panel2.Controls.Add(this.gbAcopladoTipoMantenimiento);
            this.panel2.Controls.Add(this.tbAcopladoObservaciones);
            this.panel2.Location = new System.Drawing.Point(833, 322);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(478, 317);
            this.panel2.TabIndex = 72;
            // 
            // lblAcopladoEstado
            // 
            this.lblAcopladoEstado.BackColor = System.Drawing.Color.White;
            this.lblAcopladoEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAcopladoEstado.Location = new System.Drawing.Point(357, 39);
            this.lblAcopladoEstado.Name = "lblAcopladoEstado";
            this.lblAcopladoEstado.Size = new System.Drawing.Size(102, 23);
            this.lblAcopladoEstado.TabIndex = 97;
            this.lblAcopladoEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(357, 16);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(94, 23);
            this.label16.TabIndex = 98;
            this.label16.Text = "Estado";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDtpEntregado
            // 
            this.lblAcopladoDtpEntregado.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDtpEntregado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDtpEntregado.Location = new System.Drawing.Point(258, 277);
            this.lblAcopladoDtpEntregado.Name = "lblAcopladoDtpEntregado";
            this.lblAcopladoDtpEntregado.Size = new System.Drawing.Size(107, 23);
            this.lblAcopladoDtpEntregado.TabIndex = 96;
            this.lblAcopladoDtpEntregado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDtpSalidaAprox
            // 
            this.lblAcopladoDtpSalidaAprox.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDtpSalidaAprox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDtpSalidaAprox.Location = new System.Drawing.Point(144, 277);
            this.lblAcopladoDtpSalidaAprox.Name = "lblAcopladoDtpSalidaAprox";
            this.lblAcopladoDtpSalidaAprox.Size = new System.Drawing.Size(107, 23);
            this.lblAcopladoDtpSalidaAprox.TabIndex = 94;
            this.lblAcopladoDtpSalidaAprox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.Location = new System.Drawing.Point(273, 254);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(72, 23);
            this.label17.TabIndex = 95;
            this.label17.Text = "Entregado";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDtpEntrada
            // 
            this.lblAcopladoDtpEntrada.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDtpEntrada.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDtpEntrada.Location = new System.Drawing.Point(27, 277);
            this.lblAcopladoDtpEntrada.Name = "lblAcopladoDtpEntrada";
            this.lblAcopladoDtpEntrada.Size = new System.Drawing.Size(107, 23);
            this.lblAcopladoDtpEntrada.TabIndex = 93;
            this.lblAcopladoDtpEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoDiasAFavor
            // 
            this.lblAcopladoDiasAFavor.BackColor = System.Drawing.Color.White;
            this.lblAcopladoDiasAFavor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblAcopladoDiasAFavor.Location = new System.Drawing.Point(370, 277);
            this.lblAcopladoDiasAFavor.Name = "lblAcopladoDiasAFavor";
            this.lblAcopladoDiasAFavor.Size = new System.Drawing.Size(61, 23);
            this.lblAcopladoDiasAFavor.TabIndex = 92;
            this.lblAcopladoDiasAFavor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label43
            // 
            this.label43.Location = new System.Drawing.Point(367, 254);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(64, 23);
            this.label43.TabIndex = 91;
            this.label43.Text = "Dias a favor";
            this.label43.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAcopladoSalidaAproxText
            // 
            this.lblAcopladoSalidaAproxText.Location = new System.Drawing.Point(160, 254);
            this.lblAcopladoSalidaAproxText.Name = "lblAcopladoSalidaAproxText";
            this.lblAcopladoSalidaAproxText.Size = new System.Drawing.Size(72, 23);
            this.lblAcopladoSalidaAproxText.TabIndex = 90;
            this.lblAcopladoSalidaAproxText.Text = "Salida aprox";
            this.lblAcopladoSalidaAproxText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label45
            // 
            this.label45.Location = new System.Drawing.Point(49, 254);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 23);
            this.label45.TabIndex = 89;
            this.label45.Text = "Ingresó";
            this.label45.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbAcopladoTipoMantenimiento
            // 
            this.gbAcopladoTipoMantenimiento.Controls.Add(this.cbAcopladoNeumaticos);
            this.gbAcopladoTipoMantenimiento.Controls.Add(this.cbAcopladoOtros);
            this.gbAcopladoTipoMantenimiento.Controls.Add(this.cbAcopladoService);
            this.gbAcopladoTipoMantenimiento.Enabled = false;
            this.gbAcopladoTipoMantenimiento.Location = new System.Drawing.Point(6, 10);
            this.gbAcopladoTipoMantenimiento.Name = "gbAcopladoTipoMantenimiento";
            this.gbAcopladoTipoMantenimiento.Size = new System.Drawing.Size(283, 38);
            this.gbAcopladoTipoMantenimiento.TabIndex = 82;
            this.gbAcopladoTipoMantenimiento.TabStop = false;
            this.gbAcopladoTipoMantenimiento.Text = "Tipo de mantenimiento";
            // 
            // cbAcopladoNeumaticos
            // 
            this.cbAcopladoNeumaticos.AutoSize = true;
            this.cbAcopladoNeumaticos.Location = new System.Drawing.Point(98, 16);
            this.cbAcopladoNeumaticos.Name = "cbAcopladoNeumaticos";
            this.cbAcopladoNeumaticos.Size = new System.Drawing.Size(122, 17);
            this.cbAcopladoNeumaticos.TabIndex = 79;
            this.cbAcopladoNeumaticos.Text = "Cambio neumaticos";
            this.cbAcopladoNeumaticos.UseVisualStyleBackColor = true;
            // 
            // cbAcopladoOtros
            // 
            this.cbAcopladoOtros.AutoSize = true;
            this.cbAcopladoOtros.Location = new System.Drawing.Point(222, 16);
            this.cbAcopladoOtros.Name = "cbAcopladoOtros";
            this.cbAcopladoOtros.Size = new System.Drawing.Size(54, 17);
            this.cbAcopladoOtros.TabIndex = 78;
            this.cbAcopladoOtros.Text = "Otros";
            this.cbAcopladoOtros.UseVisualStyleBackColor = true;
            // 
            // cbAcopladoService
            // 
            this.cbAcopladoService.AutoSize = true;
            this.cbAcopladoService.Location = new System.Drawing.Point(18, 16);
            this.cbAcopladoService.Name = "cbAcopladoService";
            this.cbAcopladoService.Size = new System.Drawing.Size(64, 17);
            this.cbAcopladoService.TabIndex = 77;
            this.cbAcopladoService.Text = "Service";
            this.cbAcopladoService.UseVisualStyleBackColor = true;
            // 
            // tbAcopladoObservaciones
            // 
            this.tbAcopladoObservaciones.Location = new System.Drawing.Point(4, 65);
            this.tbAcopladoObservaciones.Multiline = true;
            this.tbAcopladoObservaciones.Name = "tbAcopladoObservaciones";
            this.tbAcopladoObservaciones.ReadOnly = true;
            this.tbAcopladoObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbAcopladoObservaciones.Size = new System.Drawing.Size(461, 186);
            this.tbAcopladoObservaciones.TabIndex = 65;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LightYellow;
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel3.Controls.Add(this.lblCamionEstado);
            this.panel3.Controls.Add(this.label19);
            this.panel3.Controls.Add(this.lblCamionDtpEntregado);
            this.panel3.Controls.Add(this.label21);
            this.panel3.Controls.Add(this.lblCamionDtpSalidaAprox);
            this.panel3.Controls.Add(this.lblCamionDtpEntrada);
            this.panel3.Controls.Add(this.lblCamionDiasAFavor);
            this.panel3.Controls.Add(this.label26);
            this.panel3.Controls.Add(this.lblCamionSalidaAproxText);
            this.panel3.Controls.Add(this.label40);
            this.panel3.Controls.Add(this.gbCamionTipoMantenimiento);
            this.panel3.Controls.Add(this.tbCamionObservaciones);
            this.panel3.Location = new System.Drawing.Point(833, 12);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(478, 304);
            this.panel3.TabIndex = 71;
            // 
            // lblCamionEstado
            // 
            this.lblCamionEstado.BackColor = System.Drawing.Color.White;
            this.lblCamionEstado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionEstado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCamionEstado.Location = new System.Drawing.Point(356, 28);
            this.lblCamionEstado.Name = "lblCamionEstado";
            this.lblCamionEstado.Size = new System.Drawing.Size(102, 23);
            this.lblCamionEstado.TabIndex = 97;
            this.lblCamionEstado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Location = new System.Drawing.Point(356, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(94, 23);
            this.label19.TabIndex = 96;
            this.label19.Text = "Estado";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDtpEntregado
            // 
            this.lblCamionDtpEntregado.BackColor = System.Drawing.Color.White;
            this.lblCamionDtpEntregado.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDtpEntregado.Location = new System.Drawing.Point(256, 267);
            this.lblCamionDtpEntregado.Name = "lblCamionDtpEntregado";
            this.lblCamionDtpEntregado.Size = new System.Drawing.Size(107, 23);
            this.lblCamionDtpEntregado.TabIndex = 95;
            this.lblCamionDtpEntregado.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.Location = new System.Drawing.Point(271, 246);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(72, 23);
            this.label21.TabIndex = 94;
            this.label21.Text = "Entregado";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDtpSalidaAprox
            // 
            this.lblCamionDtpSalidaAprox.BackColor = System.Drawing.Color.White;
            this.lblCamionDtpSalidaAprox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDtpSalidaAprox.Location = new System.Drawing.Point(143, 267);
            this.lblCamionDtpSalidaAprox.Name = "lblCamionDtpSalidaAprox";
            this.lblCamionDtpSalidaAprox.Size = new System.Drawing.Size(107, 23);
            this.lblCamionDtpSalidaAprox.TabIndex = 93;
            this.lblCamionDtpSalidaAprox.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDtpEntrada
            // 
            this.lblCamionDtpEntrada.BackColor = System.Drawing.Color.White;
            this.lblCamionDtpEntrada.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDtpEntrada.Location = new System.Drawing.Point(26, 267);
            this.lblCamionDtpEntrada.Name = "lblCamionDtpEntrada";
            this.lblCamionDtpEntrada.Size = new System.Drawing.Size(107, 23);
            this.lblCamionDtpEntrada.TabIndex = 92;
            this.lblCamionDtpEntrada.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionDiasAFavor
            // 
            this.lblCamionDiasAFavor.BackColor = System.Drawing.Color.White;
            this.lblCamionDiasAFavor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblCamionDiasAFavor.Location = new System.Drawing.Point(369, 266);
            this.lblCamionDiasAFavor.Name = "lblCamionDiasAFavor";
            this.lblCamionDiasAFavor.Size = new System.Drawing.Size(61, 23);
            this.lblCamionDiasAFavor.TabIndex = 91;
            this.lblCamionDiasAFavor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Location = new System.Drawing.Point(366, 245);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(64, 23);
            this.label26.TabIndex = 90;
            this.label26.Text = "Dias a favor";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCamionSalidaAproxText
            // 
            this.lblCamionSalidaAproxText.Location = new System.Drawing.Point(159, 246);
            this.lblCamionSalidaAproxText.Name = "lblCamionSalidaAproxText";
            this.lblCamionSalidaAproxText.Size = new System.Drawing.Size(72, 23);
            this.lblCamionSalidaAproxText.TabIndex = 89;
            this.lblCamionSalidaAproxText.Text = "Salida aprox";
            this.lblCamionSalidaAproxText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.Location = new System.Drawing.Point(48, 246);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(47, 23);
            this.label40.TabIndex = 88;
            this.label40.Text = "Ingresó";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // gbCamionTipoMantenimiento
            // 
            this.gbCamionTipoMantenimiento.Controls.Add(this.cbCamionNeumaticos);
            this.gbCamionTipoMantenimiento.Controls.Add(this.cbCamionOtros);
            this.gbCamionTipoMantenimiento.Controls.Add(this.cbCamionService);
            this.gbCamionTipoMantenimiento.Enabled = false;
            this.gbCamionTipoMantenimiento.Location = new System.Drawing.Point(3, 10);
            this.gbCamionTipoMantenimiento.Name = "gbCamionTipoMantenimiento";
            this.gbCamionTipoMantenimiento.Size = new System.Drawing.Size(282, 38);
            this.gbCamionTipoMantenimiento.TabIndex = 81;
            this.gbCamionTipoMantenimiento.TabStop = false;
            this.gbCamionTipoMantenimiento.Text = "Tipo de mantenimiento";
            // 
            // cbCamionNeumaticos
            // 
            this.cbCamionNeumaticos.AutoSize = true;
            this.cbCamionNeumaticos.Location = new System.Drawing.Point(97, 14);
            this.cbCamionNeumaticos.Name = "cbCamionNeumaticos";
            this.cbCamionNeumaticos.Size = new System.Drawing.Size(122, 17);
            this.cbCamionNeumaticos.TabIndex = 79;
            this.cbCamionNeumaticos.Text = "Cambio neumaticos";
            this.cbCamionNeumaticos.UseVisualStyleBackColor = true;
            // 
            // cbCamionOtros
            // 
            this.cbCamionOtros.AutoSize = true;
            this.cbCamionOtros.Location = new System.Drawing.Point(221, 14);
            this.cbCamionOtros.Name = "cbCamionOtros";
            this.cbCamionOtros.Size = new System.Drawing.Size(54, 17);
            this.cbCamionOtros.TabIndex = 78;
            this.cbCamionOtros.Text = "Otros";
            this.cbCamionOtros.UseVisualStyleBackColor = true;
            // 
            // cbCamionService
            // 
            this.cbCamionService.AutoSize = true;
            this.cbCamionService.Location = new System.Drawing.Point(17, 14);
            this.cbCamionService.Name = "cbCamionService";
            this.cbCamionService.Size = new System.Drawing.Size(64, 17);
            this.cbCamionService.TabIndex = 77;
            this.cbCamionService.Text = "Service";
            this.cbCamionService.UseVisualStyleBackColor = true;
            // 
            // tbCamionObservaciones
            // 
            this.tbCamionObservaciones.Location = new System.Drawing.Point(3, 54);
            this.tbCamionObservaciones.Multiline = true;
            this.tbCamionObservaciones.Name = "tbCamionObservaciones";
            this.tbCamionObservaciones.ReadOnly = true;
            this.tbCamionObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbCamionObservaciones.Size = new System.Drawing.Size(461, 186);
            this.tbCamionObservaciones.TabIndex = 65;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.pnlAcopladoBusquedaFecha);
            this.groupBox4.Controls.Add(this.btnAcopladoVer);
            this.groupBox4.Controls.Add(this.cbFiltroAcoplado2);
            this.groupBox4.Controls.Add(this.cbFiltroAcoplado);
            this.groupBox4.Controls.Add(this.tbBusquedaAcoplado);
            this.groupBox4.Controls.Add(this.dgvAcopladoMant);
            this.groupBox4.Location = new System.Drawing.Point(14, 322);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(813, 317);
            this.groupBox4.TabIndex = 70;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "REMOLQUE";
            // 
            // pnlAcopladoBusquedaFecha
            // 
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.dtpAcopladoBusquedaHasta);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.label10);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.dtpAcopladoBusquedaDesde);
            this.pnlAcopladoBusquedaFecha.Controls.Add(this.label14);
            this.pnlAcopladoBusquedaFecha.Location = new System.Drawing.Point(366, 13);
            this.pnlAcopladoBusquedaFecha.Name = "pnlAcopladoBusquedaFecha";
            this.pnlAcopladoBusquedaFecha.Size = new System.Drawing.Size(333, 28);
            this.pnlAcopladoBusquedaFecha.TabIndex = 85;
            // 
            // dtpAcopladoBusquedaHasta
            // 
            this.dtpAcopladoBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAcopladoBusquedaHasta.Location = new System.Drawing.Point(228, 6);
            this.dtpAcopladoBusquedaHasta.Name = "dtpAcopladoBusquedaHasta";
            this.dtpAcopladoBusquedaHasta.Size = new System.Drawing.Size(104, 21);
            this.dtpAcopladoBusquedaHasta.TabIndex = 4;
            this.dtpAcopladoBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpAcopladoBusquedaDesde_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(190, 7);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(35, 13);
            this.label10.TabIndex = 3;
            this.label10.Text = "hasta";
            // 
            // dtpAcopladoBusquedaDesde
            // 
            this.dtpAcopladoBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpAcopladoBusquedaDesde.Location = new System.Drawing.Point(80, 6);
            this.dtpAcopladoBusquedaDesde.Name = "dtpAcopladoBusquedaDesde";
            this.dtpAcopladoBusquedaDesde.Size = new System.Drawing.Size(104, 21);
            this.dtpAcopladoBusquedaDesde.TabIndex = 2;
            this.dtpAcopladoBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpAcopladoBusquedaDesde_ValueChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(3, 7);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(78, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "Buscar  desde ";
            // 
            // btnAcopladoVer
            // 
            this.btnAcopladoVer.Location = new System.Drawing.Point(705, 16);
            this.btnAcopladoVer.Name = "btnAcopladoVer";
            this.btnAcopladoVer.Size = new System.Drawing.Size(96, 24);
            this.btnAcopladoVer.TabIndex = 81;
            this.btnAcopladoVer.Text = "Ver Detalles";
            this.btnAcopladoVer.UseVisualStyleBackColor = true;
            this.btnAcopladoVer.Click += new System.EventHandler(this.btnCamionVer_Click);
            // 
            // cbFiltroAcoplado2
            // 
            this.cbFiltroAcoplado2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroAcoplado2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroAcoplado2.FormattingEnabled = true;
            this.cbFiltroAcoplado2.Location = new System.Drawing.Point(191, 18);
            this.cbFiltroAcoplado2.Name = "cbFiltroAcoplado2";
            this.cbFiltroAcoplado2.Size = new System.Drawing.Size(168, 23);
            this.cbFiltroAcoplado2.TabIndex = 83;
            this.cbFiltroAcoplado2.SelectedIndexChanged += new System.EventHandler(this.cbFiltroAcoplado_SelectedIndexChanged);
            // 
            // cbFiltroAcoplado
            // 
            this.cbFiltroAcoplado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroAcoplado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroAcoplado.FormattingEnabled = true;
            this.cbFiltroAcoplado.Items.AddRange(new object[] {
            "Todos",
            "Por revisar",
            "En proceso",
            "Entregados"});
            this.cbFiltroAcoplado.Location = new System.Drawing.Point(6, 18);
            this.cbFiltroAcoplado.Name = "cbFiltroAcoplado";
            this.cbFiltroAcoplado.Size = new System.Drawing.Size(179, 23);
            this.cbFiltroAcoplado.TabIndex = 31;
            this.cbFiltroAcoplado.SelectedIndexChanged += new System.EventHandler(this.cbFiltroAcoplado_SelectedIndexChanged);
            // 
            // tbBusquedaAcoplado
            // 
            this.tbBusquedaAcoplado.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBusquedaAcoplado.Location = new System.Drawing.Point(405, 18);
            this.tbBusquedaAcoplado.Multiline = true;
            this.tbBusquedaAcoplado.Name = "tbBusquedaAcoplado";
            this.tbBusquedaAcoplado.Size = new System.Drawing.Size(248, 24);
            this.tbBusquedaAcoplado.TabIndex = 29;
            this.tbBusquedaAcoplado.TextChanged += new System.EventHandler(this.tbBusquedaAcoplado_TextChanged);
            // 
            // dgvAcopladoMant
            // 
            this.dgvAcopladoMant.AllowUserToAddRows = false;
            this.dgvAcopladoMant.AllowUserToDeleteRows = false;
            this.dgvAcopladoMant.AllowUserToOrderColumns = true;
            this.dgvAcopladoMant.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAcopladoMant.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvAcopladoMant.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvAcopladoMant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvAcopladoMant.Location = new System.Drawing.Point(6, 47);
            this.dgvAcopladoMant.MultiSelect = false;
            this.dgvAcopladoMant.Name = "dgvAcopladoMant";
            this.dgvAcopladoMant.ReadOnly = true;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.MediumSeaGreen;
            this.dgvAcopladoMant.RowsDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvAcopladoMant.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvAcopladoMant.Size = new System.Drawing.Size(795, 264);
            this.dgvAcopladoMant.TabIndex = 24;
            this.dgvAcopladoMant.CurrentCellChanged += new System.EventHandler(this.dgvAcopladoMant_CurrentCellChanged_1);
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.btnCamionVer);
            this.groupBox14.Controls.Add(this.pnlCamionBusquedaFecha);
            this.groupBox14.Controls.Add(this.cbFiltroCamion2);
            this.groupBox14.Controls.Add(this.cbFiltroCamion);
            this.groupBox14.Controls.Add(this.tbBusquedaCamion);
            this.groupBox14.Controls.Add(this.dgvCamionMant);
            this.groupBox14.Location = new System.Drawing.Point(8, 12);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(819, 304);
            this.groupBox14.TabIndex = 69;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "CAMION";
            // 
            // btnCamionVer
            // 
            this.btnCamionVer.Location = new System.Drawing.Point(705, 17);
            this.btnCamionVer.Name = "btnCamionVer";
            this.btnCamionVer.Size = new System.Drawing.Size(96, 24);
            this.btnCamionVer.TabIndex = 80;
            this.btnCamionVer.Text = "Ver Detalles";
            this.btnCamionVer.UseVisualStyleBackColor = true;
            this.btnCamionVer.Click += new System.EventHandler(this.btnCamionVer_Click);
            // 
            // pnlCamionBusquedaFecha
            // 
            this.pnlCamionBusquedaFecha.Controls.Add(this.dtpCamionBusquedaHasta);
            this.pnlCamionBusquedaFecha.Controls.Add(this.label36);
            this.pnlCamionBusquedaFecha.Controls.Add(this.dtpbCamionBusquedaDesde);
            this.pnlCamionBusquedaFecha.Controls.Add(this.label38);
            this.pnlCamionBusquedaFecha.Location = new System.Drawing.Point(365, 12);
            this.pnlCamionBusquedaFecha.Name = "pnlCamionBusquedaFecha";
            this.pnlCamionBusquedaFecha.Size = new System.Drawing.Size(333, 28);
            this.pnlCamionBusquedaFecha.TabIndex = 84;
            // 
            // dtpCamionBusquedaHasta
            // 
            this.dtpCamionBusquedaHasta.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpCamionBusquedaHasta.Location = new System.Drawing.Point(228, 6);
            this.dtpCamionBusquedaHasta.Name = "dtpCamionBusquedaHasta";
            this.dtpCamionBusquedaHasta.Size = new System.Drawing.Size(104, 21);
            this.dtpCamionBusquedaHasta.TabIndex = 4;
            this.dtpCamionBusquedaHasta.ValueChanged += new System.EventHandler(this.dtpbCamionBusquedaDesde_ValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(190, 7);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(35, 13);
            this.label36.TabIndex = 3;
            this.label36.Text = "hasta";
            // 
            // dtpbCamionBusquedaDesde
            // 
            this.dtpbCamionBusquedaDesde.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtpbCamionBusquedaDesde.Location = new System.Drawing.Point(80, 6);
            this.dtpbCamionBusquedaDesde.Name = "dtpbCamionBusquedaDesde";
            this.dtpbCamionBusquedaDesde.Size = new System.Drawing.Size(104, 21);
            this.dtpbCamionBusquedaDesde.TabIndex = 2;
            this.dtpbCamionBusquedaDesde.ValueChanged += new System.EventHandler(this.dtpbCamionBusquedaDesde_ValueChanged);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 7);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(78, 13);
            this.label38.TabIndex = 1;
            this.label38.Text = "Buscar  desde ";
            // 
            // cbFiltroCamion2
            // 
            this.cbFiltroCamion2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroCamion2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroCamion2.FormattingEnabled = true;
            this.cbFiltroCamion2.Items.AddRange(new object[] {
            "Id Ticket",
            "Observaciones ticket",
            "Vencidos",
            "Fecha de entrada",
            "Fecha de entrega",
            "Dominio camion",
            "Marca camion"});
            this.cbFiltroCamion2.Location = new System.Drawing.Point(191, 18);
            this.cbFiltroCamion2.Name = "cbFiltroCamion2";
            this.cbFiltroCamion2.Size = new System.Drawing.Size(168, 23);
            this.cbFiltroCamion2.TabIndex = 63;
            this.cbFiltroCamion2.SelectedIndexChanged += new System.EventHandler(this.cbFiltroCamion_SelectedIndexChanged);
            // 
            // cbFiltroCamion
            // 
            this.cbFiltroCamion.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFiltroCamion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbFiltroCamion.FormattingEnabled = true;
            this.cbFiltroCamion.Items.AddRange(new object[] {
            "Todos",
            "Por revisar",
            "En proceso",
            "Entregados"});
            this.cbFiltroCamion.Location = new System.Drawing.Point(6, 18);
            this.cbFiltroCamion.Name = "cbFiltroCamion";
            this.cbFiltroCamion.Size = new System.Drawing.Size(179, 23);
            this.cbFiltroCamion.TabIndex = 30;
            this.cbFiltroCamion.SelectedIndexChanged += new System.EventHandler(this.cbFiltroCamion_SelectedIndexChanged);
            // 
            // tbBusquedaCamion
            // 
            this.tbBusquedaCamion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbBusquedaCamion.Location = new System.Drawing.Point(405, 18);
            this.tbBusquedaCamion.Multiline = true;
            this.tbBusquedaCamion.Name = "tbBusquedaCamion";
            this.tbBusquedaCamion.Size = new System.Drawing.Size(248, 23);
            this.tbBusquedaCamion.TabIndex = 29;
            this.tbBusquedaCamion.TextChanged += new System.EventHandler(this.tbBusquedaCamion_TextChanged);
            // 
            // dgvCamionMant
            // 
            this.dgvCamionMant.AllowUserToAddRows = false;
            this.dgvCamionMant.AllowUserToDeleteRows = false;
            this.dgvCamionMant.AllowUserToOrderColumns = true;
            this.dgvCamionMant.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvCamionMant.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCamionMant.BackgroundColor = System.Drawing.SystemColors.ActiveBorder;
            this.dgvCamionMant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCamionMant.Location = new System.Drawing.Point(6, 47);
            this.dgvCamionMant.MultiSelect = false;
            this.dgvCamionMant.Name = "dgvCamionMant";
            this.dgvCamionMant.ReadOnly = true;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.MediumSeaGreen;
            this.dgvCamionMant.RowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgvCamionMant.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvCamionMant.Size = new System.Drawing.Size(795, 234);
            this.dgvCamionMant.TabIndex = 24;
            this.dgvCamionMant.CurrentCellChanged += new System.EventHandler(this.dgvCamionMant_CurrentCellChanged);
            // 
            // timer
            // 
            this.timer.Interval = 1500;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // frmLogistica
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1341, 704);
            this.Controls.Add(this.gbLogistica);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmLogistica";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ADMINISTRADOR LOGISTICA";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLogistica_FormClosing);
            this.Load += new System.EventHandler(this.frmLogistica_Load);
            this.tbViajes.ResumeLayout(false);
            this.tbViajes.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.pnlViajeAlarmas.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.pnlViajeBusquedaFecha.ResumeLayout(false);
            this.pnlViajeBusquedaFecha.PerformLayout();
            this.pnlViajeBusquedaTexto.ResumeLayout(false);
            this.pnlViajeBusquedaTexto.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvViaje)).EndInit();
            this.tpUnidades.ResumeLayout(false);
            this.gpRemolque.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcoplado)).EndInit();
            this.gpCamion.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamion)).EndInit();
            this.pnlCambiarDeUnidad.ResumeLayout(false);
            this.pnlCambiarDeUnidad.PerformLayout();
            this.gbLogistica.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.gbAcopladoTipoMantenimiento.ResumeLayout(false);
            this.gbAcopladoTipoMantenimiento.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.gbCamionTipoMantenimiento.ResumeLayout(false);
            this.gbCamionTipoMantenimiento.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.pnlAcopladoBusquedaFecha.ResumeLayout(false);
            this.pnlAcopladoBusquedaFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAcopladoMant)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.pnlCamionBusquedaFecha.ResumeLayout(false);
            this.pnlCamionBusquedaFecha.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCamionMant)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tbViajes;
        private System.Windows.Forms.TabPage tpUnidades;
        private System.Windows.Forms.TabControl gbLogistica;
        private System.Windows.Forms.DataGridView dgvViaje;
        private System.Windows.Forms.Panel pnlViajeBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpViajeBusquedaHasta;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dtpbViajeBusquedaDesde;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel pnlViajeBusquedaTexto;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox tbViajeBusquedaTexto;
        private System.Windows.Forms.ComboBox cbViajeFiltrar;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnViajeCambiarUnidad;
        private System.Windows.Forms.Label lblViajeCamionMarca;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label lblViajeCamionModelo;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label lblViajeAcoplado1Dominio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label lblViajeAcoplado1Modelo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lblViajeAcoplado1Marca;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label lblViajeChoferNombre;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label lblViajeChoferTelPersonal;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label lblViajeChoferTelTrabajo;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label lblViajeChoferLegajo;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label lblViajeChoferDireccion;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tbViajeObservaciones;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tbViajePedidoObservaciones;
        private System.Windows.Forms.Label lblViajeCamionDominio;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblViajeCamionTipo;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label lblViajeClienteCuit;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label lblViajeClienteNombre;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cbViajeFiltrar2;
        private System.Windows.Forms.Label lblCambiarUnidadesViaje;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnlCambiarDeUnidad;
        private System.Windows.Forms.Button btnViajeCancelarCambioUnidad;
        private System.Windows.Forms.Button btnViajeOkCambioDeUnidad;
        private System.Windows.Forms.Button btnViajeEditar;
        private System.Windows.Forms.Button btnViajeSuspender;
        private System.Windows.Forms.Button btnViajeEnCurso;
        private System.Windows.Forms.Button btnViajePendiente;
        private System.Windows.Forms.Button btnViajeConcluir;
        private System.Windows.Forms.Button btnViajeCancelar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnViajePorCerrar;
        private System.Windows.Forms.Button btnViajePorSalir;
        private System.Windows.Forms.Button btnCrearViaje;
        private System.Windows.Forms.GroupBox gpRemolque;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Label lbVolumenRemolque;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label lbTaraRemolque;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label lbCapCargaRemolque;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label lbEstadoRemolque;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label lbLongitudRemolque;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label lbAnchoExtRemolque;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label lbAnchoIntRemolque;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label lbAlturaRemolque;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Button btnSeleccionarRemolque;
        private System.Windows.Forms.DataGridView dgvAcoplado;
        private System.Windows.Forms.Button btnSeleccionPedido;
        private System.Windows.Forms.GroupBox gpCamion;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label lbVolumenCamion;
        private System.Windows.Forms.Label label454;
        private System.Windows.Forms.Label lbTaraCamion;
        private System.Windows.Forms.Label label334;
        private System.Windows.Forms.Label lbCapCargaCamion;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label lbLongitudCamion;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label lbAnchoExtCamion;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label lbAnchoIntCamion;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label lbAlturaCamion;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Button btnSeleccionarCamion;
        private System.Windows.Forms.DataGridView dgvCamion;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button btnAlarmaVisible;
        private System.Windows.Forms.Button btnViajeCalendarAcoplado;
        private System.Windows.Forms.Button btnViajeCalendarCamion;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label lbKmNeumRemolque;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label lblKmUnidadRemolque;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.Label lbKmNeumCamion;
        private System.Windows.Forms.Label label403;
        private System.Windows.Forms.Label lbEstadoCamion;
        private System.Windows.Forms.Label lblKmUnidadCamion;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.Label lblEngancheCamion;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.Button btnCancelarSeleccionCamion;
        private System.Windows.Forms.Button btnCancelarSeleccionRemolque;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Panel pnlAcopladoBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpAcopladoBusquedaHasta;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DateTimePicker dtpAcopladoBusquedaDesde;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button btnAcopladoVer;
        private System.Windows.Forms.ComboBox cbFiltroAcoplado2;
        private System.Windows.Forms.ComboBox cbFiltroAcoplado;
        private System.Windows.Forms.TextBox tbBusquedaAcoplado;
        private System.Windows.Forms.DataGridView dgvAcopladoMant;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Button btnCamionVer;
        private System.Windows.Forms.Panel pnlCamionBusquedaFecha;
        private System.Windows.Forms.DateTimePicker dtpCamionBusquedaHasta;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.DateTimePicker dtpbCamionBusquedaDesde;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cbFiltroCamion2;
        private System.Windows.Forms.ComboBox cbFiltroCamion;
        private System.Windows.Forms.TextBox tbBusquedaCamion;
        private System.Windows.Forms.DataGridView dgvCamionMant;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbAcopladoTipoMantenimiento;
        private System.Windows.Forms.CheckBox cbAcopladoNeumaticos;
        private System.Windows.Forms.CheckBox cbAcopladoOtros;
        private System.Windows.Forms.CheckBox cbAcopladoService;
        private System.Windows.Forms.TextBox tbAcopladoObservaciones;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.GroupBox gbCamionTipoMantenimiento;
        private System.Windows.Forms.CheckBox cbCamionNeumaticos;
        private System.Windows.Forms.CheckBox cbCamionOtros;
        private System.Windows.Forms.CheckBox cbCamionService;
        private System.Windows.Forms.TextBox tbCamionObservaciones;
        private System.Windows.Forms.Label lbTipoUnidad;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox pnlViajeAlarmas;
        private System.Windows.Forms.Label lblCamionEstado;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label lblCamionDtpEntregado;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label lblCamionDtpSalidaAprox;
        private System.Windows.Forms.Label lblCamionDtpEntrada;
        private System.Windows.Forms.Label lblCamionDiasAFavor;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label lblCamionSalidaAproxText;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblAcopladoEstado;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label lblAcopladoDtpEntregado;
        private System.Windows.Forms.Label lblAcopladoDtpSalidaAprox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lblAcopladoDtpEntrada;
        private System.Windows.Forms.Label lblAcopladoDiasAFavor;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label lblAcopladoSalidaAproxText;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label lblESTADOVIAJE;
        private System.Windows.Forms.Label lblViajeSalida;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label lblViajeRegreso;
    }
}