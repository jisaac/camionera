﻿namespace CapaPresentacion.Logistica
{
    partial class frmEditarViaje
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbObservaciones = new System.Windows.Forms.TextBox();
            this.btnOk = new System.Windows.Forms.Button();
            this.lblObservaciones = new System.Windows.Forms.Label();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.lblEstadoViaje = new System.Windows.Forms.Label();
            this.lblKmCamion = new System.Windows.Forms.Label();
            this.tbKm = new System.Windows.Forms.TextBox();
            this.pnlKmInput = new System.Windows.Forms.Panel();
            this.pnlKmInput.SuspendLayout();
            this.SuspendLayout();
            // 
            // tbObservaciones
            // 
            this.tbObservaciones.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbObservaciones.Location = new System.Drawing.Point(21, 109);
            this.tbObservaciones.Multiline = true;
            this.tbObservaciones.Name = "tbObservaciones";
            this.tbObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.tbObservaciones.Size = new System.Drawing.Size(674, 195);
            this.tbObservaciones.TabIndex = 0;
            // 
            // btnOk
            // 
            this.btnOk.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOk.Location = new System.Drawing.Point(620, 310);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 5;
            this.btnOk.Text = "Guardar";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblObservaciones
            // 
            this.lblObservaciones.AutoSize = true;
            this.lblObservaciones.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblObservaciones.Location = new System.Drawing.Point(18, 78);
            this.lblObservaciones.Name = "lblObservaciones";
            this.lblObservaciones.Size = new System.Drawing.Size(85, 13);
            this.lblObservaciones.TabIndex = 6;
            this.lblObservaciones.Text = "Observaciones :";
            // 
            // btnCancelar
            // 
            this.btnCancelar.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCancelar.Location = new System.Drawing.Point(525, 310);
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.Size = new System.Drawing.Size(75, 23);
            this.btnCancelar.TabIndex = 7;
            this.btnCancelar.Text = "Cancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // lblEstadoViaje
            // 
            this.lblEstadoViaje.AutoSize = true;
            this.lblEstadoViaje.Font = new System.Drawing.Font("Bahnschrift", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEstadoViaje.Location = new System.Drawing.Point(248, 9);
            this.lblEstadoViaje.Name = "lblEstadoViaje";
            this.lblEstadoViaje.Size = new System.Drawing.Size(193, 33);
            this.lblEstadoViaje.TabIndex = 9;
            this.lblEstadoViaje.Text = "CERRAR VIAJE";
            this.lblEstadoViaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblKmCamion
            // 
            this.lblKmCamion.AutoSize = true;
            this.lblKmCamion.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblKmCamion.Location = new System.Drawing.Point(6, 6);
            this.lblKmCamion.Name = "lblKmCamion";
            this.lblKmCamion.Size = new System.Drawing.Size(151, 13);
            this.lblKmCamion.TabIndex = 10;
            this.lblKmCamion.Text = "Kilometraje actual del camión";
            // 
            // tbKm
            // 
            this.tbKm.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbKm.Location = new System.Drawing.Point(224, 5);
            this.tbKm.MaxLength = 6;
            this.tbKm.Name = "tbKm";
            this.tbKm.ShortcutsEnabled = false;
            this.tbKm.Size = new System.Drawing.Size(170, 21);
            this.tbKm.TabIndex = 11;
            this.tbKm.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.tbKm.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tbKm_KeyPress);
            // 
            // pnlKmInput
            // 
            this.pnlKmInput.Controls.Add(this.lblKmCamion);
            this.pnlKmInput.Controls.Add(this.tbKm);
            this.pnlKmInput.Font = new System.Drawing.Font("Bahnschrift", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pnlKmInput.Location = new System.Drawing.Point(301, 72);
            this.pnlKmInput.Name = "pnlKmInput";
            this.pnlKmInput.Size = new System.Drawing.Size(407, 28);
            this.pnlKmInput.TabIndex = 12;
            // 
            // frmEditarViaje
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(720, 342);
            this.ControlBox = false;
            this.Controls.Add(this.pnlKmInput);
            this.Controls.Add(this.lblEstadoViaje);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.lblObservaciones);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.tbObservaciones);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmEditarViaje";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Edita Viaje";
            this.pnlKmInput.ResumeLayout(false);
            this.pnlKmInput.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbObservaciones;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Label lblObservaciones;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Label lblEstadoViaje;
        private System.Windows.Forms.Label lblKmCamion;
        private System.Windows.Forms.TextBox tbKm;
        private System.Windows.Forms.Panel pnlKmInput;
    }
}