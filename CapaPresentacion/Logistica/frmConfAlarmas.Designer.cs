﻿namespace CapaPresentacion.Logistica
{
    partial class frmConfAlarmas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConfAlarmas));
            this.lblVer = new System.Windows.Forms.Label();
            this.cbVer = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.nudSalir = new System.Windows.Forms.NumericUpDown();
            this.nudLLegada = new System.Windows.Forms.NumericUpDown();
            this.btnCancelar = new System.Windows.Forms.Button();
            this.btnOk = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.nudSalir)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLLegada)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblVer
            // 
            resources.ApplyResources(this.lblVer, "lblVer");
            this.lblVer.Name = "lblVer";
            // 
            // cbVer
            // 
            resources.ApplyResources(this.cbVer, "cbVer");
            this.cbVer.Name = "cbVer";
            this.cbVer.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // nudSalir
            // 
            resources.ApplyResources(this.nudSalir, "nudSalir");
            this.nudSalir.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudSalir.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudSalir.Name = "nudSalir";
            this.nudSalir.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // nudLLegada
            // 
            resources.ApplyResources(this.nudLLegada, "nudLLegada");
            this.nudLLegada.Maximum = new decimal(new int[] {
            30,
            0,
            0,
            0});
            this.nudLLegada.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLLegada.Name = "nudLLegada";
            this.nudLLegada.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // btnCancelar
            // 
            resources.ApplyResources(this.btnCancelar, "btnCancelar");
            this.btnCancelar.Name = "btnCancelar";
            this.btnCancelar.UseVisualStyleBackColor = true;
            this.btnCancelar.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.nudSalir);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.nudLLegada);
            resources.ApplyResources(this.groupBox1, "groupBox1");
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.TabStop = false;
            // 
            // frmConfAlarmas
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.btnCancelar);
            this.Controls.Add(this.cbVer);
            this.Controls.Add(this.lblVer);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConfAlarmas";
            ((System.ComponentModel.ISupportInitialize)(this.nudSalir)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nudLLegada)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblVer;
        private System.Windows.Forms.CheckBox cbVer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown nudSalir;
        private System.Windows.Forms.NumericUpDown nudLLegada;
        private System.Windows.Forms.Button btnCancelar;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}