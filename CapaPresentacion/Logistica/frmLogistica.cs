﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion;
using GestionTransporte;
using CapaPresentacion.FormsMsj;
using CapaPresentacion.Operaciones;

//Una vez que se cambia la unidad, es obligatorio que el usuario ponga el km con el que termino la unidad retirada del viaje, por eso ese form, no tiene opcion de cancelar o cerrar
namespace CapaPresentacion.Logistica
{
    public partial class frmLogistica : Form
    {

        public frmLogistica(string usuario)
        {
            InitializeComponent();
            this.usuario = usuario;
            n_viaje = new N_Viaje(usuario);
            e_UnidadesViaje = new E_UnidadesViaje();
            e_viaje = new E_Viaje();
            dtViaje = new DataTable();
            funciones = new MisFunciones();
            n_Taller = new N_Taller(usuario);
            n_Camion = new N_Camion(usuario);
            n_acoplado = new N_Acoplado(usuario);
            n_Pedido = new N_Pedido(usuario);
            this.n_camion = new N_Camion(usuario);
            n_dominio = new N_Dominio(usuario);
            cbFiltrarViaje2 = new string[] { "Dominio, Cliente, Chofer, ID Viaje", "Fecha de salida (Pedido)", "Fecha de Regreso (Pedido)", "Fecha de salida (Viaje)", "Fecha de Regreso (Viaje)" };
            cbViajeFiltrar2.DataSource = cbFiltrarViaje2;

        }
        N_Viaje n_viaje;
        String usuario;
        DataTable dtViaje;
        MisFunciones funciones;
        string[] cbFiltrarViaje2;// esta variable sirve para llenar el segundo combobox de filtro de viaje
        Estado estado;
        E_UnidadesViaje e_UnidadesViaje;
        E_Viaje e_viaje;
        DataTable dtCamion;
        DataTable dtRemolque;
        Boolean selecciona_camion = false;
        Boolean selecciona_acoplado = false;
        Boolean cambiar_unidad= false;
        N_Pedido n_Pedido;
        E_Pedido e_pedido;
        N_Taller n_Taller;
        N_Camion n_Camion;
        N_Acoplado n_acoplado;
        N_Dominio n_dominio;
        int timerCount = 299;//para controlar el tiempo que debe pasar para consultar si hay viajes por salir o por cerrar
        int dateSalidaConf = 1;
        int dateRegresoConf = 1;
        int viajesPorSalir = 0;
        int viajesPorCerrar = 0;
        EstadoTaller estadoTallerCamion;
        EstadoTaller estadoTallerAcoplado;
        DataTable dtTallerCamion;
        DataTable dtTallerAcoplado;
        List<string> respuestas;
        frmMensajes frmmensaje;
        N_Camion n_camion;
        private void frmLogistica_Load(object sender, EventArgs e)
         {
        
            #region viajes
            dtViaje = n_viaje.listarEstado(Estado.Todos);
            dgvViaje.DataSource = dtViaje;
            this.escondeBotonesEstadosViaje();
            carga_datos_label_viaje();
            cbViajeFiltrar.SelectedIndex = 0;
            pnlCambiarDeUnidad.Visible = true;//El panel cambiar de unidad en la seccion admin de unidades se oculta
            #endregion
            pnlViajeAlarmas.Visible = true;
            timer.Start();
            #region mantenimiento
            funciones.esconderFiltroBusquedaFechaTallerCamion(true, pnlCamionBusquedaFecha, tbBusquedaCamion);//escondemos dtp Camion busqueda
            funciones.esconderFiltroBusquedaFechaTallerAcoplado(true, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);//escondemos dtp Camion busqueda

            cbFiltroCamion.SelectedIndex = 0;
            cbFiltroAcoplado.SelectedIndex = 0;
            
            #endregion
        }
            void carga_cbfitrarviaje2_completo(bool var)//segun el filtro esta funcion carga el combobox2 de viaje,ejemplo: si se van a filtrar los suspendidos o los pendientes, estos no deberian dejar cargar fecha de llegadda de viaje
        {//parametro en trrue carga completo el cb, en false carga algunos items
            cbViajeFiltrar2.DataSource = null;
            cbViajeFiltrar2.Items.Clear();
            if (var)
            {
                cbViajeFiltrar2.DataSource = cbFiltrarViaje2;
            }
            else
            {
                for (int i = 0; i < 3; i++)
                {
                    cbViajeFiltrar2.Items.Add(cbFiltrarViaje2[i]);
                }

            }
            cbViajeFiltrar2.SelectedIndex = 0;
        }
        #region Unidades
        private void btnSeleccionPedido_Click(object sender, EventArgs e)
        {
            dgvCamion.DataSource = null;
            dgvAcoplado.DataSource = null;
            limpiaBindingCamion();
            limpiaTextboxBindingCamion();
            limpiaBindingRemolque();
            limpiaTextboxBindingRemolque();
            frmPedido frmPedido = new frmPedido(usuario, null);
            frmPedido.enviaCamiones += FrmPedido_enviaCamiones;
            frmPedido.enviaAcoplados += FrmPedido_enviaAcoplados;
            frmPedido.ShowDialog();
            #region visible?boton
            if (dgvCamion.RowCount > 0 && dgvAcoplado.RowCount > 0)
            {
                btnCrearViaje.Enabled = true;
                btnSeleccionarCamion.Enabled = true;
                btnSeleccionarRemolque.Enabled = true;
                btnCancelarSeleccionCamion.Enabled = true;
                btnCancelarSeleccionRemolque.Enabled = true;
            }
            else if(dgvCamion.RowCount > 0)
            {
                btnCrearViaje.Enabled = true;
                btnSeleccionarCamion.Enabled = true;
                btnCancelarSeleccionCamion.Enabled = true;
            }
            else if (dgvAcoplado.RowCount > 0 )
            {
                btnCrearViaje.Enabled = true;
                btnSeleccionarRemolque.Enabled = true;
                btnCancelarSeleccionRemolque.Enabled = true;
            }
            else
            {
                btnCrearViaje.Enabled = false;
                btnSeleccionarCamion.Enabled = false;
                btnSeleccionarRemolque.Enabled = false;
                btnCancelarSeleccionCamion.Enabled = false;
                btnCancelarSeleccionRemolque.Enabled = false;
            }
            #endregion
        }

        #region Remolque
        private void FrmPedido_enviaAcoplados(DataTable dtRemolque, E_Pedido e_pedido)
        {
            this.dtRemolque = dtRemolque;
            dgvAcoplado.DataSource = dtRemolque;
            carga_sugerencias_remolque_total();
            this.e_pedido = e_pedido;
        }
        private void carga_sugerencias_remolque_total()
        {
            verificaDGVRemolques();
            columnas_invisibles_remolque();
            carga_datos_label_remolque();
        }
        private void verificaDGVRemolques()
        {
            if (dgvAcoplado.Rows.Count < 1)
            {
                btnSeleccionarRemolque.Enabled = false;

                dgvAcoplado.BackgroundColor = Color.LightGray;
                limpiaTextboxBindingRemolque();
            }
            else
            {
                btnSeleccionarRemolque.Enabled = true;
                dgvAcoplado.BackgroundColor = Color.Gray;
            }

        }
        private void columnas_invisibles_remolque()
        {
            dgvAcoplado.Columns[0].Visible = false;
            dgvAcoplado.Columns[6].Visible = false;
            dgvAcoplado.Columns[7].Visible = false;
            dgvAcoplado.Columns[8].Visible = false;
            dgvAcoplado.Columns[9].Visible = false;
            dgvAcoplado.Columns[10].Visible = false;
            dgvAcoplado.Columns[11].Visible = false;
            dgvAcoplado.Columns[12].Visible = false;
            dgvAcoplado.Columns[13].Visible = false;
            dgvAcoplado.Columns[14].Visible = false;
            dgvAcoplado.Columns[15].Visible = false;
            dgvAcoplado.Columns[16].Visible = false;
            dgvAcoplado.Columns[17].Visible = false;
            dgvAcoplado.Columns[18].Visible = false;
            dgvAcoplado.Columns[20].Visible = false;
        }
        private void carga_datos_label_remolque()
        {
            limpiaBindingRemolque();
            this.controlaBindingCountRemolque();
            if (dtRemolque != null)
            {
                lbTaraRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Tara"));
                lbCapCargaRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Capacidad carga"));
                lbVolumenRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Volumen"));
                lblKmUnidadRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "KM Unidad"));
                lbKmNeumRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "KM Cambio Neumaticos"));
                lbAlturaRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Altura"));
                lbAnchoIntRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Ancho interior"));
                lbAnchoExtRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Ancho exterior"));
                lbLongitudRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Longitud"));
                lbTipoUnidad.DataBindings.Add(new Binding("Text", dtRemolque, "Tipo Acoplado"));
                if (dgvAcoplado.CurrentRow == null)
                {
                    if (dgvAcoplado.Rows[0].Cells[20].Value.ToString() == "HABILITADO")
                        lbEstadoRemolque.BackColor = Color.Green;
                    else
                        lbEstadoRemolque.BackColor = Color.Red;
                }
                else
                {
                    if (dgvAcoplado.CurrentRow.Cells[20].Value.ToString() == "HABILITADO")
                        lbEstadoRemolque.BackColor = Color.Green;
                    else
                        lbEstadoRemolque.BackColor = Color.Red;
                }
                //lbEstadoRemolque.DataBindings.Add(new Binding("Text", dtRemolque, "Estado"));
            }

        }
        private void controlaBindingCountRemolque()
        {
            if (lbTaraRemolque.DataBindings.Count == 0)
                lbTaraRemolque.Text = "-----";
            if (lbCapCargaRemolque.DataBindings.Count == 0)
                lbCapCargaRemolque.Text = "-----";
            if (lbVolumenRemolque.DataBindings.Count == 0)
                lbVolumenRemolque.Text = "-----";
            if (lblKmUnidadRemolque.DataBindings.Count == 0)
                lblKmUnidadRemolque.Text = "-----";
            if (lbKmNeumRemolque.DataBindings.Count == 0)
                lbKmNeumRemolque.Text = "-----";
            if (lbAlturaRemolque.DataBindings.Count == 0)
                lbAlturaRemolque.Text = "-----";
            if (lbAnchoIntRemolque.DataBindings.Count == 0)
                lbAnchoIntRemolque.Text = "-----";
            if (lbAnchoExtRemolque.DataBindings.Count == 0)
                lbAnchoExtRemolque.Text = "-----";
            if (lbLongitudRemolque.DataBindings.Count == 0)
                lbLongitudRemolque.Text = "-----";
            if (lbTipoUnidad.DataBindings.Count == 0)
                lbTipoUnidad.Text = "-----";
        }
        private void limpiaBindingRemolque()
        {
            lbTaraRemolque.DataBindings.Clear();
            lbCapCargaRemolque.DataBindings.Clear();
            lbVolumenRemolque.DataBindings.Clear();
            lblKmUnidadRemolque.DataBindings.Clear();
            lbKmNeumRemolque.DataBindings.Clear();
            lbAlturaRemolque.DataBindings.Clear();
            lbAnchoIntRemolque.DataBindings.Clear();
            lbAnchoExtRemolque.DataBindings.Clear();
            lbLongitudRemolque.DataBindings.Clear();
            lbTipoUnidad.DataBindings.Clear();
            lbEstadoRemolque.BackColor = Color.Transparent;
        }
        private void limpiaTextboxBindingRemolque()
        {
            lbTaraRemolque.Text = "-----";
            lbCapCargaRemolque.Text = "-----";
            lbVolumenRemolque.Text = "-----";
            lblKmUnidadRemolque.Text = "-----";
            lbKmNeumRemolque.Text = "-----";
            lbAlturaRemolque.Text = "-----";
            lbAnchoIntRemolque.Text = "-----";
            lbAnchoExtRemolque.Text = "-----";
            lbLongitudRemolque.Text = "-----";
            lbTipoUnidad.Text = "-----";
            lbEstadoRemolque.BackColor = Color.Transparent;
        }
        #endregion

        #region Camion
        private void FrmPedido_enviaCamiones(DataTable dtCamion, E_Pedido e_pedido)
        {
            this.dtCamion = dtCamion;
            dgvCamion.DataSource = dtCamion;
            carga_sugerencias_camion_total();
            this.e_pedido = e_pedido;
        }
        private void carga_sugerencias_camion_total()
        {
            verificaDGVCamiones();
            columnas_invisibles_camion();
            carga_datos_label_camion();
        }
        private void verificaDGVCamiones()
        {
            if (dgvCamion.Rows.Count < 1)
            {
                btnSeleccionarCamion.Enabled = false;

                dgvCamion.BackgroundColor = Color.LightGray;
                limpiaTextboxBindingCamion();
            }
            else
            {
                btnSeleccionarCamion.Enabled = true;
                dgvCamion.BackgroundColor = Color.DarkGray;

            }

        }
        private void columnas_invisibles_camion()
        {
            if (dgvCamion.ColumnCount > 14)
            {
                dgvCamion.Columns[0].Visible = false;
                dgvCamion.Columns[5].Visible = false;
                dgvCamion.Columns[6].Visible = false;
                dgvCamion.Columns[7].Visible = false;
                dgvCamion.Columns[8].Visible = false;
                dgvCamion.Columns[9].Visible = false;
                dgvCamion.Columns[10].Visible = false;
                dgvCamion.Columns[12].Visible = false;
                dgvCamion.Columns[13].Visible = false;
                dgvCamion.Columns[14].Visible = false;
                dgvCamion.Columns[15].Visible = false;
                dgvCamion.Columns[16].Visible = false;
                dgvCamion.Columns[17].Visible = false;
                dgvCamion.Columns[19].Visible = false;
                dgvCamion.Columns[20].Visible = false;
            }
            else
            {
                dgvCamion.Columns[0].Visible = false;
                dgvCamion.Columns[5].Visible = false;
                dgvCamion.Columns[6].Visible = false;
                dgvCamion.Columns[7].Visible = false;
                dgvCamion.Columns[8].Visible = false;
                dgvCamion.Columns[9].Visible = false;
                dgvCamion.Columns[10].Visible = false;
                dgvCamion.Columns[12].Visible = false;
                dgvCamion.Columns[13].Visible = false;
            }

        }
        private void carga_datos_label_camion()
        {
            limpiaBindingCamion();
            this.controlaBindingCountCamion();
            if (dtCamion != null && dtCamion.Columns.Count > 14)
            {
                lbTaraCamion.DataBindings.Add(new Binding("Text", dtCamion, "Tara"));
                lblKmUnidadCamion.DataBindings.Add(new Binding("Text", dtCamion, "KM Unidad"));
                lbKmNeumCamion.DataBindings.Add(new Binding("Text", dtCamion, "KM Cambio Neumaticos"));
                if (dgvCamion.CurrentRow == null)
                {
                    if (dgvCamion.Rows[0].Cells[19].Value.ToString() == "HABILITADO")
                        lbEstadoCamion.BackColor = Color.Green;
                    else
                        lbEstadoCamion.BackColor = Color.Red;
                }
                else
                {
                    if (dgvCamion.CurrentRow.Cells[19].Value.ToString() == "HABILITADO")
                        lbEstadoCamion.BackColor = Color.Green;
                    else
                        lbEstadoCamion.BackColor = Color.Red;
                }
                //lbEstadoCamion.DataBindings.Add(new Binding("Text", dtCamion, "Estado"));
                lbAlturaCamion.DataBindings.Add(new Binding("Text", dtCamion, "Altura"));
                lbAnchoIntCamion.DataBindings.Add(new Binding("Text", dtCamion, "Ancho interior"));
                lbAnchoExtCamion.DataBindings.Add(new Binding("Text", dtCamion, "Ancho exterior"));
                lbLongitudCamion.DataBindings.Add(new Binding("Text", dtCamion, "Longitud"));
                lbVolumenCamion.DataBindings.Add(new Binding("Text", dtCamion, "Volumen"));
                lbCapCargaCamion.DataBindings.Add(new Binding("Text", dtCamion, "Capacidad carga"));
                lblEngancheCamion.DataBindings.Add(new Binding("Text", dtCamion, "Enganche"));
            }
            else if (dgvCamion.Rows.Count > 0)
            {
                lbTaraCamion.DataBindings.Add(new Binding("Text", dtCamion, "Tara"));
                lblKmUnidadCamion.DataBindings.Add(new Binding("Text", dtCamion, "KM Unidad"));
                lbKmNeumCamion.DataBindings.Add(new Binding("Text", dtCamion, "KM Cambio Neumaticos"));
                if (dgvCamion.CurrentRow == null)
                {
                    if (dgvCamion.Rows[0].Cells[12].Value.ToString() == "HABILITADO")
                        lbEstadoCamion.BackColor = Color.Green;
                    else
                        lbEstadoCamion.BackColor = Color.Red;
                }
                else
                {
                    if (dgvCamion.CurrentRow.Cells[12].Value.ToString() == "HABILITADO")
                        lbEstadoCamion.BackColor = Color.Green;
                    else
                        lbEstadoCamion.BackColor = Color.Red;
                }
                //lbEstadoCamion.DataBindings.Add(new Binding("Text", dtCamion, "Estado"));
            }

        }
        private void controlaBindingCountCamion()
        {
            if (dtCamion != null && dtCamion.Columns.Count > 14)
            {
                if (lbTaraCamion.DataBindings.Count == 0)
                    lbTaraCamion.Text = "-----";
                if (lblKmUnidadCamion.DataBindings.Count == 0)
                    lblKmUnidadCamion.Text = "-----";
                if (lbKmNeumCamion.DataBindings.Count == 0)
                    lbKmNeumCamion.Text = "-----";
                if (lbAlturaCamion.DataBindings.Count == 0)
                    lbAlturaCamion.Text = "-----";
                if (lbAnchoIntCamion.DataBindings.Count == 0)
                    lbAnchoIntCamion.Text = "-----";
                if (lbAnchoExtCamion.DataBindings.Count == 0)
                    lbAnchoExtCamion.Text = "-----";
                if (lbLongitudCamion.DataBindings.Count == 0)
                    lbLongitudCamion.Text = "-----";
                if (lbVolumenCamion.DataBindings.Count == 0)
                    lbVolumenCamion.Text = "-----";
                if (lbCapCargaCamion.DataBindings.Count == 0)
                    lbCapCargaCamion.Text = "-----";
                if (lblEngancheCamion.DataBindings.Count == 0)
                    lblEngancheCamion.Text = "-----";
            }
            else
            {
                if (lbTaraCamion.DataBindings.Count == 0)
                    lbTaraCamion.Text = "-----";
                if (lblKmUnidadCamion.DataBindings.Count == 0)
                    lblKmUnidadCamion.Text = "-----";
                if (lbKmNeumCamion.DataBindings.Count == 0)
                    lbKmNeumCamion.Text = "-----";
                if (lbAlturaCamion.DataBindings.Count == 0)
                    lbAlturaCamion.Text = "N/A";
                if (lbAnchoIntCamion.DataBindings.Count == 0)
                    lbAnchoIntCamion.Text = "N/A";
                if (lbAnchoExtCamion.DataBindings.Count == 0)
                    lbAnchoExtCamion.Text = "N/A";
                if (lbLongitudCamion.DataBindings.Count == 0)
                    lbLongitudCamion.Text = "N/A";
                if (lbVolumenCamion.DataBindings.Count == 0)
                    lbVolumenCamion.Text = "N/A";
                if (lbCapCargaCamion.DataBindings.Count == 0)
                    lbCapCargaCamion.Text = "N/A";
                if (lblEngancheCamion.DataBindings.Count == 0)
                    lblEngancheCamion.Text = "N/A";
            }

        }
        private void limpiaBindingCamion()
        {
            lbTaraCamion.DataBindings.Clear();
            lblKmUnidadCamion.DataBindings.Clear();
            lbKmNeumCamion.DataBindings.Clear();
            lbEstadoCamion.BackColor = Color.Transparent;
            lbAlturaCamion.DataBindings.Clear();
            lbAnchoIntCamion.DataBindings.Clear();
            lbAnchoExtCamion.DataBindings.Clear();
            lbLongitudCamion.DataBindings.Clear();
            lbVolumenCamion.DataBindings.Clear();
            lbCapCargaCamion.DataBindings.Clear();
            lblEngancheCamion.DataBindings.Clear();
        }
        private void limpiaTextboxBindingCamion()
        {
            lbTaraCamion.Text = "-----";
            lblKmUnidadCamion.Text = "-----";
            lbKmNeumCamion.Text = "-----";
            lbEstadoCamion.BackColor = Color.Transparent;
            lbAlturaCamion.Text = "-----";
            lbAnchoIntCamion.Text = "-----";
            lbAnchoExtCamion.Text = "-----";
            lbLongitudCamion.Text = "-----";
            lbVolumenCamion.Text = "-----";
            lbCapCargaCamion.Text = "-----";
            lblEngancheCamion.Text = "-----";

        }
        #endregion

        #endregion

        #region viajes
        void carga_datos_label_viaje()
        {
            limpiaBindingViaje();
            //controlaBindingCountCamion();
            columnas_viaje_invisible();
            if (dtViaje != null)
            {

                //cliente
                lblViajeClienteCuit.DataBindings.Add(new Binding("Text", dtViaje, "CUIL CLIENTE"));
                lblViajeClienteNombre.DataBindings.Add(new Binding("Text", dtViaje, "CLIENTE"));
                //unidades
                lblViajeCamionDominio.DataBindings.Add(new Binding("Text", dtViaje, "DOMINIO CAMION"));
                lblViajeCamionMarca.DataBindings.Add(new Binding("Text", dtViaje, "MARCA CAMION"));
                lblViajeCamionModelo.DataBindings.Add(new Binding("Text", dtViaje, "MODELO CAMION"));
                lblViajeCamionTipo.DataBindings.Add(new Binding("Text", dtViaje, "TIPO CAMION"));
                lblViajeAcoplado1Dominio.DataBindings.Add(new Binding("Text", dtViaje, "DOM ACOPLADO"));
                lblViajeAcoplado1Marca.DataBindings.Add(new Binding("Text", dtViaje, "MARCA ACOPLADO"));
                lblViajeAcoplado1Modelo.DataBindings.Add(new Binding("Text", dtViaje, "MOD ACOPLADO"));
                //chofer
                lblViajeChoferLegajo.DataBindings.Add(new Binding("Text", dtViaje, "LEGAJO"));
                lblViajeChoferNombre.DataBindings.Add(new Binding("Text", dtViaje, "NOMBRE Y APELLIDO"));
                lblViajeChoferDireccion.DataBindings.Add(new Binding("Text", dtViaje, "DIRECCION"));
                lblViajeChoferTelPersonal.DataBindings.Add(new Binding("Text", dtViaje, "TELEFONO PERSONAL"));
                lblViajeChoferTelTrabajo.DataBindings.Add(new Binding("Text", dtViaje, "TELEFONO TRABAJO"));
                //observaciones
                tbViajeObservaciones.DataBindings.Add(new Binding("Text", dtViaje, "OBSERVACIONES VIAJE"));
                tbViajePedidoObservaciones.DataBindings.Add(new Binding("Text", dtViaje, "OBSERVACIONES PEDIDO"));
            }
        }
        void columnas_viaje_invisible()
        {
            if (dgvViaje.Rows.Count >= 0)
            {    //-en grilla
                 //''en lbl
                 //dgvViaje.Columns[0].Visible = false;// id_viaje---------------
                dgvViaje.Columns[2].Visible = false;//Salida
                dgvViaje.Columns[3].Visible = false;//Regreso
                dgvViaje.RowHeadersVisible = false;
                //dgvViaje.Columns[5].Visible = false;//'km con los que termina el viaje
                dgvViaje.Columns[6].Visible = false;//observaciones
                dgvViaje.Columns[7].Visible = false;//Estado
                dgvViaje.Columns[8].Visible = false;//nombre de cliente---------
                dgvViaje.Columns[9].Visible = false;//cuit de cliente----------
                dgvViaje.Columns[10].Visible = false; //fecha de pedido---------
                //dgvViaje.Columns[11].Visible = false; //peso neto 
                //dgvViaje.Columns[12].Visible = false; //volumen
                //dgvViaje.Columns[13].Visible = false; //km viaje pedido
                dgvViaje.Columns[14].Visible = false; //observaciones pedido''''''
                dgvViaje.Columns[15].Visible = false; //dominio camion ''''''
                dgvViaje.Columns[16].Visible = false; //marca camion''''''
                dgvViaje.Columns[17].Visible = false; //modelo camion''''
                dgvViaje.Columns[18].Visible = false; //tipo de camion''''
                dgvViaje.Columns[19].Visible = false; //dominio acoplado1''''
                dgvViaje.Columns[20].Visible = false; //marca acoplado1''''
                dgvViaje.Columns[21].Visible = false; //modelo acoplado1''''
                dgvViaje.Columns[22].Visible = false; //chofer legajo''''
                dgvViaje.Columns[23].Visible = false; //chofer nombre Y APELLIDO''''
                dgvViaje.Columns[24].Visible = false; //chofer direccion ''''
                dgvViaje.Columns[25].Visible = false; //chofer telefono personal ''''
                dgvViaje.Columns[26].Visible = false; //chofer telefono de trabajo ''''''
                dgvViaje.Columns[27].Visible = false; //id pedido''''''

                dgvViaje.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[4].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[5].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[11].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
                dgvViaje.Columns[13].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;

            }
        }//usada por carga_datos_label_viaje
        private void limpiaBindingViaje()////usada por carga_datos_label_viaje
        {
            //cliente
            lblViajeClienteCuit.DataBindings.Clear();
            lblViajeClienteNombre.DataBindings.Clear();
            //unidades
            lblViajeCamionDominio.DataBindings.Clear();
            lblViajeCamionMarca.DataBindings.Clear();
            lblViajeCamionModelo.DataBindings.Clear();
            lblViajeCamionTipo.DataBindings.Clear();
            lblViajeAcoplado1Dominio.DataBindings.Clear();
            lblViajeAcoplado1Marca.DataBindings.Clear();
            lblViajeAcoplado1Modelo.DataBindings.Clear();
            //chofer
            lblViajeChoferLegajo.DataBindings.Clear();
            lblViajeChoferNombre.DataBindings.Clear();
            lblViajeChoferDireccion.DataBindings.Clear();
            lblViajeChoferTelPersonal.DataBindings.Clear();
            lblViajeChoferTelTrabajo.DataBindings.Clear();
            //observaciones
            tbViajeObservaciones.DataBindings.Clear();
            tbViajePedidoObservaciones.DataBindings.Clear();
        }//


        #endregion viajes


        //COMBOBOX LISTAR
        private void cbViajeFiltrar_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {

                //Limpio tb de busqueda y  reseteo los dtp despues de cada cambion en el combobox
                //recordar el darle un value a un dtp dispara este evento nuevamente

                switch (cbViajeFiltrar.SelectedIndex)
                {
                    case 0:
                        estado = Estado.Todos;
                        dtViaje = n_viaje.listarEstado(estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        //dtpbViajeBusquedaDesde.Value = DateTime.Now;
                        //dtpViajeBusquedaHasta.Value = DateTime.Now;
                        tbViajeBusquedaTexto.Clear();
                        carga_cbfitrarviaje2_completo(true);
                        break;

                    case 1:
                        estado = Estado.Pendiente;
                        dtViaje = n_viaje.listarEstado(estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        //dtpbViajeBusquedaDesde.Value = DateTime.Now;
                        //dtpViajeBusquedaHasta.Value = DateTime.Now;
                        tbViajeBusquedaTexto.Clear();
                        carga_cbfitrarviaje2_completo(true);
                        break;
                    case 2:
                        estado = Estado.EnCurso;
                        dtViaje = n_viaje.listarEstado(estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        //dtpbViajeBusquedaDesde.Value = DateTime.Now;
                        //dtpViajeBusquedaHasta.Value = DateTime.Now;
                        tbViajeBusquedaTexto.Clear();
                        carga_cbfitrarviaje2_completo(true);
                        break;
                    case 3:
                        estado = Estado.Concluido;
                        dtViaje = n_viaje.listarEstado(estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        //dtpbViajeBusquedaDesde.Value = DateTime.Now;
                        //dtpViajeBusquedaHasta.Value = DateTime.Now;
                        tbViajeBusquedaTexto.Clear();
                        carga_cbfitrarviaje2_completo(true);
                        break;
                    case 4:
                        estado = Estado.Suspendido;
                        dtViaje = n_viaje.listarEstado(estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        //dtpbViajeBusquedaDesde.Value = DateTime.Now;
                        //dtpViajeBusquedaHasta.Value = DateTime.Now;
                        tbViajeBusquedaTexto.Clear();
                        carga_cbfitrarviaje2_completo(false);
                        break;
                    case 5:
                        estado = Estado.Cancelado;
                        dtViaje = n_viaje.listarEstado(estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        //dtpbViajeBusquedaDesde.Value = DateTime.Now;
                        //dtpViajeBusquedaHasta.Value = DateTime.Now;
                        tbViajeBusquedaTexto.Clear();
                        carga_cbfitrarviaje2_completo(false);
                        break;

                }
                //voy a cambiar en los listar...todos pendiente concluido suespendido cancelado...por dominio unidad , nombre de cliente, nombre de chofer (para estas uso el campo texto) 
                //y las fechas /...dos cb box..uno para los estados de los viajes a listar, y otro con el tipo de busqueda(nombre cliente,fecha.)
                //recordar tocar las funciones de d_viaje para que busque viaje segun el tipo de busqueda(texto o fecha) y el estado, para esto vamos a pasar por parametro los que se tenga q buscar (sea texto o fechas)
                // y el estado de  lo que queremos buscar busqueda( desde , hasta,concluido).saludos
                //Todos
                //Pendiente
                //En curso
                //Concluido
                //Suspendido
                //Cancelado
                //Fecha de salida(Pedido)
                //Fecha de Regreso(Pedido)
                //Fecha de salida(Viaje)
                //Fecha de Regreso(Viaje)
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Error al cargar datos").ShowDialog();
            }
        }

        private void dtpbViajeBusquedaDesde_ValueChanged(object sender, EventArgs e)
        {

            if (dtpbViajeBusquedaDesde.Value < dtpViajeBusquedaHasta.Value.AddSeconds(60))
            {

                switch (cbViajeFiltrar2.SelectedIndex)
                {
                    case 1:
                        dtViaje = n_viaje.listar_Fecha_salida_pedido(dtpbViajeBusquedaDesde.Value.ToShortDateString(), dtpViajeBusquedaHasta.Value.ToShortDateString(), estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        break;
                    case 2:
                        dtViaje = n_viaje.listar_Fecha_Regreso_pedido(dtpbViajeBusquedaDesde.Value.ToShortDateString(), dtpViajeBusquedaHasta.Value.ToShortDateString(), estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        break;
                    case 3:
                        dtViaje = n_viaje.listar_Fecha_salida_viaje(dtpbViajeBusquedaDesde.Value.ToShortDateString(), dtpViajeBusquedaHasta.Value.ToShortDateString(), estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        break;
                    case 4:
                        dtViaje = n_viaje.listar_Fecha_Regreso_viaje(dtpbViajeBusquedaDesde.Value.ToShortDateString(), dtpViajeBusquedaHasta.Value.ToShortDateString(), estado);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        break;
                }
                //Todos
                //Pendiente
                //En curso
                //Concluido
                //Suspendido
                //Cancelado
            }
            else
            {
                dtpbViajeBusquedaDesde.Refresh();
                dtpViajeBusquedaHasta.Refresh();
                new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
                dtpViajeBusquedaHasta.Focus();
            }

        }

        private void cbViajeFiltrar2_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            
            switch (cbViajeFiltrar2.SelectedIndex)
            {
                case 0:
                    funciones.esconderFiltroBusquedaFechaViaje(true, pnlViajeBusquedaFecha, pnlViajeBusquedaTexto);
                    break;
                case 1:
                    funciones.esconderFiltroBusquedaFechaViaje(false, pnlViajeBusquedaFecha, pnlViajeBusquedaTexto);
                    break;
                case 2:
                    funciones.esconderFiltroBusquedaFechaViaje(false, pnlViajeBusquedaFecha, pnlViajeBusquedaTexto);
                    break;
                case 3:
                    funciones.esconderFiltroBusquedaFechaViaje(false, pnlViajeBusquedaFecha, pnlViajeBusquedaTexto);
                    break;
                case 4:
                    funciones.esconderFiltroBusquedaFechaViaje(false, pnlViajeBusquedaFecha, pnlViajeBusquedaTexto);
                    break;

            }
        }

        private void tbViajeBusquedaTexto_TextChanged(object sender, EventArgs e)
        {
            //Todos
            //Pendiente
            //En curso
            //Concluido
            //Suspendido
            //Cancelado
            if (tbViajeBusquedaTexto.Text.Length > 2)
                dtViaje = n_viaje.listar_porEntradaDeTexto(tbViajeBusquedaTexto.Text.Trim(), estado);
            else if (tbViajeBusquedaTexto.Text.Length <= 2)
            {
                dtViaje = n_viaje.listarEstado(Estado.Todos);
                cbViajeFiltrar.SelectedIndex = 0;
            }
            else if (cbViajeFiltrar.SelectedIndex == 0)
                dtViaje = n_viaje.listarEstado(Estado.Todos);
            else if (cbViajeFiltrar.SelectedIndex == 1)
                dtViaje = n_viaje.listarEstado(Estado.Pendiente);
            else if (cbViajeFiltrar.SelectedIndex == 2)
                dtViaje = n_viaje.listarEstado(Estado.EnCurso);
            else if (cbViajeFiltrar.SelectedIndex == 3)
                dtViaje = n_viaje.listarEstado(Estado.Concluido);
            else if (cbViajeFiltrar.SelectedIndex == 4)
                dtViaje = n_viaje.listarEstado(Estado.Suspendido);
            else if (cbViajeFiltrar.SelectedIndex == 5)
                dtViaje = n_viaje.listarEstado(Estado.Cancelado);
            dgvViaje.DataSource = dtViaje;
            carga_datos_label_viaje();

        }


        #region cambiar unidad de viaje

        private void btnViajeCambiarUnidad_Click(object sender, EventArgs e)//el panel de seleccionar pedido se esconde y aparece el de cambiar unidad
        {
            try
            {
                if (dgvViaje.CurrentRow.Cells[0].Value.ToString() != null)
                {
                    cambiar_unidad = true;
                    e_viaje = n_viaje.retornaViaje(dgvViaje.CurrentRow.Cells[0].Value.ToString());
                    e_pedido = n_Pedido.retornaPedido(e_viaje.Id_pedido);
                    e_UnidadesViaje = n_viaje.retornaUnidadesViaje(e_viaje);
                    frmInputDias frmInputDias = new frmInputDias(dgvViaje.CurrentRow.Cells[27].Value.ToString(), e_UnidadesViaje, usuario);
                    frmInputDias.enviaCamiones += FrmPedido_enviaCamiones;
                    frmInputDias.enviaAcoplados += FrmPedido_enviaAcoplados;
                    frmInputDias.enviaCancelacion += () =>
                    {
                        btnViajeCancelarCambioUnidad_Click(null, null);
                    };
                    gbLogistica.SelectedTab = gbLogistica.TabPages[1];
                    btnSeleccionPedido.Visible = false;
                    pnlCambiarDeUnidad.Visible = true;
                    lblCambiarUnidadesViaje.Visible = true;
                    btnViajeCancelarCambioUnidad.Visible = true;
                    btnViajeOkCambioDeUnidad.Visible = true;
                    lblCambiarUnidadesViaje.Text = ("Cambiar unidad de viaje ID: " + dgvViaje.CurrentRow.Cells[0].Value.ToString());
                    frmInputDias.ShowDialog();

                }
            }
            catch (Exception ex)
            {
                if(ex.Message.Equals("Referencia a objeto no establecida como instancia de un objeto."))
                {
                    new frmMensajes("Informacion", "Tiene que haber un viaje seleccionado").ShowDialog();
                }
                else
                {
                    new frmMensajes("Informacion", "No se puede cambiar esta unidad").ShowDialog();
                }
                
                btnViajeCancelarCambioUnidad_Click(null,null);
            }
            
        }

        private void btnViajeCancelarCambioUnidad_Click(object sender, EventArgs e)//El panel de seleccionar pedido vuelve a verse
        {
            selecciona_acoplado = false;
            selecciona_camion = false;
            gbLogistica.SelectedTab = gbLogistica.TabPages[0];
            lblCambiarUnidadesViaje.Visible = false;
            btnViajeCancelarCambioUnidad.Visible = false;
            btnViajeOkCambioDeUnidad.Visible = false;
            btnSeleccionPedido.Visible = true;
            dgvCamion.DataSource = null;
            dgvAcoplado.DataSource = null;
            cambiar_unidad = false;
        }

        private void btnViajeOkCambioDeUnidad_Click(object sender, EventArgs e)
        {
            frmMensajes cambiarunidadmensaje;
            if ((dgvCamion.Rows.Count > 0 && dgvAcoplado.Rows.Count >0 && selecciona_camion && selecciona_acoplado) || (dgvCamion.Rows.Count>0 && selecciona_camion && dgvAcoplado.Rows.Count == 0 && !selecciona_acoplado))
            {
                string resp = string.Empty;
                string respCancela = string.Empty;
                object Camion;
                object Acoplado;
                E_Acoplado e_Acoplado;
                E_Taller e_TallerCamion = null;
                E_Taller e_TallerAcoplado = null;
                //reservo las que van a pasar a ser las unidades en deshuso para este viaje
                E_Camion e_Camion = (E_Camion)new N_Camion(usuario).retornaCamion(e_UnidadesViaje.Id_camion);
                Camion = e_Camion;
                if (e_UnidadesViaje.Id_acoplado >= 0)
                {
                    e_Acoplado = new N_Acoplado(usuario).retornaAcoplado(e_UnidadesViaje.Id_acoplado);
                    Acoplado = e_Acoplado;
                }
                else
                {
                    e_Acoplado = null;
                    Acoplado = e_Acoplado;
                }
                //E_Acoplado e_Acoplado = new N_Acoplado(usuario).retornaAcoplado(e_UnidadesViaje.Id_acoplado);
                //Acoplado = e_Acoplado;
                float kmAntiguoCamion = e_Camion.Km_unidad;//obtenemos el km antiguo de la unidad, para poder comparar despues
                float kmActual = 0;
                float kmDelViaje = 0;
                List<string> respAcoplado;
                List<string> respCamion;


                if (selecciona_camion)//voy a cambiar camion ???
                {
                    e_UnidadesViaje.Id_camion = Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value);
                }
                if (selecciona_acoplado)////voy a cambiar acoplado???
                {
                    e_UnidadesViaje.Id_acoplado = Convert.ToInt32(dgvAcoplado.CurrentRow.Cells[0].Value);

                }


                // Lo que voy a hacer es, si modifico km camion, entonces modifico acoplado, camion y kmviaje y despues sigo con los demas, esto va a cubrir todos los escenarios y el codigo 
                //no se repetira tanto, por tanto sera mas limpio y legible.

                frmInputKmActualCamion frmInputKmActualCamion = new frmInputKmActualCamion(e_Camion, usuario);  //Habrimos el form para ingresar los km actual del camion
                frmInputKmActualCamion.enviaKm += (float KmActualCamion) =>
                {
                    kmActual = KmActualCamion;
                    respuestas = n_viaje.modificarUnidadesViaje(e_viaje, e_UnidadesViaje);
                };
                frmInputKmActualCamion.enviaCancelacion += () =>
                {
                    btnViajeCancelarCambioUnidad_Click(null, null);//hago lo mismo que cancelar cambio de unidad para que se vuelva a ver todo como estaba
                    cbViajeFiltrar.SelectedIndex = 0;
                    cbViajeFiltrar2.SelectedIndex = 0;
                    tbViajeBusquedaTexto.Text = e_viaje.Id_viaje.ToString();//MOSTRAMOS ESA UNIDAD QUE SE MODIFICO
                    selecciona_acoplado = false;
                    selecciona_camion = false;
                    resp = "CANCELADO";
                };

                frmInputKmActualCamion.ShowDialog();



                if (resp.Equals("") || resp == null || resp == string.Empty)//SE MOFICARON LAS UNIDADES DEL VIAJE ?????
                {
                    new frmMensajes("Exito", "Unidad de viaje modificada").ShowDialog();
                    if (kmAntiguoCamion != kmActual)
                    {
                        e_TallerCamion = n_Taller.Calculo(ref Camion, kmActual, ref kmDelViaje);

                        respCamion = n_Camion.modificar(e_Camion, string.Empty, "VIAJE:Codigo: " + e_viaje.Id_viaje + ", modifica datos de unidad");
                        if (e_Acoplado != null)
                        {
                            e_TallerAcoplado = n_Taller.Calculo(ref Acoplado, kmActual, ref kmDelViaje);
                            respAcoplado = n_acoplado.modificar(e_Acoplado, string.Empty, "VIAJE:Codigo: " + e_viaje.Id_viaje + ", modifica datos de unidad");
                        }
                        e_viaje.Km_cierre = e_viaje.Km_cierre + kmDelViaje;// Setea Km en unidades
                        respuestas = n_viaje.modificarViaje(e_viaje);///modifica el viaje


                    }


                    if (selecciona_camion)//Enviar Camion a taller ????
                    {
                        //verificamos si la unidad ESTA EN MANTENIMIENTO (puede que este cambiando la unidad porque en un viaje anterior la misma se averio pero siguio cargada en otros viajes)
                        resp = n_Taller.Verifica_si_unidad_puede_ingresar_a_talle(n_dominio.Retorna_Dominio(e_Camion.Id_Dominio));
                        if (resp.Equals("CAMION") || resp.Equals("Este camion debe ser cargado a mantenimiento por el encargado de logistica"))// si camion se encuentra en taller entonces no pasa por aca
                        {
                            resp = string.Empty;
                            cambiarunidadmensaje = new frmMensajes("Pregunta", "Desea enviar camion al taller ? ");
                            cambiarunidadmensaje.SioNo += (bool r) =>
                            {
                                if (r)// si, lo quiero mandar a taller
                                {
                                    if (e_TallerCamion == null)//PORQUE SEGUN LOS CALCULOS NO VA A TALLER POR SERVICE O NEUMATICOS
                                    {
                                        resp = "NO_TALLER";//Esto es para controlar, si se cancela el envio a taller tenemos en cuenta si antes se creo o no taller
                                        e_TallerCamion = new E_Taller();
                                    }
                                    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_TallerCamion, e_Camion);
                                    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                    {

                                        this.respuestas = n_Taller.insertar(e_T);
                                        if (respuestas[0].Equals("Exito"))
                                        {
                                            resp = string.Empty;
                                            string[] Separador = new string[] { "+" };
                                            string[] tipos = e_TallerCamion.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string a in tipos)
                                            {
                                                resp = resp + " " + a.ToString().ToUpper() + ", ";

                                            }
                                            new frmMensajes("Exito", "Camion enviado a MANTENIMIENTO por " + resp + "").ShowDialog();
                                        }
                                        resp = "NO_TALLER";
                                    };
                                    frmUnidadTaller.EnviaCancelaEnvio += () =>
                                    {
                                        respCancela = "CANCELA_ENVIO";
                                    };
                                    frmUnidadTaller.ShowDialog();
                                    if (respCancela.Equals("CANCELA_ENVIO"))
                                        new frmMensajes("Informacion", "Se cancela envio de Camion a TALLER!").ShowDialog();

                                    if (!resp.Equals("NO_TALLER"))// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                                    {
                                        this.respuestas = n_Taller.insertar(e_TallerCamion);
                                        if (respuestas[0].Equals("Exito"))
                                        {
                                            resp = string.Empty;
                                            string[] Separador = new string[] { "+" };
                                            string[] tipos = e_TallerCamion.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string a in tipos)
                                            {
                                                resp = resp + " " + a.ToString().ToUpper() + ", ";

                                            }
                                            new frmMensajes("Exito", "Camion enviado a MANTENIMIENTO por " + resp + "").ShowDialog();
                                        }

                                    }



                                }
                                else
                                {
                                    if (e_TallerCamion != null)// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                                    {
                                        this.respuestas = n_Taller.insertar(e_TallerCamion);
                                        if (respuestas[0].Equals("Exito"))
                                        {
                                            resp = string.Empty;
                                            string[] Separador = new string[] { "+" };
                                            string[] tipos = e_TallerCamion.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string a in tipos)
                                            {
                                                resp = resp + " " + a.ToString().ToUpper() + ", ";

                                            }
                                            new frmMensajes("Exito", "Camion enviado a MANTENIMIENTO por " + resp + "").ShowDialog();
                                        }

                                    }
                                }
                            };
                            cambiarunidadmensaje.ShowDialog();
                          

                            //resp = string.Empty;
                            //DialogResult result = System.Windows.Forms.MessageBox.Show("Desea enviar camion al taller ? ", "Mantenimiento", MessageBoxButtons.YesNo);
                            
                            //if (result == DialogResult.Yes)//  //////////////           SI, LO QUIERO MANDAR A TALLER
                            //{
                            //    if (e_TallerCamion == null)//PORQUE SEGUN LOS CALCULOS NO VA A TALLER POR SERVICE O NEUMATICOS
                            //    {
                            //        resp = "NO_TALLER";//Esto es para controlar, si se cancela el envio a taller tenemos en cuenta si antes se creo o no taller
                            //        e_TallerCamion = new E_Taller();
                            //    }
                            //    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_TallerCamion, e_Camion);
                            //    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                            //    {

                            //        this.respuestas = n_Taller.insertar(e_T);
                            //        if (respuestas[0].Equals("Exito"))
                            //        {
                            //            resp = string.Empty;
                            //            string[] Separador = new string[] { "+" };
                            //            string[] tipos = e_TallerCamion.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                            //            foreach (string a in tipos)
                            //            {
                            //                resp = resp + " " + a.ToString().ToUpper() + ", ";

                            //            }
                            //            new frmMensajes("Exito", "Camion enviado a MANTENIMIENTO por " + resp + "").ShowDialog();
                            //        }
                            //        resp = "NO_TALLER";
                            //    };
                            //    frmUnidadTaller.EnviaCancelaEnvio += () =>
                            //    {
                            //        respCancela = "CANCELA_ENVIO";
                            //    };
                            //    frmUnidadTaller.ShowDialog();
                            //    if (respCancela.Equals("CANCELA_ENVIO"))
                            //        new frmMensajes("Informacion", "Se cancela envio de Camion a TALLER!").ShowDialog();

                            //    if (!resp.Equals("NO_TALLER"))// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                            //    {
                            //        this.respuestas = n_Taller.insertar(e_TallerCamion);
                            //        if (respuestas[0].Equals("Exito"))
                            //        {
                            //            resp = string.Empty;
                            //            string[] Separador = new string[] { "+" };
                            //            string[] tipos = e_TallerCamion.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                            //            foreach (string a in tipos)
                            //            {
                            //                resp = resp + " " + a.ToString().ToUpper() + ", ";

                            //            }
                            //            new frmMensajes("Exito", "Camion enviado a MANTENIMIENTO por " + resp + "").ShowDialog();
                            //        }

                            //    }




                            //} // si no se actualiza km unidad entonces solo la envio a taller
                            //else // NO, NO LO QUIERO MANDAR AL TALLER
                            //{
                            //    if (e_TallerCamion != null)// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                            //    {
                            //        this.respuestas = n_Taller.insertar(e_TallerCamion);
                            //        if (respuestas[0].Equals("Exito"))
                            //        {
                            //            resp = string.Empty;
                            //            string[] Separador = new string[] { "+" };
                            //            string[] tipos = e_TallerCamion.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                            //            foreach (string a in tipos)
                            //            {
                            //                resp = resp + " " + a.ToString().ToUpper() + ", ";

                            //            }
                            //            new frmMensajes("Exito", "Camion enviado a MANTENIMIENTO por " + resp + "").ShowDialog();
                            //        }

                            //    }
                            //}
                        }

                    }
                    if (selecciona_acoplado)//Enviar Acoplado a taller ????
                    {
                        resp = string.Empty;
                        //verificamos si la unidad ESTA EN MANTENIMIENTO (puede que este cambiando la unidad porque en un viaje anterior la misma se averio pero siguio cargada en otros viajes)
                        resp = n_Taller.Verifica_si_unidad_puede_ingresar_a_talle(n_dominio.Retorna_Dominio(e_Acoplado.Id_Dominio));
                        if (resp.Equals("ACOPLADO") || resp.Equals("Este remolque debe ser cargado a mantenimiento por el encargado de logistica"))
                        {
                            resp = string.Empty;
                            cambiarunidadmensaje = new frmMensajes("Pregunta", "Desea enviar camion al taller ? ");
                            cambiarunidadmensaje.SioNo += (bool r) =>
                            {

                                if (r)
                                {
                                    if (e_TallerAcoplado == null)//PORQUE SEGUN LOS CALCULOS NO VA A TALLER POR SERVICE O NEUMATICOS
                                    {
                                        resp = "NO_TALLER";
                                        e_TallerAcoplado = new E_Taller();
                                    }
                                    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_TallerAcoplado, e_Acoplado);
                                    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                    {
                                        respuestas = n_Taller.insertar(e_T);
                                        if (respuestas[0].Equals("Exito"))
                                        {
                                            resp = string.Empty;
                                            string[] Separador = new string[] { "+" };
                                            string[] tipos = e_TallerAcoplado.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string a in tipos)
                                            {
                                                resp = resp + " " + a.ToString().ToUpper() + ", ";

                                            }
                                            new frmMensajes("Exito", "Acoplado enviado a MANTENIMIENTO por motivos: " + resp + "").ShowDialog();
                                        }
                                        resp = "NO_TALLER";
                                    };
                                    frmUnidadTaller.EnviaCancelaEnvio += () =>
                                    {
                                        respCancela = "CANCELA_ENVIO";
                                    };
                                    frmUnidadTaller.ShowDialog();
                                    if (respCancela.Equals("CANCELA_ENVIO"))
                                        new frmMensajes("Informacion", "Se cancela envio de Camion a TALLER!").ShowDialog();

                                    if (!resp.Equals("NO_TALLER"))// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                                    {
                                        respuestas = n_Taller.insertar(e_TallerAcoplado);
                                        if (respuestas[0].Equals("Exito"))
                                            resp = string.Empty;
                                        string[] Separador = new string[] { "+" };
                                        string[] tipos = e_TallerAcoplado.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                                        foreach (string a in tipos)
                                        {
                                            resp = resp + " " + a.ToString().ToUpper() + ", ";

                                        }
                                        new frmMensajes("Exito", " Acoplado enviado a mantenimiento AUTOMATICAMENTE por " + resp + "").ShowDialog();
                                    }
                                }
                                else
                                {
                                    if (e_TallerAcoplado != null)// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                                    {
                                        respuestas = n_Taller.insertar(e_TallerAcoplado);
                                        if (respuestas[0].Equals("Exito"))
                                        {
                                            resp = string.Empty;
                                            string[] Separador = new string[] { "+" };
                                            string[] tipos = e_TallerAcoplado.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                                            foreach (string a in tipos)
                                            {
                                                resp = resp + " " + a.ToString().ToUpper() + ", ";

                                            }
                                            new frmMensajes("Exito", " Acoplado enviado a mantenimiento AUTOMATICAMENTE por " + resp + "").ShowDialog();
                                        }

                                    }
                                }
                            };
                            cambiarunidadmensaje.ShowDialog();





                            //    resp = string.Empty;
                            //DialogResult result = System.Windows.Forms.MessageBox.Show("Desea enviar Acoplado al taller ? ", "Mantenimiento", MessageBoxButtons.YesNo);

                            //if (result == DialogResult.Yes)//SI, LO QUIERO MANDAR A TALLER
                            //{
                            //    if (e_TallerAcoplado == null)//PORQUE SEGUN LOS CALCULOS NO VA A TALLER POR SERVICE O NEUMATICOS
                            //    {
                            //        resp = "NO_TALLER";
                            //        e_TallerAcoplado = new E_Taller();
                            //    }
                            //    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_TallerAcoplado, e_Acoplado);
                            //    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                            //    {
                            //        respuestas = n_Taller.insertar(e_T);
                            //        if (respuestas[0].Equals("Exito"))
                            //        {
                            //            resp = string.Empty;
                            //            string[] Separador = new string[] { "+" };
                            //            string[] tipos = e_TallerAcoplado.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                            //            foreach (string a in tipos)
                            //            {
                            //                resp = resp + " " + a.ToString().ToUpper() + ", ";

                            //            }
                            //            new frmMensajes("Exito", "Acoplado enviado a MANTENIMIENTO por motivos: " + resp +"").ShowDialog();
                            //        }
                            //        resp = "NO_TALLER";
                            //    };
                            //    frmUnidadTaller.EnviaCancelaEnvio += () =>
                            //    {
                            //        respCancela = "CANCELA_ENVIO";
                            //    };
                            //    frmUnidadTaller.ShowDialog();
                            //    if (respCancela.Equals("CANCELA_ENVIO"))
                            //        new frmMensajes("Informacion", "Se cancela envio de Camion a TALLER!").ShowDialog();

                            //    if (!resp.Equals("NO_TALLER"))// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                            //    {
                            //        respuestas = n_Taller.insertar(e_TallerAcoplado);
                            //        if (respuestas[0].Equals("Exito"))
                            //            resp = string.Empty;
                            //        string[] Separador = new string[] { "+" };
                            //        string[] tipos = e_TallerAcoplado.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                            //        foreach (string a in tipos)
                            //        {
                            //            resp = resp + " " + a.ToString().ToUpper() + ", ";

                            //        }
                            //        new frmMensajes("Exito", " Acoplado enviado a mantenimiento AUTOMATICAMENTE por " + resp + "").ShowDialog();
                            //    }

                            //}
                            //else // NO, NO LO QUIERO MANDAR AL TALLER
                            //{
                            //    if (e_TallerAcoplado != null)// si taller es distinto de null es por que km actual es mayor que km unidad y encima tiene que ir a mantenimiento
                            //    {
                            //        respuestas = n_Taller.insertar(e_TallerAcoplado);
                            //        if (respuestas[0].Equals("Exito"))
                            //        {
                            //            resp = string.Empty;
                            //            string[] Separador = new string[] { "+" };
                            //            string[] tipos = e_TallerAcoplado.Tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                            //            foreach (string a in tipos)
                            //            {
                            //                resp = resp + " " + a.ToString().ToUpper() + ", ";

                            //            }
                            //            new frmMensajes("Exito", " Acoplado enviado a mantenimiento AUTOMATICAMENTE por " + resp + "").ShowDialog();
                            //        }

                            //    }
                            //}

                        }

                    }

                }
                else
                    new frmMensajes("Error", "No pudimos modificar la/s unidad/es del viaje").ShowDialog();
                btnViajeCancelarCambioUnidad_Click(null, null);//hago lo mismo que cancelar cambio de unidad para que se vuelva a ver todo como estaba
                cbViajeFiltrar.SelectedIndex = 0;
                cbViajeFiltrar2.SelectedIndex = 0;
                tbViajeBusquedaTexto.Text = e_viaje.Id_viaje.ToString();//MOSTRAMOS ESA UNIDAD QUE SE MODIFICO
                selecciona_acoplado = false;
                selecciona_camion = false;
                dgvCamion.DataSource = null;
                dgvAcoplado.DataSource = null;
                limpiaBindingCamion();
                limpiaTextboxBindingCamion();
                limpiaBindingRemolque();
                limpiaTextboxBindingRemolque();
            }
            else
                new frmMensajes("Error", "Debe seleccionar unidades a cambiar para continuar").ShowDialog();


        }
        #endregion
        #region editar viaje
        void editarViaje(E_Viaje e_viaje)//modifica el viaje y cumple casi la misma funcion del load...carga 
            {
            respuestas = n_viaje.modificarViaje(e_viaje);
                dtViaje = n_viaje.listarEstado(Estado.Todos);
                dgvViaje.DataSource = dtViaje;
                carga_datos_label_viaje();
                cbViajeFiltrar.SelectedIndex = 0;
                carga_cbfitrarviaje2_completo(true);
            }


            private void btnViajeEditar_Click(object sender, EventArgs e)
            {// falta traer ese viaje
            try
            {
                e_viaje = n_viaje.retornaViaje(dgvViaje.CurrentRow.Cells[0].Value.ToString());
                if (e_viaje != null)
                {
                    frmEditarViaje frmEditarViaje = new frmEditarViaje(e_viaje, null, Estado.Todos);//es un editar dondo no importa el estado que tenga el viaje
                    frmEditarViaje.enviaViaje += editarViaje;
                    frmEditarViaje.EnviaCancelaEnvio += () =>
                    {
                        btnViajeCancelarCambioUnidad_Click(null, null);
                    };
                    frmEditarViaje.ShowDialog();
                }
                else
                    new frmMensajes("Error", "Ups, algo paso al buscar viaje seleccionado");
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Ups, algo paso al buscar viaje seleccionado");
            }
               
            }
        #endregion
        #region botonotera de estados de viaje
        private void escondeBotonesEstadosViaje()
        {
            if (dgvViaje.Rows.Count > 0)
            {
                btnViajeConcluir.Enabled = true;
                btnViajeEnCurso.Enabled = true;
                btnViajePendiente.Enabled = true;
                btnViajeSuspender.Enabled = true;
                btnViajeCancelar.Enabled = true;
            }
            else
            {
                btnViajeConcluir.Enabled = false;
                btnViajeEnCurso.Enabled = false;
                btnViajePendiente.Enabled = false;
                btnViajeSuspender.Enabled = false;
                btnViajeCancelar.Enabled = false;
            }
        }

        void estadoViaje(E_Viaje e_viaje, Estado e, float kmViaje)//modifica el viaje y cumple casi la misma funcion del load...carga 
        {
             respuestas= n_viaje.cambiarEstado(ref e_viaje, e, kmViaje);
            dtViaje = n_viaje.listarEstado(Estado.Todos);
            dgvViaje.DataSource = dtViaje;
            carga_datos_label_viaje();
            cbViajeFiltrar.SelectedIndex = 0;
            carga_cbfitrarviaje2_completo(true);
            if (!e.ToString().ToUpper().Equals("CONCLUIDO"))
            {
                new frmMensajes(respuestas[0],respuestas[1]).ShowDialog();
            }
            btnViajeCancelarCambioUnidad_Click(null,null);
        }
        private void btnViajeCancelar_Click(object sender, EventArgs e)//acciones al apretar botones de la botonera de estados
        {
            try
            {
                bool deseamandarunidadataller = false;
                string[] tipos = null;

                E_Taller e_Taller;
                e_viaje = n_viaje.retornaViaje(dgvViaje.CurrentRow.Cells[0].Value.ToString());
                e_UnidadesViaje = n_viaje.retornaUnidadesViaje(e_viaje);
                E_Camion e_camion = (E_Camion)new N_Camion(usuario).retornaCamion(e_UnidadesViaje.Id_camion);
                E_Acoplado e_acoplado = null;
                string estado = dgvViaje.CurrentRow.Cells[6].Value.ToString();
                bool recomiendaCambioCamion = false;
                bool recomiendaCambioAcoplado = false;
                string respAcoplado = "";
                string respCamion = "";
                //VIAJE CONCLUIR//

                if (sender == btnViajeConcluir)
                {
                    bool cancelar = false;// esto sirve solo para concluir
                    frmEditarViaje frmEditarViaje = new frmEditarViaje(e_camion, e_viaje, Estado.Concluido);
                    frmEditarViaje.cambiarEstadoViaje += estadoViaje;
                    frmEditarViaje.EnviaCancelaEnvio += () => {

                        cancelar = true;

                    };

                    frmEditarViaje.ShowDialog();
                    if (!cancelar)
                    {
                        string[] Separador = new string[] { "+" };
                        if (respuestas[1] != null)
                        {
                            tipos = respuestas[1].Split(Separador, StringSplitOptions.RemoveEmptyEntries);
                        }

                        if (!tipos[0].Equals("Viaje concluido") || tipos != null)//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                        {
                            new frmMensajes("Exito", "VIAJE CONCLUIDO").ShowDialog();
                            if (Array.Exists(tipos, element => element == "CAMION"))
                                new frmMensajes("Informacion", "El CAMION con dominio " + n_dominio.Retorna_Dominio(e_UnidadesViaje.Id_camion) + " se ingresa a mantenimiento AUTOMATICAMENTE").ShowDialog();
                            else //desea enviarlo a taller? 
                            {
                                e_camion = (E_Camion)n_Camion.retornaCamion(e_camion.Id_Dominio);


                                frmmensaje = new frmMensajes("Pregunta", "Desea a enviar CAMION a taller ? " + "\n - Faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n - Faltan " + e_camion.Km_service_suma + " KM para SERVICE");
                                frmmensaje.SioNo += (bool r) => {
                                    if (r)
                                    {
                                        e_Taller = new E_Taller();
                                        E_Camion e_Camion = (E_Camion)n_Camion.retornaCamion(e_UnidadesViaje.Id_camion);

                                        frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Camion);
                                        frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                        {
                                            e_Taller = e_T;
                                            if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                                new frmMensajes("Exito", "Camion ingresa a mantenimiento").ShowDialog();
                                            else
                                                new frmMensajes("Error", "Error al ingresar Camion a taller").ShowDialog();
                                        };
                                        frmUnidadTaller.EnviaCancelaEnvio += () =>
                                        {
                                            btnViajeCancelarCambioUnidad_Click(null, null);
                                        };
                                        frmUnidadTaller.ShowDialog();

                                    }
                                };
                                frmmensaje.ShowDialog();
                                //result = System.Windows.Forms.MessageBox.Show("Desea a enviar CAMION a taller ? " + "\n -Faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE", "Mantenimiento", MessageBoxButtons.YesNo);
                                //if (result == DialogResult.Yes)//SI, LO QUIERO MANDAR A TALLER
                                //{
                                //    e_Taller = new E_Taller();
                                //    E_Camion e_Camion = (E_Camion)n_Camion.retornaCamion(e_UnidadesViaje.Id_camion);

                                //    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Camion);
                                //    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                //    {
                                //        e_Taller = e_T;
                                //    };
                                //    frmUnidadTaller.EnviaCancelaEnvio += () =>
                                //    {
                                //        btnViajeCancelarCambioUnidad_Click(null, null);
                                //    };
                                //    frmUnidadTaller.ShowDialog();
                                //    if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                //        new frmMensajes("Exito", "Camion ingresa a mantenimiento").ShowDialog();
                                //    else
                                //        new frmMensajes("Error", "Error al ingresar Camion a taller").ShowDialog();
                                //}
                            }
                            if (Array.Exists(tipos, element => element == "ACOPLADO"))
                                new frmMensajes("Informacion", "El REMOLQUE con dominio " + n_dominio.Retorna_Dominio(e_UnidadesViaje.Id_acoplado) + " se ingresa a mantenimiento AUTOMATICAMENTE").ShowDialog();
                            else
                            {
                                if (!e_UnidadesViaje.Id_acoplado.Equals("") && e_UnidadesViaje.Id_acoplado != -1 && e_UnidadesViaje.Id_acoplado != 0)
                                {
                                    E_Acoplado e_Acoplado = n_acoplado.retornaAcoplado(e_UnidadesViaje.Id_acoplado);
                                    frmmensaje = new frmMensajes("Pregunta", "Desea a enviar REMOLQUE a taller ? " + "\n -Faltan " + e_Acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_Acoplado.Km_service_suma + " KM para SERVICE");
                                    frmmensaje.SioNo += (bool r) =>
                                    {
                                        if (r)
                                        {
                                            e_Taller = new E_Taller();
                                            frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Acoplado);
                                            frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                            {
                                                e_Taller = e_T;

                                                if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                                    new frmMensajes("Exito", "Acoplado ingresa a mantenimiento").ShowDialog();
                                                else
                                                    new frmMensajes("Error", "Error al ingresar Acoplado a taller").ShowDialog();
                                            };
                                            frmUnidadTaller.EnviaCancelaEnvio += () =>
                                            {
                                                btnViajeCancelarCambioUnidad_Click(null, null);
                                            };
                                            frmUnidadTaller.ShowDialog();

                                        }
                                    };
                                    frmmensaje.ShowDialog();

                                    //        result = System.Windows.Forms.MessageBox.Show("Desea a enviar REMOLQUE a taller ? " + "\n -Faltan " + e_Acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_Acoplado.Km_service_suma + " KM para SERVICE", "Mantenimiento", MessageBoxButtons.YesNo);

                                    //if (result == DialogResult.Yes)//SI, LO QUIERO MANDAR A TALLER
                                    //{
                                    //    e_Taller = new E_Taller();
                                    //    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Acoplado);
                                    //    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                    //    {
                                    //        e_Taller = e_T;
                                    //    };
                                    //    frmUnidadTaller.EnviaCancelaEnvio += () =>
                                    //    {
                                    //        btnViajeCancelarCambioUnidad_Click(null, null);
                                    //    };
                                    //    frmUnidadTaller.ShowDialog();

                                    //    if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                    //        new frmMensajes("Exito", "Acoplado ingresa a mantenimiento").ShowDialog();
                                    //    else
                                    //        new frmMensajes("Error", "Error al ingresar Acoplado a taller").ShowDialog();
                                    //}
                                }
                            }
                        }
                        else
                        {
                            new frmMensajes("Exito", "Viaje Concluido").ShowDialog();
                            if (!tipos.Contains("CAMION"))
                            {
                                e_camion = (E_Camion)n_Camion.retornaCamion(e_camion.Id_Dominio);
                                frmmensaje = new frmMensajes("Pregunta", "Desea a enviar CAMION a taller ? " + "\n -Faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE");
                                frmmensaje.SioNo += (bool r) =>
                                {
                                    if (r)
                                    {
                                        e_Taller = new E_Taller();
                                        E_Camion e_Camion = (E_Camion)n_Camion.retornaCamion(e_UnidadesViaje.Id_camion);

                                        frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Camion);
                                        frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                        {
                                            e_Taller = e_T;
                                            if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                                new frmMensajes("Exito", "Camion ingresa a mantenimiento").ShowDialog();
                                            else
                                                new frmMensajes("Error", "Error al ingresar camion a taller").ShowDialog();
                                        };
                                        frmUnidadTaller.EnviaCancelaEnvio += () =>
                                        {
                                            btnViajeCancelarCambioUnidad_Click(null, null);
                                        };
                                        frmUnidadTaller.ShowDialog();

                                    }
                                };
                                frmmensaje.ShowDialog();

                                //        result = System.Windows.Forms.MessageBox.Show("Desea a enviar CAMION a taller ? " + "\n -Faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE", "Mantenimiento", MessageBoxButtons.YesNo);
                                //if (result == DialogResult.Yes)//SI, LO QUIERO MANDAR A TALLER
                                //{
                                //    e_Taller = new E_Taller();
                                //    E_Camion e_Camion = (E_Camion)n_Camion.retornaCamion(e_UnidadesViaje.Id_camion);

                                //    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Camion);
                                //    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                //    {
                                //        e_Taller = e_T;
                                //    };
                                //    frmUnidadTaller.EnviaCancelaEnvio += () =>
                                //    {
                                //        btnViajeCancelarCambioUnidad_Click(null, null);
                                //    };
                                //    frmUnidadTaller.ShowDialog();
                                //    if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                //        new frmMensajes("Exito", "Camion ingresa a mantenimiento").ShowDialog();
                                //    else
                                //        new frmMensajes("Error", "Error al ingresar camion a taller").ShowDialog();
                                //}
                            }

                            if (!e_UnidadesViaje.Id_acoplado.Equals("") && e_UnidadesViaje.Id_acoplado != -1 && e_UnidadesViaje.Id_acoplado != 0)
                            {
                                E_Acoplado e_Acoplado = n_acoplado.retornaAcoplado(e_UnidadesViaje.Id_acoplado);
                                frmmensaje.SioNo += (bool r) =>
                                {
                                    if (r)
                                    {
                                        e_Taller = new E_Taller();

                                        frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Acoplado);
                                        frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                        {
                                            e_Taller = e_T;
                                            if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                                new frmMensajes("Exito", "Acoplado ingresa a mantenimiento").ShowDialog();
                                            else
                                                new frmMensajes("Error", "Error al ingresar Acoplado a taller").ShowDialog();
                                        };
                                        frmUnidadTaller.EnviaCancelaEnvio += () =>
                                        {
                                            btnViajeCancelarCambioUnidad_Click(null, null);
                                        };
                                        frmUnidadTaller.ShowDialog();

                                    }
                                };
                                frmmensaje.ShowDialog();
                                //        result = System.Windows.Forms.MessageBox.Show("Desea a enviar REMOLQUE a taller ? " + "\n -Faltan " + e_Acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_Acoplado.Km_service_suma + " KM para SERVICE", "Mantenimiento", MessageBoxButtons.YesNo);

                                //if (result == DialogResult.Yes)//SI, LO QUIERO MANDAR A TALLER
                                //{
                                //    e_Taller = new E_Taller();

                                //    frmEditarViaje frmUnidadTaller = new frmEditarViaje(e_Taller, e_Acoplado);
                                //    frmUnidadTaller.enviaTaller += (E_Taller e_T) =>
                                //    {
                                //        e_Taller = e_T;
                                //    };
                                //    frmUnidadTaller.EnviaCancelaEnvio += () =>
                                //    {
                                //        btnViajeCancelarCambioUnidad_Click(null, null);
                                //    };
                                //    frmUnidadTaller.ShowDialog();
                                //    if (n_Taller.insertar(e_Taller)[0].Equals("Exito"))
                                //        new frmMensajes("Exito", "Acoplado ingresa a mantenimiento").ShowDialog();
                                //    else
                                //        new frmMensajes("Error", "Error al ingresar Acoplado a taller").ShowDialog();
                                //}
                            }
                        }
                    }
                    else
                    {
                        return;
                    }


                }
                //VIAJE EN CURSO//
                if (sender == btnViajeEnCurso)
                {
                    deseamandarunidadataller = false;//SETEAMOS  a false( viejo EL DIALOG RESULT A "NO") LO VAMOS A USAR MAS ABAJO, DONDERECOMENTDAMOS CAMBIAR UNIDAD PARA  PASAR A EN CURSO, si no se recomienda cambiar la unidad, como esta seteado en "NO", entonces procedde a pasar el viaje a en curtso
                    respCamion = n_Taller.Verifica_si_unidad_puede_ingresar_a_talle(n_dominio.Retorna_Dominio(e_UnidadesViaje.Id_camion));
                    if (!e_UnidadesViaje.Id_acoplado.Equals("") && e_UnidadesViaje.Id_acoplado != -1 && e_UnidadesViaje.Id_acoplado != 0)//recomienda cambiar acoplado si teiene acoplado
                    {
                        respAcoplado = n_Taller.Verifica_si_unidad_puede_ingresar_a_talle(n_dominio.Retorna_Dominio(e_UnidadesViaje.Id_acoplado));
                    }
                    if ((!respCamion.Equals("Este camion debe ser cargado a mantenimiento por el encargado de logistica") && !respCamion.Equals("CAMION")) || ((!respAcoplado.Equals("Este remolque debe ser cargado a mantenimiento por el encargado de logistica") && !respAcoplado.Equals("ACOPLADO")) && (!e_UnidadesViaje.Id_acoplado.Equals("") && e_UnidadesViaje.Id_acoplado != -1 && e_UnidadesViaje.Id_acoplado != 0)))
                    {
                        if ((!respCamion.Equals("Este camion debe ser cargado a mantenimiento por el encargado de logistica") && !respCamion.Equals("CAMION")) && (!respAcoplado.Equals("Este remolque debe ser cargado a mantenimiento por el encargado de logistica") && !respAcoplado.Equals("ACOPLADO")))
                            new frmMensajes("Informacion", "CAMION Y REMOLQUE se encuentran en mantenimiento, proceder a cambiarlos").ShowDialog();
                        else if ((!respCamion.Equals("Este camion debe ser cargado a mantenimiento por el encargado de logistica") && !respCamion.Equals("CAMION")))
                            new frmMensajes("Informacion", "" + respCamion + ", proceder a cambiarla, CAMION").ShowDialog();
                        else
                            new frmMensajes("Informacion", "" + respAcoplado + ", proceder a cambiarla, ACOPLADO").ShowDialog();

                        btnViajeCambiarUnidad.Focus();
                    }// ninguna unidad se encuentra en taller ?? buenisimo seguimos...
                    else
                    {
                        if (float.Parse(dgvViaje.CurrentRow.Cells[13].Value.ToString()) > (e_camion.Km_service_suma - (e_camion.Km_service_suma * 0.3)) || float.Parse((dgvViaje.CurrentRow.Cells[13].Value.ToString())) > (e_camion.Km_cambio_neumaticos_suma - (e_camion.Km_cambio_neumaticos_suma * 0.3)))
                        {
                            recomiendaCambioCamion = true;//recomienda cambiara camion
                        }
                        if (!e_UnidadesViaje.Id_acoplado.Equals("") && e_UnidadesViaje.Id_acoplado != -1 && e_UnidadesViaje.Id_acoplado != 0)//recomienda cambiar acoplado si teiene acoplado
                        {
                            e_acoplado = n_acoplado.retornaAcoplado(e_UnidadesViaje.Id_acoplado);
                            if (float.Parse(dgvViaje.CurrentRow.Cells[13].Value.ToString()) > (e_acoplado.Km_service_suma - (e_acoplado.Km_service_suma * 0.3)) || float.Parse((dgvViaje.CurrentRow.Cells[13].Value.ToString())) > (e_acoplado.Km_cambio_neumaticos_suma - (e_acoplado.Km_cambio_neumaticos_suma * 0.3)))
                            {
                                recomiendaCambioAcoplado = true;
                            }
                        }
                        if (recomiendaCambioCamion || recomiendaCambioAcoplado)
                        {

                            if (recomiendaCambioAcoplado && recomiendaCambioCamion)
                            {
                                frmmensaje = new frmMensajes("Pregunta", "Desea a CAMBIAR CAMION y/o REMOLQUE ? " + "\n -A camion le faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE" + "\n -A remolque le faltan " + e_acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_acoplado.Km_service_suma + " KM para SERVICE" + "\n El viaje sera de aprox.  " + dgvViaje.CurrentRow.Cells[13].Value.ToString() + " Km");
                                frmmensaje.SioNo += (bool r) =>
                                {
                                    if (r)
                                    {
                                        deseamandarunidadataller = true;
                                    }
                                };
                                frmmensaje.ShowDialog();



                                //result = System.Windows.Forms.MessageBox.Show("Desea a CAMBIAR CAMION y/o REMOLQUE ? " + "\n -A camion le faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE" + "\n -A remolque le faltan " + e_acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_acoplado.Km_service_suma + " KM para SERVICE" + "\n El viaje sera de aprox.  " + dgvViaje.CurrentRow.Cells[13].Value.ToString() + " Km", "Mantenimiento", MessageBoxButtons.YesNo);
                            }
                            else if (recomiendaCambioCamion)
                            {
                                frmmensaje = new frmMensajes("Pregunta", "Desea a CAMBIAR CAMION ? " + "\n -Faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE" + "\n El viaje sera de aprox.  " + dgvViaje.CurrentRow.Cells[13].Value.ToString() + " Km");
                                frmmensaje.SioNo += (bool r) =>
                                {
                                    if (r)
                                    {
                                        deseamandarunidadataller = true;
                                    }
                                };
                                frmmensaje.ShowDialog();
                                //result = System.Windows.Forms.MessageBox.Show("Desea a CAMBIAR CAMION ? " + "\n -Faltan " + e_camion.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_camion.Km_service_suma + " KM para SERVICE" + "\n El viaje sera de aprox.  " + dgvViaje.CurrentRow.Cells[13].Value.ToString() + " Km", "Mantenimiento", MessageBoxButtons.YesNo);
                            }
                            else
                            {
                                frmmensaje = new frmMensajes("Pregunta", "Desea a CAMBIAR REMOLQUE ? " + "\n -Faltan " + e_acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_acoplado.Km_service_suma + " KM para SERVICE" + "\n El viaje sera de aprox.  " + dgvViaje.CurrentRow.Cells[13].Value.ToString() + " Km");
                                frmmensaje.SioNo += (bool r) =>
                                {
                                    if (r)
                                    {
                                        deseamandarunidadataller = true;
                                    }
                                };
                                frmmensaje.ShowDialog();
                                //result = System.Windows.Forms.MessageBox.Show("Desea a CAMBIAR REMOLQUE ? " + "\n -Faltan " + e_acoplado.Km_cambio_neumaticos_suma + " KM para CAMBIOS DE NEUMATICOS" + "\n -Faltan " + e_acoplado.Km_service_suma + " KM para SERVICE" + "\n El viaje sera de aprox.  " + dgvViaje.CurrentRow.Cells[13].Value.ToString() + " Km", "Mantenimiento", MessageBoxButtons.YesNo);
                            }
                        }
                        if (deseamandarunidadataller)
                        {
                            btnViajeCambiarUnidad.Focus();//el tipo la quiere cambiar
                        }
                        else // no la queres cambiar ??? ok, nos vamos de viaje no ma
                        {

                            frmEditarViaje frmEditarViaje = new frmEditarViaje(e_viaje, null, Estado.EnCurso);
                            frmEditarViaje.cambiarEstadoViaje += estadoViaje;
                            frmEditarViaje.EnviaCancelaEnvio += () =>
                            {
                                btnViajeCancelarCambioUnidad_Click(null, null);
                            };

                            frmEditarViaje.ShowDialog();

                        }
                    }
                }
                //// / /// //////////////////////////////////////////////////

                if (sender == btnViajePendiente)
                {
                    frmEditarViaje frmEditarViaje = new frmEditarViaje(e_viaje, e_camion, Estado.Pendiente);
                    frmEditarViaje.cambiarEstadoViaje += estadoViaje;
                    frmEditarViaje.EnviaCancelaEnvio += () =>
                    {
                        btnViajeCancelarCambioUnidad_Click(null, null);
                    };
                    frmEditarViaje.ShowDialog();
                }
                if (sender == btnViajeSuspender)
                {
                    frmEditarViaje frmEditarViaje = new frmEditarViaje(e_viaje, null, Estado.Suspendido);
                    frmEditarViaje.cambiarEstadoViaje += estadoViaje;
                    frmEditarViaje.EnviaCancelaEnvio += () =>
                    {
                        btnViajeCancelarCambioUnidad_Click(null, null);
                    };
                    frmEditarViaje.ShowDialog();
                }

                if (sender == btnViajeCancelar)
                {
                    frmEditarViaje frmEditarViaje = new frmEditarViaje(e_viaje, null, Estado.Cancelado);
                    frmEditarViaje.cambiarEstadoViaje += estadoViaje;
                    frmEditarViaje.EnviaCancelaEnvio += () =>
                    {
                        btnViajeCancelarCambioUnidad_Click(null, null);
                    };
                    frmEditarViaje.ShowDialog();
                }
            }
            catch(Exception ex)
            {
                new frmMensajes("Informacion","Ups! No pudimos modificar este viaje"+Environment.NewLine+"Verificar con admin de operaciones").ShowDialog();
            }
       
        }
        


        private void dgvViaje_CurrentCellChanged(object sender, EventArgs e)//ante desplazamientos sobre la grilla la botonera se va alterando
        {
            try
            {

                string estado = dgvViaje.CurrentRow.Cells[7].Value.ToString();
                if (estado != null)
                {
                    switch (estado)
                    {
                        case "PENDIENTE":
                            btnViajeCancelar.Enabled = true;
                            btnViajeEnCurso.Enabled = true;
                            btnViajeSuspender.Enabled = true;
                            btnViajeConcluir.Enabled = false;
                            btnViajePendiente.Enabled = false;
                            btnViajeCambiarUnidad.Enabled = true;
                            lblESTADOVIAJE.Text = "PENDIENTE";
                            lblESTADOVIAJE.BackColor = Color.Gold;
                            break;
                        case "CANCELADO":
                            btnViajeCancelar.Enabled = false;
                            btnViajeEnCurso.Enabled = false;
                            btnViajeSuspender.Enabled = false;
                            btnViajeConcluir.Enabled = false;
                            btnViajePendiente.Enabled = false;
                            btnViajeCambiarUnidad.Enabled = false;
                            lblESTADOVIAJE.Text = "CANCELADO";
                            lblESTADOVIAJE.BackColor = Color.Red;
                            break;
                        case "EN CURSO":
                            btnViajeCancelar.Enabled = false;
                            btnViajeEnCurso.Enabled = false;
                            btnViajeSuspender.Enabled = false;
                            btnViajeConcluir.Enabled = true;
                            btnViajePendiente.Enabled = true;
                            btnViajeCambiarUnidad.Enabled = true;
                            lblESTADOVIAJE.Text = "EN CURSO";
                            lblESTADOVIAJE.BackColor = Color.DeepSkyBlue;
                            break;
                        case "SUSPENDIDO":
                            btnViajeCancelar.Enabled = true;
                            btnViajeEnCurso.Enabled = false;
                            btnViajeSuspender.Enabled = false;
                            btnViajeConcluir.Enabled = false;
                            btnViajePendiente.Enabled = true;
                            btnViajeCambiarUnidad.Enabled = true;
                            lblESTADOVIAJE.Text = "SUSPENDIDO";
                            lblESTADOVIAJE.BackColor = Color.Orange;
                            break;
                        case "CONCLUIDO":
                            btnViajeCancelar.Enabled = false;
                            btnViajeEnCurso.Enabled = false;
                            btnViajeSuspender.Enabled = false;
                            btnViajeConcluir.Enabled = false;
                            btnViajePendiente.Enabled = false;
                            btnViajeCambiarUnidad.Enabled = false;
                            lblESTADOVIAJE.Text = "CONCLUIDO";
                            lblESTADOVIAJE.BackColor = Color.LightSeaGreen;
                            break;

                            

                    }
                }
                if (!dgvViaje.CurrentRow.Cells[2].Value.ToString().Equals("1/1/1900 00:00:00"))
                {
                    lblViajeSalida.Text = Convert.ToDateTime(dgvViaje.CurrentRow.Cells[2].Value.ToString()).ToShortDateString();
                }
                else
                {
                    lblViajeSalida.Text = "Pendiente";
                }
                if (!dgvViaje.CurrentRow.Cells[3].Value.ToString().Equals("1/1/1900 00:00:00"))
                {
                    lblViajeRegreso.Text = Convert.ToDateTime(dgvViaje.CurrentRow.Cells[3].Value.ToString()).ToShortDateString();
                }
                else
                {
                    lblViajeRegreso.Text = "Pendiente";
                }
             
            }
            catch
            {

            }
        }

        #endregion

        private void btnCrearViaje_Click(object sender, EventArgs e)
        {
            N_Camion n_camion = new N_Camion(usuario);
            N_Acoplado n_acoplado = new N_Acoplado(usuario);
            N_Viaje n_viaje = new N_Viaje(usuario);
            N_Pedido n_pedido = new N_Pedido(usuario);
            E_UnidadesViaje e_unidades = new E_UnidadesViaje();
            E_Camion e_camion;
            E_Acoplado e_acoplado;
            E_Viaje e_viaje = new E_Viaje();
            if (dgvCamion.Rows.Count > 0 && dgvAcoplado.Rows.Count > 0 )
            {
                if(selecciona_camion && selecciona_acoplado)
                {
                    e_camion = (E_Camion)n_camion.retornaCamion(Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value));
                    e_unidades.Id_camion = e_camion.Id_Dominio;
                    e_acoplado = (E_Acoplado)n_acoplado.retornaAcoplado(Convert.ToInt32(dgvAcoplado.CurrentRow.Cells[0].Value));
                    e_unidades.Id_acoplado = e_acoplado.Id_Dominio;
                    e_viaje.Id_pedido = e_pedido.Id_pedido;
                    e_viaje.Id_unidades_viaje = e_unidades.Id;
                    //e_viaje.Fecha_salida = e_pedido.Fecha_salida;
                    //e_viaje.Fecha_regreso = e_pedido.Fecha_regreso;
                    e_viaje.Estado = "PENDIENTE";
                    List<string> resp = n_viaje.insertar(e_viaje, e_unidades);
                    if (resp != null)
                    {
                        e_pedido = n_pedido.retornaPedido(e_pedido.Id_pedido);
                        e_pedido.Estado = "CARGADO";
                        List<string> resp2 = n_pedido.modificar(e_pedido);
                        if (resp2 != null)
                        {
                            new frmMensajes("Exito", "Viaje fue creado exitosamente").ShowDialog();
                            
                            dtViaje = n_viaje.listarEstado(Estado.Todos);
                            dgvViaje.DataSource = dtViaje;
                            carga_datos_label_viaje();
                            cbViajeFiltrar.SelectedIndex = 0;
                            carga_cbfitrarviaje2_completo(true);
                            btnViajeCancelarCambioUnidad_Click(null,null);
                         
                        }
                    }
                }
                else
                    new frmMensajes("Error", "Debe seleccionar un camión y un remolque para poder crear el viaje").ShowDialog();
            }
            else if (dgvCamion.Rows.Count > 0 && selecciona_camion)
            {
                e_camion = (E_Camion)n_camion.retornaCamion(Convert.ToInt32(dgvCamion.CurrentRow.Cells[0].Value));
                e_unidades.Id_camion = e_camion.Id_Dominio;
                e_unidades.Id_acoplado = -1;
                e_viaje.Id_pedido = e_pedido.Id_pedido;
                e_viaje.Id_unidades_viaje = e_unidades.Id;
                //e_viaje.Fecha_salida = e_pedido.Fecha_salida;
                //e_viaje.Fecha_regreso = e_pedido.Fecha_regreso;
                e_viaje.Estado = "PENDIENTE";
                List<string> resp = n_viaje.insertar(e_viaje, e_unidades);
                if (resp != null)
                {
                    e_pedido = n_pedido.retornaPedido(e_pedido.Id_pedido);
                    e_pedido.Estado = "CARGADO";
                    List<string> resp2 = n_pedido.modificar(e_pedido);
                    if (resp2 != null)
                    {
                        new frmMensajes("Exito", "Viaje fue creado exitosamente").ShowDialog();
                        dtViaje = n_viaje.listarEstado(Estado.Todos);
                        dgvViaje.DataSource = dtViaje;
                        carga_datos_label_viaje();
                        cbViajeFiltrar.SelectedIndex = 0;
                        carga_cbfitrarviaje2_completo(true);
                        btnViajeCancelarCambioUnidad_Click(null, null);
            
                    }
                }
            }
                       
            else
                new frmMensajes("Error", "Debe seleccionar un camión para poder crear el viaje").ShowDialog();
            dtViaje = n_viaje.listarEstado(Estado.Todos);
            dgvViaje.DataSource = dtViaje;
            this.escondeBotonesEstadosViaje();
            carga_datos_label_viaje();
            cbViajeFiltrar.SelectedIndex = 0;
            dgvCamion.DataSource = null;
            dgvAcoplado.DataSource = null;
            limpiaBindingCamion();
            limpiaTextboxBindingCamion();
            limpiaBindingRemolque();
            limpiaTextboxBindingRemolque();
            btnViajeConcluir.Enabled = false;
            btnViajeEnCurso.Enabled = false;
            btnViajePendiente.Enabled = false;
            btnViajeSuspender.Enabled = false;
            btnViajeCancelar.Enabled = false;
        }

        private void btnSeleccionarCamion_Click(object sender, EventArgs e)
        {
            //Le agrego cambiar unidad,porque si se trata de cambio de unidad
            if (cambiar_unidad)
            {

                if (sender == btnSeleccionarCamion)
                {
                    if (dgvCamion.Rows.Count > 0)
                    {
                        dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSeaGreen;
                        btnSeleccionarCamion.Enabled = false;
                        selecciona_camion = true;
                        btnCrearViaje.Enabled = false;
                        btnCancelarSeleccionCamion.Enabled = true;
                    }
                }
               
            }
            else
            {
                if (dgvCamion.Rows.Count > 0)
                {
                    dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSeaGreen;
                    btnSeleccionarCamion.Enabled = false;
                    selecciona_camion = true;
                    btnCrearViaje.Enabled = true;
                    btnCancelarSeleccionCamion.Enabled = true;
                }
                else if (dgvCamion.Rows.Count > 0 && dgvAcoplado.Rows.Count == 0)
                {
                    dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSeaGreen;
                    btnSeleccionarCamion.Enabled = false;
                    selecciona_camion = true;
                    btnCrearViaje.Enabled = true;
                    selecciona_acoplado = false;
                    btnCancelarSeleccionCamion.Enabled = true;
                }
            }

        }

        private void btnCancelarSeleccionCamion_Click(object sender, EventArgs e)
        {
            if (cambiar_unidad)
            {
                if (sender == btnCancelarSeleccionCamion)
                {
                    dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.Gray;
                    btnCancelarSeleccionCamion.Enabled = false;
                    btnSeleccionarCamion.Enabled = true;
                    selecciona_camion = false;
                }
            }
            else
            {
                if (dgvCamion.Rows.Count > 0 && dgvAcoplado.Rows.Count > 0)
                {
                    dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.Gray;
                    btnSeleccionarCamion.Enabled = true;
                    selecciona_camion = false;
                    btnCrearViaje.Enabled = false;
                    btnCancelarSeleccionCamion.Enabled = false;
                }
                else if (dgvCamion.Rows.Count > 0 && dgvAcoplado.Rows.Count == 0)
                {
                    dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.Gray;
                    btnSeleccionarCamion.Enabled = true;
                    selecciona_camion = false;
                    btnCrearViaje.Enabled = false;
                }
            }

        }

        private void btnSeleccionarRemolque_Click(object sender, EventArgs e)
        {
            if (dgvAcoplado.Rows.Count > 0 && dgvAcoplado.CurrentRow != null)
            {
                if (cambiar_unidad)
                {
                    if (sender == btnSeleccionarRemolque)
                    {
                        if (dgvAcoplado.Rows.Count > 0)
                        {
                            dgvAcoplado.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSeaGreen;
                            btnSeleccionarRemolque.Enabled = false;
                            btnCrearViaje.Enabled = false;
                            selecciona_acoplado = true;
                            btnCancelarSeleccionRemolque.Enabled = true;
                        }
                    }
                }
                else
                {
                    if (dgvAcoplado.Rows.Count > 0)
                    {
                        dgvAcoplado.CurrentRow.DefaultCellStyle.BackColor = Color.MediumSeaGreen;
                        selecciona_acoplado = true;
                        btnCrearViaje.Enabled = true;
                        btnCancelarSeleccionRemolque.Enabled = true;
                        btnSeleccionarRemolque.Enabled = false;
                    }

                }
            }
            else
                new frmMensajes("Error", "Debe seleccionar una unidad para poder continuar").ShowDialog();

        }

        private void btnCancelarSeleccionRemolque_Click(object sender, EventArgs e)
        {
            if (cambiar_unidad)
            {
                if (sender == btnCancelarSeleccionRemolque)
                {
                    dgvAcoplado.CurrentRow.DefaultCellStyle.BackColor = Color.Gray; 
                    btnCancelarSeleccionRemolque.Enabled = false;
                    btnSeleccionarRemolque.Enabled = true;
                    selecciona_acoplado = false;
                }
            }
            if (dgvAcoplado.Rows.Count > 0)
            {
                dgvAcoplado.CurrentRow.DefaultCellStyle.BackColor = Color.Gray;
                selecciona_acoplado = false;
                btnCrearViaje.Enabled = false;
                btnSeleccionarRemolque.Enabled = true;
                btnCancelarSeleccionRemolque.Enabled = false;
            }
        }

        private void dgvCamion_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnSeleccionarCamion.Enabled = true;
            selecciona_camion = false;
            btnCrearViaje.Enabled = false;
            dgvCamion.CurrentRow.DefaultCellStyle.BackColor = Color.White; 
        }

        private void dgvAcoplado_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            btnSeleccionarRemolque.Enabled = true;
            selecciona_acoplado = false;
            btnCrearViaje.Enabled = false;
            dgvAcoplado.CurrentRow.DefaultCellStyle.BackColor = Color.White;
        }

            #region viajes por salir, timer,conf alarma.calendarios

            private void btnViajePorSalir_Click(object sender, EventArgs e)//seteamos los filtros para listar los viajes por salir
            {
                if (sender == btnViajePorCerrar)
                {
                    cbViajeFiltrar.SelectedIndex = 2;
                    cbViajeFiltrar2.SelectedIndex = 2;
                    dtpbViajeBusquedaDesde.Value = DateTime.Now.AddDays(-365);
                    dtpViajeBusquedaHasta.Value = DateTime.Now.AddDays(viajesPorSalir);
                }
                if (sender == btnViajePorSalir)
                {
                    cbViajeFiltrar.SelectedIndex = 1;
                    cbViajeFiltrar2.SelectedIndex = 1;
                    dtpbViajeBusquedaDesde.Value = DateTime.Now.AddDays(-365);
                    dtpViajeBusquedaHasta.Value = DateTime.Now.AddDays(viajesPorSalir);
                }
            }
            private void timer_Tick(object sender, EventArgs e)
            {
                this.timerCount = this.timerCount + 1;
                //comienzo a CONTROLAR los colores de los botones de alarmas
                if (viajesPorSalir > 0)
                {
                    if (btnViajePorSalir.BackColor == Color.Transparent)
                    {
                        btnViajePorSalir.BackColor = Color.Yellow;
                    }
                    else
                    {
                        btnViajePorSalir.BackColor = Color.Transparent;
                    }
                }
                if (viajesPorCerrar > 0)
                {
                    if (btnViajePorCerrar.BackColor == Color.Transparent)
                    {
                        btnViajePorCerrar.BackColor = Color.Yellow;
                    }
                    else
                    {
                        btnViajePorCerrar.BackColor = Color.Transparent;
                    }
                }

                try
                {
                    if (timerCount == 300)
                    {
                        timerCount = 0;
                        viajesPorSalir = n_viaje.listar_Fecha_salida_pedido(DateTime.Now.AddDays(-365).ToShortDateString(), DateTime.Now.AddDays(dateSalidaConf).ToShortDateString(), Estado.Pendiente).Rows.Count;
                        viajesPorCerrar = n_viaje.listar_Fecha_Regreso_pedido(DateTime.Now.AddDays(-365).ToShortDateString(), DateTime.Now.AddDays(dateRegresoConf).ToShortDateString(), Estado.EnCurso).Rows.Count;
                    }
                }
                catch
                {

                }


            }
            void confAlarma(bool ver, int salida, int llegada)//funcion que configura el panel de alarmas(visible,rengos de busquedas) usado por delegado de frm confAlarma
            {
                if (!ver)
                {
                    pnlViajeAlarmas.Visible = false;
                    timer.Stop();
                }
                else
                {
                    pnlViajeAlarmas.Visible = true;
                    dateSalidaConf = salida;
                    dateRegresoConf = llegada;
                    timer.Start();
                }
            }
            private void btnAlarmaVisible_Click(object sender, EventArgs e)
            {
                frmConfAlarmas frm = new frmConfAlarmas();
                frm.enviarConf += confAlarma;
                frm.ShowDialog();
            }

            private void btnViajeCalendarCamion_Click(object sender, EventArgs e)
            {
                if (sender == btnViajeCalendarCamion)
                {
                    dtViaje = n_viaje.listarPorUnidadesViaje(lblViajeCamionDominio.Text);
                    if (dtViaje != null)
                    {
                        new frmCalendario(dtViaje).ShowDialog();
                    }
                    else
                    {
                    new frmMensajes("Error","No pudimos c cargar calendario");
                    }

                }
                if (sender == btnViajeCalendarAcoplado)
                {
                    dtViaje = n_viaje.listarPorUnidadesViaje(lblViajeAcoplado1Dominio.Text);
                    if (dtViaje != null)
                    {
                        new frmCalendario(dtViaje).ShowDialog();
                    }
                    else
                    {
                    new frmMensajes("Error", "No pudimos c cargar calendario");
                }
                }
            }

        #endregion
        #region MANTENIMIENTO
        #region bindingACOPLADO  MANT
        private void columnas_invisibles_AcopladoMant()
        {
            //dgvAcoplado.Columns[0].Visible = false; //Ticket
            //dgvAcoplado.Columns[1].Visible = false; //Dominio
            //dgvAcoplado.Columns[2].Visible = false; //Marca
            //dgvAcoplado.Columns[3].Visible = false; //Modelo
            dgvAcopladoMant.Columns[4].Visible = false; //Año
            dgvAcopladoMant.Columns[5].Visible = false;//Tara
            dgvAcopladoMant.Columns[6].Visible = false;//Km unidad
            dgvAcopladoMant.Columns[7].Visible = false; //Km service
            dgvAcopladoMant.Columns[8].Visible = false; //km cambio neumaticos
            dgvAcopladoMant.Columns[9].Visible = false; //Km service suma
            dgvAcopladoMant.Columns[10].Visible = false; //Km cambio neumaticos suma
            dgvAcopladoMant.Columns[11].Visible = false; //Estado acoplado
            //dgvAcoplado.Columns[12].Visible = false;//Tipo Acoplado
            dgvAcopladoMant.Columns[13].Visible = false;//Fecha de entrada
            dgvAcopladoMant.Columns[14].Visible = false;//Entrega Aprox
            dgvAcopladoMant.Columns[15].Visible = false;//Entregado
            dgvAcopladoMant.Columns[16].Visible = false;//Observaciones
            dgvAcopladoMant.Columns[17].Visible = false;//Tipo taller
            dgvAcopladoMant.Columns[18].Visible = false;//Estado 
            dgvAcopladoMant.Columns[19].Visible = false;//Num legajo
                                                        //dgvAcoplado.Columns[20].Visible = false;//Usuario

            dgvAcopladoMant.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAcopladoMant.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcopladoMant.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcopladoMant.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcopladoMant.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvAcopladoMant.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvAcopladoMant.Columns[20].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;

            //            Acoplado
            //0 Ticket 1 Dominio 2 Marca 3 Modelo 4 Año
            //5 Tipo de acoplado 6 Tara 
            //7 Km unidad 8 Km service 9 Km cambio neumaticos
            //10 Km service suma 11 Km cambio neumaticos suma 
            //12 Estado 13 Fecha de entrada 14 Entrega aprox
            //15 Entregado(fecha de allta seria) 16 Observaciones 17 Tipo taller
            //18 Estado taller 19 Num legajo 20 Usuario


        }
        private void carga_datos_label_AcopladoMant()
        {
            this.limpiaBindingAcopladoMant();
            this.controlaBindingCountAcopladoMant();

            if (dtTallerAcoplado != null && dtTallerAcoplado.Rows.Count >= 0)
            {
                this.columnas_invisibles_AcopladoMant();

                tbAcopladoObservaciones.DataBindings.Add(new Binding("Text", dtTallerAcoplado, "Observaciones"));
                gbAcopladoTipoMantenimiento.Visible = true;

            }
            else
            {
                dgvAcoplado.BackgroundColor = Color.LightGray;
                gbCamionTipoMantenimiento.Visible = false;
            }

        }
        private void controlaBindingCountAcopladoMant()
        {

            if (tbAcopladoObservaciones.DataBindings.Count == 0)
                tbAcopladoObservaciones.Text = "-----";
        }
        private void limpiaBindingAcopladoMant()
        {
  
            tbAcopladoObservaciones.DataBindings.Clear();
        }
        private void limpiaTextboxBindingAcopladoMant()
        {

            tbAcopladoObservaciones.Text = "-----";
        }
        #endregion
        private void columnas_invisibles_camionMant()
        {
            dgvCamionMant.Columns[4].Visible = false; //Año
            dgvCamionMant.Columns[5].Visible = false; //Tara
            dgvCamionMant.Columns[6].Visible = false; //Km Unidad
            dgvCamionMant.Columns[7].Visible = false; //Km service
            dgvCamionMant.Columns[8].Visible = false; //Km cambio de neumaticos
            dgvCamionMant.Columns[9].Visible = false; //Km service suma
            dgvCamionMant.Columns[10].Visible = false;//Km neumaticos suma
            dgvCamionMant.Columns[11].Visible = false;//Estado
            dgvCamionMant.Columns[13].Visible = false;//Fecha de entrada
            dgvCamionMant.Columns[14].Visible = false;//Entrega aprox
            dgvCamionMant.Columns[15].Visible = false;//Entregado
            dgvCamionMant.Columns[16].Visible = false;//Observaciones
            dgvCamionMant.Columns[17].Visible = false;//Tipo de taller
            dgvCamionMant.Columns[18].Visible = false;//Estado taller
            dgvCamionMant.Columns[19].Visible = false;//Num legajo

            dgvCamionMant.Columns[0].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCamionMant.Columns[1].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamionMant.Columns[2].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamionMant.Columns[3].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamionMant.Columns[12].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;
            dgvCamionMant.Columns[19].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight;
            dgvCamionMant.Columns[20].DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleLeft;


            //dgvCamion.Columns[18].Visible = false;//Num legajo
            //dgvCamion.Columns[19].Visible = false;//Usuario
            //Camion

            //                0 Ticket
            //                1 Dominio 
            //                2 Marca  
            //                3 Modelo
            //                4 Año   --false -- > label
            //                5 Tara
            //                6 Km Unidad   --false -- > label
            //                7 Km service  --false -- >label
            //                8 Km cambio de neumaticos-- false -- > label
            //                9 Km service suma -- false -- > label
            //                10 Km neumaticos suma-- false -- > label
            //                11 Estado camion -- false -- > que hacemos con esto ?
            //                12 Tipo camion 
            //                13 Fecha de entrada --false --> Dtp
            //                14 Entrega aprox --false -- > Dtp
            //                15 Entregado  --false -- > Dtp
            //                16 Observaciones --false -- > tb
            //                17 Tipo taller--false -- > Checkbox
            //                18 Estado taller--false -- quiza lo manejemos por colores
            //                19 Num legajo-- false--label
            //                20 Usuario -- false label
        }
        private void carga_datos_label_camionMant()
        {
            this.limpiaBindingCamionMant();
            this.controlaBindingCountCamionMant();

            if (dtTallerCamion != null && dtTallerCamion.Rows.Count >= 0)
            {
                this.columnas_invisibles_camionMant();

                tbCamionObservaciones.DataBindings.Add(new Binding("Text", dtTallerCamion, "Observaciones"));
                gbCamionTipoMantenimiento.Visible = true;



            }
            else
            {
                gbCamionTipoMantenimiento.Visible = false;
                dgvCamion.BackgroundColor = Color.LightGray;
            }


        }
        private void controlaBindingCountCamionMant()
        {

            if (tbCamionObservaciones.DataBindings.Count == 0)
                tbCamionObservaciones.Text = "-----";
        }
        private void limpiaBindingCamionMant()
        {
            tbCamionObservaciones.DataBindings.Clear();
        }
        private void limpiaTextboxBindingCamionMant()//debo vver donde se  usa
        {

            tbCamionObservaciones.Text = "-----";
        }
        void Carga_cb_tipo_de_mantenimiento(string taller_tipo, CheckBox service, CheckBox neumaticos, CheckBox otros)
        {
            string[] Separador = new string[] { "+" };
            service.Checked = false;
            neumaticos.Checked = false;
            otros.Checked = false;
            string[] tipos = taller_tipo.Split(Separador, StringSplitOptions.RemoveEmptyEntries);
            foreach (string tipo in tipos)
            {
                if (tipo.ToUpper().Equals("SERVICE"))
                {
                    service.Checked = true;
                }
                if (tipo.ToUpper().Equals("NEUMATICOS"))
                {
                    neumaticos.Checked = true;
                }
                if (tipo.ToUpper().Equals("OTROS"))
                {
                    otros.Checked = true;
                }
            }
        }


        private void cbFiltroCamion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == cbFiltroCamion)
            {
                if (cbFiltroCamion.SelectedIndex == 1)//POR REVISAR
                {
                    cbFiltroCamion2.Items.Clear();
                    cbFiltroCamion2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada" });
                    cbFiltroCamion2.SelectedIndex = 0;
                }
                else
                {
                    cbFiltroCamion2.Items.Clear();
                    cbFiltroCamion2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada", "Fecha de entrega" });
                    cbFiltroCamion2.SelectedIndex = 0;
                }
                    switch (cbFiltroCamion.SelectedIndex)
                {
                    case 0:
                        estadoTallerCamion = EstadoTaller.Todos;
                        dtTallerCamion = n_Taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvCamionMant.DataSource = dtTallerCamion;
                        carga_datos_label_camionMant();
                        //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                        //dtpCamionBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaCamion.Clear();
                        break;
                    case 1:
                        estadoTallerCamion = EstadoTaller.PorRevisar;
                        dtTallerCamion = n_Taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvCamionMant.DataSource = dtTallerCamion;
                        carga_datos_label_camionMant();
                        //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                        //dtpCamionBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaCamion.Clear();
                        break;
                    case 2:
                        estadoTallerCamion = EstadoTaller.EnProceso;
                        dtTallerCamion = n_Taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvCamionMant.DataSource = dtTallerCamion;
                        carga_datos_label_camionMant();
                        //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                        //dtpCamionBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaCamion.Clear();
                        break;
                    case 3:
                        estadoTallerCamion = EstadoTaller.Concluido;
                        dtTallerCamion = n_Taller.Listar_por_Estado_Camion(estadoTallerCamion);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvCamionMant.DataSource = dtTallerCamion;
                        carga_datos_label_camionMant();
                        //dtpbCamionBusquedaDesde.Value = DateTime.Now;
                        //dtpCamionBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaCamion.Clear();
                        break;
                }
            }

            if (sender == cbFiltroCamion2) //si el sender  es cb filtro camion 2
            {
                if (sender == cbFiltroCamion2) //si el sender  es cb filtro camion 2
                {
                    switch (cbFiltroCamion2.SelectedIndex)//controlo si muestro los dtp o el textbox del segundo filtro de camion
                    {
                        case 0:
                            funciones.esconderFiltroBusquedaFechaTallerCamion(true, pnlCamionBusquedaFecha, tbBusquedaCamion);
                            break;
                        case 1:
                            funciones.esconderFiltroBusquedaFechaTallerCamion(false, pnlCamionBusquedaFecha, tbBusquedaCamion);
                            break;
                        case 2:
                            funciones.esconderFiltroBusquedaFechaTallerCamion(false, pnlCamionBusquedaFecha, tbBusquedaCamion);
                            break;

                    }
                }
            }
        }
        private void cbFiltroAcoplado_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender == cbFiltroAcoplado)
            {
                if (cbFiltroAcoplado.SelectedIndex == 1)//POR REVISAR
                {
                    cbFiltroAcoplado2.Items.Clear();
                    cbFiltroAcoplado2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada" });
                    cbFiltroAcoplado2.SelectedIndex = 0;
                }
                else
                {
                    cbFiltroAcoplado2.Items.Clear();
                    cbFiltroAcoplado2.Items.AddRange(new string[] { "Ticket, observaciones,dominio,marca ", "Fecha de entrada", "Fecha de entrega" });
                    cbFiltroAcoplado2.SelectedIndex = 0;
                }
                switch (cbFiltroAcoplado.SelectedIndex)
                {
                    case 0:
                        estadoTallerAcoplado = EstadoTaller.Todos;
                        dtTallerAcoplado = n_Taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvAcopladoMant.DataSource = dtTallerAcoplado;
                        carga_datos_label_AcopladoMant();
                        //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                        //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaAcoplado.Clear();
                        break;
                    case 1:
                        estadoTallerAcoplado = EstadoTaller.PorRevisar;
                        dtTallerAcoplado = n_Taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvAcopladoMant.DataSource = dtTallerAcoplado;
                        carga_datos_label_AcopladoMant();
                        //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                        //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaAcoplado.Clear();
                        break;
                    case 2:
                        estadoTallerAcoplado = EstadoTaller.EnProceso;
                        dtTallerAcoplado = n_Taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvAcopladoMant.DataSource = dtTallerAcoplado;
                        carga_datos_label_AcopladoMant();
                        //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                        //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaAcoplado.Clear();
                        break;
                    case 3:
                        estadoTallerAcoplado = EstadoTaller.Concluido;
                        dtTallerAcoplado = n_Taller.Listar_por_Estado_Acoplado(estadoTallerAcoplado);//SI DATATABLE ESTA VACIO  ????MEJORAR ESO CHE
                        dgvAcopladoMant.DataSource = dtTallerAcoplado;
                        carga_datos_label_AcopladoMant();
                        //dtpAcopladoBusquedaDesde.Value = DateTime.Now;
                        //dtpAcopladoBusquedaHasta.Value = DateTime.Now;
                        tbBusquedaAcoplado.Clear();
                        break;
                }
            }

            if (sender == cbFiltroAcoplado2) //si el sender  es cb filtro Acoplado 2
            {
                switch (cbFiltroAcoplado2.SelectedIndex)//controlo si muestro los dtp o el textbox del segundo filtro de Acoplado
                {
                    case 0:
                        funciones.esconderFiltroBusquedaFechaTallerAcoplado(true, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);
                        break;
                    case 1:
                        funciones.esconderFiltroBusquedaFechaTallerAcoplado(false, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);
                        break;
                    case 2:
                        funciones.esconderFiltroBusquedaFechaTallerAcoplado(false, pnlAcopladoBusquedaFecha, tbBusquedaAcoplado);
                        break;

                }
            }

        }
        private void dtpbCamionBusquedaDesde_ValueChanged(object sender, EventArgs e)//Evento de DATE TIME PICKER CAMION
        {
            if (dtpbCamionBusquedaDesde.Value < dtpCamionBusquedaHasta.Value.AddSeconds(60))
            {
                switch (cbFiltroCamion2.SelectedItem.ToString())
                {
                    case "Fecha de entrada":
                        dtTallerCamion = n_Taller.Listar_por_Fecha_Entrada_Camion(dtpbCamionBusquedaDesde.Value.ToShortDateString(), dtpCamionBusquedaHasta.Value.ToShortDateString(), estadoTallerCamion);
                        dgvCamionMant.DataSource = dtTallerCamion;
                        carga_datos_label_camionMant();
                        break;
                    case "Fecha de entrega":
                        dtTallerCamion = n_Taller.Listar_por_Fecha_Salida_Camion(dtpbCamionBusquedaDesde.Value.ToShortDateString(), dtpCamionBusquedaHasta.Value.ToShortDateString(), estadoTallerCamion);
                        dgvCamionMant.DataSource = dtTallerCamion;
                        carga_datos_label_camionMant();
                        break;
                }
            }
            else
            {
                dtpbCamionBusquedaDesde.Refresh();
                dtpCamionBusquedaHasta.Refresh();
                new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
                dtpCamionBusquedaHasta.Focus();
            }
        }

        private void dtpAcopladoBusquedaDesde_ValueChanged(object sender, EventArgs e)
        {
            if (dtpAcopladoBusquedaDesde.Value < dtpAcopladoBusquedaHasta.Value.AddSeconds(60))
            {
                switch (cbFiltroAcoplado2.SelectedItem.ToString())
                {
                    case "Fecha de entrada":
                        dtTallerAcoplado = n_Taller.Listar_por_Fecha_Entrada_Acoplado(dtpAcopladoBusquedaDesde.Value.ToShortDateString(), dtpAcopladoBusquedaHasta.Value.ToShortDateString(), estadoTallerAcoplado);
                        dgvAcopladoMant.DataSource = dtTallerAcoplado;
                        carga_datos_label_AcopladoMant();
                        break;
                    case "Fecha de entrega":
                        dtTallerAcoplado = n_Taller.Listar_por_Fecha_Salida_Acoplado(dtpAcopladoBusquedaDesde.Value.ToShortDateString(), dtpAcopladoBusquedaHasta.Value.ToShortDateString(), estadoTallerAcoplado);
                        dgvAcopladoMant.DataSource = dtTallerAcoplado;
                        carga_datos_label_AcopladoMant();
                        break;
                }
            }
            else
            {
                dtpAcopladoBusquedaDesde.Refresh();
                dtpAcopladoBusquedaHasta.Refresh();
                new frmMensajes("Informacion", "La fecha 'HASTA' es mayor que 'DESDE'").ShowDialog();
                dtpAcopladoBusquedaHasta.Focus();
            }
        }//Evento de DATE TIME PICKER ACOPLADO

        private void tbBusquedaCamion_TextChanged(object sender, EventArgs e)
        {
            if (tbBusquedaCamion.Text.Length >= 1)
            {
                dtTallerCamion = n_Taller.Listar_por_Texto_Camion(estadoTallerCamion, tbBusquedaCamion.Text.Trim());
                dgvCamionMant.DataSource = dtTallerCamion;
                carga_datos_label_camionMant();
            }
        }

        private void tbBusquedaAcoplado_TextChanged(object sender, EventArgs e)
        {
            if (tbBusquedaAcoplado.Text.Length >= 1)
            {
                dtTallerAcoplado = n_Taller.Listar_por_Texto_Acoplado(estadoTallerAcoplado, tbBusquedaAcoplado.Text.Trim());
                dgvAcopladoMant.DataSource = dtTallerAcoplado;
                carga_datos_label_AcopladoMant();
            }
        }
        private void btnCamionVer_Click(object sender, EventArgs e)
        {
            try
            {
                object unidad;
                if (sender == btnCamionVer)
                {
                    unidad = null;
                    unidad = n_camion.retornaCamion(n_dominio.Retorna_Id_Dominio(dgvCamionMant.CurrentRow.Cells[1].Value.ToString()));
                    new frmNuevoCamion(usuario, unidad, true).ShowDialog();
                }
                if (sender == btnAcopladoVer)
                {
                    unidad = null;
                    unidad = n_acoplado.retornaAcoplado(n_dominio.Retorna_Id_Dominio(dgvAcopladoMant.CurrentRow.Cells[1].Value.ToString()));
                    new frmNuevoAcoplado(usuario, unidad, true).ShowDialog();
                }
            }
            catch (Exception ex)
            {
                new frmMensajes("Error", "Tiene que haber un ticket seleccionado para ver detalles de la unidad").ShowDialog();
            }
        }










        #endregion

        private void dgvCamionMant_CurrentCellChanged(object sender, EventArgs e)
        {


            if (dgvCamionMant.CurrentRow != null)
            {
               
                    switch (dgvCamionMant.CurrentRow.Cells[18].Value.ToString())
                    {
                        case "POR_REVISAR":
                            lblCamionEstado.Text = "POR REVISAR";
                            lblCamionEstado.ForeColor = Color.Red;
                            break;
                        case "EN_PROCESO":
                            lblCamionEstado.Text = "EN PROCESO";
                            lblCamionEstado.ForeColor = Color.Orange;
                            break;
                        case "CONCLUIDO":
                            lblCamionEstado.Text = "CONCLUIDO";
                            lblCamionEstado.ForeColor = Color.ForestGreen;
                            break;
                    }
                    if (!dgvCamionMant.CurrentRow.Cells[13].Value.ToString().Equals("1/1/1900 00:00:00"))
                    {
                        lblCamionDtpEntrada.Text = Convert.ToDateTime(dgvCamionMant.CurrentRow.Cells[13].Value.ToString()).ToShortDateString();
                    }
                    else
                    {
                        lblCamionDtpEntrada.Text = "----";
                    }
                    if (!dgvCamionMant.CurrentRow.Cells[15].Value.ToString().Equals("1/1/1900 00:00:00"))
                    {
                        lblCamionDtpEntregado.Text = Convert.ToDateTime(dgvCamionMant.CurrentRow.Cells[15].Value.ToString()).ToShortDateString();
                    }
                    else
                    {
                        lblCamionDtpEntregado.Text = "----";
                    }
                    if (dgvCamionMant.CurrentRow.Cells[13].Value == null)
                    {
                        lblCamionDtpEntrada.Text = Convert.ToDateTime(dgvCamionMant.CurrentRow.Cells[13].Value.ToString()).ToShortDateString();
                    }//cargamos DTPSSSS
                    if (!dgvCamionMant.CurrentRow.Cells[14].Value.ToString().Equals("1/1/1900 00:00:00"))
                    { // si el campo salida aproximada tiene valor que no sea null
                        lblCamionDtpSalidaAprox.Visible = true;
                        lblCamionSalidaAproxText.Visible = true;
                        lblCamionDtpSalidaAprox.Text = Convert.ToDateTime(dgvCamionMant.CurrentRow.Cells[14].Value.ToString()).ToShortDateString();
                        TimeSpan intervalo = Convert.ToDateTime(dgvCamionMant.CurrentRow.Cells[14].Value.ToString()).Date - Convert.ToDateTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                        lblCamionDiasAFavor.Text = intervalo.Days.ToString();//cargamos el label dias a favor
                    }
                    else
                    {
                        lblCamionDtpSalidaAprox.Visible = false;
                        lblCamionSalidaAproxText.Visible = false;
                        lblCamionDiasAFavor.Text = "----";
                    }//ahora cargamos los chbox
                    Carga_cb_tipo_de_mantenimiento(dgvCamionMant.CurrentRow.Cells[17].Value.ToString().ToUpper(), cbCamionService, cbCamionNeumaticos, cbCamionOtros);
                }
            
        }

        private void dgvAcopladoMant_CurrentCellChanged_1(object sender, EventArgs e)
        {
            if (dgvAcopladoMant.CurrentRow != null)
            {
                //{
                switch (dgvAcopladoMant.CurrentRow.Cells[18].Value.ToString())
                {
                    case "POR_REVISAR":
                        lblAcopladoEstado.Text = "POR REVISAR";
                        lblAcopladoEstado.ForeColor = Color.Red;
                        break;
                    case "EN_PROCESO":
                        lblAcopladoEstado.Text = "EN PROCESO";
                        lblAcopladoEstado.ForeColor = Color.Orange;
                        break;
                    case "CONCLUIDO":
                        lblAcopladoEstado.Text = "CONCLUIDO";
                        lblAcopladoEstado.ForeColor = Color.ForestGreen;
                        break;
                }
                if (!dgvAcopladoMant.CurrentRow.Cells[13].Value.ToString().Equals("1/1/1900 00:00:00"))
                {
                    lblAcopladoDtpEntrada.Text = Convert.ToDateTime(dgvAcopladoMant.CurrentRow.Cells[13].Value.ToString()).ToShortDateString();
                }
                else
                {
                    lblAcopladoDtpEntrada.Text = "----";
                }
                if (!dgvAcopladoMant.CurrentRow.Cells[15].Value.ToString().Equals("1/1/1900 00:00:00"))
                {
                    lblAcopladoDtpEntregado.Text = Convert.ToDateTime(dgvAcopladoMant.CurrentRow.Cells[15].Value.ToString()).ToShortDateString();
                }
                else
                {
                    lblAcopladoDtpEntregado.Text = "----";
                }
                //cargamos DTPS
                if (!dgvAcopladoMant.CurrentRow.Cells[14].Value.ToString().Equals("1/1/1900 00:00:00"))// esto es porque el ticket  TIENE respondable ASIGNADO
                { // si el campo salida aproximada tiene valor que no sea null
                    lblAcopladoDtpSalidaAprox.Visible = true;
                    lblAcopladoSalidaAproxText.Visible = true;
                    lblAcopladoDtpSalidaAprox.Text = Convert.ToDateTime(dgvAcopladoMant.CurrentRow.Cells[14].Value.ToString()).ToShortDateString();
                    TimeSpan intervalo = Convert.ToDateTime(dgvAcopladoMant.CurrentRow.Cells[14].Value.ToString()).Date - Convert.ToDateTime(new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day));
                    lblAcopladoDiasAFavor.Text = intervalo.Days.ToString();//cargamos el label dias a favor
                }
                else
                {
                    lblAcopladoDtpSalidaAprox.Visible = false;
                    lblAcopladoSalidaAproxText.Visible = false;
                    lblAcopladoDiasAFavor.Text = "----";
                }//ahora cargamos los chbox
                Carga_cb_tipo_de_mantenimiento(dgvAcopladoMant.CurrentRow.Cells[17].Value.ToString().ToUpper(), cbAcopladoService, cbAcopladoNeumaticos, cbAcopladoOtros);


                //}
            }

        }

        private void frmLogistica_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.Hide();
        }

        private void gbLogistica_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPageIndex == 2)
            {
                n_Taller.Listar_por_Estado_Camion(EstadoTaller.Todos);
                n_Taller.Listar_por_Estado_Acoplado(EstadoTaller.Todos);
            }
            if(e.TabPageIndex == 1)
            {
                btnSeleccionPedido.Enabled = true;
                btnCrearViaje.Enabled = true;
            }

        }
    }
} 
//para soolucionar el tema de viaje cambio de unidades para que vayan a taller
//tengo que hacer dos funciones (una para camion y otra para acoplado) ejemplo ::ACOPLADO_VA_A_TALLER???(STRING DOMINIO)
//QUE SERVIRIA PARA SABER SI ESE 