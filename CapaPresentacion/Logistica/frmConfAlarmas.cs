﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion.Logistica
{
 
    public partial class frmConfAlarmas : Form
    {
        public delegate void configurarAlarma(bool visible, int diasSalida, int diasLlegada);
        public event configurarAlarma enviarConf;
        public frmConfAlarmas()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)

        {
            if(sender == btnOk)
            {
                enviarConf(cbVer.Checked, Convert.ToInt32(nudSalir.Value), Convert.ToInt32(nudLLegada.Value));
                this.Hide();
            }
            if (sender == btnCancelar)
            {
                this.Close();
            }
        }
    }
}
