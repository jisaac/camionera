﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaEntidades;
using CapaNegocio;
using CapaPresentacion.Logistica;
using CapaPresentacion.FormsMsj;

namespace CapaPresentacion.Logistica
{
    //Para las observaciones se vera un tb vacio...al guardar tendriamos que poner e_viaje.observaciones+"salto de linea"+tbobserbaciones.text
    public partial class frmEditarViaje : Form
    {
        public delegate void editarViaje(E_Viaje e_viaje);
        public event editarViaje enviaViaje;
        public delegate void estadoViaje(E_Viaje e_viaje,Estado e,float kmActual);
        public event estadoViaje cambiarEstadoViaje;
        public delegate void enviaTallerUnidadDatos(E_Taller e_Taller);
        public event enviaTallerUnidadDatos enviaTaller;
        public delegate void enviaCancelaEnvio();
        public event enviaCancelaEnvio EnviaCancelaEnvio;


        public frmEditarViaje(E_Camion e_camion, E_Viaje e_viaje,Estado estado)//Concluir viajee
        {
            InitializeComponent();
            this.estado = estado;
            this.e_viaje = e_viaje;
            lblEstadoViaje.Text = "CONCLUIR VIAJE";
            pnlKmInput.Visible = true;
            e_CamionConcluir = e_camion;

        }

        public frmEditarViaje(E_Viaje e_viaje,E_Camion e_pendiente, Estado estado)
        {
            InitializeComponent();
        
            this.e_viaje = e_viaje;
            this.e_PendienteCamion = e_pendiente;
            
            this.estado = estado;
            switch (estado)
            {
                case Estado.Todos:
                    lblEstadoViaje.Text = "EDITAR OBSERVACIONES";
                    pnlKmInput.Visible = false;
                    break;
                case Estado.EnCurso:

                    lblEstadoViaje.Text = "VIAJE EN CURSO";
                    pnlKmInput.Visible = false;
                    break;
                case Estado.Pendiente:
                    lblEstadoViaje.Text = "VIAJE PENDIENTE";
                    if (e_viaje.Estado.ToString().Equals("EN CURSO"))
                    {
                        pnlKmInput.Visible = true;
                    }
                    else
                    {
                        pnlKmInput.Visible = false;
                    }

                    break;
                case Estado.Suspendido:
                    lblEstadoViaje.Text = "SUSPENDER VIAJE";
                    pnlKmInput.Visible = false;
                    break;
                case Estado.Cancelado:
                    lblEstadoViaje.Text = "CANCELAR VIAJE";
                    pnlKmInput.Visible = false;
                    break;
            }



        }
        public frmEditarViaje(E_Taller e_Taller, E_Camion e_Camion)
        {
            InitializeComponent();
            this.e_Taller = e_Taller;
            this.e_Camion = e_Camion;
            lblEstadoViaje.Text = "ENVIAR CAMION A TALLER";
            pnlKmInput.Visible = false;
        }
        public frmEditarViaje(E_Taller e_Taller, E_Acoplado e_Acoplado)
        {
            InitializeComponent();
            this.e_Taller = e_Taller;
            this.e_Acoplado = e_Acoplado;
            lblEstadoViaje.Text = "ENVIAR REMOLQUE A TALLER";
            pnlKmInput.Visible = false;


        }
        E_Camion e_CamionConcluir;
        E_Camion e_Camion;
        E_Acoplado e_Acoplado;
        Estado estado;
        E_Viaje e_viaje;
        E_Taller e_Taller;
        E_Camion e_PendienteCamion;
        

        private void btnOk_Click(object sender, EventArgs e)
        {
            
            if (sender == btnOk)
            {
                if (e_Camion != null || e_Acoplado!=null)//TALLER
                {
                     if (tbObservaciones.Text.Trim().Length < 1)
                        {
                             new frmMensajes("Informacion","Debe ingresar un comentario en el campo de observaciones").ShowDialog();
                            tbObservaciones.Focus();
                        }
                        else
                        {
                            e_Taller.Tipo = e_Taller.Tipo + "+OTROS";
                            e_Taller.Observaciones = tbObservaciones.Text;
                          
                           
                        }
                    if (e_Camion != null)
                        e_Taller.id_dominio = e_Camion.Id_Dominio;
                    else
                        e_Taller.id_dominio = e_Acoplado.Id_Dominio;
                    frmMensajes tr = new frmMensajes("Pregunta", "Seguro desea Guardar los cambios realizados?");
                    tr.SioNo += (bool r) =>
                    {
                        if (r)
                        {
                            this.Hide();
                            enviaTaller(e_Taller);//envia el taller ya seteado
                           
                        }
                    };
                    tr.ShowDialog();
                   
       
                }
                else if (e_CamionConcluir != null)
                {
                    if( float.Parse(tbKm.Text) < e_CamionConcluir.Km_unidad)
                    {
                        new frmMensajes("Error","Dato erroneo! "+Environment.NewLine+" Esta ingresando un kilometraje actual menor al que ya posee el CAMION (" + e_CamionConcluir.Km_unidad.ToString() + " KM) ! ").ShowDialog();
                        tbKm.Focus();
                    }
                    else
                    {
                        e_viaje.Fecha_regreso = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                        e_viaje.Observaciones = e_viaje.Observaciones + Environment.NewLine + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString() + " > " + tbObservaciones.Text;
                        cambiarEstadoViaje(e_viaje, estado, float.Parse(tbKm.Text));
                        this.Dispose();
                    }
                    
                }
                //pregunto si desea guardar los cambios y en cancelar, si desea salir sin guardar los cambios
                else
                {
                    switch (estado)//segun el cambio de estado que se haga depende la accion que se va a tomar
                    {
                        case Estado.Todos:
                            frmMensajes ccm = new frmMensajes("Pregunta", "Seguro desea Guardar los cambios realizados ? ");
                            ccm.SioNo += (bool r) =>
                              {
                                  if (r)
                                  {
                                      e_viaje.Observaciones = e_viaje.Observaciones + Environment.NewLine + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString() + " > " + tbObservaciones.Text;
                                      enviaViaje(e_viaje);
                                  }
                              };
                            ccm.ShowDialog();
                            this.Dispose();
                            break;
                 

                        case Estado.EnCurso:
                            e_viaje.Fecha_salida = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 00);
                            e_viaje.Observaciones = e_viaje.Observaciones + Environment.NewLine + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString() + " > " + tbObservaciones.Text;
                            cambiarEstadoViaje(e_viaje, estado, 0);//parametro km en 0 porque no va a tener uso alguno
                            this.Dispose();
                            break;
                        case Estado.Pendiente:
                            e_viaje.Fecha_salida = null;
                            e_viaje.Fecha_regreso = null;
                            e_viaje.Observaciones = e_viaje.Observaciones + Environment.NewLine + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString() + " > " + tbObservaciones.Text;
                            if (e_viaje.Estado.Equals("EN CURSO") && e_PendienteCamion != null)
                            {   
                                if (float.Parse(tbKm.Text) < e_PendienteCamion.Km_unidad)
                                {
                                    new frmMensajes("Error", "Dato erroneo! "+Environment.NewLine+" Esta ingresando un kilometraje actual menor al que ya posee el CAMION (" + e_PendienteCamion.Km_unidad.ToString() + " KM) ! ").ShowDialog();
                                    tbKm.Focus();
                                }
                                else
                                {
                                    cambiarEstadoViaje(e_viaje, estado, float.Parse(tbKm.Text));
                                    this.Dispose();
                                }
                                
                            }
                            else
                            {
                                cambiarEstadoViaje(e_viaje, estado, 0);
                                this.Dispose();
                            }

                            break;
                        case Estado.Suspendido:
                            e_viaje.Fecha_salida = null;
                            e_viaje.Fecha_regreso = null;
                            e_viaje.Observaciones = e_viaje.Observaciones + Environment.NewLine + DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToShortTimeString() + " > " + tbObservaciones.Text;
                            cambiarEstadoViaje(e_viaje, estado, 0);
                            this.Dispose();
                            break;
                        case Estado.Cancelado:
                            e_viaje.Fecha_salida = null;
                            e_viaje.Fecha_regreso = null;
                            e_viaje.Observaciones = e_viaje.Observaciones + Environment.NewLine + DateTime.Now.ToLongDateString() +" " +DateTime.Now.ToShortTimeString()+ " > " + tbObservaciones.Text;
                            cambiarEstadoViaje(e_viaje, estado, 0);
                            this.Dispose();
                            break;
                    }
                    
                }
            }
            if (sender == btnCancelar)
            {
                if (e_Camion != null || e_Acoplado != null)
                {
                    EnviaCancelaEnvio();
                    this.Dispose();
                }
                else
                {
                    frmMensajes m = new frmMensajes("Pregunta", "Desea salir sin guardar los cambios?");
                    m.SioNo += (bool r)=>{
                        if (r)
                        {
                            this.Hide();
                            EnviaCancelaEnvio();
                            

                        }
                    
                    };
                    m.ShowDialog();

         
                }
            }
        }

        private void tbKm_KeyPress(object sender, KeyPressEventArgs e)//evento para permitir solo numeros en el tb ...
        {

            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) )
                e.Handled = true;
            
        }

    }
}
