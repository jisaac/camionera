﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Rigido:E_Camion
    {
        public int Tipo_carga { get; set; }
        public float Altura { get; set; }
        public float Ancho_interior { get; set; }
        public float Ancho_exterior { get; set; }
        public float Longitud { get; set; }
        public float Volumen { get; set; }
        public float Capacidad_carga { get; set; }
        public bool Enganche { get; set; }
    }
}
