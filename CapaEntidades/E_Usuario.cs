﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Usuario
    {
        public string usuario { get; set; }
        public string clave { get; set; }
        public int num_legajo { get; set; }
        public int id_rol { get; set; }
        public string estado { get; set; }
    }
}
