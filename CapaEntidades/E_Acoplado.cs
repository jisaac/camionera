﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Acoplado
    {
        public int Id_Dominio { get; set; }
        public int Marca { get; set; }
        public int Modelo { get; set; }
        public int Año { get; set; }
        public int Tipo_carga { get; set; }
        public string Tipo_acoplado { set; get; }
        public float Tara { get; set; }
        public float Capacidad_carga { get; set; }
        public float Volumen { get; set; }
        public float Km_unidad { get; set; }
        public float Km_service { get; set; }
        public float Km_cambio_neumaticos { get; set; }
        public float Km_service_suma { get; set; }
        public float Km_cambio_neumaticos_suma { get; set; }
        public float Altura { get; set; }
        public float Ancho_interior { get; set; }
        public float Ancho_exterior { get; set; }
        public float Longitud { get; set; }
        public DateTime Fecha_alta { get; set; }
        public string Estado { get; set; }
    }
}
