﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Taller
    {
        public int Id_taller { get; set; }
        public string usuario { get; set; }
        public DateTime ? Entrada_taller { get; set; }
        public DateTime ? Salida_aprox { get; set; }
        public DateTime ? Salida { get; set; }
        public string Observaciones { get; set; }
        public int id_dominio { get; set; }
        public string Tipo { get; set; }
        public string Estado { get; set; }
    }
}
