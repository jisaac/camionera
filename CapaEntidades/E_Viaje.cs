﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Viaje
    {
        public int Id_viaje { get; set; }
        public int Id_pedido { get; set; }
        public int Id_unidades_viaje { get; set; }
        public DateTime ? Fecha_salida { get; set; }
        public DateTime ? Fecha_regreso { get; set; }
        public float Km_cierre { get; set; }
        public string Estado { get; set; }
        public string Observaciones { get; set; }
        
    }
}
