﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Pedido
    {
        public int Id_pedido { get; set; }
        public int Id_chofer { get; set; }
        public DateTime Fecha_pedido { get; set; }
        public DateTime Fecha_salida { get; set; }
        public DateTime Fecha_regreso { get; set; }
        public float Peso_neto { get; set; }
        public float Volumen { get; set; }
        public float Km_viaje { get; set; }
        public string Prioridad { get; set; }
        public string Estado { get; set; }
        public string Observaciones { get; set; }
        public string Nombre_cliente { get; set; }
        public string Cuil_cliente { get; set; }
        public string Enganche { get; set; }
        public string Tipo_carga { get; set; }
        public float Altura_camion { get; set; }
        public float Ancho_interior_camion { get; set; }
        public float Ancho_exterior_camion { get; set; }
        public float Longitud_camion { get; set; }
        public float Altura_acoplado { get; set; }
        public float Ancho_interior_acoplado { get; set; }
        public float Ancho_exterior_acoplado { get; set; }
        public float Longitud_acoplado { get; set; }
    }
}
