﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Rol
    {
        public int id { get; set; }
        public string rol { get; set; }
        public string estado { get; set; }
    }
}
