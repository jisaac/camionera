﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public class E_Chofer
    {
        public int Num_legajo { get; set; }
        public string Nombre { get; set; }
        public string Apellido { get; set; }
        public string Num_cedula { get; set; }
        public string Direccion { get; set; }
        public string Tel_personal { get; set; }
        public string Tel_trabajo { get; set; }
        public string Estado { get; set; }
    }
}
