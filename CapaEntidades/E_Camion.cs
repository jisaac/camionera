﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaEntidades
{
    public abstract class E_Camion
    {
        public int Id_Dominio { get; set; }
        public int Marca { get; set; }
        public int Modelo { get; set; }
        public int Año { get; set; }
        public float Tara { get; set; }
        public float Km_unidad { get; set; }
        public float Km_service { get; set; }
        public float Km_cambio_neumaticos { get; set; }
        public float Km_service_suma { get; set; }
        public float Km_cambio_neumaticos_suma { get; set; }
        public DateTime Fecha_alta { get; set; }
        public string Estado { get; set; }
    }
}
